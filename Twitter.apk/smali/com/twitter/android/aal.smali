.class Lcom/twitter/android/aal;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/client/ac;


# instance fields
.field final synthetic a:Lcom/twitter/android/VerifyLoginActivity;


# direct methods
.method public constructor <init>(Lcom/twitter/android/VerifyLoginActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Z)V
    .locals 1

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    invoke-static {v0}, Lcom/twitter/android/VerifyLoginActivity;->p(Lcom/twitter/android/VerifyLoginActivity;)Lcom/twitter/android/widget/ProgressDialogFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    invoke-static {v0}, Lcom/twitter/android/VerifyLoginActivity;->p(Lcom/twitter/android/VerifyLoginActivity;)Lcom/twitter/android/widget/ProgressDialogFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/widget/ProgressDialogFragment;->dismiss()V

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;II[IZ)V
    .locals 6

    const v4, 0x7f0f0570    # com.twitter.android.R.string.wrong_login_verification_code_error

    const/4 v3, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    invoke-virtual {v0}, Lcom/twitter/android/VerifyLoginActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p5}, Lcom/twitter/android/aal;->a(Z)V

    if-ne p2, v3, :cond_2

    iget-object v0, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    const v1, 0x7f0f0495    # com.twitter.android.R.string.sync_contacts_account_create_error

    invoke-virtual {v0, v1}, Lcom/twitter/android/VerifyLoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    invoke-virtual {v0}, Lcom/twitter/android/VerifyLoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v3, "accountAuthenticatorResponse"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/AccountAuthenticatorResponse;

    if-eqz v0, :cond_1

    const/16 v3, 0x190

    invoke-virtual {v0, v3, v1}, Landroid/accounts/AccountAuthenticatorResponse;->onError(ILjava/lang/String;)V

    :cond_1
    move-object v0, v1

    :goto_1
    iget-object v1, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    invoke-static {v1, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    invoke-virtual {v0, v2}, Lcom/twitter/android/VerifyLoginActivity;->setResult(I)V

    iget-object v0, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    invoke-static {v0}, Lcom/twitter/android/VerifyLoginActivity;->o(Lcom/twitter/android/VerifyLoginActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    invoke-static {v1}, Lcom/twitter/android/VerifyLoginActivity;->n(Lcom/twitter/android/VerifyLoginActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v5, "login::::failure"

    aput-object v5, v1, v2

    invoke-virtual {v0, v3, v4, v1}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    invoke-virtual {v0}, Lcom/twitter/android/VerifyLoginActivity;->finish()V

    goto :goto_0

    :cond_2
    if-eqz p4, :cond_3

    array-length v0, p4

    if-nez v0, :cond_5

    :cond_3
    move v0, v2

    :goto_2
    if-eqz p5, :cond_4

    const/16 v1, 0xf1

    if-eq v0, v1, :cond_4

    const/16 v1, 0x58

    if-ne v0, v1, :cond_8

    :cond_4
    if-nez p5, :cond_7

    iget-object v1, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    invoke-static {v1}, Lcom/twitter/android/VerifyLoginActivity;->a(Lcom/twitter/android/VerifyLoginActivity;)I

    move-result v1

    if-ne v1, v3, :cond_6

    iget-object v1, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    const-string/jumbo v3, "native_login:push_verification:backup_code"

    invoke-static {v1, v3, p3, v0}, Lcom/twitter/android/VerifyLoginActivity;->a(Lcom/twitter/android/VerifyLoginActivity;Ljava/lang/String;II)V

    :goto_3
    sparse-switch v0, :sswitch_data_0

    iget-object v0, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    const v1, 0x7f0f0231    # com.twitter.android.R.string.login_error_generic

    invoke-virtual {v0, v1}, Lcom/twitter/android/VerifyLoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_5
    aget v0, p4, v2

    goto :goto_2

    :cond_6
    iget-object v1, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    const-string/jumbo v3, "native_login:sms_verification:login_code"

    invoke-static {v1, v3, p3, v0}, Lcom/twitter/android/VerifyLoginActivity;->a(Lcom/twitter/android/VerifyLoginActivity;Ljava/lang/String;II)V

    goto :goto_3

    :cond_7
    iget-object v1, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    const-string/jumbo v3, "native_login:push_verification:polling"

    invoke-static {v1, v3, p3, v0}, Lcom/twitter/android/VerifyLoginActivity;->a(Lcom/twitter/android/VerifyLoginActivity;Ljava/lang/String;II)V

    goto :goto_3

    :cond_8
    iget-object v0, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    iget-object v1, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    invoke-static {v1}, Lcom/twitter/android/VerifyLoginActivity;->l(Lcom/twitter/android/VerifyLoginActivity;)J

    move-result-wide v1

    long-to-double v1, v1

    const-wide v3, 0x3ff3333333333333L    # 1.2

    mul-double/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->round(D)J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/twitter/android/VerifyLoginActivity;->a(Lcom/twitter/android/VerifyLoginActivity;J)J

    iget-object v0, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    invoke-static {v0}, Lcom/twitter/android/VerifyLoginActivity;->m(Lcom/twitter/android/VerifyLoginActivity;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/aak;

    iget-object v2, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/twitter/android/aak;-><init>(Lcom/twitter/android/VerifyLoginActivity;Lcom/twitter/android/aaj;)V

    iget-object v2, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    invoke-static {v2}, Lcom/twitter/android/VerifyLoginActivity;->l(Lcom/twitter/android/VerifyLoginActivity;)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    :sswitch_0
    iget-object v0, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    invoke-virtual {v0, v4}, Lcom/twitter/android/VerifyLoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :sswitch_1
    iget-object v0, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    const v1, 0x7f0f0235    # com.twitter.android.R.string.login_request_rejected

    invoke-virtual {v0, v1}, Lcom/twitter/android/VerifyLoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :sswitch_2
    iget-object v0, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    invoke-virtual {v0, v4}, Lcom/twitter/android/VerifyLoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x20 -> :sswitch_0
        0xec -> :sswitch_2
        0xf1 -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;Z)V
    .locals 7

    const/4 v1, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    invoke-virtual {v0}, Lcom/twitter/android/VerifyLoginActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p3}, Lcom/twitter/android/aal;->a(Z)V

    iget-object v0, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    invoke-static {v0}, Lcom/twitter/android/VerifyLoginActivity;->a(Lcom/twitter/android/VerifyLoginActivity;)I

    move-result v0

    if-ne v0, v1, :cond_3

    if-nez p3, :cond_3

    iget-object v0, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    invoke-static {v0}, Lcom/twitter/android/VerifyLoginActivity;->c(Lcom/twitter/android/VerifyLoginActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    invoke-static {v1}, Lcom/twitter/android/VerifyLoginActivity;->b(Lcom/twitter/android/VerifyLoginActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "native_login:push_verification:backup_code::success"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :goto_1
    iget-object v0, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    invoke-virtual {v0}, Lcom/twitter/android/VerifyLoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v0, "android.intent.extra.INTENT"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    if-eqz v0, :cond_5

    iget-object v2, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    invoke-virtual {v2, v0}, Lcom/twitter/android/VerifyLoginActivity;->startActivity(Landroid/content/Intent;)V

    :cond_1
    :goto_2
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v0, "accountAuthenticatorResponse"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/AccountAuthenticatorResponse;

    if-eqz v0, :cond_2

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v3, "authAccount"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v2, "accountType"

    sget-object v3, Lcom/twitter/library/util/a;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v2, "account_user_info"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/accounts/AccountAuthenticatorResponse;->onResult(Landroid/os/Bundle;)V

    :cond_2
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v1, "sb_account_name"

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "session"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "user"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/twitter/android/VerifyLoginActivity;->setResult(ILandroid/content/Intent;)V

    iget-object v0, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    invoke-static {v0}, Lcom/twitter/android/VerifyLoginActivity;->i(Lcom/twitter/android/VerifyLoginActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    invoke-static {v1}, Lcom/twitter/android/VerifyLoginActivity;->h(Lcom/twitter/android/VerifyLoginActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "login::::success"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    const v1, 0x7f0f0234    # com.twitter.android.R.string.login_request_approved

    invoke-static {v0, v1, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    invoke-static {v0}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;)Lcom/twitter/android/util/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/android/util/d;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    invoke-static {v0}, Lcom/twitter/android/VerifyLoginActivity;->j(Lcom/twitter/android/VerifyLoginActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/android/client/c;->c(Lcom/twitter/library/client/Session;)Ljava/lang/String;

    :goto_3
    iget-object v0, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    invoke-virtual {v0}, Lcom/twitter/android/VerifyLoginActivity;->finish()V

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    invoke-static {v0}, Lcom/twitter/android/VerifyLoginActivity;->a(Lcom/twitter/android/VerifyLoginActivity;)I

    move-result v0

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    invoke-static {v0}, Lcom/twitter/android/VerifyLoginActivity;->e(Lcom/twitter/android/VerifyLoginActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    invoke-static {v1}, Lcom/twitter/android/VerifyLoginActivity;->d(Lcom/twitter/android/VerifyLoginActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "native_login:push_verification:polling::success"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_4
    iget-object v0, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    invoke-static {v0}, Lcom/twitter/android/VerifyLoginActivity;->g(Lcom/twitter/android/VerifyLoginActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    invoke-static {v1}, Lcom/twitter/android/VerifyLoginActivity;->f(Lcom/twitter/android/VerifyLoginActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "native_login:sms_verification:login_code::success"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_5
    const-string/jumbo v0, "start_main"

    invoke-virtual {v1, v0, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/twitter/android/MainActivity;->a(Landroid/app/Activity;Landroid/net/Uri;)V

    goto/16 :goto_2

    :cond_6
    iget-object v0, p0, Lcom/twitter/android/aal;->a:Lcom/twitter/android/VerifyLoginActivity;

    invoke-static {v0}, Lcom/twitter/android/VerifyLoginActivity;->k(Lcom/twitter/android/VerifyLoginActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/android/client/c;->d(Lcom/twitter/library/client/Session;)Ljava/lang/String;

    goto :goto_3
.end method
