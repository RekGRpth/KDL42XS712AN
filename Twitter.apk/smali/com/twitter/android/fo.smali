.class Lcom/twitter/android/fo;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/EventSearchActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/EventSearchActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/fo;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/fo;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-virtual {v0}, Lcom/twitter/android/EventSearchActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/fo;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-static {v1}, Lcom/twitter/android/EventSearchActivity;->e(Lcom/twitter/android/EventSearchActivity;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/fo;->a:Lcom/twitter/android/EventSearchActivity;

    iget-object v1, p0, Lcom/twitter/android/fo;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-static {v1}, Lcom/twitter/android/EventSearchActivity;->f(Lcom/twitter/android/EventSearchActivity;)Landroid/widget/FrameLayout;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Lcom/twitter/android/EventSearchActivity;->a(Lcom/twitter/android/EventSearchActivity;I)V

    iget-object v0, p0, Lcom/twitter/android/fo;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-static {v0}, Lcom/twitter/android/EventSearchActivity;->f(Lcom/twitter/android/EventSearchActivity;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    return-void
.end method
