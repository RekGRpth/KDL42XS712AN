.class Lcom/twitter/android/ft;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/se;


# instance fields
.field final synthetic a:Lcom/twitter/android/EventSearchActivity;

.field private b:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Lcom/twitter/android/EventSearchActivity;Landroid/net/Uri;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/ft;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/twitter/android/ft;->b:Landroid/net/Uri;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/twitter/android/ft;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-virtual {v0, v4}, Lcom/twitter/android/EventSearchActivity;->a(Z)V

    iget-object v0, p0, Lcom/twitter/android/ft;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-static {v0}, Lcom/twitter/android/EventSearchActivity;->b(Lcom/twitter/android/EventSearchActivity;)Lcom/twitter/android/fr;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/fr;->a(Lcom/twitter/android/fr;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhb;

    iget-object v2, p0, Lcom/twitter/android/ft;->b:Landroid/net/Uri;

    iget-object v3, v0, Lhb;->c:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iput v4, v0, Lhb;->k:I

    iget-object v0, p0, Lcom/twitter/android/ft;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-static {v0}, Lcom/twitter/android/EventSearchActivity;->b(Lcom/twitter/android/EventSearchActivity;)Lcom/twitter/android/fr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/fr;->notifyDataSetChanged()V

    :cond_1
    return-void
.end method

.method public a(I)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/ft;->a:Lcom/twitter/android/EventSearchActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/EventSearchActivity;->a(Z)V

    iget-object v0, p0, Lcom/twitter/android/ft;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-static {v0}, Lcom/twitter/android/EventSearchActivity;->b(Lcom/twitter/android/EventSearchActivity;)Lcom/twitter/android/fr;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/fr;->a(Lcom/twitter/android/fr;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhb;

    iget-object v2, p0, Lcom/twitter/android/ft;->b:Landroid/net/Uri;

    iget-object v3, v0, Lhb;->c:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iput p1, v0, Lhb;->k:I

    iget-object v0, p0, Lcom/twitter/android/ft;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-static {v0}, Lcom/twitter/android/EventSearchActivity;->b(Lcom/twitter/android/EventSearchActivity;)Lcom/twitter/android/fr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/fr;->notifyDataSetChanged()V

    :cond_1
    return-void
.end method
