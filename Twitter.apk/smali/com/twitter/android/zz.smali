.class Lcom/twitter/android/zz;
.super Lcom/twitter/android/client/z;
.source "Twttr"


# instance fields
.field public final a:Z

.field public final b:I


# direct methods
.method public constructor <init>(Lcom/twitter/android/client/BaseFragmentActivity;)V
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    invoke-virtual {p0, v1}, Lcom/twitter/android/zz;->a(Z)V

    invoke-virtual {p1}, Lcom/twitter/android/client/BaseFragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "type"

    const/4 v4, -0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/twitter/android/zz;->b:I

    iget v3, p0, Lcom/twitter/android/zz;->b:I

    packed-switch v3, :pswitch_data_0

    :pswitch_0
    invoke-virtual {p0, v1}, Lcom/twitter/android/zz;->c(Z)V

    const-string/jumbo v3, "user_ids"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/zz;->a:Z

    :goto_1
    return-void

    :pswitch_1
    invoke-virtual {p0, v1}, Lcom/twitter/android/zz;->c(Z)V

    iput-boolean v1, p0, Lcom/twitter/android/zz;->a:Z

    goto :goto_1

    :pswitch_2
    iput-boolean v1, p0, Lcom/twitter/android/zz;->a:Z

    invoke-virtual {p0, v1}, Lcom/twitter/android/zz;->a(I)V

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0, v1}, Lcom/twitter/android/zz;->a(Z)V

    iput-boolean v0, p0, Lcom/twitter/android/zz;->a:Z

    goto :goto_1

    :pswitch_4
    iput-boolean v0, p0, Lcom/twitter/android/zz;->a:Z

    goto :goto_1

    :cond_0
    move v0, v1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
