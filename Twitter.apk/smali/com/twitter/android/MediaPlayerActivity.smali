.class public Lcom/twitter/android/MediaPlayerActivity;
.super Lcom/twitter/android/MediaTweetActivity;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/widget/bd;


# instance fields
.field protected a:Lcom/twitter/android/widget/MediaPlayerView;

.field private b:Landroid/widget/LinearLayout;

.field private c:Lco/vine/android/player/SdkVideoView;

.field private d:Landroid/widget/ProgressBar;

.field private e:Landroid/widget/ImageView;

.field private f:Lcom/twitter/library/widget/TweetView;

.field private g:I

.field private h:Lcom/twitter/android/ld;

.field private i:Lcom/twitter/library/util/m;

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Lcom/twitter/library/provider/Tweet;

.field private o:Lcom/twitter/library/scribe/ScribeAssociation;

.field private p:Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;

.field private q:Landroid/net/Uri;

.field private r:Landroid/view/animation/Animation;

.field private s:Landroid/view/animation/Animation;

.field private t:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/MediaTweetActivity;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->p:Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/MediaPlayerActivity;->t:Z

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/MediaPlayerActivity;)Lco/vine/android/player/SdkVideoView;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->c:Lco/vine/android/player/SdkVideoView;

    return-object v0
.end method

.method private a(Ljava/lang/String;)V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->c:Lco/vine/android/player/SdkVideoView;

    new-instance v1, Lcom/twitter/android/kw;

    invoke-direct {v1, p0}, Lcom/twitter/android/kw;-><init>(Lcom/twitter/android/MediaPlayerActivity;)V

    invoke-virtual {v0, v1}, Lco/vine/android/player/SdkVideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->c:Lco/vine/android/player/SdkVideoView;

    new-instance v1, Lcom/twitter/android/kx;

    invoke-direct {v1, p0}, Lcom/twitter/android/kx;-><init>(Lcom/twitter/android/MediaPlayerActivity;)V

    invoke-virtual {v0, v1}, Lco/vine/android/player/SdkVideoView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->c:Lco/vine/android/player/SdkVideoView;

    new-instance v1, Lcom/twitter/android/ky;

    invoke-direct {v1, p0}, Lcom/twitter/android/ky;-><init>(Lcom/twitter/android/MediaPlayerActivity;)V

    invoke-virtual {v0, v1}, Lco/vine/android/player/SdkVideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->c:Lco/vine/android/player/SdkVideoView;

    new-instance v1, Lcom/twitter/android/kz;

    invoke-direct {v1, p0}, Lcom/twitter/android/kz;-><init>(Lcom/twitter/android/MediaPlayerActivity;)V

    invoke-virtual {v0, v1}, Lco/vine/android/player/SdkVideoView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual {p0}, Lcom/twitter/android/MediaPlayerActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/android/client/c;->c:Lcom/twitter/library/util/as;

    new-instance v1, Lcom/twitter/library/util/at;

    invoke-direct {v1, p1}, Lcom/twitter/library/util/at;-><init>(Ljava/lang/String;)V

    new-instance v2, Lcom/twitter/android/la;

    invoke-direct {v2, p0, v1}, Lcom/twitter/android/la;-><init>(Lcom/twitter/android/MediaPlayerActivity;Lcom/twitter/library/util/at;)V

    invoke-virtual {v0, v2}, Lcom/twitter/library/util/as;->a(Lcom/twitter/library/util/au;)V

    invoke-virtual {p0}, Lcom/twitter/android/MediaPlayerActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3, v1, v4}, Lcom/twitter/library/util/as;->a(JLcom/twitter/library/util/at;Z)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/MediaPlayerActivity;->c:Lco/vine/android/player/SdkVideoView;

    invoke-virtual {v1, v0}, Lco/vine/android/player/SdkVideoView;->setVideoPath(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/android/MediaPlayerActivity;->f()V

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/twitter/android/MediaPlayerActivity;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->e:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/MediaPlayerActivity;)Lcom/twitter/library/util/m;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->i:Lcom/twitter/library/util/m;

    return-object v0
.end method

.method private g()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->b:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/twitter/android/lc;

    invoke-direct {v1, p0}, Lcom/twitter/android/lc;-><init>(Lcom/twitter/android/MediaPlayerActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a()Landroid/view/animation/Animation;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->r:Landroid/view/animation/Animation;

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    const v1, 0x7f0300b9    # com.twitter.android.R.layout.media_player

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(I)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/z;->a(Z)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/z;->c(Z)V

    return-object v0
.end method

.method public a(I)V
    .locals 6

    if-nez p1, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/MediaPlayerActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBar;->b()V

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/MediaPlayerActivity;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_2

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_0

    invoke-virtual {v3, p1}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/MediaPlayerActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBar;->c()V

    goto :goto_0

    :cond_2
    return-void
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 8

    new-instance v0, Lcom/twitter/android/ld;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p0, v1}, Lcom/twitter/android/ld;-><init>(Lcom/twitter/android/MediaPlayerActivity;Lcom/twitter/android/MediaTweetActivity;Lcom/twitter/android/kw;)V

    iput-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->h:Lcom/twitter/android/ld;

    invoke-virtual {p0}, Lcom/twitter/android/MediaPlayerActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v2

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->h:Lcom/twitter/android/ld;

    invoke-virtual {v2, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/j;)V

    invoke-virtual {p0}, Lcom/twitter/android/MediaPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v0, "aud"

    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/MediaPlayerActivity;->j:Z

    const-string/jumbo v0, "simple_controls"

    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/MediaPlayerActivity;->l:Z

    const-string/jumbo v0, "association"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/scribe/ScribeAssociation;

    iput-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->o:Lcom/twitter/library/scribe/ScribeAssociation;

    const-string/jumbo v0, "tweet"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/provider/Tweet;

    iput-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->n:Lcom/twitter/library/provider/Tweet;

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->n:Lcom/twitter/library/provider/Tweet;

    iget-wide v0, v0, Lcom/twitter/library/provider/Tweet;->u:J

    invoke-virtual {p0}, Lcom/twitter/android/MediaPlayerActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-static {v0, v1, v4, v5}, Lcom/twitter/library/provider/w;->b(JJ)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->q:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/twitter/android/MediaPlayerActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "media_forward"

    sget-boolean v4, Lcom/twitter/library/provider/t;->a:Z

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->n:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v0}, Lcom/twitter/library/provider/Tweet;->O()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->n:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v0}, Lcom/twitter/library/provider/Tweet;->Q()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->n:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v0}, Lcom/twitter/library/provider/Tweet;->P()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/MediaPlayerActivity;->m:Z

    const-string/jumbo v0, "player_url"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v0, "player_stream_urls"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    const/4 v0, 0x0

    if-eqz v5, :cond_9

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_9

    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/MediaDescriptor;

    iget-object v0, v0, Lcom/twitter/library/util/MediaDescriptor;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    move-object v1, v0

    :goto_1
    const v0, 0x7f090074    # com.twitter.android.R.id.progress

    invoke-virtual {p0, v0}, Lcom/twitter/android/MediaPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->d:Landroid/widget/ProgressBar;

    const v0, 0x7f0901d2    # com.twitter.android.R.id.surface

    invoke-virtual {p0, v0}, Lcom/twitter/android/MediaPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/MediaPlayerView;

    iput-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->a:Lcom/twitter/android/widget/MediaPlayerView;

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->a:Lcom/twitter/android/widget/MediaPlayerView;

    iget-boolean v6, p0, Lcom/twitter/android/MediaPlayerActivity;->l:Z

    invoke-virtual {v0, v6}, Lcom/twitter/android/widget/MediaPlayerView;->setUseSimplePlayPauseControls(Z)V

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->a:Lcom/twitter/android/widget/MediaPlayerView;

    iget-object v6, p0, Lcom/twitter/android/MediaPlayerActivity;->d:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v6}, Lcom/twitter/android/widget/MediaPlayerView;->setProgressBar(Landroid/widget/ProgressBar;)V

    const v0, 0x7f09020f    # com.twitter.android.R.id.media_tweet

    invoke-virtual {p0, v0}, Lcom/twitter/android/MediaPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/TweetView;

    iput-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->f:Lcom/twitter/library/widget/TweetView;

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->f:Lcom/twitter/library/widget/TweetView;

    iget-object v6, v2, Lcom/twitter/android/client/c;->a:Lcom/twitter/library/widget/ap;

    invoke-virtual {v0, v6}, Lcom/twitter/library/widget/TweetView;->setProvider(Lcom/twitter/library/widget/ap;)V

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->f:Lcom/twitter/library/widget/TweetView;

    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Lcom/twitter/library/widget/TweetView;->setHideInlineActions(Z)V

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->f:Lcom/twitter/library/widget/TweetView;

    iget-object v6, p0, Lcom/twitter/android/MediaPlayerActivity;->n:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v0, v6}, Lcom/twitter/library/widget/TweetView;->setTweet(Lcom/twitter/library/provider/Tweet;)V

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->f:Lcom/twitter/library/widget/TweetView;

    const/16 v6, 0x8

    invoke-virtual {v0, v6}, Lcom/twitter/library/widget/TweetView;->setVisibility(I)V

    const v0, 0x7f0901d4    # com.twitter.android.R.id.replay

    invoke-virtual {p0, v0}, Lcom/twitter/android/MediaPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    const v6, 0x7f0901d3    # com.twitter.android.R.id.replayFrame

    invoke-virtual {p0, v6}, Lcom/twitter/android/MediaPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iget-object v7, p0, Lcom/twitter/android/MediaPlayerActivity;->a:Lcom/twitter/android/widget/MediaPlayerView;

    invoke-virtual {v7, v0, v6}, Lcom/twitter/android/widget/MediaPlayerView;->a(Landroid/widget/ImageButton;Landroid/view/View;)V

    invoke-static {}, Lcom/twitter/library/util/Util;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    const v0, 0x7f0901d6    # com.twitter.android.R.id.vine_surface

    invoke-virtual {p0, v0}, Lcom/twitter/android/MediaPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lco/vine/android/player/SdkVideoView;

    iput-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->c:Lco/vine/android/player/SdkVideoView;

    invoke-static {v4}, Lcom/twitter/library/util/Util;->l(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/MediaPlayerActivity;->k:Z

    :goto_2
    const v0, 0x7f0901d5    # com.twitter.android.R.id.media_control

    invoke-virtual {p0, v0}, Lcom/twitter/android/MediaPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->b:Landroid/widget/LinearLayout;

    new-instance v0, Lcom/twitter/android/da;

    invoke-direct {v0, p0}, Lcom/twitter/android/da;-><init>(Lcom/twitter/android/db;)V

    const v4, 0x7f040007    # com.twitter.android.R.anim.fade_in

    invoke-static {p0, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v4

    iput-object v4, p0, Lcom/twitter/android/MediaPlayerActivity;->r:Landroid/view/animation/Animation;

    iget-object v4, p0, Lcom/twitter/android/MediaPlayerActivity;->r:Landroid/view/animation/Animation;

    invoke-virtual {v4, v0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v4, p0, Lcom/twitter/android/MediaPlayerActivity;->r:Landroid/view/animation/Animation;

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    iget-object v4, p0, Lcom/twitter/android/MediaPlayerActivity;->r:Landroid/view/animation/Animation;

    const-wide/16 v6, 0x96

    invoke-virtual {v4, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    const v4, 0x7f040008    # com.twitter.android.R.anim.fade_out

    invoke-static {p0, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v4

    iput-object v4, p0, Lcom/twitter/android/MediaPlayerActivity;->s:Landroid/view/animation/Animation;

    iget-object v4, p0, Lcom/twitter/android/MediaPlayerActivity;->s:Landroid/view/animation/Animation;

    invoke-virtual {v4, v0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->s:Landroid/view/animation/Animation;

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->s:Landroid/view/animation/Animation;

    const-wide/16 v6, 0x96

    invoke-virtual {v0, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    iget-boolean v0, p0, Lcom/twitter/android/MediaPlayerActivity;->k:Z

    if-eqz v0, :cond_7

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/MediaPlayerActivity;->a(Ljava/lang/String;)V

    if-nez p1, :cond_6

    new-instance v0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;

    invoke-direct {v0}, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v4, "association"

    iget-object v5, p0, Lcom/twitter/android/MediaPlayerActivity;->o:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v4, "page"

    const-string/jumbo v5, "tweet"

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    const-string/jumbo v4, "section"

    const-string/jumbo v5, ""

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    const-string/jumbo v4, "component"

    const-string/jumbo v5, "tweet"

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/twitter/android/MediaPlayerActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v4

    const v5, 0x7f0900e3    # com.twitter.android.R.id.fragment_container

    invoke-virtual {v4, v5, v0}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    invoke-virtual {v0, v1}, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->setArguments(Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->p:Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;

    :goto_3
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->b:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_4
    const-string/jumbo v0, "image_url"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    new-instance v1, Lcom/twitter/library/util/m;

    invoke-direct {v1, v0}, Lcom/twitter/library/util/m;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/twitter/android/MediaPlayerActivity;->i:Lcom/twitter/library/util/m;

    const v0, 0x7f090077    # com.twitter.android.R.id.image

    invoke-virtual {p0, v0}, Lcom/twitter/android/MediaPlayerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/twitter/android/MediaPlayerActivity;->i:Lcom/twitter/library/util/m;

    invoke-virtual {v2, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/util/m;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-boolean v1, p0, Lcom/twitter/android/MediaPlayerActivity;->j:Z

    if-eqz v1, :cond_8

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    :goto_5
    iput-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->e:Landroid/widget/ImageView;

    :cond_1
    if-eqz p1, :cond_2

    const-string/jumbo v0, "seek"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/MediaPlayerActivity;->g:I

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->q:Landroid/net/Uri;

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/twitter/android/MediaPlayerActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :cond_3
    invoke-direct {p0}, Lcom/twitter/android/MediaPlayerActivity;->g()V

    return-void

    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_5
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/MediaPlayerActivity;->k:Z

    goto/16 :goto_2

    :cond_6
    invoke-virtual {p0}, Lcom/twitter/android/MediaPlayerActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f0900e3    # com.twitter.android.R.id.fragment_container

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;

    iput-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->p:Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;

    goto :goto_3

    :cond_7
    const-string/jumbo v0, "video_index"

    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const-string/jumbo v1, "video_position"

    const/4 v4, 0x0

    invoke-virtual {v3, v1, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iget-object v4, p0, Lcom/twitter/android/MediaPlayerActivity;->a:Lcom/twitter/android/widget/MediaPlayerView;

    invoke-virtual {v4, v5, v0, v1}, Lcom/twitter/android/widget/MediaPlayerView;->a(Ljava/util/ArrayList;II)V

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->a:Lcom/twitter/android/widget/MediaPlayerView;

    const-string/jumbo v1, "is_looping"

    const/4 v4, 0x0

    invoke-virtual {v3, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/MediaPlayerView;->setIsLooping(Z)V

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->a:Lcom/twitter/android/widget/MediaPlayerView;

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/MediaPlayerView;->setMediaControllerListener(Lcom/twitter/android/widget/bd;)V

    goto/16 :goto_4

    :cond_8
    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    goto :goto_5

    :cond_9
    move-object v1, v0

    goto/16 :goto_1
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 2

    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/twitter/library/provider/Tweet;

    invoke-direct {v0, p2}, Lcom/twitter/library/provider/Tweet;-><init>(Landroid/database/Cursor;)V

    iput-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->n:Lcom/twitter/library/provider/Tweet;

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->p:Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->p:Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;

    iget-object v1, p0, Lcom/twitter/android/MediaPlayerActivity;->n:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v0, v1}, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->a(Lcom/twitter/library/provider/Tweet;)V

    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 2

    iget-boolean v0, p0, Lcom/twitter/android/MediaPlayerActivity;->t:Z

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->b:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/twitter/android/MediaPlayerActivity;->r:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    :goto_1
    iput-boolean p1, p0, Lcom/twitter/android/MediaPlayerActivity;->t:Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->b:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/twitter/android/MediaPlayerActivity;->s:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_1
.end method

.method protected a(Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/twitter/android/MediaTweetActivity;->a(Lcom/twitter/internal/android/widget/ToolBar;)Z

    move-result v3

    invoke-virtual {p0}, Lcom/twitter/android/MediaPlayerActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    iget-object v4, p0, Lcom/twitter/android/MediaPlayerActivity;->n:Lcom/twitter/library/provider/Tweet;

    iget-wide v4, v4, Lcom/twitter/library/provider/Tweet;->q:J

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const v4, 0x7f090071    # com.twitter.android.R.id.delete

    invoke-virtual {p1, v4}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v4

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->n:Lcom/twitter/library/provider/Tweet;

    iget-boolean v0, v0, Lcom/twitter/library/provider/Tweet;->r:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->p:Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {v4, v1}, Lhn;->b(Z)Lhn;

    return v3

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method protected a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/twitter/android/MediaTweetActivity;->a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/android/MediaPlayerActivity;->m:Z

    if-eqz v0, :cond_0

    const v0, 0x7f110015    # com.twitter.android.R.menu.media_player_menu

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    :cond_0
    const v0, 0x7f110008    # com.twitter.android.R.menu.delete

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lhn;)Z
    .locals 4

    const/4 v0, 0x1

    invoke-virtual {p1}, Lhn;->a()I

    move-result v1

    const v2, 0x7f090320    # com.twitter.android.R.id.tweet_video

    if-ne v1, v2, :cond_0

    invoke-static {p0}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Landroid/content/Context;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/MediaPlayerActivity;->n:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v2}, Lcom/twitter/library/provider/Tweet;->l()Lcom/twitter/library/api/TweetClassicCard;

    move-result-object v2

    iget-object v2, v2, Lcom/twitter/library/api/TweetClassicCard;->url:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Ljava/lang/String;[I)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/MediaPlayerActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Ljava/lang/String;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/twitter/android/composer/ComposerIntentWrapper;->b(Landroid/content/Context;)V

    :goto_0
    return v0

    :cond_0
    const v2, 0x7f090071    # com.twitter.android.R.id.delete

    if-ne v1, v2, :cond_1

    invoke-virtual {p0, v0}, Lcom/twitter/android/MediaPlayerActivity;->showDialog(I)V

    :cond_1
    invoke-super {p0, p1}, Lcom/twitter/android/MediaTweetActivity;->a(Lhn;)Z

    move-result v0

    goto :goto_0
.end method

.method public b()Landroid/view/animation/Animation;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->s:Landroid/view/animation/Animation;

    return-object v0
.end method

.method public b(I)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->b:Landroid/widget/LinearLayout;

    invoke-static {v0}, Lcom/twitter/library/util/Util;->b(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/twitter/android/MediaPlayerActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBar;->b()V

    return-void
.end method

.method public c(I)V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->d:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const v0, 0x7f0f0268    # com.twitter.android.R.string.media_player_error_default

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :pswitch_1
    const v0, 0x7f0f0269    # com.twitter.android.R.string.media_player_error_invalid

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0f0267    # com.twitter.android.R.string.media_player_error_connection

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :pswitch_3
    const v0, 0x7f0f0266    # com.twitter.android.R.string.media_error_audio_focus_rejected

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public d()V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/MediaPlayerActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBar;->c()V

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->b:Landroid/widget/LinearLayout;

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a(Landroid/view/View;)V

    return-void
.end method

.method public f()V
    .locals 3

    const/4 v2, 0x0

    const/16 v1, 0x8

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->d:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-boolean v0, p0, Lcom/twitter/android/MediaPlayerActivity;->j:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->e:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->e:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_0
    iget-boolean v0, p0, Lcom/twitter/android/MediaPlayerActivity;->k:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->a:Lcom/twitter/android/widget/MediaPlayerView;

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/MediaPlayerView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->c:Lco/vine/android/player/SdkVideoView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->c:Lco/vine/android/player/SdkVideoView;

    invoke-virtual {v0, v1}, Lco/vine/android/player/SdkVideoView;->setVisibility(I)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->a:Lcom/twitter/android/widget/MediaPlayerView;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/MediaPlayerView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->c:Lco/vine/android/player/SdkVideoView;

    invoke-virtual {v0, v2}, Lco/vine/android/player/SdkVideoView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->c:Lco/vine/android/player/SdkVideoView;

    invoke-virtual {v0}, Lco/vine/android/player/SdkVideoView;->start()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/MediaPlayerActivity;->t:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/twitter/android/MediaPlayerActivity;->a(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->n:Lcom/twitter/library/provider/Tweet;

    iget-wide v0, v0, Lcom/twitter/library/provider/Tweet;->C:J

    invoke-static {p0, v0, v1, p1}, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->a(Lcom/twitter/android/MediaTweetActivity;JI)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7

    const/4 v4, 0x0

    new-instance v0, Landroid/support/v4/content/CursorLoader;

    iget-object v2, p0, Lcom/twitter/android/MediaPlayerActivity;->q:Landroid/net/Uri;

    sget-object v3, Lcom/twitter/library/provider/Tweet;->b:[Ljava/lang/String;

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method protected onDestroy()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    invoke-super {p0}, Lcom/twitter/android/MediaTweetActivity;->onDestroy()V

    iget-boolean v0, p0, Lcom/twitter/android/MediaPlayerActivity;->k:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->a:Lcom/twitter/android/widget/MediaPlayerView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/MediaPlayerView;->f()V

    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->b:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/MediaPlayerActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/MediaPlayerActivity;->h:Lcom/twitter/android/ld;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/j;)V

    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/MediaPlayerActivity;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0

    return-void
.end method

.method protected onPause()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/MediaTweetActivity;->onPause()V

    iget-boolean v0, p0, Lcom/twitter/android/MediaPlayerActivity;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->c:Lco/vine/android/player/SdkVideoView;

    invoke-virtual {v0}, Lco/vine/android/player/SdkVideoView;->pause()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->a:Lcom/twitter/android/widget/MediaPlayerView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/MediaPlayerView;->pause()V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/MediaTweetActivity;->onResume()V

    iget v0, p0, Lcom/twitter/android/MediaPlayerActivity;->g:I

    if-lez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/MediaPlayerActivity;->k:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->c:Lco/vine/android/player/SdkVideoView;

    iget v1, p0, Lcom/twitter/android/MediaPlayerActivity;->g:I

    invoke-virtual {v0, v1}, Lco/vine/android/player/SdkVideoView;->setPlayingPosition(I)V

    :goto_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/MediaPlayerActivity;->g:I

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->a:Lcom/twitter/android/widget/MediaPlayerView;

    iget v1, p0, Lcom/twitter/android/MediaPlayerActivity;->g:I

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/MediaPlayerView;->setStartPosition(I)V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/MediaTweetActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-boolean v0, p0, Lcom/twitter/android/MediaPlayerActivity;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->c:Lco/vine/android/player/SdkVideoView;

    invoke-virtual {v0}, Lco/vine/android/player/SdkVideoView;->getCurrentPosition()I

    move-result v0

    :goto_0
    iput v0, p0, Lcom/twitter/android/MediaPlayerActivity;->g:I

    const-string/jumbo v1, "seek"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/MediaPlayerActivity;->a:Lcom/twitter/android/widget/MediaPlayerView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/MediaPlayerView;->getCurrentPosition()I

    move-result v0

    goto :goto_0
.end method

.method public onSearchRequested()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
