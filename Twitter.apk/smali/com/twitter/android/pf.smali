.class Lcom/twitter/android/pf;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/widget/ax;


# instance fields
.field final synthetic a:Lcom/twitter/android/PostActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/PostActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/pf;->a:Lcom/twitter/android/PostActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/net/Uri;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/pf;->a:Lcom/twitter/android/PostActivity;

    invoke-virtual {v0, p1}, Lcom/twitter/android/PostActivity;->d(Landroid/net/Uri;)V

    return-void
.end method

.method public b(Landroid/net/Uri;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/pf;->a:Lcom/twitter/android/PostActivity;

    iget-object v0, v0, Lcom/twitter/android/PostActivity;->y:Lcom/twitter/android/PhotoSelectHelper;

    iget-object v1, p0, Lcom/twitter/android/pf;->a:Lcom/twitter/android/PostActivity;

    invoke-static {v1}, Lcom/twitter/android/PostActivity;->E(Lcom/twitter/android/PostActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    sget-object v3, Lcom/twitter/android/PhotoSelectHelper$AddMediaMode;->b:Lcom/twitter/android/PhotoSelectHelper$AddMediaMode;

    invoke-virtual {v0, p1, v1, v2, v3}, Lcom/twitter/android/PhotoSelectHelper;->a(Landroid/net/Uri;JLcom/twitter/android/PhotoSelectHelper$AddMediaMode;)V

    return-void
.end method
