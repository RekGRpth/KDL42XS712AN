.class public Lcom/twitter/android/ProfileActivity;
.super Lcom/twitter/android/UserQueryActivity;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Z

.field private e:Landroid/widget/TextView;

.field private f:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/UserQueryActivity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;JLjava/lang/String;Lcom/twitter/library/api/PromotedContent;Lcom/twitter/library/scribe/ScribeAssociation;ILcom/twitter/library/api/PromotedEvent;Z)Landroid/content/Intent;
    .locals 2

    new-instance v1, Landroid/content/Intent;

    if-eqz p8, :cond_3

    const-class v0, Lcom/twitter/android/RootProfileActivity;

    :goto_0
    invoke-direct {v1, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v0, "user_id"

    invoke-virtual {v1, v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "association"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "screen_name"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, -0x1

    if-eq p6, v1, :cond_0

    const-string/jumbo v1, "friendship"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_0
    if-eqz p4, :cond_2

    if-eqz p7, :cond_1

    invoke-static {p0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1, p7, p4}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/library/api/PromotedContent;)V

    :cond_1
    const-string/jumbo v1, "pc"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    :cond_2
    return-object v0

    :cond_3
    const-class v0, Lcom/twitter/android/ProfileActivity;

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;JLjava/lang/String;Lcom/twitter/library/api/PromotedContent;Lcom/twitter/library/scribe/ScribeAssociation;Z)V
    .locals 9

    const/4 v6, -0x1

    sget-object v7, Lcom/twitter/library/api/PromotedEvent;->c:Lcom/twitter/library/api/PromotedEvent;

    move-object v0, p0

    move-wide v1, p1

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move v8, p6

    invoke-static/range {v0 .. v8}, Lcom/twitter/android/ProfileActivity;->a(Landroid/content/Context;JLjava/lang/String;Lcom/twitter/library/api/PromotedContent;Lcom/twitter/library/scribe/ScribeAssociation;ILcom/twitter/library/api/PromotedEvent;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/ProfileActivity;Lcom/twitter/library/api/TwitterUser;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/twitter/android/UserQueryActivity;->a(Lcom/twitter/library/api/TwitterUser;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 2

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(Z)V

    return-object v0
.end method

.method protected a(Landroid/content/Intent;Lcom/twitter/library/client/e;)Lcom/twitter/android/iu;
    .locals 8

    const/4 v1, 0x0

    invoke-static {p0}, Lcom/twitter/android/util/AppMetrics;->b(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v0

    new-instance v2, Lcom/twitter/android/ProfileFragment;

    invoke-direct {v2}, Lcom/twitter/android/ProfileFragment;-><init>()V

    invoke-virtual {v2, p1}, Lcom/twitter/android/ProfileFragment;->a(Landroid/content/Intent;)Lcom/twitter/android/client/BaseListFragment;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/twitter/android/client/BaseListFragment;->j(Z)Lcom/twitter/android/client/BaseListFragment;

    invoke-virtual {v2}, Lcom/twitter/android/ProfileFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    iget-wide v4, p0, Lcom/twitter/android/ProfileActivity;->b:J

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/twitter/android/ProfileActivity;->c:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->f:Z

    const-string/jumbo v0, "type"

    const-string/jumbo v4, "type"

    invoke-virtual {p1, v4, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "is_me"

    iget-boolean v4, p0, Lcom/twitter/android/ProfileActivity;->f:Z

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    new-instance v0, Lcom/twitter/android/iu;

    invoke-direct {v0, v2}, Lcom/twitter/android/iu;-><init>(Lcom/twitter/android/client/BaseListFragment;)V

    iput-boolean v1, v0, Lcom/twitter/android/iu;->c:Z

    return-object v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method protected a(Landroid/content/Intent;)Ljava/lang/CharSequence;
    .locals 1

    const v0, 0x7f0f0330    # com.twitter.android.R.string.profile_title

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 9

    const/4 v4, 0x1

    const/4 v8, 0x0

    const-wide/16 v6, 0x0

    const/4 v5, 0x0

    invoke-super {p0, p1, p2}, Lcom/twitter/android/UserQueryActivity;->a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V

    iget-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->f:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/twitter/android/qc;

    invoke-direct {v0, p0, v5}, Lcom/twitter/android/qc;-><init>(Lcom/twitter/android/ProfileActivity;Lcom/twitter/android/qb;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileActivity;->a(Lcom/twitter/library/client/z;)V

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->c:Ljava/lang/String;

    if-nez v1, :cond_2

    iget-wide v1, p0, Lcom/twitter/android/ProfileActivity;->b:J

    cmp-long v1, v1, v6

    if-nez v1, :cond_2

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_2

    const-string/jumbo v2, "com.twitter.android.action.USER_SHOW"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string/jumbo v2, "com.twitter.android.action.USER_SHOW_TYPEAHEAD"

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_1
    invoke-virtual {v1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/ProfileActivity;->c:Ljava/lang/String;

    :cond_2
    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->c:Ljava/lang/String;

    if-nez v1, :cond_4

    iget-wide v1, p0, Lcom/twitter/android/ProfileActivity;->b:J

    cmp-long v1, v1, v6

    if-nez v1, :cond_4

    const v0, 0x7f0f0531    # com.twitter.android.R.string.users_fetch_error

    invoke-static {p0, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->finish()V

    :goto_0
    return-void

    :cond_3
    const-string/jumbo v2, "twitter"

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-wide v2, p0, Lcom/twitter/android/ProfileActivity;->b:J

    cmp-long v2, v2, v6

    if-nez v2, :cond_2

    new-instance v0, Lcom/twitter/android/qd;

    invoke-direct {v0, p0, v5}, Lcom/twitter/android/qd;-><init>(Lcom/twitter/android/ProfileActivity;Lcom/twitter/android/qb;)V

    new-array v2, v4, [Landroid/net/Uri;

    aput-object v1, v2, v8

    invoke-virtual {v0, v2}, Lcom/twitter/android/qd;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    :cond_4
    iget-wide v1, p0, Lcom/twitter/android/ProfileActivity;->b:J

    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-eqz v1, :cond_6

    const-string/jumbo v1, ""

    invoke-virtual {p0, v1}, Lcom/twitter/android/ProfileActivity;->c(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->L()Lcom/twitter/android/client/bn;

    move-result-object v1

    const-string/jumbo v2, "profile"

    invoke-virtual {v1, v2}, Lcom/twitter/android/client/bn;->a(Ljava/lang/String;)Lcom/twitter/android/client/bn;

    :goto_1
    const-string/jumbo v1, "expanded_search"

    invoke-virtual {v0, v1, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->a:Z

    iget-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->a:Z

    if-eqz v0, :cond_5

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030130    # com.twitter.android.R.layout.search_tool_bar

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->setCustomView(Landroid/view/View;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "@"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/ProfileActivity;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->L()Lcom/twitter/android/client/bn;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/twitter/android/client/bn;->a(Ljava/lang/CharSequence;)V

    iput-object v0, p0, Lcom/twitter/android/ProfileActivity;->e:Landroid/widget/TextView;

    :cond_5
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->g()V

    goto/16 :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->L()Lcom/twitter/android/client/bn;

    move-result-object v1

    const-string/jumbo v2, "me"

    invoke-virtual {v1, v2}, Lcom/twitter/android/client/bn;->a(Ljava/lang/String;)Lcom/twitter/android/client/bn;

    goto :goto_1
.end method

.method protected a(Lcom/twitter/library/api/TwitterUser;)V
    .locals 4

    invoke-super {p0, p1}, Lcom/twitter/android/UserQueryActivity;->a(Lcom/twitter/library/api/TwitterUser;)V

    iget-wide v0, p0, Lcom/twitter/android/ProfileActivity;->b:J

    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    const v0, 0x7f0f04d8    # com.twitter.android.R.string.tweet_to

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p1, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/ProfileActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileActivity;->c(Ljava/lang/String;)V

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f0900e3    # com.twitter.android.R.id.fragment_container

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ProfileFragment;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/library/api/TwitterUser;)V

    :cond_0
    return-void

    :cond_1
    const v0, 0x7f0f00c0    # com.twitter.android.R.string.composer_hint

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileActivity;->c(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected a(Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/twitter/android/UserQueryActivity;->a(Lcom/twitter/internal/android/widget/ToolBar;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/ProfileActivity;->a:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->setDisplayShowTitleEnabled(Z)V

    const v0, 0x7f090309    # com.twitter.android.R.id.toolbar_search

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v0

    iget-boolean v4, p0, Lcom/twitter/android/ProfileActivity;->a:Z

    if-nez v4, :cond_2

    :goto_1
    invoke-virtual {v0, v1}, Lhn;->b(Z)Lhn;

    :cond_0
    return v3

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public a(Lhn;)Z
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "override_home"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lhn;->a()I

    move-result v0

    const v1, 0x7f090045    # com.twitter.android.R.id.home

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->onBackPressed()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/android/UserQueryActivity;->a(Lhn;)Z

    move-result v0

    goto :goto_0
.end method

.method protected c_()Ljava/lang/String;
    .locals 4

    iget-wide v0, p0, Lcom/twitter/android/ProfileActivity;->b:J

    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v1, 0x40

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lcom/twitter/android/UserQueryActivity;->c_()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 6

    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f0900e3    # com.twitter.android.R.id.fragment_container

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ProfileFragment;

    const/4 v1, -0x1

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v3, "user_id"

    iget-wide v4, p0, Lcom/twitter/android/ProfileActivity;->b:J

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "friendship"

    invoke-virtual {v0}, Lcom/twitter/android/ProfileFragment;->s()I

    move-result v0

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/ProfileActivity;->setResult(ILandroid/content/Intent;)V

    invoke-super {p0}, Lcom/twitter/android/UserQueryActivity;->onBackPressed()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f090212    # com.twitter.android.R.id.query

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->J()Z

    invoke-virtual {p0}, Lcom/twitter/android/ProfileActivity;->L()Lcom/twitter/android/client/bn;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ProfileActivity;->e:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/bn;->a(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method
