.class Lcom/twitter/android/ak;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/internal/android/service/m;


# instance fields
.field final synthetic a:Lcom/twitter/android/AuthenticatorActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/AuthenticatorActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/ak;->a:Lcom/twitter/android/AuthenticatorActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/internal/android/service/a;)V
    .locals 0

    check-cast p1, Lcom/twitter/library/api/account/i;

    invoke-virtual {p0, p1}, Lcom/twitter/android/ak;->a(Lcom/twitter/library/api/account/i;)V

    return-void
.end method

.method public a(Lcom/twitter/library/api/account/i;)V
    .locals 5

    const v4, 0x7f0f003d    # com.twitter.android.R.string.authenticator_activity_loginfail_text_pwonly

    const v3, 0x7f090091    # com.twitter.android.R.id.message

    const/4 v2, 0x1

    invoke-virtual {p1}, Lcom/twitter/library/api/account/i;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ak;->a:Lcom/twitter/android/AuthenticatorActivity;

    invoke-virtual {v1, v2}, Lcom/twitter/android/AuthenticatorActivity;->removeDialog(I)V

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/twitter/library/api/account/i;->g()Lcom/twitter/library/network/LoginResponse;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/twitter/library/network/LoginResponse;->a:Lcom/twitter/library/network/OAuthToken;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/ak;->a:Lcom/twitter/android/AuthenticatorActivity;

    iget-object v1, v1, Lcom/twitter/android/AuthenticatorActivity;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/ak;->a:Lcom/twitter/android/AuthenticatorActivity;

    iget-object v0, v0, Lcom/twitter/library/network/LoginResponse;->a:Lcom/twitter/library/network/OAuthToken;

    invoke-virtual {v1, v0}, Lcom/twitter/android/AuthenticatorActivity;->a(Lcom/twitter/library/network/OAuthToken;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/ak;->a:Lcom/twitter/android/AuthenticatorActivity;

    iget-object v0, v0, Lcom/twitter/library/network/LoginResponse;->a:Lcom/twitter/library/network/OAuthToken;

    invoke-virtual {v1, v0, v2}, Lcom/twitter/android/AuthenticatorActivity;->a(Lcom/twitter/library/network/OAuthToken;Z)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/ak;->a:Lcom/twitter/android/AuthenticatorActivity;

    invoke-virtual {v0, v3}, Lcom/twitter/android/AuthenticatorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/ak;->a:Lcom/twitter/android/AuthenticatorActivity;

    invoke-virtual {v1, v4}, Lcom/twitter/android/AuthenticatorActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/ak;->a:Lcom/twitter/android/AuthenticatorActivity;

    invoke-virtual {v0, v3}, Lcom/twitter/android/AuthenticatorActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/ak;->a:Lcom/twitter/android/AuthenticatorActivity;

    invoke-virtual {v1, v4}, Lcom/twitter/android/AuthenticatorActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
