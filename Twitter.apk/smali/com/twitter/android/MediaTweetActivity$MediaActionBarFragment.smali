.class public Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;
.super Landroid/support/v4/app/Fragment;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/twitter/android/ru;


# instance fields
.field private a:Lcom/twitter/android/client/c;

.field private b:Landroid/widget/LinearLayout;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/ImageButton;

.field private e:Landroid/widget/TextView;

.field private f:Landroid/widget/ImageButton;

.field private g:Landroid/widget/TextView;

.field private h:Lcom/twitter/library/scribe/ScribeAssociation;

.field private i:Z

.field private j:Lcom/twitter/library/client/aa;

.field private k:Lcom/twitter/library/provider/Tweet;

.field private l:Ljava/lang/CharSequence;

.field private m:Ljava/lang/CharSequence;

.field private n:Ljava/lang/CharSequence;

.field private o:Lcom/twitter/library/scribe/ScribeItem;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method protected static a(Lcom/twitter/android/MediaTweetActivity;JI)Landroid/app/Dialog;
    .locals 4

    const/4 v0, 0x0

    packed-switch p3, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v1, Lcom/twitter/android/ls;

    invoke-direct {v1, p0, p1, p2}, Lcom/twitter/android/ls;-><init>(Lcom/twitter/android/MediaTweetActivity;J)V

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0f04df    # com.twitter.android.R.string.tweets_delete_status

    invoke-virtual {p0, v3}, Lcom/twitter/android/MediaTweetActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0f04de    # com.twitter.android.R.string.tweets_delete_question

    invoke-virtual {p0, v3}, Lcom/twitter/android/MediaTweetActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0f0571    # com.twitter.android.R.string.yes

    invoke-virtual {p0, v3}, Lcom/twitter/android/MediaTweetActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0f029d    # com.twitter.android.R.string.no

    invoke-virtual {p0, v2}, Lcom/twitter/android/MediaTweetActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0f038a    # com.twitter.android.R.string.saving

    invoke-virtual {p0, v1}, Lcom/twitter/android/MediaTweetActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Ljava/lang/String;Lcom/twitter/library/provider/Tweet;)V
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->l:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    iget-object v1, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->m:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->n:Ljava/lang/CharSequence;

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "tweet"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    aput-object p1, v0, v1

    invoke-static {v0}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->a:Lcom/twitter/android/client/c;

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v3, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->j:Lcom/twitter/library/client/aa;

    invoke-virtual {v3}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v3, v6, [Ljava/lang/String;

    aput-object v0, v3, v5

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->h:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v7, p2, v2, v7}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->h:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->o:Lcom/twitter/library/scribe/ScribeItem;

    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method

.method private c(Lcom/twitter/library/provider/Tweet;)V
    .locals 9

    const v1, 0x7f0b007e    # com.twitter.android.R.color.white

    invoke-virtual {p0}, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v0, p1, Lcom/twitter/library/provider/Tweet;->ag:I

    iget v3, p1, Lcom/twitter/library/provider/Tweet;->ae:I

    iget v4, p1, Lcom/twitter/library/provider/Tweet;->V:I

    iget-object v5, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->j:Lcom/twitter/library/client/aa;

    invoke-virtual {v5}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v5

    invoke-virtual {v5}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v5

    iget-object v7, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->a:Lcom/twitter/android/client/c;

    invoke-virtual {v7}, Lcom/twitter/android/client/c;->T()Z

    move-result v7

    iget-object v8, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->c:Landroid/widget/TextView;

    if-eqz v7, :cond_1

    if-lez v0, :cond_1

    invoke-static {v2, v0}, Lcom/twitter/library/util/Util;->a(Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v8, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v7, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->e:Landroid/widget/TextView;

    if-lez v3, :cond_2

    invoke-static {v2, v3}, Lcom/twitter/library/util/Util;->a(Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->e:Landroid/widget/TextView;

    iget-boolean v0, p1, Lcom/twitter/library/provider/Tweet;->l:Z

    if-eqz v0, :cond_3

    const v0, 0x7f0b0066    # com.twitter.android.R.color.orange

    :goto_2
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v3, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->g:Landroid/widget/TextView;

    if-lez v4, :cond_4

    invoke-static {v2, v4}, Lcom/twitter/library/util/Util;->a(Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->g:Landroid/widget/TextView;

    iget-boolean v3, p1, Lcom/twitter/library/provider/Tweet;->r:Z

    if-eqz v3, :cond_0

    invoke-virtual {p1, v5, v6}, Lcom/twitter/library/provider/Tweet;->a(J)Z

    move-result v3

    if-eqz v3, :cond_0

    const v1, 0x7f0b0058    # com.twitter.android.R.color.green

    :cond_0
    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    return-void

    :cond_1
    const-string/jumbo v0, ""

    goto :goto_0

    :cond_2
    const-string/jumbo v0, ""

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    :cond_4
    const-string/jumbo v0, ""

    goto :goto_3
.end method


# virtual methods
.method public a(IJLcom/twitter/library/provider/ParcelableTweet;Z)V
    .locals 2

    const-string/jumbo v0, "quote"

    iget-object v1, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->k:Lcom/twitter/library/provider/Tweet;

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->a(Ljava/lang/String;Lcom/twitter/library/provider/Tweet;)V

    return-void
.end method

.method public a(IJLcom/twitter/library/provider/ParcelableTweet;ZLjava/lang/String;)V
    .locals 3

    if-eqz p5, :cond_1

    const-string/jumbo v0, "unretweet"

    iget-object v1, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->k:Lcom/twitter/library/provider/Tweet;

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->a(Ljava/lang/String;Lcom/twitter/library/provider/Tweet;)V

    iget-object v0, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->f:Landroid/widget/ImageButton;

    const v1, 0x7f0201b7    # com.twitter.android.R.drawable.ic_gallery_action_rt_off

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->k:Lcom/twitter/library/provider/Tweet;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/twitter/library/provider/Tweet;->r:Z

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/MediaTweetActivity;

    invoke-virtual {v0}, Lcom/twitter/android/MediaTweetActivity;->c()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v1, "rt"

    iget-object v2, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->k:Lcom/twitter/library/provider/Tweet;

    iget-boolean v2, v2, Lcom/twitter/library/provider/Tweet;->r:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_0
    return-void

    :cond_1
    const-string/jumbo v0, "retweet"

    iget-object v1, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->k:Lcom/twitter/library/provider/Tweet;

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->a(Ljava/lang/String;Lcom/twitter/library/provider/Tweet;)V

    iget-object v0, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->f:Landroid/widget/ImageButton;

    const v1, 0x7f0201ba    # com.twitter.android.R.drawable.ic_gallery_action_rt_on

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->k:Lcom/twitter/library/provider/Tweet;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/twitter/library/provider/Tweet;->r:Z

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/provider/Tweet;)V
    .locals 13

    iput-object p1, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->k:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {p0}, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v0, 0x5

    new-array v12, v0, [I

    fill-array-data v12, :array_0

    iget-object v0, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->j:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->b:Landroid/widget/LinearLayout;

    const v4, 0x7f0201ba    # com.twitter.android.R.drawable.ic_gallery_action_rt_on

    const v5, 0x7f0201b7    # com.twitter.android.R.drawable.ic_gallery_action_rt_off

    const v3, 0x7f0f007d    # com.twitter.android.R.string.button_status_retweeted

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const v3, 0x7f0f005e    # com.twitter.android.R.string.button_action_retweet

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    const v8, 0x7f0201b1    # com.twitter.android.R.drawable.ic_gallery_action_fave_on

    const v9, 0x7f0201ae    # com.twitter.android.R.drawable.ic_gallery_action_fave_off

    const v3, 0x7f0f007c    # com.twitter.android.R.string.button_status_favorited

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    const v3, 0x7f0f0058    # com.twitter.android.R.string.button_action_fave

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    move-object v1, p1

    move-object v3, p0

    invoke-static/range {v0 .. v12}, Lcom/twitter/android/widget/dn;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/provider/Tweet;Landroid/view/ViewGroup;Landroid/view/View$OnClickListener;IILjava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;[I)V

    invoke-direct {p0, p1}, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->c(Lcom/twitter/library/provider/Tweet;)V

    :cond_0
    return-void

    nop

    :array_0
    .array-data 4
        0x7f09006d    # com.twitter.android.R.id.reply
        0x7f09006e    # com.twitter.android.R.id.retweet
        0x7f09006f    # com.twitter.android.R.id.favorite
        0x7f090070    # com.twitter.android.R.id.share
        0x7f0901a3    # com.twitter.android.R.id.dismiss
    .end array-data
.end method

.method public b(IJLcom/twitter/library/provider/ParcelableTweet;Z)V
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->a:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->j:Lcom/twitter/library/client/aa;

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->l:Ljava/lang/CharSequence;

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->m:Ljava/lang/CharSequence;

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string/jumbo v5, "retweet_dialog::dismiss"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    return-void
.end method

.method public b(Lcom/twitter/library/provider/Tweet;)V
    .locals 8

    iget-object v0, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->a:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->j:Lcom/twitter/library/client/aa;

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    move-object v7, v2

    check-cast v7, Lcom/twitter/android/MediaTweetActivity;

    iget-boolean v2, p1, Lcom/twitter/library/provider/Tweet;->l:Z

    if-eqz v2, :cond_1

    iget-wide v2, p1, Lcom/twitter/library/provider/Tweet;->o:J

    iget-object v4, p1, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/Session;JLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    iput-boolean v1, p1, Lcom/twitter/library/provider/Tweet;->l:Z

    iget-object v1, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->d:Landroid/widget/ImageButton;

    const v2, 0x7f0201ae    # com.twitter.android.R.drawable.ic_gallery_action_fave_off

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    const-string/jumbo v1, "unfavorite"

    invoke-direct {p0, v1, p1}, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->a(Ljava/lang/String;Lcom/twitter/library/provider/Tweet;)V

    move-object v1, v0

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/MediaTweetActivity;

    invoke-virtual {v0}, Lcom/twitter/android/MediaTweetActivity;->c()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v2, "fav"

    iget-boolean v3, p1, Lcom/twitter/library/provider/Tweet;->l:Z

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_0
    invoke-static {v7, v1}, Lcom/twitter/android/MediaTweetActivity;->e(Lcom/twitter/android/MediaTweetActivity;Ljava/lang/String;)V

    return-void

    :cond_1
    const v2, 0x7f0f012b    # com.twitter.android.R.string.dt2f_dialog_text

    invoke-static {v7, v2}, Ljy;->a(Landroid/content/Context;I)V

    iget-wide v2, p1, Lcom/twitter/library/provider/Tweet;->o:J

    iget-wide v4, p1, Lcom/twitter/library/provider/Tweet;->C:J

    iget-object v6, p1, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;JJLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, p1, Lcom/twitter/library/provider/Tweet;->l:Z

    iget-object v1, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->d:Landroid/widget/ImageButton;

    const v2, 0x7f0201b1    # com.twitter.android.R.drawable.ic_gallery_action_fave_on

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    const-string/jumbo v1, "favorite"

    invoke-direct {p0, v1, p1}, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->a(Ljava/lang/String;Lcom/twitter/library/provider/Tweet;)V

    move-object v1, v0

    goto :goto_0
.end method

.method public c(IJLcom/twitter/library/provider/ParcelableTweet;Z)V
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->a:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->j:Lcom/twitter/library/client/aa;

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->l:Ljava/lang/CharSequence;

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->m:Ljava/lang/CharSequence;

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string/jumbo v5, "retweet_dialog::impression"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/MediaTweetActivity;

    invoke-static {v0}, Lcom/twitter/android/MediaTweetActivity;->a(Lcom/twitter/android/MediaTweetActivity;)Lcom/twitter/android/client/c;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->a:Lcom/twitter/android/client/c;

    invoke-static {v0}, Lcom/twitter/android/MediaTweetActivity;->b(Lcom/twitter/android/MediaTweetActivity;)Lcom/twitter/library/client/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->j:Lcom/twitter/library/client/aa;

    if-nez p1, :cond_4

    invoke-virtual {p0}, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string/jumbo v0, "gallery"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->i:Z

    const-string/jumbo v0, "page"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->l:Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->l:Ljava/lang/CharSequence;

    if-nez v0, :cond_1

    const-string/jumbo v0, ""

    :goto_0
    iput-object v0, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->l:Ljava/lang/CharSequence;

    const-string/jumbo v0, "section"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->m:Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->m:Ljava/lang/CharSequence;

    if-nez v0, :cond_2

    const-string/jumbo v0, ""

    :goto_1
    iput-object v0, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->m:Ljava/lang/CharSequence;

    const-string/jumbo v0, "component"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->n:Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->n:Ljava/lang/CharSequence;

    if-nez v0, :cond_3

    const-string/jumbo v0, ""

    :goto_2
    iput-object v0, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->n:Ljava/lang/CharSequence;

    const-string/jumbo v0, "association"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/scribe/ScribeAssociation;

    iput-object v0, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->h:Lcom/twitter/library/scribe/ScribeAssociation;

    const-string/jumbo v0, "item"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/scribe/ScribeItem;

    iput-object v0, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->o:Lcom/twitter/library/scribe/ScribeItem;

    :cond_0
    :goto_3
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->l:Ljava/lang/CharSequence;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->m:Ljava/lang/CharSequence;

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->n:Ljava/lang/CharSequence;

    goto :goto_2

    :cond_4
    const-string/jumbo v0, "gallery"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->i:Z

    const-string/jumbo v0, "page"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->l:Ljava/lang/CharSequence;

    const-string/jumbo v0, "section"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->m:Ljava/lang/CharSequence;

    const-string/jumbo v0, "component"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->n:Ljava/lang/CharSequence;

    const-string/jumbo v0, "association"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/scribe/ScribeAssociation;

    iput-object v0, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->h:Lcom/twitter/library/scribe/ScribeAssociation;

    const-string/jumbo v0, "item"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/scribe/ScribeItem;

    iput-object v0, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->o:Lcom/twitter/library/scribe/ScribeItem;

    goto :goto_3
.end method

.method public onClick(Landroid/view/View;)V
    .locals 13

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->k:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {p0}, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    check-cast v5, Lcom/twitter/android/MediaTweetActivity;

    iget-object v3, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->j:Lcom/twitter/library/client/aa;

    invoke-virtual {v3}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v3

    const v4, 0x7f09006d    # com.twitter.android.R.id.reply

    if-ne v2, v4, :cond_1

    sget-object v0, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;->c:Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    invoke-static {v5, v0}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Landroid/content/Context;Lcom/twitter/android/composer/ComposerIntentWrapper$Action;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Lcom/twitter/library/provider/ParcelableTweet;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v0

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Ljava/lang/String;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v0

    const/4 v2, 0x4

    invoke-virtual {v0, v5, v2}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Landroid/app/Activity;I)V

    const-string/jumbo v0, "reply"

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->a(Ljava/lang/String;Lcom/twitter/library/provider/Tweet;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v3, 0x7f09006f    # com.twitter.android.R.id.favorite

    if-ne v2, v3, :cond_2

    invoke-virtual {p0, v1}, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->b(Lcom/twitter/library/provider/Tweet;)V

    goto :goto_0

    :cond_2
    const v3, 0x7f09006e    # com.twitter.android.R.id.retweet

    if-ne v2, v3, :cond_3

    move v2, v0

    move-object v3, p0

    move-object v4, p0

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/rs;->a(ILcom/twitter/library/provider/Tweet;ZLandroid/support/v4/app/Fragment;Lcom/twitter/android/ru;Landroid/support/v4/app/FragmentActivity;)V

    goto :goto_0

    :cond_3
    const v0, 0x7f090070    # com.twitter.android.R.id.share

    if-ne v2, v0, :cond_0

    invoke-virtual {v1}, Lcom/twitter/library/provider/Tweet;->j()Ljava/lang/String;

    move-result-object v6

    iget-object v7, v1, Lcom/twitter/library/provider/Tweet;->p:Ljava/lang/String;

    iget-object v8, v1, Lcom/twitter/library/provider/Tweet;->d:Ljava/lang/String;

    iget-wide v9, v1, Lcom/twitter/library/provider/Tweet;->h:J

    iget-wide v11, v1, Lcom/twitter/library/provider/Tweet;->o:J

    invoke-static/range {v5 .. v12}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V

    const-string/jumbo v0, "share"

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->a(Ljava/lang/String;Lcom/twitter/library/provider/Tweet;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    const v0, 0x7f0300b5    # com.twitter.android.R.layout.media_action_bar

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "gallery"

    iget-boolean v1, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->i:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "page"

    iget-object v1, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->l:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    const-string/jumbo v0, "section"

    iget-object v1, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->m:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    const-string/jumbo v0, "component"

    iget-object v1, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->n:Ljava/lang/CharSequence;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    const-string/jumbo v0, "association"

    iget-object v1, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->h:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v0, "item"

    iget-object v1, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->o:Lcom/twitter/library/scribe/ScribeItem;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    const v0, 0x7f09006c    # com.twitter.android.R.id.actionbar

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->b:Landroid/widget/LinearLayout;

    const v0, 0x7f0901ca    # com.twitter.android.R.id.reply_label

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->c:Landroid/widget/TextView;

    const v0, 0x7f09006f    # com.twitter.android.R.id.favorite

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->d:Landroid/widget/ImageButton;

    const v0, 0x7f0901cc    # com.twitter.android.R.id.favorite_label

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->e:Landroid/widget/TextView;

    const v0, 0x7f09006e    # com.twitter.android.R.id.retweet

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->f:Landroid/widget/ImageButton;

    const v0, 0x7f0901cb    # com.twitter.android.R.id.retweet_label

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->g:Landroid/widget/TextView;

    return-void
.end method
