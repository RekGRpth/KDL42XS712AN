.class Lcom/twitter/android/cn;
.super Lcom/twitter/library/client/j;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/ChangeScreenNameActivity;


# direct methods
.method private constructor <init>(Lcom/twitter/android/ChangeScreenNameActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/cn;->a:Lcom/twitter/android/ChangeScreenNameActivity;

    invoke-direct {p0}, Lcom/twitter/library/client/j;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/ChangeScreenNameActivity;Lcom/twitter/android/cm;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/cn;-><init>(Lcom/twitter/android/ChangeScreenNameActivity;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;I)V
    .locals 3

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_0

    const v0, 0x7f0f0394    # com.twitter.android.R.string.screen_name_change_success

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/cn;->a:Lcom/twitter/android/ChangeScreenNameActivity;

    invoke-virtual {v1}, Lcom/twitter/android/ChangeScreenNameActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void

    :cond_0
    const v0, 0x7f0f0393    # com.twitter.android.R.string.screen_name_change_failure

    goto :goto_0
.end method
