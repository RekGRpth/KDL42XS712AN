.class Lcom/twitter/android/fd;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:Lcom/twitter/internal/android/widget/GroupedRowView;

.field public final b:Ljava/util/LinkedHashSet;

.field public final c:Lcom/twitter/internal/android/widget/GroupedRowView;

.field public final d:Lcom/twitter/internal/android/widget/GroupedRowView;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/widget/ListView;IZZ)V
    .locals 10

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v8, 0x0

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v9, Ljava/util/LinkedHashSet;

    invoke-direct {v9}, Ljava/util/LinkedHashSet;-><init>()V

    const v2, 0x7f0f04c5    # com.twitter.android.R.string.trends_title_worldwide

    const v5, 0x7f0202b4    # com.twitter.android.R.drawable.icn_trending_default

    move-object v0, p1

    move-object v1, p2

    move v4, p3

    invoke-static/range {v0 .. v6}, Lcom/twitter/android/fd;->a(Landroid/content/Context;Landroid/widget/ListView;ILjava/lang/String;III)Lcom/twitter/internal/android/widget/GroupedRowView;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/fd;->a:Lcom/twitter/internal/android/widget/GroupedRowView;

    move v0, v8

    :goto_0
    if-ge v0, v6, :cond_0

    invoke-static {p1, p2, v8}, Lcom/twitter/android/fd;->a(Landroid/content/Context;Landroid/widget/ListView;I)Lcom/twitter/internal/android/widget/GroupedRowView;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    if-nez p4, :cond_1

    if-eqz p5, :cond_2

    :cond_1
    :goto_1
    invoke-static {p1, p2, v6}, Lcom/twitter/android/fd;->a(Landroid/content/Context;Landroid/widget/ListView;I)Lcom/twitter/internal/android/widget/GroupedRowView;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    iput-object v9, p0, Lcom/twitter/android/fd;->b:Ljava/util/LinkedHashSet;

    if-eqz p4, :cond_3

    const v2, 0x7f0f000d    # com.twitter.android.R.string.activity

    const v5, 0x7f020175    # com.twitter.android.R.drawable.ic_discover_activity_default

    move-object v0, p1

    move-object v1, p2

    move v4, p3

    move v6, v7

    invoke-static/range {v0 .. v6}, Lcom/twitter/android/fd;->a(Landroid/content/Context;Landroid/widget/ListView;ILjava/lang/String;III)Lcom/twitter/internal/android/widget/GroupedRowView;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/fd;->c:Lcom/twitter/internal/android/widget/GroupedRowView;

    :goto_2
    if-eqz p5, :cond_4

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/featureswitch/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const v5, 0x7f0202b1    # com.twitter.android.R.drawable.icn_sports_default

    move-object v0, p1

    move-object v1, p2

    move v2, v8

    move v4, p3

    move v6, v7

    invoke-static/range {v0 .. v6}, Lcom/twitter/android/fd;->a(Landroid/content/Context;Landroid/widget/ListView;ILjava/lang/String;III)Lcom/twitter/internal/android/widget/GroupedRowView;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/fd;->d:Lcom/twitter/internal/android/widget/GroupedRowView;

    :goto_3
    return-void

    :cond_2
    move v6, v7

    goto :goto_1

    :cond_3
    iput-object v3, p0, Lcom/twitter/android/fd;->c:Lcom/twitter/internal/android/widget/GroupedRowView;

    goto :goto_2

    :cond_4
    iput-object v3, p0, Lcom/twitter/android/fd;->d:Lcom/twitter/internal/android/widget/GroupedRowView;

    goto :goto_3
.end method

.method private static a(Landroid/content/Context;Landroid/widget/ListView;I)Lcom/twitter/internal/android/widget/GroupedRowView;
    .locals 3

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03004b    # com.twitter.android.R.layout.discover_trend_header

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    new-instance v1, Lcom/twitter/android/fc;

    invoke-direct {v1, v0}, Lcom/twitter/android/fc;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v0, p2}, Lcom/twitter/internal/android/widget/GroupedRowView;->setStyle(I)V

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->setGroupStyle(I)V

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    return-object v0
.end method

.method private static a(Landroid/content/Context;Landroid/widget/ListView;ILjava/lang/String;III)Lcom/twitter/internal/android/widget/GroupedRowView;
    .locals 3

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03004b    # com.twitter.android.R.layout.discover_trend_header

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    new-instance v1, Lcom/twitter/android/fc;

    invoke-direct {v1, v0}, Lcom/twitter/android/fc;-><init>(Landroid/view/View;)V

    if-eqz p3, :cond_0

    invoke-virtual {v1, p0, p3, p4}, Lcom/twitter/android/fc;->a(Landroid/content/Context;Ljava/lang/CharSequence;I)V

    :goto_0
    invoke-virtual {v1, p5}, Lcom/twitter/android/fc;->b(I)V

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v0, p6}, Lcom/twitter/internal/android/widget/GroupedRowView;->setStyle(I)V

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->setGroupStyle(I)V

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->setVisibility(I)V

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    return-object v0

    :cond_0
    invoke-virtual {v1, p0, p2, p4}, Lcom/twitter/android/fc;->a(Landroid/content/Context;II)V

    goto :goto_0
.end method


# virtual methods
.method a()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/fd;->b:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->invalidate()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method a(I)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/fd;->b:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/GroupedRowView;->setVisibility(I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/fd;->a:Lcom/twitter/internal/android/widget/GroupedRowView;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/GroupedRowView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/fd;->c:Lcom/twitter/internal/android/widget/GroupedRowView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/fd;->c:Lcom/twitter/internal/android/widget/GroupedRowView;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/GroupedRowView;->setVisibility(I)V

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/fd;->d:Lcom/twitter/internal/android/widget/GroupedRowView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/fd;->d:Lcom/twitter/internal/android/widget/GroupedRowView;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/GroupedRowView;->setVisibility(I)V

    :cond_2
    return-void
.end method

.method a(Landroid/view/View;)Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/fd;->b:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
