.class Lcom/twitter/android/pc;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/PostActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/PostActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/pc;->a:Lcom/twitter/android/PostActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/twitter/android/pc;->a:Lcom/twitter/android/PostActivity;

    invoke-static {v0}, Lcom/twitter/android/PostActivity;->z(Lcom/twitter/android/PostActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    const/4 v1, -0x1

    if-ne v1, p2, :cond_0

    iget-object v1, p0, Lcom/twitter/android/pc;->a:Lcom/twitter/android/PostActivity;

    invoke-static {v1}, Lcom/twitter/android/PostActivity;->A(Lcom/twitter/android/PostActivity;)V

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->e()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/pc;->a:Lcom/twitter/android/PostActivity;

    const/16 v2, 0x204

    invoke-virtual {v1, v2}, Lcom/twitter/android/PostActivity;->showDialog(I)V

    iget-object v1, p0, Lcom/twitter/android/pc;->a:Lcom/twitter/android/PostActivity;

    invoke-static {v1}, Lcom/twitter/android/PostActivity;->B(Lcom/twitter/android/PostActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "location_prompt::::impression"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/pc;->a:Lcom/twitter/android/PostActivity;

    invoke-static {v0}, Lcom/twitter/android/PostActivity;->C(Lcom/twitter/android/PostActivity;)Lcom/twitter/library/platform/LocationProducer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/platform/LocationProducer;->e()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/pc;->a:Lcom/twitter/android/PostActivity;

    const/16 v1, 0x205

    invoke-virtual {v0, v1}, Lcom/twitter/android/PostActivity;->showDialog(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/pc;->a:Lcom/twitter/android/PostActivity;

    iget-boolean v0, v0, Lcom/twitter/android/PostActivity;->z:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/pc;->a:Lcom/twitter/android/PostActivity;

    const/4 v1, 0x4

    invoke-virtual {v0, v1, v3}, Lcom/twitter/android/PostActivity;->a(IZ)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/pc;->a:Lcom/twitter/android/PostActivity;

    invoke-static {v0}, Lcom/twitter/android/PostActivity;->t(Lcom/twitter/android/PostActivity;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/pc;->a:Lcom/twitter/android/PostActivity;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v3}, Lcom/twitter/android/PostActivity;->a(IZ)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/twitter/android/pc;->a:Lcom/twitter/android/PostActivity;

    invoke-virtual {v0, v3}, Lcom/twitter/android/PostActivity;->c(Z)V

    iget-object v0, p0, Lcom/twitter/android/pc;->a:Lcom/twitter/android/PostActivity;

    invoke-static {v0}, Lcom/twitter/android/PostActivity;->D(Lcom/twitter/android/PostActivity;)Lcom/twitter/library/platform/LocationProducer;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/pc;->a:Lcom/twitter/android/PostActivity;

    invoke-virtual {v0, v1}, Lcom/twitter/library/platform/LocationProducer;->a(Lcom/twitter/library/platform/i;)V

    goto :goto_0
.end method
