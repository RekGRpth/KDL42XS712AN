.class Lcom/twitter/android/fu;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/ExperimentSettingsActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/ExperimentSettingsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/fu;->a:Lcom/twitter/android/ExperimentSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 9

    const-wide/16 v3, 0x0

    const/4 v8, 0x1

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v0, "unassigned"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    const-string/jumbo v2, "all"

    invoke-static {v3, v4, v2}, Lju;->a(JLjava/lang/String;)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const v2, 0x7f0f03e8    # com.twitter.android.R.string.settings_lo_experiment_current_bucket_summary

    :goto_1
    iget-object v5, p0, Lcom/twitter/android/fu;->a:Lcom/twitter/android/ExperimentSettingsActivity;

    invoke-static {v5}, Lcom/twitter/android/ExperimentSettingsActivity;->d(Lcom/twitter/android/ExperimentSettingsActivity;)Landroid/content/res/Resources;

    move-result-object v5

    new-array v6, v8, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v1, v6, v7

    invoke-virtual {v5, v2, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v4, v1, v0}, Lju;->a(JLjava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/twitter/android/fu;->a:Lcom/twitter/android/ExperimentSettingsActivity;

    invoke-static {v1}, Lcom/twitter/android/ExperimentSettingsActivity;->e(Lcom/twitter/android/ExperimentSettingsActivity;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return v8

    :cond_0
    move-object v0, v1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/twitter/android/fu;->a:Lcom/twitter/android/ExperimentSettingsActivity;

    invoke-static {v2}, Lcom/twitter/android/ExperimentSettingsActivity;->c(Lcom/twitter/android/ExperimentSettingsActivity;)J

    move-result-wide v3

    const v2, 0x7f0f03d7    # com.twitter.android.R.string.settings_experiment_current_bucket_summary

    goto :goto_1
.end method
