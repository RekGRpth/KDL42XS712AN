.class Lcom/twitter/android/go;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/twitter/android/client/c;

.field final synthetic b:Lcom/twitter/android/FollowActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/FollowActivity;Lcom/twitter/android/client/c;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/go;->b:Lcom/twitter/android/FollowActivity;

    iput-object p2, p0, Lcom/twitter/android/go;->a:Lcom/twitter/android/client/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    const/4 v6, 0x1

    iget-object v0, p0, Lcom/twitter/android/go;->b:Lcom/twitter/android/FollowActivity;

    invoke-static {v0}, Lcom/twitter/android/FollowActivity;->d(Lcom/twitter/android/FollowActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/go;->b:Lcom/twitter/android/FollowActivity;

    invoke-static {v0, v6}, Lcom/twitter/android/FollowActivity;->a(Lcom/twitter/android/FollowActivity;Z)Z

    iget-object v0, p0, Lcom/twitter/android/go;->b:Lcom/twitter/android/FollowActivity;

    invoke-virtual {v0}, Lcom/twitter/android/FollowActivity;->V()V

    iget-object v0, p0, Lcom/twitter/android/go;->a:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/go;->b:Lcom/twitter/android/FollowActivity;

    invoke-static {v1}, Lcom/twitter/android/FollowActivity;->e(Lcom/twitter/android/FollowActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/twitter/android/go;->b:Lcom/twitter/android/FollowActivity;

    invoke-static {v5}, Lcom/twitter/android/FollowActivity;->f(Lcom/twitter/android/FollowActivity;)Lcom/twitter/android/FollowFlowController;

    move-result-object v5

    invoke-virtual {v5}, Lcom/twitter/android/FollowFlowController;->g()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const-string/jumbo v4, "follow_friends:stream::timeout"

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_0
    return-void
.end method
