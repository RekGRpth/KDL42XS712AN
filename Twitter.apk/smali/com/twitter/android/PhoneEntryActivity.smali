.class public Lcom/twitter/android/PhoneEntryActivity;
.super Lcom/twitter/android/client/BaseFragmentActivity;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Lcom/twitter/android/FollowFlowController;

.field private b:Lcom/twitter/android/util/d;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;-><init>()V

    return-void
.end method

.method private f()V
    .locals 2

    const v0, 0x7f09020b    # com.twitter.android.R.id.validation_error

    invoke-virtual {p0, v0}, Lcom/twitter/android/PhoneEntryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/PhoneEntryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    if-nez p1, :cond_1

    const-string/jumbo v1, "flow_controller"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    :goto_0
    check-cast v0, Lcom/twitter/android/FollowFlowController;

    check-cast v0, Lcom/twitter/android/FollowFlowController;

    iput-object v0, p0, Lcom/twitter/android/PhoneEntryActivity;->a:Lcom/twitter/android/FollowFlowController;

    iget-object v0, p0, Lcom/twitter/android/PhoneEntryActivity;->a:Lcom/twitter/android/FollowFlowController;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/android/FollowFlowController;

    invoke-direct {v0}, Lcom/twitter/android/FollowFlowController;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/PhoneEntryActivity;->a:Lcom/twitter/android/FollowFlowController;

    :cond_0
    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    const v1, 0x7f03004a    # com.twitter.android.R.layout.digits_nux_activity

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->a(Z)V

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->c(I)V

    return-object v0

    :cond_1
    const-string/jumbo v0, "flow_controller"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 7

    const/4 v6, -0x2

    const/4 v5, 0x0

    invoke-static {p0}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;)Lcom/twitter/android/util/d;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/PhoneEntryActivity;->b:Lcom/twitter/android/util/d;

    invoke-virtual {p0}, Lcom/twitter/android/PhoneEntryActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/PhoneEntryActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "welcome:enter_phone:::impression"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    const v0, 0x7f09011d    # com.twitter.android.R.id.next

    invoke-virtual {p0, v0}, Lcom/twitter/android/PhoneEntryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f09011b    # com.twitter.android.R.id.skip

    invoke-virtual {p0, v0}, Lcom/twitter/android/PhoneEntryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f09011c    # com.twitter.android.R.id.digits_cta

    invoke-virtual {p0, v0}, Lcom/twitter/android/PhoneEntryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    const v1, 0x7f0f0018    # com.twitter.android.R.string.add_phone

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-nez p1, :cond_0

    new-instance v0, Lcom/twitter/android/PhoneEntryActivity$PhoneEntryFragment;

    invoke-direct {v0}, Lcom/twitter/android/PhoneEntryActivity$PhoneEntryFragment;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/android/PhoneEntryActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0900e3    # com.twitter.android.R.id.fragment_container

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    :cond_0
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0300d8    # com.twitter.android.R.layout.nux_progress_indicator

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f09000b    # com.twitter.android.R.id.indicator

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/twitter/android/PhoneEntryActivity;->a:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v2, p0}, Lcom/twitter/android/FollowFlowController;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/twitter/android/PhoneEntryActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    new-instance v2, Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;

    const/4 v3, 0x5

    invoke-direct {v2, v6, v6, v3}, Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;-><init>(III)V

    invoke-virtual {v0, v1, v2}, Lcom/twitter/internal/android/widget/ToolBar;->a(Landroid/view/View;Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/twitter/android/PhoneEntryActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/PhoneEntryActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "welcome:enter_phone:::skip"

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/PhoneEntryActivity;->a:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v0, p0}, Lcom/twitter/android/FollowFlowController;->a(Landroid/app/Activity;)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/twitter/android/PhoneEntryActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/PhoneEntryActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "welcome:enter_phone:::submit"

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    const v0, 0x7f09020a    # com.twitter.android.R.id.phone_entry

    invoke-virtual {p0, v0}, Lcom/twitter/android/PhoneEntryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/twitter/android/PhoneEntryActivity;->b:Lcom/twitter/android/util/d;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/twitter/android/util/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const v1, 0x7f09020b    # com.twitter.android.R.id.validation_error

    invoke-virtual {p0, v1}, Lcom/twitter/android/PhoneEntryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/twitter/android/PhoneEntryActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/PhoneEntryActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    new-array v4, v6, [Ljava/lang/String;

    const-string/jumbo v5, "welcome:enter_phone:::success"

    aput-object v5, v4, v7

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v1, p0, Lcom/twitter/android/PhoneEntryActivity;->a:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {v1, v6}, Lcom/twitter/android/FollowFlowController;->b(Z)Lcom/twitter/android/FollowFlowController;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/FollowFlowController;->a(Ljava/lang/String;)Lcom/twitter/android/FollowFlowController;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/android/FollowFlowController;->a(Landroid/app/Activity;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/PhoneEntryActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/PhoneEntryActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "welcome:enter_phone:::error"

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/twitter/android/PhoneEntryActivity;->f()V

    goto/16 :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/PhoneEntryActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/PhoneEntryActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "welcome:enter_phone:::unavailable"

    aput-object v4, v3, v7

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/twitter/android/PhoneEntryActivity;->f()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f09011b
        :pswitch_0    # com.twitter.android.R.id.skip
        :pswitch_1    # com.twitter.android.R.id.digits_cta
    .end packed-switch
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "flow_controller"

    iget-object v1, p0, Lcom/twitter/android/PhoneEntryActivity;->a:Lcom/twitter/android/FollowFlowController;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method
