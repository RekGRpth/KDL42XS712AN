.class public Lcom/twitter/android/SearchActivity;
.super Lcom/twitter/android/client/BaseFragmentActivity;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/app/FragmentManager$OnBackStackChangedListener;
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# static fields
.field private static final j:Ljava/util/HashMap;


# instance fields
.field a:Lcom/twitter/android/SearchFragment;

.field b:Lcom/twitter/android/sb;

.field c:I

.field d:Z

.field e:Z

.field f:Z

.field g:Z

.field h:Z

.field i:Z

.field private k:Lcom/twitter/library/provider/az;

.field private l:Lcom/twitter/library/scribe/ScribeAssociation;

.field private m:Ljava/util/HashSet;

.field private n:Ljava/util/HashMap;

.field private o:Lcom/twitter/library/widget/SlidingPanel;

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Landroid/widget/RadioGroup;

.field private t:Landroid/widget/RadioButton;

.field private u:Landroid/widget/RadioButton;

.field private v:Landroid/widget/TextView;

.field private w:Lcom/twitter/internal/android/widget/DockLayout;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v3, 0x9

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v3}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/twitter/android/SearchActivity;->j:Ljava/util/HashMap;

    sget-object v0, Lcom/twitter/android/SearchActivity;->j:Ljava/util/HashMap;

    const-string/jumbo v1, "com.twitter.android.action.USER_SHOW"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/SearchActivity;->j:Ljava/util/HashMap;

    const-string/jumbo v1, "com.twitter.android.action.USER_SHOW_TYPEAHEAD"

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/SearchActivity;->j:Ljava/util/HashMap;

    const-string/jumbo v1, "com.twitter.android.action.USER_SHOW_SEARCH_SUGGESTION"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/SearchActivity;->j:Ljava/util/HashMap;

    const-string/jumbo v1, "com.twitter.android.action.SEARCH"

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/SearchActivity;->j:Ljava/util/HashMap;

    const-string/jumbo v1, "com.twitter.android.action.SEARCH_RECENT"

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/SearchActivity;->j:Ljava/util/HashMap;

    const-string/jumbo v1, "com.twitter.android.action.SEARCH_TYPEAHEAD_TOPIC"

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/SearchActivity;->j:Ljava/util/HashMap;

    const-string/jumbo v1, "com.twitter.android.action.SEARCH_QUERY_SAVED"

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/SearchActivity;->j:Ljava/util/HashMap;

    const-string/jumbo v1, "com.twitter.android.action.SEARCH_TREND"

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/SearchActivity;->j:Ljava/util/HashMap;

    const-string/jumbo v1, "com.twitter.android.action.SEARCH_QUERY_ONECLICK"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/SearchActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/content/Intent;Z)V
    .locals 13

    const/4 v12, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    const-string/jumbo v0, "twitter"

    invoke-virtual {p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    const-string/jumbo v1, "query"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "event_id"

    invoke-virtual {v0, v2}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v2, "event_id"

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v0, "terminal"

    invoke-virtual {p1, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_0
    const-string/jumbo v0, "query"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move v8, v7

    move v2, v7

    :goto_0
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v3, "search_id"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/twitter/internal/util/j;->a:Ljava/security/SecureRandom;

    invoke-virtual {v0}, Ljava/security/SecureRandom;->nextLong()J

    move-result-wide v9

    iget-object v11, p0, Lcom/twitter/android/SearchActivity;->n:Ljava/util/HashMap;

    new-instance v0, Lcom/twitter/android/sc;

    const-string/jumbo v3, "follows"

    invoke-virtual {p1, v3, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    const-string/jumbo v4, "near"

    invoke-virtual {p1, v4, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    const-string/jumbo v5, "recent"

    invoke-virtual {p1, v5, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/sc;-><init>(Ljava/lang/String;IZZZ)V

    invoke-virtual {v0}, Lcom/twitter/android/sc;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v9, v10}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string/jumbo v0, "search_id"

    invoke-virtual {p1, v0, v9, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->k:Lcom/twitter/library/provider/az;

    invoke-virtual {v0, v9, v10}, Lcom/twitter/library/provider/az;->r(J)V

    :cond_1
    const-string/jumbo v0, "search_type"

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v0, 0x3

    if-ne v2, v0, :cond_8

    const-string/jumbo v0, "photo_list"

    invoke-virtual {p1, v0, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_8

    new-instance v0, Lcom/twitter/android/SearchPhotosFragment;

    invoke-direct {v0}, Lcom/twitter/android/SearchPhotosFragment;-><init>()V

    :goto_1
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v2, "empty_title"

    const v3, 0x7f0f03a9    # com.twitter.android.R.string.search_no_results

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v2, "empty_desc"

    const v3, 0x7f0f03aa    # com.twitter.android.R.string.search_no_results_details

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/SearchFragment;->setArguments(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/SearchFragment;->a(Landroid/content/Intent;)Lcom/twitter/android/client/BaseListFragment;

    invoke-virtual {v0, v6}, Lcom/twitter/android/SearchFragment;->j(Z)Lcom/twitter/android/client/BaseListFragment;

    invoke-virtual {v0, v8}, Lcom/twitter/android/SearchFragment;->d(Z)V

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v2, "in_back_stack"

    invoke-virtual {p1, v2, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v1, p0}, Landroid/support/v4/app/FragmentManager;->removeOnBackStackChangedListener(Landroid/support/v4/app/FragmentManager$OnBackStackChangedListener;)V

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->popBackStackImmediate()Z

    move-result p2

    invoke-virtual {v1, p0}, Landroid/support/v4/app/FragmentManager;->addOnBackStackChangedListener(Landroid/support/v4/app/FragmentManager$OnBackStackChangedListener;)V

    :cond_2
    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    const/16 v2, 0x1001

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentTransaction;->setTransition(I)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0900df    # com.twitter.android.R.id.root_layout

    const-string/jumbo v3, "search_fragment"

    invoke-virtual {v1, v2, v0, v3}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    if-eqz p2, :cond_3

    invoke-virtual {v0, v12}, Landroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    :cond_3
    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    :goto_2
    return-void

    :cond_4
    sget-object v0, Lcom/twitter/android/SearchActivity;->j:Ljava/util/HashMap;

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    const-string/jumbo v1, "query"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    if-eqz v0, :cond_5

    const-string/jumbo v1, "user_query"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v1, "source_association"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_5
    move v0, v7

    :goto_3
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-nez v1, :cond_7

    const-string/jumbo v1, "search_type"

    invoke-virtual {p1, v1, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const-string/jumbo v1, "q_source"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_9

    const-string/jumbo v1, "q_source"

    const-string/jumbo v4, "typed_query"

    invoke-virtual {p1, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move v8, v0

    move-object v1, v3

    goto/16 :goto_0

    :pswitch_0
    const-string/jumbo v0, "search_box"

    const-string/jumbo v4, "go_to_user"

    const-string/jumbo v5, "click"

    invoke-static {v1, v0, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v2, v3}, Lcom/twitter/android/SearchActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "screen_name"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "association"

    iget-object v2, p0, Lcom/twitter/android/SearchActivity;->l:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "expanded_search"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->finish()V

    goto/16 :goto_2

    :pswitch_1
    const-string/jumbo v0, "search_box"

    const-string/jumbo v4, "typeahead"

    const-string/jumbo v5, "profile_click"

    invoke-static {v1, v0, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v2, v3, p1}, Lcom/twitter/android/SearchActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "screen_name"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "association"

    iget-object v2, p0, Lcom/twitter/android/SearchActivity;->l:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "expanded_search"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->finish()V

    goto/16 :goto_2

    :pswitch_2
    const-string/jumbo v0, "search_box"

    const-string/jumbo v4, "user"

    const-string/jumbo v5, "click"

    invoke-static {v1, v0, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v2, v3}, Lcom/twitter/android/SearchActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "screen_name"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "association"

    iget-object v2, p0, Lcom/twitter/android/SearchActivity;->l:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "expanded_search"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->finish()V

    goto/16 :goto_2

    :pswitch_3
    const-string/jumbo v0, "q_source"

    const-string/jumbo v4, "typed_query"

    invoke-virtual {p1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v4, "scribe_context"

    const-string/jumbo v5, "search_box"

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v0, "search_box"

    const-string/jumbo v4, "search"

    invoke-static {v1, v0, v12, v4}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v2, v3}, Lcom/twitter/android/SearchActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v7

    goto/16 :goto_3

    :pswitch_4
    const-string/jumbo v0, "q_source"

    const-string/jumbo v4, "recent_search_click"

    invoke-virtual {p1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v4, "scribe_context"

    const-string/jumbo v5, "typeahead_recent_search"

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v0, "search_box"

    const-string/jumbo v4, "recent"

    const-string/jumbo v5, "search"

    invoke-static {v1, v0, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v2, v3}, Lcom/twitter/android/SearchActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v7

    goto/16 :goto_3

    :pswitch_5
    const-string/jumbo v0, "q_source"

    const-string/jumbo v4, "typeahead_click"

    invoke-virtual {p1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v4, "scribe_context"

    const-string/jumbo v5, "typeahead"

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v0, "search_box"

    const-string/jumbo v4, "typeahead"

    const-string/jumbo v5, "search"

    invoke-static {v1, v0, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v2, v3, p1}, Lcom/twitter/android/SearchActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V

    move v0, v7

    goto/16 :goto_3

    :pswitch_6
    const-string/jumbo v0, "q_source"

    const-string/jumbo v4, "saved_search_click"

    invoke-virtual {p1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v4, "scribe_context"

    const-string/jumbo v5, "typeahead_saved_search"

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v0, "search_box"

    const-string/jumbo v4, "saved_search"

    const-string/jumbo v5, "search"

    invoke-static {v1, v0, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v2, v3, p1}, Lcom/twitter/android/SearchActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V

    move v0, v6

    goto/16 :goto_3

    :pswitch_7
    const-string/jumbo v0, "q_source"

    const-string/jumbo v4, "typed_query"

    invoke-virtual {p1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v4, "scribe_context"

    const-string/jumbo v5, "typeahead_cluster"

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v0, "search_box"

    const-string/jumbo v4, "cluster"

    const-string/jumbo v5, "search"

    invoke-static {v1, v0, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v2, v3}, Lcom/twitter/android/SearchActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v7

    goto/16 :goto_3

    :pswitch_8
    const-string/jumbo v0, "query_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v6

    :goto_4
    iput-boolean v0, p0, Lcom/twitter/android/SearchActivity;->h:Z

    const-string/jumbo v0, "q_source"

    const-string/jumbo v4, "taoc"

    invoke-virtual {p1, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v4, "terminal"

    iget-boolean v5, p0, Lcom/twitter/android/SearchActivity;->h:Z

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v4, "scribe_context"

    const-string/jumbo v5, "typeahead_oneclick_search"

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v0, "search_box"

    const-string/jumbo v4, "typeahead_concierge"

    const-string/jumbo v5, "search"

    invoke-static {v1, v0, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v2, v3}, Lcom/twitter/android/SearchActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "follows"

    invoke-virtual {p1, v0, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/SearchActivity;->d:Z

    const-string/jumbo v0, "near"

    invoke-virtual {p1, v0, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/SearchActivity;->e:Z

    move v0, v7

    goto/16 :goto_3

    :cond_6
    move v0, v7

    goto :goto_4

    :cond_7
    move v8, v0

    move v2, v7

    move-object v1, v3

    goto/16 :goto_0

    :cond_8
    new-instance v0, Lcom/twitter/android/SearchResultsFragment;

    invoke-direct {v0}, Lcom/twitter/android/SearchResultsFragment;-><init>()V

    goto/16 :goto_1

    :cond_9
    move v8, v0

    move-object v1, v3

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method private a(Lcom/twitter/android/SearchFragment;)V
    .locals 6

    const/4 v0, 0x1

    invoke-virtual {p1}, Lcom/twitter/android/SearchFragment;->L()I

    move-result v1

    iput v1, p0, Lcom/twitter/android/SearchActivity;->c:I

    invoke-virtual {p1}, Lcom/twitter/android/SearchFragment;->F()Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/android/SearchActivity;->d:Z

    invoke-virtual {p1}, Lcom/twitter/android/SearchFragment;->E()Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/android/SearchActivity;->e:Z

    instance-of v1, p1, Lcom/twitter/android/SearchPhotosFragment;

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    invoke-direct {p0, v0}, Lcom/twitter/android/SearchActivity;->b(Z)V

    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->b:Lcom/twitter/android/sb;

    iget v1, p0, Lcom/twitter/android/SearchActivity;->c:I

    iget-boolean v2, p0, Lcom/twitter/android/SearchActivity;->d:Z

    iget-boolean v3, p0, Lcom/twitter/android/SearchActivity;->e:Z

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/sb;->a(IZZ)V

    invoke-virtual {p1}, Lcom/twitter/android/SearchFragment;->I()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/SearchActivity;->r:Z

    invoke-virtual {p1}, Lcom/twitter/android/SearchFragment;->D()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/twitter/android/SearchFragment;->q_()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/twitter/android/SearchActivity;->c:I

    invoke-virtual {p1}, Lcom/twitter/android/SearchFragment;->H()Z

    move-result v4

    move-object v0, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/SearchActivity;->a(Ljava/lang/String;Ljava/lang/String;IZLcom/twitter/android/SearchFragment;)V

    return-void

    :cond_1
    iget v1, p0, Lcom/twitter/android/SearchActivity;->c:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Lcom/twitter/android/SearchActivity;->c:I

    const/4 v2, 0x4

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/twitter/android/SearchActivity;->c:I

    const/16 v2, 0x8

    if-eq v1, v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/twitter/android/SearchFragment;Z)V
    .locals 10

    const/4 v8, 0x1

    const/4 v7, 0x0

    if-nez p2, :cond_0

    invoke-direct {p0, v7}, Lcom/twitter/android/SearchActivity;->b(Z)V

    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->o:Lcom/twitter/library/widget/SlidingPanel;

    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->c()Z

    :cond_0
    iget-boolean v0, p0, Lcom/twitter/android/SearchActivity;->f:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/twitter/android/SearchActivity;->g:Z

    iget v1, p0, Lcom/twitter/android/SearchActivity;->c:I

    packed-switch v1, :pswitch_data_0

    :cond_1
    :pswitch_0
    move v6, v7

    move v5, v7

    :goto_0
    invoke-virtual {p1}, Lcom/twitter/android/SearchFragment;->C()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/twitter/android/SearchFragment;->q_()Ljava/lang/String;

    move-result-object v9

    new-instance v0, Lcom/twitter/android/sc;

    iget v2, p0, Lcom/twitter/android/SearchActivity;->c:I

    iget-boolean v3, p0, Lcom/twitter/android/SearchActivity;->d:Z

    iget-boolean v4, p0, Lcom/twitter/android/SearchActivity;->e:Z

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/sc;-><init>(Ljava/lang/String;IZZZ)V

    invoke-virtual {v0}, Lcom/twitter/android/sc;->hashCode()I

    move-result v2

    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/twitter/android/SearchActivity;

    invoke-direct {v0, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v3, "query"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "query_name"

    invoke-virtual {v0, v1, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "search_type"

    iget v3, p0, Lcom/twitter/android/SearchActivity;->c:I

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "follows"

    iget-boolean v3, p0, Lcom/twitter/android/SearchActivity;->d:Z

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "near"

    iget-boolean v3, p0, Lcom/twitter/android/SearchActivity;->e:Z

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "terminal"

    iget-boolean v3, p0, Lcom/twitter/android/SearchActivity;->q:Z

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "recent"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "photo_list"

    if-nez v6, :cond_3

    :goto_1
    invoke-virtual {v0, v1, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "in_back_stack"

    invoke-virtual {v0, v1, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->n:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    if-eqz v0, :cond_4

    const-string/jumbo v2, "search_id"

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    :goto_2
    invoke-virtual {p0, v1}, Lcom/twitter/android/SearchActivity;->startActivity(Landroid/content/Intent;)V

    iput-boolean v7, p0, Lcom/twitter/android/SearchActivity;->f:Z

    :cond_2
    return-void

    :pswitch_1
    if-eqz v0, :cond_1

    iput v8, p0, Lcom/twitter/android/SearchActivity;->c:I

    move v6, v7

    move v5, v8

    goto/16 :goto_0

    :pswitch_2
    if-nez v0, :cond_1

    iput v7, p0, Lcom/twitter/android/SearchActivity;->c:I

    move v6, v7

    move v5, v7

    goto/16 :goto_0

    :pswitch_3
    move v6, v0

    move v5, v7

    goto/16 :goto_0

    :pswitch_4
    if-eqz v0, :cond_1

    const/4 v0, 0x4

    iput v0, p0, Lcom/twitter/android/SearchActivity;->c:I

    move v6, v7

    move v5, v7

    goto/16 :goto_0

    :pswitch_5
    if-nez v0, :cond_1

    const/4 v0, 0x5

    iput v0, p0, Lcom/twitter/android/SearchActivity;->c:I

    move v6, v7

    move v5, v7

    goto/16 :goto_0

    :pswitch_6
    if-eqz v0, :cond_1

    const/16 v0, 0x8

    iput v0, p0, Lcom/twitter/android/SearchActivity;->c:I

    move v6, v7

    move v5, v7

    goto/16 :goto_0

    :pswitch_7
    if-nez v0, :cond_1

    const/4 v0, 0x7

    iput v0, p0, Lcom/twitter/android/SearchActivity;->c:I

    move v6, v7

    move v5, v7

    goto/16 :goto_0

    :cond_3
    move v8, v7

    goto :goto_1

    :cond_4
    sget-object v0, Lcom/twitter/internal/util/j;->a:Ljava/security/SecureRandom;

    invoke-virtual {v0}, Ljava/security/SecureRandom;->nextLong()J

    move-result-wide v3

    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->n:Ljava/util/HashMap;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v0, v2, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->k:Lcom/twitter/library/provider/az;

    invoke-virtual {v0, v3, v4}, Lcom/twitter/library/provider/az;->r(J)V

    const-string/jumbo v0, "search_id"

    invoke-virtual {v1, v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_0
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;IZLcom/twitter/android/SearchFragment;)V
    .locals 9

    const/4 v8, 0x3

    const/4 v6, 0x0

    const/16 v5, 0x8

    const/4 v1, 0x1

    const/4 v7, 0x0

    packed-switch p3, :pswitch_data_0

    iget-boolean v0, p0, Lcom/twitter/android/SearchActivity;->d:Z

    if-eqz v0, :cond_2

    const v0, 0x7f0f0484    # com.twitter.android.R.string.subtitle_following

    :goto_0
    iget-object v2, p0, Lcom/twitter/android/SearchActivity;->t:Landroid/widget/RadioButton;

    const v3, 0x7f0f03b1    # com.twitter.android.R.string.search_scope_top

    invoke-virtual {v2, v3}, Landroid/widget/RadioButton;->setText(I)V

    iget-object v2, p0, Lcom/twitter/android/SearchActivity;->u:Landroid/widget/RadioButton;

    const v3, 0x7f0f03ac    # com.twitter.android.R.string.search_scope_all

    invoke-virtual {v2, v3}, Landroid/widget/RadioButton;->setText(I)V

    move v2, v0

    move v0, v7

    :goto_1
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v4

    if-eqz p4, :cond_7

    iget-boolean v3, p0, Lcom/twitter/android/SearchActivity;->h:Z

    if-eqz v3, :cond_4

    if-ne p3, v8, :cond_4

    iget-object v3, p0, Lcom/twitter/android/SearchActivity;->u:Landroid/widget/RadioButton;

    invoke-virtual {v3, v7}, Landroid/widget/RadioButton;->setVisibility(I)V

    iget-object v3, p0, Lcom/twitter/android/SearchActivity;->s:Landroid/widget/RadioGroup;

    invoke-virtual {v3, v7}, Landroid/widget/RadioGroup;->setVisibility(I)V

    invoke-virtual {p5}, Lcom/twitter/android/SearchFragment;->K()V

    :goto_2
    invoke-virtual {v4, v6}, Lcom/twitter/internal/android/widget/ToolBar;->setCustomView(Landroid/view/View;)V

    invoke-virtual {p0, p2}, Lcom/twitter/android/SearchActivity;->setTitle(Ljava/lang/CharSequence;)V

    if-eqz v2, :cond_6

    iget-boolean v3, p0, Lcom/twitter/android/SearchActivity;->h:Z

    if-nez v3, :cond_6

    invoke-virtual {p0, v2}, Lcom/twitter/android/SearchActivity;->e(I)V

    :goto_3
    invoke-virtual {v4, p4}, Lcom/twitter/internal/android/widget/ToolBar;->setDisplayShowTitleEnabled(Z)V

    iget-boolean v2, p0, Lcom/twitter/android/SearchActivity;->g:Z

    if-eqz v2, :cond_9

    iget-object v4, p0, Lcom/twitter/android/SearchActivity;->u:Landroid/widget/RadioButton;

    :goto_4
    iget-boolean v2, p0, Lcom/twitter/android/SearchActivity;->h:Z

    if-eqz v2, :cond_a

    if-ne p3, v8, :cond_a

    move v2, v1

    :goto_5
    if-eqz p4, :cond_0

    if-eqz v2, :cond_b

    :cond_0
    if-nez v0, :cond_b

    move v6, v1

    :goto_6
    new-instance v0, Lcom/twitter/android/rw;

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/SearchActivity;->w:Lcom/twitter/internal/android/widget/DockLayout;

    iget-object v3, p0, Lcom/twitter/android/SearchActivity;->s:Landroid/widget/RadioGroup;

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/rw;-><init>(Landroid/content/res/Resources;Lcom/twitter/internal/android/widget/DockLayout;Landroid/view/View;Landroid/widget/RadioButton;Lcom/twitter/android/SearchFragment;Z)V

    invoke-virtual {p5, v0}, Lcom/twitter/android/SearchFragment;->a(Lcom/twitter/android/se;)V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_c

    :goto_7
    invoke-virtual {p2, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x23

    if-eq v0, v1, :cond_1

    invoke-virtual {p2, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x24

    if-ne v0, v1, :cond_d

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f04d7    # com.twitter.android.R.string.tweet_title

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchActivity;->c(Ljava/lang/String;)V

    :goto_8
    iput-object p5, p0, Lcom/twitter/android/SearchActivity;->a:Lcom/twitter/android/SearchFragment;

    iput-boolean p4, p0, Lcom/twitter/android/SearchActivity;->q:Z

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->V()V

    return-void

    :pswitch_0
    const v0, 0x7f0f0487    # com.twitter.android.R.string.subtitle_people

    iget-object v2, p0, Lcom/twitter/android/SearchActivity;->t:Landroid/widget/RadioButton;

    const v3, 0x7f0f0487    # com.twitter.android.R.string.subtitle_people

    invoke-virtual {v2, v3}, Landroid/widget/RadioButton;->setText(I)V

    move v2, v0

    move v0, v1

    goto/16 :goto_1

    :pswitch_1
    const v0, 0x7f0f0488    # com.twitter.android.R.string.subtitle_photos

    iget-object v2, p0, Lcom/twitter/android/SearchActivity;->t:Landroid/widget/RadioButton;

    const v3, 0x7f0f03af    # com.twitter.android.R.string.search_scope_list

    invoke-virtual {v2, v3}, Landroid/widget/RadioButton;->setText(I)V

    iget-object v2, p0, Lcom/twitter/android/SearchActivity;->u:Landroid/widget/RadioButton;

    const v3, 0x7f0f03ae    # com.twitter.android.R.string.search_scope_grid

    invoke-virtual {v2, v3}, Landroid/widget/RadioButton;->setText(I)V

    move v2, v0

    move v0, v7

    goto/16 :goto_1

    :pswitch_2
    const v0, 0x7f0f0489    # com.twitter.android.R.string.subtitle_videos

    iget-object v2, p0, Lcom/twitter/android/SearchActivity;->t:Landroid/widget/RadioButton;

    const v3, 0x7f0f03b2    # com.twitter.android.R.string.search_scope_video

    invoke-virtual {v2, v3}, Landroid/widget/RadioButton;->setText(I)V

    iget-object v2, p0, Lcom/twitter/android/SearchActivity;->u:Landroid/widget/RadioButton;

    const v3, 0x7f0f03b3    # com.twitter.android.R.string.search_scope_vine

    invoke-virtual {v2, v3}, Landroid/widget/RadioButton;->setText(I)V

    move v2, v0

    move v0, v7

    goto/16 :goto_1

    :pswitch_3
    const v0, 0x7f0f0483    # com.twitter.android.R.string.subtitle_collections

    iget-object v2, p0, Lcom/twitter/android/SearchActivity;->t:Landroid/widget/RadioButton;

    const v3, 0x7f0f03ad    # com.twitter.android.R.string.search_scope_collections

    invoke-virtual {v2, v3}, Landroid/widget/RadioButton;->setText(I)V

    iget-object v2, p0, Lcom/twitter/android/SearchActivity;->u:Landroid/widget/RadioButton;

    const v3, 0x7f0f03b0    # com.twitter.android.R.string.search_scope_lists

    invoke-virtual {v2, v3}, Landroid/widget/RadioButton;->setText(I)V

    move v2, v0

    move v0, v7

    goto/16 :goto_1

    :pswitch_4
    const v0, 0x7f0f0486    # com.twitter.android.R.string.subtitle_news

    iget-object v2, p0, Lcom/twitter/android/SearchActivity;->t:Landroid/widget/RadioButton;

    const v3, 0x7f0f0486    # com.twitter.android.R.string.subtitle_news

    invoke-virtual {v2, v3}, Landroid/widget/RadioButton;->setText(I)V

    move v2, v0

    move v0, v1

    goto/16 :goto_1

    :cond_2
    iget-boolean v0, p0, Lcom/twitter/android/SearchActivity;->e:Z

    if-eqz v0, :cond_3

    const v0, 0x7f0f0485    # com.twitter.android.R.string.subtitle_nearby

    goto/16 :goto_0

    :cond_3
    move v0, v7

    goto/16 :goto_0

    :cond_4
    iget-object v3, p0, Lcom/twitter/android/SearchActivity;->s:Landroid/widget/RadioGroup;

    invoke-virtual {v3, v5}, Landroid/widget/RadioGroup;->setVisibility(I)V

    iget-boolean v3, p0, Lcom/twitter/android/SearchActivity;->g:Z

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/twitter/android/SearchActivity;->t:Landroid/widget/RadioButton;

    :goto_9
    invoke-virtual {v3, v5}, Landroid/widget/RadioButton;->setVisibility(I)V

    invoke-virtual {p5}, Lcom/twitter/android/SearchFragment;->J()V

    goto/16 :goto_2

    :cond_5
    iget-object v3, p0, Lcom/twitter/android/SearchActivity;->u:Landroid/widget/RadioButton;

    goto :goto_9

    :cond_6
    invoke-virtual {p0, v6}, Lcom/twitter/android/SearchActivity;->a(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    :cond_7
    if-eqz v0, :cond_8

    iget-object v2, p0, Lcom/twitter/android/SearchActivity;->u:Landroid/widget/RadioButton;

    invoke-virtual {v2, v5}, Landroid/widget/RadioButton;->setVisibility(I)V

    iget-object v2, p0, Lcom/twitter/android/SearchActivity;->s:Landroid/widget/RadioGroup;

    invoke-virtual {v2, v5}, Landroid/widget/RadioGroup;->setVisibility(I)V

    invoke-virtual {p5}, Lcom/twitter/android/SearchFragment;->J()V

    :goto_a
    iget-object v2, p0, Lcom/twitter/android/SearchActivity;->v:Landroid/widget/TextView;

    invoke-virtual {v4, v2}, Lcom/twitter/internal/android/widget/ToolBar;->setCustomView(Landroid/view/View;)V

    iget-object v2, p0, Lcom/twitter/android/SearchActivity;->v:Landroid/widget/TextView;

    invoke-virtual {v2, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v6}, Lcom/twitter/android/SearchActivity;->a(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    :cond_8
    iget-object v2, p0, Lcom/twitter/android/SearchActivity;->u:Landroid/widget/RadioButton;

    invoke-virtual {v2, v7}, Landroid/widget/RadioButton;->setVisibility(I)V

    iget-object v2, p0, Lcom/twitter/android/SearchActivity;->s:Landroid/widget/RadioGroup;

    invoke-virtual {v2, v7}, Landroid/widget/RadioGroup;->setVisibility(I)V

    invoke-virtual {p5}, Lcom/twitter/android/SearchFragment;->K()V

    goto :goto_a

    :cond_9
    iget-object v4, p0, Lcom/twitter/android/SearchActivity;->t:Landroid/widget/RadioButton;

    goto/16 :goto_4

    :cond_a
    move v2, v7

    goto/16 :goto_5

    :cond_b
    move v6, v7

    goto/16 :goto_6

    :cond_c
    move-object p2, p1

    goto/16 :goto_7

    :cond_d
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f00c0    # com.twitter.android.R.string.composer_hint

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchActivity;->c(Ljava/lang/String;)V

    goto/16 :goto_8

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    new-instance v0, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0, p2}, Lcom/twitter/library/scribe/ScribeLog;->c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/twitter/library/scribe/ScribeLog;->f(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V
    .locals 3

    new-instance v0, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0, p2}, Lcom/twitter/library/scribe/ScribeLog;->c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/twitter/library/scribe/ScribeLog;->f(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    :cond_0
    const-string/jumbo v1, "intent_extra_data_key"

    invoke-virtual {p4, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v1, "intent_extra_data_key"

    invoke-virtual {p4, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/library/scribe/ScribeItem;->d(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method

.method private a(Z)V
    .locals 3

    const/4 v2, 0x2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->b:Lcom/twitter/android/sb;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/sb;->a(Z)V

    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->o:Lcom/twitter/library/widget/SlidingPanel;

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/SlidingPanel;->a(I)V

    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->o:Lcom/twitter/library/widget/SlidingPanel;

    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->e()Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->o:Lcom/twitter/library/widget/SlidingPanel;

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/SlidingPanel;->b(I)V

    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->b:Lcom/twitter/android/sb;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/sb;->a(Z)V

    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->o:Lcom/twitter/library/widget/SlidingPanel;

    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->f()Z

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/SearchActivity;Ljava/lang/String;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/SearchActivity;->e(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/twitter/android/SearchActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private b(Lcom/twitter/android/SearchFragment;)V
    .locals 4

    invoke-virtual {p1}, Lcom/twitter/android/SearchFragment;->L()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/SearchActivity;->c:I

    invoke-virtual {p1}, Lcom/twitter/android/SearchFragment;->F()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/SearchActivity;->d:Z

    invoke-virtual {p1}, Lcom/twitter/android/SearchFragment;->E()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/SearchActivity;->e:Z

    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->b:Lcom/twitter/android/sb;

    iget v1, p0, Lcom/twitter/android/SearchActivity;->c:I

    iget-boolean v2, p0, Lcom/twitter/android/SearchActivity;->d:Z

    iget-boolean v3, p0, Lcom/twitter/android/SearchActivity;->e:Z

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/sb;->a(IZZ)V

    return-void
.end method

.method private b(Z)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->s:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v3}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->t:Landroid/widget/RadioButton;

    iget-object v1, p0, Lcom/twitter/android/SearchActivity;->u:Landroid/widget/RadioButton;

    if-eqz p1, :cond_0

    invoke-virtual {v1, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    :goto_0
    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/RadioButton;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    invoke-virtual {v1, v2, v2, v2, v2}, Landroid/widget/RadioButton;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v1, v3}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setVisibility(I)V

    invoke-virtual {v1, v2}, Landroid/widget/RadioButton;->setVisibility(I)V

    iput-boolean p1, p0, Lcom/twitter/android/SearchActivity;->g:Z

    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->s:Landroid/widget/RadioGroup;

    invoke-virtual {v0, p0}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    return-void

    :cond_0
    invoke-virtual {v0, v4}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/SearchActivity;Ljava/lang/String;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/SearchActivity;->e(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static f()Z
    .locals 4

    const/4 v0, 0x1

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->aq()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const-string/jumbo v1, "android_timeline_gallery_module_1938"

    invoke-static {v1}, Lkk;->b(Ljava/lang/String;)V

    const-string/jumbo v1, "android_timeline_gallery_module_1938"

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "timeline_gallery"

    aput-object v3, v0, v2

    invoke-static {v1, v0}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 3

    const/4 v2, 0x1

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    const v1, 0x7f030127    # com.twitter.android.R.layout.search_activity

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(I)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/z;->d(Z)V

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->c(I)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/z;->b(I)V

    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-static {p0, v0, v1}, Lcom/twitter/library/provider/az;->a(Landroid/content/Context;J)Lcom/twitter/library/provider/az;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SearchActivity;->k:Lcom/twitter/library/provider/az;

    const v0, 0x7f090251    # com.twitter.android.R.id.scope_option

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    invoke-virtual {v0, p0}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    const v1, 0x7f090252    # com.twitter.android.R.id.scope_option_1

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p0, Lcom/twitter/android/SearchActivity;->t:Landroid/widget/RadioButton;

    const v1, 0x7f090253    # com.twitter.android.R.id.scope_option_2

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    iput-object v1, p0, Lcom/twitter/android/SearchActivity;->u:Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/twitter/android/SearchActivity;->s:Landroid/widget/RadioGroup;

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030130    # com.twitter.android.R.layout.search_tool_bar

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/SearchActivity;->v:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->v:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/support/v4/app/FragmentManager;->addOnBackStackChangedListener(Landroid/support/v4/app/FragmentManager$OnBackStackChangedListener;)V

    new-instance v0, Lcom/twitter/android/rz;

    invoke-direct {v0, p0, v4}, Lcom/twitter/android/rz;-><init>(Lcom/twitter/android/SearchActivity;Lcom/twitter/android/rv;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchActivity;->a(Lcom/twitter/library/client/j;)V

    new-instance v0, Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/library/scribe/ScribeAssociation;-><init>()V

    const-string/jumbo v1, "search"

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SearchActivity;->l:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->L()Lcom/twitter/android/client/bn;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/SearchActivity;->l:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/bn;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/android/client/bn;

    const v0, 0x7f09020c    # com.twitter.android.R.id.sliding_panel

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/SlidingPanel;

    new-instance v1, Lcom/twitter/android/sb;

    invoke-direct {v1, v0, p0}, Lcom/twitter/android/sb;-><init>(Lcom/twitter/library/widget/SlidingPanel;Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    iget-object v2, v1, Lcom/twitter/android/sb;->n:Landroid/view/View;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, v1, Lcom/twitter/android/sb;->o:Landroid/view/View;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, v1, Lcom/twitter/android/sb;->p:Landroid/view/View;

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iput-object v1, p0, Lcom/twitter/android/SearchActivity;->b:Lcom/twitter/android/sb;

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/SlidingPanel;->b(I)V

    new-instance v2, Lcom/twitter/android/sa;

    invoke-direct {v2, v0, v1}, Lcom/twitter/android/sa;-><init>(Lcom/twitter/library/widget/SlidingPanel;Lcom/twitter/android/sb;)V

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/SlidingPanel;->setPanelSlideListener(Lcom/twitter/library/widget/o;)V

    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    iput-object v0, p0, Lcom/twitter/android/SearchActivity;->o:Lcom/twitter/library/widget/SlidingPanel;

    const v0, 0x7f0900e1    # com.twitter.android.R.id.dock

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/DockLayout;

    iput-object v0, p0, Lcom/twitter/android/SearchActivity;->w:Lcom/twitter/internal/android/widget/DockLayout;

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v3, v4, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    iput-boolean v3, p0, Lcom/twitter/android/SearchActivity;->h:Z

    if-nez p1, :cond_0

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/SearchActivity;->m:Ljava/util/HashSet;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/SearchActivity;->n:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0, v3}, Lcom/twitter/android/SearchActivity;->a(Landroid/content/Intent;Z)V

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->k:Lcom/twitter/library/provider/az;

    iget-object v1, p0, Lcom/twitter/android/SearchActivity;->n:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/provider/az;->a(Ljava/util/Collection;)V

    return-void

    :cond_0
    const-string/jumbo v0, "search_saved_queries"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    iput-object v0, p0, Lcom/twitter/android/SearchActivity;->m:Ljava/util/HashSet;

    const-string/jumbo v0, "search_ids"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    iput-object v0, p0, Lcom/twitter/android/SearchActivity;->n:Ljava/util/HashMap;

    const-string/jumbo v0, "filter_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/SearchActivity;->c:I

    const-string/jumbo v0, "filter_following"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/SearchActivity;->d:Z

    const-string/jumbo v0, "filter_near"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/SearchActivity;->e:Z

    const-string/jumbo v0, "filter_scope_alt"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/SearchActivity;->g:Z

    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->u:Landroid/widget/RadioButton;

    iget-boolean v1, p0, Lcom/twitter/android/SearchActivity;->g:Z

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setChecked(Z)V

    const-string/jumbo v0, "state_panel_maximized"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/SearchActivity;->i:Z

    goto :goto_0
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->m:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x2

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/SearchActivity;->m:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->V()V

    goto :goto_0
.end method

.method protected a(Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lcom/twitter/internal/android/widget/ToolBar;)Z

    const v0, 0x7f090309    # com.twitter.android.R.id.toolbar_search

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v3

    iget-boolean v0, p0, Lcom/twitter/android/SearchActivity;->q:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/android/SearchActivity;->r:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lhn;->b(Z)Lhn;

    const v0, 0x7f09032a    # com.twitter.android.R.id.menu_search_filter_slideup

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v3

    iget-boolean v0, p0, Lcom/twitter/android/SearchActivity;->q:Z

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lhn;->b(Z)Lhn;

    const v0, 0x7f09032f    # com.twitter.android.R.id.menu_share

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v3

    iget-boolean v0, p0, Lcom/twitter/android/SearchActivity;->q:Z

    if-nez v0, :cond_3

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Lhn;->b(Z)Lhn;

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->a:Lcom/twitter/android/SearchFragment;

    if-eqz v0, :cond_0

    const v3, 0x7f09032c    # com.twitter.android.R.id.menu_search_result_delete

    invoke-virtual {p1, v3}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v3

    const v4, 0x7f09032b    # com.twitter.android.R.id.menu_search_result_save

    invoke-virtual {p1, v4}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v4

    iget-boolean v5, p0, Lcom/twitter/android/SearchActivity;->q:Z

    if-eqz v5, :cond_4

    invoke-virtual {v3, v2}, Lhn;->b(Z)Lhn;

    invoke-virtual {v4, v2}, Lhn;->b(Z)Lhn;

    :cond_0
    :goto_3
    return v1

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    invoke-virtual {v0}, Lcom/twitter/android/SearchFragment;->C()Ljava/lang/String;

    move-result-object v0

    iget-object v5, p0, Lcom/twitter/android/SearchActivity;->m:Ljava/util/HashSet;

    invoke-virtual {v5, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    invoke-virtual {v3, v5}, Lhn;->b(Z)Lhn;

    move-result-object v0

    invoke-virtual {v0, v5}, Lhn;->c(Z)Lhn;

    if-nez v5, :cond_6

    move v0, v1

    :goto_4
    invoke-virtual {v4, v0}, Lhn;->b(Z)Lhn;

    move-result-object v0

    if-nez v5, :cond_5

    move v2, v1

    :cond_5
    invoke-virtual {v0, v2}, Lhn;->c(Z)Lhn;

    goto :goto_3

    :cond_6
    move v0, v2

    goto :goto_4
.end method

.method protected a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f11001e    # com.twitter.android.R.menu.search_results

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    :cond_0
    const v0, 0x7f110023    # com.twitter.android.R.menu.toolbar_share

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    const/4 v0, 0x1

    return v0
.end method

.method public a(Lhn;)Z
    .locals 7

    const/4 v6, 0x0

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/twitter/android/SearchActivity;->a:Lcom/twitter/android/SearchFragment;

    invoke-virtual {v1}, Lcom/twitter/android/SearchFragment;->C()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lhn;->a()I

    move-result v3

    const v4, 0x7f09032b    # com.twitter.android.R.id.menu_search_result_save

    if-ne v3, v4, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/twitter/android/client/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/SearchActivity;->d(Ljava/lang/String;)V

    invoke-virtual {p1, v6}, Lhn;->c(Z)Lhn;

    :goto_0
    return v0

    :cond_0
    const v4, 0x7f09032c    # com.twitter.android.R.id.menu_search_result_delete

    if-ne v3, v4, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/twitter/android/client/c;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/SearchActivity;->d(Ljava/lang/String;)V

    invoke-virtual {p1, v6}, Lhn;->c(Z)Lhn;

    goto :goto_0

    :cond_1
    const v4, 0x7f09032a    # com.twitter.android.R.id.menu_search_filter_slideup

    if-ne v3, v4, :cond_2

    iget-object v1, p0, Lcom/twitter/android/SearchActivity;->a:Lcom/twitter/android/SearchFragment;

    invoke-direct {p0, v1}, Lcom/twitter/android/SearchActivity;->b(Lcom/twitter/android/SearchFragment;)V

    iget-boolean v1, p0, Lcom/twitter/android/SearchActivity;->p:Z

    invoke-direct {p0, v1}, Lcom/twitter/android/SearchActivity;->a(Z)V

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    new-array v4, v0, [Ljava/lang/String;

    const-string/jumbo v5, "search:universal:filter_sheet::impression"

    aput-object v5, v4, v6

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const v4, 0x7f09032f    # com.twitter.android.R.id.menu_share

    if-ne v3, v4, :cond_3

    invoke-virtual {v1}, Lcom/twitter/android/SearchFragment;->q_()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v2, v1}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lhn;)Z

    move-result v0

    goto :goto_0
.end method

.method protected c_()Ljava/lang/String;
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->a:Lcom/twitter/android/SearchFragment;

    invoke-virtual {v0}, Lcom/twitter/android/SearchFragment;->D()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1

    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x23

    if-eq v1, v2, :cond_0

    invoke-virtual {v0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v1

    const/16 v2, 0x24

    if-ne v1, v2, :cond_1

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->c_()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected k_()[I
    .locals 1

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    return-object v0

    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public onBackStackChanged()V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->getBackStackEntryCount()I

    move-result v1

    if-ltz v1, :cond_0

    const-string/jumbo v1, "search_fragment"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/SearchFragment;

    invoke-direct {p0, v0}, Lcom/twitter/android/SearchActivity;->a(Lcom/twitter/android/SearchFragment;)V

    :cond_0
    return-void
.end method

.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 9

    const/4 v1, 0x0

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/twitter/android/SearchActivity;->a:Lcom/twitter/android/SearchFragment;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v3}, Lcom/twitter/android/SearchFragment;->F()Z

    move-result v0

    invoke-virtual {v3}, Lcom/twitter/android/SearchFragment;->E()Z

    move-result v4

    invoke-virtual {p1}, Landroid/widget/RadioGroup;->getId()I

    move-result v5

    const v6, 0x7f090255    # com.twitter.android.R.id.search_filter

    if-ne v5, v6, :cond_8

    const v0, 0x7f090257    # com.twitter.android.R.id.filter_photos

    if-ne p2, v0, :cond_3

    const/4 v0, 0x3

    :goto_1
    iget v4, p0, Lcom/twitter/android/SearchActivity;->c:I

    if-eq v4, v0, :cond_2

    iput v0, p0, Lcom/twitter/android/SearchActivity;->c:I

    iput-boolean v2, p0, Lcom/twitter/android/SearchActivity;->f:Z

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->o:Lcom/twitter/library/widget/SlidingPanel;

    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->getPanelState()I

    move-result v0

    const/4 v2, 0x4

    if-eq v0, v2, :cond_0

    invoke-direct {p0, v3, v1}, Lcom/twitter/android/SearchActivity;->a(Lcom/twitter/android/SearchFragment;Z)V

    goto :goto_0

    :cond_3
    const v0, 0x7f090258    # com.twitter.android.R.id.filter_videos

    if-ne p2, v0, :cond_4

    const/4 v0, 0x5

    goto :goto_1

    :cond_4
    const v0, 0x7f090259    # com.twitter.android.R.id.filter_news

    if-ne p2, v0, :cond_5

    const/4 v0, 0x6

    goto :goto_1

    :cond_5
    const v0, 0x7f09025a    # com.twitter.android.R.id.filter_people

    if-ne p2, v0, :cond_6

    const/4 v0, 0x2

    goto :goto_1

    :cond_6
    const v0, 0x7f09025b    # com.twitter.android.R.id.filter_timelines

    if-ne p2, v0, :cond_7

    const/4 v0, 0x7

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v4

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v5

    invoke-virtual {v5}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v5

    new-array v7, v2, [Ljava/lang/String;

    const-string/jumbo v8, "search::filter_sheet:timeline_filter:click"

    aput-object v8, v7, v1

    invoke-virtual {v4, v5, v6, v7}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_1

    :cond_7
    move v0, v1

    goto :goto_1

    :cond_8
    const v3, 0x7f09025c    # com.twitter.android.R.id.search_filter_social

    if-ne v5, v3, :cond_a

    const v3, 0x7f09025e    # com.twitter.android.R.id.filter_follows

    if-ne p2, v3, :cond_9

    move v1, v2

    :cond_9
    if-eq v0, v1, :cond_0

    iput-boolean v1, p0, Lcom/twitter/android/SearchActivity;->d:Z

    iput-boolean v2, p0, Lcom/twitter/android/SearchActivity;->f:Z

    goto :goto_0

    :cond_a
    const v0, 0x7f09025f    # com.twitter.android.R.id.search_filter_geo

    if-ne v5, v0, :cond_d

    const v0, 0x7f090261    # com.twitter.android.R.id.filter_geo

    if-ne p2, v0, :cond_b

    move v0, v2

    :goto_2
    if-eq v4, v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v3

    if-eqz v0, :cond_c

    invoke-virtual {v3}, Lcom/twitter/android/client/c;->e()Z

    move-result v4

    if-nez v4, :cond_c

    invoke-virtual {p0, v2}, Lcom/twitter/android/SearchActivity;->showDialog(I)V

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    new-array v0, v2, [Ljava/lang/String;

    const-string/jumbo v2, "location_prompt::::impression"

    aput-object v2, v0, v1

    invoke-virtual {v3, v4, v5, v0}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_b
    move v0, v1

    goto :goto_2

    :cond_c
    iput-boolean v0, p0, Lcom/twitter/android/SearchActivity;->e:Z

    iput-boolean v2, p0, Lcom/twitter/android/SearchActivity;->f:Z

    goto/16 :goto_0

    :cond_d
    const v0, 0x7f090251    # com.twitter.android.R.id.scope_option

    if-ne v5, v0, :cond_0

    const v0, 0x7f090253    # com.twitter.android.R.id.scope_option_2

    if-ne p2, v0, :cond_e

    move v1, v2

    :cond_e
    iget-boolean v0, p0, Lcom/twitter/android/SearchActivity;->g:Z

    if-eq v0, v1, :cond_0

    iput-boolean v1, p0, Lcom/twitter/android/SearchActivity;->g:Z

    iput-boolean v2, p0, Lcom/twitter/android/SearchActivity;->f:Z

    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->a:Lcom/twitter/android/SearchFragment;

    invoke-direct {p0, v0, v2}, Lcom/twitter/android/SearchActivity;->a(Lcom/twitter/android/SearchFragment;Z)V

    goto/16 :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    const/4 v3, 0x1

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f090212    # com.twitter.android.R.id.query

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->J()Z

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->L()Lcom/twitter/android/client/bn;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/SearchActivity;->v:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/bn;->a(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v1, 0x7f090262    # com.twitter.android.R.id.search_filter_more

    if-ne v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "search:universal:filter_sheet:more:click"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-direct {p0, v5}, Lcom/twitter/android/SearchActivity;->a(Z)V

    goto :goto_0

    :cond_2
    const v1, 0x7f090265    # com.twitter.android.R.id.search_filter_done

    if-ne v0, v1, :cond_3

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "search:universal:filter_sheet::apply"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->a:Lcom/twitter/android/SearchFragment;

    invoke-direct {p0, v0, v5}, Lcom/twitter/android/SearchActivity;->a(Lcom/twitter/android/SearchFragment;Z)V

    goto :goto_0

    :cond_3
    const v1, 0x7f090264    # com.twitter.android.R.id.search_filter_cancel

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "search:universal:filter_sheet::cancel"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->o:Lcom/twitter/library/widget/SlidingPanel;

    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->c()Z

    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->a:Lcom/twitter/android/SearchFragment;

    invoke-direct {p0, v0}, Lcom/twitter/android/SearchActivity;->b(Lcom/twitter/android/SearchFragment;)V

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 2

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/rv;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/rv;-><init>(Lcom/twitter/android/SearchActivity;Lcom/twitter/android/client/c;)V

    invoke-virtual {v0, p0, v1}, Lcom/twitter/android/client/c;->a(Landroid/app/Activity;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7

    new-instance v0, Landroid/support/v4/content/CursorLoader;

    sget-object v1, Lcom/twitter/library/provider/ap;->a:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/twitter/library/provider/w;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/twitter/android/ry;->a:[Ljava/lang/String;

    const-string/jumbo v4, "type=? AND latitude IS NULL AND longitude IS NULL"

    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v6, 0x6

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    const-string/jumbo v6, "query_id DESC, time ASC"

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method protected onDestroy()V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/support/v4/app/FragmentManager;->removeOnBackStackChangedListener(Landroid/support/v4/app/FragmentManager$OnBackStackChangedListener;)V

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->b()Lcom/twitter/library/client/Session$LoginStatus;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/client/Session$LoginStatus;->c:Lcom/twitter/library/client/Session$LoginStatus;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->k:Lcom/twitter/library/provider/az;

    iget-object v1, p0, Lcom/twitter/android/SearchActivity;->n:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/provider/az;->b(Ljava/util/Collection;)V

    :cond_0
    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onDestroy()V

    return-void
.end method

.method public onGlobalLayout()V
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/SearchActivity;->o:Lcom/twitter/library/widget/SlidingPanel;

    const v0, 0x7f090263    # com.twitter.android.R.id.search_filter_header

    invoke-virtual {v3, v0}, Lcom/twitter/library/widget/SlidingPanel;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    const v4, 0x7f090254    # com.twitter.android.R.id.search_filter_content

    invoke-virtual {v3, v4}, Lcom/twitter/library/widget/SlidingPanel;->findViewById(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int/2addr v4, v0

    invoke-virtual {v3, v4}, Lcom/twitter/library/widget/SlidingPanel;->setPanelPreviewHeight(I)V

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v5

    invoke-interface {v5}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/Display;->getHeight()I

    move-result v5

    add-int/2addr v0, v4

    if-le v5, v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/SearchActivity;->p:Z

    invoke-virtual {v3}, Lcom/twitter/library/widget/SlidingPanel;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    invoke-virtual {v3}, Lcom/twitter/library/widget/SlidingPanel;->getPanelState()I

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/SearchActivity;->i:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/twitter/android/SearchActivity;->p:Z

    if-eqz v0, :cond_2

    :goto_1
    invoke-direct {p0, v1}, Lcom/twitter/android/SearchActivity;->a(Z)V

    invoke-virtual {v3}, Lcom/twitter/library/widget/SlidingPanel;->requestLayout()V

    :cond_0
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/SearchActivity;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->m:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->onNewIntent(Landroid/content/Intent;)V

    invoke-virtual {p0, p1}, Lcom/twitter/android/SearchActivity;->setIntent(Landroid/content/Intent;)V

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/SearchActivity;->a(Landroid/content/Intent;Z)V

    return-void
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onResume()V

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string/jumbo v1, "search_fragment"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/SearchFragment;

    invoke-direct {p0, v0}, Lcom/twitter/android/SearchActivity;->a(Lcom/twitter/android/SearchFragment;)V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "search_saved_queries"

    iget-object v1, p0, Lcom/twitter/android/SearchActivity;->m:Ljava/util/HashSet;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const-string/jumbo v0, "search_ids"

    iget-object v1, p0, Lcom/twitter/android/SearchActivity;->n:Ljava/util/HashMap;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const-string/jumbo v0, "filter_type"

    iget v1, p0, Lcom/twitter/android/SearchActivity;->c:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "filter_following"

    iget-boolean v1, p0, Lcom/twitter/android/SearchActivity;->d:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "filter_near"

    iget-boolean v1, p0, Lcom/twitter/android/SearchActivity;->e:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "filter_scope_alt"

    iget-boolean v1, p0, Lcom/twitter/android/SearchActivity;->g:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v1, "state_panel_maximized"

    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->o:Lcom/twitter/library/widget/SlidingPanel;

    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->getPanelState()I

    move-result v0

    const/4 v2, 0x4

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onSearchRequested()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->o:Lcom/twitter/library/widget/SlidingPanel;

    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->getPanelState()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/SearchActivity;->o:Lcom/twitter/library/widget/SlidingPanel;

    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->c()Z

    :cond_0
    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onSearchRequested()Z

    move-result v0

    return v0
.end method

.method protected s_()Landroid/content/Intent;
    .locals 4

    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    sget-object v0, Lcom/twitter/android/SearchActivity;->j:Ljava/util/HashMap;

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "in_back_stack"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    :goto_0
    return-object v0

    :pswitch_0
    invoke-virtual {p0}, Lcom/twitter/android/SearchActivity;->aa()Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
