.class Lcom/twitter/android/qu;
.super Landroid/os/AsyncTask;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/ProfileFragment;

.field private final b:Ljava/lang/ref/WeakReference;


# direct methods
.method public constructor <init>(Lcom/twitter/android/ProfileFragment;)V
    .locals 2

    iput-object p1, p0, Lcom/twitter/android/qu;->a:Lcom/twitter/android/ProfileFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/qu;->b:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Landroid/graphics/Bitmap;
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/qu;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    iget-object v2, p0, Lcom/twitter/android/qu;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v2, v2, Lcom/twitter/android/ProfileFragment;->B:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/twitter/android/ProfileFragment;->a(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v3

    const-wide/32 v5, 0x927c0

    add-long/2addr v3, v5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-lez v3, :cond_2

    iget-object v1, p0, Lcom/twitter/android/qu;->a:Lcom/twitter/android/ProfileFragment;

    iget-wide v3, v1, Lcom/twitter/android/ProfileFragment;->I:J

    invoke-static {v0, v3, v4}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;J)Landroid/graphics/Bitmap;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-static {v0, v2}, Lcom/twitter/android/ProfileFragment;->c(Landroid/content/Context;Ljava/lang/String;)V

    :cond_1
    move-object v0, v1

    goto :goto_0

    :cond_2
    invoke-static {v0, v2}, Lcom/twitter/android/ProfileFragment;->c(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/twitter/android/qu;->a:Lcom/twitter/android/ProfileFragment;

    iget-wide v2, v2, Lcom/twitter/android/ProfileFragment;->I:J

    invoke-static {v0, v2, v3}, Lcom/twitter/library/util/Util;->e(Landroid/content/Context;J)Z

    move-object v0, v1

    goto :goto_0
.end method

.method protected a(Landroid/graphics/Bitmap;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/qu;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/qu;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v1}, Lcom/twitter/android/ProfileFragment;->U()Z

    move-result v1

    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_2

    iget-object v1, p0, Lcom/twitter/android/qu;->a:Lcom/twitter/android/ProfileFragment;

    new-instance v2, Lcom/twitter/library/util/m;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/twitter/library/util/m;-><init>(Ljava/lang/String;)V

    iput-object v2, v1, Lcom/twitter/android/ProfileFragment;->h:Lcom/twitter/library/util/m;

    iget-object v1, p0, Lcom/twitter/android/qu;->a:Lcom/twitter/android/ProfileFragment;

    iget-object v1, v1, Lcom/twitter/android/ProfileFragment;->j:Lcom/twitter/android/widget/ProfileHeader;

    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-direct {v2, v0, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/ProfileHeader;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/twitter/android/qu;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/ProfileFragment;->a(Landroid/content/res/Resources;)V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/qu;->a([Ljava/lang/Void;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/twitter/android/qu;->a(Landroid/graphics/Bitmap;)V

    return-void
.end method
