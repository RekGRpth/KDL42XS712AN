.class final Lcom/twitter/android/uy;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/client/c;

.field final synthetic b:Lcom/twitter/android/ul;

.field final synthetic c:Lcom/twitter/library/client/aa;

.field final synthetic d:Lcom/twitter/library/scribe/ScribeAssociation;


# direct methods
.method constructor <init>(Lcom/twitter/android/client/c;Lcom/twitter/android/ul;Lcom/twitter/library/client/aa;Lcom/twitter/library/scribe/ScribeAssociation;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/uy;->a:Lcom/twitter/android/client/c;

    iput-object p2, p0, Lcom/twitter/android/uy;->b:Lcom/twitter/android/ul;

    iput-object p3, p0, Lcom/twitter/android/uy;->c:Lcom/twitter/library/client/aa;

    iput-object p4, p0, Lcom/twitter/android/uy;->d:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/uy;->a:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/uy;->b:Lcom/twitter/android/ul;

    iget-wide v1, v1, Lcom/twitter/android/ul;->c:J

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/client/c;->r(J)Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/uy;->b:Lcom/twitter/android/ul;

    iget-object v0, v0, Lcom/twitter/android/ul;->b:Lcom/twitter/library/scribe/ScribeItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/uy;->b:Lcom/twitter/android/ul;

    iget-object v0, v0, Lcom/twitter/android/ul;->b:Lcom/twitter/library/scribe/ScribeItem;

    iget-object v0, v0, Lcom/twitter/library/scribe/ScribeItem;->x:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/uy;->a:Lcom/twitter/android/client/c;

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v2, p0, Lcom/twitter/android/uy;->c:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/uy;->d:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v4}, Lcom/twitter/library/scribe/ScribeAssociation;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/twitter/android/uy;->d:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v4}, Lcom/twitter/library/scribe/ScribeAssociation;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/twitter/android/uy;->b:Lcom/twitter/android/ul;

    iget-object v4, v4, Lcom/twitter/android/ul;->b:Lcom/twitter/library/scribe/ScribeItem;

    iget-object v4, v4, Lcom/twitter/library/scribe/ScribeItem;->x:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const/4 v4, 0x0

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string/jumbo v4, "dismiss"

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/uy;->b:Lcom/twitter/android/ul;

    iget-object v2, v2, Lcom/twitter/android/ul;->b:Lcom/twitter/library/scribe/ScribeItem;

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_0
    return-void
.end method
