.class Lcom/twitter/android/pi;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/PostStorage$MediaItem;

.field final synthetic b:Lcom/twitter/android/PostActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/PostActivity;Lcom/twitter/android/PostStorage$MediaItem;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/pi;->b:Lcom/twitter/android/PostActivity;

    iput-object p2, p0, Lcom/twitter/android/pi;->a:Lcom/twitter/android/PostStorage$MediaItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/pi;->b:Lcom/twitter/android/PostActivity;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/android/pi;->b:Lcom/twitter/android/PostActivity;

    const-class v3, Lcom/twitter/android/MediaTagActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "uri"

    iget-object v3, p0, Lcom/twitter/android/pi;->a:Lcom/twitter/android/PostStorage$MediaItem;

    iget-object v3, v3, Lcom/twitter/android/PostStorage$MediaItem;->b:Landroid/net/Uri;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "photo_tags"

    iget-object v3, p0, Lcom/twitter/android/pi;->a:Lcom/twitter/android/PostStorage$MediaItem;

    iget-object v3, v3, Lcom/twitter/android/PostStorage$MediaItem;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v1

    const/16 v2, 0x201

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/PostActivity;->startActivityForResult(Landroid/content/Intent;I)V

    iget-object v0, p0, Lcom/twitter/android/pi;->b:Lcom/twitter/android/PostActivity;

    invoke-static {v0}, Lcom/twitter/android/PostActivity;->G(Lcom/twitter/android/PostActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/pi;->b:Lcom/twitter/android/PostActivity;

    invoke-static {v1}, Lcom/twitter/android/PostActivity;->F(Lcom/twitter/android/PostActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "composition:::media_tag_prompt:click"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    return-void
.end method
