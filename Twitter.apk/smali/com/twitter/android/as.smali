.class Lcom/twitter/android/as;
.super Lcom/twitter/android/widget/ah;
.source "Twttr"


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/twitter/android/client/c;

.field private final d:Lcom/twitter/android/to;

.field private final e:Lcom/twitter/android/to;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/android/client/c;Lcom/twitter/android/ar;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x3

    invoke-direct {p0, p3, v0}, Lcom/twitter/android/widget/ah;-><init>(Landroid/widget/ListAdapter;I)V

    iput-object p1, p0, Lcom/twitter/android/as;->b:Landroid/content/Context;

    iput-object p2, p0, Lcom/twitter/android/as;->c:Lcom/twitter/android/client/c;

    new-instance v0, Lcom/twitter/android/to;

    const v1, 0x7f0f00db    # com.twitter.android.R.string.copy_backup_code_to_clipboard

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/to;-><init>(Ljava/lang/String;Landroid/content/Intent;)V

    iput-object v0, p0, Lcom/twitter/android/as;->d:Lcom/twitter/android/to;

    new-instance v0, Lcom/twitter/android/to;

    const v1, 0x7f0f01b3    # com.twitter.android.R.string.generate_new_backup_code

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/to;-><init>(Ljava/lang/String;Landroid/content/Intent;)V

    iput-object v0, p0, Lcom/twitter/android/as;->e:Lcom/twitter/android/to;

    return-void
.end method


# virtual methods
.method protected a(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    const v0, 0x7f030136    # com.twitter.android.R.layout.section_simple_row_view

    iget-object v1, p0, Lcom/twitter/android/as;->d:Lcom/twitter/android/to;

    iget-object v2, p0, Lcom/twitter/android/as;->c:Lcom/twitter/android/client/c;

    invoke-virtual {v2}, Lcom/twitter/android/client/c;->V()F

    move-result v2

    invoke-static {v0, p1, p2, v1, v2}, Lcom/twitter/android/tp;->a(ILandroid/view/View;Landroid/view/ViewGroup;Lcom/twitter/android/to;F)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected a()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/as;->d:Lcom/twitter/android/to;

    return-object v0
.end method

.method protected b(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    const v0, 0x7f030136    # com.twitter.android.R.layout.section_simple_row_view

    iget-object v1, p0, Lcom/twitter/android/as;->e:Lcom/twitter/android/to;

    iget-object v2, p0, Lcom/twitter/android/as;->c:Lcom/twitter/android/client/c;

    invoke-virtual {v2}, Lcom/twitter/android/client/c;->V()F

    move-result v2

    invoke-static {v0, p1, p2, v1, v2}, Lcom/twitter/android/tp;->a(ILandroid/view/View;Landroid/view/ViewGroup;Lcom/twitter/android/to;F)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected b()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/as;->e:Lcom/twitter/android/to;

    return-object v0
.end method
