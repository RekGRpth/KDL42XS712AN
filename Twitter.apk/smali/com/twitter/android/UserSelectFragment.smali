.class public Lcom/twitter/android/UserSelectFragment;
.super Landroid/support/v4/app/Fragment;
.source "Twttr"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Lcom/twitter/android/widget/cr;
.implements Lcom/twitter/library/util/ar;


# static fields
.field private static final d:Landroid/view/View$OnLongClickListener;


# instance fields
.field protected a:Lcom/twitter/android/UserSelectFragment$UserSelectEditText;

.field protected b:Landroid/widget/ListView;

.field protected c:Lcom/twitter/android/zm;

.field private final e:Lcom/twitter/android/zv;

.field private final f:Lcom/twitter/android/zu;

.field private g:Lcom/twitter/android/client/c;

.field private h:Lcom/twitter/library/client/aa;

.field private i:I

.field private j:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/twitter/android/zn;

    invoke-direct {v0}, Lcom/twitter/android/zn;-><init>()V

    sput-object v0, Lcom/twitter/android/UserSelectFragment;->d:Landroid/view/View$OnLongClickListener;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    new-instance v0, Lcom/twitter/android/zv;

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/zv;-><init>(Lcom/twitter/android/UserSelectFragment;Lcom/twitter/android/zn;)V

    iput-object v0, p0, Lcom/twitter/android/UserSelectFragment;->e:Lcom/twitter/android/zv;

    new-instance v0, Lcom/twitter/android/zu;

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/zu;-><init>(Lcom/twitter/android/UserSelectFragment;Lcom/twitter/android/zn;)V

    iput-object v0, p0, Lcom/twitter/android/UserSelectFragment;->f:Lcom/twitter/android/zu;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/UserSelectFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/UserSelectFragment;->h()V

    return-void
.end method

.method private a(Ljava/util/List;Ljava/lang/String;)V
    .locals 9

    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/android/UserSelectFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/SelectedUser;

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v5

    new-instance v6, Lcom/twitter/android/zt;

    invoke-direct {v6, v0, v2}, Lcom/twitter/android/zt;-><init>(Lcom/twitter/library/api/SelectedUser;Landroid/content/res/Resources;)V

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, v0, Lcom/twitter/library/api/SelectedUser;->name:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    const/16 v8, 0x21

    invoke-virtual {v1, v6, v5, v7, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    iget-wide v5, v0, Lcom/twitter/library/api/SelectedUser;->userId:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/UserSelectFragment;->c:Lcom/twitter/android/zm;

    invoke-virtual {v0, v3}, Lcom/twitter/android/zm;->a(Ljava/util/Set;)V

    if-eqz p2, :cond_1

    invoke-virtual {v1, p2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_1
    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    invoke-direct {p0, v1, v0}, Lcom/twitter/android/UserSelectFragment;->b(Ljava/lang/CharSequence;I)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/UserSelectFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/UserSelectFragment;->j:Z

    return p1
.end method

.method private b(Ljava/lang/CharSequence;I)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/UserSelectFragment;->a:Lcom/twitter/android/UserSelectFragment$UserSelectEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/SuggestionEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/SuggestionEditText;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, p2}, Lcom/twitter/android/widget/SuggestionEditText;->setSelection(I)V

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/SuggestionEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    invoke-direct {p0}, Lcom/twitter/android/UserSelectFragment;->j()V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/UserSelectFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/UserSelectFragment;->j:Z

    return v0
.end method

.method private h()V
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/android/UserSelectFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p0}, Lcom/twitter/android/UserSelectFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    return-void
.end method

.method private i()V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/UserSelectFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/twitter/android/UserSelectFragment;->a:Lcom/twitter/android/UserSelectFragment$UserSelectEditText;

    invoke-virtual {v0, v1}, Landroid/view/inputmethod/InputMethodManager;->restartInput(Landroid/view/View;)V

    return-void
.end method

.method private j()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/UserSelectFragment;->a:Lcom/twitter/android/UserSelectFragment$UserSelectEditText;

    invoke-virtual {v0}, Lcom/twitter/android/UserSelectFragment$UserSelectEditText;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v1, p0, Lcom/twitter/android/UserSelectFragment;->a:Lcom/twitter/android/UserSelectFragment$UserSelectEditText;

    new-instance v2, Lcom/twitter/android/zs;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/zs;-><init>(Lcom/twitter/android/UserSelectFragment;Landroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual {v1, v2}, Lcom/twitter/android/UserSelectFragment$UserSelectEditText;->post(Ljava/lang/Runnable;)Z

    return-void
.end method


# virtual methods
.method protected final a(Landroid/view/LayoutInflater;ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    const/4 v0, 0x0

    invoke-virtual {p1, p2, p3, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/zo;

    invoke-direct {v1, p0, v2}, Lcom/twitter/android/zo;-><init>(Lcom/twitter/android/UserSelectFragment;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    const v0, 0x7f09011f    # com.twitter.android.R.id.suggestion_list_view

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iget-object v1, p0, Lcom/twitter/android/UserSelectFragment;->c:Lcom/twitter/android/zm;

    new-instance v3, Lcom/twitter/android/zp;

    invoke-direct {v3, p0, v0}, Lcom/twitter/android/zp;-><init>(Lcom/twitter/android/UserSelectFragment;Landroid/widget/ListView;)V

    invoke-virtual {v1, v3}, Lcom/twitter/android/zm;->a(Lcom/twitter/android/aq;)V

    new-instance v1, Lcom/twitter/android/zr;

    invoke-direct {v1, p0}, Lcom/twitter/android/zr;-><init>(Lcom/twitter/android/UserSelectFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    iput-object v0, p0, Lcom/twitter/android/UserSelectFragment;->b:Landroid/widget/ListView;

    const v1, 0x7f09011e    # com.twitter.android.R.id.user_select_text

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/UserSelectFragment$UserSelectEditText;

    invoke-static {}, Lcom/twitter/android/widget/ar;->a()Landroid/text/Editable$Factory;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/twitter/android/UserSelectFragment$UserSelectEditText;->setEditableFactory(Landroid/text/Editable$Factory;)V

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lcom/twitter/android/UserSelectFragment$UserSelectEditText;->setFilteringEnabled(Z)V

    invoke-virtual {v1, p0}, Lcom/twitter/android/UserSelectFragment$UserSelectEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    invoke-virtual {v1, p0}, Lcom/twitter/android/UserSelectFragment$UserSelectEditText;->setSuggestionListener(Lcom/twitter/android/widget/cr;)V

    sget-object v3, Lcom/twitter/android/UserSelectFragment;->d:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v1, v3}, Lcom/twitter/android/UserSelectFragment$UserSelectEditText;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    invoke-static {}, Lcom/twitter/android/widget/at;->a()Landroid/text/method/MovementMethod;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/twitter/android/UserSelectFragment$UserSelectEditText;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    iget-object v3, p0, Lcom/twitter/android/UserSelectFragment;->g:Lcom/twitter/android/client/c;

    iget-boolean v3, v3, Lcom/twitter/android/client/c;->f:Z

    if-eqz v3, :cond_0

    const/4 v3, 0x5

    invoke-virtual {v1, v3}, Lcom/twitter/android/UserSelectFragment$UserSelectEditText;->setGravity(I)V

    :cond_0
    invoke-virtual {v1, v0}, Lcom/twitter/android/UserSelectFragment$UserSelectEditText;->setSuggestionList(Landroid/widget/ListView;)V

    iget-object v0, p0, Lcom/twitter/android/UserSelectFragment;->c:Lcom/twitter/android/zm;

    invoke-virtual {v1, v0}, Lcom/twitter/android/UserSelectFragment$UserSelectEditText;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/twitter/android/UserSelectFragment;->f:Lcom/twitter/android/zu;

    iget-object v3, p0, Lcom/twitter/android/UserSelectFragment;->g:Lcom/twitter/android/client/c;

    invoke-virtual {v3}, Lcom/twitter/android/client/c;->B()J

    move-result-wide v3

    invoke-virtual {v1, v0, v3, v4}, Lcom/twitter/android/UserSelectFragment$UserSelectEditText;->a(Landroid/widget/MultiAutoCompleteTextView$Tokenizer;J)V

    iput-object v1, p0, Lcom/twitter/android/UserSelectFragment;->a:Lcom/twitter/android/UserSelectFragment$UserSelectEditText;

    return-object v2
.end method

.method protected a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    const v0, 0x7f0300a5    # com.twitter.android.R.layout.list_fragment

    invoke-virtual {p0, p1, v0, p2}, Lcom/twitter/android/UserSelectFragment;->a(Landroid/view/LayoutInflater;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(II)V
    .locals 0

    return-void
.end method

.method protected a(JLjava/lang/String;Ljava/lang/String;)V
    .locals 11

    const/4 v4, 0x0

    invoke-virtual {p3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    new-instance v7, Landroid/text/SpannableStringBuilder;

    iget-object v0, p0, Lcom/twitter/android/UserSelectFragment;->a:Lcom/twitter/android/UserSelectFragment$UserSelectEditText;

    invoke-virtual {v0}, Lcom/twitter/android/UserSelectFragment$UserSelectEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-direct {v7, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v7}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    const-class v1, Lcom/twitter/android/zt;

    invoke-virtual {v7, v4, v0, v1}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/android/zt;

    const/4 v2, 0x0

    array-length v8, v0

    move v3, v4

    :goto_0
    if-ge v3, v8, :cond_0

    aget-object v1, v0, v3

    invoke-virtual {v1}, Lcom/twitter/android/zt;->a()Lcom/twitter/library/api/SelectedUser;

    move-result-object v9

    iget-wide v9, v9, Lcom/twitter/library/api/SelectedUser;->userId:J

    cmp-long v9, v9, p1

    if-nez v9, :cond_4

    :goto_1
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move-object v2, v1

    goto :goto_0

    :cond_0
    if-eqz v2, :cond_2

    const-string/jumbo v0, ""

    invoke-static {v7, v2, v0, v4}, Lcom/twitter/library/util/Util;->a(Landroid/text/Editable;Ljava/lang/Object;Ljava/lang/String;Z)V

    invoke-virtual {v7}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    invoke-direct {p0, v7, v0}, Lcom/twitter/android/UserSelectFragment;->b(Ljava/lang/CharSequence;I)V

    :goto_2
    iget-object v0, p0, Lcom/twitter/android/UserSelectFragment;->c:Lcom/twitter/android/zm;

    invoke-virtual {p0}, Lcom/twitter/android/UserSelectFragment;->f()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/zm;->a(Ljava/util/Set;)V

    :cond_1
    return-void

    :cond_2
    array-length v0, v0

    invoke-virtual {p0}, Lcom/twitter/android/UserSelectFragment;->d()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/UserSelectFragment;->a:Lcom/twitter/android/UserSelectFragment$UserSelectEditText;

    invoke-virtual {v0}, Lcom/twitter/android/UserSelectFragment$UserSelectEditText;->getSelectionEnd()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/UserSelectFragment;->f:Lcom/twitter/android/zu;

    invoke-virtual {v1, v7, v0}, Lcom/twitter/android/zu;->findTokenStart(Ljava/lang/CharSequence;I)I

    move-result v1

    new-instance v2, Lcom/twitter/library/api/SelectedUser;

    invoke-direct {v2, p1, p2, v5, v6}, Lcom/twitter/library/api/SelectedUser;-><init>(JLjava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/android/UserSelectFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    new-instance v4, Lcom/twitter/android/zt;

    invoke-direct {v4, v2, v3}, Lcom/twitter/android/zt;-><init>(Lcom/twitter/library/api/SelectedUser;Landroid/content/res/Resources;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v1, v0, v2}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    const/16 v2, 0x21

    invoke-virtual {v7, v4, v1, v0, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    invoke-direct {p0, v7, v0}, Lcom/twitter/android/UserSelectFragment;->b(Ljava/lang/CharSequence;I)V

    iget-boolean v0, p0, Lcom/twitter/android/UserSelectFragment;->j:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/UserSelectFragment;->a:Lcom/twitter/android/UserSelectFragment$UserSelectEditText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/UserSelectFragment$UserSelectEditText;->a(Z)V

    :cond_3
    invoke-direct {p0}, Lcom/twitter/android/UserSelectFragment;->i()V

    goto :goto_2

    :cond_4
    move-object v1, v2

    goto :goto_1
.end method

.method public a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/UserSelectFragment;->c:Lcom/twitter/android/zm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/UserSelectFragment;->c:Lcom/twitter/android/zm;

    invoke-virtual {v0, p2}, Lcom/twitter/android/zm;->a(Ljava/util/HashMap;)V

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 5

    const/4 v1, 0x1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/android/provider/SuggestionsProvider;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/UserSelectFragment;->g:Lcom/twitter/android/client/c;

    iget v3, p0, Lcom/twitter/android/UserSelectFragment;->i:I

    const-string/jumbo v4, "search_box"

    invoke-virtual {v0, v2, v1, v3, v4}, Lcom/twitter/android/client/c;->a(Ljava/lang/String;IILjava/lang/String;)Ljava/lang/String;

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/CharSequence;I)V
    .locals 5

    invoke-virtual {p0, p2}, Lcom/twitter/android/UserSelectFragment;->b(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/UserSelectFragment;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/UserSelectFragment;->c:Lcom/twitter/android/zm;

    invoke-virtual {v1, v0}, Lcom/twitter/android/zm;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    const/4 v3, 0x3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v2, v3, v0}, Lcom/twitter/android/UserSelectFragment;->a(JLjava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected a(I)Z
    .locals 1

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/UserSelectFragment;->c:Lcom/twitter/android/zm;

    invoke-virtual {v0}, Lcom/twitter/android/zm;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 10

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1, v1}, Landroid/text/Editable;->charAt(I)C

    move-result v0

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a(C)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/UserSelectFragment;->g:Lcom/twitter/android/client/c;

    iget-boolean v0, v0, Lcom/twitter/android/client/c;->f:Z

    if-eqz v0, :cond_3

    :cond_1
    move v0, v2

    :goto_0
    iget-object v3, p0, Lcom/twitter/android/UserSelectFragment;->a:Lcom/twitter/android/UserSelectFragment$UserSelectEditText;

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    :goto_1
    invoke-virtual {v3, v0}, Lcom/twitter/android/UserSelectFragment$UserSelectEditText;->setGravity(I)V

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    const-class v3, Lcom/twitter/android/zt;

    invoke-interface {p1, v1, v0, v3}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/android/zt;

    array-length v3, v0

    if-lez v3, :cond_7

    iget-object v3, p0, Lcom/twitter/android/UserSelectFragment;->a:Lcom/twitter/android/UserSelectFragment$UserSelectEditText;

    invoke-virtual {v3, p0}, Lcom/twitter/android/UserSelectFragment$UserSelectEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    array-length v5, v0

    move v4, v1

    move v3, v1

    :goto_2
    if-ge v4, v5, :cond_5

    aget-object v6, v0, v4

    invoke-interface {p1, v6}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v7

    invoke-interface {p1, v6}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v8

    const/4 v9, -0x1

    if-le v7, v9, :cond_2

    if-lt v8, v7, :cond_2

    invoke-interface {p1, v7, v8}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6}, Lcom/twitter/android/zt;->a()Lcom/twitter/library/api/SelectedUser;

    move-result-object v9

    iget-object v9, v9, Lcom/twitter/library/api/SelectedUser;->name:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    const-string/jumbo v3, ""

    invoke-static {p1, v6, v3, v1}, Lcom/twitter/library/util/Util;->a(Landroid/text/Editable;Ljava/lang/Object;Ljava/lang/String;Z)V

    move v3, v2

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    const/4 v0, 0x3

    goto :goto_1

    :cond_5
    if-eqz v3, :cond_6

    iget-object v0, p0, Lcom/twitter/android/UserSelectFragment;->c:Lcom/twitter/android/zm;

    invoke-virtual {p0}, Lcom/twitter/android/UserSelectFragment;->f()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/zm;->a(Ljava/util/Set;)V

    :cond_6
    iget-object v0, p0, Lcom/twitter/android/UserSelectFragment;->a:Lcom/twitter/android/UserSelectFragment$UserSelectEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/android/UserSelectFragment$UserSelectEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    :cond_7
    invoke-direct {p0}, Lcom/twitter/android/UserSelectFragment;->j()V

    invoke-virtual {p0}, Lcom/twitter/android/UserSelectFragment;->g()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_8

    iget-boolean v0, p0, Lcom/twitter/android/UserSelectFragment;->j:Z

    if-nez v0, :cond_9

    :cond_8
    iget-object v0, p0, Lcom/twitter/android/UserSelectFragment;->a:Lcom/twitter/android/UserSelectFragment$UserSelectEditText;

    invoke-virtual {v0, v2}, Lcom/twitter/android/UserSelectFragment$UserSelectEditText;->a(Z)V

    :cond_9
    return-void
.end method

.method protected b(I)I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/UserSelectFragment;->b:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v0

    sub-int v0, p1, v0

    return v0
.end method

.method public b()Lcom/twitter/android/client/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/UserSelectFragment;->g:Lcom/twitter/android/client/c;

    return-object v0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public c()Lcom/twitter/library/client/Session;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/UserSelectFragment;->h:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method public d()I
    .locals 1

    const v0, 0x7fffffff

    return v0
.end method

.method public e()Ljava/util/ArrayList;
    .locals 5

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/UserSelectFragment;->a:Lcom/twitter/android/UserSelectFragment$UserSelectEditText;

    invoke-virtual {v0}, Lcom/twitter/android/UserSelectFragment$UserSelectEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v3

    const-class v4, Lcom/twitter/android/zt;

    invoke-interface {v0, v1, v3, v4}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/android/zt;

    array-length v3, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v0, v1

    invoke-virtual {v4}, Lcom/twitter/android/zt;->a()Lcom/twitter/library/api/SelectedUser;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v2
.end method

.method public f()Ljava/util/Set;
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/UserSelectFragment;->a:Lcom/twitter/android/UserSelectFragment$UserSelectEditText;

    invoke-virtual {v0}, Lcom/twitter/android/UserSelectFragment$UserSelectEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v3

    const-class v4, Lcom/twitter/android/zt;

    invoke-interface {v0, v1, v3, v4}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/android/zt;

    array-length v3, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v0, v1

    invoke-virtual {v4}, Lcom/twitter/android/zt;->a()Lcom/twitter/library/api/SelectedUser;

    move-result-object v4

    iget-wide v4, v4, Lcom/twitter/library/api/SelectedUser;->userId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v2
.end method

.method public g()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/UserSelectFragment;->a:Lcom/twitter/android/UserSelectFragment$UserSelectEditText;

    invoke-virtual {v0}, Lcom/twitter/android/UserSelectFragment$UserSelectEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/UserSelectFragment;->a:Lcom/twitter/android/UserSelectFragment$UserSelectEditText;

    invoke-virtual {v1}, Lcom/twitter/android/UserSelectFragment$UserSelectEditText;->getSelectionEnd()I

    move-result v1

    iget-object v2, p0, Lcom/twitter/android/UserSelectFragment;->f:Lcom/twitter/android/zu;

    invoke-virtual {v2, v0, v1}, Lcom/twitter/android/zu;->findTokenStart(Ljava/lang/CharSequence;I)I

    move-result v2

    if-ltz v2, :cond_0

    if-le v1, v2, :cond_0

    invoke-interface {v0, v2, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o_()V
    .locals 0

    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    if-eqz p1, :cond_1

    const-string/jumbo v0, "users"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    const-string/jumbo v1, "partial_users"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    invoke-direct {p0, v0, v1}, Lcom/twitter/android/UserSelectFragment;->a(Ljava/util/List;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/UserSelectFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/UserSelectFragment;->h:Lcom/twitter/library/client/aa;

    invoke-static {v0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p0}, Lcom/twitter/android/client/c;->a(ILcom/twitter/library/util/ar;)V

    iget-object v1, p0, Lcom/twitter/android/UserSelectFragment;->e:Lcom/twitter/android/zv;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/j;)V

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->A()I

    move-result v1

    iput v1, p0, Lcom/twitter/android/UserSelectFragment;->i:I

    iput-object v0, p0, Lcom/twitter/android/UserSelectFragment;->g:Lcom/twitter/android/client/c;

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/UserSelectFragment;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    iget-object v0, p0, Lcom/twitter/android/UserSelectFragment;->g:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/UserSelectFragment;->e:Lcom/twitter/android/zv;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/j;)V

    iget-object v0, p0, Lcom/twitter/android/UserSelectFragment;->g:Lcom/twitter/android/client/c;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p0}, Lcom/twitter/android/client/c;->b(ILcom/twitter/library/util/ar;)V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "users"

    invoke-virtual {p0}, Lcom/twitter/android/UserSelectFragment;->e()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const-string/jumbo v0, "partial_users"

    invoke-virtual {p0}, Lcom/twitter/android/UserSelectFragment;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onStart()V
    .locals 3

    const/4 v2, 0x1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    iget-object v0, p0, Lcom/twitter/android/UserSelectFragment;->a:Lcom/twitter/android/UserSelectFragment$UserSelectEditText;

    invoke-virtual {v0}, Lcom/twitter/android/UserSelectFragment$UserSelectEditText;->requestFocus()Z

    invoke-virtual {p0}, Lcom/twitter/android/UserSelectFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/UserSelectFragment;->a:Lcom/twitter/android/UserSelectFragment$UserSelectEditText;

    invoke-static {v0, v1, v2}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/twitter/android/UserSelectFragment;->a:Lcom/twitter/android/UserSelectFragment$UserSelectEditText;

    invoke-virtual {v0, v2}, Lcom/twitter/android/UserSelectFragment$UserSelectEditText;->a(Z)V

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method
