.class Lcom/twitter/android/mh;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/twitter/android/MessagesComposeFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/MessagesComposeFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/mh;->a:Lcom/twitter/android/MessagesComposeFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/mh;->a:Lcom/twitter/android/MessagesComposeFragment;

    invoke-static {v0}, Lcom/twitter/android/MessagesComposeFragment;->a(Lcom/twitter/android/MessagesComposeFragment;)Lcom/twitter/android/mk;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/mh;->a:Lcom/twitter/android/MessagesComposeFragment;

    invoke-static {v0}, Lcom/twitter/android/MessagesComposeFragment;->a(Lcom/twitter/android/MessagesComposeFragment;)Lcom/twitter/android/mk;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/android/mk;->a(Ljava/lang/String;)V

    :cond_0
    return-void
.end method
