.class public Lcom/twitter/android/EventSearchActivity;
.super Lcom/twitter/android/client/BaseFragmentActivity;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/widget/cs;
.implements Lcom/twitter/library/util/ac;


# static fields
.field private static final a:Landroid/net/Uri;

.field private static final b:Landroid/net/Uri;

.field private static final c:Landroid/net/Uri;

.field private static final d:Z


# instance fields
.field private A:I

.field private B:I

.field private C:I

.field private E:I

.field private F:I

.field private G:I

.field private H:I

.field private I:I

.field private J:Z

.field private K:Z

.field private L:Z

.field private M:Z

.field private N:Z

.field private O:Z

.field private P:Z

.field private Q:Z

.field private R:Landroid/os/Handler;

.field private e:Lcom/twitter/library/provider/az;

.field private f:Lcom/twitter/android/widget/TopicView;

.field private g:Landroid/widget/FrameLayout;

.field private h:Lcom/twitter/internal/android/widget/HorizontalListView;

.field private i:Lcom/twitter/android/widget/TopicView$TopicData;

.field private j:Lcom/twitter/android/fr;

.field private k:Landroid/support/v4/view/ViewPager;

.field private l:Lcom/twitter/android/widget/SwipeRefreshObserverLayout;

.field private m:Ljava/util/ArrayList;

.field private n:Ljava/util/HashMap;

.field private o:Landroid/support/v4/view/PagerAdapter;

.field private p:Landroid/graphics/drawable/Drawable;

.field private q:Landroid/graphics/drawable/Drawable;

.field private r:Landroid/graphics/drawable/Drawable;

.field private s:Lcom/twitter/android/widget/cl;

.field private t:Lcom/twitter/android/fp;

.field private u:Lcom/twitter/android/widget/SwipeProgressBarView;

.field private v:Landroid/view/View;

.field private w:Landroid/widget/ImageView;

.field private x:Landroid/widget/TextView;

.field private y:Landroid/view/animation/Animation;

.field private z:Landroid/view/animation/Animation;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string/jumbo v0, "twitter://events/default"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/EventSearchActivity;->a:Landroid/net/Uri;

    const-string/jumbo v0, "twitter://events/media"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/EventSearchActivity;->b:Landroid/net/Uri;

    const-string/jumbo v0, "twitter://events/accounts"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/EventSearchActivity;->c:Landroid/net/Uri;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/android/EventSearchActivity;->d:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;-><init>()V

    new-instance v0, Lcom/twitter/android/fq;

    invoke-direct {v0, p0}, Lcom/twitter/android/fq;-><init>(Lcom/twitter/android/EventSearchActivity;)V

    iput-object v0, p0, Lcom/twitter/android/EventSearchActivity;->R:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/EventSearchActivity;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/EventSearchActivity;->p:Landroid/graphics/drawable/Drawable;

    return-object p1
.end method

.method static synthetic a(Lcom/twitter/android/EventSearchActivity;Lcom/twitter/android/fp;)Lcom/twitter/android/fp;
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/EventSearchActivity;->t:Lcom/twitter/android/fp;

    return-object p1
.end method

.method static synthetic a(Lcom/twitter/android/EventSearchActivity;Lcom/twitter/android/widget/cl;)Lcom/twitter/android/widget/cl;
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/EventSearchActivity;->s:Lcom/twitter/android/widget/cl;

    return-object p1
.end method

.method private a(Landroid/net/Uri;I)Lhb;
    .locals 7

    new-instance v4, Lhd;

    const-class v0, Lcom/twitter/android/EventResultsFragment;

    invoke-direct {v4, p1, v0}, Lhd;-><init>(Landroid/net/Uri;Ljava/lang/Class;)V

    invoke-virtual {p0}, Lcom/twitter/android/EventSearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/twitter/android/EventResultsFragment;->a(Landroid/content/Intent;Z)Landroid/os/Bundle;

    move-result-object v5

    sget-object v0, Lcom/twitter/android/EventSearchActivity;->a:Landroid/net/Uri;

    invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0f01d7    # com.twitter.android.R.string.home_timeline

    invoke-virtual {p0, v0}, Lcom/twitter/android/EventSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x0

    move v6, v0

    move-object v0, v1

    move v1, v6

    :goto_0
    invoke-virtual {v4, v0}, Lhd;->a(Ljava/lang/CharSequence;)Lhd;

    const-string/jumbo v0, "search_type"

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "fragment_page_number"

    invoke-virtual {v5, v0, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->n:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->n:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    :goto_1
    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->e:Lcom/twitter/library/provider/az;

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/provider/az;->r(J)V

    const-string/jumbo v0, "search_id"

    invoke-virtual {v5, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->n:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v4, v5}, Lhd;->a(Landroid/os/Bundle;)Lhd;

    invoke-virtual {v4}, Lhd;->a()Lhb;

    move-result-object v0

    return-object v0

    :cond_0
    sget-object v0, Lcom/twitter/android/EventSearchActivity;->b:Landroid/net/Uri;

    invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0f03a0    # com.twitter.android.R.string.search_filter_photos

    invoke-virtual {p0, v0}, Lcom/twitter/android/EventSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/16 v0, 0x9

    move v6, v0

    move-object v0, v1

    move v1, v6

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/twitter/android/EventSearchActivity;->c:Landroid/net/Uri;

    invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0f000b    # com.twitter.android.R.string.accounts_title

    invoke-virtual {p0, v0}, Lcom/twitter/android/EventSearchActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x2

    move v6, v0

    move-object v0, v1

    move v1, v6

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    const-string/jumbo v0, "search_type"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    move v6, v0

    move-object v0, v1

    move v1, v6

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/twitter/internal/util/j;->a:Ljava/security/SecureRandom;

    invoke-virtual {v0}, Ljava/security/SecureRandom;->nextLong()J

    move-result-wide v2

    goto :goto_1
.end method

.method private a(I)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->v:Landroid/view/View;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    return-void
.end method

.method private a(Landroid/graphics/Bitmap;)V
    .locals 9

    const/4 v0, 0x0

    const/4 v7, 0x1

    const/high16 v6, 0x3f000000    # 0.5f

    iget-object v1, p0, Lcom/twitter/android/EventSearchActivity;->f:Lcom/twitter/android/widget/TopicView;

    invoke-virtual {v1}, Lcom/twitter/android/widget/TopicView;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    iget v2, p0, Lcom/twitter/android/EventSearchActivity;->E:I

    mul-int/2addr v2, v1

    mul-int v5, v3, v4

    if-le v2, v5, :cond_0

    iget v2, p0, Lcom/twitter/android/EventSearchActivity;->E:I

    int-to-float v2, v2

    int-to-float v4, v4

    div-float/2addr v2, v4

    int-to-float v4, v3

    int-to-float v1, v1

    mul-float/2addr v1, v2

    sub-float v1, v4, v1

    mul-float/2addr v1, v6

    :goto_0
    new-instance v4, Landroid/graphics/Matrix;

    invoke-direct {v4}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {v4, v2, v2}, Landroid/graphics/Matrix;->setScale(FF)V

    add-float/2addr v1, v6

    float-to-int v1, v1

    int-to-float v1, v1

    add-float/2addr v0, v6

    float-to-int v0, v0

    int-to-float v0, v0

    invoke-virtual {v4, v1, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget v0, p0, Lcom/twitter/android/EventSearchActivity;->E:I

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v1

    invoke-static {v3, v0, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v2, v7}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    invoke-virtual {v1, p1, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    iget-object v1, p0, Lcom/twitter/android/EventSearchActivity;->t:Lcom/twitter/android/fp;

    if-nez v1, :cond_1

    invoke-static {}, Lcom/twitter/android/util/ae;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Lcom/twitter/android/fp;

    invoke-direct {v1, p0}, Lcom/twitter/android/fp;-><init>(Lcom/twitter/android/EventSearchActivity;)V

    iput-object v1, p0, Lcom/twitter/android/EventSearchActivity;->t:Lcom/twitter/android/fp;

    iget-object v1, p0, Lcom/twitter/android/EventSearchActivity;->t:Lcom/twitter/android/fp;

    new-array v2, v7, [Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/android/fp;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_1
    return-void

    :cond_0
    int-to-float v2, v3

    int-to-float v1, v1

    div-float/2addr v2, v1

    iget v1, p0, Lcom/twitter/android/EventSearchActivity;->E:I

    int-to-float v1, v1

    int-to-float v4, v4

    mul-float/2addr v4, v2

    sub-float/2addr v1, v4

    mul-float/2addr v1, v6

    move v8, v1

    move v1, v0

    move v0, v8

    goto :goto_0

    :cond_1
    invoke-direct {p0, v0, v7}, Lcom/twitter/android/EventSearchActivity;->a(Landroid/graphics/Bitmap;Z)V

    goto :goto_1
.end method

.method private a(Landroid/graphics/Bitmap;Z)V
    .locals 5

    const/4 v1, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->f:Lcom/twitter/android/widget/TopicView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/TopicView;->getWidth()I

    move-result v0

    const/4 v2, 0x0

    iget v3, p0, Lcom/twitter/android/EventSearchActivity;->E:I

    iget v4, p0, Lcom/twitter/android/EventSearchActivity;->B:I

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/twitter/android/EventSearchActivity;->B:I

    invoke-static {p1, v2, v3, v0, v4}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/twitter/android/EventSearchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    const/4 v2, 0x2

    new-array v2, v2, [Landroid/graphics/drawable/Drawable;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    iget-object v3, p0, Lcom/twitter/android/EventSearchActivity;->r:Landroid/graphics/drawable/Drawable;

    aput-object v3, v2, v0

    new-instance v0, Landroid/graphics/drawable/LayerDrawable;

    invoke-direct {v0, v2}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    iput-object v0, p0, Lcom/twitter/android/EventSearchActivity;->p:Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p2, :cond_0

    if-eq p1, v1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    if-eqz p2, :cond_1

    if-eq p1, v1, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    :cond_1
    throw v0
.end method

.method private a(Landroid/support/v4/app/Fragment;)V
    .locals 2

    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/twitter/android/EventResultsFragment;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/twitter/android/EventResultsFragment;

    iget v0, p0, Lcom/twitter/android/EventSearchActivity;->F:I

    iget v1, p0, Lcom/twitter/android/EventSearchActivity;->A:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/twitter/android/EventSearchActivity;->C:I

    invoke-virtual {p1, v0, v1}, Lcom/twitter/android/EventResultsFragment;->a(II)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/EventSearchActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/EventSearchActivity;->f()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/EventSearchActivity;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/EventSearchActivity;->b(I)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/EventSearchActivity;Landroid/graphics/Bitmap;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/EventSearchActivity;->a(Landroid/graphics/Bitmap;Z)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/EventSearchActivity;Landroid/support/v4/app/Fragment;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/EventSearchActivity;->a(Landroid/support/v4/app/Fragment;)V

    return-void
.end method

.method private a(Lcom/twitter/android/widget/TopicView;)V
    .locals 3

    iput-object p1, p0, Lcom/twitter/android/EventSearchActivity;->f:Lcom/twitter/android/widget/TopicView;

    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/twitter/android/EventSearchActivity;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->removeAllViews()V

    iget-object v1, p0, Lcom/twitter/android/EventSearchActivity;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v1, p1, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->g:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/fo;

    invoke-direct {v1, p0}, Lcom/twitter/android/fo;-><init>(Lcom/twitter/android/EventSearchActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/EventSearchActivity;I)I
    .locals 0

    iput p1, p0, Lcom/twitter/android/EventSearchActivity;->G:I

    return p1
.end method

.method static synthetic b(Lcom/twitter/android/EventSearchActivity;)Lcom/twitter/android/fr;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->j:Lcom/twitter/android/fr;

    return-object v0
.end method

.method private b(I)V
    .locals 3

    iget v0, p0, Lcom/twitter/android/EventSearchActivity;->B:I

    sub-int v0, p1, v0

    iput v0, p0, Lcom/twitter/android/EventSearchActivity;->F:I

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhb;

    invoke-virtual {p0}, Lcom/twitter/android/EventSearchActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v0, v2}, Lhb;->a(Landroid/support/v4/app/FragmentManager;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/SearchFragment;

    invoke-direct {p0, v0}, Lcom/twitter/android/EventSearchActivity;->a(Landroid/support/v4/app/Fragment;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/twitter/android/EventSearchActivity;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->m:Ljava/util/ArrayList;

    return-object v0
.end method

.method private c(I)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->g:Landroid/widget/FrameLayout;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setTranslationY(F)V

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->h:Lcom/twitter/internal/android/widget/HorizontalListView;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/HorizontalListView;->setTranslationY(F)V

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->s:Lcom/twitter/android/widget/cl;

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/twitter/android/EventSearchActivity;->d(I)V

    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/twitter/android/EventSearchActivity;)Landroid/support/v4/view/ViewPager;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->k:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method private d(I)V
    .locals 2

    iget v0, p0, Lcom/twitter/android/EventSearchActivity;->E:I

    add-int/2addr v0, p1

    iget v1, p0, Lcom/twitter/android/EventSearchActivity;->E:I

    div-int/lit8 v1, v1, 0x4

    div-int/2addr v0, v1

    iput v0, p0, Lcom/twitter/android/EventSearchActivity;->G:I

    iget-object v1, p0, Lcom/twitter/android/EventSearchActivity;->s:Lcom/twitter/android/widget/cl;

    iget v0, p0, Lcom/twitter/android/EventSearchActivity;->G:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/twitter/android/EventSearchActivity;->G:I

    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/cl;->a(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic e(Lcom/twitter/android/EventSearchActivity;)Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->r:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/EventSearchActivity;)Landroid/widget/FrameLayout;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->g:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method private f()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->x:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/EventSearchActivity;->Q:Z

    invoke-virtual {p0}, Lcom/twitter/android/EventSearchActivity;->V()V

    return-void
.end method

.method private g()V
    .locals 2

    iget-boolean v0, p0, Lcom/twitter/android/EventSearchActivity;->K:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->i:Lcom/twitter/android/widget/TopicView$TopicData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->i:Lcom/twitter/android/widget/TopicView$TopicData;

    iget v0, v0, Lcom/twitter/android/widget/TopicView$TopicData;->b:I

    invoke-static {v0}, Lcom/twitter/android/widget/o;->a(I)Lcom/twitter/android/widget/p;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/EventSearchActivity;->i:Lcom/twitter/android/widget/TopicView$TopicData;

    invoke-interface {v0, v1}, Lcom/twitter/android/widget/p;->a(Lcom/twitter/android/widget/TopicView$TopicData;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/twitter/android/EventSearchActivity;->a(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/twitter/android/EventSearchActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBar;->invalidate()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/EventSearchActivity;->K:Z

    :cond_0
    return-void
.end method

.method static synthetic g(Lcom/twitter/android/EventSearchActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/EventSearchActivity;->M:Z

    return v0
.end method

.method static synthetic h(Lcom/twitter/android/EventSearchActivity;)Lcom/twitter/android/widget/cl;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->s:Lcom/twitter/android/widget/cl;

    return-object v0
.end method

.method private h()V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/EventSearchActivity;->K:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/EventSearchActivity;->a(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/twitter/android/EventSearchActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBar;->invalidate()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/EventSearchActivity;->K:Z

    :cond_0
    return-void
.end method

.method static synthetic i(Lcom/twitter/android/EventSearchActivity;)Lcom/twitter/android/widget/TopicView;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->f:Lcom/twitter/android/widget/TopicView;

    return-object v0
.end method

.method static synthetic j(Lcom/twitter/android/EventSearchActivity;)Lcom/twitter/internal/android/widget/HorizontalListView;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->h:Lcom/twitter/internal/android/widget/HorizontalListView;

    return-object v0
.end method

.method private k()Ljava/util/ArrayList;
    .locals 3

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    sget-object v1, Lcom/twitter/android/EventSearchActivity;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/twitter/android/EventSearchActivity;->a(Landroid/net/Uri;I)Lhb;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v1, Lcom/twitter/android/EventSearchActivity;->b:Landroid/net/Uri;

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lcom/twitter/android/EventSearchActivity;->a(Landroid/net/Uri;I)Lhb;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v1, Lcom/twitter/android/EventSearchActivity;->c:Landroid/net/Uri;

    const/4 v2, 0x2

    invoke-direct {p0, v1, v2}, Lcom/twitter/android/EventSearchActivity;->a(Landroid/net/Uri;I)Lhb;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 2

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    const v1, 0x7f030069    # com.twitter.android.R.layout.event_search_activity

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(I)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(Z)V

    const/16 v1, 0x16

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->c(I)V

    return-object v0
.end method

.method public a(F)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    iget-boolean v0, p0, Lcom/twitter/android/EventSearchActivity;->P:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->w:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->x:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->v:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iput-boolean v3, p0, Lcom/twitter/android/EventSearchActivity;->P:Z

    iput-boolean v3, p0, Lcom/twitter/android/EventSearchActivity;->Q:Z

    invoke-virtual {p0}, Lcom/twitter/android/EventSearchActivity;->V()V

    :cond_0
    const/high16 v0, 0x42c80000    # 100.0f

    mul-float/2addr v0, p1

    const/high16 v1, 0x42480000    # 50.0f

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_3

    iput-boolean v4, p0, Lcom/twitter/android/EventSearchActivity;->O:Z

    const v0, 0x7f0f0348    # com.twitter.android.R.string.refresh_pull_down

    iget v1, p0, Lcom/twitter/android/EventSearchActivity;->I:I

    if-ne v1, v3, :cond_1

    iget-object v1, p0, Lcom/twitter/android/EventSearchActivity;->w:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->clearAnimation()V

    iget-object v1, p0, Lcom/twitter/android/EventSearchActivity;->w:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/twitter/android/EventSearchActivity;->z:Landroid/view/animation/Animation;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    const/4 v1, 0x2

    iput v1, p0, Lcom/twitter/android/EventSearchActivity;->I:I

    :cond_1
    iget v1, p0, Lcom/twitter/android/EventSearchActivity;->A:I

    int-to-float v1, v1

    mul-float/2addr v1, p1

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iget v2, p0, Lcom/twitter/android/EventSearchActivity;->A:I

    sub-int/2addr v1, v2

    sget-boolean v2, Lcom/twitter/android/EventSearchActivity;->d:Z

    if-eqz v2, :cond_2

    invoke-direct {p0, v1}, Lcom/twitter/android/EventSearchActivity;->a(I)V

    :cond_2
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/EventSearchActivity;->x:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    return-void

    :cond_3
    iput-boolean v3, p0, Lcom/twitter/android/EventSearchActivity;->O:Z

    const v0, 0x7f0f0349    # com.twitter.android.R.string.refresh_release

    iget v1, p0, Lcom/twitter/android/EventSearchActivity;->I:I

    if-eq v1, v3, :cond_4

    iget-object v1, p0, Lcom/twitter/android/EventSearchActivity;->w:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->clearAnimation()V

    iget-object v1, p0, Lcom/twitter/android/EventSearchActivity;->w:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/twitter/android/EventSearchActivity;->y:Landroid/view/animation/Animation;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    iput v3, p0, Lcom/twitter/android/EventSearchActivity;->I:I

    :cond_4
    sget-boolean v1, Lcom/twitter/android/EventSearchActivity;->d:Z

    if-eqz v1, :cond_2

    invoke-direct {p0, v4}, Lcom/twitter/android/EventSearchActivity;->a(I)V

    goto :goto_0
.end method

.method public a(II)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->k:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    if-eq p2, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/EventSearchActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    iget v1, p0, Lcom/twitter/android/EventSearchActivity;->E:I

    add-int/2addr v1, p1

    iget v2, p0, Lcom/twitter/android/EventSearchActivity;->A:I

    if-gt v1, v2, :cond_5

    iget-boolean v1, p0, Lcom/twitter/android/EventSearchActivity;->L:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/twitter/android/EventSearchActivity;->p:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/twitter/android/EventSearchActivity;->p:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_1
    iget-object v1, p0, Lcom/twitter/android/EventSearchActivity;->i:Lcom/twitter/android/widget/TopicView$TopicData;

    iget-object v1, v1, Lcom/twitter/android/widget/TopicView$TopicData;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->setTitle(Ljava/lang/CharSequence;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/EventSearchActivity;->L:Z

    :cond_2
    :goto_2
    iput p1, p0, Lcom/twitter/android/EventSearchActivity;->C:I

    iget-boolean v0, p0, Lcom/twitter/android/EventSearchActivity;->J:Z

    if-nez v0, :cond_6

    iget v0, p0, Lcom/twitter/android/EventSearchActivity;->F:I

    add-int/2addr v0, p1

    if-gtz v0, :cond_6

    invoke-direct {p0}, Lcom/twitter/android/EventSearchActivity;->g()V

    :cond_3
    :goto_3
    sget-boolean v0, Lcom/twitter/android/EventSearchActivity;->d:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/twitter/android/EventSearchActivity;->c(I)V

    iget-boolean v0, p0, Lcom/twitter/android/EventSearchActivity;->N:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->u:Lcom/twitter/android/widget/SwipeProgressBarView;

    iget v1, p0, Lcom/twitter/android/EventSearchActivity;->H:I

    iget v2, p0, Lcom/twitter/android/EventSearchActivity;->C:I

    add-int/2addr v1, v2

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/SwipeProgressBarView;->setProgressTop(I)V

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/twitter/android/EventSearchActivity;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    :cond_5
    iget-boolean v1, p0, Lcom/twitter/android/EventSearchActivity;->L:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/android/EventSearchActivity;->r:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->setTitle(Ljava/lang/CharSequence;)V

    iput-boolean v3, p0, Lcom/twitter/android/EventSearchActivity;->L:Z

    goto :goto_2

    :cond_6
    iget-boolean v0, p0, Lcom/twitter/android/EventSearchActivity;->J:Z

    if-nez v0, :cond_3

    invoke-direct {p0}, Lcom/twitter/android/EventSearchActivity;->h()V

    goto :goto_3
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 10

    const v9, 0x7f0b005a    # com.twitter.android.R.color.light_blue

    const v8, 0x7f0b004d    # com.twitter.android.R.color.faded_blue

    const/4 v7, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/EventSearchActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-static {p0, v3, v4}, Lcom/twitter/library/provider/az;->a(Landroid/content/Context;J)Lcom/twitter/library/provider/az;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/EventSearchActivity;->e:Lcom/twitter/library/provider/az;

    if-nez p1, :cond_1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/EventSearchActivity;->n:Ljava/util/HashMap;

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->e:Lcom/twitter/library/provider/az;

    iget-object v3, p0, Lcom/twitter/android/EventSearchActivity;->n:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/library/provider/az;->a(Ljava/util/Collection;)V

    invoke-direct {p0}, Lcom/twitter/android/EventSearchActivity;->k()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/EventSearchActivity;->m:Ljava/util/ArrayList;

    new-instance v0, Lcom/twitter/android/fr;

    iget-object v3, p0, Lcom/twitter/android/EventSearchActivity;->m:Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Lcom/twitter/android/fr;-><init>(Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/twitter/android/EventSearchActivity;->j:Lcom/twitter/android/fr;

    const v0, 0x7f090156    # com.twitter.android.R.id.tabs

    invoke-virtual {p0, v0}, Lcom/twitter/android/EventSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/HorizontalListView;

    iput-object v0, p0, Lcom/twitter/android/EventSearchActivity;->h:Lcom/twitter/internal/android/widget/HorizontalListView;

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->h:Lcom/twitter/internal/android/widget/HorizontalListView;

    iget-object v3, p0, Lcom/twitter/android/EventSearchActivity;->j:Lcom/twitter/android/fr;

    invoke-virtual {v0, v3}, Lcom/twitter/internal/android/widget/HorizontalListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->h:Lcom/twitter/internal/android/widget/HorizontalListView;

    new-instance v3, Lcom/twitter/android/fn;

    invoke-direct {v3, p0}, Lcom/twitter/android/fn;-><init>(Lcom/twitter/android/EventSearchActivity;)V

    invoke-virtual {v0, v3}, Lcom/twitter/internal/android/widget/HorizontalListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-virtual {p0}, Lcom/twitter/android/EventSearchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v0, 0x7f0c009d    # com.twitter.android.R.dimen.nav_bar_height

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/EventSearchActivity;->A:I

    iget v0, p0, Lcom/twitter/android/EventSearchActivity;->A:I

    iput v0, p0, Lcom/twitter/android/EventSearchActivity;->B:I

    const v0, 0x7f0c00ba    # com.twitter.android.R.dimen.search_card_image_height

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/EventSearchActivity;->E:I

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v4, 0x140

    if-le v0, v4, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/twitter/android/EventSearchActivity;->M:Z

    const/high16 v0, 0x7f020000    # com.twitter.android.R.drawable.actionbar_background_overlay

    :try_start_0
    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/EventSearchActivity;->r:Landroid/graphics/drawable/Drawable;

    const v0, 0x7f0b0004    # com.twitter.android.R.color.border_gray

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    const/4 v4, 0x2

    new-array v4, v4, [Landroid/graphics/drawable/Drawable;

    const/4 v5, 0x0

    new-instance v6, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v6, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    aput-object v6, v4, v5

    const/4 v0, 0x1

    iget-object v5, p0, Lcom/twitter/android/EventSearchActivity;->r:Landroid/graphics/drawable/Drawable;

    aput-object v5, v4, v0

    new-instance v0, Landroid/graphics/drawable/LayerDrawable;

    invoke-direct {v0, v4}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    iput-object v0, p0, Lcom/twitter/android/EventSearchActivity;->q:Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    const v0, 0x7f090158    # com.twitter.android.R.id.event_header_view

    invoke-virtual {p0, v0}, Lcom/twitter/android/EventSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/twitter/android/EventSearchActivity;->g:Landroid/widget/FrameLayout;

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    invoke-virtual {p0}, Lcom/twitter/android/EventSearchActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v7, :cond_3

    :goto_3
    iput-boolean v1, p0, Lcom/twitter/android/EventSearchActivity;->J:Z

    const v0, 0x7f090157    # com.twitter.android.R.id.swipe_refresh_layout

    invoke-virtual {p0, v0}, Lcom/twitter/android/EventSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/SwipeRefreshObserverLayout;

    iput-object v0, p0, Lcom/twitter/android/EventSearchActivity;->l:Lcom/twitter/android/widget/SwipeRefreshObserverLayout;

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->l:Lcom/twitter/android/widget/SwipeRefreshObserverLayout;

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/SwipeRefreshObserverLayout;->setSwipeListener(Lcom/twitter/android/widget/cs;)V

    const v0, 0x7f090159    # com.twitter.android.R.id.progress_view

    invoke-virtual {p0, v0}, Lcom/twitter/android/EventSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/SwipeProgressBarView;

    iput-object v0, p0, Lcom/twitter/android/EventSearchActivity;->u:Lcom/twitter/android/widget/SwipeProgressBarView;

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->u:Lcom/twitter/android/widget/SwipeProgressBarView;

    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v3, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v1, v2, v4, v3}, Lcom/twitter/android/widget/SwipeProgressBarView;->a(IIII)V

    const v0, 0x7f09015a    # com.twitter.android.R.id.ptr_overlay

    invoke-virtual {p0, v0}, Lcom/twitter/android/EventSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/EventSearchActivity;->v:Landroid/view/View;

    const v0, 0x7f090050    # com.twitter.android.R.id.refresh_icon

    invoke-virtual {p0, v0}, Lcom/twitter/android/EventSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/EventSearchActivity;->w:Landroid/widget/ImageView;

    const v0, 0x7f090052    # com.twitter.android.R.id.refresh_text

    invoke-virtual {p0, v0}, Lcom/twitter/android/EventSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/EventSearchActivity;->x:Landroid/widget/TextView;

    const v0, 0x7f040013    # com.twitter.android.R.anim.rotate_up

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/EventSearchActivity;->y:Landroid/view/animation/Animation;

    const v0, 0x7f040012    # com.twitter.android.R.anim.rotate_down

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/EventSearchActivity;->z:Landroid/view/animation/Animation;

    invoke-virtual {p0}, Lcom/twitter/android/EventSearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "topic_data"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/TopicView$TopicData;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/twitter/android/EventSearchActivity;->a(Lcom/twitter/android/widget/TopicView$TopicData;)V

    :cond_0
    const v0, 0x7f0900bb    # com.twitter.android.R.id.pager

    invoke-virtual {p0, v0}, Lcom/twitter/android/EventSearchActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/twitter/android/EventSearchActivity;->k:Landroid/support/v4/view/ViewPager;

    new-instance v0, Lcom/twitter/android/fs;

    iget-object v1, p0, Lcom/twitter/android/EventSearchActivity;->m:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/twitter/android/EventSearchActivity;->k:Landroid/support/v4/view/ViewPager;

    invoke-direct {v0, p0, p0, v1, v2}, Lcom/twitter/android/fs;-><init>(Lcom/twitter/android/EventSearchActivity;Landroid/support/v4/app/FragmentActivity;Ljava/util/ArrayList;Landroid/support/v4/view/ViewPager;)V

    iput-object v0, p0, Lcom/twitter/android/EventSearchActivity;->o:Landroid/support/v4/view/PagerAdapter;

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->k:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/twitter/android/EventSearchActivity;->o:Landroid/support/v4/view/PagerAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    return-void

    :cond_1
    const-string/jumbo v0, "search_ids"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    iput-object v0, p0, Lcom/twitter/android/EventSearchActivity;->n:Ljava/util/HashMap;

    goto/16 :goto_0

    :cond_2
    move v0, v2

    goto/16 :goto_1

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/EventSearchActivity;->r:Landroid/graphics/drawable/Drawable;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/EventSearchActivity;->q:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_2

    :cond_3
    move v1, v2

    goto/16 :goto_3
.end method

.method public a(Lcom/twitter/android/widget/TopicView$TopicData;)V
    .locals 3

    iput-object p1, p0, Lcom/twitter/android/EventSearchActivity;->i:Lcom/twitter/android/widget/TopicView$TopicData;

    iget v0, p1, Lcom/twitter/android/widget/TopicView$TopicData;->b:I

    invoke-static {v0}, Lcom/twitter/android/widget/o;->a(I)Lcom/twitter/android/widget/p;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/EventSearchActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/twitter/internal/android/widget/ToolBar;->setDisplayShowTitleEnabled(Z)V

    iget-boolean v2, p0, Lcom/twitter/android/EventSearchActivity;->J:Z

    if-nez v2, :cond_0

    sget-boolean v2, Lcom/twitter/android/EventSearchActivity;->d:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/EventSearchActivity;->r:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/EventSearchActivity;->q:Landroid/graphics/drawable/Drawable;

    if-nez v2, :cond_2

    :cond_0
    iget-object v0, p1, Lcom/twitter/android/widget/TopicView$TopicData;->c:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->setTitle(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/twitter/android/EventSearchActivity;->g()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/EventSearchActivity;->H:I

    :cond_1
    :goto_0
    return-void

    :cond_2
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/twitter/android/EventSearchActivity;->f:Lcom/twitter/android/widget/TopicView;

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/twitter/android/EventSearchActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/EventSearchActivity;->f:Lcom/twitter/android/widget/TopicView;

    invoke-interface {v0, v1, v2, p1}, Lcom/twitter/android/widget/p;->a(Lcom/twitter/android/client/c;Landroid/view/View;Lcom/twitter/android/widget/TopicView$TopicData;)V

    goto :goto_0

    :cond_3
    iget v1, p0, Lcom/twitter/android/EventSearchActivity;->E:I

    iget v2, p0, Lcom/twitter/android/EventSearchActivity;->A:I

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/twitter/android/EventSearchActivity;->H:I

    iget-object v1, p0, Lcom/twitter/android/EventSearchActivity;->g:Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/twitter/android/EventSearchActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v2

    invoke-interface {v0, p0, v1, v2, p1}, Lcom/twitter/android/widget/p;->a(Landroid/content/Context;Landroid/view/ViewGroup;Lcom/twitter/android/client/c;Lcom/twitter/android/widget/TopicView$TopicData;)Lcom/twitter/android/widget/TopicView;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/EventSearchActivity;->a(Lcom/twitter/android/widget/TopicView;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/util/aa;Ljava/util/HashMap;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->f:Lcom/twitter/android/widget/TopicView;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->f:Lcom/twitter/android/widget/TopicView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/TopicView;->getImageKey()Lcom/twitter/library/util/m;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/ae;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/twitter/library/util/ae;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    :try_start_0
    iget-object v0, v0, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-direct {p0, v0}, Lcom/twitter/android/EventSearchActivity;->a(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->f:Lcom/twitter/android/widget/TopicView;

    invoke-virtual {v0, p2}, Lcom/twitter/android/widget/TopicView;->setEventImages(Ljava/util/HashMap;)V

    iget v0, p0, Lcom/twitter/android/EventSearchActivity;->F:I

    iget-object v1, p0, Lcom/twitter/android/EventSearchActivity;->f:Lcom/twitter/android/widget/TopicView;

    invoke-virtual {v1}, Lcom/twitter/android/widget/TopicView;->getHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget v1, p0, Lcom/twitter/android/EventSearchActivity;->F:I

    if-eq v0, v1, :cond_0

    invoke-direct {p0, v0}, Lcom/twitter/android/EventSearchActivity;->b(I)V

    :cond_0
    return-void

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/twitter/android/EventSearchActivity;->p:Landroid/graphics/drawable/Drawable;

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->i:Lcom/twitter/android/widget/TopicView$TopicData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->i:Lcom/twitter/android/widget/TopicView$TopicData;

    iget-object v0, v0, Lcom/twitter/android/widget/TopicView$TopicData;->k:[B

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterTopic$SportsEvent;

    iput-object p1, v0, Lcom/twitter/library/api/TwitterTopic$SportsEvent;->summary:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/EventSearchActivity;->i:Lcom/twitter/android/widget/TopicView$TopicData;

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a(Ljava/lang/Object;)[B

    move-result-object v0

    iput-object v0, v1, Lcom/twitter/android/widget/TopicView$TopicData;->k:[B

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->i:Lcom/twitter/android/widget/TopicView$TopicData;

    invoke-virtual {p0, v0}, Lcom/twitter/android/EventSearchActivity;->a(Lcom/twitter/android/widget/TopicView$TopicData;)V

    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 5

    const/4 v4, 0x2

    const/16 v2, 0x8

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/twitter/android/EventSearchActivity;->O:Z

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->l:Lcom/twitter/android/widget/SwipeRefreshObserverLayout;

    invoke-virtual {v0, p1, v3}, Lcom/twitter/android/widget/SwipeRefreshObserverLayout;->a(ZZ)V

    iput-boolean p1, p0, Lcom/twitter/android/EventSearchActivity;->N:Z

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->u:Lcom/twitter/android/widget/SwipeProgressBarView;

    invoke-virtual {v0, v3}, Lcom/twitter/android/widget/SwipeProgressBarView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->u:Lcom/twitter/android/widget/SwipeProgressBarView;

    iget v1, p0, Lcom/twitter/android/EventSearchActivity;->H:I

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/SwipeProgressBarView;->setProgressTop(I)V

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->u:Lcom/twitter/android/widget/SwipeProgressBarView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/SwipeProgressBarView;->a()V

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->w:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->w:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->x:Landroid/widget/TextView;

    const v1, 0x7f0f0224    # com.twitter.android.R.string.loading

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->R:Landroid/os/Handler;

    const-wide/16 v1, 0x3e8

    invoke-virtual {v0, v4, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->m:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/twitter/android/EventSearchActivity;->k:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhb;

    invoke-virtual {p0}, Lcom/twitter/android/EventSearchActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhb;->a(Landroid/support/v4/app/FragmentManager;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/BaseListFragment;

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/BaseListFragment;->c(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, v3}, Lcom/twitter/android/EventSearchActivity;->a(Z)V

    goto :goto_0

    :cond_2
    iput-boolean v3, p0, Lcom/twitter/android/EventSearchActivity;->P:Z

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->R:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->u:Lcom/twitter/android/widget/SwipeProgressBarView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/SwipeProgressBarView;->b()V

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->u:Lcom/twitter/android/widget/SwipeProgressBarView;

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/SwipeProgressBarView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->v:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iput v3, p0, Lcom/twitter/android/EventSearchActivity;->I:I

    iget-boolean v0, p0, Lcom/twitter/android/EventSearchActivity;->Q:Z

    if-eqz v0, :cond_0

    iput-boolean v3, p0, Lcom/twitter/android/EventSearchActivity;->Q:Z

    invoke-virtual {p0}, Lcom/twitter/android/EventSearchActivity;->V()V

    goto :goto_0
.end method

.method protected a(Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 2

    const v0, 0x7f09030b    # com.twitter.android.R.id.takeover_placeholder

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/android/EventSearchActivity;->Q:Z

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lhn;->f()Z

    :goto_0
    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lcom/twitter/internal/android/widget/ToolBar;)Z

    move-result v0

    return v0

    :cond_0
    invoke-virtual {v0}, Lhn;->g()Z

    goto :goto_0
.end method

.method protected a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 1

    const v0, 0x7f11000a    # com.twitter.android.R.menu.event_search

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z

    move-result v0

    return v0
.end method

.method public b()Landroid/view/View;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->m:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/twitter/android/EventSearchActivity;->k:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhb;

    invoke-virtual {p0}, Lcom/twitter/android/EventSearchActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhb;->a(Landroid/support/v4/app/FragmentManager;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/BaseListFragment;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/twitter/android/client/BaseListFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->k:Landroid/support/v4/view/ViewPager;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->k:Landroid/support/v4/view/ViewPager;

    goto :goto_0
.end method

.method public c()V
    .locals 4

    const/4 v3, 0x1

    iget-boolean v0, p0, Lcom/twitter/android/EventSearchActivity;->O:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, v3}, Lcom/twitter/android/EventSearchActivity;->a(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->R:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->R:Landroid/os/Handler;

    const-wide/16 v1, 0x32

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method protected c_()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->i:Lcom/twitter/android/widget/TopicView$TopicData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->i:Lcom/twitter/android/widget/TopicView$TopicData;

    iget-object v0, v0, Lcom/twitter/android/widget/TopicView$TopicData;->i:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/EventSearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "seed_hashtag"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 5

    const/4 v1, 0x0

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->t:Lcom/twitter/android/fp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->t:Lcom/twitter/android/fp;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/twitter/android/fp;->cancel(Z)Z

    iput-object v4, p0, Lcom/twitter/android/EventSearchActivity;->t:Lcom/twitter/android/fp;

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/EventSearchActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->b()Lcom/twitter/library/client/Session$LoginStatus;

    move-result-object v0

    sget-object v2, Lcom/twitter/library/client/Session$LoginStatus;->c:Lcom/twitter/library/client/Session$LoginStatus;

    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->e:Lcom/twitter/library/provider/az;

    iget-object v2, p0, Lcom/twitter/android/EventSearchActivity;->n:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/library/provider/az;->b(Ljava/util/Collection;)V

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/EventSearchActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/twitter/internal/android/widget/ToolBar;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->p:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->p:Landroid/graphics/drawable/Drawable;

    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/LayerDrawable;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v4, p0, Lcom/twitter/android/EventSearchActivity;->p:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->s:Lcom/twitter/android/widget/cl;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->s:Lcom/twitter/android/widget/cl;

    invoke-virtual {v0}, Lcom/twitter/android/widget/cl;->a()[Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->s:Lcom/twitter/android/widget/cl;

    invoke-virtual {v0, v4}, Lcom/twitter/android/widget/cl;->a([Landroid/graphics/drawable/BitmapDrawable;)V

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_3

    aget-object v1, v2, v0

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/EventSearchActivity;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    iput-object v4, p0, Lcom/twitter/android/EventSearchActivity;->f:Lcom/twitter/android/widget/TopicView;

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onDestroy()V

    return-void
.end method

.method protected onPause()V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/EventSearchActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/util/ac;)V

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onPause()V

    return-void
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onResume()V

    invoke-virtual {p0}, Lcom/twitter/android/EventSearchActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/util/ac;)V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    const-string/jumbo v0, "search_ids"

    iget-object v1, p0, Lcom/twitter/android/EventSearchActivity;->n:Ljava/util/HashMap;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method
