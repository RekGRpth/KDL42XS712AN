.class Lcom/twitter/android/mx;
.super Lcom/twitter/library/client/j;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/MessagesThreadFragment;


# direct methods
.method private constructor <init>(Lcom/twitter/android/MessagesThreadFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/mx;->a:Lcom/twitter/android/MessagesThreadFragment;

    invoke-direct {p0}, Lcom/twitter/library/client/j;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/MessagesThreadFragment;Lcom/twitter/android/ms;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/mx;-><init>(Lcom/twitter/android/MessagesThreadFragment;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/util/HashMap;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/mx;->a:Lcom/twitter/android/MessagesThreadFragment;

    invoke-static {v0}, Lcom/twitter/android/MessagesThreadFragment;->i(Lcom/twitter/android/MessagesThreadFragment;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/m;

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/util/ae;

    iget-object v3, p0, Lcom/twitter/android/mx;->a:Lcom/twitter/android/MessagesThreadFragment;

    invoke-static {v3}, Lcom/twitter/android/MessagesThreadFragment;->i(Lcom/twitter/android/MessagesThreadFragment;)Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v1, v1, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v1, Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public i(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/mx;->a:Lcom/twitter/android/MessagesThreadFragment;

    invoke-static {v0, p2}, Lcom/twitter/android/MessagesThreadFragment;->b(Lcom/twitter/android/MessagesThreadFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    if-eqz v0, :cond_0

    const/16 v0, 0xc8

    if-eq p3, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/mx;->a:Lcom/twitter/android/MessagesThreadFragment;

    invoke-virtual {v0}, Lcom/twitter/android/MessagesThreadFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0f0283    # com.twitter.android.R.string.message_delete_failed

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method
