.class Lcom/twitter/android/pn;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/widget/j;


# instance fields
.field final synthetic a:Lcom/twitter/library/widget/ObservableScrollView;

.field final synthetic b:Lcom/twitter/android/PostActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/PostActivity;Lcom/twitter/library/widget/ObservableScrollView;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/pn;->b:Lcom/twitter/android/PostActivity;

    iput-object p2, p0, Lcom/twitter/android/pn;->a:Lcom/twitter/library/widget/ObservableScrollView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/pn;->b:Lcom/twitter/android/PostActivity;

    invoke-static {v0}, Lcom/twitter/android/PostActivity;->c(Lcom/twitter/android/PostActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/pn;->b:Lcom/twitter/android/PostActivity;

    iget-object v0, v0, Lcom/twitter/android/PostActivity;->k:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/pn;->a:Lcom/twitter/library/widget/ObservableScrollView;

    new-instance v1, Lcom/twitter/android/po;

    invoke-direct {v1, p0}, Lcom/twitter/android/po;-><init>(Lcom/twitter/android/pn;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/ObservableScrollView;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method public a(IIII)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/widget/ObservableScrollView;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/pn;->b:Lcom/twitter/android/PostActivity;

    const/4 v1, 0x3

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/PostActivity;->a(IZ)V

    return-void
.end method

.method public a(Lcom/twitter/library/widget/ObservableScrollView;IIII)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/pn;->b:Lcom/twitter/android/PostActivity;

    invoke-virtual {v0, p3}, Lcom/twitter/android/PostActivity;->b(I)V

    if-nez p3, :cond_0

    iget-object v0, p0, Lcom/twitter/android/pn;->b:Lcom/twitter/android/PostActivity;

    invoke-static {v0}, Lcom/twitter/android/PostActivity;->c(Lcom/twitter/android/PostActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/pn;->b:Lcom/twitter/android/PostActivity;

    iget-object v0, v0, Lcom/twitter/android/PostActivity;->k:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/pn;->b:Lcom/twitter/android/PostActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/android/PostActivity;->a(Lcom/twitter/android/PostActivity;Z)Z

    :cond_0
    return-void
.end method

.method public b()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/pn;->b:Lcom/twitter/android/PostActivity;

    iget-object v0, v0, Lcom/twitter/android/PostActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetBoxFragment;->a()V

    iget-object v0, p0, Lcom/twitter/android/pn;->b:Lcom/twitter/android/PostActivity;

    iget-object v0, v0, Lcom/twitter/android/PostActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    iget-object v1, p0, Lcom/twitter/android/pn;->b:Lcom/twitter/android/PostActivity;

    iget-object v1, v1, Lcom/twitter/android/PostActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v1}, Lcom/twitter/android/TweetBoxFragment;->l()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetBoxFragment;->a(I)V

    iget-object v0, p0, Lcom/twitter/android/pn;->b:Lcom/twitter/android/PostActivity;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/PostActivity;->a(IZ)V

    return-void
.end method
