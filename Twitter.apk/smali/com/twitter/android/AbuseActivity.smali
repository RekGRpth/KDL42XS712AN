.class public Lcom/twitter/android/AbuseActivity;
.super Lcom/twitter/android/client/BaseFragmentActivity;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Lcom/twitter/library/scribe/ScribeAssociation;

.field private b:J

.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    iget-wide v0, p0, Lcom/twitter/android/AbuseActivity;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const-string/jumbo v0, "profile::report_user_abusive"

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/AbuseActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/AbuseActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object p1, v3, v0

    const/4 v0, 0x2

    aput-object p2, v3, v0

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/AbuseActivity;->a:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void

    :cond_0
    const-string/jumbo v0, "tweet::report_tweet_abusive"

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 5

    const/4 v4, 0x0

    new-instance v1, Lcom/twitter/android/client/z;

    invoke-direct {v1, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    invoke-virtual {p0}, Lcom/twitter/android/AbuseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "mute_enabled"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/AbuseActivity;->c:Z

    iget-boolean v0, p0, Lcom/twitter/android/AbuseActivity;->c:Z

    if-eqz v0, :cond_0

    const v0, 0x7f030001    # com.twitter.android.R.layout.abuse_fragment

    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/android/client/z;->d(I)V

    invoke-virtual {v1, v4}, Lcom/twitter/android/client/z;->a(Z)V

    return-object v1

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/AbuseActivity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const v2, 0x7f1000ff    # com.twitter.android.R.style.Theme_Settings

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    const v0, 0x7f030002    # com.twitter.android.R.layout.abuse_fragment_legacy

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 4

    const-wide/16 v2, 0x0

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V

    invoke-virtual {p0}, Lcom/twitter/android/AbuseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v0, "association"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/scribe/ScribeAssociation;

    iput-object v0, p0, Lcom/twitter/android/AbuseActivity;->a:Lcom/twitter/library/scribe/ScribeAssociation;

    const-string/jumbo v0, "status_id"

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/AbuseActivity;->b:J

    const/4 v0, 0x0

    const-string/jumbo v1, "impression"

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/AbuseActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/twitter/android/AbuseActivity;->c:Z

    if-eqz v0, :cond_1

    const v0, 0x7f090067    # com.twitter.android.R.id.cancel

    invoke-virtual {p0, v0}, Lcom/twitter/android/AbuseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f0f03c1    # com.twitter.android.R.string.select_abuse_type

    invoke-virtual {p0, v0}, Lcom/twitter/android/AbuseActivity;->setTitle(I)V

    iget-wide v0, p0, Lcom/twitter/android/AbuseActivity;->b:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const v0, 0x7f090061    # com.twitter.android.R.id.abuse_header

    invoke-virtual {p0, v0}, Lcom/twitter/android/AbuseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0f0005    # com.twitter.android.R.string.abuse_account_header

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v0, 0x7f0f03c3    # com.twitter.android.R.string.select_form

    invoke-virtual {p0, v0}, Lcom/twitter/android/AbuseActivity;->setTitle(I)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 5

    iget-boolean v0, p0, Lcom/twitter/android/AbuseActivity;->c:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/AbuseActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/AbuseActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "tweet::report_tweet::cancel"

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/AbuseActivity;->a:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_0
    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onBackPressed()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 13

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v12, -0x1

    const/4 v0, 0x0

    const/4 v11, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v4

    invoke-virtual {p0}, Lcom/twitter/android/AbuseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v8

    iget-wide v1, p0, Lcom/twitter/android/AbuseActivity;->b:J

    const-string/jumbo v3, "spammer_username"

    invoke-virtual {v8, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    :goto_0
    const v5, 0x7f090062    # com.twitter.android.R.id.impersonation

    if-ne v4, v5, :cond_1

    const v0, 0x7f0f01e2    # com.twitter.android.R.string.impersonation_url

    invoke-virtual {p0, v0}, Lcom/twitter/android/AbuseActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v0, 0x7f0f01e1    # com.twitter.android.R.string.impersonation

    invoke-virtual {p0, v0}, Lcom/twitter/android/AbuseActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v4, "impersonation"

    move-object v9, v3

    move-object v3, v4

    :goto_1
    const-string/jumbo v4, "click"

    invoke-direct {p0, v3, v4}, Lcom/twitter/android/AbuseActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/twitter/android/AbuseActivity;->c:Z

    if-eqz v3, :cond_6

    if-eqz v9, :cond_6

    const-string/jumbo v1, "out_abuse_type"

    invoke-virtual {v8, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v0, "out_abuse_url"

    invoke-virtual {v8, v0, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v12, v8}, Lcom/twitter/android/AbuseActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/twitter/android/AbuseActivity;->finish()V

    :goto_2
    return-void

    :cond_0
    const-string/jumbo v3, ""

    goto :goto_0

    :cond_1
    const v5, 0x7f090063    # com.twitter.android.R.id.trademarks

    if-ne v4, v5, :cond_2

    const v0, 0x7f0f04b1    # com.twitter.android.R.string.trademarks_url

    invoke-virtual {p0, v0}, Lcom/twitter/android/AbuseActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v0, 0x7f0f04b0    # com.twitter.android.R.string.trademarks

    invoke-virtual {p0, v0}, Lcom/twitter/android/AbuseActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v4, "trademarks"

    move-object v9, v3

    move-object v3, v4

    goto :goto_1

    :cond_2
    const v5, 0x7f090064    # com.twitter.android.R.id.harassment

    if-ne v4, v5, :cond_3

    const v0, 0x7f0f01be    # com.twitter.android.R.string.harassment_url

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v3, v4, v11

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v4, v6

    invoke-virtual {p0, v0, v4}, Lcom/twitter/android/AbuseActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const v0, 0x7f0f01bd    # com.twitter.android.R.string.harassment

    invoke-virtual {p0, v0}, Lcom/twitter/android/AbuseActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v4, "harassment"

    move-object v9, v3

    move-object v3, v4

    goto :goto_1

    :cond_3
    const v5, 0x7f090065    # com.twitter.android.R.id.report_self_harm

    if-ne v4, v5, :cond_4

    const v0, 0x7f0f0359    # com.twitter.android.R.string.report_self_harm_url

    invoke-virtual {p0, v0}, Lcom/twitter/android/AbuseActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    const v0, 0x7f0f0358    # com.twitter.android.R.string.report_self_harm

    invoke-virtual {p0, v0}, Lcom/twitter/android/AbuseActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v4, "report_self_harm"

    move-object v9, v3

    move-object v3, v4

    goto :goto_1

    :cond_4
    const v5, 0x7f090066    # com.twitter.android.R.id.report_an_ad

    if-ne v4, v5, :cond_5

    const v0, 0x7f0f0355    # com.twitter.android.R.string.report_an_ad_url

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v3, v4, v11

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v4, v6

    invoke-virtual {p0, v0, v4}, Lcom/twitter/android/AbuseActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const v0, 0x7f0f0354    # com.twitter.android.R.string.report_an_ad

    invoke-virtual {p0, v0}, Lcom/twitter/android/AbuseActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v4, "report_an_ad"

    move-object v9, v3

    move-object v3, v4

    goto/16 :goto_1

    :cond_5
    const v3, 0x7f090067    # com.twitter.android.R.id.cancel

    if-ne v4, v3, :cond_7

    invoke-virtual {p0}, Lcom/twitter/android/AbuseActivity;->onBackPressed()V

    goto/16 :goto_2

    :cond_6
    invoke-virtual {p0}, Lcom/twitter/android/AbuseActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    const-string/jumbo v3, "spammer_id"

    const-wide/16 v4, 0x0

    invoke-virtual {v8, v3, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    const-string/jumbo v5, "pc_impression_id"

    invoke-virtual {v8, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "pc_earned"

    invoke-virtual {v8, v6, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    const-string/jumbo v7, "abuse"

    const-string/jumbo v10, "should_block"

    invoke-virtual {v8, v10, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v8

    invoke-virtual/range {v0 .. v8}, Lcom/twitter/android/client/c;->a(JJLjava/lang/String;ZLjava/lang/String;Z)Ljava/lang/String;

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/WebViewActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/AbuseActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0, v12}, Lcom/twitter/android/AbuseActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/twitter/android/AbuseActivity;->finish()V

    goto/16 :goto_2

    :cond_7
    move-object v9, v0

    move-object v3, v0

    goto/16 :goto_1
.end method
