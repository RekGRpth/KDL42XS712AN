.class Lcom/twitter/android/wx;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/in;


# instance fields
.field final synthetic a:Lcom/twitter/android/TweetBoxFragment;


# direct methods
.method private constructor <init>(Lcom/twitter/android/TweetBoxFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/wx;->a:Lcom/twitter/android/TweetBoxFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/TweetBoxFragment;Lcom/twitter/android/wq;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/wx;-><init>(Lcom/twitter/android/TweetBoxFragment;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    new-instance v0, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v1, p0, Lcom/twitter/android/wx;->a:Lcom/twitter/android/TweetBoxFragment;

    invoke-static {v1}, Lcom/twitter/android/TweetBoxFragment;->i(Lcom/twitter/android/TweetBoxFragment;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/wx;->a:Lcom/twitter/android/TweetBoxFragment;

    invoke-static {v3}, Lcom/twitter/android/TweetBoxFragment;->h(Lcom/twitter/android/TweetBoxFragment;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "composition"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "reply_dialog"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "cancel"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "click"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/wx;->a:Lcom/twitter/android/TweetBoxFragment;

    invoke-static {v1}, Lcom/twitter/android/TweetBoxFragment;->j(Lcom/twitter/android/TweetBoxFragment;)Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method

.method public a(J)V
    .locals 0

    return-void
.end method

.method public a(Ljava/util/HashSet;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/wx;->a:Lcom/twitter/android/TweetBoxFragment;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v1}, Lcom/twitter/android/TweetBoxFragment;->a(Lcom/twitter/android/TweetBoxFragment;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/twitter/android/wx;->a:Lcom/twitter/android/TweetBoxFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetBoxFragment;->e(Lcom/twitter/android/TweetBoxFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/RepliedUser;

    iget-wide v2, v0, Lcom/twitter/library/api/RepliedUser;->userId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/wx;->a:Lcom/twitter/android/TweetBoxFragment;

    invoke-static {v2}, Lcom/twitter/android/TweetBoxFragment;->f(Lcom/twitter/android/TweetBoxFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/wx;->a:Lcom/twitter/android/TweetBoxFragment;

    iget-object v1, p0, Lcom/twitter/android/wx;->a:Lcom/twitter/android/TweetBoxFragment;

    iget-object v2, p0, Lcom/twitter/android/wx;->a:Lcom/twitter/android/TweetBoxFragment;

    invoke-static {v2}, Lcom/twitter/android/TweetBoxFragment;->f(Lcom/twitter/android/TweetBoxFragment;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/twitter/android/TweetBoxFragment;->a(Lcom/twitter/android/TweetBoxFragment;Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/TweetBoxFragment;->a(Lcom/twitter/android/TweetBoxFragment;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/wx;->a:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetBoxFragment;->q()V

    iget-object v0, p0, Lcom/twitter/android/wx;->a:Lcom/twitter/android/TweetBoxFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetBoxFragment;->g(Lcom/twitter/android/TweetBoxFragment;)V

    new-instance v0, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v1, p0, Lcom/twitter/android/wx;->a:Lcom/twitter/android/TweetBoxFragment;

    invoke-static {v1}, Lcom/twitter/android/TweetBoxFragment;->i(Lcom/twitter/android/TweetBoxFragment;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/wx;->a:Lcom/twitter/android/TweetBoxFragment;

    invoke-static {v3}, Lcom/twitter/android/TweetBoxFragment;->h(Lcom/twitter/android/TweetBoxFragment;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "composition"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "reply_dialog"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "done"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "click"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {p1}, Ljava/util/HashSet;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->e(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    iget-object v1, p0, Lcom/twitter/android/wx;->a:Lcom/twitter/android/TweetBoxFragment;

    invoke-static {v1}, Lcom/twitter/android/TweetBoxFragment;->j(Lcom/twitter/android/TweetBoxFragment;)Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method
