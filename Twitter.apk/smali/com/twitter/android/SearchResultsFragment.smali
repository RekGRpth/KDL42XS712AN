.class public Lcom/twitter/android/SearchResultsFragment;
.super Lcom/twitter/android/SearchFragment;
.source "Twttr"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/twitter/android/vw;
.implements Lcom/twitter/android/zb;
.implements Lcom/twitter/library/util/ar;


# static fields
.field private static final J:Ljava/util/HashMap;


# instance fields
.field private K:I

.field private L:J

.field private U:J

.field private V:Ljava/lang/String;

.field private W:Ljava/lang/String;

.field private X:I

.field private Y:Lcom/twitter/library/util/FriendshipCache;

.field private Z:Lcom/twitter/android/ob;

.field private aa:Z

.field private ab:Ljava/lang/String;

.field private ac:Lcom/twitter/library/util/aa;

.field private ad:Z

.field private ae:Z

.field private af:Ljava/lang/String;

.field private ag:Ljava/util/HashSet;

.field private ah:Lcom/twitter/android/yb;

.field private ai:Lcom/twitter/android/yb;

.field private aj:Lcom/twitter/android/yb;

.field private final ak:Ljava/util/ArrayList;

.field private final al:Ljava/util/HashSet;

.field z:Lcom/twitter/android/sh;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/16 v3, 0x8

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v3}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/twitter/android/SearchResultsFragment;->J:Ljava/util/HashMap;

    sget-object v0, Lcom/twitter/android/SearchResultsFragment;->J:Ljava/util/HashMap;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "universal_all"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/SearchResultsFragment;->J:Ljava/util/HashMap;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "users"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/SearchResultsFragment;->J:Ljava/util/HashMap;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "photo_tweets"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/SearchResultsFragment;->J:Ljava/util/HashMap;

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "videos_all"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/SearchResultsFragment;->J:Ljava/util/HashMap;

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "videos_vines"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/SearchResultsFragment;->J:Ljava/util/HashMap;

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "news"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/SearchResultsFragment;->J:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "top_lists"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/SearchResultsFragment;->J:Ljava/util/HashMap;

    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string/jumbo v2, "top_timelines"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/SearchFragment;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/SearchResultsFragment;->X:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->ak:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->al:Ljava/util/HashSet;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/SearchResultsFragment;I)I
    .locals 0

    iput p1, p0, Lcom/twitter/android/SearchResultsFragment;->X:I

    return p1
.end method

.method private a(Lcom/twitter/android/yb;Ljava/lang/String;Z)Lcom/twitter/android/yb;
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/twitter/android/yb;->a()Z

    move-result v0

    if-eq v0, p3, :cond_1

    :cond_0
    invoke-direct {p0, p2, p3}, Lcom/twitter/android/SearchResultsFragment;->b(Ljava/lang/String;Z)Lcom/twitter/android/yb;

    move-result-object p1

    :cond_1
    return-object p1
.end method

.method private a(Ljava/lang/String;Z)Lcom/twitter/android/yb;
    .locals 2

    const/4 v0, -0x1

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_1
    return-object v0

    :sswitch_0
    const-string/jumbo v1, "tweet"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string/jumbo v1, "news"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string/jumbo v1, "highlight"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->ah:Lcom/twitter/android/yb;

    invoke-direct {p0, v0, p1, p2}, Lcom/twitter/android/SearchResultsFragment;->a(Lcom/twitter/android/yb;Ljava/lang/String;Z)Lcom/twitter/android/yb;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->ah:Lcom/twitter/android/yb;

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->ai:Lcom/twitter/android/yb;

    invoke-direct {p0, v0, p1, p2}, Lcom/twitter/android/SearchResultsFragment;->a(Lcom/twitter/android/yb;Ljava/lang/String;Z)Lcom/twitter/android/yb;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->ai:Lcom/twitter/android/yb;

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->aj:Lcom/twitter/android/yb;

    invoke-direct {p0, v0, p1, p2}, Lcom/twitter/android/SearchResultsFragment;->a(Lcom/twitter/android/yb;Ljava/lang/String;Z)Lcom/twitter/android/yb;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->aj:Lcom/twitter/android/yb;

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x289a734c -> :sswitch_2
        0x338ad3 -> :sswitch_1
        0x69a4671 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic a(Lcom/twitter/android/SearchResultsFragment;)Ljava/util/HashSet;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->al:Ljava/util/HashSet;

    return-object v0
.end method

.method private a(Ljava/lang/String;Lcom/twitter/library/api/af;)V
    .locals 6

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->T()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/twitter/android/SearchResultsFragment;->K:I

    invoke-static {v3}, Lcom/twitter/android/SearchResultsFragment;->d_(I)Ljava/lang/String;

    move-result-object v3

    iget-boolean v4, p0, Lcom/twitter/android/SearchResultsFragment;->j:Z

    iget-boolean v5, p0, Lcom/twitter/android/SearchResultsFragment;->i:Z

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/SearchResultsFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-static {p2}, Lcom/twitter/library/scribe/ScribeItem;->a(Lcom/twitter/library/api/af;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method

.method private a(IJ)Z
    .locals 13

    invoke-virtual {p0, p1}, Lcom/twitter/android/SearchResultsFragment;->c_(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iput p1, p0, Lcom/twitter/android/SearchResultsFragment;->n:I

    invoke-virtual {p0, p1}, Lcom/twitter/android/SearchResultsFragment;->a_(I)V

    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->z:Lcom/twitter/android/sh;

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid fetch type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const/4 v10, 0x0

    const/4 v9, 0x0

    :goto_1
    new-instance v0, Lcom/twitter/library/scribe/ScribeLog;

    iget-wide v1, p0, Lcom/twitter/android/SearchResultsFragment;->Q:J

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/SearchResultsFragment;->h:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->M()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, p1}, Lcom/twitter/android/SearchResultsFragment;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->c:Ljava/lang/String;

    iget v2, p0, Lcom/twitter/android/SearchResultsFragment;->K:I

    invoke-static {v2}, Lcom/twitter/android/SearchResultsFragment;->d_(I)Ljava/lang/String;

    move-result-object v2

    iget-boolean v3, p0, Lcom/twitter/android/SearchResultsFragment;->j:Z

    iget-boolean v4, p0, Lcom/twitter/android/SearchResultsFragment;->i:Z

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v12

    new-instance v0, Lcom/twitter/library/api/search/c;

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    iget-object v5, p0, Lcom/twitter/android/SearchResultsFragment;->c:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->G()I

    move-result v6

    iget-object v7, p0, Lcom/twitter/android/SearchResultsFragment;->d:Ljava/lang/String;

    iget-object v8, p0, Lcom/twitter/android/SearchResultsFragment;->b:Ljava/lang/String;

    iget-object v11, p0, Lcom/twitter/android/SearchResultsFragment;->g:Ljava/lang/String;

    move-wide v3, p2

    invoke-direct/range {v0 .. v11}, Lcom/twitter/library/api/search/c;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    iget v1, p0, Lcom/twitter/android/SearchResultsFragment;->K:I

    iget-boolean v2, p0, Lcom/twitter/android/SearchResultsFragment;->j:Z

    iget-boolean v3, p0, Lcom/twitter/android/SearchResultsFragment;->k:Z

    iget-boolean v4, p0, Lcom/twitter/android/SearchResultsFragment;->l:Z

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/library/api/search/c;->a(IZZZ)Lcom/twitter/library/api/search/c;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/android/SearchResultsFragment;->L:J

    iget-wide v3, p0, Lcom/twitter/android/SearchResultsFragment;->U:J

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/library/api/search/c;->a(JJ)Lcom/twitter/library/api/search/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->W:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/SearchResultsFragment;->V:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/api/search/c;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/api/search/c;

    move-result-object v0

    const-string/jumbo v1, "scribe_log"

    invoke-virtual {v0, v1, v12}, Lcom/twitter/library/api/search/c;->a(Ljava/lang/String;Landroid/os/Parcelable;)Lcom/twitter/library/service/b;

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->c(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;

    iget-boolean v2, p0, Lcom/twitter/android/SearchResultsFragment;->i:Z

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->d(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;

    :cond_1
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1, p1}, Lcom/twitter/android/SearchResultsFragment;->a(Lcom/twitter/library/service/b;II)Z

    const/4 v0, 0x1

    goto/16 :goto_0

    :pswitch_1
    const/4 v10, 0x1

    invoke-virtual {v0}, Lcom/twitter/android/sh;->d()I

    move-result v9

    goto/16 :goto_1

    :pswitch_2
    const/4 v10, 0x1

    invoke-virtual {v0}, Lcom/twitter/android/sh;->d()I

    move-result v9

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->ak()V

    goto/16 :goto_1

    :pswitch_3
    const/4 v10, 0x2

    invoke-virtual {v0}, Lcom/twitter/android/sh;->e()I

    move-result v9

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private aC()V
    .locals 3

    iget v0, p0, Lcom/twitter/android/SearchResultsFragment;->K:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    new-instance v0, Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/library/scribe/ScribeAssociation;-><init>()V

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->a(I)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/android/SearchResultsFragment;->Q:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->a(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    const-string/jumbo v1, "people"

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchResultsFragment;->b(Lcom/twitter/library/scribe/ScribeAssociation;)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/library/scribe/ScribeAssociation;-><init>()V

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->a(I)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->M()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchResultsFragment;->b(Lcom/twitter/library/scribe/ScribeAssociation;)V

    goto :goto_0
.end method

.method private aD()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->W:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aE()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->V:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/lang/String;Z)Lcom/twitter/android/yb;
    .locals 16

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/SearchResultsFragment;->h:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/SearchResultsFragment;->M()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/SearchResultsFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    const-string/jumbo v5, "avatar"

    const-string/jumbo v6, "profile_click"

    move-object/from16 v0, p1

    invoke-static {v4, v0, v5, v6}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v2, v4, v6

    const/4 v6, 0x1

    aput-object v3, v4, v6

    const/4 v6, 0x2

    aput-object p1, v4, v6

    const/4 v6, 0x3

    const-string/jumbo v7, "link"

    aput-object v7, v4, v6

    const/4 v6, 0x4

    const-string/jumbo v7, "open_link"

    aput-object v7, v4, v6

    invoke-static {v1, v4}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/StringBuilder;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v2, v4, v7

    const/4 v7, 0x1

    aput-object v3, v4, v7

    const/4 v7, 0x2

    aput-object p1, v4, v7

    const/4 v7, 0x3

    const-string/jumbo v8, "platform_photo_card"

    aput-object v8, v4, v7

    const/4 v7, 0x4

    const-string/jumbo v8, "click"

    aput-object v8, v4, v7

    invoke-static {v1, v4}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/StringBuilder;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/String;

    const/4 v8, 0x0

    aput-object v2, v4, v8

    const/4 v8, 0x1

    aput-object v3, v4, v8

    const/4 v8, 0x2

    aput-object p1, v4, v8

    const/4 v8, 0x3

    const-string/jumbo v9, "platform_player_card"

    aput-object v9, v4, v8

    const/4 v8, 0x4

    const-string/jumbo v9, "click"

    aput-object v9, v4, v8

    invoke-static {v1, v4}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/StringBuilder;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/String;

    const/4 v9, 0x0

    aput-object v2, v4, v9

    const/4 v2, 0x1

    aput-object v3, v4, v2

    const/4 v2, 0x2

    aput-object p1, v4, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "platform_summary_card"

    aput-object v3, v4, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "open_link"

    aput-object v3, v4, v2

    invoke-static {v1, v4}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/StringBuilder;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    if-eqz p2, :cond_0

    new-instance v1, Lcom/twitter/android/yb;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/SearchResultsFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/SearchResultsFragment;->c:Ljava/lang/String;

    sget-object v2, Lcom/twitter/library/provider/aq;->a:Landroid/net/Uri;

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/SearchResultsFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v10

    invoke-virtual {v10}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v10

    invoke-static {v2, v10, v11}, Lcom/twitter/library/provider/w;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v10

    sget-object v11, Lcom/twitter/library/provider/bs;->a:[Ljava/lang/String;

    const-string/jumbo v12, "flags&1|512!=0 AND search_id=?"

    const/4 v2, 0x1

    new-array v13, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/twitter/android/SearchResultsFragment;->q:J

    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v14

    aput-object v14, v13, v2

    const-string/jumbo v14, "type_id ASC, _id ASC"

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v14}, Lcom/twitter/android/yb;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/twitter/android/yb;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/SearchResultsFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/SearchResultsFragment;->c:Ljava/lang/String;

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v9}, Lcom/twitter/android/yb;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/SearchResultsFragment;)Lcom/twitter/library/scribe/ScribeAssociation;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/SearchResultsFragment;)Lcom/twitter/library/scribe/ScribeAssociation;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/SearchResultsFragment;)Lcom/twitter/library/scribe/ScribeAssociation;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/SearchResultsFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/SearchResultsFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/android/SearchResultsFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/android/SearchResultsFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/android/SearchResultsFragment;)Z
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/SearchResultsFragment;->aD()Z

    move-result v0

    return v0
.end method

.method static synthetic j(Lcom/twitter/android/SearchResultsFragment;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->W:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic k(Lcom/twitter/android/SearchResultsFragment;)I
    .locals 1

    iget v0, p0, Lcom/twitter/android/SearchResultsFragment;->X:I

    return v0
.end method

.method static synthetic l(Lcom/twitter/android/SearchResultsFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic m(Lcom/twitter/android/SearchResultsFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic n(Lcom/twitter/android/SearchResultsFragment;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->ak:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic o(Lcom/twitter/android/SearchResultsFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public L()I
    .locals 1

    iget v0, p0, Lcom/twitter/android/SearchResultsFragment;->K:I

    return v0
.end method

.method protected M()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/twitter/android/SearchResultsFragment;->X:I

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchResultsFragment;->g(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected N()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x2

    iget v0, p0, Lcom/twitter/android/SearchResultsFragment;->K:I

    if-ne v0, v3, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":people:users::impression"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchResultsFragment;->b(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/android/SearchResultsFragment;->h:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->M()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    aput-object v4, v0, v3

    const/4 v1, 0x3

    aput-object v4, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "impression"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchResultsFragment;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected O()V
    .locals 7

    const/4 v4, 0x4

    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->ak:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, p0, Lcom/twitter/android/SearchResultsFragment;->X:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/SearchResultsFragment;->aD()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->h:Ljava/lang/String;

    aput-object v1, v0, v5

    iget v1, p0, Lcom/twitter/android/SearchResultsFragment;->X:I

    invoke-virtual {p0, v1}, Lcom/twitter/android/SearchResultsFragment;->g(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v6

    const-string/jumbo v1, "stream"

    aput-object v1, v0, v2

    const/4 v1, 0x0

    aput-object v1, v0, v3

    const-string/jumbo v1, "results"

    aput-object v1, v0, v4

    invoke-static {v0}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v3, v6, [Ljava/lang/String;

    aput-object v0, v3, v5

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->T()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/twitter/android/SearchResultsFragment;->K:I

    invoke-static {v3}, Lcom/twitter/android/SearchResultsFragment;->d_(I)Ljava/lang/String;

    move-result-object v3

    iget-boolean v4, p0, Lcom/twitter/android/SearchResultsFragment;->j:Z

    iget-boolean v5, p0, Lcom/twitter/android/SearchResultsFragment;->i:Z

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/SearchResultsFragment;->ak:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/util/ArrayList;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->ak:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->h:Ljava/lang/String;

    aput-object v1, v0, v5

    const-string/jumbo v1, "universal_top"

    aput-object v1, v0, v6

    iget v1, p0, Lcom/twitter/android/SearchResultsFragment;->X:I

    invoke-static {v1}, Lcom/twitter/library/api/TwitterTopic;->b(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v2

    const-string/jumbo v1, "event"

    aput-object v1, v0, v3

    const-string/jumbo v1, "results"

    aput-object v1, v0, v4

    invoke-static {v0}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected P()Z
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    const/4 v0, 0x1

    return v0
.end method

.method protected Q()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/SearchFragment;->Q()V

    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->ac:Lcom/twitter/library/util/aa;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->f(Landroid/content/Context;)Lcom/twitter/library/util/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->ac:Lcom/twitter/library/util/aa;

    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->ac:Lcom/twitter/library/util/aa;

    invoke-virtual {v0, p0}, Lcom/twitter/library/util/aa;->a(Lcom/twitter/library/util/ac;)V

    :cond_0
    return-void
.end method

.method protected R()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/SearchFragment;->R()V

    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->ac:Lcom/twitter/library/util/aa;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->ac:Lcom/twitter/library/util/aa;

    invoke-virtual {v0, p0}, Lcom/twitter/library/util/aa;->b(Lcom/twitter/library/util/ac;)V

    :cond_0
    return-void
.end method

.method public S()V
    .locals 7

    const/4 v6, 0x2

    const/4 v5, 0x1

    invoke-direct {p0}, Lcom/twitter/android/SearchResultsFragment;->aD()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->M()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v0, "user_rail"

    :goto_0
    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/SearchResultsFragment;->h:Ljava/lang/String;

    aput-object v4, v2, v3

    aput-object v1, v2, v5

    aput-object v0, v2, v6

    const/4 v0, 0x3

    const-string/jumbo v1, "more"

    aput-object v1, v2, v0

    const/4 v0, 0x4

    const-string/jumbo v1, "search"

    aput-object v1, v2, v0

    invoke-static {v2}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchResultsFragment;->b(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/SearchTerminalActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "query"

    iget-object v2, p0, Lcom/twitter/android/SearchResultsFragment;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "query_name"

    iget-object v2, p0, Lcom/twitter/android/SearchResultsFragment;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "search_type"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "terminal"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchResultsFragment;->startActivity(Landroid/content/Intent;)V

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->M()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v0, "user_gallery"

    goto :goto_0
.end method

.method protected T()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/SearchResultsFragment;->aE()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->V:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/SearchResultsFragment;->aD()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->W:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public a(JLcom/twitter/library/api/PromotedContent;ILcom/twitter/android/zd;)V
    .locals 10

    const/4 v7, 0x5

    const/4 v9, 0x1

    const/4 v8, 0x0

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "user_id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "screen_name"

    iget-object v2, p5, Lcom/twitter/android/zd;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    if-eqz v1, :cond_0

    const-string/jumbo v1, "association"

    iget-wide v2, p0, Lcom/twitter/android/SearchResultsFragment;->Q:J

    iget-object v4, p0, Lcom/twitter/android/SearchResultsFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-static {v7, v2, v3, v4}, Lcom/twitter/library/scribe/ScribeAssociation;->a(IJLcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v6

    if-eqz p3, :cond_1

    sget-object v1, Lcom/twitter/library/api/PromotedEvent;->d:Lcom/twitter/library/api/PromotedEvent;

    invoke-virtual {v6, v1, p3}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/library/api/PromotedContent;)V

    const-string/jumbo v1, "pc"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    :cond_1
    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchResultsFragment;->startActivity(Landroid/content/Intent;)V

    invoke-direct {p0}, Lcom/twitter/android/SearchResultsFragment;->aD()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->M()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v1, "user_rail"

    const-string/jumbo v0, "avatar"

    :goto_0
    new-instance v3, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v4, v9, [Ljava/lang/String;

    new-array v5, v7, [Ljava/lang/String;

    iget-object v7, p0, Lcom/twitter/android/SearchResultsFragment;->h:Ljava/lang/String;

    aput-object v7, v5, v8

    aput-object v2, v5, v9

    const/4 v2, 0x2

    aput-object v1, v5, v2

    const/4 v1, 0x3

    aput-object v0, v5, v1

    const/4 v0, 0x4

    const-string/jumbo v1, "profile_click"

    aput-object v1, v5, v0

    invoke-static {v5}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v8

    invoke-virtual {v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->c:Ljava/lang/String;

    iget v2, p0, Lcom/twitter/android/SearchResultsFragment;->K:I

    invoke-static {v2}, Lcom/twitter/android/SearchResultsFragment;->d_(I)Ljava/lang/String;

    move-result-object v2

    iget-boolean v3, p0, Lcom/twitter/android/SearchResultsFragment;->j:Z

    iget-boolean v4, p0, Lcom/twitter/android/SearchResultsFragment;->i:Z

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    const/4 v4, 0x0

    move-wide v1, p1

    move-object v3, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/scribe/ScribeLog;->a(JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;I)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->M()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v1, "user_gallery"

    const-string/jumbo v0, "user"

    goto :goto_0
.end method

.method protected a(Landroid/content/Context;)V
    .locals 12

    const/4 v10, 0x1

    new-instance v0, Lcom/twitter/library/api/search/c;

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    iget-wide v3, p0, Lcom/twitter/android/SearchResultsFragment;->q:J

    iget-object v5, p0, Lcom/twitter/android/SearchResultsFragment;->c:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->G()I

    move-result v6

    iget-object v7, p0, Lcom/twitter/android/SearchResultsFragment;->d:Ljava/lang/String;

    iget-object v8, p0, Lcom/twitter/android/SearchResultsFragment;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->z:Lcom/twitter/android/sh;

    invoke-virtual {v1}, Lcom/twitter/android/sh;->d()I

    move-result v1

    iget v9, p0, Lcom/twitter/android/SearchResultsFragment;->t:I

    sub-int v9, v1, v9

    iget-object v11, p0, Lcom/twitter/android/SearchResultsFragment;->g:Ljava/lang/String;

    move-object v1, p1

    invoke-direct/range {v0 .. v11}, Lcom/twitter/library/api/search/c;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLjava/lang/String;ILjava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    iget v1, p0, Lcom/twitter/android/SearchResultsFragment;->K:I

    iget-boolean v2, p0, Lcom/twitter/android/SearchResultsFragment;->j:Z

    iget-boolean v3, p0, Lcom/twitter/android/SearchResultsFragment;->k:Z

    iget-boolean v4, p0, Lcom/twitter/android/SearchResultsFragment;->l:Z

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/library/api/search/c;->a(IZZZ)Lcom/twitter/library/api/search/c;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/android/SearchResultsFragment;->L:J

    iget-wide v3, p0, Lcom/twitter/android/SearchResultsFragment;->U:J

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/library/api/search/c;->a(JJ)Lcom/twitter/library/api/search/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->W:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/SearchResultsFragment;->V:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/api/search/c;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/api/search/c;

    move-result-object v0

    invoke-virtual {v0, v10}, Lcom/twitter/library/api/search/c;->c(Z)Lcom/twitter/library/service/b;

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->c(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;

    iget-boolean v2, p0, Lcom/twitter/android/SearchResultsFragment;->i:Z

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->d(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;

    :cond_0
    const/4 v1, 0x4

    invoke-virtual {p0, v0, v10, v1}, Lcom/twitter/android/SearchResultsFragment;->a(Lcom/twitter/library/service/b;II)Z

    return-void
.end method

.method protected a(Landroid/content/Context;IILcom/twitter/library/service/b;)V
    .locals 8

    const/4 v2, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x3

    const/4 v5, 0x1

    invoke-super {p0, p1, p2, p3, p4}, Lcom/twitter/android/SearchFragment;->a(Landroid/content/Context;IILcom/twitter/library/service/b;)V

    if-ne p2, v2, :cond_1

    iget-object v0, p4, Lcom/twitter/library/service/b;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchResultsFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    check-cast p4, Lcom/twitter/library/api/search/c;

    invoke-virtual {p4}, Lcom/twitter/library/api/search/c;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-nez v0, :cond_2

    const v0, 0x7f0f03b8    # com.twitter.android.R.string.search_status_fetch_error

    invoke-static {p1, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0, p3}, Lcom/twitter/android/SearchResultsFragment;->b(I)V

    :cond_0
    :goto_0
    invoke-virtual {p4}, Lcom/twitter/library/api/search/c;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->af:Ljava/lang/String;

    invoke-virtual {p4}, Lcom/twitter/library/api/search/c;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/SearchResultsFragment;->y:J

    :cond_1
    return-void

    :cond_2
    if-ne p3, v6, :cond_5

    invoke-virtual {p4}, Lcom/twitter/library/api/search/c;->k()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/SearchResultsFragment;->aa:Z

    invoke-virtual {p4}, Lcom/twitter/library/api/search/c;->i()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->W:Ljava/lang/String;

    invoke-virtual {p4}, Lcom/twitter/library/api/search/c;->j()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchResultsFragment;->a(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p4}, Lcom/twitter/library/api/search/c;->e()I

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->h:Ljava/lang/String;

    aput-object v1, v0, v7

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->M()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v5

    const-string/jumbo v1, "stream"

    aput-object v1, v0, v2

    const/4 v1, 0x0

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const-string/jumbo v2, "no_results"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v3, v5, [Ljava/lang/String;

    aput-object v0, v3, v7

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/SearchResultsFragment;->c:Ljava/lang/String;

    iget v3, p0, Lcom/twitter/android/SearchResultsFragment;->K:I

    invoke-static {v3}, Lcom/twitter/android/SearchResultsFragment;->d_(I)Ljava/lang/String;

    move-result-object v3

    iget-boolean v4, p0, Lcom/twitter/android/SearchResultsFragment;->j:Z

    iget-boolean v5, p0, Lcom/twitter/android/SearchResultsFragment;->i:Z

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    invoke-virtual {p0, v6}, Lcom/twitter/android/SearchResultsFragment;->b(I)V

    :cond_4
    iget-boolean v0, p0, Lcom/twitter/android/SearchResultsFragment;->w:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->r()V

    goto :goto_0

    :cond_5
    invoke-virtual {p4}, Lcom/twitter/library/api/search/c;->e()I

    move-result v0

    if-nez v0, :cond_0

    if-ne p3, v5, :cond_6

    iput-boolean v5, p0, Lcom/twitter/android/SearchResultsFragment;->r:Z

    :cond_6
    invoke-virtual {p0, p3}, Lcom/twitter/android/SearchResultsFragment;->b(I)V

    goto/16 :goto_0
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v7, 0x3

    iget-object v3, p0, Lcom/twitter/android/SearchResultsFragment;->z:Lcom/twitter/android/sh;

    iget v4, p0, Lcom/twitter/android/SearchResultsFragment;->n:I

    iget-boolean v0, p0, Lcom/twitter/android/SearchResultsFragment;->ad:Z

    if-eqz v0, :cond_1

    if-ne v4, v7, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/SearchResultsFragment;->aC()V

    const-string/jumbo v5, "tweet"

    iget v0, p0, Lcom/twitter/android/SearchResultsFragment;->K:I

    if-ne v0, v7, :cond_3

    move v0, v1

    :goto_0
    invoke-direct {p0, v5, v0}, Lcom/twitter/android/SearchResultsFragment;->a(Ljava/lang/String;Z)Lcom/twitter/android/yb;

    move-result-object v0

    const-string/jumbo v5, "news"

    invoke-direct {p0, v5, v2}, Lcom/twitter/android/SearchResultsFragment;->a(Ljava/lang/String;Z)Lcom/twitter/android/yb;

    move-result-object v5

    const-string/jumbo v6, "highlight"

    invoke-direct {p0, v6, v2}, Lcom/twitter/android/SearchResultsFragment;->a(Ljava/lang/String;Z)Lcom/twitter/android/yb;

    move-result-object v2

    invoke-virtual {v3, v0, v5, v2}, Lcom/twitter/android/sh;->a(Lcom/twitter/library/widget/aa;Lcom/twitter/library/widget/aa;Lcom/twitter/library/widget/aa;)V

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->q()V

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->A()Lcom/twitter/refresh/widget/a;

    move-result-object v0

    invoke-virtual {v3, p2}, Lcom/twitter/android/sh;->a(Landroid/database/Cursor;)Landroid/database/Cursor;

    iget-boolean v2, p0, Lcom/twitter/android/SearchResultsFragment;->ad:Z

    if-eqz v2, :cond_4

    invoke-virtual {p0, v4}, Lcom/twitter/android/SearchResultsFragment;->b(I)V

    const/4 v2, 0x2

    if-ne v4, v2, :cond_2

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/SearchResultsFragment;->a(Lcom/twitter/refresh/widget/a;Z)V

    iput v7, p0, Lcom/twitter/android/SearchResultsFragment;->n:I

    :cond_2
    :goto_1
    return-void

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->z:Lcom/twitter/android/sh;

    invoke-virtual {v0}, Lcom/twitter/android/sh;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0, v7}, Lcom/twitter/android/SearchResultsFragment;->d(I)Z

    :cond_5
    iput-boolean v1, p0, Lcom/twitter/android/SearchResultsFragment;->ad:Z

    goto :goto_1
.end method

.method public a(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 15

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->V()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v1

    move/from16 v0, p3

    if-ge v0, v1, :cond_1

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v8

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v9

    iget v1, p0, Lcom/twitter/android/SearchResultsFragment;->K:I

    invoke-static {v1}, Lcom/twitter/android/SearchResultsFragment;->d_(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/sk;

    iget-object v2, v1, Lcom/twitter/android/sk;->l:Lcom/twitter/android/sp;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, v2, Lcom/twitter/android/sp;->b:I

    packed-switch v3, :pswitch_data_0

    :pswitch_1
    goto :goto_0

    :pswitch_2
    iget-object v1, v1, Lcom/twitter/android/sk;->a:Lcom/twitter/android/yd;

    iget-object v1, v1, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v1}, Lcom/twitter/library/widget/TweetView;->getTweet()Lcom/twitter/library/provider/Tweet;

    move-result-object v3

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const-class v7, Lcom/twitter/android/TweetActivity;

    invoke-direct {v1, v4, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v4, "tw"

    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v4, "association"

    iget-object v7, p0, Lcom/twitter/android/SearchResultsFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v1, v4, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/SearchResultsFragment;->startActivity(Landroid/content/Intent;)V

    invoke-direct {p0}, Lcom/twitter/android/SearchResultsFragment;->aD()Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v1, v9, v10}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v7, 0x5

    new-array v7, v7, [Ljava/lang/String;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/twitter/android/SearchResultsFragment;->h:Ljava/lang/String;

    aput-object v10, v7, v9

    const/4 v9, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->M()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v7, v9

    const/4 v9, 0x2

    const-string/jumbo v10, "tweet"

    aput-object v10, v7, v9

    const/4 v9, 0x3

    const-string/jumbo v10, "tweet"

    aput-object v10, v7, v9

    const/4 v9, 0x4

    const-string/jumbo v10, "click"

    aput-object v10, v7, v9

    invoke-static {v6, v7}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/StringBuilder;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v4

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v4, p0, Lcom/twitter/android/SearchResultsFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    const/4 v6, 0x0

    invoke-virtual {v1, v2, v3, v4, v6}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/SearchResultsFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/SearchResultsFragment;->c:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/twitter/android/SearchResultsFragment;->j:Z

    iget-boolean v4, p0, Lcom/twitter/android/SearchResultsFragment;->i:Z

    invoke-virtual {v1, v2, v5, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v8, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    goto/16 :goto_0

    :cond_2
    iget v1, v2, Lcom/twitter/android/sp;->b:I

    if-nez v1, :cond_4

    invoke-direct {p0}, Lcom/twitter/android/SearchResultsFragment;->aE()Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v1, v9, v10}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/twitter/android/SearchResultsFragment;->h:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ":cluster_detail:tweet:tweet:click"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v4

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v4, p0, Lcom/twitter/android/SearchResultsFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    const/4 v6, 0x0

    invoke-virtual {v1, v2, v3, v4, v6}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/SearchResultsFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/SearchResultsFragment;->c:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/twitter/android/SearchResultsFragment;->j:Z

    iget-boolean v4, p0, Lcom/twitter/android/SearchResultsFragment;->i:Z

    invoke-virtual {v1, v2, v5, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v8, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    goto/16 :goto_0

    :cond_3
    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v1, v9, v10}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v6, p0, Lcom/twitter/android/SearchResultsFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    const-string/jumbo v7, "tweet"

    const-string/jumbo v9, "tweet"

    const-string/jumbo v10, "click"

    invoke-static {v6, v7, v9, v10}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v4

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v4, p0, Lcom/twitter/android/SearchResultsFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    const/4 v6, 0x0

    invoke-virtual {v1, v2, v3, v4, v6}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/SearchResultsFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/SearchResultsFragment;->c:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/twitter/android/SearchResultsFragment;->j:Z

    iget-boolean v4, p0, Lcom/twitter/android/SearchResultsFragment;->i:Z

    invoke-virtual {v1, v2, v5, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v8, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    goto/16 :goto_0

    :cond_4
    iget v1, v2, Lcom/twitter/android/sp;->b:I

    const/16 v4, 0x18

    if-ne v1, v4, :cond_6

    iget-object v1, v2, Lcom/twitter/android/sp;->k:Lcom/twitter/library/api/af;

    if-eqz v1, :cond_5

    iget-object v1, v2, Lcom/twitter/android/sp;->k:Lcom/twitter/library/api/af;

    iget-object v1, v1, Lcom/twitter/library/api/af;->d:Ljava/lang/String;

    :goto_1
    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v2, v9, v10}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/twitter/android/SearchResultsFragment;->h:Ljava/lang/String;

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v9, ":cluster:tweet::click"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v6

    invoke-virtual {v2, v4}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    const/4 v4, 0x0

    iget-object v6, p0, Lcom/twitter/android/SearchResultsFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    const/4 v7, 0x0

    invoke-virtual {v2, v4, v3, v6, v7}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/SearchResultsFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/SearchResultsFragment;->c:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/twitter/android/SearchResultsFragment;->j:Z

    iget-boolean v6, p0, Lcom/twitter/android/SearchResultsFragment;->i:Z

    invoke-virtual {v2, v3, v5, v4, v6}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/twitter/library/scribe/ScribeLog;->c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v8, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    goto/16 :goto_0

    :cond_5
    const/4 v1, 0x0

    goto :goto_1

    :cond_6
    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v1, v9, v10}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v7, 0x5

    new-array v7, v7, [Ljava/lang/String;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/twitter/android/SearchResultsFragment;->h:Ljava/lang/String;

    aput-object v10, v7, v9

    const/4 v9, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->M()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v7, v9

    const/4 v9, 0x2

    const-string/jumbo v10, "news"

    aput-object v10, v7, v9

    const/4 v9, 0x3

    const-string/jumbo v10, "tweet"

    aput-object v10, v7, v9

    const/4 v9, 0x4

    const-string/jumbo v10, "click"

    aput-object v10, v7, v9

    invoke-static {v6, v7}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/StringBuilder;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v4

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v4, p0, Lcom/twitter/android/SearchResultsFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    const/4 v6, 0x0

    invoke-virtual {v1, v2, v3, v4, v6}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/SearchResultsFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/SearchResultsFragment;->c:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/twitter/android/SearchResultsFragment;->j:Z

    iget-boolean v4, p0, Lcom/twitter/android/SearchResultsFragment;->i:Z

    invoke-virtual {v1, v2, v5, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v8, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    goto/16 :goto_0

    :pswitch_3
    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/SearchResultsFragment;->h:Ljava/lang/String;

    aput-object v4, v1, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->M()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    const/4 v3, 0x2

    const-string/jumbo v4, "highlight"

    aput-object v4, v1, v3

    const/4 v3, 0x3

    const-string/jumbo v4, "more"

    aput-object v4, v1, v3

    const/4 v3, 0x4

    const-string/jumbo v4, "search"

    aput-object v4, v1, v3

    invoke-static {v6, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/StringBuilder;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/SearchResultsFragment;->b(Ljava/lang/String;)V

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-class v4, Lcom/twitter/android/SearchTerminalActivity;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v3, "query"

    iget-object v4, p0, Lcom/twitter/android/SearchResultsFragment;->c:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v3, "query_name"

    iget-object v4, p0, Lcom/twitter/android/SearchResultsFragment;->b:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v3, "q_source"

    const-string/jumbo v4, "highlight_tweet_drill_down_click"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v3, "since"

    iget-object v4, v2, Lcom/twitter/android/sp;->j:Lcom/twitter/library/api/TwitterSearchHighlight;

    iget-wide v4, v4, Lcom/twitter/library/api/TwitterSearchHighlight;->timeSince:J

    invoke-virtual {v1, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v3, "until"

    iget-object v2, v2, Lcom/twitter/android/sp;->j:Lcom/twitter/library/api/TwitterSearchHighlight;

    iget-wide v4, v2, Lcom/twitter/library/api/TwitterSearchHighlight;->timeUntil:J

    invoke-virtual {v1, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "terminal"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/SearchResultsFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :pswitch_4
    check-cast p2, Lcom/twitter/internal/android/widget/GroupedRowView;

    const/4 v1, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/twitter/library/widget/UserView;

    invoke-virtual {v4}, Lcom/twitter/library/widget/UserView;->getUserId()J

    move-result-wide v2

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v7

    const-class v11, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v1, v7, v11}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v7, "user_id"

    invoke-virtual {v1, v7, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v7, "screen_name"

    invoke-virtual {v4}, Lcom/twitter/library/widget/UserView;->getUserName()Ljava/lang/CharSequence;

    move-result-object v11

    invoke-virtual {v1, v7, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    iget-object v7, p0, Lcom/twitter/android/SearchResultsFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    if-eqz v7, :cond_7

    const-string/jumbo v7, "association"

    const/4 v11, 0x5

    iget-wide v12, p0, Lcom/twitter/android/SearchResultsFragment;->Q:J

    iget-object v14, p0, Lcom/twitter/android/SearchResultsFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-static {v11, v12, v13, v14}, Lcom/twitter/library/scribe/ScribeAssociation;->a(IJLcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v11

    invoke-virtual {v1, v7, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_7
    invoke-virtual {v4}, Lcom/twitter/library/widget/UserView;->getPromotedContent()Lcom/twitter/library/api/PromotedContent;

    move-result-object v7

    if-eqz v7, :cond_8

    sget-object v11, Lcom/twitter/library/api/PromotedEvent;->d:Lcom/twitter/library/api/PromotedEvent;

    invoke-virtual {v8, v11, v7}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/library/api/PromotedContent;)V

    const-string/jumbo v11, "pc"

    invoke-virtual {v1, v11, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    :cond_8
    invoke-virtual {p0, v1}, Lcom/twitter/android/SearchResultsFragment;->startActivity(Landroid/content/Intent;)V

    iget v1, p0, Lcom/twitter/android/SearchResultsFragment;->K:I

    const/4 v7, 0x2

    if-ne v1, v7, :cond_9

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/twitter/android/SearchResultsFragment;->h:Ljava/lang/String;

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v6, ":people:users:user:profile_click"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_2
    new-instance v6, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v6, v9, v10}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v9, 0x0

    aput-object v1, v7, v9

    invoke-virtual {v6, v7}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v6, p0, Lcom/twitter/android/SearchResultsFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v1, v6}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v6, p0, Lcom/twitter/android/SearchResultsFragment;->c:Ljava/lang/String;

    iget-boolean v7, p0, Lcom/twitter/android/SearchResultsFragment;->j:Z

    iget-boolean v9, p0, Lcom/twitter/android/SearchResultsFragment;->i:Z

    invoke-virtual {v1, v6, v5, v7, v9}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v4}, Lcom/twitter/library/widget/UserView;->getPromotedContent()Lcom/twitter/library/api/PromotedContent;

    move-result-object v4

    const/4 v5, 0x0

    move/from16 v6, p3

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/library/scribe/ScribeLog;->a(JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;I)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v8, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    goto/16 :goto_0

    :cond_9
    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v7, 0x0

    iget-object v11, p0, Lcom/twitter/android/SearchResultsFragment;->h:Ljava/lang/String;

    aput-object v11, v1, v7

    const/4 v7, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->M()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v1, v7

    const/4 v7, 0x2

    const/4 v11, 0x0

    aput-object v11, v1, v7

    const/4 v7, 0x3

    const-string/jumbo v11, "user"

    aput-object v11, v1, v7

    const/4 v7, 0x4

    const-string/jumbo v11, "profile_click"

    aput-object v11, v1, v7

    invoke-static {v6, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/StringBuilder;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :pswitch_5
    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/SearchResultsFragment;->h:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->M()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "user"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "more"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "search"

    aput-object v3, v1, v2

    invoke-static {v6, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/StringBuilder;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/SearchResultsFragment;->b(Ljava/lang/String;)V

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/twitter/android/SearchTerminalActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "query"

    iget-object v3, p0, Lcom/twitter/android/SearchResultsFragment;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "query_name"

    iget-object v3, p0, Lcom/twitter/android/SearchResultsFragment;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "search_type"

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "terminal"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/SearchResultsFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :pswitch_6
    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/SearchResultsFragment;->h:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->M()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "timeline_gallery"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "more"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "click"

    aput-object v3, v1, v2

    invoke-static {v6, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/StringBuilder;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/SearchResultsFragment;->b(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->af:Ljava/lang/String;

    if-eqz v1, :cond_a

    const-string/jumbo v1, "curated"

    iget-object v2, p0, Lcom/twitter/android/SearchResultsFragment;->af:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    const/4 v1, 0x7

    :goto_3
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    if-eqz v2, :cond_0

    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/twitter/android/SearchActivity;

    invoke-direct {v3, v2, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "query"

    iget-object v4, p0, Lcom/twitter/android/SearchResultsFragment;->c:Ljava/lang/String;

    invoke-virtual {v3, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "query_name"

    iget-object v4, p0, Lcom/twitter/android/SearchResultsFragment;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "search_type"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "terminal"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/SearchResultsFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_a
    const/16 v1, 0x8

    goto :goto_3

    :pswitch_7
    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/SearchResultsFragment;->h:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->M()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "media_gallery"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "more"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "search"

    aput-object v3, v1, v2

    invoke-static {v6, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/StringBuilder;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/SearchResultsFragment;->b(Ljava/lang/String;)V

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/twitter/android/SearchTerminalActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "query"

    iget-object v3, p0, Lcom/twitter/android/SearchResultsFragment;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "query_name"

    iget-object v3, p0, Lcom/twitter/android/SearchResultsFragment;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "search_type"

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "terminal"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/SearchResultsFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :pswitch_8
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-class v4, Lcom/twitter/android/SearchActivity;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v3, "query"

    iget-object v4, v2, Lcom/twitter/android/sp;->i:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v3, "query_name"

    iget-object v2, v2, Lcom/twitter/android/sp;->i:Ljava/lang/String;

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "q_source"

    const-string/jumbo v3, "related_query_click"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/SearchResultsFragment;->startActivity(Landroid/content/Intent;)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/SearchResultsFragment;->h:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->M()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "related_queries"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const/4 v3, 0x0

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "search"

    aput-object v3, v1, v2

    invoke-static {v6, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/StringBuilder;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/SearchResultsFragment;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_9
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/twitter/android/SearchActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "query"

    iget-object v3, p0, Lcom/twitter/android/SearchResultsFragment;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "query_name"

    iget-object v3, p0, Lcom/twitter/android/SearchResultsFragment;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "q_source"

    const-string/jumbo v3, "auto_spell_correct_revert_click"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/SearchResultsFragment;->startActivity(Landroid/content/Intent;)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/SearchResultsFragment;->h:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->M()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "spelling_corrections"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const/4 v3, 0x0

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "revert_click"

    aput-object v3, v1, v2

    invoke-static {v6, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/StringBuilder;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/SearchResultsFragment;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_a
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-class v4, Lcom/twitter/android/SearchTerminalActivity;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v3, "query"

    iget-object v4, p0, Lcom/twitter/android/SearchResultsFragment;->c:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v3, "scribe_context"

    const-string/jumbo v4, "cluster_header"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v3, "cluster_id"

    iget-object v4, v2, Lcom/twitter/android/sp;->k:Lcom/twitter/library/api/af;

    invoke-virtual {v4}, Lcom/twitter/library/api/af;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v3, "query_name"

    iget-object v4, v2, Lcom/twitter/android/sp;->k:Lcom/twitter/library/api/af;

    iget-object v4, v4, Lcom/twitter/library/api/af;->d:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v3, "terminal"

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/SearchResultsFragment;->startActivity(Landroid/content/Intent;)V

    invoke-direct {p0}, Lcom/twitter/android/SearchResultsFragment;->aD()Z

    move-result v1

    if-eqz v1, :cond_b

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/SearchResultsFragment;->h:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->M()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const/4 v3, 0x0

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "cluster_header"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "click"

    aput-object v3, v1, v2

    invoke-static {v6, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/StringBuilder;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/SearchResultsFragment;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_b
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/twitter/android/SearchResultsFragment;->h:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, ":cluster::cluster_header:click"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v2, Lcom/twitter/android/sp;->k:Lcom/twitter/library/api/af;

    invoke-direct {p0, v1, v2}, Lcom/twitter/android/SearchResultsFragment;->a(Ljava/lang/String;Lcom/twitter/library/api/af;)V

    goto/16 :goto_0

    :pswitch_b
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-class v4, Lcom/twitter/android/SearchTerminalActivity;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v3, "query"

    iget-object v4, p0, Lcom/twitter/android/SearchResultsFragment;->c:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v3, "terminal"

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v3, "q_type"

    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    iget-object v3, v2, Lcom/twitter/android/sp;->k:Lcom/twitter/library/api/af;

    if-eqz v3, :cond_d

    const-string/jumbo v3, "scribe_context"

    const-string/jumbo v4, "cluster_footer"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v4, "cluster_id"

    iget-object v5, v2, Lcom/twitter/android/sp;->k:Lcom/twitter/library/api/af;

    invoke-virtual {v5}, Lcom/twitter/library/api/af;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v4, "query_name"

    iget-object v5, v2, Lcom/twitter/android/sp;->k:Lcom/twitter/library/api/af;

    iget-object v5, v5, Lcom/twitter/library/api/af;->d:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/twitter/android/SearchResultsFragment;->h:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ":cluster::cluster_footer:click"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v2, v2, Lcom/twitter/android/sp;->k:Lcom/twitter/library/api/af;

    invoke-direct {p0, v3, v2}, Lcom/twitter/android/SearchResultsFragment;->a(Ljava/lang/String;Lcom/twitter/library/api/af;)V

    :cond_c
    :goto_4
    invoke-virtual {p0, v1}, Lcom/twitter/android/SearchResultsFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_d
    iget-object v3, v2, Lcom/twitter/android/sp;->l:Lcom/twitter/library/api/TwitterSearchFilter;

    if-eqz v3, :cond_c

    const-string/jumbo v3, "query_name"

    iget-object v4, p0, Lcom/twitter/android/SearchResultsFragment;->b:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v4, "follows"

    iget-object v5, v2, Lcom/twitter/android/sp;->l:Lcom/twitter/library/api/TwitterSearchFilter;

    iget-boolean v5, v5, Lcom/twitter/library/api/TwitterSearchFilter;->follow:Z

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v4, "near"

    iget-object v5, v2, Lcom/twitter/android/sp;->l:Lcom/twitter/library/api/TwitterSearchFilter;

    iget-boolean v5, v5, Lcom/twitter/library/api/TwitterSearchFilter;->nearby:Z

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v3, v2, Lcom/twitter/android/sp;->l:Lcom/twitter/library/api/TwitterSearchFilter;

    iget-boolean v3, v3, Lcom/twitter/library/api/TwitterSearchFilter;->follow:Z

    if-eqz v3, :cond_e

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/SearchResultsFragment;->h:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->M()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string/jumbo v4, "follows_pivot"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string/jumbo v4, "more"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string/jumbo v4, "search"

    aput-object v4, v2, v3

    invoke-static {v6, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/StringBuilder;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/twitter/android/SearchResultsFragment;->b(Ljava/lang/String;)V

    goto :goto_4

    :cond_e
    iget-object v2, v2, Lcom/twitter/android/sp;->l:Lcom/twitter/library/api/TwitterSearchFilter;

    iget-boolean v2, v2, Lcom/twitter/library/api/TwitterSearchFilter;->nearby:Z

    if-eqz v2, :cond_c

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/SearchResultsFragment;->h:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->M()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string/jumbo v4, "nearby_pivot"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string/jumbo v4, "more"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string/jumbo v4, "search"

    aput-object v4, v2, v3

    invoke-static {v6, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/StringBuilder;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/twitter/android/SearchResultsFragment;->b(Ljava/lang/String;)V

    goto/16 :goto_4

    :pswitch_c
    invoke-direct {p0}, Lcom/twitter/android/SearchResultsFragment;->aD()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v7, v1, Lcom/twitter/android/sk;->j:Lcom/twitter/android/widget/TopicView;

    invoke-virtual {v7}, Lcom/twitter/android/widget/TopicView;->getTopicType()I

    move-result v3

    invoke-virtual {v7}, Lcom/twitter/android/widget/TopicView;->getTopicId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7}, Lcom/twitter/android/widget/TopicView;->getSeedHashtag()Ljava/lang/String;

    move-result-object v6

    instance-of v1, v7, Lcom/twitter/android/widget/CollectionView;

    if-eqz v1, :cond_f

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v1, v9, v10}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v9, p0, Lcom/twitter/android/SearchResultsFragment;->h:Ljava/lang/String;

    aput-object v9, v4, v6

    const/4 v6, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->M()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v4, v6

    const/4 v6, 0x2

    const-string/jumbo v9, ""

    aput-object v9, v4, v6

    const/4 v6, 0x3

    const-string/jumbo v9, "timeline"

    aput-object v9, v4, v6

    const/4 v6, 0x4

    const-string/jumbo v9, "click"

    aput-object v9, v4, v6

    invoke-static {v4}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/SearchResultsFragment;->c:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/twitter/android/SearchResultsFragment;->j:Z

    iget-boolean v4, p0, Lcom/twitter/android/SearchResultsFragment;->i:Z

    invoke-virtual {v1, v2, v5, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v8, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    move-object v1, v7

    check-cast v1, Lcom/twitter/android/widget/CollectionView;

    invoke-virtual {v1}, Lcom/twitter/android/widget/CollectionView;->getLaunchIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/SearchResultsFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_f
    invoke-static {v3}, Lcom/twitter/library/api/TwitterTopic;->b(I)Ljava/lang/String;

    move-result-object v1

    new-instance v4, Lcom/twitter/library/scribe/ScribeItem;

    invoke-direct {v4}, Lcom/twitter/library/scribe/ScribeItem;-><init>()V

    iput-object v2, v4, Lcom/twitter/library/scribe/ScribeItem;->b:Ljava/lang/String;

    const/16 v11, 0x10

    iput v11, v4, Lcom/twitter/library/scribe/ScribeItem;->c:I

    iput-object v1, v4, Lcom/twitter/library/scribe/ScribeItem;->x:Ljava/lang/String;

    new-instance v11, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v11, v9, v10}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/String;

    const/4 v10, 0x0

    const/4 v12, 0x5

    new-array v12, v12, [Ljava/lang/String;

    const/4 v13, 0x0

    iget-object v14, p0, Lcom/twitter/android/SearchResultsFragment;->h:Ljava/lang/String;

    aput-object v14, v12, v13

    const/4 v13, 0x1

    const-string/jumbo v14, "universal_top"

    aput-object v14, v12, v13

    const/4 v13, 0x2

    aput-object v1, v12, v13

    const/4 v1, 0x3

    const-string/jumbo v13, "event"

    aput-object v13, v12, v1

    const/4 v1, 0x4

    const-string/jumbo v13, "click"

    aput-object v13, v12, v1

    invoke-static {v12}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v9, v10

    invoke-virtual {v11, v9}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v9, p0, Lcom/twitter/android/SearchResultsFragment;->c:Ljava/lang/String;

    iget-boolean v10, p0, Lcom/twitter/android/SearchResultsFragment;->j:Z

    iget-boolean v11, p0, Lcom/twitter/android/SearchResultsFragment;->i:Z

    invoke-virtual {v1, v9, v5, v10, v11}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v8, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    new-instance v1, Lcom/twitter/android/vf;

    invoke-direct {v1, p0}, Lcom/twitter/android/vf;-><init>(Landroid/support/v4/app/Fragment;)V

    iget-object v4, p0, Lcom/twitter/android/SearchResultsFragment;->b:Ljava/lang/String;

    iget-object v5, p0, Lcom/twitter/android/SearchResultsFragment;->c:Ljava/lang/String;

    invoke-virtual {v7}, Lcom/twitter/android/widget/TopicView;->getTopicData()Lcom/twitter/android/widget/TopicView$TopicData;

    move-result-object v7

    invoke-virtual/range {v1 .. v7}, Lcom/twitter/android/vf;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/android/widget/TopicView$TopicData;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_4
        :pswitch_9
        :pswitch_8
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_5
        :pswitch_2
        :pswitch_1
        :pswitch_c
        :pswitch_1
        :pswitch_5
        :pswitch_3
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_a
        :pswitch_a
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_b
        :pswitch_7
        :pswitch_1
        :pswitch_6
        :pswitch_c
        :pswitch_c
        :pswitch_c
    .end packed-switch
.end method

.method public a(Lcom/twitter/library/util/aa;Ljava/util/HashMap;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->M:Lcom/twitter/android/client/au;

    invoke-virtual {v0}, Lcom/twitter/android/client/au;->a()Z

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->M:Lcom/twitter/android/client/au;

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/au;->a(Z)V

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->z:Lcom/twitter/android/sh;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->z:Lcom/twitter/android/sh;

    invoke-virtual {v1, p1, p2}, Lcom/twitter/android/sh;->a(Lcom/twitter/library/util/aa;Ljava/util/HashMap;)V

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->z:Lcom/twitter/android/sh;

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, p2, v0}, Lcom/twitter/android/sh;->a(Ljava/util/HashMap;Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;Z)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->z:Lcom/twitter/android/sh;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->z:Lcom/twitter/android/sh;

    if-nez p3, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, p1, p2, v0}, Lcom/twitter/android/sh;->a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Z)V
    .locals 3

    const/4 v2, 0x5

    invoke-super {p0, p1}, Lcom/twitter/android/SearchFragment;->a(Z)V

    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->z:Lcom/twitter/android/sh;

    if-eqz p1, :cond_1

    invoke-virtual {p0, v2}, Lcom/twitter/android/SearchResultsFragment;->a_(I)V

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->P()Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Lcom/twitter/android/sh;->b()Landroid/database/Cursor;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-virtual {p0, v2}, Lcom/twitter/android/SearchResultsFragment;->a_(I)V

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->p()V

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/twitter/android/sh;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_3
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchResultsFragment;->d(I)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/SearchResultsFragment;->ad:Z

    goto :goto_0
.end method

.method protected a_()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->z:Lcom/twitter/android/sh;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/android/sh;->c()V

    :cond_0
    return-void
.end method

.method protected b(J)I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->z:Lcom/twitter/android/sh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->z:Lcom/twitter/android/sh;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/sh;->a(J)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(JLcom/twitter/library/api/PromotedContent;ILcom/twitter/android/zd;)I
    .locals 8

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v7

    iget-object v4, p0, Lcom/twitter/android/SearchResultsFragment;->Y:Lcom/twitter/library/util/FriendshipCache;

    iget v5, p5, Lcom/twitter/android/zd;->b:I

    invoke-virtual {v4, p1, p2}, Lcom/twitter/library/util/FriendshipCache;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v4, p1, p2}, Lcom/twitter/library/util/FriendshipCache;->i(J)Z

    move-result v0

    :goto_0
    invoke-direct {p0}, Lcom/twitter/android/SearchResultsFragment;->aD()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->M()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v1, "user_rail"

    move-object v3, v2

    move-object v2, v1

    :goto_1
    if-eqz v0, :cond_2

    const/4 v0, 0x1

    invoke-static {v5, v0}, Lcom/twitter/library/provider/ay;->b(II)I

    move-result v1

    invoke-virtual {v7, p1, p2, p3}, Lcom/twitter/android/client/c;->a(JLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    invoke-virtual {v4, p1, p2}, Lcom/twitter/library/util/FriendshipCache;->e(J)V

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/twitter/android/SearchResultsFragment;->h:Ljava/lang/String;

    aput-object v5, v0, v4

    const/4 v4, 0x1

    aput-object v3, v0, v4

    const/4 v3, 0x2

    aput-object v2, v0, v3

    const/4 v2, 0x3

    const-string/jumbo v3, "user"

    aput-object v3, v0, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "unfollow"

    aput-object v3, v0, v2

    invoke-static {v0}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v6, v1

    :goto_2
    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->c:Ljava/lang/String;

    iget v2, p0, Lcom/twitter/android/SearchResultsFragment;->K:I

    invoke-static {v2}, Lcom/twitter/android/SearchResultsFragment;->d_(I)Ljava/lang/String;

    move-result-object v2

    iget-boolean v3, p0, Lcom/twitter/android/SearchResultsFragment;->j:Z

    iget-boolean v4, p0, Lcom/twitter/android/SearchResultsFragment;->i:Z

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    const/4 v4, 0x0

    move-wide v1, p1

    move-object v3, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/scribe/ScribeLog;->a(JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;I)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return v6

    :cond_0
    invoke-static {v5}, Lcom/twitter/library/provider/ay;->b(I)Z

    move-result v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->M()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v1, "user_gallery"

    move-object v3, v2

    move-object v2, v1

    goto :goto_1

    :cond_2
    const/4 v0, 0x1

    invoke-static {v5, v0}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v1

    const/4 v0, 0x0

    invoke-virtual {v7, p1, p2, v0, p3}, Lcom/twitter/android/client/c;->a(JZLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    invoke-virtual {v4, p1, p2}, Lcom/twitter/library/util/FriendshipCache;->b(J)V

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/twitter/android/SearchResultsFragment;->h:Ljava/lang/String;

    aput-object v5, v0, v4

    const/4 v4, 0x1

    aput-object v3, v0, v4

    const/4 v3, 0x2

    aput-object v2, v0, v3

    const/4 v2, 0x3

    const-string/jumbo v3, "user"

    aput-object v3, v0, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "follow"

    aput-object v3, v0, v2

    invoke-static {v0}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move v6, v1

    goto :goto_2
.end method

.method public b(Landroid/view/View;)Lcom/twitter/android/yd;
    .locals 1

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/twitter/android/sk;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/sk;

    iget-object v0, v0, Lcom/twitter/android/sk;->a:Lcom/twitter/android/yd;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b(Ljava/lang/String;)V
    .locals 6

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    iget-object v2, p0, Lcom/twitter/android/SearchResultsFragment;->ab:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->h(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->T()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/twitter/android/SearchResultsFragment;->K:I

    invoke-static {v3}, Lcom/twitter/android/SearchResultsFragment;->d_(I)Ljava/lang/String;

    move-result-object v3

    iget-boolean v4, p0, Lcom/twitter/android/SearchResultsFragment;->j:Z

    iget-boolean v5, p0, Lcom/twitter/android/SearchResultsFragment;->i:Z

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/SearchResultsFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method

.method protected c(Landroid/view/View;)Lcom/twitter/library/widget/TweetView;
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/twitter/android/sk;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/twitter/android/sk;

    iget-object v0, v0, Lcom/twitter/android/sk;->a:Lcom/twitter/android/yd;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected d(I)Z
    .locals 2

    iget-wide v0, p0, Lcom/twitter/android/SearchResultsFragment;->q:J

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/android/SearchResultsFragment;->a(IJ)Z

    move-result v0

    return v0
.end method

.method protected g(I)Ljava/lang/String;
    .locals 2

    invoke-direct {p0}, Lcom/twitter/android/SearchResultsFragment;->aE()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "cluster_detail"

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-boolean v0, p0, Lcom/twitter/android/SearchResultsFragment;->aa:Z

    if-eqz v0, :cond_2

    const-string/jumbo v0, "cluster"

    goto :goto_0

    :cond_2
    const/4 v0, -0x1

    if-eq p1, v0, :cond_3

    invoke-static {p1}, Lcom/twitter/library/api/TwitterTopic;->b(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/twitter/android/SearchResultsFragment;->J:Ljava/util/HashMap;

    iget v1, p0, Lcom/twitter/android/SearchResultsFragment;->K:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_0

    const-string/jumbo v0, "universal_top"

    goto :goto_0
.end method

.method protected i()V
    .locals 5

    const/4 v4, 0x2

    iget v0, p0, Lcom/twitter/android/SearchResultsFragment;->K:I

    if-ne v0, v4, :cond_0

    iget-wide v0, p0, Lcom/twitter/android/SearchResultsFragment;->L:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/twitter/android/SearchResultsFragment;->s:I

    if-lez v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->v()V

    goto :goto_0

    :cond_1
    iget-wide v0, p0, Lcom/twitter/android/SearchResultsFragment;->q:J

    invoke-direct {p0, v4, v0, v1}, Lcom/twitter/android/SearchResultsFragment;->a(IJ)Z

    goto :goto_0
.end method

.method protected o()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->z:Lcom/twitter/android/sh;

    invoke-virtual {v0}, Lcom/twitter/android/sh;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 23

    invoke-super/range {p0 .. p1}, Lcom/twitter/android/SearchFragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/SearchResultsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/SearchResultsFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v2, "search_type"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/twitter/android/SearchResultsFragment;->K:I

    const-string/jumbo v2, "cluster_id"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/android/SearchResultsFragment;->V:Ljava/lang/String;

    const-string/jumbo v2, "scribe_context"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/twitter/android/SearchResultsFragment;->ab:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/SearchResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v10

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/SearchResultsFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/SearchResultsFragment;->z:Lcom/twitter/android/sh;

    if-nez v1, :cond_0

    new-instance v1, Lcom/twitter/android/ny;

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/SearchResultsFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/SearchResultsFragment;->o:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/SearchResultsFragment;->W:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/SearchResultsFragment;->c:Ljava/lang/String;

    invoke-direct/range {v1 .. v6}, Lcom/twitter/android/ny;-><init>(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Lcom/twitter/android/sh;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/SearchResultsFragment;->b:Ljava/lang/String;

    iget-object v6, v11, Lcom/twitter/android/client/c;->a:Lcom/twitter/library/widget/ap;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/android/SearchResultsFragment;->C:Lcom/twitter/android/vs;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/android/SearchResultsFragment;->Y:Lcom/twitter/library/util/FriendshipCache;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/android/SearchResultsFragment;->Z:Lcom/twitter/android/ob;

    move-object/from16 v0, p0

    iget v13, v0, Lcom/twitter/android/SearchResultsFragment;->K:I

    invoke-direct/range {p0 .. p0}, Lcom/twitter/android/SearchResultsFragment;->aE()Z

    move-result v14

    invoke-direct/range {p0 .. p0}, Lcom/twitter/android/SearchResultsFragment;->aD()Z

    move-result v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/SearchResultsFragment;->W:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/SearchResultsFragment;->ac:Lcom/twitter/library/util/aa;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/twitter/android/SearchResultsFragment;->m:Z

    move/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/SearchResultsFragment;->ag:Ljava/util/HashSet;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/SearchResultsFragment;->o:Ljava/lang/String;

    move-object/from16 v20, v0

    if-eqz p1, :cond_2

    const/16 v21, 0x1

    :goto_0
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/twitter/android/SearchResultsFragment;->ae:Z

    move/from16 v22, v0

    move-object v3, v10

    move-object v4, v11

    move-object/from16 v10, p0

    move-object/from16 v11, p0

    move-object v12, v1

    invoke-direct/range {v2 .. v22}, Lcom/twitter/android/sh;-><init>(Landroid/content/Context;Lcom/twitter/android/client/c;Ljava/lang/String;Lcom/twitter/library/widget/ap;Lcom/twitter/android/vs;Lcom/twitter/library/util/FriendshipCache;Lcom/twitter/android/ob;Landroid/widget/AdapterView$OnItemClickListener;Lcom/twitter/android/zb;Lcom/twitter/android/ny;IZZLjava/lang/String;Lcom/twitter/library/util/aa;ZLjava/util/HashSet;Ljava/lang/String;ZZ)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/android/SearchResultsFragment;->z:Lcom/twitter/android/sh;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/SearchResultsFragment;->D:Lcom/twitter/android/vv;

    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, Lcom/twitter/android/vv;->a(Lcom/twitter/android/vw;)V

    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/SearchResultsFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    if-nez v1, :cond_1

    invoke-direct/range {p0 .. p0}, Lcom/twitter/android/SearchResultsFragment;->aC()V

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/SearchResultsFragment;->z:Lcom/twitter/android/sh;

    const-string/jumbo v3, "tweet"

    move-object/from16 v0, p0

    iget v1, v0, Lcom/twitter/android/SearchResultsFragment;->K:I

    const/4 v4, 0x3

    if-ne v1, v4, :cond_3

    const/4 v1, 0x1

    :goto_1
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v1}, Lcom/twitter/android/SearchResultsFragment;->a(Ljava/lang/String;Z)Lcom/twitter/android/yb;

    move-result-object v1

    const-string/jumbo v3, "news"

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4}, Lcom/twitter/android/SearchResultsFragment;->a(Ljava/lang/String;Z)Lcom/twitter/android/yb;

    move-result-object v3

    const-string/jumbo v4, "highlight"

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/twitter/android/SearchResultsFragment;->a(Ljava/lang/String;Z)Lcom/twitter/android/yb;

    move-result-object v4

    invoke-virtual {v2, v1, v3, v4}, Lcom/twitter/android/sh;->a(Lcom/twitter/library/widget/aa;Lcom/twitter/library/widget/aa;Lcom/twitter/library/widget/aa;)V

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/SearchResultsFragment;->V()Landroid/widget/ListView;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/SearchResultsFragment;->z:Lcom/twitter/android/sh;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void

    :cond_2
    const/16 v21, 0x0

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    const-wide/16 v4, 0x0

    const/4 v3, -0x1

    invoke-super {p0, p1}, Lcom/twitter/android/SearchFragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_1

    const-string/jumbo v0, "friendship_cache"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "friendship_cache"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/FriendshipCache;

    iput-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->Y:Lcom/twitter/library/util/FriendshipCache;

    :goto_0
    const-string/jumbo v0, "viewed_item_ids"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    iput-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->ag:Ljava/util/HashSet;

    const-string/jumbo v0, "since"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/SearchResultsFragment;->L:J

    const-string/jumbo v0, "until"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/SearchResultsFragment;->U:J

    const-string/jumbo v0, "searches_with_clusters"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/SearchResultsFragment;->aa:Z

    const-string/jumbo v0, "event_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->W:Ljava/lang/String;

    const-string/jumbo v0, "event_type"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/SearchResultsFragment;->X:I

    const-string/jumbo v0, "in_back_stack"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/SearchResultsFragment;->ae:Z

    :goto_1
    new-instance v0, Lcom/twitter/android/so;

    invoke-direct {v0, p0}, Lcom/twitter/android/so;-><init>(Lcom/twitter/android/SearchResultsFragment;)V

    iput-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->Z:Lcom/twitter/android/ob;

    const/4 v0, 0x1

    invoke-virtual {p0, v0, p0}, Lcom/twitter/android/SearchResultsFragment;->a(ILcom/twitter/library/util/ar;)V

    const/4 v0, 0x2

    invoke-virtual {p0, v0, p0}, Lcom/twitter/android/SearchResultsFragment;->a(ILcom/twitter/library/util/ar;)V

    return-void

    :cond_0
    new-instance v0, Lcom/twitter/library/util/FriendshipCache;

    invoke-direct {v0}, Lcom/twitter/library/util/FriendshipCache;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->Y:Lcom/twitter/library/util/FriendshipCache;

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/twitter/library/util/FriendshipCache;

    invoke-direct {v0}, Lcom/twitter/library/util/FriendshipCache;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->Y:Lcom/twitter/library/util/FriendshipCache;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->ag:Ljava/util/HashSet;

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "since"

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/twitter/android/SearchResultsFragment;->L:J

    const-string/jumbo v1, "until"

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/twitter/android/SearchResultsFragment;->U:J

    const-string/jumbo v1, "event_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->W:Ljava/lang/String;

    const-string/jumbo v1, "event_type"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/SearchResultsFragment;->X:I

    const-string/jumbo v1, "in_back_stack"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/SearchResultsFragment;->ae:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/SearchResultsFragment;->aa:Z

    goto :goto_1
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 9

    new-instance v0, Landroid/support/v4/content/CursorLoader;

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/provider/aq;->a:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/twitter/library/provider/w;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/twitter/library/provider/bs;->a:[Ljava/lang/String;

    const-string/jumbo v4, "search_id=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    iget-wide v7, p0, Lcom/twitter/android/SearchResultsFragment;->q:J

    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const-string/jumbo v6, "type_id ASC, _id ASC"

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public onDestroy()V
    .locals 10

    const/4 v9, 0x0

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->z:Lcom/twitter/android/sh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->z:Lcom/twitter/android/sh;

    invoke-virtual {v0, v9}, Lcom/twitter/android/sh;->a(Landroid/database/Cursor;)Landroid/database/Cursor;

    :cond_0
    invoke-virtual {p0, v6, p0}, Lcom/twitter/android/SearchResultsFragment;->b(ILcom/twitter/library/util/ar;)V

    invoke-virtual {p0, v8, p0}, Lcom/twitter/android/SearchResultsFragment;->b(ILcom/twitter/library/util/ar;)V

    invoke-direct {p0}, Lcom/twitter/android/SearchResultsFragment;->aD()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v3, v6, [Ljava/lang/String;

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/String;

    iget-object v5, p0, Lcom/twitter/android/SearchResultsFragment;->h:Ljava/lang/String;

    aput-object v5, v4, v7

    iget v5, p0, Lcom/twitter/android/SearchResultsFragment;->X:I

    invoke-static {v5}, Lcom/twitter/library/api/TwitterTopic;->b(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    const-string/jumbo v5, "time_nav"

    aput-object v5, v4, v8

    const/4 v5, 0x3

    aput-object v9, v4, v5

    const/4 v5, 0x4

    const-string/jumbo v6, "close"

    aput-object v6, v4, v5

    invoke-static {v0, v4}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/StringBuilder;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v7

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/SearchResultsFragment;->W:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeLog;->f(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_1
    :goto_0
    invoke-super {p0}, Lcom/twitter/android/SearchFragment;->onDestroy()V

    return-void

    :cond_2
    invoke-direct {p0}, Lcom/twitter/android/SearchResultsFragment;->aE()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v2, v6, [Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/twitter/android/SearchResultsFragment;->h:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ":cluster_detail:time_nav::close"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/SearchResultsFragment;->W:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->f(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 10

    invoke-virtual {p1}, Landroid/widget/AdapterView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/sp;

    iget v1, v0, Lcom/twitter/android/sp;->b:I

    const/16 v2, 0x15

    if-ne v1, v2, :cond_0

    const/4 v2, 0x3

    iget-object v1, v0, Lcom/twitter/android/sp;->k:Lcom/twitter/library/api/af;

    invoke-static {v1}, Lcom/twitter/library/scribe/ScribeItem;->a(Lcom/twitter/library/api/af;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v1

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    new-instance v4, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    const-class v6, Lcom/twitter/android/GalleryActivity;

    invoke-direct {v4, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v5, Lcom/twitter/library/provider/aq;->a:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v6

    invoke-virtual {v6}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    invoke-static {v5, v6, v7}, Lcom/twitter/library/provider/w;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v4

    const-string/jumbo v5, "prj"

    sget-object v6, Lcom/twitter/library/provider/bs;->a:[Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    const-string/jumbo v5, "sel"

    const-string/jumbo v6, "flags&1|512!=0 AND search_id=? AND type_id=?"

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    const-string/jumbo v5, "selArgs"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    iget-wide v8, p0, Lcom/twitter/android/SearchResultsFragment;->q:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget v0, v0, Lcom/twitter/android/sp;->c:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v4, "orderBy"

    const-string/jumbo v5, "type_id ASC, _id ASC"

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v4, "id"

    invoke-virtual {v0, v4, p4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v4, "context"

    invoke-virtual {v0, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "item"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/android/SearchResultsFragment;->h:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->M()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "media_gallery"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "photo"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "click"

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/SearchResultsFragment;->b(Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v2, 0x2

    const/4 v1, 0x0

    goto/16 :goto_0
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/SearchResultsFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->z:Lcom/twitter/android/sh;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/sh;->a(Landroid/database/Cursor;)Landroid/database/Cursor;

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/twitter/android/SearchFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->Y:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v0}, Lcom/twitter/library/util/FriendshipCache;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "friendship_cache"

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->Y:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    :cond_0
    const-string/jumbo v0, "search_id"

    iget-wide v1, p0, Lcom/twitter/android/SearchResultsFragment;->q:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string/jumbo v0, "since"

    iget-wide v1, p0, Lcom/twitter/android/SearchResultsFragment;->L:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string/jumbo v0, "until"

    iget-wide v1, p0, Lcom/twitter/android/SearchResultsFragment;->U:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string/jumbo v0, "cluster_id"

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->V:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "searches_with_clusters"

    iget-boolean v1, p0, Lcom/twitter/android/SearchResultsFragment;->aa:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "event_id"

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->W:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "event_type"

    iget v1, p0, Lcom/twitter/android/SearchResultsFragment;->X:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "viewed_item_ids"

    iget-object v1, p0, Lcom/twitter/android/SearchResultsFragment;->ag:Ljava/util/HashSet;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const-string/jumbo v0, "in_back_stack"

    iget-boolean v1, p0, Lcom/twitter/android/SearchResultsFragment;->ae:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method protected p()V
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    return-void
.end method

.method protected u()Z
    .locals 4

    iget v0, p0, Lcom/twitter/android/SearchResultsFragment;->K:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget-wide v0, p0, Lcom/twitter/android/SearchResultsFragment;->L:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected y()V
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/android/SearchResultsFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/android/SearchResultsFragment;->q:J

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/client/c;->f(J)Ljava/lang/String;

    return-void
.end method

.method protected z()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->z:Lcom/twitter/android/sh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/SearchResultsFragment;->z:Lcom/twitter/android/sh;

    invoke-virtual {v0}, Lcom/twitter/android/sh;->getCount()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
