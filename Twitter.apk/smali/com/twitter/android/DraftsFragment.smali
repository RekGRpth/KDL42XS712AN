.class public Lcom/twitter/android/DraftsFragment;
.super Lcom/twitter/android/client/BaseListFragment;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/widget/ce;


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private b:Lcom/twitter/library/client/Session;

.field private c:Lcom/twitter/android/ff;

.field private d:Ljava/lang/String;

.field private e:Z

.field private f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "in_r_status_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "content"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "url_entities"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "mention_entities"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "media_entities"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "hashtag_entities"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "pc"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/android/DraftsFragment;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseListFragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/DraftsFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/DraftsFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Landroid/content/DialogInterface;II)V
    .locals 3

    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/DraftsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/DraftsFragment;->b:Lcom/twitter/library/client/Session;

    invoke-static {v0, v1}, Lcom/twitter/android/client/cd;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;)Ljava/lang/String;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    :cond_0
    return-void
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/DraftsFragment;->c:Lcom/twitter/android/ff;

    invoke-virtual {v0, p2}, Lcom/twitter/android/ff;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    iget-object v0, p0, Lcom/twitter/android/DraftsFragment;->c:Lcom/twitter/android/ff;

    invoke-virtual {v0}, Lcom/twitter/android/ff;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/DraftsFragment;->f:Z

    invoke-virtual {p0}, Lcom/twitter/android/DraftsFragment;->ap()V

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/twitter/android/DraftsFragment;->b(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 10

    const/4 v9, 0x0

    const/4 v8, 0x1

    invoke-virtual {p1, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    new-instance v1, Lcom/twitter/library/api/TweetEntities;

    invoke-direct {v1}, Lcom/twitter/library/api/TweetEntities;-><init>()V

    const/4 v2, 0x3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/api/TweetEntities;->a([B)Lcom/twitter/library/api/TweetEntities;

    move-result-object v1

    const/4 v2, 0x4

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/api/TweetEntities;->b([B)Lcom/twitter/library/api/TweetEntities;

    move-result-object v1

    const/4 v2, 0x5

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/api/TweetEntities;->c([B)Lcom/twitter/library/api/TweetEntities;

    move-result-object v1

    const/4 v2, 0x6

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/api/TweetEntities;->d([B)Lcom/twitter/library/api/TweetEntities;

    move-result-object v3

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const-wide/16 v1, 0x0

    cmp-long v1, v4, v1

    if-lez v1, :cond_0

    sget-object v1, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;->c:Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    :goto_0
    iget-boolean v2, p0, Lcom/twitter/android/DraftsFragment;->e:Z

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/DraftsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Landroid/content/Context;Lcom/twitter/android/composer/ComposerIntentWrapper$Action;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v1

    move-object v2, v1

    :goto_1
    const/4 v1, 0x7

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/PromotedContent;

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-virtual {v2, v6, v7, v3}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(JLcom/twitter/library/api/TweetEntities;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v3

    invoke-virtual {v3, v4, v5}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(J)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Lcom/twitter/library/api/PromotedContent;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v1

    const/4 v3, 0x2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Ljava/lang/String;[I)Lcom/twitter/android/composer/ComposerIntentWrapper;

    invoke-virtual {p0}, Lcom/twitter/android/DraftsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/android/DraftsFragment;->e:Z

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/twitter/android/DraftsFragment;->d:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Ljava/lang/String;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    invoke-virtual {v2}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "return_to_drafts"

    invoke-virtual {v0, v1, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/DraftsFragment;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/twitter/android/DraftsFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/DraftsFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v8, [Ljava/lang/String;

    const-string/jumbo v4, ":drafts:composition:::impression"

    aput-object v4, v3, v9

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :goto_2
    return-void

    :cond_0
    sget-object v1, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;->b:Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    goto :goto_0

    :cond_1
    invoke-static {v1}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Lcom/twitter/android/composer/ComposerIntentWrapper$Action;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v1

    move-object v2, v1

    goto :goto_1

    :cond_2
    const/4 v1, -0x1

    invoke-virtual {v2}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_2
.end method

.method protected a(Lcom/twitter/internal/android/widget/ToolBar;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->a(Lcom/twitter/internal/android/widget/ToolBar;)V

    const v0, 0x7f09030a    # com.twitter.android.R.id.menu_send_all

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/android/DraftsFragment;->f:Z

    invoke-virtual {v0, v1}, Lhn;->c(Z)Lhn;

    return-void
.end method

.method protected a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseListFragment;->a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)V

    const v0, 0x7f110009    # com.twitter.android.R.menu.drafts

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    return-void
.end method

.method public a(Lhn;)Z
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p1}, Lhn;->a()I

    move-result v1

    const v2, 0x7f09030a    # com.twitter.android.R.id.menu_send_all

    if-ne v1, v2, :cond_0

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    const v2, 0x7f0f0108    # com.twitter.android.R.string.dialog_send_all_drafts_title

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    const v2, 0x7f0f0107    # com.twitter.android.R.string.dialog_send_all_drafts_confirmation

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->e(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    const v2, 0x7f0f0571    # com.twitter.android.R.string.yes

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->g(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    const v2, 0x7f0f029d    # com.twitter.android.R.string.no

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->i(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p0, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/twitter/android/DraftsFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->a(Lhn;)Z

    move-result v0

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 6

    const/4 v5, 0x0

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/twitter/android/DraftsFragment;->c:Lcom/twitter/android/ff;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/android/ff;

    invoke-virtual {p0}, Lcom/twitter/android/DraftsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/ff;-><init>(Lcom/twitter/android/DraftsFragment;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/DraftsFragment;->c:Lcom/twitter/android/ff;

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/DraftsFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/DraftsFragment;->c:Lcom/twitter/android/ff;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/twitter/android/DraftsFragment;->c:Lcom/twitter/android/ff;

    invoke-virtual {v0}, Lcom/twitter/android/ff;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/DraftsFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v5, v1, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/twitter/android/DraftsFragment;->a_(I)V

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/DraftsFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/DraftsFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, ":drafts::::impression"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/DraftsFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "account_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "return_to_drafts"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/DraftsFragment;->e:Z

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/DraftsFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/DraftsFragment;->b:Lcom/twitter/library/client/Session;

    iget-object v0, p0, Lcom/twitter/android/DraftsFragment;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/DraftsFragment;->d:Ljava/lang/String;

    :goto_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/DraftsFragment;->l(Z)V

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/DraftsFragment;->au()Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/aa;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/DraftsFragment;->b:Lcom/twitter/library/client/Session;

    iput-object v1, p0, Lcom/twitter/android/DraftsFragment;->d:Ljava/lang/String;

    goto :goto_0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/twitter/android/DraftsFragment;->b:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    new-instance v0, Landroid/support/v4/content/CursorLoader;

    invoke-virtual {p0}, Lcom/twitter/android/DraftsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v3, Lcom/twitter/library/provider/ah;->a:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    const-string/jumbo v4, "ownerId"

    invoke-virtual {v3, v4, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/twitter/android/DraftsFragment;->a:[Ljava/lang/String;

    const-string/jumbo v4, "flags&1= 0"

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/DraftsFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/DraftsFragment;->c:Lcom/twitter/android/ff;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/ff;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    return-void
.end method
