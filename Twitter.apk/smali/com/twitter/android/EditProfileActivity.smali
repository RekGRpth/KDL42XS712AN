.class public Lcom/twitter/android/EditProfileActivity;
.super Lcom/twitter/android/BaseEditProfileActivity;
.source "Twttr"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field u:Landroid/widget/EditText;

.field v:Landroid/widget/EditText;

.field w:Landroid/widget/EditText;

.field private x:Ljava/lang/String;

.field private y:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/BaseEditProfileActivity;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/api/TweetEntities;)V
    .locals 6

    const/4 v2, 0x0

    iput-object p1, p0, Lcom/twitter/android/EditProfileActivity;->x:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->u:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iput-object p2, p0, Lcom/twitter/android/EditProfileActivity;->r:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->p:Landroid/widget/EditText;

    invoke-virtual {v0, p2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    if-eqz p5, :cond_0

    iget-object v0, p5, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p5, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p5, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/UrlEntity;

    iget v4, v0, Lcom/twitter/library/api/UrlEntity;->start:I

    add-int/2addr v4, v1

    iget v5, v0, Lcom/twitter/library/api/UrlEntity;->end:I

    add-int/2addr v5, v1

    invoke-virtual {p2, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    iget-object v5, v0, Lcom/twitter/library/api/UrlEntity;->displayUrl:Ljava/lang/String;

    invoke-virtual {p2, v4, v5}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    iget-object v4, v0, Lcom/twitter/library/api/UrlEntity;->displayUrl:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    iget v5, v0, Lcom/twitter/library/api/UrlEntity;->end:I

    iget v0, v0, Lcom/twitter/library/api/UrlEntity;->start:I

    sub-int v0, v5, v0

    sub-int v0, v4, v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    :cond_0
    if-eqz p6, :cond_1

    iget-object v0, p6, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p6, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p6, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/UrlEntity;

    iget-object v1, v0, Lcom/twitter/library/api/UrlEntity;->displayUrl:Ljava/lang/String;

    if-nez v1, :cond_2

    iget-object p3, v0, Lcom/twitter/library/api/UrlEntity;->url:Ljava/lang/String;

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->v:Landroid/widget/EditText;

    invoke-virtual {v0, p3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iput-object p3, p0, Lcom/twitter/android/EditProfileActivity;->s:Ljava/lang/String;

    iput-object p4, p0, Lcom/twitter/android/EditProfileActivity;->t:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->w:Landroid/widget/EditText;

    invoke-virtual {v0, p4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0f0089    # com.twitter.android.R.string.cancel

    invoke-virtual {p0, v0}, Lcom/twitter/android/EditProfileActivity;->setTitle(I)V

    return-void

    :cond_2
    iget-object p3, v0, Lcom/twitter/library/api/UrlEntity;->expandedUrl:Ljava/lang/String;

    goto :goto_1
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 2

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    const v1, 0x7f03005f    # com.twitter.android.R.layout.edit_profile

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(I)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->a(Z)V

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->c(I)V

    return-object v0
.end method

.method protected a()Lcom/twitter/library/scribe/ScribeAssociation;
    .locals 2

    new-instance v0, Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/library/scribe/ScribeAssociation;-><init>()V

    const-string/jumbo v1, "edit_profile"

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/content/Intent;)V
    .locals 1

    const/4 v0, -0x1

    invoke-virtual {p0, v0, p1}, Lcom/twitter/android/EditProfileActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileActivity;->finish()V

    return-void
.end method

.method protected a(Landroid/graphics/Bitmap;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->n:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getId()I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/EditProfileActivity;->a(Landroid/graphics/Bitmap;I)V

    return-void
.end method

.method protected a(Landroid/net/Uri;)V
    .locals 3

    new-instance v0, Lcom/twitter/android/ax;

    iget-object v1, p0, Lcom/twitter/android/EditProfileActivity;->n:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getId()I

    move-result v1

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/ax;-><init>(Lcom/twitter/android/BaseEditProfileActivity;I)V

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/net/Uri;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/android/ax;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 10

    const/4 v5, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    invoke-super {p0, p1, p2}, Lcom/twitter/android/BaseEditProfileActivity;->a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V

    const v0, 0x7f090144    # com.twitter.android.R.id.edit_name

    invoke-virtual {p0, v0}, Lcom/twitter/android/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/twitter/android/EditProfileActivity;->u:Landroid/widget/EditText;

    const v0, 0x7f090146    # com.twitter.android.R.id.edit_web_url

    invoke-virtual {p0, v0}, Lcom/twitter/android/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/twitter/android/EditProfileActivity;->v:Landroid/widget/EditText;

    const v0, 0x7f090145    # com.twitter.android.R.id.edit_location

    invoke-virtual {p0, v0}, Lcom/twitter/android/EditProfileActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/twitter/android/EditProfileActivity;->w:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    const-string/jumbo v1, "failure"

    invoke-virtual {v7, v1, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/android/EditProfileActivity;->y:Z

    iget-boolean v1, p0, Lcom/twitter/android/EditProfileActivity;->y:Z

    if-eqz v1, :cond_3

    const-string/jumbo v1, "update_profile"

    invoke-virtual {v7, v1, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string/jumbo v0, "name"

    invoke-virtual {v7, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v0, "description"

    invoke-virtual {v7, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v0, "url"

    invoke-virtual {v7, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v0, "location"

    invoke-virtual {v7, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :goto_0
    move-object v0, p0

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/EditProfileActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/api/TweetEntities;)V

    const-string/jumbo v0, "header_uri"

    invoke-virtual {v7, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "crop_rect"

    invoke-virtual {v7, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    iput-object v0, p0, Lcom/twitter/android/EditProfileActivity;->k:Landroid/graphics/Rect;

    const-string/jumbo v0, "header_uri"

    invoke-virtual {v7, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/twitter/android/EditProfileActivity;->b:Landroid/net/Uri;

    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->b:Landroid/net/Uri;

    iput-object v0, p0, Lcom/twitter/android/EditProfileActivity;->e:Landroid/net/Uri;

    new-instance v0, Lcom/twitter/android/ax;

    iget-object v1, p0, Lcom/twitter/android/EditProfileActivity;->n:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getId()I

    move-result v1

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/ax;-><init>(Lcom/twitter/android/BaseEditProfileActivity;I)V

    new-array v1, v9, [Landroid/net/Uri;

    iget-object v2, p0, Lcom/twitter/android/EditProfileActivity;->b:Landroid/net/Uri;

    aput-object v2, v1, v8

    invoke-virtual {v0, v1}, Lcom/twitter/android/ax;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    const-string/jumbo v0, "avatar_uri"

    invoke-virtual {v7, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "avatar_uri"

    invoke-virtual {v7, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/twitter/android/EditProfileActivity;->c:Landroid/net/Uri;

    new-instance v0, Lcom/twitter/android/ax;

    iget-object v1, p0, Lcom/twitter/android/EditProfileActivity;->o:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getId()I

    move-result v1

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/ax;-><init>(Lcom/twitter/android/BaseEditProfileActivity;I)V

    new-array v1, v9, [Landroid/net/Uri;

    iget-object v2, p0, Lcom/twitter/android/EditProfileActivity;->c:Landroid/net/Uri;

    aput-object v2, v1, v8

    invoke-virtual {v0, v1}, Lcom/twitter/android/ax;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->u:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/twitter/android/EditProfileActivity;->u:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->u:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void

    :cond_2
    iget-object v1, v0, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    iget-object v2, v0, Lcom/twitter/library/api/TwitterUser;->profileDescription:Ljava/lang/String;

    iget-object v3, v0, Lcom/twitter/library/api/TwitterUser;->profileUrl:Ljava/lang/String;

    iget-object v4, v0, Lcom/twitter/library/api/TwitterUser;->location:Ljava/lang/String;

    goto :goto_0

    :cond_3
    iget-object v1, v0, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    iget-object v2, v0, Lcom/twitter/library/api/TwitterUser;->profileDescription:Ljava/lang/String;

    iget-object v3, v0, Lcom/twitter/library/api/TwitterUser;->profileUrl:Ljava/lang/String;

    iget-object v4, v0, Lcom/twitter/library/api/TwitterUser;->location:Ljava/lang/String;

    iget-object v5, v0, Lcom/twitter/library/api/TwitterUser;->descriptionEntities:Lcom/twitter/library/api/TweetEntities;

    iget-object v6, v0, Lcom/twitter/library/api/TwitterUser;->urlEntities:Lcom/twitter/library/api/TweetEntities;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/EditProfileActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/api/TweetEntities;)V

    goto :goto_1
.end method

.method public a(Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 3

    const/4 v1, 0x1

    const v0, 0x7f090141    # com.twitter.android.R.id.save

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v2

    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->u:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Lhn;->c(Z)Lhn;

    return v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 1

    const v0, 0x7f110022    # com.twitter.android.R.menu.toolbar_save

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    const/4 v0, 0x1

    return v0
.end method

.method public a(Lhn;)Z
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-virtual {p1}, Lhn;->a()I

    move-result v0

    const v1, 0x7f090141    # com.twitter.android.R.id.save

    if-ne v0, v1, :cond_6

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileActivity;->k()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->v:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string/jumbo v1, "://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    sget-object v1, Lcom/twitter/library/util/text/b;->e:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-nez v1, :cond_2

    const v0, 0x7f0f01f4    # com.twitter.android.R.string.invalid_url

    invoke-static {p0, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    :goto_1
    return v5

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "http://"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/twitter/android/EditProfileActivity;->v:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->w:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    const/16 v1, 0x1e

    if-le v0, v1, :cond_4

    const v0, 0x7f0f01f2    # com.twitter.android.R.string.invalid_location

    invoke-static {p0, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    :cond_4
    new-instance v0, Lcom/twitter/android/ay;

    invoke-direct {v0, p0}, Lcom/twitter/android/ay;-><init>(Lcom/twitter/android/BaseEditProfileActivity;)V

    new-array v1, v4, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/ay;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1

    :cond_5
    invoke-virtual {p0}, Lcom/twitter/android/EditProfileActivity;->finish()V

    goto :goto_1

    :cond_6
    const v1, 0x7f090045    # com.twitter.android.R.id.home

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileActivity;->k()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileActivity;->f_()V

    goto :goto_1

    :cond_7
    invoke-virtual {p0, v4}, Lcom/twitter/android/EditProfileActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileActivity;->finish()V

    goto :goto_1
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileActivity;->V()V

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->u:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected g()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->v:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected h()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->w:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected m()Z
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->u:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/EditProfileActivity;->v:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/EditProfileActivity;->w:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileActivity;->l()Z

    move-result v3

    if-nez v3, :cond_5

    iget-boolean v3, p0, Lcom/twitter/android/EditProfileActivity;->y:Z

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/twitter/android/EditProfileActivity;->x:Ljava/lang/String;

    if-nez v3, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    :cond_0
    iget-object v3, p0, Lcom/twitter/android/EditProfileActivity;->x:Ljava/lang/String;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/twitter/android/EditProfileActivity;->x:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->s:Ljava/lang/String;

    if-nez v0, :cond_2

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->s:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->t:Ljava/lang/String;

    if-nez v0, :cond_4

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_4
    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->t:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/twitter/android/EditProfileActivity;->t:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    :cond_5
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_6
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method
