.class Lcom/twitter/android/mc;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public a:Lcom/twitter/internal/android/widget/HighlightedRelativeLayout;

.field public b:Landroid/widget/ImageView;

.field public c:Landroid/widget/TextView;

.field public d:Landroid/widget/TextView;

.field public e:Landroid/widget/TextView;

.field public f:Landroid/widget/TextView;

.field public g:J

.field public h:Ljava/lang/String;

.field public i:Landroid/widget/ImageView;

.field public j:Lcom/twitter/library/util/m;

.field final synthetic k:Lcom/twitter/android/lx;


# direct methods
.method private constructor <init>(Lcom/twitter/android/lx;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/mc;->k:Lcom/twitter/android/lx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/lx;Lcom/twitter/android/ly;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/mc;-><init>(Lcom/twitter/android/lx;)V

    return-void
.end method


# virtual methods
.method public a(J)V
    .locals 0

    iput-wide p1, p0, Lcom/twitter/android/mc;->g:J

    return-void
.end method

.method public a(JLjava/lang/String;)V
    .locals 1

    iput-object p3, p0, Lcom/twitter/android/mc;->h:Ljava/lang/String;

    const/4 v0, 0x0

    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/twitter/android/mc;->k:Lcom/twitter/android/lx;

    invoke-static {v0}, Lcom/twitter/android/lx;->b(Lcom/twitter/android/lx;)Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/twitter/android/client/c;->g(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/twitter/android/mc;->a(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/mc;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/mc;->b:Landroid/widget/ImageView;

    const v1, 0x7f02003a    # com.twitter.android.R.drawable.bg_no_profile_photo_md

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/mc;->e:Landroid/widget/TextView;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/android/mc;->k:Lcom/twitter/android/lx;

    invoke-static {v2}, Lcom/twitter/android/lx;->b(Lcom/twitter/android/lx;)Lcom/twitter/android/client/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/android/client/c;->V()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v0, p0, Lcom/twitter/android/mc;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public a(Ljava/lang/String;Landroid/content/Context;Z)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    const-string/jumbo v0, "\\n+"

    const-string/jumbo v1, " "

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/mc;->c:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/twitter/android/mc;->k:Lcom/twitter/android/lx;

    invoke-static {v2}, Lcom/twitter/android/lx;->b(Lcom/twitter/android/lx;)Lcom/twitter/android/client/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/android/client/c;->V()F

    move-result v2

    invoke-virtual {v1, v4, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    if-eqz p3, :cond_0

    new-instance v1, Landroid/text/SpannableStringBuilder;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    new-instance v0, Landroid/text/style/ImageSpan;

    const v2, 0x7f020176    # com.twitter.android.R.drawable.ic_dm_reply_default

    invoke-direct {v0, p2, v2, v5}, Landroid/text/style/ImageSpan;-><init>(Landroid/content/Context;II)V

    const/16 v2, 0x12

    invoke-virtual {v1, v0, v4, v5, v2}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    iget-object v0, p0, Lcom/twitter/android/mc;->c:Landroid/widget/TextView;

    sget-object v2, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/mc;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public a(ZLandroid/content/res/Resources;)V
    .locals 4

    const v3, 0x7f0b006d    # com.twitter.android.R.color.primary_text

    const v2, 0x7f0b0064    # com.twitter.android.R.color.medium_gray

    iget-object v0, p0, Lcom/twitter/android/mc;->a:Lcom/twitter/internal/android/widget/HighlightedRelativeLayout;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/HighlightedRelativeLayout;->setHighlighted(Z)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/mc;->c:Landroid/widget/TextView;

    invoke-virtual {p2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/twitter/android/mc;->d:Landroid/widget/TextView;

    invoke-virtual {p2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/mc;->c:Landroid/widget/TextView;

    invoke-virtual {p2, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/twitter/android/mc;->d:Landroid/widget/TextView;

    invoke-virtual {p2, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/mc;->f:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "@"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
