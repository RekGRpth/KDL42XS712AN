.class public Lcom/twitter/android/card/ab;
.super Lcom/twitter/library/card/element/h;
.source "Twttr"


# instance fields
.field private c:Lcom/twitter/library/util/at;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/card/element/Player;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/library/card/element/h;-><init>(Landroid/content/Context;Lcom/twitter/library/card/element/Player;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/card/ab;)Lcom/twitter/library/card/element/Player;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/card/ab;->b:Lcom/twitter/library/card/element/Player;

    return-object v0
.end method

.method private i()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/card/ab;->b:Lcom/twitter/library/card/element/Player;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/Player;->A()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/card/PlayerDelegateVineView;

    iget-object v0, v0, Lcom/twitter/android/card/PlayerDelegateVineView;->a:Lco/vine/android/player/SdkVideoView;

    new-instance v1, Lcom/twitter/android/card/ac;

    invoke-direct {v1, p0}, Lcom/twitter/android/card/ac;-><init>(Lcom/twitter/android/card/ab;)V

    invoke-virtual {v0, v1}, Lco/vine/android/player/SdkVideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    new-instance v1, Lcom/twitter/android/card/ad;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/card/ad;-><init>(Lcom/twitter/android/card/ab;Lco/vine/android/player/SdkVideoView;)V

    invoke-virtual {v0, v1}, Lco/vine/android/player/SdkVideoView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    new-instance v1, Lcom/twitter/android/card/ae;

    invoke-direct {v1, p0}, Lcom/twitter/android/card/ae;-><init>(Lcom/twitter/android/card/ab;)V

    invoke-virtual {v0, v1}, Lco/vine/android/player/SdkVideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    return-void
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 3

    new-instance v0, Lcom/twitter/android/card/PlayerDelegateVineView;

    iget-object v1, p0, Lcom/twitter/android/card/ab;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/card/ab;->b:Lcom/twitter/library/card/element/Player;

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/card/PlayerDelegateVineView;-><init>(Landroid/content/Context;Lcom/twitter/library/card/element/Player;)V

    return-object v0
.end method

.method public a(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/RectF;)V
    .locals 0

    return-void
.end method

.method public declared-synchronized a(Lcom/twitter/library/util/at;Ljava/lang/String;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/card/ab;->c:Lcom/twitter/library/util/at;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/card/ab;->b:Lcom/twitter/library/card/element/Player;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/Player;->A()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/card/PlayerDelegateVineView;

    iget-object v1, v0, Lcom/twitter/android/card/PlayerDelegateVineView;->a:Lco/vine/android/player/SdkVideoView;

    invoke-virtual {v0}, Lcom/twitter/android/card/PlayerDelegateVineView;->B_()Z

    invoke-virtual {v1, p2}, Lco/vine/android/player/SdkVideoView;->setVideoPath(Ljava/lang/String;)V

    invoke-virtual {v1}, Lco/vine/android/player/SdkVideoView;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(JLcom/twitter/library/util/aa;Lcom/twitter/library/util/as;)Z
    .locals 6

    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/twitter/android/card/ab;->i()V

    new-instance v0, Lcom/twitter/library/util/at;

    iget-object v1, p0, Lcom/twitter/android/card/ab;->b:Lcom/twitter/library/card/element/Player;

    iget-object v1, v1, Lcom/twitter/library/card/element/Player;->streamUrl:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/twitter/library/util/at;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/android/card/ab;->c:Lcom/twitter/library/util/at;

    new-instance v0, Lcom/twitter/library/card/element/i;

    iget-object v2, p0, Lcom/twitter/android/card/ab;->c:Lcom/twitter/library/util/at;

    move-object v1, p4

    move-wide v3, p1

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/card/element/i;-><init>(Lcom/twitter/library/util/as;Lcom/twitter/library/util/at;JLcom/twitter/library/card/element/h;)V

    invoke-virtual {v0}, Lcom/twitter/library/card/element/i;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b()Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/card/ab;->b:Lcom/twitter/library/card/element/Player;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/Player;->A()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/card/PlayerDelegateVineView;

    iget-object v0, v0, Lcom/twitter/android/card/PlayerDelegateVineView;->a:Lco/vine/android/player/SdkVideoView;

    invoke-virtual {v0}, Lco/vine/android/player/SdkVideoView;->a()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lco/vine/android/player/SdkVideoView;->pause()V

    :goto_0
    const/4 v0, 0x1

    return v0

    :cond_0
    invoke-virtual {v0}, Lco/vine/android/player/SdkVideoView;->b()V

    goto :goto_0
.end method

.method public declared-synchronized c()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/twitter/android/card/ab;->c:Lcom/twitter/library/util/at;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public d()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public e()V
    .locals 0

    return-void
.end method

.method public f()V
    .locals 0

    return-void
.end method
