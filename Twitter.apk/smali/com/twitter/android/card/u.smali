.class public Lcom/twitter/android/card/u;
.super Lcom/twitter/android/card/a;
.source "Twttr"


# static fields
.field protected static final a:Lcom/twitter/library/scribe/ScribeAssociation;


# instance fields
.field protected o:Lcom/twitter/library/widget/TweetView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/library/scribe/ScribeAssociation;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->a(I)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    const-string/jumbo v1, "tweet"

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/card/u;->a:Lcom/twitter/library/scribe/ScribeAssociation;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/twitter/android/card/a;-><init>(Landroid/app/Activity;)V

    sget-object v0, Lcom/twitter/android/card/u;->a:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {p0, v0}, Lcom/twitter/android/card/u;->a(Lcom/twitter/library/scribe/ScribeAssociation;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/widget/TweetView;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/card/u;->o:Lcom/twitter/library/widget/TweetView;

    return-void
.end method

.method public b()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public d()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public h()V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/card/u;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/TweetActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "tw"

    iget-object v3, p0, Lcom/twitter/android/card/u;->d:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "association"

    iget-object v3, p0, Lcom/twitter/android/card/u;->g:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    const-string/jumbo v0, "tweet"

    const-string/jumbo v1, "click"

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/card/u;->e(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/card/u;->j:Lcom/twitter/android/client/c;

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    goto :goto_0
.end method

.method public y()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/card/u;->d:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v0}, Lcom/twitter/library/provider/Tweet;->V()Lcom/twitter/library/card/instance/CardInstanceData;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/card/u;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/BaseFragmentActivity;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lcom/twitter/android/client/a;)V

    invoke-static {}, Lcom/twitter/android/card/n;->a()Lcom/twitter/android/card/n;

    move-result-object v0

    const/4 v1, 0x0

    new-instance v2, Lcom/twitter/android/card/v;

    invoke-direct {v2, p0, p0}, Lcom/twitter/android/card/v;-><init>(Lcom/twitter/android/card/u;Lcom/twitter/android/card/u;)V

    invoke-virtual {v0, p0, v1, p0, v2}, Lcom/twitter/android/card/n;->a(Lcom/twitter/library/card/k;ZLcom/twitter/library/card/j;Lcom/twitter/android/card/q;)Lcom/twitter/android/card/p;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/card/u;->e:Lcom/twitter/android/card/p;

    goto :goto_0
.end method

.method public z()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/card/u;->e:Lcom/twitter/android/card/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/card/u;->e:Lcom/twitter/android/card/p;

    invoke-virtual {v0}, Lcom/twitter/android/card/p;->a()Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/card/u;->e:Lcom/twitter/android/card/p;

    :cond_0
    invoke-static {}, Lcom/twitter/android/card/n;->a()Lcom/twitter/android/card/n;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/card/u;->f:Lcom/twitter/library/card/Card;

    invoke-virtual {v0, p0, v1}, Lcom/twitter/android/card/n;->a(Lcom/twitter/library/card/k;Lcom/twitter/library/card/Card;)V

    iget-object v0, p0, Lcom/twitter/android/card/u;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/BaseFragmentActivity;

    if-nez v0, :cond_1

    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0, p0}, Lcom/twitter/android/client/BaseFragmentActivity;->b(Lcom/twitter/android/client/a;)V

    goto :goto_0
.end method
