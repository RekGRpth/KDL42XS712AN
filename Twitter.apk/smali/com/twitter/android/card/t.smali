.class public Lcom/twitter/android/card/t;
.super Landroid/os/AsyncTask;
.source "Twttr"


# instance fields
.field private a:Lcom/twitter/android/card/i;

.field private b:Lcom/twitter/library/card/k;

.field private c:Lcom/twitter/library/card/Card;


# direct methods
.method public constructor <init>(Lcom/twitter/android/card/i;Lcom/twitter/library/card/k;Lcom/twitter/library/card/Card;)V
    .locals 0

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/card/t;->a:Lcom/twitter/android/card/i;

    iput-object p2, p0, Lcom/twitter/android/card/t;->b:Lcom/twitter/library/card/k;

    iput-object p3, p0, Lcom/twitter/android/card/t;->c:Lcom/twitter/library/card/Card;

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/twitter/android/card/t;->c:Lcom/twitter/library/card/Card;

    invoke-virtual {v0}, Lcom/twitter/library/card/Card;->h()V

    iget-object v0, p0, Lcom/twitter/android/card/t;->a:Lcom/twitter/android/card/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/card/t;->b:Lcom/twitter/library/card/k;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/card/t;->b:Lcom/twitter/library/card/k;

    invoke-interface {v0}, Lcom/twitter/library/card/k;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/card/t;->b:Lcom/twitter/library/card/k;

    invoke-interface {v0}, Lcom/twitter/library/card/k;->q()Lcom/twitter/library/card/instance/CardInstanceData;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/card/t;->b:Lcom/twitter/library/card/k;

    invoke-interface {v1}, Lcom/twitter/library/card/k;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, v0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardCardTypeURL:Ljava/lang/String;

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/card/t;->a:Lcom/twitter/android/card/i;

    iget-object v2, p0, Lcom/twitter/android/card/t;->c:Lcom/twitter/library/card/Card;

    invoke-virtual {v1, v0, v2}, Lcom/twitter/android/card/i;->a(Ljava/lang/String;Lcom/twitter/library/card/Card;)V

    :cond_0
    iput-object v3, p0, Lcom/twitter/android/card/t;->c:Lcom/twitter/library/card/Card;

    return-object v3

    :cond_1
    iget-object v0, v0, Lcom/twitter/library/card/instance/CardInstanceData;->cardTypeURL:Ljava/lang/String;

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/card/t;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
