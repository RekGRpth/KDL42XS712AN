.class public Lcom/twitter/android/card/AppFollowButtonDelegateView;
.super Lcom/twitter/internal/android/widget/ShadowTextView;
.source "Twttr"


# instance fields
.field private a:Lcom/twitter/library/card/element/FollowButtonElement;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/card/element/FollowButtonElement;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/internal/android/widget/ShadowTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object p2, p0, Lcom/twitter/android/card/AppFollowButtonDelegateView;->a:Lcom/twitter/library/card/element/FollowButtonElement;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/card/AppFollowButtonDelegateView;->setWillNotDraw(Z)V

    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/internal/android/widget/ShadowTextView;->draw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/twitter/android/card/AppFollowButtonDelegateView;->a:Lcom/twitter/library/card/element/FollowButtonElement;

    iget v0, v0, Lcom/twitter/library/card/element/FollowButtonElement;->opacity:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/card/AppFollowButtonDelegateView;->a:Lcom/twitter/library/card/element/FollowButtonElement;

    invoke-virtual {v0, p1}, Lcom/twitter/library/card/element/FollowButtonElement;->a(Landroid/graphics/Canvas;)V

    :cond_0
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/internal/android/widget/ShadowTextView;->onDraw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/twitter/android/card/AppFollowButtonDelegateView;->a:Lcom/twitter/library/card/element/FollowButtonElement;

    iget v0, v0, Lcom/twitter/library/card/element/FollowButtonElement;->opacity:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/card/AppFollowButtonDelegateView;->a:Lcom/twitter/library/card/element/FollowButtonElement;

    invoke-virtual {v0, p1}, Lcom/twitter/library/card/element/FollowButtonElement;->b(Landroid/graphics/Canvas;)V

    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/card/AppFollowButtonDelegateView;->a:Lcom/twitter/library/card/element/FollowButtonElement;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/FollowButtonElement;->q()V

    invoke-super {p0, p1, p2}, Lcom/twitter/internal/android/widget/ShadowTextView;->onMeasure(II)V

    return-void
.end method
