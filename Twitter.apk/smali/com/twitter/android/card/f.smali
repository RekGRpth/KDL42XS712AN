.class public Lcom/twitter/android/card/f;
.super Landroid/os/AsyncTask;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/ref/WeakReference;

.field private final c:Ljava/lang/ref/WeakReference;

.field private final d:Ljava/lang/ref/WeakReference;

.field private final e:Ljava/lang/ref/WeakReference;

.field private final f:Ljava/lang/ref/WeakReference;

.field private final g:Z

.field private final h:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/card/Card;Lcom/twitter/library/card/element/Element;Lcom/twitter/library/card/property/Action;Lcom/twitter/android/card/h;Z)V
    .locals 1

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/card/f;->a:Landroid/content/Context;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/card/f;->b:Ljava/lang/ref/WeakReference;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/card/f;->c:Ljava/lang/ref/WeakReference;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p4}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/card/f;->d:Ljava/lang/ref/WeakReference;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p5}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/card/f;->e:Ljava/lang/ref/WeakReference;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p6}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/card/f;->f:Ljava/lang/ref/WeakReference;

    iput-boolean p7, p0, Lcom/twitter/android/card/f;->g:Z

    invoke-virtual {p3}, Lcom/twitter/library/card/Card;->a()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/card/f;->h:I

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Lcom/twitter/internal/network/k;
    .locals 14

    iget-object v0, p0, Lcom/twitter/android/card/f;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v5

    iget-object v0, p0, Lcom/twitter/android/card/f;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v6

    iget-object v0, p0, Lcom/twitter/android/card/f;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/Action;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, v0, Lcom/twitter/library/card/property/Action;->url:Ljava/lang/String;

    if-nez v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-boolean v2, p0, Lcom/twitter/android/card/f;->g:Z

    if-eqz v2, :cond_2

    invoke-virtual {v5, v1}, Lcom/twitter/android/client/c;->f(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "API Url is not whitelisted "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/library/card/CardDebugLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v1, p0, Lcom/twitter/android/card/f;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/provider/Tweet;

    if-nez v1, :cond_3

    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    invoke-virtual {v1}, Lcom/twitter/library/provider/Tweet;->t()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, v1, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    if-eqz v2, :cond_4

    iget-object v3, v2, Lcom/twitter/library/api/PromotedContent;->impressionId:Ljava/lang/String;

    if-eqz v3, :cond_4

    const-string/jumbo v3, "impression_id"

    iget-object v2, v2, Lcom/twitter/library/api/PromotedContent;->impressionId:Ljava/lang/String;

    invoke-static {v7, v3, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget-object v2, p0, Lcom/twitter/android/card/f;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/twitter/library/network/aa;->a(Landroid/content/Context;)Lcom/twitter/library/network/aa;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/twitter/library/network/aa;->b(Ljava/lang/StringBuilder;)V

    iget-object v8, v0, Lcom/twitter/library/card/property/Action;->apiRequest:Lcom/twitter/library/card/property/ApiRequest;

    iget v0, v8, Lcom/twitter/library/card/property/ApiRequest;->method:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_7

    const/4 v0, 0x1

    move v4, v0

    :goto_1
    if-eqz v4, :cond_8

    sget-object v0, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->a:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    :goto_2
    iget-object v9, v8, Lcom/twitter/library/card/property/ApiRequest;->parameters:[Lcom/twitter/library/card/property/ApiRequestParameter;

    const/4 v2, 0x0

    if-eqz v9, :cond_a

    if-nez v4, :cond_5

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    :cond_5
    array-length v10, v9

    const/4 v3, 0x0

    :goto_3
    if-ge v3, v10, :cond_a

    aget-object v11, v9, v3

    iget-object v12, v11, Lcom/twitter/library/card/property/ApiRequestParameter;->key:Ljava/lang/String;

    iget-object v11, v11, Lcom/twitter/library/card/property/ApiRequestParameter;->value:Ljava/lang/String;

    invoke-static {v12}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_6

    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v13

    if-nez v13, :cond_6

    if-eqz v4, :cond_9

    invoke-static {v7, v12, v11}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    :goto_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_7
    const/4 v0, 0x0

    move v4, v0

    goto :goto_1

    :cond_8
    sget-object v0, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    goto :goto_2

    :cond_9
    new-instance v13, Lorg/apache/http/message/BasicNameValuePair;

    invoke-direct {v13, v12, v11}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_a
    iget-object v3, v8, Lcom/twitter/library/card/property/ApiRequest;->apiProxyRule:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_b

    if-eqz v4, :cond_d

    const-string/jumbo v3, "apiProxyRule"

    iget-object v8, v8, Lcom/twitter/library/card/property/ApiRequest;->apiProxyRule:Ljava/lang/String;

    invoke-static {v7, v3, v8}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_b
    :goto_5
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    new-instance v8, Lcom/twitter/android/card/g;

    iget-object v3, p0, Lcom/twitter/android/card/f;->f:Ljava/lang/ref/WeakReference;

    invoke-direct {v8, v3, v7}, Lcom/twitter/android/card/g;-><init>(Ljava/lang/ref/WeakReference;Ljava/lang/String;)V

    const/4 v3, 0x0

    invoke-static {v7}, Lcom/twitter/library/util/Util;->b(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v9

    if-eqz v9, :cond_c

    invoke-virtual {v9}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_c

    invoke-virtual {v9}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v10, ".twitter.com"

    invoke-virtual {v9, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_c

    invoke-virtual {v6}, Lcom/twitter/library/client/Session;->h()Lcom/twitter/library/network/OAuthToken;

    move-result-object v9

    new-instance v3, Lcom/twitter/library/network/n;

    invoke-direct {v3, v9}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    :cond_c
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    if-eqz v4, :cond_f

    const-string/jumbo v4, " GET"

    :goto_6
    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v9, 0x0

    invoke-static {v4, v9}, Lcom/twitter/library/card/CardDebugLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v4, Lcom/twitter/library/network/d;

    iget-object v9, p0, Lcom/twitter/android/card/f;->a:Landroid/content/Context;

    invoke-direct {v4, v9, v7}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    invoke-virtual {v4, v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/twitter/library/network/d;->b(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/internal/network/HttpOperation;->n()Z

    move-result v0

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/twitter/android/card/f;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/Card;

    if-eqz v0, :cond_12

    monitor-enter v0

    :try_start_0
    iget v2, p0, Lcom/twitter/android/card/f;->h:I

    invoke-virtual {v0}, Lcom/twitter/library/card/Card;->a()I

    move-result v3

    if-ne v2, v3, :cond_11

    iget-object v2, v8, Lcom/twitter/android/card/g;->a:Ljava/util/HashMap;

    if-eqz v2, :cond_11

    invoke-virtual {v2}, Ljava/util/HashMap;->size()I

    move-result v3

    if-eqz v3, :cond_11

    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_7
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_10

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/card/instance/BindingValue;

    invoke-virtual {v0, v3, v2}, Lcom/twitter/library/card/Card;->a(Ljava/lang/String;Lcom/twitter/library/card/instance/BindingValue;)V

    goto :goto_7

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    :cond_d
    if-nez v2, :cond_e

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    :cond_e
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v9, "apiProxyRule"

    iget-object v8, v8, Lcom/twitter/library/card/property/ApiRequest;->apiProxyRule:Ljava/lang/String;

    invoke-direct {v3, v9, v8}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_5

    :cond_f
    const-string/jumbo v4, " POST"

    goto/16 :goto_6

    :cond_10
    :try_start_1
    iget-object v2, p0, Lcom/twitter/android/card/f;->a:Landroid/content/Context;

    invoke-virtual {v0, v2, v1}, Lcom/twitter/library/card/Card;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;)V

    invoke-virtual {v0, v0}, Lcom/twitter/library/card/Card;->d(Lcom/twitter/library/card/Card;)V

    invoke-virtual {v6}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    iget-object v3, v5, Lcom/twitter/android/client/c;->b:Lcom/twitter/library/util/aa;

    iget-object v5, v5, Lcom/twitter/android/client/c;->c:Lcom/twitter/library/util/as;

    invoke-virtual {v0, v1, v2, v3, v5}, Lcom/twitter/library/card/Card;->a(JLcom/twitter/library/util/aa;Lcom/twitter/library/util/as;)V

    :cond_11
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_12
    invoke-virtual {v4}, Lcom/twitter/internal/network/HttpOperation;->l()Lcom/twitter/internal/network/k;

    move-result-object v0

    goto/16 :goto_0

    :cond_13
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method protected a(Lcom/twitter/internal/network/k;)V
    .locals 5

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/card/f;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/Action;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/card/f;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/card/Card;

    if-eqz v1, :cond_5

    monitor-enter v1

    :try_start_0
    invoke-virtual {v1}, Lcom/twitter/library/card/Card;->a()I

    move-result v3

    iget v4, p0, Lcom/twitter/android/card/f;->h:I

    if-ne v3, v4, :cond_4

    if-eqz p1, :cond_6

    iget v3, p1, Lcom/twitter/internal/network/k;->a:I

    const/16 v4, 0xc8

    if-ne v3, v4, :cond_2

    const/4 v2, 0x1

    :cond_2
    move v3, v2

    :goto_1
    iget-object v2, p0, Lcom/twitter/android/card/f;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/card/element/Element;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/twitter/library/card/element/Element;->z()I

    move-result v2

    iget v4, v0, Lcom/twitter/library/card/property/Action;->id:I

    invoke-virtual {v1, v2, v4, v3}, Lcom/twitter/library/card/Card;->a(IIZ)V

    :cond_3
    invoke-virtual {v1}, Lcom/twitter/library/card/Card;->r()V

    invoke-virtual {v1}, Lcom/twitter/library/card/Card;->x()V

    :cond_4
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_5
    iget-object v1, p0, Lcom/twitter/android/card/f;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/card/h;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/twitter/library/card/property/Action;->url:Ljava/lang/String;

    invoke-interface {v1, v0, p1}, Lcom/twitter/android/card/h;->a(Ljava/lang/String;Lcom/twitter/internal/network/k;)V

    goto :goto_0

    :cond_6
    move v3, v2

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/card/f;->a([Ljava/lang/Void;)Lcom/twitter/internal/network/k;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/twitter/internal/network/k;

    invoke-virtual {p0, p1}, Lcom/twitter/android/card/f;->a(Lcom/twitter/internal/network/k;)V

    return-void
.end method
