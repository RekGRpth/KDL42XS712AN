.class final Lcom/twitter/android/v;
.super Lcom/twitter/android/aaa;
.source "Twttr"


# instance fields
.field private a:Z

.field private b:I

.field private k:J

.field private l:Lcom/twitter/android/ob;


# direct methods
.method public constructor <init>(Landroid/content/Context;IILcom/twitter/library/widget/a;Lcom/twitter/library/util/FriendshipCache;IIZZZIJLcom/twitter/android/ob;)V
    .locals 0

    invoke-direct/range {p0 .. p9}, Lcom/twitter/android/aaa;-><init>(Landroid/content/Context;IILcom/twitter/library/widget/a;Lcom/twitter/library/util/FriendshipCache;IIZZ)V

    iput-boolean p10, p0, Lcom/twitter/android/v;->a:Z

    iput p11, p0, Lcom/twitter/android/v;->b:I

    iput-wide p12, p0, Lcom/twitter/android/v;->k:J

    iput-object p14, p0, Lcom/twitter/android/v;->l:Lcom/twitter/android/ob;

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/library/widget/UserView;)Lcom/twitter/library/widget/UserView;
    .locals 3

    const v0, 0x7f020085    # com.twitter.android.R.drawable.btn_follow_action

    iget-object v1, p0, Lcom/twitter/android/v;->f:Lcom/twitter/library/widget/a;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/library/widget/UserView;->a(ILcom/twitter/library/widget/a;)V

    iget-object v0, p1, Lcom/twitter/library/widget/UserView;->m:Lcom/twitter/library/widget/ActionButton;

    const v1, 0x7f020086    # com.twitter.android.R.drawable.btn_follow_action_bg

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/ActionButton;->setBackgroundResource(I)V

    new-instance v0, Lcom/twitter/android/zx;

    invoke-direct {v0, p1}, Lcom/twitter/android/zx;-><init>(Lcom/twitter/library/widget/BaseUserView;)V

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/UserView;->setTag(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/twitter/android/v;->d:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object p1
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 9

    const/4 v8, 0x3

    invoke-static {p3}, Lcom/twitter/android/ActivityCursor;->a(Landroid/database/Cursor;)Lcom/twitter/library/api/TwitterUser;

    move-result-object v3

    move-object v0, p1

    check-cast v0, Lcom/twitter/android/widget/ActivityUserView;

    iget-wide v1, v3, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/widget/ActivityUserView;->setUserId(J)V

    iget-object v1, p0, Lcom/twitter/android/v;->c:Lcom/twitter/android/client/c;

    iget-object v2, p0, Lcom/twitter/android/v;->g:Lcom/twitter/library/util/FriendshipCache;

    iget-boolean v4, p0, Lcom/twitter/android/v;->a:Z

    iget-wide v5, p0, Lcom/twitter/android/v;->j:J

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/widget/ActivityUserView;->a(Lcom/twitter/android/client/c;Lcom/twitter/library/util/FriendshipCache;Lcom/twitter/library/api/TwitterUser;ZJ)V

    invoke-virtual {v0}, Lcom/twitter/android/widget/ActivityUserView;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/zx;

    if-eqz v1, :cond_0

    const/4 v2, 0x5

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/twitter/android/zx;->g:Ljava/lang/String;

    :cond_0
    iget v1, p0, Lcom/twitter/android/v;->b:I

    iget-wide v4, p0, Lcom/twitter/android/v;->k:J

    invoke-virtual {v3}, Lcom/twitter/library/api/TwitterUser;->a()J

    move-result-wide v6

    invoke-static {v1, v4, v5, v6, v7}, Lcom/twitter/library/scribe/ScribeItem;->a(IJJ)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/ActivityUserView;->setScribeItem(Lcom/twitter/library/scribe/ScribeItem;)V

    iget-object v0, p0, Lcom/twitter/android/v;->l:Lcom/twitter/android/ob;

    if-eqz v0, :cond_1

    iput v8, v1, Lcom/twitter/library/scribe/ScribeItem;->c:I

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0, v8}, Landroid/os/Bundle;-><init>(I)V

    const-string/jumbo v2, "scribe_item"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v1, "user_tag"

    invoke-virtual {v3}, Lcom/twitter/library/api/TwitterUser;->a()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string/jumbo v1, "position"

    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/twitter/android/v;->l:Lcom/twitter/android/ob;

    const/4 v2, 0x0

    invoke-interface {v1, p1, v2, v0}, Lcom/twitter/android/ob;->a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V

    :cond_1
    return-void
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03000d    # com.twitter.android.R.layout.activity_user_with_bio_view

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/ActivityUserView;

    invoke-virtual {p0, v0}, Lcom/twitter/android/v;->a(Lcom/twitter/library/widget/UserView;)Lcom/twitter/library/widget/UserView;

    return-object v0
.end method
