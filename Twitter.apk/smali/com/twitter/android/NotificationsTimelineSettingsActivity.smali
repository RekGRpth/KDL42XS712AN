.class public Lcom/twitter/android/NotificationsTimelineSettingsActivity;
.super Lcom/twitter/android/client/BasePreferenceActivity;
.source "Twttr"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field private a:Landroid/preference/ListPreference;

.field private b:Lcom/twitter/library/client/f;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BasePreferenceActivity;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;)I
    .locals 1

    const-string/jumbo v0, "following"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    const-string/jumbo v0, "filtered"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const-string/jumbo v0, "verified"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(I)Ljava/lang/String;
    .locals 1

    packed-switch p1, :pswitch_data_0

    const v0, 0x7f0f02c3    # com.twitter.android.R.string.notification_filter_none

    invoke-virtual {p0, v0}, Lcom/twitter/android/NotificationsTimelineSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    const v0, 0x7f0f02c0    # com.twitter.android.R.string.notification_filter_filtered

    invoke-virtual {p0, v0}, Lcom/twitter/android/NotificationsTimelineSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0f02c1    # com.twitter.android.R.string.notification_filter_follow_only

    invoke-virtual {p0, v0}, Lcom/twitter/android/NotificationsTimelineSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0f02c5    # com.twitter.android.R.string.notification_filter_verified

    invoke-virtual {p0, v0}, Lcom/twitter/android/NotificationsTimelineSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    const/4 v5, 0x0

    invoke-super {p0, p1}, Lcom/twitter/android/client/BasePreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f0f0415    # com.twitter.android.R.string.settings_notifications_timeline

    invoke-virtual {p0, v0}, Lcom/twitter/android/NotificationsTimelineSettingsActivity;->setTitle(I)V

    const v0, 0x7f060010    # com.twitter.android.R.xml.notifications_timeline_prefs

    invoke-virtual {p0, v0}, Lcom/twitter/android/NotificationsTimelineSettingsActivity;->addPreferencesFromResource(I)V

    new-instance v0, Lcom/twitter/library/client/f;

    invoke-virtual {p0}, Lcom/twitter/android/NotificationsTimelineSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/NotificationsTimelineSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "account_name"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/android/NotificationsTimelineSettingsActivity;->b:Lcom/twitter/library/client/f;

    const-string/jumbo v0, "timeline_filters"

    invoke-virtual {p0, v0}, Lcom/twitter/android/NotificationsTimelineSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    invoke-static {p0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/android/util/ah;->a(Lcom/twitter/library/api/TwitterUser;)Z

    move-result v2

    const-string/jumbo v1, "connect_tab"

    invoke-virtual {p0, v1}, Lcom/twitter/android/NotificationsTimelineSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    iget-object v3, p0, Lcom/twitter/android/NotificationsTimelineSettingsActivity;->b:Lcom/twitter/library/client/f;

    const-string/jumbo v4, "connect_tab"

    invoke-virtual {v3, v4, v5}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v1, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    invoke-virtual {v1, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    const-string/jumbo v1, "notifications_follow_only"

    invoke-virtual {p0, v1}, Lcom/twitter/android/NotificationsTimelineSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/CheckBoxPreference;

    if-nez v2, :cond_0

    invoke-static {}, Lgo;->a()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    :goto_0
    invoke-static {}, Lgm;->a()Z

    move-result v3

    const-string/jumbo v1, "vit_filters"

    invoke-virtual {p0, v1}, Lcom/twitter/android/NotificationsTimelineSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    check-cast v1, Landroid/preference/ListPreference;

    iput-object v1, p0, Lcom/twitter/android/NotificationsTimelineSettingsActivity;->a:Landroid/preference/ListPreference;

    if-eqz v3, :cond_2

    if-eqz v2, :cond_2

    iget-object v0, p0, Lcom/twitter/android/NotificationsTimelineSettingsActivity;->a:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/twitter/android/NotificationsTimelineSettingsActivity;->a:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/NotificationsTimelineSettingsActivity;->a:Landroid/preference/ListPreference;

    invoke-virtual {v0, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    :goto_1
    return-void

    :cond_1
    iget-object v3, p0, Lcom/twitter/android/NotificationsTimelineSettingsActivity;->b:Lcom/twitter/library/client/f;

    const-string/jumbo v4, "notifications_follow_only"

    invoke-virtual {v3, v4, v5}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v1, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    invoke-virtual {v1, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/twitter/android/NotificationsTimelineSettingsActivity;->a:Landroid/preference/ListPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_1
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 4

    const/4 v1, 0x1

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    const/4 v0, -0x1

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    :goto_1
    return v1

    :sswitch_0
    const-string/jumbo v3, "vit_filters"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const-string/jumbo v3, "notifications_follow_only"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :sswitch_2
    const-string/jumbo v3, "connect_tab"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    :pswitch_0
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/NotificationsTimelineSettingsActivity;->a(Ljava/lang/String;)I

    move-result v0

    iget-object v2, p0, Lcom/twitter/android/NotificationsTimelineSettingsActivity;->b:Lcom/twitter/library/client/f;

    invoke-virtual {v2}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v2

    const-string/jumbo v3, "vit_notification_filter_type"

    invoke-virtual {v2, v3, v0}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;I)Lcom/twitter/library/client/f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/f;->d()V

    iget-object v2, p0, Lcom/twitter/android/NotificationsTimelineSettingsActivity;->a:Landroid/preference/ListPreference;

    invoke-direct {p0, v0}, Lcom/twitter/android/NotificationsTimelineSettingsActivity;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_1

    :pswitch_1
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iget-object v2, p0, Lcom/twitter/android/NotificationsTimelineSettingsActivity;->b:Lcom/twitter/library/client/f;

    invoke-virtual {v2}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v2

    const-string/jumbo v3, "notifications_follow_only"

    invoke-virtual {v2, v3, v0}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;Z)Lcom/twitter/library/client/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->d()V

    goto :goto_1

    :pswitch_2
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iget-object v2, p0, Lcom/twitter/android/NotificationsTimelineSettingsActivity;->b:Lcom/twitter/library/client/f;

    invoke-virtual {v2}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v2

    const-string/jumbo v3, "connect_tab"

    invoke-virtual {v2, v3, v0}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;Z)Lcom/twitter/library/client/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->d()V

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x5b6b1aa3 -> :sswitch_1
        0x5c2c61dd -> :sswitch_0
        0x66c6e2c0 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
