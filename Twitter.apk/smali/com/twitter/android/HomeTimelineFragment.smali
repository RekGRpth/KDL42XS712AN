.class public Lcom/twitter/android/HomeTimelineFragment;
.super Lcom/twitter/android/TimelineFragment;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Lcom/twitter/android/widget/AttachmentObservableFrameLayout;

.field private b:Landroid/view/View;

.field private c:Landroid/view/View;

.field private g:Landroid/view/View;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/view/View;

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Lcom/twitter/library/api/Prompt;

.field private o:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/TimelineFragment;-><init>()V

    return-void
.end method

.method private G()V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/HomeTimelineFragment;->b:Landroid/view/View;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/HomeTimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030066    # com.twitter.android.R.layout.empty_timeline_header

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f09014d    # com.twitter.android.R.id.find_friends_cta

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/HomeTimelineFragment;->c:Landroid/view/View;

    iget-object v0, p0, Lcom/twitter/android/HomeTimelineFragment;->c:Landroid/view/View;

    const v2, 0x7f09014e    # com.twitter.android.R.id.empty_timeline_title_find_friends

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/HomeTimelineFragment;->i:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/twitter/android/HomeTimelineFragment;->c:Landroid/view/View;

    const v2, 0x7f090124    # com.twitter.android.R.id.import_contacts_btn

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/HomeTimelineFragment;->j:Landroid/view/View;

    const v0, 0x7f09014b    # com.twitter.android.R.id.empty_timeline_title

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/HomeTimelineFragment;->g:Landroid/view/View;

    iget-object v0, p0, Lcom/twitter/android/HomeTimelineFragment;->g:Landroid/view/View;

    const v2, 0x7f09014c    # com.twitter.android.R.id.empty_timeline_title_text

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/HomeTimelineFragment;->h:Landroid/widget/TextView;

    iput-object v1, p0, Lcom/twitter/android/HomeTimelineFragment;->b:Landroid/view/View;

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/HomeTimelineFragment;->a:Lcom/twitter/android/widget/AttachmentObservableFrameLayout;

    iget-object v1, p0, Lcom/twitter/android/HomeTimelineFragment;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/AttachmentObservableFrameLayout;->addView(Landroid/view/View;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/HomeTimelineFragment;->k:Z

    iget-object v0, p0, Lcom/twitter/android/HomeTimelineFragment;->a:Lcom/twitter/android/widget/AttachmentObservableFrameLayout;

    new-instance v1, Lcom/twitter/android/id;

    invoke-direct {v1, p0}, Lcom/twitter/android/id;-><init>(Lcom/twitter/android/HomeTimelineFragment;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/AttachmentObservableFrameLayout;->setWindowAttachmentListener(Lcom/twitter/android/widget/b;)V

    return-void
.end method

.method private H()V
    .locals 2

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/twitter/android/HomeTimelineFragment;->m:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object v1, p0, Lcom/twitter/android/HomeTimelineFragment;->n:Lcom/twitter/library/api/Prompt;

    iget-object v0, p0, Lcom/twitter/android/HomeTimelineFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/uh;

    invoke-virtual {v0, v1}, Lcom/twitter/android/uh;->a(Lcom/twitter/library/api/Prompt;)V

    goto :goto_0
.end method

.method private I()V
    .locals 4

    const/4 v2, 0x0

    const/16 v3, 0x8

    invoke-direct {p0}, Lcom/twitter/android/HomeTimelineFragment;->J()Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/HomeTimelineFragment;->a(Lcom/twitter/android/PromptView;)V

    iget-boolean v0, p0, Lcom/twitter/android/HomeTimelineFragment;->k:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/HomeTimelineFragment;->G()V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/HomeTimelineFragment;->b:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/twitter/android/HomeTimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/cx;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/HomeTimelineFragment;->i:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/twitter/android/HomeTimelineFragment;->K()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/HomeTimelineFragment;->g:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/HomeTimelineFragment;->c:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/HomeTimelineFragment;->j:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/HomeTimelineFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    const v1, 0x7f090040    # com.twitter.android.R.id.footer_dot

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-boolean v2, p0, Lcom/twitter/android/HomeTimelineFragment;->k:Z

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    :cond_2
    :goto_1
    return-void

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/HomeTimelineFragment;->h:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/twitter/android/HomeTimelineFragment;->K()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/HomeTimelineFragment;->c:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/HomeTimelineFragment;->g:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/twitter/android/HomeTimelineFragment;->b:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/HomeTimelineFragment;->b:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method private J()Z
    .locals 2

    iget v0, p0, Lcom/twitter/android/HomeTimelineFragment;->F:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/twitter/android/HomeTimelineFragment;->d:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/android/HomeTimelineFragment;->d:I

    const/4 v1, 0x7

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private K()Ljava/lang/String;
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    iget v0, p0, Lcom/twitter/android/HomeTimelineFragment;->d:I

    rsub-int/lit8 v0, v0, 0x7

    if-ne v0, v3, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/HomeTimelineFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0157    # com.twitter.android.R.string.empty_timeline_title_format_singular

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v1, 0x7

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/HomeTimelineFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0156    # com.twitter.android.R.string.empty_timeline_title_format_initial

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/HomeTimelineFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0155    # com.twitter.android.R.string.empty_timeline_title_format

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private L()V
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/android/HomeTimelineFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const v0, 0x7f09014f    # com.twitter.android.R.id.mini_wtf_fragment_container

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/MiniWTFFragment;

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    :cond_0
    return-void
.end method

.method private M()Z
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    invoke-virtual {p0}, Lcom/twitter/android/HomeTimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/HomeTimelineFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/HomeTimelineFragment;->H()V

    return-void
.end method

.method private a(Lcom/twitter/library/service/b;)V
    .locals 9

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/twitter/library/service/b;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    invoke-virtual {p0}, Lcom/twitter/android/HomeTimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    iget-object v0, v0, Lcom/twitter/library/service/e;->a:Landroid/os/Bundle;

    const-string/jumbo v3, "prompt"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/Prompt;

    iget-boolean v3, p0, Lcom/twitter/android/HomeTimelineFragment;->m:Z

    if-eqz v3, :cond_6

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/twitter/library/api/Prompt;->c()Z

    move-result v3

    if-eqz v3, :cond_4

    move v3, v1

    :goto_0
    if-eqz v0, :cond_5

    iget-wide v5, v0, Lcom/twitter/library/api/Prompt;->n:J

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-lez v5, :cond_5

    :goto_1
    iget-object v2, p0, Lcom/twitter/android/HomeTimelineFragment;->n:Lcom/twitter/library/api/Prompt;

    if-eqz v2, :cond_2

    if-eqz v3, :cond_0

    if-nez v1, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/twitter/android/HomeTimelineFragment;->n:Lcom/twitter/library/api/Prompt;

    invoke-virtual {v2}, Lcom/twitter/library/api/Prompt;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    invoke-direct {p0}, Lcom/twitter/android/HomeTimelineFragment;->H()V

    :cond_2
    if-eqz v3, :cond_6

    if-eqz v1, :cond_3

    iput-object v0, p0, Lcom/twitter/android/HomeTimelineFragment;->n:Lcom/twitter/library/api/Prompt;

    iget-object v0, p0, Lcom/twitter/android/HomeTimelineFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/uh;

    iget-object v1, p0, Lcom/twitter/android/HomeTimelineFragment;->n:Lcom/twitter/library/api/Prompt;

    invoke-virtual {v0, v1}, Lcom/twitter/android/uh;->a(Lcom/twitter/library/api/Prompt;)V

    :cond_3
    :goto_2
    return-void

    :cond_4
    move v3, v2

    goto :goto_0

    :cond_5
    move v1, v2

    goto :goto_1

    :cond_6
    if-nez v0, :cond_7

    invoke-static {v4}, Lgp;->d(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-static {v4}, Lgp;->a(Landroid/content/Context;)Lcom/twitter/library/api/Prompt;

    move-result-object v0

    :cond_7
    invoke-virtual {p0, v0}, Lcom/twitter/android/HomeTimelineFragment;->a(Lcom/twitter/library/api/Prompt;)V

    invoke-virtual {p0}, Lcom/twitter/android/HomeTimelineFragment;->an()V

    goto :goto_2
.end method

.method static synthetic a(Lcom/twitter/android/HomeTimelineFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/HomeTimelineFragment;->l:Z

    return p1
.end method

.method static synthetic b(Lcom/twitter/android/HomeTimelineFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/HomeTimelineFragment;->I()V

    return-void
.end method

.method static synthetic c(Lcom/twitter/android/HomeTimelineFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/HomeTimelineFragment;->L()V

    return-void
.end method

.method private g(I)Z
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x2

    if-eq p1, v0, :cond_0

    const/4 v0, 0x4

    if-ne p1, v0, :cond_3

    :cond_0
    move v0, v2

    :goto_0
    iget-boolean v3, p0, Lcom/twitter/android/HomeTimelineFragment;->m:Z

    if-eqz v3, :cond_2

    if-nez v0, :cond_1

    const/4 v0, 0x3

    if-ne p1, v0, :cond_4

    :cond_1
    move v0, v2

    :cond_2
    :goto_1
    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/twitter/android/HomeTimelineFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->s()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/twitter/android/HomeTimelineFragment;->F()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/twitter/android/HomeTimelineFragment;->u()Z

    move-result v0

    if-eqz v0, :cond_5

    :goto_2
    return v2

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    :cond_5
    move v2, v1

    goto :goto_2
.end method


# virtual methods
.method public a(Landroid/content/Context;IILcom/twitter/library/service/b;)V
    .locals 10

    iget-object v0, p4, Lcom/twitter/library/service/b;->b:Ljava/lang/String;

    const-class v1, Lcom/twitter/library/client/t;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/HomeTimelineFragment;->ad()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x3

    if-eq p3, v0, :cond_2

    const/4 v0, 0x4

    if-ne p3, v0, :cond_3

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/HomeTimelineFragment;->U()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/twitter/android/HomeTimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/android/HomeTimelineFragment;->Q:J

    const-string/jumbo v3, "home:first_tweet_api"

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/android/util/AppMetrics;->a(Landroid/content/Context;JLjava/lang/String;)V

    :cond_3
    :goto_1
    if-nez p2, :cond_e

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/HomeTimelineFragment;->l:Z

    iget-object v0, p4, Lcom/twitter/library/service/b;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/twitter/android/HomeTimelineFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    invoke-virtual {p0, p3}, Lcom/twitter/android/HomeTimelineFragment;->b(I)V

    iget-object v0, p4, Lcom/twitter/library/service/b;->b:Ljava/lang/String;

    const-class v1, Lcom/twitter/library/api/q;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-direct {p0, p4}, Lcom/twitter/android/HomeTimelineFragment;->a(Lcom/twitter/library/service/b;)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/twitter/android/HomeTimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string/jumbo v1, "home:first_tweet_api"

    invoke-static {v0, v1}, Lcom/twitter/android/util/AppMetrics;->b(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    invoke-virtual {p4}, Lcom/twitter/library/service/b;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-nez v0, :cond_6

    const v0, 0x7f0f04e3    # com.twitter.android.R.string.tweets_fetch_error

    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_6
    iget-boolean v0, p0, Lcom/twitter/android/HomeTimelineFragment;->m:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p3}, Lcom/twitter/android/HomeTimelineFragment;->g(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/HomeTimelineFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "home_timeline,action_tooltip"

    const/4 v0, 0x0

    move v2, v0

    :goto_2
    const/4 v0, 0x5

    if-ge v2, v0, :cond_f

    new-instance v5, Lcom/twitter/library/provider/Tweet;

    invoke-direct {v5, v3}, Lcom/twitter/library/provider/Tweet;-><init>(Landroid/database/Cursor;)V

    invoke-virtual {v5}, Lcom/twitter/library/provider/Tweet;->v()Z

    move-result v0

    if-nez v0, :cond_7

    invoke-virtual {v5}, Lcom/twitter/library/provider/Tweet;->ab()Z

    move-result v0

    if-nez v0, :cond_7

    invoke-virtual {v5}, Lcom/twitter/library/provider/Tweet;->Y()Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_7
    const/4 v0, 0x1

    :goto_3
    iget-object v6, p0, Lcom/twitter/android/HomeTimelineFragment;->n:Lcom/twitter/library/api/Prompt;

    if-eqz v6, :cond_a

    iget-object v6, p0, Lcom/twitter/android/HomeTimelineFragment;->n:Lcom/twitter/library/api/Prompt;

    iget-wide v6, v6, Lcom/twitter/library/api/Prompt;->n:J

    iget-wide v8, v5, Lcom/twitter/library/provider/Tweet;->u:J

    cmp-long v6, v6, v8

    if-nez v6, :cond_a

    const-string/jumbo v0, "home_timeline"

    :goto_4
    invoke-virtual {p0}, Lcom/twitter/android/HomeTimelineFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-static {p1}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;)Lcom/twitter/android/util/d;

    move-result-object v2

    invoke-interface {v2}, Lcom/twitter/android/util/d;->g()Z

    move-result v2

    invoke-static {p1, v1, v0, v2}, Lcom/twitter/library/api/q;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Z)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "home_timeline,action_tooltip"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string/jumbo v0, "tweet_ids"

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/service/b;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    :cond_8
    iget v0, p0, Lcom/twitter/android/HomeTimelineFragment;->F:I

    const/4 v2, 0x6

    invoke-virtual {p0, v1, v0, v2}, Lcom/twitter/android/HomeTimelineFragment;->a(Lcom/twitter/library/service/b;II)Z

    goto/16 :goto_0

    :cond_9
    const/4 v0, 0x0

    goto :goto_3

    :cond_a
    if-nez v0, :cond_c

    invoke-virtual {v5}, Lcom/twitter/library/provider/Tweet;->J()Z

    move-result v0

    if-nez v0, :cond_c

    invoke-virtual {v5}, Lcom/twitter/library/provider/Tweet;->E()Z

    move-result v0

    if-nez v0, :cond_c

    if-lez v2, :cond_b

    const-string/jumbo v0, ","

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_b
    iget-wide v5, v5, Lcom/twitter/library/provider/Tweet;->u:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    :cond_c
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_d

    move-object v0, v1

    goto :goto_4

    :cond_d
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_e
    invoke-super {p0, p1, p2, p3, p4}, Lcom/twitter/android/TimelineFragment;->a(Landroid/content/Context;IILcom/twitter/library/service/b;)V

    goto/16 :goto_0

    :cond_f
    move-object v0, v1

    goto :goto_4
.end method

.method protected a(Landroid/view/View;)Z
    .locals 1

    invoke-static {}, Lcom/twitter/library/client/App;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/HomeTimelineFragment;->ax()Lcom/twitter/library/client/m;

    move-result-object v0

    if-eqz v0, :cond_0

    const v0, 0x7f09028f    # com.twitter.android.R.id.banner_text

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iput-object v0, p0, Lcom/twitter/android/HomeTimelineFragment;->o:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/HomeTimelineFragment;->o:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Lcom/twitter/android/client/q;)Z
    .locals 8

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/android/HomeTimelineFragment;->o:Landroid/widget/TextView;

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/twitter/android/HomeTimelineFragment;->ax()Lcom/twitter/library/client/m;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/twitter/library/client/m;->b()I

    move-result v2

    :goto_0
    if-lez v2, :cond_0

    iget-object v3, p0, Lcom/twitter/android/HomeTimelineFragment;->o:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/twitter/android/HomeTimelineFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e0009    # com.twitter.android.R.plurals.new_tweet_count

    new-array v6, v0, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v1

    invoke-virtual {v4, v5, v2, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_1
    if-lez v2, :cond_2

    :goto_2
    return v0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_2

    :cond_3
    move v2, v1

    goto :goto_1
.end method

.method protected c(I)Z
    .locals 4

    invoke-virtual {p0, p1}, Lcom/twitter/android/HomeTimelineFragment;->c_(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Lcom/twitter/library/featureswitch/a;->aK()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/HomeTimelineFragment;->m:Z

    iget-object v0, p0, Lcom/twitter/android/HomeTimelineFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/uh;

    iget-boolean v1, p0, Lcom/twitter/android/HomeTimelineFragment;->m:Z

    invoke-virtual {v0, v1}, Lcom/twitter/android/uh;->a(Z)V

    iget-boolean v0, p0, Lcom/twitter/android/HomeTimelineFragment;->m:Z

    if-nez v0, :cond_1

    invoke-direct {p0, p1}, Lcom/twitter/android/HomeTimelineFragment;->g(I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/HomeTimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/HomeTimelineFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    const-string/jumbo v2, "home_timeline"

    invoke-static {v0}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;)Lcom/twitter/android/util/d;

    move-result-object v3

    invoke-interface {v3}, Lcom/twitter/android/util/d;->g()Z

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/library/api/q;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Z)Lcom/twitter/library/service/b;

    move-result-object v0

    iget v1, p0, Lcom/twitter/android/HomeTimelineFragment;->F:I

    const/4 v2, 0x6

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/HomeTimelineFragment;->a(Lcom/twitter/library/service/b;II)Z

    :cond_1
    const/4 v0, 0x3

    if-eq p1, v0, :cond_2

    const/4 v0, 0x4

    if-ne p1, v0, :cond_3

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/HomeTimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string/jumbo v1, "home:first_tweet_api"

    invoke-static {v0, v1}, Lcom/twitter/android/util/AppMetrics;->a(Landroid/content/Context;Ljava/lang/String;)V

    :cond_3
    invoke-super {p0, p1}, Lcom/twitter/android/TimelineFragment;->c(I)Z

    move-result v0

    goto :goto_0
.end method

.method public e_(I)V
    .locals 2

    iget-boolean v0, p0, Lcom/twitter/android/HomeTimelineFragment;->m:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    packed-switch p1, :pswitch_data_0

    :goto_1
    invoke-direct {p0}, Lcom/twitter/android/HomeTimelineFragment;->H()V

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/twitter/android/HomeTimelineFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/HomeTimelineFragment;->n:Lcom/twitter/library/api/Prompt;

    iget v1, v1, Lcom/twitter/library/api/Prompt;->b:I

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->e(I)V

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/twitter/android/HomeTimelineFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/HomeTimelineFragment;->n:Lcom/twitter/library/api/Prompt;

    iget v1, v1, Lcom/twitter/library/api/Prompt;->b:I

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->d(I)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected h()V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/HomeTimelineFragment;->l:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/HomeTimelineFragment;->g()V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Lcom/twitter/android/TimelineFragment;->h()V

    goto :goto_0
.end method

.method protected o()Z
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/HomeTimelineFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/TimelineFragment;->onActivityCreated(Landroid/os/Bundle;)V

    new-instance v0, Lcom/twitter/android/ig;

    invoke-direct {v0, p0}, Lcom/twitter/android/ig;-><init>(Lcom/twitter/android/HomeTimelineFragment;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/HomeTimelineFragment;->a(Lcom/twitter/library/client/z;)V

    iget-object v0, p0, Lcom/twitter/android/HomeTimelineFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/uh;

    iget-boolean v1, p0, Lcom/twitter/android/HomeTimelineFragment;->m:Z

    invoke-virtual {v0, v1}, Lcom/twitter/android/uh;->a(Z)V

    iget-boolean v1, p0, Lcom/twitter/android/HomeTimelineFragment;->m:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/HomeTimelineFragment;->n:Lcom/twitter/library/api/Prompt;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/HomeTimelineFragment;->n:Lcom/twitter/library/api/Prompt;

    invoke-virtual {v0, v1}, Lcom/twitter/android/uh;->a(Lcom/twitter/library/api/Prompt;)V

    :cond_0
    new-instance v0, Lcom/twitter/android/widget/AttachmentObservableFrameLayout;

    invoke-virtual {p0}, Lcom/twitter/android/HomeTimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/android/widget/AttachmentObservableFrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/HomeTimelineFragment;->a:Lcom/twitter/android/widget/AttachmentObservableFrameLayout;

    invoke-virtual {p0}, Lcom/twitter/android/HomeTimelineFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/HomeTimelineFragment;->a:Lcom/twitter/android/widget/AttachmentObservableFrameLayout;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/twitter/android/HomeTimelineFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->j()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0, v0}, Lcom/twitter/android/HomeTimelineFragment;->d(Ljava/lang/String;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/HomeTimelineFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v0

    if-eqz v0, :cond_1

    iget v1, v0, Lcom/twitter/library/api/TwitterUser;->friendsCount:I

    if-ltz v1, :cond_1

    iget-object v1, v0, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget v0, v0, Lcom/twitter/library/api/TwitterUser;->friendsCount:I

    iput v0, p0, Lcom/twitter/android/HomeTimelineFragment;->d:I

    invoke-direct {p0}, Lcom/twitter/android/HomeTimelineFragment;->I()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f09028f    # com.twitter.android.R.id.banner_text

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/HomeTimelineFragment;->ax()Lcom/twitter/library/client/m;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/client/m;->d()V

    invoke-virtual {p0}, Lcom/twitter/android/HomeTimelineFragment;->ad()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-super {p0, p1}, Lcom/twitter/android/TimelineFragment;->onClick(Landroid/view/View;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/TimelineFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->aK()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/HomeTimelineFragment;->m:Z

    if-eqz p1, :cond_0

    const-string/jumbo v0, "action_prompt"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/Prompt;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/api/Prompt;->a()Z

    move-result v1

    if-nez v1, :cond_0

    iput-object v0, p0, Lcom/twitter/android/HomeTimelineFragment;->n:Lcom/twitter/library/api/Prompt;

    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    const v0, 0x7f030152    # com.twitter.android.R.layout.timeline_fragment

    invoke-virtual {p0, p1, v0, p2}, Lcom/twitter/android/HomeTimelineFragment;->a(Landroid/view/LayoutInflater;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroyView()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/TimelineFragment;->onDestroyView()V

    iget-boolean v0, p0, Lcom/twitter/android/HomeTimelineFragment;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/HomeTimelineFragment;->a:Lcom/twitter/android/widget/AttachmentObservableFrameLayout;

    iget-object v1, p0, Lcom/twitter/android/HomeTimelineFragment;->b:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/AttachmentObservableFrameLayout;->removeView(Landroid/view/View;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/HomeTimelineFragment;->k:Z

    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    invoke-direct {p0}, Lcom/twitter/android/HomeTimelineFragment;->M()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    invoke-direct {p0}, Lcom/twitter/android/HomeTimelineFragment;->L()V

    :cond_2
    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-super {p0}, Lcom/twitter/android/TimelineFragment;->onResume()V

    invoke-direct {p0}, Lcom/twitter/android/HomeTimelineFragment;->I()V

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->aT()Ljava/lang/String;

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/TimelineFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/twitter/android/HomeTimelineFragment;->n:Lcom/twitter/library/api/Prompt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/HomeTimelineFragment;->n:Lcom/twitter/library/api/Prompt;

    invoke-virtual {v0}, Lcom/twitter/library/api/Prompt;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "action_prompt"

    iget-object v1, p0, Lcom/twitter/android/HomeTimelineFragment;->n:Lcom/twitter/library/api/Prompt;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    return-void
.end method

.method protected s()Lcom/twitter/android/va;
    .locals 1

    new-instance v0, Lcom/twitter/android/if;

    invoke-direct {v0, p0}, Lcom/twitter/android/if;-><init>(Lcom/twitter/android/HomeTimelineFragment;)V

    return-object v0
.end method

.method protected t()Lcom/twitter/android/vc;
    .locals 6

    new-instance v0, Lcom/twitter/android/ih;

    iget-object v3, p0, Lcom/twitter/android/HomeTimelineFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {p0}, Lcom/twitter/android/HomeTimelineFragment;->C()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/android/HomeTimelineFragment;->B:Lcom/twitter/android/vn;

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/ih;-><init>(Lcom/twitter/android/HomeTimelineFragment;Landroid/support/v4/app/Fragment;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Lcom/twitter/android/vn;)V

    return-object v0
.end method

.method protected u()Z
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/TimelineFragment;->u()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/HomeTimelineFragment;->b:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/HomeTimelineFragment;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected v()Z
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/TimelineFragment;->v()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/HomeTimelineFragment;->b:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/HomeTimelineFragment;->b:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public w()Ljava/lang/String;
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/HomeTimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/service/TimelineHelper;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    const-string/jumbo v3, "PTR Override: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-static {v3, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method protected x()Lcom/twitter/library/client/r;
    .locals 4

    invoke-virtual {p0}, Lcom/twitter/android/HomeTimelineFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/client/t;

    invoke-virtual {p0}, Lcom/twitter/android/HomeTimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v3

    invoke-direct {v1, v2, v0, v3}, Lcom/twitter/library/client/t;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/api/TwitterUser;)V

    return-object v1
.end method

.method protected synthetic y()Lcom/twitter/android/yb;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/HomeTimelineFragment;->t()Lcom/twitter/android/vc;

    move-result-object v0

    return-object v0
.end method
