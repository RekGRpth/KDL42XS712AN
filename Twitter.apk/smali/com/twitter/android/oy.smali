.class Lcom/twitter/android/oy;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/twitter/android/PostActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/PostActivity;I)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/oy;->b:Lcom/twitter/android/PostActivity;

    iput p2, p0, Lcom/twitter/android/oy;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/twitter/android/oy;->b:Lcom/twitter/android/PostActivity;

    invoke-virtual {v0, v4}, Lcom/twitter/android/PostActivity;->setResult(I)V

    iget-object v0, p0, Lcom/twitter/android/oy;->b:Lcom/twitter/android/PostActivity;

    invoke-static {v0}, Lcom/twitter/android/PostActivity;->m(Lcom/twitter/android/PostActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/oy;->b:Lcom/twitter/android/PostActivity;

    invoke-static {v0}, Lcom/twitter/android/PostActivity;->n(Lcom/twitter/android/PostActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v2, p0, Lcom/twitter/android/oy;->b:Lcom/twitter/android/PostActivity;

    invoke-static {v2}, Lcom/twitter/android/PostActivity;->o(Lcom/twitter/android/PostActivity;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "welcome:compose:::failure"

    aput-object v3, v2, v4

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/oy;->b:Lcom/twitter/android/PostActivity;

    invoke-virtual {v0, v4}, Lcom/twitter/android/PostActivity;->d(Z)V

    iget v0, p0, Lcom/twitter/android/oy;->a:I

    const/16 v1, 0x201

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/oy;->b:Lcom/twitter/android/PostActivity;

    invoke-static {v0}, Lcom/twitter/android/PostActivity;->p(Lcom/twitter/android/PostActivity;)V

    iget-object v0, p0, Lcom/twitter/android/oy;->b:Lcom/twitter/android/PostActivity;

    invoke-virtual {v0}, Lcom/twitter/android/PostActivity;->finish()V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/oy;->b:Lcom/twitter/android/PostActivity;

    invoke-static {v0}, Lcom/twitter/android/PostActivity;->q(Lcom/twitter/android/PostActivity;)V

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 10

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    iget-object v0, p0, Lcom/twitter/android/oy;->b:Lcom/twitter/android/PostActivity;

    invoke-static {v0}, Lcom/twitter/android/PostActivity;->g(Lcom/twitter/android/PostActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/oy;->b:Lcom/twitter/android/PostActivity;

    invoke-static {v1}, Lcom/twitter/android/PostActivity;->h(Lcom/twitter/android/PostActivity;)I

    move-result v1

    if-ne v1, v6, :cond_2

    const-string/jumbo v1, "cancel_reply_sheet"

    :goto_0
    const/4 v2, -0x3

    if-ne p2, v2, :cond_3

    iget-object v2, p0, Lcom/twitter/android/oy;->b:Lcom/twitter/android/PostActivity;

    iget-object v2, v2, Lcom/twitter/android/PostActivity;->b:Lcom/twitter/android/widget/MediaAttachmentsView;

    invoke-virtual {v2}, Lcom/twitter/android/widget/MediaAttachmentsView;->a()V

    iget-object v2, p0, Lcom/twitter/android/oy;->b:Lcom/twitter/android/PostActivity;

    iget-object v2, v2, Lcom/twitter/android/PostActivity;->r:Lcom/twitter/android/PostStorage;

    invoke-virtual {v2}, Lcom/twitter/android/PostStorage;->e()V

    iget-object v2, p0, Lcom/twitter/android/oy;->b:Lcom/twitter/android/PostActivity;

    invoke-static {v2}, Lcom/twitter/android/PostActivity;->i(Lcom/twitter/android/PostActivity;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/oy;->b:Lcom/twitter/android/PostActivity;

    iget-object v2, v2, Lcom/twitter/android/PostActivity;->s:Lcom/twitter/library/client/Session;

    iget-object v3, p0, Lcom/twitter/android/oy;->b:Lcom/twitter/android/PostActivity;

    invoke-static {v3}, Lcom/twitter/android/PostActivity;->i(Lcom/twitter/android/PostActivity;)J

    move-result-wide v3

    invoke-virtual {v0, v2, v3, v4}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/Session;J)V

    :cond_0
    iget-object v2, p0, Lcom/twitter/android/oy;->b:Lcom/twitter/android/PostActivity;

    invoke-static {v2}, Lcom/twitter/android/PostActivity;->j(Lcom/twitter/android/PostActivity;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    new-array v4, v9, [Ljava/lang/String;

    const-string/jumbo v5, ":composition"

    aput-object v5, v4, v7

    aput-object v1, v4, v6

    const-string/jumbo v1, "dont_save:click"

    aput-object v1, v4, v8

    invoke-virtual {v0, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/twitter/android/oy;->a()V

    :cond_1
    :goto_1
    return-void

    :cond_2
    const-string/jumbo v1, "cancel_sheet"

    goto :goto_0

    :cond_3
    const/4 v2, -0x1

    if-ne p2, v2, :cond_1

    iget-object v2, p0, Lcom/twitter/android/oy;->b:Lcom/twitter/android/PostActivity;

    invoke-static {v2}, Lcom/twitter/android/PostActivity;->k(Lcom/twitter/android/PostActivity;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    new-array v4, v9, [Ljava/lang/String;

    const-string/jumbo v5, ":composition"

    aput-object v5, v4, v7

    aput-object v1, v4, v6

    const-string/jumbo v1, "save_draft:click"

    aput-object v1, v4, v8

    invoke-virtual {v0, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v1, p0, Lcom/twitter/android/oy;->b:Lcom/twitter/android/PostActivity;

    iget-object v1, v1, Lcom/twitter/android/PostActivity;->s:Lcom/twitter/library/client/Session;

    iget-object v2, p0, Lcom/twitter/android/oy;->b:Lcom/twitter/android/PostActivity;

    iget-object v2, v2, Lcom/twitter/android/PostActivity;->p:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v2}, Lcom/twitter/android/TweetBoxFragment;->i()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/oy;->b:Lcom/twitter/android/PostActivity;

    invoke-static {v3}, Lcom/twitter/android/PostActivity;->i(Lcom/twitter/android/PostActivity;)J

    move-result-wide v3

    iget-object v5, p0, Lcom/twitter/android/oy;->b:Lcom/twitter/android/PostActivity;

    iget-wide v5, v5, Lcom/twitter/android/PostActivity;->C:J

    iget-object v7, p0, Lcom/twitter/android/oy;->b:Lcom/twitter/android/PostActivity;

    invoke-virtual {v7}, Lcom/twitter/android/PostActivity;->r()Lcom/twitter/library/api/TweetEntities;

    move-result-object v7

    iget-object v8, p0, Lcom/twitter/android/oy;->b:Lcom/twitter/android/PostActivity;

    invoke-static {v8}, Lcom/twitter/android/PostActivity;->l(Lcom/twitter/android/PostActivity;)Lcom/twitter/library/api/PromotedContent;

    move-result-object v8

    invoke-virtual/range {v0 .. v8}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;JJLcom/twitter/library/api/TweetEntities;Lcom/twitter/library/api/PromotedContent;)V

    invoke-direct {p0}, Lcom/twitter/android/oy;->a()V

    goto :goto_1
.end method
