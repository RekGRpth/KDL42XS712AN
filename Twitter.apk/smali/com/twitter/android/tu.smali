.class Lcom/twitter/android/tu;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/client/ak;


# instance fields
.field final synthetic a:Lcom/twitter/android/StartActivity;


# direct methods
.method private constructor <init>(Lcom/twitter/android/StartActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/tu;->a:Lcom/twitter/android/StartActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/StartActivity;Lcom/twitter/android/tr;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/tu;-><init>(Lcom/twitter/android/StartActivity;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;IILcom/twitter/library/api/t;)V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/tu;->a:Lcom/twitter/android/StartActivity;

    invoke-static {v0}, Lcom/twitter/android/StartActivity;->d(Lcom/twitter/android/StartActivity;)Lcom/twitter/android/util/r;

    move-result-object v0

    invoke-interface {v0, p3, p4}, Lcom/twitter/android/util/r;->a(ILcom/twitter/library/api/t;)V

    iget-object v0, p0, Lcom/twitter/android/tu;->a:Lcom/twitter/android/StartActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/StartActivity;->removeDialog(I)V

    iget-object v0, p0, Lcom/twitter/android/tu;->a:Lcom/twitter/android/StartActivity;

    invoke-static {v0}, Lcom/twitter/android/StartActivity;->d(Lcom/twitter/android/StartActivity;)Lcom/twitter/android/util/r;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/android/util/r;->r()Lcom/twitter/android/util/r;

    iget-object v0, p0, Lcom/twitter/android/tu;->a:Lcom/twitter/android/StartActivity;

    invoke-virtual {v0}, Lcom/twitter/android/StartActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/android/tu;->a:Lcom/twitter/android/StartActivity;

    const-class v3, Lcom/twitter/android/SignUpActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "android.intent.extra.INTENT"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "android.intent.extra.INTENT"

    const-string/jumbo v3, "android.intent.extra.INTENT"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/tu;->a:Lcom/twitter/android/StartActivity;

    invoke-virtual {v0, v1}, Lcom/twitter/android/StartActivity;->startActivity(Landroid/content/Intent;)V

    return-void

    :cond_0
    const-string/jumbo v0, "android.intent.extra.INTENT"

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/twitter/android/tu;->a:Lcom/twitter/android/StartActivity;

    const-class v4, Lcom/twitter/android/WelcomeActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/client/Session;Lcom/twitter/library/api/t;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;)V
    .locals 7

    iget-object v0, p0, Lcom/twitter/android/tu;->a:Lcom/twitter/android/StartActivity;

    invoke-static {v0}, Lcom/twitter/android/StartActivity;->d(Lcom/twitter/android/StartActivity;)Lcom/twitter/android/util/r;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/android/util/r;->t()V

    iget-object v0, p0, Lcom/twitter/android/tu;->a:Lcom/twitter/android/StartActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/StartActivity;->removeDialog(I)V

    iget-object v0, p0, Lcom/twitter/android/tu;->a:Lcom/twitter/android/StartActivity;

    invoke-static {v0}, Lcom/twitter/android/StartActivity;->d(Lcom/twitter/android/StartActivity;)Lcom/twitter/android/util/r;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/android/util/r;->c()V

    iget-object v0, p0, Lcom/twitter/android/tu;->a:Lcom/twitter/android/StartActivity;

    iget-object v1, p0, Lcom/twitter/android/tu;->a:Lcom/twitter/android/StartActivity;

    invoke-static {v1}, Lcom/twitter/android/StartActivity;->d(Lcom/twitter/android/StartActivity;)Lcom/twitter/android/util/r;

    move-result-object v1

    invoke-interface {v1}, Lcom/twitter/android/util/r;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/tu;->a:Lcom/twitter/android/StartActivity;

    invoke-static {v2}, Lcom/twitter/android/StartActivity;->d(Lcom/twitter/android/StartActivity;)Lcom/twitter/android/util/r;

    move-result-object v2

    invoke-interface {v2}, Lcom/twitter/android/util/r;->f()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/tu;->a:Lcom/twitter/android/StartActivity;

    invoke-static {v3}, Lcom/twitter/android/StartActivity;->d(Lcom/twitter/android/StartActivity;)Lcom/twitter/android/util/r;

    move-result-object v3

    invoke-interface {v3}, Lcom/twitter/android/util/r;->e()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/tu;->a:Lcom/twitter/android/StartActivity;

    invoke-static {v4}, Lcom/twitter/android/StartActivity;->d(Lcom/twitter/android/StartActivity;)Lcom/twitter/android/util/r;

    move-result-object v4

    invoke-interface {v4}, Lcom/twitter/android/util/r;->h()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/android/tu;->a:Lcom/twitter/android/StartActivity;

    invoke-static {v5}, Lcom/twitter/android/StartActivity;->d(Lcom/twitter/android/StartActivity;)Lcom/twitter/android/util/r;

    move-result-object v5

    invoke-interface {v5}, Lcom/twitter/android/util/r;->h()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/twitter/android/tu;->a:Lcom/twitter/android/StartActivity;

    invoke-static {v6}, Lcom/twitter/android/StartActivity;->d(Lcom/twitter/android/StartActivity;)Lcom/twitter/android/util/r;

    move-result-object v6

    invoke-interface {v6}, Lcom/twitter/android/util/r;->i()Ljava/lang/String;

    move-result-object v6

    invoke-static/range {v0 .. v6}, Lcom/twitter/android/StartActivity;->a(Lcom/twitter/android/StartActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
