.class Lcom/twitter/android/kv;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:Landroid/widget/ImageView;

.field public final b:Landroid/widget/TextView;

.field public final c:Landroid/widget/TextView;

.field public final d:Landroid/view/View;

.field public final e:Landroid/view/View;

.field public final f:Landroid/view/ViewGroup;

.field public final g:Lcom/twitter/library/widget/ActionButton;

.field public h:I

.field public i:Ljava/lang/String;

.field public j:Lcom/twitter/library/provider/Tweet;

.field public k:Lcom/twitter/library/api/TwitterUser;

.field public l:J


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/view/View;Landroid/view/View;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7f09005e    # com.twitter.android.R.id.user_image

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/kv;->a:Landroid/widget/ImageView;

    const v0, 0x7f090076    # com.twitter.android.R.id.title_view

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/kv;->b:Landroid/widget/TextView;

    const v0, 0x7f090201    # com.twitter.android.R.id.subtitle_view

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/kv;->c:Landroid/widget/TextView;

    const v0, 0x7f09002c    # com.twitter.android.R.id.action_button

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/ActionButton;

    iput-object v0, p0, Lcom/twitter/android/kv;->g:Lcom/twitter/library/widget/ActionButton;

    const v0, 0x7f090202    # com.twitter.android.R.id.retweet_container

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/kv;->f:Landroid/view/ViewGroup;

    iput-object p2, p0, Lcom/twitter/android/kv;->e:Landroid/view/View;

    iput-object p3, p0, Lcom/twitter/android/kv;->d:Landroid/view/View;

    return-void
.end method

.method public static a(Landroid/view/LayoutInflater;Landroid/content/Context;Ljava/util/ArrayList;Landroid/view/View$OnClickListener;I)Landroid/view/View;
    .locals 7

    const/4 v3, 0x0

    invoke-virtual {p0, p4, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0300d6    # com.twitter.android.R.layout.notifications_teachable_favorite_footer

    invoke-virtual {p0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f03009d    # com.twitter.android.R.layout.inline_composebox

    invoke-virtual {p0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    new-instance v3, Lcom/twitter/android/ku;

    invoke-direct {v3, v2}, Lcom/twitter/android/ku;-><init>(Landroid/view/View;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v2, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v3, Lcom/twitter/android/kv;

    invoke-direct {v3, v0, v1, v2}, Lcom/twitter/android/kv;-><init>(Landroid/view/View;Landroid/view/View;Landroid/view/View;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v0, p3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v4, v3, Lcom/twitter/android/kv;->g:Lcom/twitter/library/widget/ActionButton;

    if-eqz v4, :cond_0

    new-instance v5, Lcom/twitter/android/l;

    invoke-direct {v5}, Lcom/twitter/android/l;-><init>()V

    invoke-virtual {v4, v5}, Lcom/twitter/library/widget/ActionButton;->setTag(Ljava/lang/Object;)V

    const v5, 0x7f020085    # com.twitter.android.R.drawable.btn_follow_action

    invoke-virtual {v4, v5}, Lcom/twitter/library/widget/ActionButton;->a(I)V

    invoke-virtual {v4, p3}, Lcom/twitter/library/widget/ActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    new-instance v4, Lcom/twitter/internal/android/widget/HighlightedLinearLayout;

    invoke-direct {v4, p1}, Lcom/twitter/internal/android/widget/HighlightedLinearLayout;-><init>(Landroid/content/Context;)V

    invoke-static {}, Lcom/twitter/android/h;->c()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v5

    invoke-virtual {v4, v3}, Lcom/twitter/internal/android/widget/HighlightedLinearLayout;->setTag(Ljava/lang/Object;)V

    invoke-static {}, Lcom/twitter/android/h;->b()Landroid/widget/AbsListView$LayoutParams;

    move-result-object v6

    invoke-virtual {v4, v6}, Lcom/twitter/internal/android/widget/HighlightedLinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Lcom/twitter/internal/android/widget/HighlightedLinearLayout;->setOrientation(I)V

    invoke-virtual {v4, v0, v5}, Lcom/twitter/internal/android/widget/HighlightedLinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v4, v1, v5}, Lcom/twitter/internal/android/widget/HighlightedLinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v4, v2, v5}, Lcom/twitter/internal/android/widget/HighlightedLinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, v3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v4
.end method

.method public static a(Landroid/content/Context;Landroid/view/View;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/provider/Tweet;ILcom/twitter/android/client/c;Lcom/twitter/library/util/FriendshipCache;)V
    .locals 4

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/kv;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p2, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    invoke-virtual {p5, v2}, Lcom/twitter/android/client/c;->g(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v3, v0, Lcom/twitter/android/kv;->a:Landroid/widget/ImageView;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :goto_0
    iput p4, v0, Lcom/twitter/android/kv;->h:I

    iput-object p2, v0, Lcom/twitter/android/kv;->k:Lcom/twitter/library/api/TwitterUser;

    iput-object p3, v0, Lcom/twitter/android/kv;->j:Lcom/twitter/library/provider/Tweet;

    const/4 v2, 0x1

    if-ne p4, v2, :cond_1

    const-string/jumbo v2, "favorited"

    iput-object v2, v0, Lcom/twitter/android/kv;->i:Ljava/lang/String;

    :goto_1
    invoke-virtual {v0, p0, p5, v1, p6}, Lcom/twitter/android/kv;->a(Landroid/content/Context;Lcom/twitter/android/client/c;Landroid/content/res/Resources;Lcom/twitter/library/util/FriendshipCache;)V

    return-void

    :cond_0
    iget-object v2, v0, Lcom/twitter/android/kv;->a:Landroid/widget/ImageView;

    const v3, 0x7f02003a    # com.twitter.android.R.drawable.bg_no_profile_photo_md

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_1
    const/4 v2, 0x2

    if-ne p4, v2, :cond_2

    const-string/jumbo v2, "mention"

    iput-object v2, v0, Lcom/twitter/android/kv;->i:Ljava/lang/String;

    goto :goto_1

    :cond_2
    const/4 v2, 0x3

    if-ne p4, v2, :cond_3

    const-string/jumbo v2, "reply"

    iput-object v2, v0, Lcom/twitter/android/kv;->i:Ljava/lang/String;

    goto :goto_1

    :cond_3
    const/4 v2, 0x5

    if-ne p4, v2, :cond_4

    const-string/jumbo v2, "followed"

    iput-object v2, v0, Lcom/twitter/android/kv;->i:Ljava/lang/String;

    goto :goto_1

    :cond_4
    const/4 v2, 0x4

    if-ne p4, v2, :cond_5

    const-string/jumbo v2, "retweeted"

    iput-object v2, v0, Lcom/twitter/android/kv;->i:Ljava/lang/String;

    goto :goto_1

    :cond_5
    const/16 v2, 0xf

    if-ne p4, v2, :cond_6

    const-string/jumbo v2, "media_tagged"

    iput-object v2, v0, Lcom/twitter/android/kv;->i:Ljava/lang/String;

    goto :goto_1

    :cond_6
    const-string/jumbo v2, ""

    iput-object v2, v0, Lcom/twitter/android/kv;->i:Ljava/lang/String;

    goto :goto_1
.end method

.method private a(Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/util/FriendshipCache;ZJ)V
    .locals 7

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/twitter/android/kv;->g:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v0}, Lcom/twitter/library/widget/ActionButton;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/kv;->g:Lcom/twitter/library/widget/ActionButton;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/widget/ActivityUserView;->a(Lcom/twitter/library/widget/ActionButton;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/util/FriendshipCache;ZJ)V

    iget-object v0, p0, Lcom/twitter/android/kv;->g:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v0}, Lcom/twitter/library/widget/ActionButton;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/l;

    iput-object p1, v0, Lcom/twitter/android/l;->a:Lcom/twitter/library/api/TwitterUser;

    iget-object v0, p0, Lcom/twitter/android/kv;->c:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/kv;->c:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/kv;->g:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v1}, Lcom/twitter/library/widget/ActionButton;->getWidth()I

    move-result v1

    invoke-virtual {v0, v6, v6, v1, v6}, Landroid/widget/TextView;->setPadding(IIII)V

    goto :goto_0
.end method

.method private a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/android/client/c;Lcom/twitter/library/client/aa;Landroid/content/res/Resources;)V
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x1

    iget-object v0, p0, Lcom/twitter/android/kv;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/kv;->d:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ku;

    iget v1, p0, Lcom/twitter/android/kv;->h:I

    iput v1, v0, Lcom/twitter/android/ku;->c:I

    iput-object p1, v0, Lcom/twitter/android/ku;->d:Lcom/twitter/library/provider/Tweet;

    iput-object p2, v0, Lcom/twitter/android/ku;->e:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {p2}, Lcom/twitter/library/api/TwitterUser;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v2

    iget-object v3, v2, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    invoke-virtual {p3, v3}, Lcom/twitter/android/client/c;->g(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v3

    iget-wide v4, v2, Lcom/twitter/library/api/TwitterUser;->userId:J

    iput-wide v4, p0, Lcom/twitter/android/kv;->l:J

    iget v2, p0, Lcom/twitter/android/kv;->h:I

    sparse-switch v2, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const v2, 0x7f0f04a3    # com.twitter.android.R.string.teachable_reply_to

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v1, v4, v7

    invoke-virtual {p5, v2, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v6, v3}, Lcom/twitter/android/ku;->a(Ljava/lang/String;ZLandroid/graphics/Bitmap;)V

    goto :goto_0

    :sswitch_1
    const v2, 0x7f0f04d9    # com.twitter.android.R.string.tweet_to_joined_twitter

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v1, v3, v7

    invoke-virtual {p5, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v7, v2}, Lcom/twitter/android/ku;->a(Ljava/lang/String;ZLandroid/graphics/Bitmap;)V

    goto :goto_0

    :sswitch_2
    const v2, 0x7f0f04a6    # com.twitter.android.R.string.teachable_say_hello

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v1, v4, v7

    invoke-virtual {p5, v2, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v6, v3}, Lcom/twitter/android/ku;->a(Ljava/lang/String;ZLandroid/graphics/Bitmap;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x3 -> :sswitch_0
        0x5 -> :sswitch_2
        0xd -> :sswitch_1
    .end sparse-switch
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/kv;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/kv;->c:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/kv;->c:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/kv;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Lcom/twitter/android/client/c;Landroid/content/res/Resources;Lcom/twitter/library/util/FriendshipCache;)V
    .locals 7

    const/16 v3, 0x8

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/twitter/android/kv;->k:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {v0}, Lcom/twitter/library/api/TwitterUser;->c()Ljava/lang/String;

    move-result-object v0

    new-array v1, v6, [Lcom/twitter/internal/android/widget/TypefacesSpan;

    new-instance v2, Lcom/twitter/internal/android/widget/TypefacesSpan;

    invoke-direct {v2, p1, v6}, Lcom/twitter/internal/android/widget/TypefacesSpan;-><init>(Landroid/content/Context;I)V

    aput-object v2, v1, v5

    iget-object v2, p0, Lcom/twitter/android/kv;->g:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v2, v3}, Lcom/twitter/library/widget/ActionButton;->setVisibility(I)V

    iget-object v2, p0, Lcom/twitter/android/kv;->d:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Lcom/twitter/android/kv;->c:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/kv;->c:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    iget-object v2, p0, Lcom/twitter/android/kv;->e:Landroid/view/View;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/twitter/android/kv;->e:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    iget-object v2, p0, Lcom/twitter/android/kv;->f:Landroid/view/ViewGroup;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/twitter/android/kv;->f:Landroid/view/ViewGroup;

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_2
    iget v2, p0, Lcom/twitter/android/kv;->h:I

    packed-switch v2, :pswitch_data_0

    :goto_0
    :pswitch_0
    invoke-static {p1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v4

    iget-object v1, p0, Lcom/twitter/android/kv;->j:Lcom/twitter/library/provider/Tweet;

    iget-object v2, p0, Lcom/twitter/android/kv;->k:Lcom/twitter/library/api/TwitterUser;

    move-object v0, p0

    move-object v3, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/kv;->a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/android/client/c;Lcom/twitter/library/client/aa;Landroid/content/res/Resources;)V

    iget-object v1, p0, Lcom/twitter/android/kv;->k:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {v4}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    move-object v0, p0

    move-object v2, p4

    move v3, v6

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/kv;->a(Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/util/FriendshipCache;ZJ)V

    return-void

    :pswitch_1
    iget-object v2, p0, Lcom/twitter/android/kv;->b:Landroid/widget/TextView;

    invoke-static {}, Lcom/twitter/android/kt;->i()[I

    move-result-object v3

    aget v3, v3, v6

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-virtual {p3, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const/16 v3, 0x22

    invoke-static {v1, v0, v3}, Lcom/twitter/library/util/Util;->a([Ljava/lang/Object;Ljava/lang/String;C)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/kv;->g:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v0, v5}, Lcom/twitter/library/widget/ActionButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/kv;->d:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :pswitch_2
    const v1, 0x7f0f04a0    # com.twitter.android.R.string.teachable_followed_you

    new-array v2, v6, [Ljava/lang/Object;

    aput-object v0, v2, v5

    invoke-virtual {p3, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/kv;->k:Lcom/twitter/library/api/TwitterUser;

    iget-object v1, v1, Lcom/twitter/library/api/TwitterUser;->profileDescription:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/kv;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/kv;->g:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v0, v5}, Lcom/twitter/library/widget/ActionButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/kv;->d:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :pswitch_3
    const v1, 0x7f0f04a2    # com.twitter.android.R.string.teachable_mention

    new-array v2, v6, [Ljava/lang/Object;

    aput-object v0, v2, v5

    invoke-virtual {p3, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/kv;->j:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v1}, Lcom/twitter/library/provider/Tweet;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/kv;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/kv;->d:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :pswitch_4
    const v1, 0x7f0f049e    # com.twitter.android.R.string.teachable_favorite

    new-array v2, v6, [Ljava/lang/Object;

    aput-object v0, v2, v5

    invoke-virtual {p3, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/kv;->j:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v1}, Lcom/twitter/library/provider/Tweet;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/kv;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/kv;->e:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    :pswitch_5
    const v1, 0x7f0f04a4    # com.twitter.android.R.string.teachable_retweet

    new-array v2, v6, [Ljava/lang/Object;

    aput-object v0, v2, v5

    invoke-virtual {p3, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/kv;->j:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v1}, Lcom/twitter/library/provider/Tweet;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/kv;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/kv;->f:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto/16 :goto_0

    :pswitch_6
    const v1, 0x7f0f04a1    # com.twitter.android.R.string.teachable_media_tag

    new-array v2, v6, [Ljava/lang/Object;

    aput-object v0, v2, v5

    invoke-virtual {p3, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/kv;->j:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v1}, Lcom/twitter/library/provider/Tweet;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/kv;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/kv;->d:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_5
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_6
    .end packed-switch
.end method
