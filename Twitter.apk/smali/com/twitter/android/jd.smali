.class Lcom/twitter/android/jd;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/twitter/android/client/c;

.field final synthetic b:Lcom/twitter/android/LoginActivity;

.field private c:Z

.field private d:Z


# direct methods
.method constructor <init>(Lcom/twitter/android/LoginActivity;Lcom/twitter/android/client/c;)V
    .locals 2

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/twitter/android/jd;->b:Lcom/twitter/android/LoginActivity;

    iput-object p2, p0, Lcom/twitter/android/jd;->a:Lcom/twitter/android/client/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/twitter/android/jd;->c:Z

    iget-object v1, p0, Lcom/twitter/android/jd;->b:Lcom/twitter/android/LoginActivity;

    invoke-static {v1}, Lcom/twitter/android/LoginActivity;->a(Lcom/twitter/android/LoginActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lcom/twitter/android/jd;->d:Z

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 7

    const/4 v6, 0x1

    iget-boolean v0, p0, Lcom/twitter/android/jd;->d:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/jd;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/jd;->a:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/jd;->b:Lcom/twitter/android/LoginActivity;

    invoke-static {v1}, Lcom/twitter/android/LoginActivity;->b(Lcom/twitter/android/LoginActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v6, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "login:::username:edit"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iput-boolean v6, p0, Lcom/twitter/android/jd;->c:Z

    :cond_0
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method
