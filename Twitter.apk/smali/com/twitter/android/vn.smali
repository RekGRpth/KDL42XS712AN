.class public Lcom/twitter/android/vn;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/vu;


# instance fields
.field protected final a:Lcom/twitter/android/client/c;

.field protected final b:Lcom/twitter/library/client/aa;

.field protected final c:Ljava/lang/ref/WeakReference;

.field protected final d:Lcom/twitter/library/scribe/ScribeAssociation;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/Fragment;Lcom/twitter/library/scribe/ScribeAssociation;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/vn;->a:Lcom/twitter/android/client/c;

    invoke-static {v0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/vn;->b:Lcom/twitter/library/client/aa;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/vn;->c:Ljava/lang/ref/WeakReference;

    iput-object p2, p0, Lcom/twitter/android/vn;->d:Lcom/twitter/library/scribe/ScribeAssociation;

    return-void
.end method

.method private a(Landroid/app/Activity;)Ljava/lang/String;
    .locals 1

    instance-of v0, p1, Lcom/twitter/android/TweetActivity;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "non_focused_tweet"

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/vn;Landroid/app/Activity;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1}, Lcom/twitter/android/vn;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/twitter/library/view/TweetActionType;Lcom/twitter/library/provider/Tweet;Landroid/support/v4/app/Fragment;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;Lcom/twitter/library/util/FriendshipCache;Lcom/twitter/library/scribe/ScribeItem;)V
    .locals 6

    sget-object v0, Lcom/twitter/library/view/TweetActionType;->b:Lcom/twitter/library/view/TweetActionType;

    if-ne p1, v0, :cond_1

    invoke-virtual {p0, p2, p4, p5, p7}, Lcom/twitter/android/vn;->a(Lcom/twitter/library/provider/Tweet;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;Lcom/twitter/library/scribe/ScribeItem;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/twitter/library/view/TweetActionType;->c:Lcom/twitter/library/view/TweetActionType;

    if-ne p1, v0, :cond_2

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p7

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/vn;->a(Lcom/twitter/library/provider/Tweet;Landroid/support/v4/app/Fragment;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;Lcom/twitter/library/scribe/ScribeItem;)V

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/twitter/library/view/TweetActionType;->d:Lcom/twitter/library/view/TweetActionType;

    if-ne p1, v0, :cond_3

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p7

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/vn;->b(Lcom/twitter/library/provider/Tweet;Landroid/support/v4/app/Fragment;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;Lcom/twitter/library/scribe/ScribeItem;)V

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/twitter/library/view/TweetActionType;->e:Lcom/twitter/library/view/TweetActionType;

    if-ne p1, v0, :cond_4

    invoke-virtual {p0, p2, p6, p7}, Lcom/twitter/android/vn;->a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/util/FriendshipCache;Lcom/twitter/library/scribe/ScribeItem;)V

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/twitter/library/view/TweetActionType;->f:Lcom/twitter/library/view/TweetActionType;

    if-ne p1, v0, :cond_5

    invoke-virtual {p0, p2, p3, p4, p5}, Lcom/twitter/android/vn;->b(Lcom/twitter/library/provider/Tweet;Landroid/support/v4/app/Fragment;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;)V

    goto :goto_0

    :cond_5
    sget-object v0, Lcom/twitter/library/view/TweetActionType;->g:Lcom/twitter/library/view/TweetActionType;

    if-ne p1, v0, :cond_6

    invoke-virtual {p0, p2, p3, p4, p5}, Lcom/twitter/android/vn;->c(Lcom/twitter/library/provider/Tweet;Landroid/support/v4/app/Fragment;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;)V

    goto :goto_0

    :cond_6
    sget-object v0, Lcom/twitter/library/view/TweetActionType;->h:Lcom/twitter/library/view/TweetActionType;

    if-ne p1, v0, :cond_0

    invoke-virtual {p0, p2, p3, p4, p5}, Lcom/twitter/android/vn;->a(Lcom/twitter/library/provider/Tweet;Landroid/support/v4/app/Fragment;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;)V

    goto :goto_0
.end method


# virtual methods
.method protected a(Lcom/twitter/library/provider/Tweet;)Ljava/lang/String;
    .locals 1

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->K()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "focal"

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->J()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "ancestor"

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/vn;->a:Lcom/twitter/android/client/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/vn;->a:Lcom/twitter/android/client/c;

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/vn;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/twitter/android/util/u;->a(Landroid/content/Context;)Lcom/twitter/android/util/u;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/twitter/android/util/u;->a(I)I

    :cond_0
    return-void
.end method

.method public a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    new-instance v0, Lcom/twitter/android/vr;

    invoke-direct {v0, p0, p2, p1}, Lcom/twitter/android/vr;-><init>(Lcom/twitter/android/vn;Ljava/lang/String;Landroid/content/Context;)V

    iget-object v1, p0, Lcom/twitter/android/vn;->a:Lcom/twitter/android/client/c;

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/j;)V

    return-void
.end method

.method protected a(Landroid/support/v4/app/FragmentActivity;Landroid/content/DialogInterface$OnClickListener;)V
    .locals 3

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0f0032    # com.twitter.android.R.string.app_name

    invoke-virtual {p1, v1}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f04e2    # com.twitter.android.R.string.tweets_dismiss_question

    invoke-virtual {p1, v1}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f04e1    # com.twitter.android.R.string.tweets_dismiss_positive

    invoke-virtual {p1, v1}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f0089    # com.twitter.android.R.string.cancel

    invoke-virtual {p1, v1}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method protected a(Lcom/twitter/library/provider/Tweet;Landroid/support/v4/app/Fragment;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;)V
    .locals 1

    new-instance v0, Lcom/twitter/android/vq;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/vq;-><init>(Lcom/twitter/android/vn;Lcom/twitter/library/provider/Tweet;)V

    invoke-virtual {p0, p3, v0}, Lcom/twitter/android/vn;->a(Landroid/support/v4/app/FragmentActivity;Landroid/content/DialogInterface$OnClickListener;)V

    return-void
.end method

.method protected a(Lcom/twitter/library/provider/Tweet;Landroid/support/v4/app/Fragment;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;Lcom/twitter/library/scribe/ScribeItem;)V
    .locals 6

    const/4 v0, 0x0

    new-instance v4, Lcom/twitter/android/vo;

    invoke-direct {v4, p0, p3, p1, p5}, Lcom/twitter/android/vo;-><init>(Lcom/twitter/android/vn;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeItem;)V

    move-object v1, p1

    move v2, v0

    move-object v3, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/rs;->a(ILcom/twitter/library/provider/Tweet;ZLandroid/support/v4/app/Fragment;Lcom/twitter/android/ru;Landroid/support/v4/app/FragmentActivity;)V

    return-void
.end method

.method protected a(Lcom/twitter/library/provider/Tweet;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;Lcom/twitter/library/scribe/ScribeItem;)V
    .locals 7

    iget-boolean v0, p1, Lcom/twitter/library/provider/Tweet;->l:Z

    if-nez v0, :cond_0

    const v0, 0x7f0f012b    # com.twitter.android.R.string.dt2f_dialog_text

    invoke-static {p2, v0}, Ljy;->a(Landroid/content/Context;I)V

    iget-object v0, p0, Lcom/twitter/android/vn;->a:Lcom/twitter/android/client/c;

    iget-wide v2, p1, Lcom/twitter/library/provider/Tweet;->o:J

    iget-wide v4, p1, Lcom/twitter/library/provider/Tweet;->C:J

    iget-object v6, p1, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    move-object v1, p3

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;JJLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, p1, Lcom/twitter/library/provider/Tweet;->l:Z

    iget v1, p1, Lcom/twitter/library/provider/Tweet;->ae:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p1, Lcom/twitter/library/provider/Tweet;->ae:I

    invoke-virtual {p2}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/vn;->a(Landroid/content/Context;Ljava/lang/String;)V

    invoke-direct {p0, p2}, Lcom/twitter/android/vn;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "favorite"

    invoke-virtual {p0, v0, v1, p1, p4}, Lcom/twitter/android/vn;->a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeItem;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/vn;->a:Lcom/twitter/android/client/c;

    iget-wide v1, p1, Lcom/twitter/library/provider/Tweet;->o:J

    iget-object v3, p1, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {v0, p3, v1, v2, v3}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/Session;JLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    iput-boolean v1, p1, Lcom/twitter/library/provider/Tweet;->l:Z

    iget v1, p1, Lcom/twitter/library/provider/Tweet;->ae:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p1, Lcom/twitter/library/provider/Tweet;->ae:I

    invoke-virtual {p2}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/vn;->a(Landroid/content/Context;Ljava/lang/String;)V

    invoke-direct {p0, p2}, Lcom/twitter/android/vn;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "unfavorite"

    invoke-virtual {p0, v0, v1, p1, p4}, Lcom/twitter/android/vn;->a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeItem;)V

    goto :goto_0
.end method

.method protected a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/util/FriendshipCache;Lcom/twitter/library/scribe/ScribeItem;)V
    .locals 5

    const/4 v0, 0x0

    if-eqz p2, :cond_1

    iget-wide v1, p1, Lcom/twitter/library/provider/Tweet;->n:J

    invoke-virtual {p2, v1, v2}, Lcom/twitter/library/util/FriendshipCache;->i(J)Z

    move-result v1

    if-nez v1, :cond_2

    const-string/jumbo v1, "follow"

    invoke-virtual {p0, v1, p1, p3}, Lcom/twitter/android/vn;->a(Ljava/lang/String;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeItem;)V

    iget-object v1, p0, Lcom/twitter/android/vn;->a:Lcom/twitter/android/client/c;

    iget-wide v2, p1, Lcom/twitter/library/provider/Tweet;->n:J

    iget-object v4, p1, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {v1, v2, v3, v0, v4}, Lcom/twitter/android/client/c;->a(JZLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    iget-wide v1, p1, Lcom/twitter/library/provider/Tweet;->n:J

    invoke-virtual {p2, v1, v2}, Lcom/twitter/library/util/FriendshipCache;->a(J)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-wide v0, p1, Lcom/twitter/library/provider/Tweet;->n:J

    invoke-virtual {p2, v0, v1}, Lcom/twitter/library/util/FriendshipCache;->h(J)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :cond_0
    or-int/lit8 v0, v0, 0x1

    or-int/lit8 v0, v0, 0x40

    iget-wide v1, p1, Lcom/twitter/library/provider/Tweet;->n:J

    invoke-virtual {p2, v1, v2, v0}, Lcom/twitter/library/util/FriendshipCache;->b(JI)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string/jumbo v0, "unfollow"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lcom/twitter/android/vn;->a(Ljava/lang/String;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeItem;)V

    iget-object v0, p0, Lcom/twitter/android/vn;->a:Lcom/twitter/android/client/c;

    iget-wide v1, p1, Lcom/twitter/library/provider/Tweet;->n:J

    iget-object v3, p1, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(JLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    iget-wide v0, p1, Lcom/twitter/library/provider/Tweet;->n:J

    invoke-virtual {p2, v0, v1}, Lcom/twitter/library/util/FriendshipCache;->e(J)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/view/TweetActionType;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/util/FriendshipCache;Lcom/twitter/library/scribe/ScribeItem;)V
    .locals 8

    iget-object v0, p0, Lcom/twitter/android/vn;->a:Lcom/twitter/android/client/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/vn;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/support/v4/app/Fragment;

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v0, p0, Lcom/twitter/android/vn;->b:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v5

    invoke-virtual {v5}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/vn;->a(Lcom/twitter/library/view/TweetActionType;Lcom/twitter/library/provider/Tweet;Landroid/support/v4/app/Fragment;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;Lcom/twitter/library/util/FriendshipCache;Lcom/twitter/library/scribe/ScribeItem;)V

    :cond_0
    return-void
.end method

.method protected a(Ljava/lang/String;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeItem;)V
    .locals 1

    const-string/jumbo v0, ""

    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/twitter/android/vn;->a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeItem;)V

    return-void
.end method

.method protected a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeItem;)V
    .locals 6

    invoke-static {p3}, Lcom/twitter/library/provider/Tweet;->b(Lcom/twitter/library/provider/Tweet;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/vn;->a:Lcom/twitter/android/client/c;

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v3, p0, Lcom/twitter/android/vn;->b:Lcom/twitter/library/client/aa;

    invoke-virtual {v3}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/twitter/android/vn;->d:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-static {v5, v0, p1, p2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/vn;->d:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {p0, p3}, Lcom/twitter/android/vn;->a(Lcom/twitter/library/provider/Tweet;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, p3, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/vn;->d:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method

.method public a(Lcom/twitter/library/view/TweetActionType;Lcom/twitter/library/provider/Tweet;)Z
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, v0}, Lcom/twitter/android/vn;->a(Lcom/twitter/library/view/TweetActionType;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/util/FriendshipCache;Lcom/twitter/library/scribe/ScribeItem;)V

    const/4 v0, 0x1

    return v0
.end method

.method public b()V
    .locals 0

    return-void
.end method

.method protected b(Lcom/twitter/library/provider/Tweet;Landroid/support/v4/app/Fragment;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;)V
    .locals 3

    const/4 v2, 0x0

    invoke-static {v2}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f04df    # com.twitter.android.R.string.tweets_delete_status

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f04de    # com.twitter.android.R.string.tweets_delete_question

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->e(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0571    # com.twitter.android.R.string.yes

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->g(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f029d    # com.twitter.android.R.string.no

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->i(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {v0, p2, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p3}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    new-instance v1, Lcom/twitter/android/vp;

    invoke-direct {v1, p0, p4, p1, p3}, Lcom/twitter/android/vp;-><init>(Lcom/twitter/android/vn;Lcom/twitter/library/client/Session;Lcom/twitter/library/provider/Tweet;Landroid/support/v4/app/FragmentActivity;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Lcom/twitter/android/widget/ce;)Lcom/twitter/android/widget/PromptDialogFragment;

    return-void
.end method

.method protected b(Lcom/twitter/library/provider/Tweet;Landroid/support/v4/app/Fragment;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;Lcom/twitter/library/scribe/ScribeItem;)V
    .locals 3

    const/4 v1, 0x1

    const-string/jumbo v0, "reply"

    invoke-virtual {p0, v0, p1, p5}, Lcom/twitter/android/vn;->a(Ljava/lang/String;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeItem;)V

    invoke-static {}, Lkn;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lkn;->c()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/TweetActivity;

    invoke-direct {v0, p3, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "tw"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "reply"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "association"

    iget-object v2, p0, Lcom/twitter/android/vn;->d:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;->c:Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    invoke-static {p3, v0}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Landroid/content/Context;Lcom/twitter/android/composer/ComposerIntentWrapper$Action;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Lcom/twitter/library/provider/ParcelableTweet;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v0

    invoke-virtual {p4}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Ljava/lang/String;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/twitter/android/composer/ComposerIntentWrapper;->b(Landroid/content/Context;)V

    goto :goto_1
.end method

.method protected c(Lcom/twitter/library/provider/Tweet;Landroid/support/v4/app/Fragment;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/client/Session;)V
    .locals 8

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->j()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcom/twitter/library/provider/Tweet;->p:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/provider/Tweet;->d:Ljava/lang/String;

    iget-wide v4, p1, Lcom/twitter/library/provider/Tweet;->h:J

    iget-wide v6, p1, Lcom/twitter/library/provider/Tweet;->o:J

    move-object v0, p3

    invoke-static/range {v0 .. v7}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V

    invoke-direct {p0, p3}, Lcom/twitter/android/vn;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "share"

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/twitter/android/vn;->a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeItem;)V

    return-void
.end method
