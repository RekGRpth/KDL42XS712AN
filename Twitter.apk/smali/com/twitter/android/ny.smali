.class public Lcom/twitter/android/ny;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:Landroid/support/v4/app/FragmentManager;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private f:Lcom/twitter/android/widget/NotificationSettingsDialogFragment;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/ny;->a:Landroid/support/v4/app/FragmentManager;

    iput-object p2, p0, Lcom/twitter/android/ny;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/twitter/android/ny;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/twitter/android/ny;->d:Ljava/lang/String;

    iput-object p5, p0, Lcom/twitter/android/ny;->e:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/ny;->f:Lcom/twitter/android/widget/NotificationSettingsDialogFragment;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ny;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/ny;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/ny;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/ny;->e:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/android/widget/NotificationSettingsDialogFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ny;->f:Lcom/twitter/android/widget/NotificationSettingsDialogFragment;

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ny;->f:Lcom/twitter/android/widget/NotificationSettingsDialogFragment;

    iget-object v1, p0, Lcom/twitter/android/ny;->a:Landroid/support/v4/app/FragmentManager;

    const-string/jumbo v2, "notification_setting"

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    return-void
.end method
