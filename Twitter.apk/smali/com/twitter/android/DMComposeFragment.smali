.class public Lcom/twitter/android/DMComposeFragment;
.super Lcom/twitter/android/UserSelectFragment;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/util/ar;


# instance fields
.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/UserSelectFragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/DMComposeFragment;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/DMComposeFragment;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/DMComposeFragment;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/api/conversations/t;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/DMComposeFragment;->a(Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/api/conversations/t;)V

    return-void
.end method

.method private a(Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/api/conversations/t;)V
    .locals 4

    if-eqz p2, :cond_0

    iget-boolean v0, p2, Lcom/twitter/library/api/conversations/t;->a:Z

    if-eqz v0, :cond_1

    iget-wide v0, p1, Lcom/twitter/library/api/TwitterUser;->userId:J

    iget-object v2, p1, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/twitter/android/DMComposeFragment;->a(JLjava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/twitter/android/DMComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f0f010c    # com.twitter.android.R.string.direct_message_error_title

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f011b    # com.twitter.android.R.string.dm_error_not_follower

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f02d5    # com.twitter.android.R.string.ok

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/DMComposeFragment;->c:Lcom/twitter/android/zm;

    check-cast v0, Lcom/twitter/android/eh;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/eh;->a(J)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Liu;

    invoke-virtual {p0}, Lcom/twitter/android/DMComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/DMComposeFragment;->c()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, Liu;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/util/List;)V

    iget-object v0, v1, Liu;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/android/DMComposeFragment;->d:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/DMComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/client/w;->a(Landroid/content/Context;)Lcom/twitter/library/client/w;

    move-result-object v0

    new-instance v2, Lcom/twitter/android/dh;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/twitter/android/dh;-><init>(Lcom/twitter/android/DMComposeFragment;Lcom/twitter/android/dg;)V

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/w;->a(Lcom/twitter/library/service/b;Lcom/twitter/library/service/a;)Ljava/lang/String;

    return-void
.end method

.method private h()V
    .locals 1

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/twitter/android/DMComposeFragment;->d:Ljava/lang/String;

    return-void
.end method

.method private i()Landroid/os/Bundle;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/DMComposeFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p0, v0}, Lcom/twitter/android/DMComposeFragment;->setArguments(Landroid/os/Bundle;)V

    :cond_0
    return-object v0
.end method


# virtual methods
.method protected a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    const v0, 0x7f03004d    # com.twitter.android.R.layout.dm_compose_fragment

    invoke-super {p0, p1, v0, p2}, Lcom/twitter/android/UserSelectFragment;->a(Landroid/view/LayoutInflater;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected a(JLjava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/DMComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/api/conversations/ae;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0, p1, p2, p3, p4}, Lcom/twitter/android/UserSelectFragment;->a(JLjava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/DMComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/DMConversationActivity;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/DMConversationActivity;->a(J)V

    goto :goto_0
.end method

.method public a(Landroid/content/Intent;)V
    .locals 2

    invoke-direct {p0}, Lcom/twitter/android/DMComposeFragment;->i()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/CharSequence;I)V
    .locals 6

    invoke-virtual {p0, p2}, Lcom/twitter/android/DMComposeFragment;->b(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/DMComposeFragment;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/DMComposeFragment;->c:Lcom/twitter/android/zm;

    invoke-virtual {v1, v0}, Lcom/twitter/android/zm;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    const/4 v3, 0x3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-wide/16 v4, 0x0

    cmp-long v4, v1, v4

    if-nez v4, :cond_1

    invoke-direct {p0, v0}, Lcom/twitter/android/DMComposeFragment;->a(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, v1, v2, v3, v0}, Lcom/twitter/android/DMComposeFragment;->a(JLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/DMComposeFragment;->a:Lcom/twitter/android/UserSelectFragment$UserSelectEditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/DMComposeFragment;->a:Lcom/twitter/android/UserSelectFragment$UserSelectEditText;

    invoke-virtual {v0}, Lcom/twitter/android/UserSelectFragment$UserSelectEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 9

    invoke-super {p0, p1}, Lcom/twitter/android/UserSelectFragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/DMComposeFragment;->b()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p0}, Lcom/twitter/android/DMComposeFragment;->c()Lcom/twitter/library/client/Session;

    move-result-object v3

    new-instance v4, Lcom/twitter/library/client/f;

    invoke-virtual {p0}, Lcom/twitter/android/DMComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "dm"

    invoke-direct {v4, v5, v6, v7}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v5, "followers_timestamp"

    const-wide/16 v6, 0x0

    invoke-virtual {v4, v5, v6, v7}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;J)J

    move-result-wide v5

    const-wide/32 v7, 0x5265c00

    add-long/2addr v5, v7

    cmp-long v5, v5, v1

    if-gez v5, :cond_0

    invoke-virtual {v4}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v4

    const-string/jumbo v5, "followers_timestamp"

    invoke-virtual {v4, v5, v1, v2}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;J)Lcom/twitter/library/client/f;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/f;->d()V

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/16 v4, 0x190

    invoke-virtual {v0, v3, v1, v2, v4}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;III)Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    invoke-super {p0, p1}, Lcom/twitter/android/UserSelectFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/DMComposeFragment;->b()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/DMComposeFragment;->c()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "messages:compose:::impression"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    new-instance v0, Lcom/twitter/android/eh;

    invoke-virtual {p0}, Lcom/twitter/android/DMComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/android/eh;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/twitter/android/DMComposeFragment;->c:Lcom/twitter/android/zm;

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/DMComposeFragment;->h()V

    return-void
.end method
