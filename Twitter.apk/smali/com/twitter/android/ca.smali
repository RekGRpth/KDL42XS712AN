.class Lcom/twitter/android/ca;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/internal/network/i;


# instance fields
.field public a:Lcom/twitter/library/card/instance/CardInstanceData;

.field private final b:Ljava/lang/ref/WeakReference;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/twitter/android/CardPreviewerFragment;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/ca;->b:Ljava/lang/ref/WeakReference;

    iput-object p2, p0, Lcom/twitter/android/ca;->c:Ljava/lang/String;

    return-void
.end method

.method private a(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/android/cd;
    .locals 4

    const/4 v0, 0x0

    new-instance v2, Lcom/twitter/android/cd;

    invoke-direct {v2, v0}, Lcom/twitter/android/cd;-><init>(Lcom/twitter/android/bw;)V

    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_3

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_3

    sget-object v3, Lcom/twitter/android/by;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_0
    const-string/jumbo v1, "code"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0, p1}, Lcom/twitter/android/ca;->b(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/android/cc;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/android/cd;->a:Lcom/twitter/android/cc;

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_1
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v3, "tag_name"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    iput-object v1, v2, Lcom/twitter/android/cd;->b:Ljava/lang/String;

    goto :goto_1

    :cond_2
    const-string/jumbo v3, "tag_value"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    iput-object v1, v2, Lcom/twitter/android/cd;->c:Ljava/lang/String;

    goto :goto_1

    :pswitch_3
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    return-object v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic a(Lcom/twitter/android/ca;)Ljava/lang/ref/WeakReference;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ca;->b:Ljava/lang/ref/WeakReference;

    return-object v0
.end method

.method private a(Ljava/io/InputStream;)V
    .locals 7

    const/4 v1, 0x0

    invoke-static {p1}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v4

    invoke-virtual {v4}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v2, v1

    move-object v3, v1

    :goto_0
    if-eqz v0, :cond_3

    sget-object v5, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v5, :cond_3

    sget-object v5, Lcom/twitter/android/by;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v5, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    move-object v0, v1

    move-object v1, v2

    move-object v2, v3

    :goto_1
    invoke-virtual {v4}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v3

    move-object v6, v0

    move-object v0, v3

    move-object v3, v2

    move-object v2, v1

    move-object v1, v6

    goto :goto_0

    :pswitch_0
    const-string/jumbo v0, "card"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/twitter/android/cb;

    invoke-direct {v0, p0}, Lcom/twitter/android/cb;-><init>(Lcom/twitter/android/ca;)V

    invoke-static {v4, v0}, Lcom/twitter/library/card/instance/a;->a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/card/instance/CardInstanceData;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ca;->a:Lcom/twitter/library/card/instance/CardInstanceData;

    move-object v0, v1

    move-object v1, v2

    move-object v2, v3

    goto :goto_1

    :cond_1
    const-string/jumbo v0, "info"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0, v4}, Lcom/twitter/android/ca;->a(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/android/cd;

    move-result-object v0

    move-object v1, v2

    move-object v2, v3

    goto :goto_1

    :cond_2
    invoke-virtual {v4}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v0, v1

    move-object v1, v2

    move-object v2, v3

    goto :goto_1

    :pswitch_1
    invoke-virtual {v4}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v0, v1

    move-object v1, v2

    move-object v2, v3

    goto :goto_1

    :pswitch_2
    const-string/jumbo v0, "error"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v4}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    move-object v2, v3

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    goto :goto_1

    :pswitch_3
    invoke-virtual {v4}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    move-object v6, v1

    move-object v1, v2

    move-object v2, v0

    move-object v0, v6

    goto :goto_1

    :cond_3
    invoke-static {v4}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/twitter/android/ca;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/CardPreviewerFragment;

    if-eqz v0, :cond_6

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz v1, :cond_5

    const-string/jumbo v2, "\n"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v1, Lcom/twitter/android/cd;->a:Lcom/twitter/android/cc;

    if-eqz v2, :cond_4

    const-string/jumbo v2, "Code: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v1, Lcom/twitter/android/cd;->a:Lcom/twitter/android/cc;

    iget-object v2, v2, Lcom/twitter/android/cc;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    const-string/jumbo v2, "?"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_2
    const-string/jumbo v2, " ("

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v1, Lcom/twitter/android/cd;->a:Lcom/twitter/android/cc;

    iget v2, v2, Lcom/twitter/android/cc;->a:I

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string/jumbo v2, ")\n"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    const-string/jumbo v2, "Tag: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v1, Lcom/twitter/android/cd;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    const-string/jumbo v2, "?"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_3
    const-string/jumbo v2, " \u2192 "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v1, Lcom/twitter/android/cd;->c:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_9

    const-string/jumbo v1, "?"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    :goto_4
    iget-object v1, p0, Lcom/twitter/android/ca;->c:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/twitter/android/CardPreviewerFragment;->a(Lcom/twitter/android/CardPreviewerFragment;Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    return-void

    :cond_7
    iget-object v2, v1, Lcom/twitter/android/cd;->a:Lcom/twitter/android/cc;

    iget-object v2, v2, Lcom/twitter/android/cc;->b:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_8
    iget-object v2, v1, Lcom/twitter/android/cd;->b:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    :cond_9
    iget-object v1, v1, Lcom/twitter/android/cd;->c:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private b(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/android/cc;
    .locals 4

    const/4 v0, 0x0

    new-instance v2, Lcom/twitter/android/cc;

    invoke-direct {v2, v0}, Lcom/twitter/android/cc;-><init>(Lcom/twitter/android/bw;)V

    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_1

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_1

    sget-object v3, Lcom/twitter/android/by;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_0
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_1
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v3, "name"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    iput-object v1, v2, Lcom/twitter/android/cc;->b:Ljava/lang/String;

    goto :goto_1

    :pswitch_2
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v1

    const-string/jumbo v3, "value"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    iput v1, v2, Lcom/twitter/android/cc;->a:I

    goto :goto_1

    :pswitch_3
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    return-object v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public a(ILjava/io/InputStream;ILjava/lang/String;Ljava/lang/String;)V
    .locals 4

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/ca;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/CardPreviewerFragment;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/twitter/android/CardPreviewerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    :goto_0
    const/16 v3, 0xc8

    if-ne p1, v3, :cond_3

    const-string/jumbo v1, "application/json"

    invoke-virtual {p4, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "Expecting JSON content in response"

    invoke-static {v1, v2}, Lcom/twitter/library/card/CardDebugLog;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :try_start_0
    invoke-direct {p0, p2}, Lcom/twitter/android/ca;->a(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    return-void

    :cond_2
    move-object v1, v2

    goto :goto_0

    :catch_0
    move-exception v1

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/twitter/android/ca;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v2, v1}, Lcom/twitter/android/CardPreviewerFragment;->a(Lcom/twitter/android/CardPreviewerFragment;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    const/16 v2, 0x193

    if-ne p1, v2, :cond_1

    if-eqz v1, :cond_1

    const v2, 0x7f0f00fa    # com.twitter.android.R.string.developer_card_previewer_error_forbidden

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/CardPreviewerFragment;->b(Lcom/twitter/android/CardPreviewerFragment;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public a(Lcom/twitter/internal/network/k;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/ca;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/CardPreviewerFragment;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/ca;->c:Ljava/lang/String;

    invoke-static {v0, v1, p1}, Lcom/twitter/android/CardPreviewerFragment;->a(Lcom/twitter/android/CardPreviewerFragment;Ljava/lang/String;Lcom/twitter/internal/network/k;)V

    :cond_0
    return-void
.end method
