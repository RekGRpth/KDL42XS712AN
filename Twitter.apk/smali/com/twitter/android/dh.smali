.class Lcom/twitter/android/dh;
.super Lcom/twitter/library/service/a;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/DMComposeFragment;


# direct methods
.method private constructor <init>(Lcom/twitter/android/DMComposeFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/dh;->a:Lcom/twitter/android/DMComposeFragment;

    invoke-direct {p0}, Lcom/twitter/library/service/a;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/DMComposeFragment;Lcom/twitter/android/dg;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/dh;-><init>(Lcom/twitter/android/DMComposeFragment;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/internal/android/service/a;)V
    .locals 0

    check-cast p1, Lcom/twitter/library/service/b;

    invoke-virtual {p0, p1}, Lcom/twitter/android/dh;->a(Lcom/twitter/library/service/b;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/b;)V
    .locals 5

    const/4 v4, 0x0

    check-cast p1, Liu;

    invoke-virtual {p1}, Liu;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    iget-object v1, p1, Liu;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/dh;->a:Lcom/twitter/android/DMComposeFragment;

    invoke-static {v2}, Lcom/twitter/android/DMComposeFragment;->a(Lcom/twitter/android/DMComposeFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Liu;->e()Lcom/twitter/library/api/conversations/v;

    move-result-object v0

    iget-object v2, v0, Lcom/twitter/library/api/conversations/v;->b:Ljava/util/Map;

    invoke-virtual {p1}, Liu;->e()Lcom/twitter/library/api/conversations/v;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/library/api/conversations/v;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/dh;->a:Lcom/twitter/android/DMComposeFragment;

    invoke-virtual {v1}, Lcom/twitter/android/DMComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v3, 0x7f0f011a    # com.twitter.android.R.string.dm_error_non_existing

    invoke-static {v1, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterUser;

    iget-object v1, v0, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/conversations/t;

    iget-object v4, p0, Lcom/twitter/android/dh;->a:Lcom/twitter/android/DMComposeFragment;

    invoke-static {v4, v0, v1}, Lcom/twitter/android/DMComposeFragment;->a(Lcom/twitter/android/DMComposeFragment;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/api/conversations/t;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/dh;->a:Lcom/twitter/android/DMComposeFragment;

    invoke-virtual {v0}, Lcom/twitter/android/DMComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0f00ee    # com.twitter.android.R.string.default_error_message

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/dh;->a:Lcom/twitter/android/DMComposeFragment;

    iget-object v0, v0, Lcom/twitter/android/DMComposeFragment;->c:Lcom/twitter/android/zm;

    check-cast v0, Lcom/twitter/android/eh;

    invoke-virtual {v0}, Lcom/twitter/android/eh;->a()V

    return-void
.end method
