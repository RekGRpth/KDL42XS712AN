.class Lcom/twitter/android/od;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/twitter/android/OneFactorLoginActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/OneFactorLoginActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/od;->a:Lcom/twitter/android/OneFactorLoginActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/od;->a:Lcom/twitter/android/OneFactorLoginActivity;

    invoke-static {v0}, Lcom/twitter/android/OneFactorLoginActivity;->b(Lcom/twitter/android/OneFactorLoginActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/od;->a:Lcom/twitter/android/OneFactorLoginActivity;

    invoke-static {v1}, Lcom/twitter/android/OneFactorLoginActivity;->a(Lcom/twitter/android/OneFactorLoginActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "login::1fa:intercept:timeout"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/od;->a:Lcom/twitter/android/OneFactorLoginActivity;

    sget-object v1, Lcom/twitter/android/OneFactorLoginActivity$LoginState;->d:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    invoke-static {v0, v1}, Lcom/twitter/android/OneFactorLoginActivity;->a(Lcom/twitter/android/OneFactorLoginActivity;Lcom/twitter/android/OneFactorLoginActivity$LoginState;)V

    return-void
.end method
