.class Lcom/twitter/android/ra;
.super Lcom/twitter/library/client/z;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/ProfileFragment;


# direct methods
.method public constructor <init>(Lcom/twitter/android/ProfileFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/ra;->a:Lcom/twitter/android/ProfileFragment;

    invoke-direct {p0}, Lcom/twitter/library/client/z;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ra;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/StartActivity;->a(Landroid/app/Activity;Landroid/content/Intent;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/ra;->a:Lcom/twitter/android/ProfileFragment;

    iget-boolean v1, v1, Lcom/twitter/android/ProfileFragment;->D:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/ra;->a:Lcom/twitter/android/ProfileFragment;

    iput v2, v1, Lcom/twitter/android/ProfileFragment;->F:I

    iget-object v1, p0, Lcom/twitter/android/ra;->a:Lcom/twitter/android/ProfileFragment;

    iput v2, v1, Lcom/twitter/android/ProfileFragment;->E:I

    iget-object v1, p0, Lcom/twitter/android/ra;->a:Lcom/twitter/android/ProfileFragment;

    const/16 v2, 0x7f

    iput v2, v1, Lcom/twitter/android/ProfileFragment;->G:I

    iget-object v1, p0, Lcom/twitter/android/ra;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v1, v0}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/library/api/TwitterUser;)V

    goto :goto_0
.end method
