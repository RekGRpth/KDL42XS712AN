.class public final enum Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;
.super Ljava/lang/Enum;
.source "Twttr"


# static fields
.field public static final enum a:Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;

.field public static final enum b:Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;

.field private static final synthetic c:[Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;

    const-string/jumbo v1, "FAILED"

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;->a:Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;

    new-instance v0, Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;

    const-string/jumbo v1, "CANCELED"

    invoke-direct {v0, v1, v3}, Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;->b:Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;

    sget-object v1, Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;->a:Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;

    aput-object v1, v0, v2

    sget-object v1, Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;->b:Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;

    aput-object v1, v0, v3

    sput-object v0, Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;->c:[Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;
    .locals 1

    const-class v0, Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;

    return-object v0
.end method

.method public static values()[Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;
    .locals 1

    sget-object v0, Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;->c:[Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;

    invoke-virtual {v0}, [Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;

    return-object v0
.end method
