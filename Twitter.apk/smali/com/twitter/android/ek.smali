.class Lcom/twitter/android/ek;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/widget/a;


# instance fields
.field final synthetic a:Lcom/twitter/android/DMRequestInboxFragment;


# direct methods
.method private constructor <init>(Lcom/twitter/android/DMRequestInboxFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/ek;->a:Lcom/twitter/android/DMRequestInboxFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/DMRequestInboxFragment;Lcom/twitter/android/ej;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/ek;-><init>(Lcom/twitter/android/DMRequestInboxFragment;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic onClick(Lcom/twitter/library/widget/BaseUserView;JI)V
    .locals 0

    check-cast p1, Lcom/twitter/library/widget/UserApprovalView;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/twitter/android/ek;->onClick(Lcom/twitter/library/widget/UserApprovalView;JI)V

    return-void
.end method

.method public onClick(Lcom/twitter/library/widget/UserApprovalView;JI)V
    .locals 9

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-virtual {p1, v6}, Lcom/twitter/library/widget/UserApprovalView;->a(I)Z

    move-result v5

    new-instance v0, Lcom/twitter/library/api/conversations/i;

    iget-object v1, p0, Lcom/twitter/android/ek;->a:Lcom/twitter/android/DMRequestInboxFragment;

    invoke-virtual {v1}, Lcom/twitter/android/DMRequestInboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/ek;->a:Lcom/twitter/android/DMRequestInboxFragment;

    invoke-static {v2}, Lcom/twitter/android/DMRequestInboxFragment;->a(Lcom/twitter/android/DMRequestInboxFragment;)Lcom/twitter/library/client/Session;

    move-result-object v2

    move-wide v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/api/conversations/i;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JZ)V

    invoke-virtual {p1, v6}, Lcom/twitter/library/widget/UserApprovalView;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/ek;->a:Lcom/twitter/android/DMRequestInboxFragment;

    iget-object v1, v1, Lcom/twitter/android/DMRequestInboxFragment;->a:Ljava/util/HashMap;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/twitter/android/ek;->a:Lcom/twitter/android/DMRequestInboxFragment;

    invoke-static {v1, v0, v7, v6}, Lcom/twitter/android/DMRequestInboxFragment;->a(Lcom/twitter/android/DMRequestInboxFragment;Lcom/twitter/library/service/b;II)Z

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1, v7}, Lcom/twitter/library/widget/UserApprovalView;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/ek;->a:Lcom/twitter/android/DMRequestInboxFragment;

    iget-object v1, v1, Lcom/twitter/android/DMRequestInboxFragment;->a:Ljava/util/HashMap;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/twitter/android/ek;->a:Lcom/twitter/android/DMRequestInboxFragment;

    invoke-static {v1, v0, v8, v6}, Lcom/twitter/android/DMRequestInboxFragment;->b(Lcom/twitter/android/DMRequestInboxFragment;Lcom/twitter/library/service/b;II)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/ek;->a:Lcom/twitter/android/DMRequestInboxFragment;

    iget-object v0, v0, Lcom/twitter/android/DMRequestInboxFragment;->a:Ljava/util/HashMap;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
