.class Lcom/twitter/android/kk;
.super Lcom/twitter/library/service/a;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/MainActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/MainActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/kk;->a:Lcom/twitter/android/MainActivity;

    invoke-direct {p0}, Lcom/twitter/library/service/a;-><init>()V

    return-void
.end method

.method private a(Lcom/twitter/library/api/upload/ad;Lcom/twitter/internal/android/service/k;Z)V
    .locals 4

    invoke-virtual {p2}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v1

    invoke-virtual {p1}, Lcom/twitter/library/api/upload/ad;->r()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    move-object v0, p1

    check-cast v0, Lcom/twitter/library/api/upload/n;

    if-eqz v1, :cond_0

    if-eqz p3, :cond_0

    iget-object v1, p1, Lcom/twitter/library/api/upload/ad;->k:Landroid/os/Bundle;

    const-string/jumbo v2, "header_uri"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    invoke-virtual {v0}, Lcom/twitter/library/api/upload/n;->e()Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/kk;->a:Lcom/twitter/android/MainActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/MainActivity;->a(Z)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/kk;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v0}, Lcom/twitter/android/MainActivity;->I(Lcom/twitter/android/MainActivity;)Lcom/twitter/library/client/aa;

    move-result-object v0

    if-eqz v1, :cond_2

    if-eqz p3, :cond_2

    iget-object v1, p0, Lcom/twitter/android/kk;->a:Lcom/twitter/android/MainActivity;

    sget-object v2, Lcom/twitter/android/MainActivity;->a:Landroid/net/Uri;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/MainActivity;->a(Landroid/net/Uri;I)V

    :cond_2
    iget-object v1, p0, Lcom/twitter/android/kk;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v1, v0}, Lgx;->a(Landroid/content/Context;Lcom/twitter/library/client/aa;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/kk;->a:Lcom/twitter/android/MainActivity;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/twitter/android/MainActivity;->showDialog(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private a(Lcom/twitter/library/client/r;)V
    .locals 3

    invoke-virtual {p1}, Lcom/twitter/library/client/r;->r()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/client/r;->f()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/kk;->a:Lcom/twitter/android/MainActivity;

    sget-object v2, Lcom/twitter/android/MainActivity;->a:Landroid/net/Uri;

    invoke-virtual {v1, v2, v0}, Lcom/twitter/android/MainActivity;->a(Landroid/net/Uri;I)V

    :cond_0
    return-void
.end method

.method private a(Ljl;)V
    .locals 5

    invoke-virtual {p1}, Ljl;->e()J

    move-result-wide v0

    invoke-virtual {p1}, Ljl;->f()I

    move-result v2

    const-wide/16 v3, 0x0

    cmp-long v0, v0, v3

    if-nez v0, :cond_0

    if-lez v2, :cond_0

    iget-object v0, p0, Lcom/twitter/android/kk;->a:Lcom/twitter/android/MainActivity;

    sget-object v1, Lcom/twitter/android/MainActivity;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/MainActivity;->a(Landroid/net/Uri;I)V

    :cond_0
    invoke-virtual {p1}, Ljl;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v1, "home"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v1, p0, Lcom/twitter/android/kk;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v1, v0}, Lcom/twitter/android/MainActivity;->b(Lcom/twitter/android/MainActivity;Landroid/os/Bundle;)V

    :cond_1
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/internal/android/service/a;)V
    .locals 0

    check-cast p1, Lcom/twitter/library/service/b;

    invoke-virtual {p0, p1}, Lcom/twitter/android/kk;->a(Lcom/twitter/library/service/b;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/b;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/kk;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v0}, Lcom/twitter/android/MainActivity;->H(Lcom/twitter/android/MainActivity;)Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/library/service/b;->s()Lcom/twitter/library/service/p;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/service/p;->a(Lcom/twitter/library/client/Session;)Z

    move-result v1

    instance-of v0, p1, Ljl;

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    check-cast p1, Ljl;

    invoke-direct {p0, p1}, Lcom/twitter/android/kk;->a(Ljl;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p1, Lcom/twitter/library/service/b;->b:Ljava/lang/String;

    const-class v2, Lcom/twitter/library/client/t;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz v1, :cond_0

    check-cast p1, Lcom/twitter/library/client/r;

    invoke-direct {p0, p1}, Lcom/twitter/android/kk;->a(Lcom/twitter/library/client/r;)V

    goto :goto_0

    :cond_2
    instance-of v0, p1, Lcom/twitter/library/api/upload/ad;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lcom/twitter/library/api/upload/ad;

    invoke-virtual {p1}, Lcom/twitter/library/service/b;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v2

    invoke-direct {p0, v0, v2, v1}, Lcom/twitter/android/kk;->a(Lcom/twitter/library/api/upload/ad;Lcom/twitter/internal/android/service/k;Z)V

    goto :goto_0
.end method
