.class abstract enum Lcom/twitter/android/OneFactorLoginActivity$LoginState;
.super Ljava/lang/Enum;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/og;


# static fields
.field public static final enum a:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

.field public static final enum b:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

.field public static final enum c:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

.field public static final enum d:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

.field public static final enum e:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

.field public static final enum f:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

.field public static final enum g:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

.field public static final enum h:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

.field public static final enum i:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

.field private static final synthetic j:[Lcom/twitter/android/OneFactorLoginActivity$LoginState;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/twitter/android/OneFactorLoginActivity$LoginState$1;

    const-string/jumbo v1, "REQUESTING_CHALLENGE"

    invoke-direct {v0, v1, v3}, Lcom/twitter/android/OneFactorLoginActivity$LoginState$1;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/OneFactorLoginActivity$LoginState;->a:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    new-instance v0, Lcom/twitter/android/OneFactorLoginActivity$LoginState$2;

    const-string/jumbo v1, "WAITING_FOR_SMS"

    invoke-direct {v0, v1, v4}, Lcom/twitter/android/OneFactorLoginActivity$LoginState$2;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/OneFactorLoginActivity$LoginState;->b:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    new-instance v0, Lcom/twitter/android/OneFactorLoginActivity$LoginState$3;

    const-string/jumbo v1, "MANUAL_SMS_ENTRY"

    invoke-direct {v0, v1, v5}, Lcom/twitter/android/OneFactorLoginActivity$LoginState$3;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/OneFactorLoginActivity$LoginState;->c:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    new-instance v0, Lcom/twitter/android/OneFactorLoginActivity$LoginState$4;

    const-string/jumbo v1, "FAILED_TO_RECEIVE"

    invoke-direct {v0, v1, v6}, Lcom/twitter/android/OneFactorLoginActivity$LoginState$4;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/OneFactorLoginActivity$LoginState;->d:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    new-instance v0, Lcom/twitter/android/OneFactorLoginActivity$LoginState$5;

    const-string/jumbo v1, "CONFIRMING_CHALLENGE"

    invoke-direct {v0, v1, v7}, Lcom/twitter/android/OneFactorLoginActivity$LoginState$5;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/OneFactorLoginActivity$LoginState;->e:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    new-instance v0, Lcom/twitter/android/OneFactorLoginActivity$LoginState$6;

    const-string/jumbo v1, "INELIGIBLE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/OneFactorLoginActivity$LoginState$6;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/OneFactorLoginActivity$LoginState;->f:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    new-instance v0, Lcom/twitter/android/OneFactorLoginActivity$LoginState$7;

    const-string/jumbo v1, "INVALID_USERNAME"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/OneFactorLoginActivity$LoginState$7;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/OneFactorLoginActivity$LoginState;->g:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    new-instance v0, Lcom/twitter/android/OneFactorLoginActivity$LoginState$8;

    const-string/jumbo v1, "FAILED_INVALID_CODE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/OneFactorLoginActivity$LoginState$8;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/OneFactorLoginActivity$LoginState;->h:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    new-instance v0, Lcom/twitter/android/OneFactorLoginActivity$LoginState$9;

    const-string/jumbo v1, "FAILED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/OneFactorLoginActivity$LoginState$9;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/OneFactorLoginActivity$LoginState;->i:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    sget-object v1, Lcom/twitter/android/OneFactorLoginActivity$LoginState;->a:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    aput-object v1, v0, v3

    sget-object v1, Lcom/twitter/android/OneFactorLoginActivity$LoginState;->b:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    aput-object v1, v0, v4

    sget-object v1, Lcom/twitter/android/OneFactorLoginActivity$LoginState;->c:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    aput-object v1, v0, v5

    sget-object v1, Lcom/twitter/android/OneFactorLoginActivity$LoginState;->d:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    aput-object v1, v0, v6

    sget-object v1, Lcom/twitter/android/OneFactorLoginActivity$LoginState;->e:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/twitter/android/OneFactorLoginActivity$LoginState;->f:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/twitter/android/OneFactorLoginActivity$LoginState;->g:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/twitter/android/OneFactorLoginActivity$LoginState;->h:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/twitter/android/OneFactorLoginActivity$LoginState;->i:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/android/OneFactorLoginActivity$LoginState;->j:[Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILcom/twitter/android/od;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/OneFactorLoginActivity$LoginState;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/twitter/android/OneFactorLoginActivity$LoginState;
    .locals 1

    const-class v0, Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    return-object v0
.end method

.method public static values()[Lcom/twitter/android/OneFactorLoginActivity$LoginState;
    .locals 1

    sget-object v0, Lcom/twitter/android/OneFactorLoginActivity$LoginState;->j:[Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    invoke-virtual {v0}, [Lcom/twitter/android/OneFactorLoginActivity$LoginState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    return-object v0
.end method
