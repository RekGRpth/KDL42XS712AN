.class final Lcom/twitter/android/ah;
.super Landroid/os/AsyncTask;
.source "Twttr"


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;


# direct methods
.method private constructor <init>(Lcom/twitter/android/AmplifyMediaPlayerActivity;)V
    .locals 1

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/ah;->a:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/AmplifyMediaPlayerActivity;Lcom/twitter/android/ag;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/ah;-><init>(Lcom/twitter/android/AmplifyMediaPlayerActivity;)V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/ah;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/AmplifyMediaPlayerActivity;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/twitter/android/AmplifyMediaPlayerActivity;->c(Lcom/twitter/android/AmplifyMediaPlayerActivity;)Lcom/twitter/library/card/element/Player;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/twitter/android/AmplifyMediaPlayerActivity;->d(Lcom/twitter/android/AmplifyMediaPlayerActivity;)Lcom/twitter/library/amplify/AmplifyPlayer;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->c(Landroid/content/Context;)V

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method protected a(Ljava/lang/Void;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/ah;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/AmplifyMediaPlayerActivity;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/twitter/android/AmplifyMediaPlayerActivity;->c(Lcom/twitter/android/AmplifyMediaPlayerActivity;)Lcom/twitter/library/card/element/Player;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/twitter/android/AmplifyMediaPlayerActivity;->d(Lcom/twitter/android/AmplifyMediaPlayerActivity;)Lcom/twitter/library/amplify/AmplifyPlayer;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/AmplifyPlayer;->e(Z)V

    :cond_0
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/ah;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/ah;->a(Ljava/lang/Void;)V

    return-void
.end method
