.class Lcom/twitter/android/dd;
.super Landroid/os/AsyncTask;
.source "Twttr"


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;

.field private final b:Landroid/content/Context;

.field private final c:Landroid/graphics/Rect;

.field private final d:I

.field private final e:Lcom/twitter/android/dc;

.field private f:Landroid/graphics/Rect;

.field private g:I


# direct methods
.method constructor <init>(Lcom/twitter/android/CropManager;Landroid/content/Context;Landroid/graphics/Rect;I)V
    .locals 1

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/dd;->a:Ljava/lang/ref/WeakReference;

    iput-object p2, p0, Lcom/twitter/android/dd;->b:Landroid/content/Context;

    iput-object p3, p0, Lcom/twitter/android/dd;->c:Landroid/graphics/Rect;

    iput p4, p0, Lcom/twitter/android/dd;->d:I

    iget-object v0, p1, Lcom/twitter/android/CropManager;->a:Lcom/twitter/android/dc;

    iput-object v0, p0, Lcom/twitter/android/dd;->e:Lcom/twitter/android/dc;

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/dd;->g:I

    return-void
.end method

.method private a(Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 9

    const/4 v8, 0x0

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, p1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iget-object v1, p0, Lcom/twitter/android/dd;->e:Lcom/twitter/android/dc;

    iget v1, v1, Lcom/twitter/android/dc;->b:I

    iget-object v2, p0, Lcom/twitter/android/dd;->e:Lcom/twitter/android/dc;

    iget-object v2, v2, Lcom/twitter/android/dc;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    div-int/2addr v1, v2

    iget-object v2, p0, Lcom/twitter/android/dd;->e:Lcom/twitter/android/dc;

    iget v2, v2, Lcom/twitter/android/dc;->b:I

    iget-object v3, p0, Lcom/twitter/android/dd;->e:Lcom/twitter/android/dc;

    iget v3, v3, Lcom/twitter/android/dc;->c:I

    rem-int v4, v2, v1

    rem-int v5, v3, v1

    iget v6, v0, Landroid/graphics/Rect;->left:I

    iget v7, v0, Landroid/graphics/Rect;->right:I

    if-ne v6, v7, :cond_0

    iget v6, v0, Landroid/graphics/Rect;->right:I

    add-int/lit8 v6, v6, 0x1

    iput v6, v0, Landroid/graphics/Rect;->right:I

    :cond_0
    iget v6, v0, Landroid/graphics/Rect;->top:I

    iget v7, v0, Landroid/graphics/Rect;->bottom:I

    if-ne v6, v7, :cond_1

    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v6, v6, 0x1

    iput v6, v0, Landroid/graphics/Rect;->bottom:I

    :cond_1
    iget v6, v0, Landroid/graphics/Rect;->left:I

    mul-int/2addr v6, v1

    invoke-static {v6, v8}, Ljava/lang/Math;->max(II)I

    move-result v6

    iget v7, v0, Landroid/graphics/Rect;->top:I

    mul-int/2addr v7, v1

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v7

    iget v8, v0, Landroid/graphics/Rect;->right:I

    sub-int v4, v8, v4

    mul-int/2addr v4, v1

    add-int/lit8 v2, v2, -0x1

    invoke-static {v4, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    iget v4, v0, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v4, v5

    mul-int/2addr v1, v4

    add-int/lit8 v3, v3, -0x1

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-virtual {v0, v6, v7, v2, v1}, Landroid/graphics/Rect;->set(IIII)V

    return-object v0
.end method

.method private a(Lcom/twitter/android/dc;)Landroid/net/Uri;
    .locals 9

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v8, 0x1

    if-nez p1, :cond_0

    move-object v0, v6

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/dd;->b:Landroid/content/Context;

    iget-object v1, p1, Lcom/twitter/android/dc;->a:Landroid/graphics/Bitmap;

    iget-object v7, p0, Lcom/twitter/android/dd;->c:Landroid/graphics/Rect;

    new-instance v2, Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-direct {v2, v5, v5, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v5

    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v4

    if-le v4, v8, :cond_2

    if-le v5, v8, :cond_2

    invoke-virtual {v2, v7}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget v2, v7, Landroid/graphics/Rect;->left:I

    iget v3, v7, Landroid/graphics/Rect;->top:I

    invoke-static/range {v0 .. v5}, Lcom/twitter/library/util/n;->a(Landroid/content/Context;Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-static {v0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    const/16 v4, 0x64

    invoke-static {v0, v1, v2, v3, v4}, Lcom/twitter/library/util/n;->a(Landroid/content/Context;Landroid/graphics/Bitmap;JI)Landroid/net/Uri;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-direct {p0, v7}, Lcom/twitter/android/dd;->a(Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/dd;->f:Landroid/graphics/Rect;

    iget v2, p0, Lcom/twitter/android/dd;->d:I

    invoke-static {v0, v1, v2}, Lcom/twitter/library/util/n;->a(Landroid/content/Context;Landroid/net/Uri;I)V

    move-object v0, v1

    goto :goto_0

    :cond_1
    iput v8, p0, Lcom/twitter/android/dd;->g:I

    :cond_2
    move-object v0, v6

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/twitter/android/dd;->g:I

    return v0
.end method

.method protected varargs a([Landroid/net/Uri;)Landroid/net/Uri;
    .locals 6

    const/4 v5, 0x1

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/android/dd;->e:Lcom/twitter/android/dc;

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/dd;->b:Landroid/content/Context;

    invoke-static {}, Lcom/twitter/media/MediaUtils;->a()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v0, p0, Lcom/twitter/android/dd;->e:Lcom/twitter/android/dc;

    invoke-direct {p0, v0}, Lcom/twitter/android/dd;->a(Lcom/twitter/android/dc;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-static {v1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    iget-object v4, p0, Lcom/twitter/android/dd;->c:Landroid/graphics/Rect;

    invoke-direct {p0, v4}, Lcom/twitter/android/dd;->a(Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v4

    invoke-static {v1, v5, v2, v3}, Lcom/twitter/library/util/n;->a(Landroid/content/Context;ZJ)Ljava/io/File;

    move-result-object v2

    if-nez v2, :cond_2

    iput v5, p0, Lcom/twitter/android/dd;->g:I

    goto :goto_0

    :cond_2
    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-static {v1, v3, v2, v4}, Lcom/twitter/media/MediaUtils;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/io/File;Landroid/graphics/Rect;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/library/util/n;->b(Landroid/net/Uri;)Z

    goto :goto_0

    :cond_3
    iput-object v4, p0, Lcom/twitter/android/dd;->f:Landroid/graphics/Rect;

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    iget v2, p0, Lcom/twitter/android/dd;->d:I

    invoke-static {v1, v0, v2}, Lcom/twitter/library/util/n;->a(Landroid/content/Context;Landroid/net/Uri;I)V

    goto :goto_0
.end method

.method protected a(Landroid/net/Uri;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/dd;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/CropManager;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/dd;->f:Landroid/graphics/Rect;

    invoke-static {v0, p1, v1}, Lcom/twitter/android/CropManager;->a(Lcom/twitter/android/CropManager;Landroid/net/Uri;Landroid/graphics/Rect;)V

    :cond_0
    return-void
.end method

.method protected b(Landroid/net/Uri;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/dd;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/CropManager;

    if-eqz v0, :cond_0

    invoke-static {v0, v1, v1}, Lcom/twitter/android/CropManager;->a(Lcom/twitter/android/CropManager;Landroid/net/Uri;Landroid/graphics/Rect;)V

    :cond_0
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Landroid/net/Uri;

    invoke-virtual {p0, p1}, Lcom/twitter/android/dd;->a([Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Landroid/net/Uri;

    invoke-virtual {p0, p1}, Lcom/twitter/android/dd;->b(Landroid/net/Uri;)V

    return-void
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Landroid/net/Uri;

    invoke-virtual {p0, p1}, Lcom/twitter/android/dd;->a(Landroid/net/Uri;)V

    return-void
.end method
