.class Lcom/twitter/android/wo;
.super Lcom/twitter/library/client/j;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/TweetActivity;


# direct methods
.method private constructor <init>(Lcom/twitter/android/TweetActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/wo;->a:Lcom/twitter/android/TweetActivity;

    invoke-direct {p0}, Lcom/twitter/library/client/j;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/TweetActivity;Lcom/twitter/android/vx;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/wo;-><init>(Lcom/twitter/android/TweetActivity;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;J)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/wo;->a:Lcom/twitter/android/TweetActivity;

    iget-object v1, p0, Lcom/twitter/android/wo;->a:Lcom/twitter/android/TweetActivity;

    iget-object v1, v1, Lcom/twitter/android/TweetActivity;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    invoke-static {p5, p6, v1, v2}, Lcom/twitter/library/provider/w;->a(JJ)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/TweetActivity;->a(Lcom/twitter/android/TweetActivity;Landroid/net/Uri;)Landroid/net/Uri;

    iget-object v0, p0, Lcom/twitter/android/wo;->a:Lcom/twitter/android/TweetActivity;

    invoke-virtual {v0}, Lcom/twitter/android/TweetActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/wo;->a:Lcom/twitter/android/TweetActivity;

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    return-void
.end method

.method public b(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;J)V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/wo;->a:Lcom/twitter/android/TweetActivity;

    iget-object v1, p0, Lcom/twitter/android/wo;->a:Lcom/twitter/android/TweetActivity;

    iget-object v1, v1, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v1, v1, Lcom/twitter/library/provider/Tweet;->o:J

    iget-object v3, p0, Lcom/twitter/android/wo;->a:Lcom/twitter/android/TweetActivity;

    iget-object v3, v3, Lcom/twitter/android/TweetActivity;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-static {v1, v2, v3, v4}, Lcom/twitter/library/provider/w;->b(JJ)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/TweetActivity;->a(Lcom/twitter/android/TweetActivity;Landroid/net/Uri;)Landroid/net/Uri;

    iget-object v0, p0, Lcom/twitter/android/wo;->a:Lcom/twitter/android/TweetActivity;

    invoke-virtual {v0}, Lcom/twitter/android/TweetActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/wo;->a:Lcom/twitter/android/TweetActivity;

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    return-void
.end method
