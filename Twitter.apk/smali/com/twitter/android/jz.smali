.class Lcom/twitter/android/jz;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/twitter/android/MainActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/MainActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/jz;->a:Lcom/twitter/android/MainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/jz;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v0}, Lcom/twitter/android/MainActivity;->e(Lcom/twitter/android/MainActivity;)Lcom/twitter/android/km;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/km;->notifyDataSetChanged()V

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/android/jz;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v1}, Lcom/twitter/android/MainActivity;->j(Lcom/twitter/android/MainActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhb;

    iget-boolean v3, v0, Lhb;->g:Z

    if-eqz v3, :cond_2

    iget v0, v0, Lhb;->k:I

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/jz;->a:Lcom/twitter/android/MainActivity;

    invoke-virtual {v0}, Lcom/twitter/android/MainActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->setNumber(I)V

    iget-object v0, p0, Lcom/twitter/android/jz;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v0}, Lcom/twitter/android/MainActivity;->d(Lcom/twitter/android/MainActivity;)Lcom/twitter/android/kf;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/kf;->b()Lcom/twitter/android/kg;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/kn;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/twitter/android/jz;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v1}, Lcom/twitter/android/MainActivity;->k(Lcom/twitter/android/MainActivity;)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/jz;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v1}, Lcom/twitter/android/MainActivity;->k(Lcom/twitter/android/MainActivity;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/kn;->a(Landroid/view/View;)V

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/jz;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v0}, Lcom/twitter/android/MainActivity;->d(Lcom/twitter/android/MainActivity;)Lcom/twitter/android/kf;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/kf;->notifyDataSetChanged()V

    return-void

    :cond_2
    move v0, v1

    goto :goto_1
.end method
