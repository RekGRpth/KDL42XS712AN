.class public Lcom/twitter/android/fl;
.super Lcom/twitter/android/az;
.source "Twttr"


# instance fields
.field private final b:Lcom/twitter/android/client/c;

.field private final c:Landroid/view/LayoutInflater;

.field private final d:Ljava/util/ArrayList;

.field private final e:Lcom/twitter/android/vf;

.field private final f:Z

.field private final g:I

.field private final h:I

.field private final i:Ljava/lang/String;

.field private final j:Lcom/twitter/library/client/aa;

.field private k:Landroid/database/Cursor;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/android/vf;ZLjava/lang/String;)V
    .locals 3

    invoke-direct {p0}, Lcom/twitter/android/az;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/fl;->d:Ljava/util/ArrayList;

    invoke-static {p1}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/fl;->b:Lcom/twitter/android/client/c;

    invoke-static {p1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/fl;->j:Lcom/twitter/library/client/aa;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/fl;->c:Landroid/view/LayoutInflater;

    iput-object p2, p0, Lcom/twitter/android/fl;->e:Lcom/twitter/android/vf;

    iput-boolean p3, p0, Lcom/twitter/android/fl;->f:Z

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0060    # com.twitter.android.R.dimen.event_gallery_height

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/fl;->g:I

    const/4 v1, 0x0

    const v2, 0x7f0c0061    # com.twitter.android.R.dimen.event_sports_gallery_height

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-static {v1, v2, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/twitter/android/fl;->h:I

    iput-object p4, p0, Lcom/twitter/android/fl;->i:Ljava/lang/String;

    return-void
.end method

.method private a(Landroid/view/ViewGroup;Landroid/database/Cursor;II)Landroid/view/View;
    .locals 23

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/fl;->c:Landroid/view/LayoutInflater;

    const/4 v2, 0x0

    move/from16 v0, p4

    invoke-virtual {v1, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/widget/EventView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/fl;->d:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget v2, Lcom/twitter/library/provider/cc;->C:I

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/twitter/library/provider/cc;->J:I

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    sget v3, Lcom/twitter/library/provider/cc;->K:I

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    sget v3, Lcom/twitter/library/provider/cc;->N:I

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    sget v3, Lcom/twitter/library/provider/cc;->I:I

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v3, Lcom/twitter/library/provider/cc;->G:I

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget v3, Lcom/twitter/library/provider/cc;->F:I

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget v3, Lcom/twitter/library/provider/cc;->E:I

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    sget v3, Lcom/twitter/library/provider/cc;->H:I

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    sget v3, Lcom/twitter/library/provider/cc;->j:I

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    invoke-static {v3}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v3

    move-object/from16 v22, v3

    check-cast v22, Lcom/twitter/library/api/TimelineScribeContent;

    sget v3, Lcom/twitter/library/provider/cc;->M:I

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v15

    const-wide/16 v10, -0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/fl;->b:Lcom/twitter/android/client/c;

    iget-object v12, v3, Lcom/twitter/android/client/c;->a:Lcom/twitter/library/widget/ap;

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/twitter/android/fl;->f:Z

    move/from16 v16, v0

    const/16 v17, 0x1

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    move/from16 v3, p3

    invoke-virtual/range {v1 .. v21}, Lcom/twitter/android/widget/EventView;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJLcom/twitter/library/widget/ap;Ljava/lang/String;Ljava/lang/String;[BZZLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    invoke-static/range {p3 .. p3}, Lcom/twitter/library/api/TwitterTopic;->b(I)Ljava/lang/String;

    move-result-object v16

    new-instance v8, Lcom/twitter/android/fm;

    move-object/from16 v9, p0

    move-object v10, v2

    move/from16 v11, p3

    move-object v12, v5

    move-object v14, v1

    move-object/from16 v15, v22

    invoke-direct/range {v8 .. v16}, Lcom/twitter/android/fm;-><init>(Lcom/twitter/android/fl;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/twitter/android/widget/EventView;Lcom/twitter/library/api/TimelineScribeContent;Ljava/lang/String;)V

    invoke-virtual {v1, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v1
.end method

.method static synthetic a(Lcom/twitter/android/fl;)Lcom/twitter/android/vf;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/fl;->e:Lcom/twitter/android/vf;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/fl;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/fl;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/fl;)Lcom/twitter/library/client/aa;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/fl;->j:Lcom/twitter/library/client/aa;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/fl;)Lcom/twitter/android/client/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/fl;->b:Lcom/twitter/android/client/c;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/fl;->k:Landroid/database/Cursor;

    if-eq v0, p1, :cond_2

    iget-object v1, p0, Lcom/twitter/android/fl;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    iput-object p1, p0, Lcom/twitter/android/fl;->k:Landroid/database/Cursor;

    if-eqz p1, :cond_1

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/fl;->a:Ljava/util/List;

    sget v2, Lcom/twitter/library/provider/cc;->C:I

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/fl;->notifyDataSetChanged()V

    :cond_2
    return-object v0
.end method

.method public a(Ljava/util/HashMap;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/fl;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/EventView;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/EventView;->setEventImages(Ljava/util/HashMap;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 2

    move-object v0, p3

    check-cast v0, Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/android/fl;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/fl;->k:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    return v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/fl;->k:Landroid/database/Cursor;

    invoke-interface {v0, p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    sget v1, Lcom/twitter/library/provider/cc;->D:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    new-instance v0, Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    :goto_0
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0

    :pswitch_1
    const v3, 0x7f03006e    # com.twitter.android.R.layout.event_view

    invoke-direct {p0, p1, v0, v1, v3}, Lcom/twitter/android/fl;->a(Landroid/view/ViewGroup;Landroid/database/Cursor;II)Landroid/view/View;

    move-result-object v0

    iget v1, p0, Lcom/twitter/android/fl;->g:I

    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_0

    :pswitch_2
    const v3, 0x7f03006b    # com.twitter.android.R.layout.event_sports_view

    invoke-direct {p0, p1, v0, v1, v3}, Lcom/twitter/android/fl;->a(Landroid/view/ViewGroup;Landroid/database/Cursor;II)Landroid/view/View;

    move-result-object v0

    iget v1, p0, Lcom/twitter/android/fl;->h:I

    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
