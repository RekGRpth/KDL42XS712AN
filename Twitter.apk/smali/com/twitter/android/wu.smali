.class Lcom/twitter/android/wu;
.super Landroid/os/AsyncTask;
.source "Twttr"


# instance fields
.field final a:Landroid/content/Context;

.field final b:Ljava/util/HashSet;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/HashSet;)V
    .locals 0

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/wu;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/twitter/android/wu;->b:Ljava/util/HashSet;

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Object;)Ljava/util/HashSet;
    .locals 4

    const-string/jumbo v0, "android_typeahead_2_2006"

    invoke-static {v0}, Lkk;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v0, "trigger_100"

    invoke-virtual {v1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "100"

    :goto_0
    const-string/jumbo v2, "trigger_dmable"

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    const-string/jumbo v3, "_agg"

    invoke-virtual {v1, v3}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Lcom/twitter/library/util/x;->x:Ljava/util/regex/Pattern;

    :goto_1
    iget-object v3, p0, Lcom/twitter/android/wu;->a:Landroid/content/Context;

    invoke-static {v3, v0, v2, v1}, Lcom/twitter/android/provider/SuggestionsProvider;->a(Landroid/content/Context;Ljava/lang/String;ZLjava/util/regex/Pattern;)Ljava/util/HashSet;

    move-result-object v0

    return-object v0

    :cond_0
    const-string/jumbo v0, "trigger_500"

    invoke-virtual {v1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "500"

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    sget-object v1, Lcom/twitter/library/util/x;->w:Ljava/util/regex/Pattern;

    goto :goto_1
.end method

.method protected a(Ljava/util/HashSet;)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/HashSet;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/wu;->b:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    iget-object v0, p0, Lcom/twitter/android/wu;->b:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    :cond_0
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/wu;->a([Ljava/lang/Object;)Ljava/util/HashSet;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/util/HashSet;

    invoke-virtual {p0, p1}, Lcom/twitter/android/wu;->a(Ljava/util/HashSet;)V

    return-void
.end method
