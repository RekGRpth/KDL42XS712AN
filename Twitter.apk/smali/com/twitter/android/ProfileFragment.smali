.class public Lcom/twitter/android/ProfileFragment;
.super Lcom/twitter/android/client/BaseListFragment;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/twitter/android/f;
.implements Lcom/twitter/android/ty;
.implements Lcom/twitter/android/widget/ce;
.implements Lcom/twitter/library/util/ac;


# static fields
.field private static final U:I


# instance fields
.field A:Ljava/lang/String;

.field B:Ljava/lang/String;

.field C:Ljava/lang/String;

.field D:Z

.field E:I

.field F:I

.field G:I

.field H:I

.field I:J

.field J:J

.field K:Z

.field final L:Z

.field private V:Lcom/twitter/android/ob;

.field private W:Lcom/twitter/library/view/c;

.field private final X:Ljava/util/HashSet;

.field private final Y:Ljava/util/ArrayList;

.field private final Z:Ljava/util/HashSet;

.field a:I

.field private aA:Lcom/twitter/library/api/TweetEntities;

.field private aB:Landroid/view/View;

.field private aC:Landroid/view/View;

.field private aD:Landroid/widget/TextView;

.field private aE:Z

.field private aF:Z

.field private aG:Z

.field private aH:Z

.field private aI:Z

.field private aJ:Z

.field private aK:Z

.field private aL:I

.field private aM:Lcom/twitter/library/scribe/ScribeAssociation;

.field private aN:Lcom/twitter/library/api/TwitterUser;

.field private aO:Z

.field private aP:Ljava/lang/String;

.field private aQ:Z

.field private aR:Landroid/graphics/drawable/ColorDrawable;

.field private aS:I

.field private aT:Lcom/twitter/library/scribe/ScribeItem;

.field private aU:J

.field private aV:Lcom/twitter/library/util/aa;

.field private aW:Lcom/twitter/library/service/a;

.field private final aa:Landroid/util/SparseArray;

.field private final ab:Landroid/util/SparseArray;

.field private ac:Ljava/util/HashMap;

.field private ad:Ljava/util/HashMap;

.field private ae:Ljava/util/HashMap;

.field private af:Ljava/util/HashMap;

.field private ag:Ljava/util/HashMap;

.field private ah:Lcom/twitter/android/e;

.field private ai:Landroid/graphics/Bitmap;

.field private aj:Lcom/twitter/library/util/FriendshipCache;

.field private ak:Landroid/widget/Button;

.field private al:Landroid/widget/ImageButton;

.field private am:Landroid/widget/Button;

.field private an:Landroid/widget/ImageButton;

.field private ao:Landroid/widget/LinearLayout;

.field private ap:Lcom/twitter/android/oo;

.field private aq:Landroid/widget/RelativeLayout;

.field private ar:Lcom/twitter/android/widget/cg;

.field private as:Lcom/twitter/android/tn;

.field private at:Ljava/lang/String;

.field private au:Ljava/lang/String;

.field private av:Lcom/twitter/internal/android/widget/ShadowTextView;

.field private aw:Landroid/widget/ImageButton;

.field private ax:Landroid/widget/ImageButton;

.field private ay:Landroid/widget/ImageButton;

.field private az:Landroid/view/animation/TranslateAnimation;

.field b:I

.field c:Lcom/twitter/android/qt;

.field d:Landroid/widget/ImageView;

.field e:Landroid/widget/ImageView;

.field f:Landroid/widget/ImageView;

.field g:Lcom/twitter/android/widget/PipView;

.field h:Lcom/twitter/library/util/m;

.field i:Lcom/twitter/library/util/m;

.field j:Lcom/twitter/android/widget/ProfileHeader;

.field k:Lcom/twitter/android/qq;

.field l:Lcom/twitter/android/qz;

.field m:Lcom/twitter/android/rb;

.field n:Lcom/twitter/android/qz;

.field o:Lcom/twitter/library/api/PromotedContent;

.field p:Landroid/widget/TextView;

.field q:Landroid/widget/TextView;

.field r:Landroid/widget/TextView;

.field s:Landroid/widget/TextView;

.field t:Landroid/widget/TextView;

.field u:Landroid/widget/TextView;

.field v:Landroid/widget/TextView;

.field w:Lcom/twitter/android/ud;

.field x:Lcom/twitter/library/api/TwitterUser;

.field y:Landroid/support/v4/view/ViewPager;

.field z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/twitter/android/ProfileFragment$DialogMoreButtons;->values()[Lcom/twitter/android/ProfileFragment$DialogMoreButtons;

    move-result-object v0

    array-length v0, v0

    sput v0, Lcom/twitter/android/ProfileFragment;->U:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, -0x1

    invoke-direct {p0}, Lcom/twitter/android/client/BaseListFragment;-><init>()V

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->a:I

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->b:I

    iput v1, p0, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-static {}, Lcom/twitter/android/util/af;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->L:Z

    new-instance v0, Lcom/twitter/android/qe;

    invoke-direct {v0, p0}, Lcom/twitter/android/qe;-><init>(Lcom/twitter/android/ProfileFragment;)V

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->V:Lcom/twitter/android/ob;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->X:Ljava/util/HashSet;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->Y:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->Z:Ljava/util/HashSet;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->aa:Landroid/util/SparseArray;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->ab:Landroid/util/SparseArray;

    iput-boolean v1, p0, Lcom/twitter/android/ProfileFragment;->aK:Z

    return-void
.end method

.method static synthetic A(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic B(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/library/util/FriendshipCache;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->aj:Lcom/twitter/library/util/FriendshipCache;

    return-object v0
.end method

.method static synthetic C(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic D(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic E(Lcom/twitter/android/ProfileFragment;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->ad:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic F(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method private F()V
    .locals 18

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/ProfileFragment;->k:Lcom/twitter/android/qq;

    if-nez v1, :cond_0

    invoke-direct/range {p0 .. p0}, Lcom/twitter/android/ProfileFragment;->N()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Lcom/twitter/android/qq;

    move-object/from16 v0, p0

    invoke-direct {v1, v2, v0}, Lcom/twitter/android/qq;-><init>(Landroid/content/Context;Lcom/twitter/android/ProfileFragment;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/twitter/android/ProfileFragment;->k:Lcom/twitter/android/qq;

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/ProfileFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string/jumbo v3, "friendship"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    const/16 v3, 0x20

    if-ne v1, v3, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/ProfileFragment;->k:Lcom/twitter/android/qq;

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lcom/twitter/android/qq;->a(Z)V

    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/ProfileFragment;->l:Lcom/twitter/android/qz;

    if-nez v1, :cond_1

    new-instance v1, Lcom/twitter/android/rh;

    const/4 v3, 0x0

    const v5, 0x7f020085    # com.twitter.android.R.drawable.btn_follow_action

    const/16 v6, 0x14

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/twitter/android/ProfileFragment;->g(I)Lcom/twitter/library/widget/a;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/android/ProfileFragment;->aj:Lcom/twitter/library/util/FriendshipCache;

    const/4 v8, 0x1

    invoke-direct/range {v1 .. v8}, Lcom/twitter/android/rh;-><init>(Landroid/content/Context;ILcom/twitter/android/client/c;ILcom/twitter/library/widget/a;Lcom/twitter/library/util/FriendshipCache;Z)V

    const/16 v3, 0x14

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/twitter/android/ProfileFragment;->l(I)Lcom/twitter/android/ob;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/twitter/android/rh;->a(Lcom/twitter/android/ob;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/ProfileFragment;->W:Lcom/twitter/library/view/c;

    invoke-virtual {v1, v3}, Lcom/twitter/android/rh;->a(Lcom/twitter/library/view/c;)V

    new-instance v3, Lcom/twitter/android/qz;

    const/16 v5, 0x14

    const/4 v6, 0x1

    invoke-direct {v3, v2, v1, v5, v6}, Lcom/twitter/android/qz;-><init>(Landroid/content/Context;Lcom/twitter/android/rh;II)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/twitter/android/ProfileFragment;->l:Lcom/twitter/android/qz;

    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/ProfileFragment;->m:Lcom/twitter/android/rb;

    if-nez v1, :cond_3

    new-instance v1, Lcom/twitter/android/ye;

    const/4 v3, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    iget-object v13, v4, Lcom/twitter/android/client/c;->a:Lcom/twitter/library/widget/ap;

    new-instance v5, Lcom/twitter/android/qw;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/android/ProfileFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/ProfileFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    const-string/jumbo v7, "tweet"

    const-string/jumbo v9, "avatar"

    const-string/jumbo v10, "profile_click"

    invoke-static {v6, v7, v9, v10}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v10, "profile::tweet:link:open_link"

    move-object/from16 v6, p0

    move-object/from16 v7, p0

    invoke-direct/range {v5 .. v10}, Lcom/twitter/android/qw;-><init>(Lcom/twitter/android/ProfileFragment;Landroid/support/v4/app/Fragment;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;)V

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/ProfileFragment;->aj:Lcom/twitter/library/util/FriendshipCache;

    move-object/from16 v17, v0

    move-object v6, v1

    move-object v7, v2

    move v8, v3

    move v9, v14

    move v10, v15

    move-object v14, v4

    move-object v15, v5

    invoke-direct/range {v6 .. v17}, Lcom/twitter/android/ye;-><init>(Landroid/app/Activity;IZZZZLcom/twitter/library/widget/ap;Lcom/twitter/android/client/c;Lcom/twitter/library/widget/aa;Lcom/twitter/android/vs;Lcom/twitter/library/util/FriendshipCache;)V

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/twitter/android/ProfileFragment;->D:Z

    if-eqz v3, :cond_2

    invoke-static {}, Lgw;->b()I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/twitter/android/ye;->e(I)V

    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/ProfileFragment;->V:Lcom/twitter/android/ob;

    invoke-virtual {v1, v3}, Lcom/twitter/android/ye;->b(Lcom/twitter/android/ob;)V

    new-instance v3, Lcom/twitter/android/rb;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/ProfileFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {v3, v2, v5, v1}, Lcom/twitter/android/rb;-><init>(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeAssociation;Lcom/twitter/android/ye;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/twitter/android/ProfileFragment;->m:Lcom/twitter/android/rb;

    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/ProfileFragment;->as:Lcom/twitter/android/tn;

    if-nez v1, :cond_4

    new-instance v1, Lcom/twitter/android/tn;

    const/4 v3, 0x0

    new-array v3, v3, [Lcom/twitter/android/to;

    const v5, 0x7f030136    # com.twitter.android.R.layout.section_simple_row_view

    invoke-direct {v1, v4, v3, v5}, Lcom/twitter/android/tn;-><init>(Lcom/twitter/android/client/c;[Lcom/twitter/android/to;I)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/twitter/android/ProfileFragment;->as:Lcom/twitter/android/tn;

    :cond_4
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/ProfileFragment;->aV:Lcom/twitter/library/util/aa;

    if-nez v1, :cond_5

    invoke-virtual {v4, v2}, Lcom/twitter/android/client/c;->f(Landroid/content/Context;)Lcom/twitter/library/util/aa;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, Lcom/twitter/library/util/aa;->a(Lcom/twitter/library/util/ac;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/twitter/android/ProfileFragment;->aV:Lcom/twitter/library/util/aa;

    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/ProfileFragment;->w:Lcom/twitter/android/ud;

    if-nez v3, :cond_6

    new-instance v3, Lcom/twitter/android/ud;

    const/4 v5, 0x0

    invoke-direct {v3, v2, v5, v1}, Lcom/twitter/android/ud;-><init>(Landroid/content/Context;ZLcom/twitter/library/util/aa;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/twitter/android/ProfileFragment;->w:Lcom/twitter/android/ud;

    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/ProfileFragment;->ap:Lcom/twitter/android/oo;

    if-nez v1, :cond_7

    new-instance v1, Lcom/twitter/android/oo;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/ProfileFragment;->w:Lcom/twitter/android/ud;

    invoke-direct {v1, v3}, Lcom/twitter/android/oo;-><init>(Lcom/twitter/android/ud;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/twitter/android/ProfileFragment;->ap:Lcom/twitter/android/oo;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/ProfileFragment;->ap:Lcom/twitter/android/oo;

    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, Lcom/twitter/android/oo;->a(Landroid/widget/AdapterView$OnItemClickListener;)V

    :cond_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/ProfileFragment;->n:Lcom/twitter/android/qz;

    if-nez v1, :cond_8

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->d()Z

    move-result v1

    if-eqz v1, :cond_9

    const v5, 0x7f020085    # com.twitter.android.R.drawable.btn_follow_action

    :goto_0
    new-instance v1, Lcom/twitter/android/rh;

    const/4 v3, 0x0

    const/16 v6, 0xa

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/twitter/android/ProfileFragment;->g(I)Lcom/twitter/library/widget/a;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/android/ProfileFragment;->aj:Lcom/twitter/library/util/FriendshipCache;

    const/4 v8, 0x1

    invoke-direct/range {v1 .. v8}, Lcom/twitter/android/rh;-><init>(Landroid/content/Context;ILcom/twitter/android/client/c;ILcom/twitter/library/widget/a;Lcom/twitter/library/util/FriendshipCache;Z)V

    const/16 v3, 0xa

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/twitter/android/ProfileFragment;->l(I)Lcom/twitter/android/ob;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/twitter/android/rh;->a(Lcom/twitter/android/ob;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/ProfileFragment;->W:Lcom/twitter/library/view/c;

    invoke-virtual {v1, v3}, Lcom/twitter/android/rh;->a(Lcom/twitter/library/view/c;)V

    new-instance v3, Lcom/twitter/android/qz;

    const/16 v4, 0xa

    const/4 v5, 0x3

    invoke-direct {v3, v2, v1, v4, v5}, Lcom/twitter/android/qz;-><init>(Landroid/content/Context;Lcom/twitter/android/rh;II)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/twitter/android/ProfileFragment;->n:Lcom/twitter/android/qz;

    :cond_8
    return-void

    :cond_9
    const/4 v5, 0x0

    goto :goto_0
.end method

.method static synthetic G(Lcom/twitter/android/ProfileFragment;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->ae:Ljava/util/HashMap;

    return-object v0
.end method

.method private G()V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->aa:Landroid/util/SparseArray;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    const-string/jumbo v1, "profile:similar_to:stream::results"

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/ProfileFragment;->a(Ljava/util/ArrayList;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->aa:Landroid/util/SparseArray;

    const/16 v1, 0x14

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    const-string/jumbo v1, "profile:user_similarities_list:stream::results"

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/ProfileFragment;->a(Ljava/util/ArrayList;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->Y:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "profile:tweets:stream::results"

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/ProfileFragment;->Y:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/util/ArrayList;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->Y:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    return-void
.end method

.method static synthetic H(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private H()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->aQ:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-static {v0}, Lcom/twitter/library/provider/ay;->l(I)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-static {v0}, Lcom/twitter/library/provider/ay;->h(I)Z

    move-result v0

    goto :goto_0
.end method

.method private I()V
    .locals 5

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->au()Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    new-instance v1, Liz;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Liz;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    iget-wide v2, p0, Lcom/twitter/android/ProfileFragment;->I:J

    invoke-virtual {v1, v2, v3}, Liz;->a(J)Ljb;

    move-result-object v0

    const/16 v1, 0xb

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/library/service/b;II)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    or-int/lit16 v0, v0, 0x2000

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-direct {p0, v4}, Lcom/twitter/android/ProfileFragment;->e(Z)V

    invoke-virtual {p0, v4}, Lcom/twitter/android/ProfileFragment;->d(Z)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/util/af;->a(Landroid/content/Context;)V

    goto :goto_0
.end method

.method static synthetic I(Lcom/twitter/android/ProfileFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->aQ:Z

    return v0
.end method

.method static synthetic J(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private J()V
    .locals 5

    const/4 v4, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->au()Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    new-instance v1, Lja;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lja;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    iget-wide v2, p0, Lcom/twitter/android/ProfileFragment;->I:J

    invoke-virtual {v1, v2, v3}, Lja;->a(J)Ljb;

    move-result-object v0

    const/16 v1, 0xc

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/library/service/b;II)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    const/16 v1, 0x2000

    invoke-static {v0, v1}, Lcom/twitter/library/provider/ay;->b(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-direct {p0, v4}, Lcom/twitter/android/ProfileFragment;->e(Z)V

    invoke-virtual {p0, v4}, Lcom/twitter/android/ProfileFragment;->d(Z)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/util/af;->b(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private K()V
    .locals 9

    const/4 v1, 0x0

    iget-object v6, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    iget-object v7, v6, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/client/cd;->a(Lcom/twitter/library/client/Session;)Lcom/twitter/library/client/v;

    move-result-object v4

    iget-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->D:Z

    if-eqz v0, :cond_2

    if-eqz v4, :cond_2

    iget-boolean v0, v4, Lcom/twitter/library/client/v;->h:Z

    if-eqz v0, :cond_2

    iget-object v0, v4, Lcom/twitter/library/client/v;->e:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/library/api/ap;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v2, v4, Lcom/twitter/library/client/v;->g:Ljava/lang/String;

    iget-object v0, v4, Lcom/twitter/library/client/v;->f:Ljava/lang/String;

    iget-object v4, v4, Lcom/twitter/library/client/v;->d:Ljava/lang/String;

    move-object v5, v4

    move-object v4, v3

    move-object v3, v2

    move-object v2, v0

    move-object v0, v1

    :cond_0
    :goto_0
    invoke-direct {p0, v5}, Lcom/twitter/android/ProfileFragment;->f(Ljava/lang/String;)V

    invoke-direct {p0, v7}, Lcom/twitter/android/ProfileFragment;->c(Ljava/lang/String;)V

    iget-boolean v5, v6, Lcom/twitter/library/api/TwitterUser;->verified:Z

    iget-boolean v7, v6, Lcom/twitter/library/api/TwitterUser;->isProtected:Z

    iget-boolean v6, v6, Lcom/twitter/library/api/TwitterUser;->isTranslator:Z

    invoke-direct {p0, v5, v7, v6}, Lcom/twitter/android/ProfileFragment;->a(ZZZ)V

    invoke-direct {p0, v4, v1}, Lcom/twitter/android/ProfileFragment;->a(Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;)V

    invoke-direct {p0, v3}, Lcom/twitter/android/ProfileFragment;->b(Ljava/lang/String;)V

    invoke-direct {p0, v2, v0}, Lcom/twitter/android/ProfileFragment;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/qt;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/qt;

    invoke-virtual {v0}, Lcom/twitter/android/qt;->notifyDataSetChanged()V

    :cond_1
    return-void

    :cond_2
    iget-object v4, v6, Lcom/twitter/library/api/TwitterUser;->profileDescription:Ljava/lang/String;

    iget-object v3, v6, Lcom/twitter/library/api/TwitterUser;->location:Ljava/lang/String;

    iget-object v2, v6, Lcom/twitter/library/api/TwitterUser;->profileUrl:Ljava/lang/String;

    iget-object v5, v6, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    iget-object v0, v6, Lcom/twitter/library/api/TwitterUser;->urlEntities:Lcom/twitter/library/api/TweetEntities;

    if-eqz v0, :cond_3

    iget-object v8, v0, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    if-eqz v8, :cond_3

    iget-object v8, v0, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_3

    iget-object v0, v0, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    const/4 v8, 0x0

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/UrlEntity;

    iget-object v0, v0, Lcom/twitter/library/api/UrlEntity;->displayUrl:Ljava/lang/String;

    :goto_1
    iget-object v8, v6, Lcom/twitter/library/api/TwitterUser;->descriptionEntities:Lcom/twitter/library/api/TweetEntities;

    if-eqz v8, :cond_0

    iget-object v1, v6, Lcom/twitter/library/api/TwitterUser;->descriptionEntities:Lcom/twitter/library/api/TweetEntities;

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method static synthetic K(Lcom/twitter/android/ProfileFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->aO:Z

    return v0
.end method

.method static synthetic L(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method private L()V
    .locals 3

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->F:I

    and-int/lit8 v0, v0, 0x1

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/android/ProfileFragment;->I:J

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/client/c;->a(J)Ljava/lang/String;

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->F:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->F:I

    :cond_0
    return-void
.end method

.method static synthetic M(Lcom/twitter/android/ProfileFragment;)Landroid/view/animation/TranslateAnimation;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->az:Landroid/view/animation/TranslateAnimation;

    return-object v0
.end method

.method private M()V
    .locals 2

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->F:I

    and-int/lit8 v0, v0, 0x20

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->d(Ljava/lang/String;)V

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->F:I

    or-int/lit8 v0, v0, 0x20

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->F:I

    :cond_0
    return-void
.end method

.method static synthetic N(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private N()Z
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/android/ProfileFragment;->D:Z

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v0

    iget-boolean v0, v0, Lcom/twitter/library/api/TwitterUser;->isProtected:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic O(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method private O()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->D:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    iget-boolean v0, v0, Lcom/twitter/library/api/TwitterUser;->isProtected:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private Q()V
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->au()Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    new-instance v1, Lio;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lio;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    iget-wide v2, p0, Lcom/twitter/android/ProfileFragment;->I:J

    invoke-virtual {v1, v2, v3}, Lio;->a(J)Lio;

    move-result-object v0

    const/16 v1, 0xd

    invoke-virtual {p0, v0, v1, v4}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/library/service/b;II)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    const/16 v1, 0x4000

    invoke-static {v0, v1}, Lcom/twitter/library/provider/ay;->b(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    const/4 v0, 0x1

    const v1, 0x7f0f019f    # com.twitter.android.R.string.follow

    invoke-virtual {p0, v0, v1, v4}, Lcom/twitter/android/ProfileFragment;->a(ZII)V

    :cond_0
    return-void
.end method

.method private R()V
    .locals 8

    const/4 v7, 0x1

    iget-wide v2, p0, Lcom/twitter/android/ProfileFragment;->I:J

    iget-object v4, p0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/library/api/PromotedContent;

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-static {v0, v7}, Lcom/twitter/library/provider/ay;->b(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->aj:Lcom/twitter/library/util/FriendshipCache;

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    iget-wide v5, v1, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-virtual {v0, v5, v6}, Lcom/twitter/library/util/FriendshipCache;->e(J)V

    const v0, 0x7f0f019f    # com.twitter.android.R.string.follow

    invoke-virtual {p0, v7, v0, v7}, Lcom/twitter/android/ProfileFragment;->a(ZII)V

    invoke-virtual {p0, v7}, Lcom/twitter/android/ProfileFragment;->d(Z)V

    const/4 v0, -0x1

    invoke-direct {p0, v2, v3, v4, v0}, Lcom/twitter/android/ProfileFragment;->a(JLcom/twitter/library/api/PromotedContent;I)V

    const-string/jumbo v1, "profile:profile:::unfollow"

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileFragment;->c(Lcom/twitter/library/api/TwitterUser;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/twitter/android/ProfileFragment;->aM:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ProfileFragment;->a(Ljava/lang/String;JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;)V

    return-void
.end method

.method private S()V
    .locals 7

    const/4 v3, 0x1

    const/4 v5, 0x0

    iget-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->aQ:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    const/16 v1, 0x800

    invoke-static {v0, v1}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->aj:Lcom/twitter/library/util/FriendshipCache;

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    iget-wide v1, v1, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/util/FriendshipCache;->d(J)V

    new-instance v0, Lin;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lin;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-virtual {v0, v3}, Lin;->c(I)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "user_id"

    iget-object v3, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    iget-wide v3, v3, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    invoke-static {}, Lgl;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "device_follow"

    invoke-virtual {v0, v1, v5}, Lin;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/b;

    :cond_0
    const/16 v1, 0x8

    invoke-virtual {p0, v0, v1, v5}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/library/service/b;II)Z

    :goto_0
    invoke-virtual {p0, v5}, Lcom/twitter/android/ProfileFragment;->d(Z)V

    const-string/jumbo v1, "profile:profile:::favorite_user"

    iget-wide v2, p0, Lcom/twitter/android/ProfileFragment;->I:J

    iget-object v4, p0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/library/api/PromotedContent;

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileFragment;->c(Lcom/twitter/library/api/TwitterUser;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/twitter/android/ProfileFragment;->aM:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ProfileFragment;->a(Ljava/lang/String;JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;)V

    return-void

    :cond_1
    iget v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    const/16 v1, 0x10

    invoke-static {v0, v1}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/api/TwitterUser;Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private T()V
    .locals 7

    const/4 v5, 0x0

    iget-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->aQ:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    const/16 v1, 0x800

    invoke-static {v0, v1}, Lcom/twitter/library/provider/ay;->b(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->aj:Lcom/twitter/library/util/FriendshipCache;

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    iget-wide v1, v1, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/util/FriendshipCache;->g(J)V

    new-instance v0, Lin;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lin;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lin;->c(I)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "user_id"

    iget-object v3, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    iget-wide v3, v3, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    const/16 v1, 0x9

    invoke-virtual {p0, v0, v1, v5}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/library/service/b;II)Z

    :goto_0
    invoke-virtual {p0, v5}, Lcom/twitter/android/ProfileFragment;->d(Z)V

    const-string/jumbo v1, "profile:profile:::unfavorite_user"

    iget-wide v2, p0, Lcom/twitter/android/ProfileFragment;->I:J

    iget-object v4, p0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/library/api/PromotedContent;

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileFragment;->c(Lcom/twitter/library/api/TwitterUser;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/twitter/android/ProfileFragment;->aM:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ProfileFragment;->a(Ljava/lang/String;JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;)V

    return-void

    :cond_0
    iget v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    const/16 v1, 0x10

    invoke-static {v0, v1}, Lcom/twitter/library/provider/ay;->b(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {v0, v1, v2, v5}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/api/TwitterUser;Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)J
    .locals 4

    new-instance v0, Lcom/twitter/library/client/f;

    const-string/jumbo v1, "profile"

    invoke-direct {v0, p0, p1, v1}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "ht"

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method private a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 10

    const/16 v9, 0xd

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->aQ:Z

    if-eqz v0, :cond_0

    iget-wide v3, p0, Lcom/twitter/android/ProfileFragment;->Q:J

    iget-wide v5, p0, Lcom/twitter/android/ProfileFragment;->I:J

    cmp-long v0, v3, v5

    if-nez v0, :cond_0

    const-class v0, Lcom/twitter/android/TabbedFollowingsActivity;

    :goto_0
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3, p1, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v0, "com.twitter.android.intent.action.FOLLOW"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v3, "owner_id"

    iget-wide v4, p0, Lcom/twitter/android/ProfileFragment;->I:J

    invoke-virtual {v0, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v3, "type"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v3, "owner_name"

    iget-object v4, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    iget-object v4, v4, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v3, "fetch_always"

    invoke-virtual {v0, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v4, "cluster_follow"

    iget-wide v5, p0, Lcom/twitter/android/ProfileFragment;->Q:J

    iget-wide v7, p0, Lcom/twitter/android/ProfileFragment;->I:J

    cmp-long v0, v5, v7

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, v9}, Lcom/twitter/android/util/c;->a(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "cluster_follow_experiment"

    invoke-virtual {v0, v1, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    :cond_0
    const-class v0, Lcom/twitter/android/UsersActivity;

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1
.end method

.method static synthetic a(Lcom/twitter/android/ProfileFragment;Landroid/view/animation/TranslateAnimation;)Landroid/view/animation/TranslateAnimation;
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/ProfileFragment;->az:Landroid/view/animation/TranslateAnimation;

    return-object p1
.end method

.method static synthetic a(Lcom/twitter/android/ProfileFragment;Lcom/twitter/library/api/TwitterUser;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1}, Lcom/twitter/android/ProfileFragment;->c(Lcom/twitter/library/api/TwitterUser;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(JIZ)Ljava/util/HashMap;
    .locals 3

    const/4 v1, 0x0

    const/16 v0, 0xa

    if-ne p3, v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->af:Ljava/util/HashMap;

    if-nez v0, :cond_5

    if-eqz p4, :cond_1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->af:Ljava/util/HashMap;

    move-object v1, v0

    :goto_0
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    if-nez v0, :cond_0

    if-eqz p4, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    :goto_1
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_1

    :cond_2
    const/16 v0, 0x14

    if-ne p3, v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->ag:Ljava/util/HashMap;

    if-nez v0, :cond_3

    if-eqz p4, :cond_3

    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x6

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->ag:Ljava/util/HashMap;

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->ag:Ljava/util/HashMap;

    goto :goto_1

    :cond_4
    move-object v0, v1

    goto :goto_1

    :cond_5
    move-object v1, v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/ProfileFragment;)Ljava/util/HashSet;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->Z:Ljava/util/HashSet;

    return-object v0
.end method

.method private a(JLcom/twitter/library/api/PromotedContent;)V
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/twitter/android/client/c;->b(JLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->d(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->ad:Ljava/util/HashMap;

    if-nez v1, :cond_0

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/twitter/android/ProfileFragment;->ad:Ljava/util/HashMap;

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->ad:Ljava/util/HashMap;

    iget v2, p0, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private a(JLcom/twitter/library/api/PromotedContent;I)V
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/twitter/android/client/c;->a(JLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->d(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->ae:Ljava/util/HashMap;

    if-nez v1, :cond_0

    new-instance v1, Ljava/util/HashMap;

    const/4 v2, 0x6

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(I)V

    iput-object v1, p0, Lcom/twitter/android/ProfileFragment;->ae:Ljava/util/HashMap;

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->ae:Ljava/util/HashMap;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private a(JZLcom/twitter/library/api/PromotedContent;I)V
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/twitter/android/client/c;->a(JZLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->d(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->ac:Ljava/util/HashMap;

    if-nez v1, :cond_0

    new-instance v1, Ljava/util/HashMap;

    const/4 v2, 0x6

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(I)V

    iput-object v1, p0, Lcom/twitter/android/ProfileFragment;->ac:Ljava/util/HashMap;

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->ac:Ljava/util/HashMap;

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private a(Landroid/content/Context;J)V
    .locals 5

    const v4, 0x7f0f032a    # com.twitter.android.R.string.profile_tab_title_favorites

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {p0, p1}, Lcom/twitter/android/ProfileFragment;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/to;

    const v3, 0x7f0f0326    # com.twitter.android.R.string.profile_friends

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Lcom/twitter/android/to;-><init>(Ljava/lang/String;Landroid/content/Intent;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0, p1}, Lcom/twitter/android/ProfileFragment;->b(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/to;

    const v3, 0x7f0f0325    # com.twitter.android.R.string.profile_followers

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Lcom/twitter/android/to;-><init>(Ljava/lang/String;Landroid/content/Intent;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->O()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/UsersActivity;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "type"

    const/16 v3, 0x12

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/to;

    const v3, 0x7f0f01a3    # com.twitter.android.R.string.follow_requests_title

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Lcom/twitter/android/to;-><init>(Ljava/lang/String;Landroid/content/Intent;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/TimelineActivity;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "owner_id"

    invoke-virtual {v1, v2, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "type"

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "title"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/to;

    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Lcom/twitter/android/to;-><init>(Ljava/lang/String;Landroid/content/Intent;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/ListsActivity;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "owner_id"

    invoke-virtual {v1, v2, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "profile"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/to;

    const v3, 0x7f0f032e    # com.twitter.android.R.string.profile_tab_title_lists_owned_by

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Lcom/twitter/android/to;-><init>(Ljava/lang/String;Landroid/content/Intent;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-boolean v1, p0, Lcom/twitter/android/ProfileFragment;->D:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    iget-boolean v1, v1, Lcom/twitter/library/api/TwitterUser;->hasCollections:Z

    if-eqz v1, :cond_2

    :cond_1
    invoke-static {}, Lgj;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/ProfileCollectionsListActivity;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "user_id"

    invoke-virtual {v1, v2, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/to;

    const v3, 0x7f0f0328    # com.twitter.android.R.string.profile_tab_title_collections_owned_by

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v1}, Lcom/twitter/android/to;-><init>(Ljava/lang/String;Landroid/content/Intent;)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->as:Lcom/twitter/android/tn;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Lcom/twitter/android/to;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/android/to;

    invoke-virtual {v1, v0}, Lcom/twitter/android/tn;->a([Lcom/twitter/android/to;)V

    return-void
.end method

.method private a(Landroid/view/View;IZ)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-ne v0, p2, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/animation/Animation;->reset()V

    invoke-virtual {v0}, Landroid/view/animation/Animation;->cancel()V

    invoke-virtual {p1}, Landroid/view/View;->clearAnimation()V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setEnabled(Z)V

    :cond_1
    if-eqz p3, :cond_3

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-nez p2, :cond_2

    const v0, 0x7f04000f    # com.twitter.android.R.anim.quick_fade_in

    :goto_1
    invoke-static {v1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/qo;

    invoke-direct {v1, p0, p1, p2}, Lcom/twitter/android/qo;-><init>(Lcom/twitter/android/ProfileFragment;Landroid/view/View;I)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    :cond_2
    const v0, 0x7f040010    # com.twitter.android.R.anim.quick_fade_out

    goto :goto_1

    :cond_3
    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(Landroid/view/View;JLcom/twitter/android/qz;Lcom/twitter/android/widget/cg;III)V
    .locals 9

    invoke-virtual/range {p5 .. p7}, Lcom/twitter/android/widget/cg;->a(II)I

    move-result v1

    invoke-virtual {p4, v1}, Lcom/twitter/android/qz;->b(I)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p4, v1}, Lcom/twitter/android/qz;->c(I)Z

    move-result v2

    if-nez v2, :cond_6

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "user_id"

    invoke-virtual {v1, v2, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "type"

    move/from16 v0, p8

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v8

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->aj:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v1, p2, p3}, Lcom/twitter/library/util/FriendshipCache;->h(J)Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_2

    const-string/jumbo v2, "friendship"

    invoke-virtual {v8, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    :cond_2
    check-cast p1, Lcom/twitter/library/widget/UserView;

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserView;->getPromotedContent()Lcom/twitter/library/api/PromotedContent;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/api/PromotedEvent;->d:Lcom/twitter/library/api/PromotedEvent;

    invoke-virtual {v1, v2, v5}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/library/api/PromotedContent;)V

    const-string/jumbo v1, "pc"

    invoke-virtual {v8, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    :cond_3
    const/16 v1, 0xa

    move/from16 v0, p8

    if-ne v0, v1, :cond_5

    const-string/jumbo v1, "association"

    new-instance v2, Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {v2}, Lcom/twitter/library/scribe/ScribeAssociation;-><init>()V

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeAssociation;->a(I)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    iget-wide v3, v3, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeAssociation;->a(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v2

    const-string/jumbo v3, "profile"

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v2

    const-string/jumbo v3, "similar_to"

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string/jumbo v2, "profile:similar_to::user:profile_click"

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserView;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/zx;

    iget-object v6, v1, Lcom/twitter/android/zx;->f:Ljava/lang/String;

    iget-object v7, p0, Lcom/twitter/android/ProfileFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object v1, p0

    move-wide v3, p2

    invoke-direct/range {v1 .. v7}, Lcom/twitter/android/ProfileFragment;->a(Ljava/lang/String;JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;)V

    :cond_4
    :goto_1
    const/4 v1, 0x2

    invoke-virtual {p0, v8, v1}, Lcom/twitter/android/ProfileFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :cond_5
    const/16 v1, 0x14

    move/from16 v0, p8

    if-ne v0, v1, :cond_4

    const-string/jumbo v2, "profile:user_similarities_list::user:profile_click"

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserView;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/zx;

    iget-object v6, v1, Lcom/twitter/android/zx;->f:Ljava/lang/String;

    iget-object v7, p0, Lcom/twitter/android/ProfileFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object v1, p0

    move-wide v3, p2

    invoke-direct/range {v1 .. v7}, Lcom/twitter/android/ProfileFragment;->a(Ljava/lang/String;JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;)V

    goto :goto_1

    :cond_6
    invoke-virtual {p4, v1}, Lcom/twitter/android/qz;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    if-eqz v1, :cond_0

    invoke-virtual {p0, v1}, Lcom/twitter/android/ProfileFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method private a(Landroid/view/ViewGroup;ILjava/lang/String;I)V
    .locals 3

    invoke-virtual {p1, p2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090097    # com.twitter.android.R.id.name

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f09024a    # com.twitter.android.R.id.value

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, p4}, Lcom/twitter/library/util/Util;->a(Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private a(Landroid/widget/TextView;Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;)V
    .locals 4

    const/4 v3, 0x0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->aE()V

    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->aF()V

    return-void

    :cond_0
    if-eqz p3, :cond_2

    iget-object v0, p3, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    iget-object v0, p3, Lcom/twitter/library/api/TweetEntities;->mentions:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->W:Lcom/twitter/library/view/c;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b007e    # com.twitter.android.R.color.white

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-static {p2, p3, v0, v1, v3}, Lcom/twitter/library/view/d;->a(Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/view/c;II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {p1}, Lcom/twitter/library/view/l;->a(Landroid/widget/TextView;)V

    :goto_1
    invoke-virtual {p1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/twitter/android/ProfileFragment;Ljava/lang/String;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/twitter/android/ProfileFragment;->d(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/ProfileFragment;Ljava/lang/String;JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;)V
    .locals 0

    invoke-direct/range {p0 .. p6}, Lcom/twitter/android/ProfileFragment;->a(Ljava/lang/String;JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;)V

    return-void
.end method

.method private a(Lcom/twitter/library/client/Session;I)V
    .locals 12

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, p2, v2}, Lcom/twitter/android/ProfileFragment;->a(JIZ)Ljava/util/HashMap;

    move-result-object v10

    if-eqz v10, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/android/ProfileFragment;->I:J

    invoke-virtual {v10}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    const-wide/16 v5, -0x1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    const/4 v9, 0x0

    move-object v1, p1

    move v4, p2

    invoke-virtual/range {v0 .. v9}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;JIJJLjava/lang/Integer;)Ljava/lang/String;

    goto :goto_0

    :cond_0
    invoke-virtual {v10}, Ljava/util/HashMap;->clear()V

    :cond_1
    return-void
.end method

.method private a(Ljava/lang/String;JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;)V
    .locals 8

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/ProfileFragment;->a(Ljava/lang/String;JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;Lcom/twitter/library/scribe/ScribeItem;)V

    return-void
.end method

.method private a(Ljava/lang/String;JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;Lcom/twitter/library/scribe/ScribeItem;)V
    .locals 4

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v1, p2, p3, p4, p5}, Lcom/twitter/library/scribe/ScribeLog;->a(JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v1, p6}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/android/ProfileFragment;->I:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->i(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v1, p7}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method

.method private a(Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/ProfileFragment;->z:Ljava/lang/String;

    iput-object p2, p0, Lcom/twitter/android/ProfileFragment;->aA:Lcom/twitter/library/api/TweetEntities;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->C()V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/ProfileFragment;->A:Ljava/lang/String;

    if-eqz p2, :cond_0

    iput-object p2, p0, Lcom/twitter/android/ProfileFragment;->at:Ljava/lang/String;

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->v()V

    return-void

    :cond_0
    iput-object p1, p0, Lcom/twitter/android/ProfileFragment;->at:Ljava/lang/String;

    goto :goto_0
.end method

.method private a(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 4

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/util/ArrayList;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/android/ProfileFragment;->I:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->i(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    :cond_0
    return-void
.end method

.method private a(ZZZ)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/ProfileFragment;->aE:Z

    iput-boolean p2, p0, Lcom/twitter/android/ProfileFragment;->aF:Z

    iput-boolean p3, p0, Lcom/twitter/android/ProfileFragment;->aG:Z

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->w_()V

    return-void
.end method

.method private aC()V
    .locals 6

    const/4 v5, 0x0

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    const/16 v1, 0x10

    invoke-static {v0, v1}, Lcom/twitter/library/provider/ay;->b(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    iget-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->aQ:Z

    if-eqz v0, :cond_0

    new-instance v0, Lin;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lin;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lin;->c(I)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "user_id"

    iget-object v3, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    iget-wide v3, v3, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "device_follow"

    iget v3, p0, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-static {v3}, Lcom/twitter/library/provider/ay;->h(I)Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "email_follow"

    iget v3, p0, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-static {v3}, Lcom/twitter/library/provider/ay;->m(I)Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/b;

    const/16 v1, 0xa

    invoke-virtual {p0, v0, v1, v5}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/library/service/b;II)Z

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {v0, v1, v2, v5}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/api/TwitterUser;Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private aD()V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    const-string/jumbo v1, "_normal."

    const-string/jumbo v2, "."

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/twitter/android/ImageActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "image_url"

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "android.intent.extra.TEXT"

    iget-object v2, p0, Lcom/twitter/android/ProfileFragment;->B:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method private aE()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->g:Lcom/twitter/android/widget/PipView;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->z:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->C:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->A:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PipView;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PipView;->setVisibility(I)V

    goto :goto_0
.end method

.method private aF()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->v:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->C:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->A:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->v:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->v:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private b(Landroid/content/Context;)Landroid/content/Intent;
    .locals 6

    const/4 v5, 0x1

    iget-wide v0, p0, Lcom/twitter/android/ProfileFragment;->Q:J

    iget-wide v2, p0, Lcom/twitter/android/ProfileFragment;->I:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/16 v0, 0xf

    :goto_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/UsersActivity;

    invoke-direct {v1, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "com.twitter.android.intent.action.FOLLOW"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "owner_id"

    iget-wide v3, p0, Lcom/twitter/android/ProfileFragment;->I:J

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "type"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "owner_name"

    iget-object v3, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    iget-object v3, v3, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "fetch_always"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "cluster_follow"

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/twitter/android/util/c;->a(Landroid/content/Context;I)Z

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "cluster_follow_experiment"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0

    :cond_0
    const/16 v0, 0xe

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/ProfileFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/ProfileFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/library/scribe/ScribeAssociation;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    return-object v0
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;)V
    .locals 4

    new-instance v0, Lcom/twitter/library/client/f;

    const-string/jumbo v1, "profile"

    invoke-direct {v0, p0, p1, v1}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v0

    const-string/jumbo v1, "ht"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;J)Lcom/twitter/library/client/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->d()V

    return-void
.end method

.method private b(Lcom/twitter/library/api/TwitterUser;)V
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/TwitterUser;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->d(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->ad:Ljava/util/HashMap;

    if-nez v1, :cond_0

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/twitter/android/ProfileFragment;->ad:Ljava/util/HashMap;

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->ad:Ljava/util/HashMap;

    iget v2, p0, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/ProfileFragment;->C:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->u()V

    return-void
.end method

.method private c(Landroid/content/Context;)Lcom/twitter/android/tn;
    .locals 5

    const/4 v4, 0x1

    new-instance v0, Lcom/twitter/android/to;

    const v1, 0x7f0f0128    # com.twitter.android.R.string.drafts

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/twitter/android/DraftsActivity;

    invoke-direct {v2, p1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v3, "return_to_drafts"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/to;-><init>(Ljava/lang/String;Landroid/content/Intent;)V

    new-instance v1, Lcom/twitter/android/tn;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v2

    new-array v3, v4, [Lcom/twitter/android/to;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const v0, 0x7f030136    # com.twitter.android.R.layout.section_simple_row_view

    invoke-direct {v1, v2, v3, v0}, Lcom/twitter/android/tn;-><init>(Lcom/twitter/android/client/c;[Lcom/twitter/android/to;I)V

    return-object v1
.end method

.method private c(Lcom/twitter/library/api/TwitterUser;)Ljava/lang/String;
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/api/TwitterUser;->f()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/twitter/android/ProfileFragment;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->Y:Ljava/util/ArrayList;

    return-object v0
.end method

.method public static c(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    new-instance v0, Lcom/twitter/library/client/f;

    const-string/jumbo v1, "profile"

    invoke-direct {v0, p0, p1, v1}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v0

    const-string/jumbo v1, "ht"

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/f;->c(Ljava/lang/String;)Lcom/twitter/library/client/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->d()V

    return-void
.end method

.method static synthetic c(Lcom/twitter/android/ProfileFragment;Ljava/lang/String;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/twitter/android/ProfileFragment;->d(Ljava/lang/String;)V

    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/ProfileFragment;->B:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->y()V

    return-void
.end method

.method static synthetic d(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/ProfileFragment;Ljava/lang/String;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/twitter/android/ProfileFragment;->d(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic e(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/ProfileFragment;Ljava/lang/String;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/twitter/android/ProfileFragment;->d(Ljava/lang/String;)V

    return-void
.end method

.method private e(Z)V
    .locals 2

    iget-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->L:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-static {v0}, Lcom/twitter/library/provider/ay;->d(I)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->aw:Landroid/widget/ImageButton;

    invoke-direct {p0, v1, v0, p1}, Lcom/twitter/android/ProfileFragment;->a(Landroid/view/View;IZ)V

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method static synthetic f(Lcom/twitter/android/ProfileFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/ProfileFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method private f(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/ProfileFragment;->au:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->z()V

    return-void
.end method

.method private f(Z)V
    .locals 6

    const/4 v5, 0x5

    const/4 v4, 0x4

    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v1, 0x0

    if-eqz p1, :cond_4

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->aj:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v0}, Lcom/twitter/library/util/FriendshipCache;->b()V

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->ac:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->ac:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->ae:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->ae:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v2, v1, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v3, v1, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->af()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v4, v1, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :cond_2
    iget-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->aO:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->D:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v5, v1, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :cond_3
    :goto_0
    return-void

    :cond_4
    iget v0, p0, Lcom/twitter/android/ProfileFragment;->F:I

    and-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_5

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v2, v1, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->F:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->F:I

    :cond_5
    iget v0, p0, Lcom/twitter/android/ProfileFragment;->F:I

    and-int/lit8 v0, v0, 0x4

    if-nez v0, :cond_6

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v3, v1, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->F:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->F:I

    :cond_6
    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->af()Z

    move-result v0

    if-nez v0, :cond_7

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->F:I

    and-int/lit8 v0, v0, 0x8

    if-nez v0, :cond_7

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v4, v1, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->F:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->F:I

    :cond_7
    iget-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->aO:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->D:Z

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->F:I

    and-int/lit8 v0, v0, 0x10

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v5, v1, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->F:I

    or-int/lit8 v0, v0, 0x10

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->F:I

    goto :goto_0
.end method

.method static synthetic g(Lcom/twitter/android/ProfileFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/ProfileFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method private g(I)Lcom/twitter/library/widget/a;
    .locals 1

    new-instance v0, Lcom/twitter/android/qi;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/qi;-><init>(Lcom/twitter/android/ProfileFragment;I)V

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/android/ProfileFragment;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->au:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/android/ProfileFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/ProfileFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/android/ProfileFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/ProfileFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic j(Lcom/twitter/android/ProfileFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/ProfileFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic j(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic k(Lcom/twitter/android/ProfileFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/ProfileFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic k(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic l(Lcom/twitter/android/ProfileFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/ProfileFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method private l(I)Lcom/twitter/android/ob;
    .locals 1

    new-instance v0, Lcom/twitter/android/qj;

    invoke-direct {v0, p0, p1}, Lcom/twitter/android/qj;-><init>(Lcom/twitter/android/ProfileFragment;I)V

    return-object v0
.end method

.method static synthetic l(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/library/scribe/ScribeAssociation;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->aM:Lcom/twitter/library/scribe/ScribeAssociation;

    return-object v0
.end method

.method static synthetic m(Lcom/twitter/android/ProfileFragment;)J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/android/ProfileFragment;->Q:J

    return-wide v0
.end method

.method static synthetic m(Lcom/twitter/android/ProfileFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/ProfileFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method private m(I)V
    .locals 3

    const/4 v2, 0x0

    if-lez p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->am:Landroid/widget/Button;

    const v1, 0x7f020209    # com.twitter.android.R.drawable.ic_profile_messages_new

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->am:Landroid/widget/Button;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->am:Landroid/widget/Button;

    const v1, 0x7f020208    # com.twitter.android.R.drawable.ic_profile_messages

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->am:Landroid/widget/Button;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method static synthetic n(Lcom/twitter/android/ProfileFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/ProfileFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic n(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method private n(I)V
    .locals 1

    iput p1, p0, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->D()V

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/qt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/qt;

    invoke-virtual {v0}, Lcom/twitter/android/qt;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method static synthetic o(Lcom/twitter/android/ProfileFragment;)J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/android/ProfileFragment;->Q:J

    return-wide v0
.end method

.method static synthetic o(Lcom/twitter/android/ProfileFragment;Ljava/lang/String;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/twitter/android/ProfileFragment;->d(Ljava/lang/String;)V

    return-void
.end method

.method private o(I)Z
    .locals 1

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->E:I

    and-int/2addr v0, p1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic p(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method private p(I)Z
    .locals 1

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->G:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic q(Lcom/twitter/android/ProfileFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->J()V

    return-void
.end method

.method static synthetic r(Lcom/twitter/android/ProfileFragment;)J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/android/ProfileFragment;->Q:J

    return-wide v0
.end method

.method static synthetic s(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic t(Lcom/twitter/android/ProfileFragment;)J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/android/ProfileFragment;->Q:J

    return-wide v0
.end method

.method static synthetic u(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic v(Lcom/twitter/android/ProfileFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->I()V

    return-void
.end method

.method static synthetic w(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic x(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/library/scribe/ScribeAssociation;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    return-object v0
.end method

.method static synthetic y(Lcom/twitter/android/ProfileFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->aD()V

    return-void
.end method

.method static synthetic z(Lcom/twitter/android/ProfileFragment;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->ac:Ljava/util/HashMap;

    return-object v0
.end method


# virtual methods
.method C()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->s:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->z:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/ProfileFragment;->aA:Lcom/twitter/library/api/TweetEntities;

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/ProfileFragment;->a(Landroid/widget/TextView;Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;)V

    :cond_0
    return-void
.end method

.method D()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->r:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-static {v0}, Lcom/twitter/library/provider/ay;->c(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->r:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->r:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method a(JJI)Ljava/lang/Long;
    .locals 2

    const/16 v0, 0xa

    if-ne p5, v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->n:Lcom/twitter/android/qz;

    invoke-virtual {v0}, Lcom/twitter/android/qz;->d()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/rh;

    invoke-virtual {v0, p3, p4}, Lcom/twitter/android/rh;->c(J)Ljava/lang/Long;

    :cond_0
    :goto_0
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p5, v0}, Lcom/twitter/android/ProfileFragment;->a(JIZ)Ljava/util/HashMap;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    :goto_1
    return-object v0

    :cond_1
    const/16 v0, 0x14

    if-ne p5, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->l:Lcom/twitter/android/qz;

    invoke-virtual {v0}, Lcom/twitter/android/qz;->d()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/rh;

    invoke-virtual {v0, p3, p4}, Lcom/twitter/android/rh;->c(J)Ljava/lang/Long;

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method a(JJJI)V
    .locals 3

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p7, v0}, Lcom/twitter/android/ProfileFragment;->a(JIZ)Ljava/util/HashMap;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public a(Landroid/content/Context;IILcom/twitter/library/service/b;)V
    .locals 11

    const v10, 0x7f0f00ee    # com.twitter.android.R.string.default_error_message

    const/4 v6, 0x2

    const/16 v4, 0x2000

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p4, Lcom/twitter/library/service/b;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    invoke-virtual {p4}, Lcom/twitter/library/service/b;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    packed-switch p2, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-wide v4, p0, Lcom/twitter/android/ProfileFragment;->Q:J

    invoke-static {v3, v4, v5, v1}, Lcom/twitter/android/util/AppMetrics;->a(Landroid/content/Context;JI)V

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->c()I

    move-result v0

    const/16 v3, 0x191

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->m:Lcom/twitter/android/rb;

    invoke-virtual {v0}, Lcom/twitter/android/rb;->d()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ye;

    invoke-virtual {v0, v1}, Lcom/twitter/android/ye;->c(I)V

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->m:Lcom/twitter/android/rb;

    invoke-virtual {v0, v2}, Lcom/twitter/android/rb;->a(Z)V

    goto :goto_0

    :cond_2
    check-cast p4, Ljs;

    invoke-virtual {p4}, Ljs;->e()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->m:Lcom/twitter/android/rb;

    invoke-virtual {v0}, Lcom/twitter/android/rb;->d()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ye;

    invoke-virtual {v0}, Lcom/twitter/android/ye;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0, v6}, Lcom/twitter/android/ye;->c(I)V

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->m:Lcom/twitter/android/rb;

    invoke-virtual {v0, v2}, Lcom/twitter/android/rb;->a(Z)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/android/ProfileFragment;->Q:J

    invoke-static {v1, v2, v3, v6}, Lcom/twitter/android/util/AppMetrics;->a(Landroid/content/Context;JI)V

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->w:Lcom/twitter/android/ud;

    invoke-virtual {v0}, Lcom/twitter/android/ud;->notifyDataSetChanged()V

    goto :goto_0

    :pswitch_3
    iget-object v3, p4, Lcom/twitter/library/service/b;->k:Landroid/os/Bundle;

    const-string/jumbo v4, "friendship"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v5

    invoke-virtual {v5}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v5

    invoke-static {p1, v5}, Lcom/twitter/library/platform/PushService;->f(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    iput v4, p0, Lcom/twitter/android/ProfileFragment;->H:I

    iget-object v7, p0, Lcom/twitter/android/ProfileFragment;->aj:Lcom/twitter/library/util/FriendshipCache;

    iget-wide v8, p0, Lcom/twitter/android/ProfileFragment;->I:J

    invoke-virtual {v7, v8, v9, v4}, Lcom/twitter/library/util/FriendshipCache;->b(JI)V

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    if-nez v6, :cond_0

    :cond_4
    const-string/jumbo v0, "notification_not_enabled"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    if-nez v6, :cond_7

    :cond_5
    invoke-static {p1}, Lcom/twitter/android/client/aw;->a(Landroid/content/Context;)Lcom/twitter/android/client/aw;

    move-result-object v3

    if-nez v6, :cond_6

    move v0, v1

    :goto_1
    invoke-virtual {v3, v5, v0}, Lcom/twitter/android/client/aw;->a(Ljava/lang/String;Z)V

    goto/16 :goto_0

    :cond_6
    move v0, v2

    goto :goto_1

    :cond_7
    invoke-static {p1, v10, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0, v2}, Lcom/twitter/android/ProfileFragment;->d(Z)V

    goto/16 :goto_0

    :pswitch_4
    iget-object v3, p4, Lcom/twitter/library/service/b;->k:Landroid/os/Bundle;

    const-string/jumbo v4, "friendship"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iget-object v4, p0, Lcom/twitter/android/ProfileFragment;->aj:Lcom/twitter/library/util/FriendshipCache;

    iget-wide v5, p0, Lcom/twitter/android/ProfileFragment;->I:J

    invoke-virtual {v4, v5, v6, v3}, Lcom/twitter/library/util/FriendshipCache;->b(JI)V

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1, v10, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0, v2}, Lcom/twitter/android/ProfileFragment;->d(Z)V

    goto/16 :goto_0

    :pswitch_5
    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v3

    if-nez v3, :cond_8

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-static {v0, v4}, Lcom/twitter/library/provider/ay;->b(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-direct {p0, v2}, Lcom/twitter/android/ProfileFragment;->e(Z)V

    invoke-virtual {p0, v1}, Lcom/twitter/android/ProfileFragment;->d(Z)V

    invoke-static {p1}, Lcom/twitter/android/util/af;->a(Landroid/content/Context;)V

    goto/16 :goto_0

    :cond_8
    iget v1, p0, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-static {v1, v4}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/ProfileFragment;->H:I

    iget-object v0, v0, Lcom/twitter/library/service/e;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "muted_username"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/twitter/android/util/af;->b(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_6
    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v3

    if-nez v3, :cond_9

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-static {v0, v4}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-direct {p0, v2}, Lcom/twitter/android/ProfileFragment;->e(Z)V

    invoke-virtual {p0, v1}, Lcom/twitter/android/ProfileFragment;->d(Z)V

    invoke-static {p1}, Lcom/twitter/android/util/af;->b(Landroid/content/Context;)V

    goto/16 :goto_0

    :cond_9
    iget-object v0, v0, Lcom/twitter/library/service/e;->a:Landroid/os/Bundle;

    const-string/jumbo v1, "muted_username"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/twitter/android/util/af;->c(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_7
    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    const/16 v3, 0x4000

    invoke-static {v0, v3}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    const v0, 0x7f0f02f6    # com.twitter.android.R.string.pending

    invoke-virtual {p0, v1, v0, v2}, Lcom/twitter/android/ProfileFragment;->a(ZII)V

    const v0, 0x7f0f0523    # com.twitter.android.R.string.users_cancel_follow_request_error

    invoke-static {p1, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 12

    iget-wide v2, p0, Lcom/twitter/android/ProfileFragment;->I:J

    iget-object v4, p0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/library/api/PromotedContent;

    packed-switch p2, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->Q()V

    goto :goto_0

    :pswitch_2
    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0, v2, v3, v4}, Lcom/twitter/android/client/c;->c(JLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    const-string/jumbo v1, "profile::::block"

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileFragment;->c(Lcom/twitter/library/api/TwitterUser;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/twitter/android/ProfileFragment;->aM:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ProfileFragment;->a(Ljava/lang/String;JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;)V

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    const/4 v0, 0x1

    const v1, 0x7f0f0508    # com.twitter.android.R.string.unblock

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/ProfileFragment;->a(ZII)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->d(Z)V

    goto :goto_0

    :pswitch_3
    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0, v2, v3, v4}, Lcom/twitter/android/client/c;->d(JLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    const-string/jumbo v1, "profile::::unblock"

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileFragment;->c(Lcom/twitter/library/api/TwitterUser;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/twitter/android/ProfileFragment;->aM:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ProfileFragment;->a(Ljava/lang/String;JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;)V

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/twitter/library/provider/ay;->b(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    const/4 v0, 0x1

    const v1, 0x7f0f019f    # com.twitter.android.R.string.follow

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/ProfileFragment;->a(ZII)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->d(Z)V

    goto :goto_0

    :pswitch_4
    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    if-nez v4, :cond_1

    const/4 v8, 0x0

    const/4 v9, 0x0

    :goto_1
    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v5

    const-string/jumbo v10, "spam"

    const/4 v11, 0x1

    move-wide v6, v2

    invoke-virtual/range {v5 .. v11}, Lcom/twitter/android/client/c;->a(JLjava/lang/String;ZLjava/lang/String;Z)Ljava/lang/String;

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    const/4 v0, 0x1

    const v1, 0x7f0f0508    # com.twitter.android.R.string.unblock

    const/4 v5, 0x0

    invoke-virtual {p0, v0, v1, v5}, Lcom/twitter/android/ProfileFragment;->a(ZII)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->d(Z)V

    const-string/jumbo v1, "profile::::report_as_spam"

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileFragment;->c(Lcom/twitter/library/api/TwitterUser;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/twitter/android/ProfileFragment;->aM:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ProfileFragment;->a(Ljava/lang/String;JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;)V

    goto/16 :goto_0

    :cond_1
    iget-object v8, v4, Lcom/twitter/library/api/PromotedContent;->impressionId:Ljava/lang/String;

    invoke-virtual {v4}, Lcom/twitter/library/api/PromotedContent;->b()Z

    move-result v9

    goto :goto_1

    :pswitch_5
    const/4 v0, -0x2

    if-ne p3, v0, :cond_2

    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->R()V

    const-string/jumbo v1, "profile:profile:change_friendship_dialog:unfollow:click"

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileFragment;->c(Lcom/twitter/library/api/TwitterUser;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/twitter/android/ProfileFragment;->aM:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ProfileFragment;->a(Ljava/lang/String;JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;)V

    goto/16 :goto_0

    :cond_2
    const/4 v0, -0x3

    if-ne p3, v0, :cond_3

    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->aC()V

    const-string/jumbo v1, "profile:profile:change_friendship_dialog:device_unfollow:click"

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileFragment;->c(Lcom/twitter/library/api/TwitterUser;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/twitter/android/ProfileFragment;->aM:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ProfileFragment;->a(Ljava/lang/String;JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;)V

    goto/16 :goto_0

    :cond_3
    const-string/jumbo v1, "profile:profile:change_friendship_dialog:cancel:click"

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileFragment;->c(Lcom/twitter/library/api/TwitterUser;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/twitter/android/ProfileFragment;->aM:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ProfileFragment;->a(Ljava/lang/String;JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;)V

    goto/16 :goto_0

    :pswitch_6
    const/4 v0, -0x1

    if-ne p3, v0, :cond_4

    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->R()V

    goto/16 :goto_0

    :cond_4
    const/4 v0, -0x3

    if-ne p3, v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->T()V

    goto/16 :goto_0

    :pswitch_7
    if-nez p3, :cond_5

    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->aD()V

    goto/16 :goto_0

    :cond_5
    const/4 v0, 0x1

    if-ne p3, v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/EditProfileActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/ProfileFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :pswitch_8
    iget-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->aQ:Z

    if-eqz v0, :cond_8

    const/4 v0, -0x2

    if-ne p3, v0, :cond_6

    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->aC()V

    const-string/jumbo v1, "profile:profile:confirm_unfavorite_dialog:device_unfollow:click"

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileFragment;->c(Lcom/twitter/library/api/TwitterUser;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/twitter/android/ProfileFragment;->aM:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ProfileFragment;->a(Ljava/lang/String;JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;)V

    goto/16 :goto_0

    :cond_6
    const/4 v0, -0x3

    if-ne p3, v0, :cond_7

    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->T()V

    const-string/jumbo v1, "profile:profile:confirm_unfavorite_dialog:unfavorite:click"

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileFragment;->c(Lcom/twitter/library/api/TwitterUser;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/twitter/android/ProfileFragment;->aM:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ProfileFragment;->a(Ljava/lang/String;JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;)V

    goto/16 :goto_0

    :cond_7
    const-string/jumbo v1, "profile:profile:confirm_unfavorite_dialog:cancel:click"

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileFragment;->c(Lcom/twitter/library/api/TwitterUser;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/twitter/android/ProfileFragment;->aM:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ProfileFragment;->a(Ljava/lang/String;JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;)V

    goto/16 :goto_0

    :cond_8
    const/4 v0, -0x1

    if-ne p3, v0, :cond_9

    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->T()V

    const-string/jumbo v1, "profile:profile:confirm_unfavorite_dialog:unfavorite:click"

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileFragment;->c(Lcom/twitter/library/api/TwitterUser;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/twitter/android/ProfileFragment;->aM:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ProfileFragment;->a(Ljava/lang/String;JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;)V

    goto/16 :goto_0

    :cond_9
    const-string/jumbo v1, "profile:profile:confirm_unfavorite_dialog:cancel:click"

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileFragment;->c(Lcom/twitter/library/api/TwitterUser;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/twitter/android/ProfileFragment;->aM:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ProfileFragment;->a(Ljava/lang/String;JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;)V

    goto/16 :goto_0

    :pswitch_9
    const/4 v0, -0x1

    if-ne p3, v0, :cond_a

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/android/ProfileFragment;->Q:J

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "profile::tweet:mute_dialog:mute_user"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->I()V

    goto/16 :goto_0

    :cond_a
    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/android/ProfileFragment;->Q:J

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "profile::tweet:mute_dialog:cancel"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_a
    const/4 v0, -0x1

    if-ne p3, v0, :cond_b

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/android/ProfileFragment;->Q:J

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "profile::tweet:muted_button:unmute_user"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->J()V

    goto/16 :goto_0

    :cond_b
    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/android/ProfileFragment;->Q:J

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "profile::tweet:muted_button:cancel"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_7
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_1
    .end packed-switch
.end method

.method a(Landroid/content/res/Resources;)V
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->h:Lcom/twitter/library/util/m;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/util/m;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->j:Lcom/twitter/android/widget/ProfileHeader;

    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v2, p1, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/ProfileHeader;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->j:Lcom/twitter/android/widget/ProfileHeader;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0031    # com.twitter.android.R.color.dark_gray

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/ProfileHeader;->setBackgroundColor(I)V

    goto :goto_0
.end method

.method a(Landroid/graphics/Bitmap;)V
    .locals 3

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02003a    # com.twitter.android.R.drawable.bg_no_profile_photo_md

    invoke-static {v0, v1, v2}, Lcom/twitter/library/util/n;->a(Landroid/content/Context;Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object p1

    :cond_0
    iput-object p1, p0, Lcom/twitter/android/ProfileFragment;->ai:Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->v_()V

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/qt;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/qt;

    invoke-virtual {v0}, Lcom/twitter/android/qt;->notifyDataSetChanged()V

    :cond_1
    return-void
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 10

    const/16 v9, 0x8

    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->d(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->m:Lcom/twitter/android/rb;

    invoke-virtual {v0}, Lcom/twitter/android/rb;->d()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ye;

    invoke-virtual {v0, p2}, Lcom/twitter/android/ye;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    iget-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->D:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    iget-boolean v0, v0, Lcom/twitter/library/api/TwitterUser;->isProtected:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-static {v0}, Lcom/twitter/library/provider/ay;->b(I)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->m:Lcom/twitter/android/rb;

    invoke-virtual {v0}, Lcom/twitter/android/rb;->d()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ye;

    invoke-virtual {v0, v1}, Lcom/twitter/android/ye;->c(I)V

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->U()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0, v7}, Lcom/twitter/android/ProfileFragment;->p(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->m:Lcom/twitter/android/rb;

    invoke-virtual {v0}, Lcom/twitter/android/rb;->d()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ye;

    invoke-virtual {v0}, Lcom/twitter/android/ye;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    invoke-direct {p0, v7}, Lcom/twitter/android/ProfileFragment;->o(I)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_3
    iget v0, p0, Lcom/twitter/android/ProfileFragment;->G:I

    and-int/lit8 v0, v0, -0x3

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->G:I

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->E:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->E:I

    new-instance v0, Ljs;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v4

    iget-wide v5, p0, Lcom/twitter/android/ProfileFragment;->I:J

    invoke-direct {v0, v3, v4, v5, v6}, Ljs;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;J)V

    const/4 v3, 0x3

    invoke-virtual {v0, v3}, Ljs;->a(I)Ljs;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljs;->c(I)Lcom/twitter/library/service/b;

    iget-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->D:Z

    if-eqz v0, :cond_4

    invoke-static {}, Lgw;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Ljs;->a(Z)Ljs;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/twitter/android/client/c;->c(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;

    invoke-virtual {p0, v3, v7, v2}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/library/service/b;II)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/android/ProfileFragment;->Q:J

    invoke-static {v0, v2, v3, v1}, Lcom/twitter/android/util/AppMetrics;->a(Landroid/content/Context;JI)V

    goto/16 :goto_0

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->m:Lcom/twitter/android/rb;

    invoke-virtual {v0, v2}, Lcom/twitter/android/rb;->a(Z)V

    goto/16 :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->n:Lcom/twitter/android/qz;

    invoke-virtual {v0}, Lcom/twitter/android/qz;->d()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/rh;

    invoke-virtual {v0, p2}, Lcom/twitter/android/rh;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    invoke-direct {p0, v8}, Lcom/twitter/android/ProfileFragment;->p(I)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->n:Lcom/twitter/android/qz;

    invoke-virtual {v0}, Lcom/twitter/android/qz;->d()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/rh;

    invoke-virtual {v0}, Lcom/twitter/android/rh;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    :cond_6
    invoke-direct {p0, v8}, Lcom/twitter/android/ProfileFragment;->o(I)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_7
    iget v0, p0, Lcom/twitter/android/ProfileFragment;->G:I

    and-int/lit8 v0, v0, -0x5

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->G:I

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->E:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->E:I

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    const/16 v3, 0xa

    const/4 v4, 0x6

    iget-wide v5, p0, Lcom/twitter/android/ProfileFragment;->I:J

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/client/c;->a(ZIIIJ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->w:Lcom/twitter/android/ud;

    invoke-virtual {v0, p2}, Lcom/twitter/android/ud;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->w:Lcom/twitter/android/ud;

    invoke-virtual {v0}, Lcom/twitter/android/ud;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    iget-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->aK:Z

    if-nez v0, :cond_8

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    const-string/jumbo v5, "profile::media_gallery::impression"

    const/4 v6, 0x0

    invoke-virtual {v0, v3, v4, v5, v6}, Lcom/twitter/android/client/c;->a(JLjava/lang/String;Ljava/lang/String;)V

    iput-boolean v1, p0, Lcom/twitter/android/ProfileFragment;->aK:Z

    :cond_8
    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->U()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-direct {p0, v9}, Lcom/twitter/android/ProfileFragment;->p(I)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->w:Lcom/twitter/android/ud;

    invoke-virtual {v0}, Lcom/twitter/android/ud;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    :cond_9
    invoke-direct {p0, v9}, Lcom/twitter/android/ProfileFragment;->o(I)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_a
    iget v0, p0, Lcom/twitter/android/ProfileFragment;->G:I

    and-int/lit8 v0, v0, -0x9

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->G:I

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->E:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->E:I

    new-instance v0, Ljs;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v4

    iget-wide v5, p0, Lcom/twitter/android/ProfileFragment;->I:J

    invoke-direct {v0, v3, v4, v5, v6}, Ljs;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;J)V

    const/16 v3, 0x32

    invoke-virtual {v0, v3}, Ljs;->a(I)Ljs;

    move-result-object v0

    const/16 v3, 0x11

    invoke-virtual {v0, v3}, Ljs;->c(I)Lcom/twitter/library/service/b;

    iget-boolean v3, p0, Lcom/twitter/android/ProfileFragment;->D:Z

    if-eqz v3, :cond_b

    invoke-static {}, Lgw;->a()Z

    move-result v3

    if-eqz v3, :cond_b

    :goto_2
    invoke-virtual {v0, v1}, Ljs;->a(Z)Ljs;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->c(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;

    invoke-virtual {p0, v0, v8, v2}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/library/service/b;II)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/android/ProfileFragment;->Q:J

    invoke-static {v0, v1, v2, v7}, Lcom/twitter/android/util/AppMetrics;->a(Landroid/content/Context;JI)V

    goto/16 :goto_0

    :cond_b
    move v1, v2

    goto :goto_2

    :pswitch_4
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileFragment;->m(I)V

    goto/16 :goto_0

    :pswitch_5
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->O()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->as:Lcom/twitter/android/tn;

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-virtual {v0, v7, v1}, Lcom/twitter/android/tn;->a(II)V

    goto/16 :goto_0

    :cond_c
    iget-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->D:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->k:Lcom/twitter/android/qq;

    if-eqz v0, :cond_0

    :cond_d
    invoke-interface {p2, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iget-wide v4, p0, Lcom/twitter/android/ProfileFragment;->I:J

    cmp-long v0, v2, v4

    if-nez v0, :cond_e

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->k:Lcom/twitter/android/qq;

    invoke-virtual {v0, v1}, Lcom/twitter/android/qq;->a(Z)V

    goto/16 :goto_0

    :cond_e
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_d

    goto/16 :goto_0

    :pswitch_6
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->l:Lcom/twitter/android/qz;

    invoke-virtual {v0}, Lcom/twitter/android/qz;->d()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/rh;

    invoke-virtual {v0, p2}, Lcom/twitter/android/rh;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public a(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 9

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/LoginActivity;->a(Landroid/app/Activity;Landroid/content/Intent;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v5, p0, Lcom/twitter/android/ProfileFragment;->ar:Lcom/twitter/android/widget/cg;

    if-eqz v5, :cond_0

    invoke-super/range {p0 .. p5}, Lcom/twitter/android/client/BaseListFragment;->a(Landroid/widget/ListView;Landroid/view/View;IJ)V

    invoke-virtual {p1}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v0

    if-lt p3, v0, :cond_0

    invoke-virtual {p1}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v0

    sub-int v7, p3, v0

    invoke-virtual {v5, v7}, Lcom/twitter/android/widget/cg;->b(I)I

    move-result v6

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->a:I

    if-ne v6, v0, :cond_2

    iget-object v4, p0, Lcom/twitter/android/ProfileFragment;->l:Lcom/twitter/android/qz;

    const/16 v8, 0x14

    move-object v0, p0

    move-object v1, p2

    move-wide v2, p4

    invoke-direct/range {v0 .. v8}, Lcom/twitter/android/ProfileFragment;->a(Landroid/view/View;JLcom/twitter/android/qz;Lcom/twitter/android/widget/cg;III)V

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/twitter/android/ProfileFragment;->b:I

    if-ne v6, v0, :cond_3

    iget-object v4, p0, Lcom/twitter/android/ProfileFragment;->n:Lcom/twitter/android/qz;

    const/16 v8, 0xa

    move-object v0, p0

    move-object v1, p2

    move-wide v2, p4

    invoke-direct/range {v0 .. v8}, Lcom/twitter/android/ProfileFragment;->a(Landroid/view/View;JLcom/twitter/android/qz;Lcom/twitter/android/widget/cg;III)V

    goto :goto_0

    :cond_3
    invoke-virtual {p1, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected a(Lcom/twitter/internal/android/widget/ToolBar;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->a(Lcom/twitter/internal/android/widget/ToolBar;)V

    const v0, 0x7f09032f    # com.twitter.android.R.id.menu_share

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lhn;->b(Z)Lhn;

    return-void
.end method

.method public a(Lcom/twitter/library/api/TwitterUser;)V
    .locals 17

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/twitter/library/api/TwitterUser;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    const/4 v2, 0x1

    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/ProfileFragment;->o()Z

    move-result v5

    if-nez v2, :cond_0

    if-eqz v5, :cond_a

    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v10

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/ProfileFragment;->V()Landroid/widget/ListView;

    move-result-object v7

    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/twitter/library/api/TwitterUser;->userId:J

    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    if-eqz v4, :cond_c

    iget-wide v2, v4, Lcom/twitter/library/api/TwitterUser;->userId:J

    move-object/from16 v0, p1

    iget-wide v12, v0, Lcom/twitter/library/api/TwitterUser;->userId:J

    cmp-long v2, v2, v12

    if-eqz v2, :cond_c

    const/4 v2, 0x1

    move v3, v2

    :goto_1
    move-object/from16 v0, p0

    iput-wide v8, v0, Lcom/twitter/android/ProfileFragment;->I:J

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    invoke-direct/range {p0 .. p0}, Lcom/twitter/android/ProfileFragment;->K()V

    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/twitter/library/api/TwitterUser;->isLifelineInstitution:Z

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/twitter/android/ProfileFragment;->aH:Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/ProfileFragment;->ao:Landroid/widget/LinearLayout;

    const v12, 0x7f0902bc    # com.twitter.android.R.id.stats

    invoke-virtual {v2, v12}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/ProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f090289    # com.twitter.android.R.id.tweets_stat

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/ProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f0e0010    # com.twitter.android.R.plurals.profile_tweets_count_label

    move-object/from16 v0, p1

    iget v0, v0, Lcom/twitter/library/api/TwitterUser;->statusesCount:I

    move/from16 v16, v0

    invoke-virtual/range {v14 .. v16}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    iget v15, v0, Lcom/twitter/library/api/TwitterUser;->statusesCount:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v13, v14, v15}, Lcom/twitter/android/ProfileFragment;->a(Landroid/view/ViewGroup;ILjava/lang/String;I)V

    const v13, 0x7f09028a    # com.twitter.android.R.id.following_stat

    const v14, 0x7f0f0326    # com.twitter.android.R.string.profile_friends

    invoke-virtual {v12, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    iget v15, v0, Lcom/twitter/library/api/TwitterUser;->friendsCount:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v13, v14, v15}, Lcom/twitter/android/ProfileFragment;->a(Landroid/view/ViewGroup;ILjava/lang/String;I)V

    const v13, 0x7f09028b    # com.twitter.android.R.id.followers_stat

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/ProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f0e000f    # com.twitter.android.R.plurals.profile_followers_count_label

    move-object/from16 v0, p1

    iget v0, v0, Lcom/twitter/library/api/TwitterUser;->followersCount:I

    move/from16 v16, v0

    invoke-virtual/range {v14 .. v16}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v14

    move-object/from16 v0, p1

    iget v15, v0, Lcom/twitter/library/api/TwitterUser;->followersCount:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v13, v14, v15}, Lcom/twitter/android/ProfileFragment;->a(Landroid/view/ViewGroup;ILjava/lang/String;I)V

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    if-nez v2, :cond_d

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/twitter/android/ProfileFragment;->a(Landroid/graphics/Bitmap;)V

    :cond_1
    :goto_2
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/twitter/library/api/TwitterUser;->profileHeaderImageUrl:Ljava/lang/String;

    if-nez v2, :cond_f

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/ProfileFragment;->j:Lcom/twitter/android/widget/ProfileHeader;

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/ProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0031    # com.twitter.android.R.color.dark_gray

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v2, v4}, Lcom/twitter/android/widget/ProfileHeader;->setBackgroundColor(I)V

    :cond_2
    :goto_3
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/twitter/android/ProfileFragment;->f(Z)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-direct/range {p0 .. p0}, Lcom/twitter/android/ProfileFragment;->N()Z

    move-result v4

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/twitter/android/ProfileFragment;->D:Z

    if-eqz v5, :cond_12

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/twitter/android/ProfileFragment;->d(I)V

    move-object/from16 v0, p1

    iget-boolean v5, v0, Lcom/twitter/library/api/TwitterUser;->isProtected:Z

    if-eqz v5, :cond_3

    invoke-direct/range {p0 .. p0}, Lcom/twitter/android/ProfileFragment;->M()V

    :cond_3
    move-object/from16 v0, p1

    iget-wide v12, v0, Lcom/twitter/library/api/TwitterUser;->lastUpdated:J

    const-wide/32 v14, 0x493e0

    add-long/2addr v12, v14

    cmp-long v2, v12, v2

    if-gez v2, :cond_4

    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/android/ProfileFragment;->F:I

    and-int/lit8 v2, v2, 0x40

    if-nez v2, :cond_4

    invoke-virtual {v10, v11, v8, v9}, Lcom/twitter/android/client/c;->a(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/twitter/android/ProfileFragment;->d(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/android/ProfileFragment;->F:I

    or-int/lit8 v2, v2, 0x40

    move-object/from16 v0, p0

    iput v2, v0, Lcom/twitter/android/ProfileFragment;->F:I

    :cond_4
    :goto_4
    move-object/from16 v0, p0

    invoke-direct {v0, v6, v8, v9}, Lcom/twitter/android/ProfileFragment;->a(Landroid/content/Context;J)V

    if-eqz v4, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/ProfileFragment;->k:Lcom/twitter/android/qq;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/twitter/android/qq;->a(Lcom/twitter/library/api/TwitterUser;)V

    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/ProfileFragment;->m:Lcom/twitter/android/rb;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/twitter/android/rb;->a(Lcom/twitter/library/api/TwitterUser;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/ProfileFragment;->l:Lcom/twitter/android/qz;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/twitter/android/qz;->a(Lcom/twitter/library/api/TwitterUser;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/ProfileFragment;->n:Lcom/twitter/android/qz;

    move-object/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/twitter/android/qz;->a(Lcom/twitter/library/api/TwitterUser;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/ProfileFragment;->ap:Lcom/twitter/android/oo;

    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/twitter/android/PhotoGridActivity;

    invoke-direct {v3, v6, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v4, "user_id"

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/api/TwitterUser;->a()J

    move-result-wide v8

    invoke-virtual {v3, v4, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v4, "user_name"

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/android/oo;->a(Landroid/content/Intent;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/ProfileFragment;->ar:Lcom/twitter/android/widget/cg;

    if-nez v2, :cond_6

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/twitter/android/ProfileFragment;->D:Z

    if-eqz v2, :cond_16

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/twitter/android/ProfileFragment;->c(Landroid/content/Context;)Lcom/twitter/android/tn;

    move-result-object v2

    :goto_5
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/twitter/android/ProfileFragment;->D:Z

    if-nez v3, :cond_18

    move-object/from16 v0, p1

    iget-boolean v3, v0, Lcom/twitter/library/api/TwitterUser;->isProtected:Z

    if-eqz v3, :cond_18

    move-object/from16 v0, p1

    iget v3, v0, Lcom/twitter/library/api/TwitterUser;->friendship:I

    invoke-static {v3}, Lcom/twitter/library/provider/ay;->b(I)Z

    move-result v3

    if-nez v3, :cond_18

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/ProfileFragment;->k:Lcom/twitter/android/qq;

    if-eqz v2, :cond_17

    new-instance v2, Lcom/twitter/android/widget/cg;

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/widget/BaseAdapter;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/ProfileFragment;->k:Lcom/twitter/android/qq;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/ProfileFragment;->m:Lcom/twitter/android/rb;

    aput-object v5, v3, v4

    invoke-direct {v2, v3}, Lcom/twitter/android/widget/cg;-><init>([Landroid/widget/BaseAdapter;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/android/ProfileFragment;->ar:Lcom/twitter/android/widget/cg;

    :cond_6
    :goto_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/ProfileFragment;->ar:Lcom/twitter/android/widget/cg;

    invoke-virtual {v7, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/ProfileFragment;->aR:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v7, v2}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/android/ProfileFragment;->aS:I

    invoke-virtual {v7, v2}, Landroid/widget/ListView;->setDividerHeight(I)V

    const/4 v2, 0x3

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/twitter/android/ProfileFragment;->b(I)V

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/twitter/android/ProfileFragment;->aI:Z

    if-eqz v2, :cond_8

    const-string/jumbo v3, "profile::::impression"

    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/twitter/library/api/TwitterUser;->userId:J

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/twitter/library/api/TwitterUser;->promotedContent:Lcom/twitter/library/api/PromotedContent;

    invoke-direct/range {p0 .. p1}, Lcom/twitter/android/ProfileFragment;->c(Lcom/twitter/library/api/TwitterUser;)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/android/ProfileFragment;->aM:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/android/ProfileFragment;->aT:Lcom/twitter/library/scribe/ScribeItem;

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v9}, Lcom/twitter/android/ProfileFragment;->a(Ljava/lang/String;JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;Lcom/twitter/library/scribe/ScribeItem;)V

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/twitter/library/api/TwitterUser;->profileHeaderImageUrl:Ljava/lang/String;

    if-eqz v2, :cond_7

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string/jumbo v6, "profile:::header_image:impression"

    aput-object v6, v4, v5

    invoke-virtual {v10, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_7
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/twitter/android/ProfileFragment;->aI:Z

    :cond_8
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/twitter/android/ProfileFragment;->D:Z

    if-eqz v2, :cond_9

    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/twitter/library/api/TwitterUser;->isProtected:Z

    if-eqz v2, :cond_9

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/ProfileFragment;->r()V

    :cond_9
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/twitter/android/ProfileFragment;->K:Z

    if-eqz v2, :cond_a

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/ProfileFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v2

    const/4 v3, 0x7

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v2, v3, v4, v0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/twitter/android/ProfileFragment;->K:Z

    :cond_a
    return-void

    :cond_b
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_c
    const/4 v2, 0x0

    move v3, v2

    goto/16 :goto_1

    :cond_d
    if-nez v5, :cond_e

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/twitter/library/api/TwitterUser;->a(Lcom/twitter/library/api/TwitterUser;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_e
    const v2, 0x7f0c00af    # com.twitter.android.R.dimen.profile_avatar_size

    invoke-virtual {v12, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    move-object/from16 v0, p1

    iget-object v13, v0, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    invoke-static {v13, v2}, Lcom/twitter/library/util/Util;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v13

    new-instance v14, Lcom/twitter/library/util/m;

    invoke-direct {v14, v13, v2, v2}, Lcom/twitter/library/util/m;-><init>(Ljava/lang/String;II)V

    invoke-virtual {v10, v14}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/util/m;)Landroid/graphics/Bitmap;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/twitter/android/ProfileFragment;->a(Landroid/graphics/Bitmap;)V

    move-object/from16 v0, p0

    iput-object v14, v0, Lcom/twitter/android/ProfileFragment;->i:Lcom/twitter/library/util/m;

    goto/16 :goto_2

    :cond_f
    if-nez v5, :cond_10

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lcom/twitter/library/api/TwitterUser;->b(Lcom/twitter/library/api/TwitterUser;)Z

    move-result v2

    if-nez v2, :cond_2

    :cond_10
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/twitter/library/api/TwitterUser;->profileHeaderImageUrl:Ljava/lang/String;

    iget v4, v10, Lcom/twitter/android/client/c;->d:F

    invoke-static {v2, v4}, Lcom/twitter/library/util/Util;->a(Ljava/lang/String;F)Ljava/lang/String;

    move-result-object v2

    new-instance v4, Lcom/twitter/library/util/m;

    invoke-direct {v4, v2}, Lcom/twitter/library/util/m;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/android/ProfileFragment;->h:Lcom/twitter/library/util/m;

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/twitter/android/ProfileFragment;->D:Z

    if-eqz v2, :cond_11

    new-instance v2, Lcom/twitter/android/qu;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/twitter/android/qu;-><init>(Lcom/twitter/android/ProfileFragment;)V

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Void;

    invoke-virtual {v2, v4}, Lcom/twitter/android/qu;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_3

    :cond_11
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/twitter/android/ProfileFragment;->a(Landroid/content/res/Resources;)V

    goto/16 :goto_3

    :cond_12
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/ProfileFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    const-string/jumbo v11, "friendship"

    invoke-virtual {v5, v11}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_13

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/ProfileFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "friendship"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/twitter/android/ProfileFragment;->d(I)V

    const-string/jumbo v3, "friendship"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/android/ProfileFragment;->F:I

    or-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/twitter/android/ProfileFragment;->F:I

    if-eqz v4, :cond_4

    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/android/ProfileFragment;->F:I

    or-int/lit8 v2, v2, 0x20

    move-object/from16 v0, p0

    iput v2, v0, Lcom/twitter/android/ProfileFragment;->F:I

    goto/16 :goto_4

    :cond_13
    move-object/from16 v0, p1

    iget-wide v11, v0, Lcom/twitter/library/api/TwitterUser;->friendshipTime:J

    const-wide/32 v13, 0x493e0

    add-long/2addr v11, v13

    cmp-long v2, v11, v2

    if-ltz v2, :cond_14

    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/twitter/library/api/TwitterUser;->isProtected:Z

    if-eqz v2, :cond_15

    move-object/from16 v0, p1

    iget v2, v0, Lcom/twitter/library/api/TwitterUser;->friendship:I

    invoke-static {v2}, Lcom/twitter/library/provider/ay;->j(I)Z

    move-result v2

    if-eqz v2, :cond_15

    :cond_14
    invoke-direct/range {p0 .. p0}, Lcom/twitter/android/ProfileFragment;->L()V

    :goto_7
    if-eqz v4, :cond_4

    invoke-direct/range {p0 .. p0}, Lcom/twitter/android/ProfileFragment;->M()V

    goto/16 :goto_4

    :cond_15
    move-object/from16 v0, p1

    iget v2, v0, Lcom/twitter/library/api/TwitterUser;->friendship:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/twitter/android/ProfileFragment;->d(I)V

    goto :goto_7

    :cond_16
    new-instance v2, Lcom/twitter/android/tn;

    const/4 v3, 0x0

    new-array v3, v3, [Lcom/twitter/android/to;

    const v4, 0x7f030136    # com.twitter.android.R.layout.section_simple_row_view

    invoke-direct {v2, v10, v3, v4}, Lcom/twitter/android/tn;-><init>(Lcom/twitter/android/client/c;[Lcom/twitter/android/to;I)V

    goto/16 :goto_5

    :cond_17
    new-instance v2, Lcom/twitter/android/widget/cg;

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/widget/BaseAdapter;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/ProfileFragment;->m:Lcom/twitter/android/rb;

    aput-object v5, v3, v4

    invoke-direct {v2, v3}, Lcom/twitter/android/widget/cg;-><init>([Landroid/widget/BaseAdapter;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/android/ProfileFragment;->ar:Lcom/twitter/android/widget/cg;

    goto/16 :goto_6

    :cond_18
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/ProfileFragment;->k:Lcom/twitter/android/qq;

    if-eqz v3, :cond_19

    new-instance v3, Lcom/twitter/android/widget/cg;

    const/4 v4, 0x7

    new-array v4, v4, [Landroid/widget/BaseAdapter;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/ProfileFragment;->k:Lcom/twitter/android/qq;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/ProfileFragment;->l:Lcom/twitter/android/qz;

    aput-object v6, v4, v5

    const/4 v5, 0x2

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/ProfileFragment;->m:Lcom/twitter/android/rb;

    aput-object v6, v4, v5

    const/4 v5, 0x3

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/ProfileFragment;->ap:Lcom/twitter/android/oo;

    aput-object v6, v4, v5

    const/4 v5, 0x4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/ProfileFragment;->as:Lcom/twitter/android/tn;

    aput-object v6, v4, v5

    const/4 v5, 0x5

    aput-object v2, v4, v5

    const/4 v2, 0x6

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/ProfileFragment;->n:Lcom/twitter/android/qz;

    aput-object v5, v4, v2

    invoke-direct {v3, v4}, Lcom/twitter/android/widget/cg;-><init>([Landroid/widget/BaseAdapter;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/twitter/android/ProfileFragment;->ar:Lcom/twitter/android/widget/cg;

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput v2, v0, Lcom/twitter/android/ProfileFragment;->a:I

    const/4 v2, 0x6

    move-object/from16 v0, p0

    iput v2, v0, Lcom/twitter/android/ProfileFragment;->b:I

    goto/16 :goto_6

    :cond_19
    new-instance v3, Lcom/twitter/android/widget/cg;

    const/4 v4, 0x6

    new-array v4, v4, [Landroid/widget/BaseAdapter;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/ProfileFragment;->l:Lcom/twitter/android/qz;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/ProfileFragment;->m:Lcom/twitter/android/rb;

    aput-object v6, v4, v5

    const/4 v5, 0x2

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/ProfileFragment;->ap:Lcom/twitter/android/oo;

    aput-object v6, v4, v5

    const/4 v5, 0x3

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/ProfileFragment;->as:Lcom/twitter/android/tn;

    aput-object v6, v4, v5

    const/4 v5, 0x4

    aput-object v2, v4, v5

    const/4 v2, 0x5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/ProfileFragment;->n:Lcom/twitter/android/qz;

    aput-object v5, v4, v2

    invoke-direct {v3, v4}, Lcom/twitter/android/widget/cg;-><init>([Landroid/widget/BaseAdapter;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/twitter/android/ProfileFragment;->ar:Lcom/twitter/android/widget/cg;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/twitter/android/ProfileFragment;->a:I

    const/4 v2, 0x5

    move-object/from16 v0, p0

    iput v2, v0, Lcom/twitter/android/ProfileFragment;->b:I

    goto/16 :goto_6
.end method

.method public a(Lcom/twitter/library/util/aa;Ljava/util/HashMap;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->w:Lcom/twitter/android/ud;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/ud;->a(Lcom/twitter/library/util/aa;Ljava/util/HashMap;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;)V
    .locals 2

    const/4 v0, 0x1

    iget v1, p1, Lcom/twitter/library/util/ao;->g:I

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->l:Lcom/twitter/android/qz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->l:Lcom/twitter/android/qz;

    invoke-virtual {v0}, Lcom/twitter/android/qz;->d()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/rh;

    invoke-virtual {v0, p2}, Lcom/twitter/android/rh;->a(Ljava/util/HashMap;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->m:Lcom/twitter/android/rb;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->m:Lcom/twitter/android/rb;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/rb;->a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;)V

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->n:Lcom/twitter/android/qz;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->n:Lcom/twitter/android/qz;

    invoke-virtual {v0}, Lcom/twitter/android/qz;->d()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/rh;

    invoke-virtual {v0, p2}, Lcom/twitter/android/rh;->a(Ljava/util/HashMap;)V

    :cond_2
    return-void
.end method

.method public a(Lcom/twitter/library/widget/BaseUserView;Lcom/twitter/library/api/PromotedContent;Landroid/os/Bundle;I)V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->ab:Landroid/util/SparseArray;

    invoke-virtual {v0, p4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->ab:Landroid/util/SparseArray;

    invoke-virtual {v1, p4, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_0
    invoke-virtual {p1}, Lcom/twitter/library/widget/BaseUserView;->getUserId()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->aa:Landroid/util/SparseArray;

    invoke-virtual {v0, p4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-nez v0, :cond_3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->aa:Landroid/util/SparseArray;

    invoke-virtual {v1, p4, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    move-object v1, v0

    :goto_0
    invoke-virtual {p1}, Lcom/twitter/library/widget/BaseUserView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/zx;

    iget-object v0, v0, Lcom/twitter/android/zx;->f:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v2, v3, p2, v0, v4}, Lcom/twitter/library/scribe/ScribeItem;->a(JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    const-string/jumbo v2, "position"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/twitter/library/scribe/ScribeItem;->g:I

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->X:Ljava/util/HashSet;

    iget-object v1, p2, Lcom/twitter/library/api/PromotedContent;->impressionId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/api/PromotedEvent;->a:Lcom/twitter/library/api/PromotedEvent;

    invoke-virtual {v0, v1, p2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/library/api/PromotedContent;)V

    :cond_2
    return-void

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/widget/UserView;JI)V
    .locals 11

    const/16 v10, 0x14

    const/16 v9, 0xa

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserView;->getPromotedContent()Lcom/twitter/library/api/PromotedContent;

    move-result-object v8

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    iget-object v0, p1, Lcom/twitter/library/widget/UserView;->m:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v0}, Lcom/twitter/library/widget/ActionButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v0, p0

    move-wide v3, p2

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/ProfileFragment;->a(JJI)Ljava/lang/Long;

    invoke-direct {p0, p2, p3, v8, p4}, Lcom/twitter/android/ProfileFragment;->a(JLcom/twitter/library/api/PromotedContent;I)V

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->aj:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v0, p2, p3}, Lcom/twitter/library/util/FriendshipCache;->e(J)V

    if-ne p4, v9, :cond_1

    const-string/jumbo v1, "profile:similar_to::user:unfollow"

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/zx;

    iget-object v5, v0, Lcom/twitter/android/zx;->f:Ljava/lang/String;

    iget-object v6, p0, Lcom/twitter/android/ProfileFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object v0, p0

    move-wide v2, p2

    move-object v4, v8

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ProfileFragment;->a(Ljava/lang/String;JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-ne p4, v10, :cond_0

    const-string/jumbo v1, "profile:user_similarities_list::user:unfollow"

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/zx;

    iget-object v5, v0, Lcom/twitter/android/zx;->f:Ljava/lang/String;

    iget-object v6, p0, Lcom/twitter/android/ProfileFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object v0, p0

    move-wide v2, p2

    move-object v4, v8

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ProfileFragment;->a(Ljava/lang/String;JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/twitter/library/widget/UserView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/zx;

    iget-wide v5, v0, Lcom/twitter/android/zx;->c:J

    move-object v0, p0

    move-wide v3, p2

    move v7, p4

    invoke-virtual/range {v0 .. v7}, Lcom/twitter/android/ProfileFragment;->a(JJJI)V

    const/4 v3, 0x0

    move-object v0, p0

    move-wide v1, p2

    move-object v4, v8

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/ProfileFragment;->a(JZLcom/twitter/library/api/PromotedContent;I)V

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->aj:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v0, p2, p3}, Lcom/twitter/library/util/FriendshipCache;->b(J)V

    if-ne p4, v9, :cond_3

    const-string/jumbo v1, "profile:similar_to::user:follow"

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/zx;

    iget-object v5, v0, Lcom/twitter/android/zx;->f:Ljava/lang/String;

    iget-object v6, p0, Lcom/twitter/android/ProfileFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object v0, p0

    move-wide v2, p2

    move-object v4, v8

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ProfileFragment;->a(Ljava/lang/String;JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;)V

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/zx;

    iget v0, v0, Lcom/twitter/android/zx;->e:I

    invoke-static {v0}, Lcom/twitter/library/provider/ay;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v1, "profile:similar_to::user:follow_back"

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/zx;

    iget-object v5, v0, Lcom/twitter/android/zx;->f:Ljava/lang/String;

    iget-object v6, p0, Lcom/twitter/android/ProfileFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object v0, p0

    move-wide v2, p2

    move-object v4, v8

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ProfileFragment;->a(Ljava/lang/String;JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;)V

    goto :goto_0

    :cond_3
    if-ne p4, v10, :cond_0

    const-string/jumbo v1, "profile:user_similarities_list::user:follow"

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/zx;

    iget-object v5, v0, Lcom/twitter/android/zx;->f:Ljava/lang/String;

    iget-object v6, p0, Lcom/twitter/android/ProfileFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object v0, p0

    move-wide v2, p2

    move-object v4, v8

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ProfileFragment;->a(Ljava/lang/String;JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;)V

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/zx;

    iget v0, v0, Lcom/twitter/android/zx;->e:I

    invoke-static {v0}, Lcom/twitter/library/provider/ay;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v1, "profile:user_similarities_list::user:follow_back"

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/zx;

    iget-object v5, v0, Lcom/twitter/android/zx;->f:Ljava/lang/String;

    iget-object v6, p0, Lcom/twitter/android/ProfileFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object v0, p0

    move-wide v2, p2

    move-object v4, v8

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ProfileFragment;->a(Ljava/lang/String;JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;)V

    goto/16 :goto_0
.end method

.method protected a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseListFragment;->a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)V

    const v0, 0x7f110023    # com.twitter.android.R.menu.toolbar_share

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->au()Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/library/client/aa;->d(Ljava/lang/String;)V

    return-void
.end method

.method a(ZII)V
    .locals 10

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->av:Lcom/twitter/internal/android/widget/ShadowTextView;

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->al:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/twitter/android/ProfileFragment;->ay:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/ShadowTextView;->setEnabled(Z)V

    invoke-virtual {v1, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    invoke-virtual {v2, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    if-lez p2, :cond_1

    invoke-virtual {v0, p2}, Lcom/twitter/internal/android/widget/ShadowTextView;->setText(I)V

    const v4, 0x7f0f019f    # com.twitter.android.R.string.follow

    if-ne p2, v4, :cond_4

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Lcom/twitter/internal/android/widget/ShadowTextView;->setChecked(Z)V

    const v4, 0x7f0201a7    # com.twitter.android.R.drawable.ic_follow_action_default

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v0, v4, v5, v6, v7}, Lcom/twitter/internal/android/widget/ShadowTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    :cond_0
    :goto_0
    new-instance v4, Landroid/content/res/ColorStateList;

    const/4 v5, 0x3

    new-array v5, v5, [[I

    const/4 v6, 0x0

    const/4 v7, 0x1

    new-array v7, v7, [I

    const/4 v8, 0x0

    const v9, -0x10100a0

    aput v9, v7, v8

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const/4 v7, 0x1

    new-array v7, v7, [I

    const/4 v8, 0x0

    const v9, 0x10100a7    # android.R.attr.state_pressed

    aput v9, v7, v8

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const/4 v7, 0x1

    new-array v7, v7, [I

    const/4 v8, 0x0

    const v9, 0x10100a0    # android.R.attr.state_checked

    aput v9, v7, v8

    aput-object v7, v5, v6

    const/4 v6, 0x3

    new-array v6, v6, [I

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0b007b    # com.twitter.android.R.color.twitter_blue

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    aput v8, v6, v7

    const/4 v7, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0b007b    # com.twitter.android.R.color.twitter_blue

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    aput v8, v6, v7

    const/4 v7, 0x2

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0b007e    # com.twitter.android.R.color.white

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    aput v8, v6, v7

    invoke-direct {v4, v5, v6}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    const v5, 0x7f0f02f6    # com.twitter.android.R.string.pending

    if-ne p2, v5, :cond_7

    const v4, 0x7f020093    # com.twitter.android.R.drawable.btn_pending_action_bg

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/twitter/internal/android/widget/ShadowTextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const v4, 0x7f0b0031    # com.twitter.android.R.color.dark_gray

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/twitter/internal/android/widget/ShadowTextView;->setTextColor(I)V

    :goto_1
    const v4, 0x7f0c0017    # com.twitter.android.R.dimen.btn_padding

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    const v5, 0x7f0c0018    # com.twitter.android.R.dimen.btn_padding_horiz

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-virtual {v0, v5, v4, v5, v4}, Lcom/twitter/internal/android/widget/ShadowTextView;->setPadding(IIII)V

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Lcom/twitter/internal/android/widget/ShadowTextView;->setIncludeFontPadding(Z)V

    :cond_1
    const/4 v4, 0x1

    if-ne p3, v4, :cond_8

    const v4, 0x7f020120    # com.twitter.android.R.drawable.ic_alert_default

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setImageResource(I)V

    const v4, 0x7f02005d    # com.twitter.android.R.drawable.btn_alerts_bg_default

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_2
    :goto_2
    iget-boolean v3, p0, Lcom/twitter/android/ProfileFragment;->D:Z

    if-eqz v3, :cond_a

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Lcom/twitter/internal/android/widget/ShadowTextView;->setVisibility(I)V

    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->ak:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->aO:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->am:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    :cond_3
    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/a;->b(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_9

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->an:Landroid/widget/ImageButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    :goto_3
    return-void

    :cond_4
    const v4, 0x7f0f050b    # com.twitter.android.R.string.unfollow

    if-ne p2, v4, :cond_5

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lcom/twitter/internal/android/widget/ShadowTextView;->setChecked(Z)V

    const v4, 0x7f0201a6    # com.twitter.android.R.drawable.ic_follow_action_checked

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v0, v4, v5, v6, v7}, Lcom/twitter/internal/android/widget/ShadowTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    :cond_5
    const v4, 0x7f0f0508    # com.twitter.android.R.string.unblock

    if-ne p2, v4, :cond_6

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Lcom/twitter/internal/android/widget/ShadowTextView;->setChecked(Z)V

    const v4, 0x7f020126    # com.twitter.android.R.drawable.ic_blocked_default

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v0, v4, v5, v6, v7}, Lcom/twitter/internal/android/widget/ShadowTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    :cond_6
    const v4, 0x7f0f02f6    # com.twitter.android.R.string.pending

    if-ne p2, v4, :cond_0

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Lcom/twitter/internal/android/widget/ShadowTextView;->setChecked(Z)V

    const v4, 0x7f0201e4    # com.twitter.android.R.drawable.ic_pending_default

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v0, v4, v5, v6, v7}, Lcom/twitter/internal/android/widget/ShadowTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    :cond_7
    invoke-virtual {v0, v4}, Lcom/twitter/internal/android/widget/ShadowTextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    const v4, 0x7f020086    # com.twitter.android.R.drawable.btn_follow_action_bg

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/twitter/internal/android/widget/ShadowTextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    :cond_8
    const/4 v4, 0x2

    if-ne p3, v4, :cond_2

    const v4, 0x7f02011f    # com.twitter.android.R.drawable.ic_alert_active

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setImageResource(I)V

    const v4, 0x7f02005c    # com.twitter.android.R.drawable.btn_alerts_bg_active

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    :cond_9
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->an:Landroid/widget/ImageButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_3

    :cond_a
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->ak:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "alerts_activation_enabled"

    const/4 v3, 0x0

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iget-boolean v1, p0, Lcom/twitter/android/ProfileFragment;->aH:Z

    if-eqz v1, :cond_b

    const v1, 0x7f0f0508    # com.twitter.android.R.string.unblock

    if-eq p2, v1, :cond_b

    if-eqz v0, :cond_b

    iget-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->aF:Z

    if-nez v0, :cond_b

    iget-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->aJ:Z

    if-eqz v0, :cond_b

    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    const-string/jumbo v1, "profile:profile::lifeline_follow:impression"

    iget-wide v2, p0, Lcom/twitter/android/ProfileFragment;->I:J

    iget-object v4, p0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/library/api/PromotedContent;

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileFragment;->c(Lcom/twitter/library/api/TwitterUser;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/twitter/android/ProfileFragment;->aM:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ProfileFragment;->a(Ljava/lang/String;JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;)V

    goto/16 :goto_3

    :cond_b
    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_3
.end method

.method public a(Lhn;)Z
    .locals 4

    invoke-virtual {p1}, Lhn;->a()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->a(Lhn;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {v1}, Lcom/twitter/library/api/TwitterUser;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    iget-object v2, v2, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    iget-object v3, v3, Lcom/twitter/library/api/TwitterUser;->profileDescription:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f09032f
        :pswitch_0    # com.twitter.android.R.id.menu_share
    .end packed-switch
.end method

.method protected b()V
    .locals 4

    const/4 v3, 0x3

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->b()V

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    iget-boolean v2, p0, Lcom/twitter/android/ProfileFragment;->D:Z

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/library/api/TwitterUser;)V

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->ag()Z

    move-result v2

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->ag()Z

    move-result v2

    if-nez v2, :cond_1

    iput v1, p0, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->q()V

    move v0, v1

    :cond_1
    invoke-virtual {p0, v0, v1, v1}, Lcom/twitter/android/ProfileFragment;->a(ZII)V

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    if-nez v0, :cond_4

    invoke-virtual {p0, v3}, Lcom/twitter/android/ProfileFragment;->a_(I)V

    :goto_1
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/android/StartActivity;->a(Landroid/app/Activity;)V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/twitter/android/ProfileFragment;->aN:Lcom/twitter/library/api/TwitterUser;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/ProfileFragment;->aN:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {p0, v2}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/library/api/TwitterUser;)V

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/twitter/android/ProfileFragment;->aN:Lcom/twitter/library/api/TwitterUser;

    goto :goto_0

    :cond_4
    invoke-virtual {p0, v3}, Lcom/twitter/android/ProfileFragment;->b(I)V

    goto :goto_1
.end method

.method protected b_(I)Landroid/app/Dialog;
    .locals 8

    const/4 v0, 0x5

    if-eq p1, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v7, Ljava/util/ArrayList;

    sget v0, Lcom/twitter/android/ProfileFragment;->U:I

    invoke-direct {v7, v0}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v2, Ljava/util/ArrayList;

    sget v0, Lcom/twitter/android/ProfileFragment;->U:I

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget v1, p0, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-static {v1}, Lcom/twitter/library/provider/ay;->g(I)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {v1}, Lcom/twitter/library/provider/ay;->c(I)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_1
    const/4 v0, 0x1

    :goto_1
    invoke-static {v1}, Lcom/twitter/library/provider/ay;->e(I)Z

    move-result v6

    invoke-static {v1}, Lcom/twitter/android/util/af;->a(I)Z

    move-result v5

    invoke-static {v1}, Lcom/twitter/library/provider/ay;->b(I)Z

    move-result v3

    invoke-static {v1}, Lcom/twitter/library/provider/ay;->f(I)Z

    move-result v4

    if-eqz v0, :cond_2

    const v0, 0x7f0f052e    # com.twitter.android.R.string.users_direct_message

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/twitter/android/ProfileFragment$DialogMoreButtons;->a:Lcom/twitter/android/ProfileFragment$DialogMoreButtons;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    const v0, 0x7f0f051e    # com.twitter.android.R.string.users_add_list_member

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/twitter/android/ProfileFragment$DialogMoreButtons;->b:Lcom/twitter/android/ProfileFragment$DialogMoreButtons;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz v3, :cond_3

    if-eqz v4, :cond_7

    const v0, 0x7f0f053f    # com.twitter.android.R.string.users_turn_off_retweets

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_2
    sget-object v0, Lcom/twitter/android/ProfileFragment$DialogMoreButtons;->c:Lcom/twitter/android/ProfileFragment$DialogMoreButtons;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    iget-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->L:Z

    if-eqz v0, :cond_4

    if-nez v6, :cond_4

    if-nez v5, :cond_8

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    invoke-static {v0, v1}, Lcom/twitter/android/util/af;->a(Landroid/content/Context;Lcom/twitter/library/api/TwitterUser;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_3
    sget-object v0, Lcom/twitter/android/ProfileFragment$DialogMoreButtons;->d:Lcom/twitter/android/ProfileFragment$DialogMoreButtons;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    if-eqz v6, :cond_9

    const v0, 0x7f0f0547    # com.twitter.android.R.string.users_unblock

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_4
    sget-object v0, Lcom/twitter/android/ProfileFragment$DialogMoreButtons;->e:Lcom/twitter/android/ProfileFragment$DialogMoreButtons;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->L:Z

    if-nez v0, :cond_5

    const v0, 0x7f0f053d    # com.twitter.android.R.string.users_report_spammer

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/twitter/android/ProfileFragment$DialogMoreButtons;->f:Lcom/twitter/android/ProfileFragment$DialogMoreButtons;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    new-instance v0, Lcom/twitter/android/qk;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/qk;-><init>(Lcom/twitter/android/ProfileFragment;Ljava/util/ArrayList;Landroid/content/Context;ZZZ)V

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v1, v0}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_0

    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_1

    :cond_7
    const v0, 0x7f0f0541    # com.twitter.android.R.string.users_turn_on_retweets

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_8
    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    invoke-static {v0, v1}, Lcom/twitter/android/util/af;->b(Landroid/content/Context;Lcom/twitter/library/api/TwitterUser;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_9
    iget-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->L:Z

    if-eqz v0, :cond_a

    const v0, 0x7f0f004f    # com.twitter.android.R.string.block_or_report

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_a
    const v0, 0x7f0f051f    # com.twitter.android.R.string.users_block

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4
.end method

.method protected c()V
    .locals 6

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->c()V

    iget-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->D:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/android/ProfileFragment;->I:J

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "me::::impression"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    iget-boolean v0, v0, Lcom/twitter/library/api/TwitterUser;->isProtected:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->r()V

    :cond_0
    return-void
.end method

.method protected c_(I)Z
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->c_(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/telephony/TelephonyUtil;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected d()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->d()V

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    const/16 v1, 0x14

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/library/client/Session;I)V

    const/16 v1, 0xa

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/library/client/Session;I)V

    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->G()V

    return-void
.end method

.method d(I)V
    .locals 5

    const v4, 0x7f0f019f    # com.twitter.android.R.string.follow

    const/4 v1, 0x1

    const/4 v3, 0x0

    invoke-static {p1}, Lcom/twitter/library/provider/ay;->k(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->d()Z

    move-result v2

    if-eqz v2, :cond_6

    iget-boolean v2, p0, Lcom/twitter/android/ProfileFragment;->D:Z

    if-eqz v2, :cond_1

    invoke-virtual {p0, v1, v3, v3}, Lcom/twitter/android/ProfileFragment;->a(ZII)V

    :goto_1
    iput p1, p0, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-direct {p0, v3}, Lcom/twitter/android/ProfileFragment;->e(Z)V

    invoke-virtual {p0, v3}, Lcom/twitter/android/ProfileFragment;->d(Z)V

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->aB:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->aq:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    invoke-static {p1}, Lcom/twitter/library/provider/ay;->b(I)Z

    move-result v2

    if-eqz v2, :cond_3

    const v2, 0x7f0f050b    # com.twitter.android.R.string.unfollow

    invoke-virtual {p0, v1, v2, v0}, Lcom/twitter/android/ProfileFragment;->a(ZII)V

    :cond_2
    :goto_2
    invoke-direct {p0, p1}, Lcom/twitter/android/ProfileFragment;->n(I)V

    goto :goto_1

    :cond_3
    invoke-static {p1}, Lcom/twitter/library/provider/ay;->e(I)Z

    move-result v2

    if-eqz v2, :cond_4

    const v2, 0x7f0f0508    # com.twitter.android.R.string.unblock

    invoke-virtual {p0, v1, v2, v0}, Lcom/twitter/android/ProfileFragment;->a(ZII)V

    goto :goto_2

    :cond_4
    invoke-static {p1}, Lcom/twitter/library/provider/ay;->j(I)Z

    move-result v2

    if-eqz v2, :cond_5

    const v2, 0x7f0f02f6    # com.twitter.android.R.string.pending

    invoke-virtual {p0, v1, v2, v0}, Lcom/twitter/android/ProfileFragment;->a(ZII)V

    :goto_3
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    iget-boolean v0, v0, Lcom/twitter/library/api/TwitterUser;->isProtected:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->ao:Landroid/widget/LinearLayout;

    const v2, 0x7f0902bc    # com.twitter.android.R.id.stats

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const v2, 0x7f090289    # com.twitter.android.R.id.tweets_stat

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    const v2, 0x7f09028a    # com.twitter.android.R.id.following_stat

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setEnabled(Z)V

    const v2, 0x7f09028b    # com.twitter.android.R.id.followers_stat

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->m:Lcom/twitter/android/rb;

    invoke-virtual {v0}, Lcom/twitter/android/rb;->d()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ye;

    invoke-virtual {v0, v1}, Lcom/twitter/android/ye;->c(I)V

    goto :goto_2

    :cond_5
    invoke-virtual {p0, v1, v4, v0}, Lcom/twitter/android/ProfileFragment;->a(ZII)V

    goto :goto_3

    :cond_6
    invoke-virtual {p0, v3, v4, v0}, Lcom/twitter/android/ProfileFragment;->a(ZII)V

    goto/16 :goto_1
.end method

.method d(Z)V
    .locals 5

    const v4, 0x7f020079    # com.twitter.android.R.drawable.btn_fav_people_bg_default

    const/16 v0, 0x8

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->ax:Landroid/widget/ImageButton;

    iget v2, p0, Lcom/twitter/android/ProfileFragment;->H:I

    iget-boolean v3, p0, Lcom/twitter/android/ProfileFragment;->aJ:Z

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/twitter/android/ProfileFragment;->D:Z

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/twitter/android/ProfileFragment;->aH:Z

    if-eqz v3, :cond_1

    :cond_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    :goto_0
    return-void

    :cond_1
    invoke-static {v2}, Lcom/twitter/library/provider/ay;->b(I)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {v2}, Lcom/twitter/android/util/af;->a(I)Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v0, 0x0

    :cond_2
    iget-boolean v2, p0, Lcom/twitter/android/ProfileFragment;->aQ:Z

    if-eqz v2, :cond_3

    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->H()Z

    move-result v2

    invoke-static {v2}, Lgl;->a(Z)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setImageResource(I)V

    invoke-static {v2}, Lgl;->b(Z)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    :goto_1
    invoke-direct {p0, v1, v0, p1}, Lcom/twitter/android/ProfileFragment;->a(Landroid/view/View;IZ)V

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->H()Z

    move-result v2

    if-eqz v2, :cond_4

    const v2, 0x7f020201    # com.twitter.android.R.drawable.ic_profile_fav_people_active

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto :goto_1

    :cond_4
    const v2, 0x7f020202    # com.twitter.android.R.drawable.ic_profile_fav_people_default

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setBackgroundResource(I)V

    goto :goto_1
.end method

.method public f_(I)V
    .locals 14

    const v1, 0x7f0f052f    # com.twitter.android.R.string.users_disable_notifications

    const v11, 0x7f0f02d5    # com.twitter.android.R.string.ok

    const v4, 0x7f0f0089    # com.twitter.android.R.string.cancel

    const/4 v9, 0x1

    const/4 v10, 0x0

    const v2, 0x7f0f0571    # com.twitter.android.R.string.yes

    const v8, 0x7f0f029d    # com.twitter.android.R.string.no

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    const/16 v0, 0xe

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Ljava/lang/String;)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v7, 0x7f0f0522    # com.twitter.android.R.string.users_cancel_follow_request_dialog_message

    const v8, 0x7f0f029d    # com.twitter.android.R.string.no

    const v2, 0x7f0f0571    # com.twitter.android.R.string.yes

    move v3, v9

    :goto_1
    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v4, v9, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {v5}, Lcom/twitter/library/api/TwitterUser;->c()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v10

    invoke-virtual {v1, v7, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->b(Ljava/lang/String;)Lcom/twitter/android/widget/PromptDialogFragment;

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->g(I)Lcom/twitter/android/widget/PromptDialogFragment;

    if-eqz v3, :cond_0

    invoke-virtual {v0, v8}, Lcom/twitter/android/widget/PromptDialogFragment;->i(I)Lcom/twitter/android/widget/PromptDialogFragment;

    :cond_0
    invoke-virtual {v0, p0, v10}, Lcom/twitter/android/widget/PromptDialogFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    goto :goto_0

    :pswitch_2
    const/16 v0, 0x8

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0525    # com.twitter.android.R.string.users_change_friendship_dialog_title

    new-array v5, v9, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {v6}, Lcom/twitter/library/api/TwitterUser;->c()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v10

    invoke-virtual {v2, v3, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Ljava/lang/String;)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v7, 0x7f0f0524    # com.twitter.android.R.string.users_change_friendship_dialog_message

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->h(I)Lcom/twitter/android/widget/PromptDialogFragment;

    const v8, 0x7f0f052a    # com.twitter.android.R.string.users_destroy_friendship

    move v2, v4

    move v3, v9

    goto :goto_1

    :pswitch_3
    invoke-static {v9}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v3, 0x7f0f052a    # com.twitter.android.R.string.users_destroy_friendship

    invoke-virtual {v0, v3}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v7, 0x7f0f052c    # com.twitter.android.R.string.users_destroy_friendship_question

    iget v3, p0, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-static {v3}, Lcom/twitter/library/provider/ay;->h(I)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->h(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move v3, v9

    goto :goto_1

    :pswitch_4
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0547    # com.twitter.android.R.string.users_unblock

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v7, 0x7f0f0549    # com.twitter.android.R.string.users_unblock_question

    move v3, v9

    goto/16 :goto_1

    :pswitch_5
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f051f    # com.twitter.android.R.string.users_block

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v7, 0x7f0f0521    # com.twitter.android.R.string.users_block_question

    move v3, v9

    goto/16 :goto_1

    :pswitch_6
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f053d    # com.twitter.android.R.string.users_report_spammer

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v7, 0x7f0f053e    # com.twitter.android.R.string.users_report_spammer_question

    move v3, v9

    goto/16 :goto_1

    :pswitch_7
    const/4 v0, 0x7

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0546    # com.twitter.android.R.string.users_tweet_notifications_dialog_title

    new-array v3, v9, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {v4}, Lcom/twitter/library/api/TwitterUser;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v10

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Ljava/lang/String;)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v12

    iget-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->aQ:Z

    if-eqz v0, :cond_1

    invoke-static {}, Lgl;->g()I

    move-result v0

    move v7, v0

    :goto_2
    const-string/jumbo v1, "profile:profile:confirm_favorite_dialog::show"

    iget-wide v2, p0, Lcom/twitter/android/ProfileFragment;->I:J

    iget-object v4, p0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/library/api/PromotedContent;

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileFragment;->c(Lcom/twitter/library/api/TwitterUser;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/twitter/android/ProfileFragment;->aM:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ProfileFragment;->a(Ljava/lang/String;JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;)V

    move v2, v11

    move v3, v10

    move-object v0, v12

    goto/16 :goto_1

    :cond_1
    iget-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->aQ:Z

    if-eqz v0, :cond_2

    const v0, 0x7f0f0544    # com.twitter.android.R.string.users_tweet_notifications_dialog_message_with_timeline

    :goto_3
    move v7, v0

    goto :goto_2

    :cond_2
    const v0, 0x7f0f0543    # com.twitter.android.R.string.users_tweet_notifications_dialog_message

    goto :goto_3

    :pswitch_8
    const/16 v0, 0x9

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0534    # com.twitter.android.R.string.users_lifeline_follow_dialog_title

    new-array v3, v9, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {v4}, Lcom/twitter/library/api/TwitterUser;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v10

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Ljava/lang/String;)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v7, 0x7f0f0533    # com.twitter.android.R.string.users_lifeline_follow_dialog_message

    move v2, v11

    move v3, v10

    goto/16 :goto_1

    :pswitch_9
    const/16 v0, 0xa

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0536    # com.twitter.android.R.string.users_lifeline_unfollow_dialog_title

    new-array v3, v9, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {v4}, Lcom/twitter/library/api/TwitterUser;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v10

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Ljava/lang/String;)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v7, 0x7f0f0535    # com.twitter.android.R.string.users_lifeline_unfollow_dialog_message

    move v2, v11

    move v3, v10

    goto/16 :goto_1

    :pswitch_a
    const/16 v0, 0xb

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v5

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-boolean v3, p0, Lcom/twitter/android/ProfileFragment;->aQ:Z

    if-eqz v3, :cond_4

    iget v2, p0, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-static {v2}, Lcom/twitter/library/provider/ay;->h(I)Z

    move-result v2

    if-eqz v2, :cond_3

    const v2, 0x7f0f054b    # com.twitter.android.R.string.users_unfavorite_question_dialog_title

    new-array v3, v9, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {v6}, Lcom/twitter/library/api/TwitterUser;->c()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v10

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const v2, 0x7f0f054c    # com.twitter.android.R.string.users_unfavorite_question_with_option_dialog_message

    move v3, v2

    move v2, v9

    :goto_4
    const v6, 0x7f0f0526    # com.twitter.android.R.string.users_change_friendship_dialog_unfavorite

    invoke-virtual {v5, v6}, Lcom/twitter/android/widget/PromptDialogFragment;->h(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move v13, v4

    move v4, v3

    move v3, v2

    move v2, v13

    :goto_5
    invoke-virtual {v5, v0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Ljava/lang/String;)Lcom/twitter/android/widget/PromptDialogFragment;

    move v8, v1

    move v7, v4

    move-object v0, v5

    goto/16 :goto_1

    :cond_3
    const v1, 0x7f0f0526    # com.twitter.android.R.string.users_change_friendship_dialog_unfavorite

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0f054b    # com.twitter.android.R.string.users_unfavorite_question_dialog_title

    move v2, v10

    move v3, v1

    move v1, v8

    goto :goto_4

    :cond_4
    const v1, 0x7f0f054b    # com.twitter.android.R.string.users_unfavorite_question_dialog_title

    new-array v3, v9, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {v4}, Lcom/twitter/library/api/TwitterUser;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v10

    invoke-virtual {v0, v1, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const v3, 0x7f0f054a    # com.twitter.android.R.string.users_unfavorite_question_dialog_message

    move v1, v8

    move v4, v3

    move v3, v9

    goto :goto_5

    :cond_5
    move v3, v9

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_2
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected o()Z
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v1

    invoke-virtual {v0}, Landroid/widget/ListView;->getFooterViewsCount()I

    move-result v0

    sub-int v0, v1, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    const-wide/16 v2, -0x1

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    if-eqz p1, :cond_4

    const-string/jumbo v0, "state_is_me"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->D:Z

    const-string/jumbo v0, "state_user"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterUser;

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->aN:Lcom/twitter/library/api/TwitterUser;

    const-string/jumbo v0, "state_user_id"

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/ProfileFragment;->I:J

    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->F()V

    iget-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->D:Z

    if-eqz v0, :cond_1

    if-nez p1, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/library/api/TwitterUser;)V

    :cond_1
    new-instance v0, Lcom/twitter/android/qv;

    invoke-direct {v0, p0}, Lcom/twitter/android/qv;-><init>(Lcom/twitter/android/ProfileFragment;)V

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->O:Lcom/twitter/library/client/j;

    new-instance v0, Lcom/twitter/android/ra;

    invoke-direct {v0, p0}, Lcom/twitter/android/ra;-><init>(Lcom/twitter/android/ProfileFragment;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/library/client/z;)V

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->y:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->y:Landroid/support/v4/view/ViewPager;

    iget v1, p0, Lcom/twitter/android/ProfileFragment;->aL:I

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->k:Lcom/twitter/android/qq;

    if-eqz v0, :cond_3

    if-eqz p1, :cond_3

    const-string/jumbo v0, "prot_follow_resp"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->k:Lcom/twitter/android/qq;

    const-string/jumbo v1, "prot_follow_resp"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/qq;->a(I)V

    :cond_3
    return-void

    :cond_4
    const-string/jumbo v0, "is_me"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->D:Z

    iget-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->D:Z

    if-nez v0, :cond_0

    const-string/jumbo v0, "user"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterUser;

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->aN:Lcom/twitter/library/api/TwitterUser;

    const-string/jumbo v0, "user_id"

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/ProfileFragment;->I:J

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    const/4 v3, 0x0

    const/4 v1, 0x4

    const/4 v2, 0x1

    const/4 v0, -0x1

    const/4 v4, 0x0

    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/client/BaseListFragment;->onActivityResult(IILandroid/content/Intent;)V

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const-string/jumbo v0, "list_id"

    const-wide/16 v2, -0x1

    invoke-virtual {p3, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    const-string/jumbo v0, "user_id"

    const-wide/16 v4, -0x1

    invoke-virtual {p3, v0, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/client/c;->a(IJJ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->d(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    if-ne v0, p2, :cond_0

    if-eqz p3, :cond_0

    const-string/jumbo v0, "user_id"

    const-wide/16 v1, 0x0

    invoke-virtual {p3, v0, v1, v2}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    const-string/jumbo v2, "friendship"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "friendship"

    invoke-virtual {p3, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iget-object v3, p0, Lcom/twitter/android/ProfileFragment;->aj:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v3, v0, v1, v2}, Lcom/twitter/library/util/FriendshipCache;->a(JI)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v3, v0, v1, v2}, Lcom/twitter/library/util/FriendshipCache;->b(JI)V

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->ar:Lcom/twitter/android/widget/cg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->ar:Lcom/twitter/android/widget/cg;

    invoke-virtual {v0}, Lcom/twitter/android/widget/cg;->notifyDataSetChanged()V

    goto :goto_0

    :pswitch_2
    if-ne v0, p2, :cond_0

    if-eqz p3, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/client/cd;->a(Lcom/twitter/library/client/Session;)Lcom/twitter/library/client/v;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-boolean v1, v0, Lcom/twitter/library/client/v;->h:Z

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/twitter/library/client/v;->d:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/twitter/android/ProfileFragment;->f(Ljava/lang/String;)V

    iget-object v1, v0, Lcom/twitter/library/client/v;->e:Ljava/lang/String;

    invoke-direct {p0, v1, v3}, Lcom/twitter/android/ProfileFragment;->a(Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;)V

    iget-object v1, v0, Lcom/twitter/library/client/v;->g:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/twitter/android/ProfileFragment;->b(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/twitter/library/client/v;->f:Ljava/lang/String;

    invoke-direct {p0, v0, v3}, Lcom/twitter/android/ProfileFragment;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/qt;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/qt;

    invoke-virtual {v0}, Lcom/twitter/android/qt;->notifyDataSetChanged()V

    :cond_2
    const-string/jumbo v0, "update_header"

    invoke-virtual {p3, v0, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lcom/twitter/android/qu;

    invoke-direct {v0, p0}, Lcom/twitter/android/qu;-><init>(Lcom/twitter/android/ProfileFragment;)V

    new-array v1, v4, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/qu;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    :cond_3
    const-string/jumbo v0, "remove_header"

    invoke-virtual {p3, v0, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->j:Lcom/twitter/android/widget/ProfileHeader;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0031    # com.twitter.android.R.color.dark_gray

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/ProfileHeader;->setBackgroundColor(I)V

    goto/16 :goto_0

    :pswitch_3
    if-ne p2, v2, :cond_0

    const-string/jumbo v0, "account"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/UserAccount;

    iget-object v0, v0, Lcom/twitter/android/UserAccount;->a:Landroid/accounts/Account;

    iget-object v1, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->ah:Lcom/twitter/android/e;

    invoke-virtual {v1, v0}, Lcom/twitter/android/e;->a(Landroid/accounts/Account;)V

    goto/16 :goto_0

    :pswitch_4
    if-ne v0, p2, :cond_0

    if-eqz p3, :cond_0

    const-string/jumbo v0, "should_block"

    invoke-virtual {p3, v0, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-static {v0, v1}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    const v0, 0x7f0f0508    # com.twitter.android.R.string.unblock

    invoke-virtual {p0, v2, v0, v4}, Lcom/twitter/android/ProfileFragment;->a(ZII)V

    invoke-virtual {p0, v2}, Lcom/twitter/android/ProfileFragment;->d(Z)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 11

    const v6, 0x7f0f050b    # com.twitter.android.R.string.unfollow

    const/16 v5, 0x100

    const/4 v4, 0x3

    const/4 v10, 0x0

    const/4 v9, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->d()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/LoginActivity;->a(Landroid/app/Activity;Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v7

    invoke-interface {v7}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    const v3, 0x7f0902c2    # com.twitter.android.R.id.button_bar_action

    if-ne v2, v3, :cond_9

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-static {v0}, Lcom/twitter/library/provider/ay;->b(I)Z

    move-result v0

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-static {v0}, Lcom/twitter/library/provider/ay;->h(I)Z

    move-result v0

    if-eqz v0, :cond_3

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->f_(I)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0, v9}, Lcom/twitter/android/ProfileFragment;->f_(I)V

    goto :goto_0

    :cond_4
    iget v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-static {v0}, Lcom/twitter/library/provider/ay;->e(I)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0, v4}, Lcom/twitter/android/ProfileFragment;->f_(I)V

    goto :goto_0

    :cond_5
    iget v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-static {v0}, Lcom/twitter/library/provider/ay;->j(I)Z

    move-result v0

    if-eqz v0, :cond_6

    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->f_(I)V

    goto :goto_0

    :cond_6
    iget v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-static {v0}, Lcom/twitter/library/provider/ay;->c(I)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    iget-boolean v0, v0, Lcom/twitter/library/api/TwitterUser;->isProtected:Z

    if-eqz v0, :cond_8

    const v0, 0x7f0f02f6    # com.twitter.android.R.string.pending

    invoke-virtual {p0, v9, v0, v10}, Lcom/twitter/android/ProfileFragment;->a(ZII)V

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    const/16 v1, 0x4000

    invoke-static {v0, v1}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    :goto_1
    const-string/jumbo v1, "profile:profile:::follow"

    iget-wide v2, p0, Lcom/twitter/android/ProfileFragment;->I:J

    iget-object v4, p0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/library/api/PromotedContent;

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileFragment;->c(Lcom/twitter/library/api/TwitterUser;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/twitter/android/ProfileFragment;->aM:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ProfileFragment;->a(Ljava/lang/String;JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;)V

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_7

    const-string/jumbo v1, "profile:profile:::follow_back"

    iget-wide v2, p0, Lcom/twitter/android/ProfileFragment;->I:J

    iget-object v4, p0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/library/api/PromotedContent;

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileFragment;->c(Lcom/twitter/library/api/TwitterUser;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/twitter/android/ProfileFragment;->aM:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ProfileFragment;->a(Ljava/lang/String;JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;)V

    :cond_7
    iget-wide v1, p0, Lcom/twitter/android/ProfileFragment;->I:J

    iget-object v4, p0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/library/api/PromotedContent;

    const/4 v5, -0x1

    move-object v0, p0

    move v3, v10

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/ProfileFragment;->a(JZLcom/twitter/library/api/PromotedContent;I)V

    goto/16 :goto_0

    :cond_8
    iget v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-static {v0, v9}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-virtual {p0, v9, v6, v10}, Lcom/twitter/android/ProfileFragment;->a(ZII)V

    goto :goto_1

    :cond_9
    const v3, 0x7f0902c1    # com.twitter.android.R.id.button_bar_fav_people_follow

    if-ne v2, v3, :cond_b

    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->H()Z

    move-result v0

    if-eqz v0, :cond_a

    const/16 v0, 0xb

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->f_(I)V

    goto/16 :goto_0

    :cond_a
    invoke-direct {p0}, Lcom/twitter/android/ProfileFragment;->S()V

    const-string/jumbo v0, "profile_favorite_follow_dialog_shown"

    invoke-interface {v7, v0, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->f_(I)V

    const-string/jumbo v0, "profile_favorite_follow_dialog_shown"

    invoke-interface {v8, v0, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v8}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto/16 :goto_0

    :cond_b
    const v3, 0x7f0902c5    # com.twitter.android.R.id.button_bar_alerts

    if-ne v2, v3, :cond_f

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-static {v0}, Lcom/twitter/library/provider/ay;->k(I)Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileFragment;->b(Lcom/twitter/library/api/TwitterUser;)V

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-static {v0, v5}, Lcom/twitter/library/provider/ay;->b(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-virtual {p0, v9, v10, v9}, Lcom/twitter/android/ProfileFragment;->a(ZII)V

    const-string/jumbo v1, "profile:profile:::lifeline_unfollow"

    iget-wide v2, p0, Lcom/twitter/android/ProfileFragment;->I:J

    iget-object v4, p0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/library/api/PromotedContent;

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileFragment;->c(Lcom/twitter/library/api/TwitterUser;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/twitter/android/ProfileFragment;->aM:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ProfileFragment;->a(Ljava/lang/String;JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;)V

    const-string/jumbo v0, "lifeline_unfollow_dialog_shown"

    invoke-interface {v7, v0, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_c

    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->f_(I)V

    const-string/jumbo v0, "lifeline_unfollow_dialog_shown"

    invoke-interface {v8, v0, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v8}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto/16 :goto_0

    :cond_c
    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0f0537    # com.twitter.android.R.string.users_lifeline_unfollow_toast_message

    new-array v2, v9, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {v3}, Lcom/twitter/library/api/TwitterUser;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v10

    invoke-virtual {p0, v1, v2}, Lcom/twitter/android/ProfileFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :cond_d
    iget-wide v0, p0, Lcom/twitter/android/ProfileFragment;->I:J

    iget-object v2, p0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/library/api/PromotedContent;

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/ProfileFragment;->a(JLcom/twitter/library/api/PromotedContent;)V

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-static {v0, v5}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    const/4 v0, 0x2

    invoke-virtual {p0, v9, v6, v0}, Lcom/twitter/android/ProfileFragment;->a(ZII)V

    const-string/jumbo v1, "profile:profile:::lifeline_follow"

    iget-wide v2, p0, Lcom/twitter/android/ProfileFragment;->I:J

    iget-object v4, p0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/library/api/PromotedContent;

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileFragment;->c(Lcom/twitter/library/api/TwitterUser;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/twitter/android/ProfileFragment;->aM:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ProfileFragment;->a(Ljava/lang/String;JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;)V

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-static {v0}, Lcom/twitter/library/provider/ay;->c(I)Z

    move-result v0

    if-eqz v0, :cond_e

    const-string/jumbo v1, "profile:profile:::follow_back"

    iget-wide v2, p0, Lcom/twitter/android/ProfileFragment;->I:J

    iget-object v4, p0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/library/api/PromotedContent;

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileFragment;->c(Lcom/twitter/library/api/TwitterUser;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/twitter/android/ProfileFragment;->aM:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ProfileFragment;->a(Ljava/lang/String;JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;)V

    :cond_e
    const-string/jumbo v0, "lifeline_follow_dialog_shown"

    invoke-interface {v7, v0, v10}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->f_(I)V

    const-string/jumbo v0, "lifeline_follow_dialog_shown"

    invoke-interface {v8, v0, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v8}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto/16 :goto_0

    :cond_f
    const v3, 0x7f0902bf    # com.twitter.android.R.id.button_bar_more

    if-ne v2, v3, :cond_10

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->k(I)V

    goto/16 :goto_0

    :cond_10
    const v3, 0x7f0902c3    # com.twitter.android.R.id.button_messages

    if-ne v2, v3, :cond_11

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/android/MessagesActivity;->a(Landroid/content/Context;)Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/twitter/android/ProfileFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_11
    const v3, 0x7f0902c4    # com.twitter.android.R.id.button_switch_accounts

    if-ne v2, v3, :cond_12

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/twitter/android/AccountsDialogActivity;

    invoke-direct {v2, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v0, "account_name"

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v4}, Lcom/twitter/android/ProfileFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :cond_12
    const v1, 0x7f0902be    # com.twitter.android.R.id.button_edit_profile

    if-ne v2, v1, :cond_13

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/EditProfileActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/ProfileFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :cond_13
    const v1, 0x7f090289    # com.twitter.android.R.id.tweets_stat

    if-ne v2, v1, :cond_14

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/TimelineActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v0, "owner_id"

    iget-wide v2, p0, Lcom/twitter/android/ProfileFragment;->I:J

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "type"

    invoke-virtual {v0, v1, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "title"

    const v2, 0x7f0f032f    # com.twitter.android.R.string.profile_tab_title_timeline

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_14
    const v1, 0x7f09028a    # com.twitter.android.R.id.following_stat

    if-ne v2, v1, :cond_15

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileFragment;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_15
    const v1, 0x7f09028b    # com.twitter.android.R.id.followers_stat

    if-ne v2, v1, :cond_16

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileFragment;->b(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_16
    const v0, 0x7f0902c0    # com.twitter.android.R.id.button_bar_unmute

    if-ne v2, v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/android/ProfileFragment;->Q:J

    new-array v3, v9, [Ljava/lang/String;

    const-string/jumbo v4, "profile::tweet:muted_button:click"

    aput-object v4, v3, v10

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->B:Ljava/lang/String;

    const/16 v2, 0xd

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    invoke-static {v0, v1, v2, v3, p0}, Lcom/twitter/android/util/af;->a(Landroid/content/Context;Ljava/lang/String;ILandroid/support/v4/app/FragmentManager;Landroid/support/v4/app/Fragment;)Z

    goto/16 :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 10

    const/4 v9, 0x6

    const/4 v1, 0x1

    const-wide/16 v7, -0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string/jumbo v0, "reason"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->aP:Ljava/lang/String;

    const-string/jumbo v0, "association"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/scribe/ScribeAssociation;

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->aM:Lcom/twitter/library/scribe/ScribeAssociation;

    new-instance v0, Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/library/scribe/ScribeAssociation;-><init>()V

    const/4 v5, 0x5

    invoke-virtual {v0, v5}, Lcom/twitter/library/scribe/ScribeAssociation;->a(I)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    const-string/jumbo v5, "profile"

    invoke-virtual {v0, v5}, Lcom/twitter/library/scribe/ScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->b(Lcom/twitter/library/scribe/ScribeAssociation;)V

    new-instance v0, Lcom/twitter/android/widget/cd;

    iget-object v5, p0, Lcom/twitter/android/ProfileFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {v0, p0, v5}, Lcom/twitter/android/widget/cd;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/library/scribe/ScribeAssociation;)V

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->W:Lcom/twitter/library/view/c;

    const-string/jumbo v0, "pc"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/PromotedContent;

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/library/api/PromotedContent;

    new-instance v0, Lcom/twitter/android/qy;

    const/4 v5, 0x0

    invoke-direct {v0, p0, v5}, Lcom/twitter/android/qy;-><init>(Lcom/twitter/android/ProfileFragment;Lcom/twitter/android/qe;)V

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->aW:Lcom/twitter/library/service/a;

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->aW:Lcom/twitter/library/service/a;

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/library/service/a;)V

    if-eqz p1, :cond_1

    const-string/jumbo v0, "state_friendship_cache"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "state_friendship_cache"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/FriendshipCache;

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->aj:Lcom/twitter/library/util/FriendshipCache;

    :goto_0
    const-string/jumbo v0, "state_activity_row_id"

    invoke-virtual {p1, v0, v7, v8}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/twitter/android/ProfileFragment;->J:J

    const-string/jumbo v0, "state_friendship"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    const-string/jumbo v0, "header_page"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->aL:I

    const-string/jumbo v0, "state_re"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->E:I

    const-string/jumbo v0, "fl"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->G:I

    const-string/jumbo v0, "state_fr"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->K:Z

    const-string/jumbo v0, "state_follows"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->ac:Ljava/util/HashMap;

    const-string/jumbo v0, "state_unfollows"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->ae:Ljava/util/HashMap;

    const-string/jumbo v0, "state_scribe_item"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/scribe/ScribeItem;

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->aT:Lcom/twitter/library/scribe/ScribeItem;

    const-string/jumbo v0, "state_magic_rec_id"

    invoke-virtual {p1, v0, v7, v8}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/twitter/android/ProfileFragment;->aU:J

    :goto_1
    invoke-static {v3}, Lcom/twitter/library/platform/PushService;->c(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->aJ:Z

    invoke-virtual {p0, v1, p0}, Lcom/twitter/android/ProfileFragment;->a(ILcom/twitter/library/util/ar;)V

    iget-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->D:Z

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->N:Z

    invoke-static {}, Lgm;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->aO:Z

    invoke-static {}, Lgl;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->aQ:Z

    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0004    # com.twitter.android.R.color.border_gray

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->aR:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0056    # com.twitter.android.R.dimen.divider_thickness

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->aS:I

    return-void

    :cond_0
    new-instance v0, Lcom/twitter/library/util/FriendshipCache;

    invoke-direct {v0, v9}, Lcom/twitter/library/util/FriendshipCache;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->aj:Lcom/twitter/library/util/FriendshipCache;

    goto/16 :goto_0

    :cond_1
    const-string/jumbo v0, "scribe_item"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/scribe/ScribeItem;

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->aT:Lcom/twitter/library/scribe/ScribeItem;

    const-string/jumbo v0, "magic_rec_id"

    invoke-virtual {v4, v0, v7, v8}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v5

    iput-wide v5, p0, Lcom/twitter/android/ProfileFragment;->aU:J

    const-string/jumbo v0, "activity_row_id"

    invoke-virtual {v4, v0, v7, v8}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/twitter/android/ProfileFragment;->J:J

    new-instance v0, Lcom/twitter/library/util/FriendshipCache;

    invoke-direct {v0, v9}, Lcom/twitter/library/util/FriendshipCache;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->aj:Lcom/twitter/library/util/FriendshipCache;

    iput v2, p0, Lcom/twitter/android/ProfileFragment;->H:I

    iput v2, p0, Lcom/twitter/android/ProfileFragment;->aL:I

    iput v2, p0, Lcom/twitter/android/ProfileFragment;->E:I

    const/16 v0, 0x7f

    iput v0, p0, Lcom/twitter/android/ProfileFragment;->G:I

    iput-boolean v1, p0, Lcom/twitter/android/ProfileFragment;->aI:Z

    iput-boolean v2, p0, Lcom/twitter/android/ProfileFragment;->K:Z

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 12

    const/4 v7, 0x1

    const/4 v4, 0x0

    packed-switch p1, :pswitch_data_0

    move-object v0, v4

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/twitter/library/provider/ay;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/android/ProfileFragment;->I:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v1, "ownerId"

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    new-instance v0, Lcom/twitter/android/bl;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v3, Lcom/twitter/android/qs;->a:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/bl;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/twitter/library/provider/as;->b:Landroid/net/Uri;

    iget-wide v1, p0, Lcom/twitter/android/ProfileFragment;->I:J

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string/jumbo v0, "limit"

    const-string/jumbo v1, "3"

    invoke-virtual {v2, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v1, "ownerId"

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->m:Lcom/twitter/android/rb;

    invoke-virtual {v0, v7}, Lcom/twitter/android/rb;->a(Z)V

    new-instance v0, Landroid/support/v4/content/CursorLoader;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/twitter/library/provider/Tweet;->b:[Ljava/lang/String;

    const-string/jumbo v6, "updated_at DESC, _id ASC"

    move-object v5, v4

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    sget-object v0, Lcom/twitter/library/provider/ax;->y:Landroid/net/Uri;

    iget-wide v1, p0, Lcom/twitter/android/ProfileFragment;->I:J

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    :goto_1
    const-string/jumbo v1, "limit"

    const-string/jumbo v2, "3"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string/jumbo v2, "ownerId"

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->aj:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v1}, Lcom/twitter/library/util/FriendshipCache;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v9, "(friendship IS NULL OR (friendship & 1 == 0)) AND user_id!=?"

    new-array v10, v7, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/twitter/android/ProfileFragment;->I:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v10, v1

    :goto_2
    new-instance v5, Lcom/twitter/android/bl;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v7

    sget-object v8, Lcom/twitter/library/provider/cm;->b:[Ljava/lang/String;

    move-object v11, v4

    invoke-direct/range {v5 .. v11}, Lcom/twitter/android/bl;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v5

    goto/16 :goto_0

    :cond_0
    sget-object v0, Lcom/twitter/library/provider/ax;->z:Landroid/net/Uri;

    iget-wide v1, p0, Lcom/twitter/android/ProfileFragment;->I:J

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    goto :goto_1

    :cond_1
    move-object v10, v4

    move-object v9, v4

    goto :goto_2

    :pswitch_3
    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/library/api/UserSettings;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-boolean v0, v0, Lcom/twitter/library/api/UserSettings;->k:Z

    if-eqz v0, :cond_2

    const-string/jumbo v9, "flags&1 != 0"

    :goto_3
    sget-object v0, Lcom/twitter/library/provider/at;->r:Landroid/net/Uri;

    iget-wide v1, p0, Lcom/twitter/android/ProfileFragment;->I:J

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v1, "limit"

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string/jumbo v2, "ownerId"

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    new-instance v5, Landroid/support/v4/content/CursorLoader;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v7

    sget-object v8, Lcom/twitter/android/ud;->a:[Ljava/lang/String;

    const-string/jumbo v11, "updated_at DESC, _id ASC"

    move-object v10, v4

    invoke-direct/range {v5 .. v11}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v5

    goto/16 :goto_0

    :cond_2
    const-string/jumbo v9, "flags&1 != 0 AND flags&64 = 0"

    goto :goto_3

    :pswitch_4
    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    sget-object v2, Lcom/twitter/library/provider/ax;->s:Landroid/net/Uri;

    invoke-static {v2, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2, v0, v1}, Lcom/twitter/library/provider/w;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    new-instance v0, Landroid/support/v4/content/CursorLoader;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v3, Lcom/twitter/library/provider/cm;->a:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_5
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/provider/k;->a:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/twitter/library/provider/g;->a:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_2
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->onDestroy()V

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->w:Lcom/twitter/android/ud;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/ud;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->aW:Lcom/twitter/library/service/a;

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->b(Lcom/twitter/library/service/a;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0, p0}, Lcom/twitter/android/ProfileFragment;->b(ILcom/twitter/library/util/ar;)V

    return-void
.end method

.method public onDestroyView()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->aV:Lcom/twitter/library/util/aa;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Lcom/twitter/library/util/aa;->b(Lcom/twitter/library/util/ac;)V

    :cond_0
    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->onDestroyView()V

    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7

    sget-object v0, Lcom/twitter/library/provider/as;->d:Landroid/net/Uri;

    iget-wide v1, p0, Lcom/twitter/android/ProfileFragment;->I:J

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v1, "ownerId"

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-class v4, Lcom/twitter/android/GalleryActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "prj"

    sget-object v3, Lcom/twitter/library/provider/Tweet;->b:[Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "id"

    invoke-virtual {v0, v2, p4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "sel"

    const-string/jumbo v3, "flags&1 != 0"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "context"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "association"

    iget-object v3, p0, Lcom/twitter/android/ProfileFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    const-string/jumbo v1, "profile::media_gallery:photo:click"

    iget-wide v2, p0, Lcom/twitter/android/ProfileFragment;->I:J

    iget-object v4, p0, Lcom/twitter/android/ProfileFragment;->o:Lcom/twitter/library/api/PromotedContent;

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    invoke-direct {p0, v0}, Lcom/twitter/android/ProfileFragment;->c(Lcom/twitter/library/api/TwitterUser;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/twitter/android/ProfileFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ProfileFragment;->a(Ljava/lang/String;JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;)V

    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/ProfileFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->m:Lcom/twitter/android/rb;

    invoke-virtual {v0}, Lcom/twitter/android/rb;->d()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ye;

    invoke-virtual {v0, v1}, Lcom/twitter/android/ye;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->n:Lcom/twitter/android/qz;

    invoke-virtual {v0}, Lcom/twitter/android/qz;->d()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/rh;

    invoke-virtual {v0, v1}, Lcom/twitter/android/rh;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->w:Lcom/twitter/android/ud;

    invoke-virtual {v0, v1}, Lcom/twitter/android/ud;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->l:Lcom/twitter/android/qz;

    invoke-virtual {v0}, Lcom/twitter/android/qz;->d()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/rh;

    invoke-virtual {v0, v1}, Lcom/twitter/android/rh;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->onResume()V

    const-string/jumbo v0, "android_view_count_on_me_t_1756"

    invoke-static {v0}, Lkk;->b(Ljava/lang/String;)V

    const-string/jumbo v0, "android_rt_action_1837"

    invoke-static {v0}, Lkk;->b(Ljava/lang/String;)V

    invoke-static {}, Lgm;->c()V

    invoke-static {}, Lgl;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/ProfileFragment;->aQ:Z

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "state_is_me"

    iget-boolean v1, p0, Lcom/twitter/android/ProfileFragment;->D:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "state_user_id"

    iget-wide v1, p0, Lcom/twitter/android/ProfileFragment;->I:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string/jumbo v0, "state_re"

    iget v1, p0, Lcom/twitter/android/ProfileFragment;->E:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "fl"

    iget v1, p0, Lcom/twitter/android/ProfileFragment;->G:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "state_friendship"

    iget v1, p0, Lcom/twitter/android/ProfileFragment;->H:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "state_activity_row_id"

    iget-wide v1, p0, Lcom/twitter/android/ProfileFragment;->J:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string/jumbo v0, "state_magic_rec_id"

    iget-wide v1, p0, Lcom/twitter/android/ProfileFragment;->aU:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->aj:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v0}, Lcom/twitter/library/util/FriendshipCache;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "state_friendship_cache"

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->aj:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    iget v1, p0, Lcom/twitter/android/ProfileFragment;->H:I

    iput v1, v0, Lcom/twitter/library/api/TwitterUser;->friendship:I

    const-string/jumbo v0, "state_user"

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->x:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->y:Landroid/support/v4/view/ViewPager;

    if-eqz v0, :cond_2

    const-string/jumbo v0, "header_page"

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->y:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->j:Lcom/twitter/android/widget/ProfileHeader;

    if-eqz v0, :cond_3

    const-string/jumbo v0, "state_pfs"

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->j:Lcom/twitter/android/widget/ProfileHeader;

    invoke-virtual {v1}, Lcom/twitter/android/widget/ProfileHeader;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->k:Lcom/twitter/android/qq;

    if-eqz v0, :cond_4

    const-string/jumbo v0, "prot_follow_resp"

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->k:Lcom/twitter/android/qq;

    invoke-virtual {v1}, Lcom/twitter/android/qq;->a()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_4
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->l:Lcom/twitter/android/qz;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->l:Lcom/twitter/android/qz;

    invoke-virtual {v0}, Lcom/twitter/android/qz;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    const-string/jumbo v0, "state_fr"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_5
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->ac:Ljava/util/HashMap;

    if-eqz v0, :cond_6

    const-string/jumbo v0, "state_follows"

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->ac:Ljava/util/HashMap;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    :cond_6
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->ae:Ljava/util/HashMap;

    if-eqz v0, :cond_7

    const-string/jumbo v0, "state_unfollows"

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->ae:Ljava/util/HashMap;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    :cond_7
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->aT:Lcom/twitter/library/scribe/ScribeItem;

    if-eqz v0, :cond_8

    const-string/jumbo v0, "state_scribe_item"

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->aT:Lcom/twitter/library/scribe/ScribeItem;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_8
    return-void

    :cond_9
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->aN:Lcom/twitter/library/api/TwitterUser;

    if-eqz v0, :cond_1

    const-string/jumbo v0, "state_user"

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->aN:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 13

    const/4 v12, 0x0

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v10

    const v0, 0x7f030177    # com.twitter.android.R.layout.user_profile

    const/4 v1, 0x0

    invoke-virtual {v10, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Landroid/widget/LinearLayout;

    const v0, 0x7f0902bd    # com.twitter.android.R.id.button_bar

    invoke-virtual {v8, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Landroid/widget/RelativeLayout;

    iput-object v8, p0, Lcom/twitter/android/ProfileFragment;->ao:Landroid/widget/LinearLayout;

    const v0, 0x7f090235    # com.twitter.android.R.id.reason_byline

    invoke-virtual {v8, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->aC:Landroid/view/View;

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->aC:Landroid/view/View;

    const v1, 0x7f09009d    # com.twitter.android.R.id.text_item

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->aD:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->aP:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->aC:Landroid/view/View;

    invoke-virtual {v0, v12}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->aD:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->aP:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-wide v0, p0, Lcom/twitter/android/ProfileFragment;->J:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v11, p0, Lcom/twitter/android/ProfileFragment;->aC:Landroid/view/View;

    new-instance v0, Lcom/twitter/android/tq;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/android/ProfileFragment;->J:J

    const/16 v4, 0x13

    iget-object v5, p0, Lcom/twitter/android/ProfileFragment;->aT:Lcom/twitter/library/scribe/ScribeItem;

    iget-wide v6, p0, Lcom/twitter/android/ProfileFragment;->aU:J

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/tq;-><init>(Landroid/content/Context;JILcom/twitter/library/scribe/ScribeItem;J)V

    invoke-virtual {v11, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    const v0, 0x7f0902be    # com.twitter.android.R.id.button_edit_profile

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->ak:Landroid/widget/Button;

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->ak:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0902c2    # com.twitter.android.R.id.button_bar_action

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ShadowTextView;

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->av:Lcom/twitter/internal/android/widget/ShadowTextView;

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->av:Lcom/twitter/internal/android/widget/ShadowTextView;

    invoke-virtual {v0, p0}, Lcom/twitter/internal/android/widget/ShadowTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0902bf    # com.twitter.android.R.id.button_bar_more

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->al:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->al:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0902c3    # com.twitter.android.R.id.button_messages

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->am:Landroid/widget/Button;

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->am:Landroid/widget/Button;

    const v1, 0x7f020208    # com.twitter.android.R.drawable.ic_profile_messages

    invoke-virtual {v0, v1, v12, v12, v12}, Landroid/widget/Button;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->am:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0902c4    # com.twitter.android.R.id.button_switch_accounts

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->an:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->an:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0902c0    # com.twitter.android.R.id.button_bar_unmute

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->aw:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->aw:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0902c1    # com.twitter.android.R.id.button_bar_fav_people_follow

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->ax:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->ax:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0902c5    # com.twitter.android.R.id.button_bar_alerts

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->ay:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->ay:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iput-object v9, p0, Lcom/twitter/android/ProfileFragment;->aq:Landroid/widget/RelativeLayout;

    const v0, 0x7f0900e0    # com.twitter.android.R.id.loading

    invoke-virtual {v8, v0}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ProfileFragment;->aB:Landroid/view/View;

    const v0, 0x102000a    # android.R.id.list

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    const v1, 0x7f0902bb    # com.twitter.android.R.id.profile_header

    invoke-virtual {v8, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/widget/ProfileHeader;

    if-eqz p2, :cond_1

    const-string/jumbo v2, "state_pfs"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string/jumbo v2, "state_pfs"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/ProfileHeader;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    :cond_1
    iput-object v1, p0, Lcom/twitter/android/ProfileFragment;->j:Lcom/twitter/android/widget/ProfileHeader;

    const v2, 0x7f0900bb    # com.twitter.android.R.id.pager

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/ProfileHeader;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/v4/view/ViewPager;

    if-eqz v2, :cond_2

    new-instance v3, Lcom/twitter/android/qt;

    invoke-direct {v3, p0, v10}, Lcom/twitter/android/qt;-><init>(Lcom/twitter/android/ProfileFragment;Landroid/view/LayoutInflater;)V

    const v4, 0x7f09027c    # com.twitter.android.R.id.pager_pip

    invoke-virtual {v1, v4}, Lcom/twitter/android/widget/ProfileHeader;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/widget/PipView;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-static {v4}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v4

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    new-instance v5, Lcom/twitter/android/qg;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getParent()Landroid/view/ViewParent;

    move-result-object v6

    invoke-direct {v5, p0, v6, v4, v1}, Lcom/twitter/android/qg;-><init>(Lcom/twitter/android/ProfileFragment;Landroid/view/ViewParent;ILcom/twitter/android/widget/PipView;)V

    invoke-virtual {v2, v5}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    new-instance v4, Lcom/twitter/android/qh;

    invoke-direct {v4, p0, v1, v3, v2}, Lcom/twitter/android/qh;-><init>(Lcom/twitter/android/ProfileFragment;Lcom/twitter/android/widget/PipView;Lcom/twitter/android/qt;Landroid/support/v4/view/ViewPager;)V

    invoke-virtual {v1, v4}, Lcom/twitter/android/widget/PipView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v1, v12}, Lcom/twitter/android/widget/PipView;->setPipOnPosition(I)V

    iput-object v1, p0, Lcom/twitter/android/ProfileFragment;->g:Lcom/twitter/android/widget/PipView;

    iput-object v3, p0, Lcom/twitter/android/ProfileFragment;->c:Lcom/twitter/android/qt;

    iput-object v2, p0, Lcom/twitter/android/ProfileFragment;->y:Landroid/support/v4/view/ViewPager;

    :goto_0
    new-instance v2, Lcom/twitter/android/e;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    invoke-direct {v2, v3, v1, p0}, Lcom/twitter/android/e;-><init>(Landroid/content/Context;Landroid/view/View;Lcom/twitter/android/f;)V

    iput-object v2, p0, Lcom/twitter/android/ProfileFragment;->ah:Lcom/twitter/android/e;

    invoke-virtual {v0, v8}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    invoke-virtual {v0, v12}, Landroid/widget/ListView;->setHeaderDividersEnabled(Z)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileFragment;->l(Z)V

    return-void

    :cond_2
    const v2, 0x7f09012d    # com.twitter.android.R.id.profile_image

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/ProfileHeader;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/twitter/android/ProfileFragment;->d:Landroid/widget/ImageView;

    const v2, 0x7f090097    # com.twitter.android.R.id.name

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/ProfileHeader;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/twitter/android/ProfileFragment;->p:Landroid/widget/TextView;

    const v2, 0x7f09001f    # com.twitter.android.R.id.icon

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/ProfileHeader;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/twitter/android/ProfileFragment;->e:Landroid/widget/ImageView;

    const v2, 0x7f0902c6    # com.twitter.android.R.id.translator_icon

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/ProfileHeader;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/twitter/android/ProfileFragment;->f:Landroid/widget/ImageView;

    const v2, 0x7f0900f1    # com.twitter.android.R.id.user_name

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/ProfileHeader;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/twitter/android/ProfileFragment;->q:Landroid/widget/TextView;

    const v2, 0x7f09003e    # com.twitter.android.R.id.follows_you

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/ProfileHeader;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/twitter/android/ProfileFragment;->r:Landroid/widget/TextView;

    const v2, 0x7f09005c    # com.twitter.android.R.id.user_bio

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/ProfileHeader;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/twitter/android/ProfileFragment;->s:Landroid/widget/TextView;

    const v2, 0x7f0902c7    # com.twitter.android.R.id.user_location

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/ProfileHeader;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/twitter/android/ProfileFragment;->t:Landroid/widget/TextView;

    const v2, 0x7f0902ca    # com.twitter.android.R.id.separator

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/ProfileHeader;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/twitter/android/ProfileFragment;->v:Landroid/widget/TextView;

    const v2, 0x7f0902c8    # com.twitter.android.R.id.user_url

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/ProfileHeader;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/twitter/android/ProfileFragment;->u:Landroid/widget/TextView;

    goto/16 :goto_0
.end method

.method q()V
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    return-void
.end method

.method r()V
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/android/ProfileFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x6

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    return-void
.end method

.method public s()I
    .locals 1

    iget v0, p0, Lcom/twitter/android/ProfileFragment;->H:I

    return v0
.end method

.method u()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->t:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->C:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/ProfileFragment;->a(Landroid/widget/TextView;Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;)V

    :cond_0
    return-void
.end method

.method public u_()V
    .locals 0

    return-void
.end method

.method v()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->u:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->at:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/ProfileFragment;->a(Landroid/widget/TextView;Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;)V

    new-instance v1, Lcom/twitter/android/ql;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/ql;-><init>(Lcom/twitter/android/ProfileFragment;Landroid/widget/TextView;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method v_()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->d:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->ai:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    new-instance v1, Lcom/twitter/android/qm;

    invoke-direct {v1, p0}, Lcom/twitter/android/qm;-><init>(Lcom/twitter/android/ProfileFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method w_()V
    .locals 4

    const/16 v3, 0x8

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->e:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lcom/twitter/android/ProfileFragment;->aE:Z

    if-eqz v1, :cond_2

    const v1, 0x7f02020e    # com.twitter.android.R.drawable.ic_profile_verified

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    new-instance v1, Lcom/twitter/android/qn;

    invoke-direct {v1, p0}, Lcom/twitter/android/qn;-><init>(Lcom/twitter/android/ProfileFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->f:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    iget-boolean v1, p0, Lcom/twitter/android/ProfileFragment;->aG:Z

    if-eqz v1, :cond_4

    const v1, 0x7f02020d    # com.twitter.android.R.drawable.ic_profile_translator

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-boolean v1, p0, Lcom/twitter/android/ProfileFragment;->aF:Z

    if-eqz v1, :cond_3

    const v1, 0x7f020207    # com.twitter.android.R.drawable.ic_profile_locked

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_3
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_4
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1
.end method

.method y()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->q:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0x40

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/ProfileFragment;->B:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method z()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/ProfileFragment;->p:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->au:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->B:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/ProfileFragment;->au:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
