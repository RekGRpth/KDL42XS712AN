.class public Lcom/twitter/android/kt;
.super Lcom/twitter/android/h;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/widget/a;


# static fields
.field private static final A:[I

.field private static final B:[I

.field private static final C:[I

.field private static final D:[I

.field private static final E:[I

.field private static final F:[I

.field private static final G:[I

.field private static final H:[I

.field private static final I:[I

.field private static final J:[I

.field private static final K:[I

.field private static final L:[I

.field private static final M:[I

.field private static final N:[I

.field private static final O:[I

.field private static final P:Landroid/util/SparseArray;

.field private static final Q:Landroid/util/SparseArray;

.field private static final R:Landroid/util/SparseArray;

.field private static final S:Landroid/util/SparseIntArray;

.field private static final x:[I

.field private static final y:[I

.field private static final z:[I


# instance fields
.field private final T:Lcom/twitter/library/scribe/ScribeAssociation;

.field private final U:Lcom/twitter/android/util/v;

.field private final V:Ljava/util/ArrayList;

.field private W:Ljava/lang/String;

.field private X:J

.field public w:Z


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x6

    const/4 v5, 0x5

    const/4 v4, 0x4

    const/4 v3, 0x1

    const/4 v1, 0x7

    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/twitter/android/kt;->x:[I

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/twitter/android/kt;->y:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/twitter/android/kt;->z:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/twitter/android/kt;->A:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/twitter/android/kt;->B:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/twitter/android/kt;->C:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_6

    sput-object v0, Lcom/twitter/android/kt;->D:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_7

    sput-object v0, Lcom/twitter/android/kt;->E:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_8

    sput-object v0, Lcom/twitter/android/kt;->F:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_9

    sput-object v0, Lcom/twitter/android/kt;->G:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_a

    sput-object v0, Lcom/twitter/android/kt;->H:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_b

    sput-object v0, Lcom/twitter/android/kt;->I:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_c

    sput-object v0, Lcom/twitter/android/kt;->J:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_d

    sput-object v0, Lcom/twitter/android/kt;->K:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_e

    sput-object v0, Lcom/twitter/android/kt;->L:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_f

    sput-object v0, Lcom/twitter/android/kt;->M:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_10

    sput-object v0, Lcom/twitter/android/kt;->N:[I

    new-array v0, v1, [I

    fill-array-data v0, :array_11

    sput-object v0, Lcom/twitter/android/kt;->O:[I

    new-instance v0, Landroid/util/SparseArray;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    sput-object v0, Lcom/twitter/android/kt;->P:Landroid/util/SparseArray;

    sget-object v0, Lcom/twitter/android/kt;->P:Landroid/util/SparseArray;

    sget-object v1, Lcom/twitter/android/kt;->x:[I

    invoke-virtual {v0, v5, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/twitter/android/kt;->P:Landroid/util/SparseArray;

    const/16 v1, 0xd

    sget-object v2, Lcom/twitter/android/kt;->y:[I

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/twitter/android/kt;->P:Landroid/util/SparseArray;

    sget-object v1, Lcom/twitter/android/kt;->F:[I

    invoke-virtual {v0, v4, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/twitter/android/kt;->P:Landroid/util/SparseArray;

    const/16 v1, 0x9

    sget-object v2, Lcom/twitter/android/kt;->H:[I

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/twitter/android/kt;->P:Landroid/util/SparseArray;

    const/16 v1, 0xb

    sget-object v2, Lcom/twitter/android/kt;->J:[I

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/twitter/android/kt;->P:Landroid/util/SparseArray;

    sget-object v1, Lcom/twitter/android/kt;->z:[I

    invoke-virtual {v0, v3, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/twitter/android/kt;->P:Landroid/util/SparseArray;

    const/16 v1, 0xa

    sget-object v2, Lcom/twitter/android/kt;->B:[I

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/twitter/android/kt;->P:Landroid/util/SparseArray;

    const/16 v1, 0xc

    sget-object v2, Lcom/twitter/android/kt;->D:[I

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/twitter/android/kt;->P:Landroid/util/SparseArray;

    const/16 v1, 0x10

    sget-object v2, Lcom/twitter/android/kt;->L:[I

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/twitter/android/kt;->P:Landroid/util/SparseArray;

    const/16 v1, 0x11

    sget-object v2, Lcom/twitter/android/kt;->N:[I

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/twitter/android/kt;->P:Landroid/util/SparseArray;

    const/16 v1, 0x13

    sget-object v2, Lcom/twitter/android/kt;->x:[I

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0, v6}, Landroid/util/SparseArray;-><init>(I)V

    sput-object v0, Lcom/twitter/android/kt;->Q:Landroid/util/SparseArray;

    sget-object v0, Lcom/twitter/android/kt;->Q:Landroid/util/SparseArray;

    sget-object v1, Lcom/twitter/android/kt;->G:[I

    invoke-virtual {v0, v4, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/twitter/android/kt;->Q:Landroid/util/SparseArray;

    const/16 v1, 0x9

    sget-object v2, Lcom/twitter/android/kt;->I:[I

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/twitter/android/kt;->Q:Landroid/util/SparseArray;

    const/16 v1, 0xb

    sget-object v2, Lcom/twitter/android/kt;->K:[I

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/twitter/android/kt;->Q:Landroid/util/SparseArray;

    sget-object v1, Lcom/twitter/android/kt;->A:[I

    invoke-virtual {v0, v3, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/twitter/android/kt;->Q:Landroid/util/SparseArray;

    const/16 v1, 0xa

    sget-object v2, Lcom/twitter/android/kt;->C:[I

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/twitter/android/kt;->Q:Landroid/util/SparseArray;

    const/16 v1, 0xc

    sget-object v2, Lcom/twitter/android/kt;->E:[I

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/twitter/android/kt;->Q:Landroid/util/SparseArray;

    const/16 v1, 0x10

    sget-object v2, Lcom/twitter/android/kt;->M:[I

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/twitter/android/kt;->Q:Landroid/util/SparseArray;

    const/16 v1, 0x11

    sget-object v2, Lcom/twitter/android/kt;->O:[I

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    new-instance v0, Landroid/util/SparseIntArray;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Landroid/util/SparseIntArray;-><init>(I)V

    sput-object v0, Lcom/twitter/android/kt;->S:Landroid/util/SparseIntArray;

    sget-object v0, Lcom/twitter/android/kt;->S:Landroid/util/SparseIntArray;

    const v1, 0x7f020116    # com.twitter.android.R.drawable.ic_activity_follow_default

    invoke-virtual {v0, v5, v1}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/twitter/android/kt;->S:Landroid/util/SparseIntArray;

    const/16 v1, 0xd

    const v2, 0x7f020116    # com.twitter.android.R.drawable.ic_activity_follow_default

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/twitter/android/kt;->S:Landroid/util/SparseIntArray;

    const v1, 0x7f020114    # com.twitter.android.R.drawable.ic_activity_fave_default

    invoke-virtual {v0, v3, v1}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/twitter/android/kt;->S:Landroid/util/SparseIntArray;

    const/16 v1, 0xa

    const v2, 0x7f020114    # com.twitter.android.R.drawable.ic_activity_fave_default

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/twitter/android/kt;->S:Landroid/util/SparseIntArray;

    const/16 v1, 0xc

    const v2, 0x7f020114    # com.twitter.android.R.drawable.ic_activity_fave_default

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/twitter/android/kt;->S:Landroid/util/SparseIntArray;

    const v1, 0x7f02011c    # com.twitter.android.R.drawable.ic_activity_rt_default

    invoke-virtual {v0, v4, v1}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/twitter/android/kt;->S:Landroid/util/SparseIntArray;

    const/16 v1, 0x9

    const v2, 0x7f02011c    # com.twitter.android.R.drawable.ic_activity_rt_default

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/twitter/android/kt;->S:Landroid/util/SparseIntArray;

    const/16 v1, 0xb

    const v2, 0x7f02011c    # com.twitter.android.R.drawable.ic_activity_rt_default

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/twitter/android/kt;->S:Landroid/util/SparseIntArray;

    const/16 v1, 0x10

    const v2, 0x7f020114    # com.twitter.android.R.drawable.ic_activity_fave_default

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    sget-object v0, Lcom/twitter/android/kt;->S:Landroid/util/SparseIntArray;

    const/16 v1, 0x11

    const v2, 0x7f02011c    # com.twitter.android.R.drawable.ic_activity_rt_default

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0, v5}, Landroid/util/SparseArray;-><init>(I)V

    sput-object v0, Lcom/twitter/android/kt;->R:Landroid/util/SparseArray;

    sget-object v0, Lcom/twitter/android/kt;->R:Landroid/util/SparseArray;

    const-string/jumbo v1, "follow_moment"

    invoke-virtual {v0, v5, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/twitter/android/kt;->R:Landroid/util/SparseArray;

    const/4 v1, 0x2

    const-string/jumbo v2, "mention_moment"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/twitter/android/kt;->R:Landroid/util/SparseArray;

    const/4 v1, 0x3

    const-string/jumbo v2, "mention_moment"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/twitter/android/kt;->R:Landroid/util/SparseArray;

    const-string/jumbo v1, "favorite_moment"

    invoke-virtual {v0, v3, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/twitter/android/kt;->R:Landroid/util/SparseArray;

    const-string/jumbo v1, "retweet_moment"

    invoke-virtual {v0, v4, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    return-void

    :array_0
    .array-data 4
        0x0
        0x7f0f01aa    # com.twitter.android.R.string.followed_you_one
        0x7f0f01ae    # com.twitter.android.R.string.followed_you_two
        0x7f0f01ab    # com.twitter.android.R.string.followed_you_three
        0x7f0f01a9    # com.twitter.android.R.string.followed_you_four
        0x7f0f01aa    # com.twitter.android.R.string.followed_you_one
        0x7f0f01ac    # com.twitter.android.R.string.followed_you_three_other
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x7f0f01f9    # com.twitter.android.R.string.joined_twitter
    .end array-data

    :array_2
    .array-data 4
        0x0
        0x7f0f017e    # com.twitter.android.R.string.favorited_you_one
        0x7f0f0182    # com.twitter.android.R.string.favorited_you_two
        0x7f0f017e    # com.twitter.android.R.string.favorited_you_one
        0x7f0f017e    # com.twitter.android.R.string.favorited_you_one
        0x7f0f0180    # com.twitter.android.R.string.favorited_you_other
        0x7f0f017e    # com.twitter.android.R.string.favorited_you_one
    .end array-data

    :array_3
    .array-data 4
        0x0
        0x7f0f017f    # com.twitter.android.R.string.favorited_you_one_x_tweets
        0x7f0f0183    # com.twitter.android.R.string.favorited_you_two_x_tweets
        0x7f0f017f    # com.twitter.android.R.string.favorited_you_one_x_tweets
        0x7f0f017f    # com.twitter.android.R.string.favorited_you_one_x_tweets
        0x7f0f0181    # com.twitter.android.R.string.favorited_you_other_x_tweets
        0x7f0f017f    # com.twitter.android.R.string.favorited_you_one_x_tweets
    .end array-data

    :array_4
    .array-data 4
        0x0
        0x7f0f0184    # com.twitter.android.R.string.favorited_your_retweet_one
        0x7f0f0188    # com.twitter.android.R.string.favorited_your_retweet_two
        0x7f0f0184    # com.twitter.android.R.string.favorited_your_retweet_one
        0x7f0f0184    # com.twitter.android.R.string.favorited_your_retweet_one
        0x7f0f0186    # com.twitter.android.R.string.favorited_your_retweet_other
        0x7f0f0184    # com.twitter.android.R.string.favorited_your_retweet_one
    .end array-data

    :array_5
    .array-data 4
        0x0
        0x7f0f0185    # com.twitter.android.R.string.favorited_your_retweet_one_x_tweets
        0x7f0f0189    # com.twitter.android.R.string.favorited_your_retweet_two_x_tweets
        0x7f0f0185    # com.twitter.android.R.string.favorited_your_retweet_one_x_tweets
        0x7f0f0185    # com.twitter.android.R.string.favorited_your_retweet_one_x_tweets
        0x7f0f0187    # com.twitter.android.R.string.favorited_your_retweet_other_x_tweets
        0x7f0f0185    # com.twitter.android.R.string.favorited_your_retweet_one_x_tweets
    .end array-data

    :array_6
    .array-data 4
        0x0
        0x7f0f0173    # com.twitter.android.R.string.favorited_mention_one
        0x7f0f0177    # com.twitter.android.R.string.favorited_mention_two
        0x7f0f0173    # com.twitter.android.R.string.favorited_mention_one
        0x7f0f0173    # com.twitter.android.R.string.favorited_mention_one
        0x7f0f0175    # com.twitter.android.R.string.favorited_mention_other
        0x7f0f0173    # com.twitter.android.R.string.favorited_mention_one
    .end array-data

    :array_7
    .array-data 4
        0x0
        0x7f0f0174    # com.twitter.android.R.string.favorited_mention_one_x_tweets
        0x7f0f0178    # com.twitter.android.R.string.favorited_mention_two_x_tweets
        0x7f0f0174    # com.twitter.android.R.string.favorited_mention_one_x_tweets
        0x7f0f0174    # com.twitter.android.R.string.favorited_mention_one_x_tweets
        0x7f0f0176    # com.twitter.android.R.string.favorited_mention_other_x_tweets
        0x7f0f0174    # com.twitter.android.R.string.favorited_mention_one_x_tweets
    .end array-data

    :array_8
    .array-data 4
        0x0
        0x7f0f0378    # com.twitter.android.R.string.retweeted_you_one
        0x7f0f037c    # com.twitter.android.R.string.retweeted_you_two
        0x7f0f0378    # com.twitter.android.R.string.retweeted_you_one
        0x7f0f0378    # com.twitter.android.R.string.retweeted_you_one
        0x7f0f037a    # com.twitter.android.R.string.retweeted_you_other
        0x7f0f0378    # com.twitter.android.R.string.retweeted_you_one
    .end array-data

    :array_9
    .array-data 4
        0x0
        0x7f0f0379    # com.twitter.android.R.string.retweeted_you_one_x_tweets
        0x7f0f037d    # com.twitter.android.R.string.retweeted_you_two_x_tweets
        0x7f0f0379    # com.twitter.android.R.string.retweeted_you_one_x_tweets
        0x7f0f037b    # com.twitter.android.R.string.retweeted_you_other_x_tweets
        0x7f0f0379    # com.twitter.android.R.string.retweeted_you_one_x_tweets
    .end array-data

    :array_a
    .array-data 4
        0x0
        0x7f0f037e    # com.twitter.android.R.string.retweeted_your_retweet_one
        0x7f0f0382    # com.twitter.android.R.string.retweeted_your_retweet_two
        0x7f0f037e    # com.twitter.android.R.string.retweeted_your_retweet_one
        0x7f0f037e    # com.twitter.android.R.string.retweeted_your_retweet_one
        0x7f0f0380    # com.twitter.android.R.string.retweeted_your_retweet_other
        0x7f0f037e    # com.twitter.android.R.string.retweeted_your_retweet_one
    .end array-data

    :array_b
    .array-data 4
        0x0
        0x7f0f037f    # com.twitter.android.R.string.retweeted_your_retweet_one_x_tweets
        0x7f0f0383    # com.twitter.android.R.string.retweeted_your_retweet_two_x_tweets
        0x7f0f037f    # com.twitter.android.R.string.retweeted_your_retweet_one_x_tweets
        0x7f0f0381    # com.twitter.android.R.string.retweeted_your_retweet_other_x_tweets
        0x7f0f037f    # com.twitter.android.R.string.retweeted_your_retweet_one_x_tweets
    .end array-data

    :array_c
    .array-data 4
        0x0
        0x7f0f036d    # com.twitter.android.R.string.retweeted_mention_one
        0x7f0f0371    # com.twitter.android.R.string.retweeted_mention_two
        0x7f0f036d    # com.twitter.android.R.string.retweeted_mention_one
        0x7f0f036d    # com.twitter.android.R.string.retweeted_mention_one
        0x7f0f036f    # com.twitter.android.R.string.retweeted_mention_other
        0x7f0f036d    # com.twitter.android.R.string.retweeted_mention_one
    .end array-data

    :array_d
    .array-data 4
        0x0
        0x7f0f036e    # com.twitter.android.R.string.retweeted_mention_one_x_tweets
        0x7f0f0372    # com.twitter.android.R.string.retweeted_mention_two_x_tweets
        0x7f0f036e    # com.twitter.android.R.string.retweeted_mention_one_x_tweets
        0x7f0f0370    # com.twitter.android.R.string.retweeted_mention_other_x_tweets
        0x7f0f036e    # com.twitter.android.R.string.retweeted_mention_one_x_tweets
    .end array-data

    :array_e
    .array-data 4
        0x0
        0x7f0f016d    # com.twitter.android.R.string.favorited_media_tag_one
        0x7f0f0171    # com.twitter.android.R.string.favorited_media_tag_two
        0x7f0f016d    # com.twitter.android.R.string.favorited_media_tag_one
        0x7f0f016d    # com.twitter.android.R.string.favorited_media_tag_one
        0x7f0f016f    # com.twitter.android.R.string.favorited_media_tag_other
        0x7f0f016d    # com.twitter.android.R.string.favorited_media_tag_one
    .end array-data

    :array_f
    .array-data 4
        0x0
        0x7f0f016e    # com.twitter.android.R.string.favorited_media_tag_one_x_tweets
        0x7f0f0172    # com.twitter.android.R.string.favorited_media_tag_two_x_tweets
        0x7f0f016e    # com.twitter.android.R.string.favorited_media_tag_one_x_tweets
        0x7f0f016e    # com.twitter.android.R.string.favorited_media_tag_one_x_tweets
        0x7f0f0170    # com.twitter.android.R.string.favorited_media_tag_other_x_tweets
        0x7f0f016e    # com.twitter.android.R.string.favorited_media_tag_one_x_tweets
    .end array-data

    :array_10
    .array-data 4
        0x0
        0x7f0f0367    # com.twitter.android.R.string.retweeted_media_tag_one
        0x7f0f036b    # com.twitter.android.R.string.retweeted_media_tag_two
        0x7f0f0367    # com.twitter.android.R.string.retweeted_media_tag_one
        0x7f0f0367    # com.twitter.android.R.string.retweeted_media_tag_one
        0x7f0f0369    # com.twitter.android.R.string.retweeted_media_tag_other
        0x7f0f0367    # com.twitter.android.R.string.retweeted_media_tag_one
    .end array-data

    :array_11
    .array-data 4
        0x0
        0x7f0f0368    # com.twitter.android.R.string.retweeted_media_tag_one_x_tweets
        0x7f0f036c    # com.twitter.android.R.string.retweeted_media_tag_two_x_tweets
        0x7f0f0368    # com.twitter.android.R.string.retweeted_media_tag_one_x_tweets
        0x7f0f0368    # com.twitter.android.R.string.retweeted_media_tag_one_x_tweets
        0x7f0f036a    # com.twitter.android.R.string.retweeted_media_tag_other_x_tweets
        0x7f0f0368    # com.twitter.android.R.string.retweeted_media_tag_one_x_tweets
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;ILcom/twitter/android/client/c;Lcom/twitter/library/widget/ap;Lcom/twitter/library/widget/aa;Lcom/twitter/library/view/c;Lcom/twitter/android/o;Lcom/twitter/library/util/FriendshipCache;Lcom/twitter/android/util/v;)V
    .locals 11

    invoke-virtual {p3}, Lcom/twitter/android/client/c;->aa()Z

    move-result v4

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v5, p3

    move-object v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    invoke-direct/range {v1 .. v10}, Lcom/twitter/android/h;-><init>(Landroid/content/Context;IZLcom/twitter/android/client/c;Lcom/twitter/library/widget/ap;Lcom/twitter/library/widget/aa;Lcom/twitter/library/view/c;Lcom/twitter/android/o;Lcom/twitter/library/util/FriendshipCache;)V

    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/twitter/android/kt;->X:J

    new-instance v1, Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {v1}, Lcom/twitter/library/scribe/ScribeAssociation;-><init>()V

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeAssociation;->a(I)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v1

    const-string/jumbo v2, "connect"

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v1

    const-string/jumbo v2, "activity"

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/kt;->T:Lcom/twitter/library/scribe/ScribeAssociation;

    const/4 v1, 0x3

    iput v1, p0, Lcom/twitter/android/kt;->t:I

    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/twitter/android/kt;->U:Lcom/twitter/android/util/v;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/twitter/android/kt;->V:Ljava/util/ArrayList;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/twitter/internal/android/widget/TypefacesSpan;

    const/4 v2, 0x0

    new-instance v3, Lcom/twitter/internal/android/widget/TypefacesSpan;

    const/4 v4, 0x1

    invoke-direct {v3, p1, v4}, Lcom/twitter/internal/android/widget/TypefacesSpan;-><init>(Landroid/content/Context;I)V

    aput-object v3, v1, v2

    iput-object v1, p0, Lcom/twitter/android/kt;->r:[Lcom/twitter/internal/android/widget/TypefacesSpan;

    const/4 v1, 0x4

    new-array v1, v1, [Lcom/twitter/internal/android/widget/TypefacesSpan;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/kt;->r:[Lcom/twitter/internal/android/widget/TypefacesSpan;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Lcom/twitter/internal/android/widget/TypefacesSpan;

    const/4 v4, 0x1

    invoke-direct {v3, p1, v4}, Lcom/twitter/internal/android/widget/TypefacesSpan;-><init>(Landroid/content/Context;I)V

    aput-object v3, v1, v2

    const/4 v2, 0x2

    new-instance v3, Lcom/twitter/internal/android/widget/TypefacesSpan;

    const/4 v4, 0x1

    invoke-direct {v3, p1, v4}, Lcom/twitter/internal/android/widget/TypefacesSpan;-><init>(Landroid/content/Context;I)V

    aput-object v3, v1, v2

    const/4 v2, 0x3

    new-instance v3, Lcom/twitter/internal/android/widget/TypefacesSpan;

    const/4 v4, 0x1

    invoke-direct {v3, p1, v4}, Lcom/twitter/internal/android/widget/TypefacesSpan;-><init>(Landroid/content/Context;I)V

    aput-object v3, v1, v2

    iput-object v1, p0, Lcom/twitter/android/kt;->s:[Lcom/twitter/internal/android/widget/TypefacesSpan;

    const/4 v1, 0x3

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    iput-object v1, p0, Lcom/twitter/android/kt;->u:[I

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "state_is_teachable"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/kt;->b:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/kt;->W:Ljava/lang/String;

    return-void

    nop

    :array_0
    .array-data 4
        0x7f03003c    # com.twitter.android.R.layout.connect_social_header
        0x7f03000d    # com.twitter.android.R.layout.activity_user_with_bio_view
        0x7f03003d    # com.twitter.android.R.layout.connect_view_all
    .end array-data
.end method

.method private a(II)I
    .locals 1

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    if-le p2, p1, :cond_0

    invoke-static {p2}, Lcom/twitter/android/kt;->a(I)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-static {p1}, Lcom/twitter/android/kt;->a(I)I

    move-result v0

    goto :goto_0
.end method

.method private a(ILcom/twitter/android/ActivityCursor;)I
    .locals 1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x6

    invoke-virtual {p2, v0}, Lcom/twitter/android/ActivityCursor;->getInt(I)I

    move-result v0

    :goto_0
    return v0

    :cond_1
    const/16 v0, 0x9

    invoke-virtual {p2, v0}, Lcom/twitter/android/ActivityCursor;->getInt(I)I

    move-result v0

    goto :goto_0
.end method

.method private a(ILandroid/database/Cursor;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p2, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/kt;->w:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    check-cast p2, Lcom/twitter/android/ActivityCursor;

    const-string/jumbo v0, "android_teachable_moments_1489"

    invoke-static {v0}, Lkk;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    sget-object v0, Lcom/twitter/android/kt;->R:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_3

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string/jumbo v0, "all_moment"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move v0, v1

    :goto_1
    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/kt;->a(ILcom/twitter/android/ActivityCursor;)I

    move-result v0

    const/4 v3, 0x3

    invoke-virtual {p2, v3}, Lcom/twitter/android/ActivityCursor;->getInt(I)I

    move-result v3

    if-gt v0, v1, :cond_4

    if-gt v3, v1, :cond_4

    move v0, v1

    :goto_2
    move v2, v0

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_2
.end method

.method static synthetic i()[I
    .locals 1

    sget-object v0, Lcom/twitter/android/kt;->y:[I

    return-object v0
.end method

.method private j()Z
    .locals 4

    const-string/jumbo v0, "android_bioforward_joined_twitter_1455"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "bioforward_joined"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/view/View;Landroid/database/Cursor;)Landroid/content/Intent;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected a(Landroid/view/View;Landroid/content/res/Resources;Landroid/database/Cursor;JI[I)V
    .locals 15

    move-object/from16 v1, p3

    check-cast v1, Lcom/twitter/android/ActivityCursor;

    const/4 v2, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/16 v2, 0x12

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-direct {p0, v2, v4}, Lcom/twitter/android/kt;->a(II)I

    move-result v14

    sget-object v2, Lcom/twitter/android/ActivityCursor$ObjectField;->a:Lcom/twitter/android/ActivityCursor$ObjectField;

    invoke-virtual {p0, v1, v2}, Lcom/twitter/android/kt;->a(Lcom/twitter/android/ActivityCursor;Lcom/twitter/android/ActivityCursor$ObjectField;)Ljava/util/ArrayList;

    move-result-object v1

    const/16 v2, 0x11

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/library/provider/az;->a([B)Ljava/util/ArrayList;

    move-result-object v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {v1}, Lcom/twitter/library/api/TwitterUser;->a()J

    move-result-wide v7

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    const v5, 0x7f020116    # com.twitter.android.R.drawable.ic_activity_follow_default

    iget-wide v10, p0, Lcom/twitter/android/kt;->q:J

    move-object v1, p0

    move-object/from16 v2, p1

    move-wide/from16 v6, p4

    move/from16 v8, p6

    move-object/from16 v9, p2

    invoke-virtual/range {v1 .. v11}, Lcom/twitter/android/kt;->a(Landroid/view/View;Ljava/util/ArrayList;IIJILandroid/content/res/Resources;J)V

    const/4 v12, 0x4

    move-object v5, p0

    move-object/from16 v6, p1

    move-object/from16 v7, p2

    move-object/from16 v8, p7

    move-object/from16 v9, p7

    move-object v10, v3

    move v11, v4

    move v13, v4

    invoke-virtual/range {v5 .. v13}, Lcom/twitter/android/kt;->a(Landroid/view/View;Landroid/content/res/Resources;[I[ILjava/util/ArrayList;III)V

    move-object/from16 v0, p1

    invoke-virtual {p0, v0, v3, v14}, Lcom/twitter/android/kt;->a(Landroid/view/View;Ljava/util/ArrayList;I)V

    return-void
.end method

.method protected a(Landroid/view/View;Lcom/twitter/library/provider/Tweet;JIII)V
    .locals 2

    invoke-super/range {p0 .. p7}, Lcom/twitter/android/h;->a(Landroid/view/View;Lcom/twitter/library/provider/Tweet;JIII)V

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/j;

    iget-object v1, v0, Lcom/twitter/android/j;->d:Lcom/twitter/library/widget/TweetView;

    iget-boolean v0, p2, Lcom/twitter/library/provider/Tweet;->M:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/library/widget/TweetView;->setHighlighted(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Landroid/view/View;Lcom/twitter/library/provider/Tweet;JIIIJ)V
    .locals 3

    invoke-super/range {p0 .. p7}, Lcom/twitter/android/h;->a(Landroid/view/View;Lcom/twitter/library/provider/Tweet;JIII)V

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/j;

    iget-object v2, v0, Lcom/twitter/android/j;->d:Lcom/twitter/library/widget/TweetView;

    iget-boolean v1, p2, Lcom/twitter/library/provider/Tweet;->M:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v1}, Lcom/twitter/library/widget/TweetView;->setHighlighted(Z)V

    iget-object v0, v0, Lcom/twitter/android/j;->d:Lcom/twitter/library/widget/TweetView;

    iget-wide v1, p2, Lcom/twitter/library/provider/Tweet;->u:J

    invoke-static {p5, p8, p9, v1, v2}, Lcom/twitter/library/scribe/ScribeItem;->a(IJJ)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/TweetView;->setScribeItem(Lcom/twitter/library/scribe/ScribeItem;)V

    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected a(Landroid/view/View;Ljava/util/ArrayList;Ljava/util/ArrayList;IIIZI[I[IJILandroid/content/res/Resources;)V
    .locals 11

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/twitter/android/ad;

    iget-object v0, p0, Lcom/twitter/android/kt;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/kt;->a:Lcom/twitter/android/client/c;

    iget-boolean v9, v1, Lcom/twitter/android/client/c;->f:Z

    move-object v1, p1

    move/from16 v2, p8

    move-object v3, p2

    move-object v4, p3

    move/from16 v5, p7

    move-wide/from16 v6, p11

    move/from16 v8, p13

    invoke-static/range {v0 .. v9}, Lcom/twitter/android/ad;->a(Landroid/content/Context;Landroid/view/View;ILjava/util/ArrayList;Ljava/util/ArrayList;ZJIZ)V

    iget-object v1, v10, Lcom/twitter/android/ad;->b:Landroid/widget/TextView;

    move-object v0, p0

    move-object/from16 v2, p14

    move-object/from16 v3, p9

    move-object/from16 v4, p10

    move-object v5, p2

    move v6, p4

    move/from16 v7, p6

    move/from16 v8, p5

    invoke-virtual/range {v0 .. v8}, Lcom/twitter/android/kt;->a(Landroid/widget/TextView;Landroid/content/res/Resources;[I[ILjava/util/ArrayList;III)V

    iget-object v0, v10, Lcom/twitter/android/ad;->e:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, v10, Lcom/twitter/android/ad;->e:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0, p2}, Lcom/twitter/android/kt;->a(Landroid/view/ViewGroup;Ljava/util/ArrayList;)V

    :cond_0
    return-void
.end method

.method protected a(Landroid/widget/TextView;Landroid/content/res/Resources;[I[ILjava/util/ArrayList;III)V
    .locals 9

    invoke-virtual {p0, p5}, Lcom/twitter/android/kt;->a(Ljava/util/ArrayList;)I

    move-result v4

    if-nez v4, :cond_0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0, p1, v1, v2}, Lcom/twitter/android/kt;->a(Landroid/widget/TextView;Ljava/lang/String;[Lcom/twitter/internal/android/widget/TypefacesSpan;)V

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x4

    move/from16 v0, p7

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v5

    iget-object v1, p0, Lcom/twitter/android/kt;->a:Lcom/twitter/android/client/c;

    iget-boolean v6, v1, Lcom/twitter/android/client/c;->f:Z

    const/4 v1, 0x1

    if-gt v4, v1, :cond_3

    const/4 v1, 0x1

    move v2, v1

    :goto_1
    add-int/lit8 v7, v5, -0x1

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    move v3, v1

    :goto_2
    if-gt v3, v7, :cond_4

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v1, v4, :cond_4

    add-int/lit8 v1, v7, -0x1

    if-le v3, v1, :cond_1

    if-ne v4, v5, :cond_2

    :cond_1
    invoke-virtual {p5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/TwitterUser;

    iget-object v1, v1, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    move v2, v1

    goto :goto_1

    :cond_4
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v1

    sub-int v3, p6, v1

    const/4 v4, 0x1

    move/from16 v0, p8

    if-le v0, v4, :cond_7

    :goto_3
    packed-switch v1, :pswitch_data_0

    const/4 v1, 0x1

    aget v1, p3, v1

    :goto_4
    if-lez v3, :cond_5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    if-lez p8, :cond_6

    invoke-static/range {p8 .. p8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_6
    invoke-virtual {v8}, Ljava/util/ArrayList;->toArray()[Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p2, v1, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v6}, Lcom/twitter/library/util/Util;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    if-eqz v2, :cond_a

    iget-object v1, p0, Lcom/twitter/android/kt;->r:[Lcom/twitter/internal/android/widget/TypefacesSpan;

    :goto_5
    invoke-virtual {p0, p1, v3, v1}, Lcom/twitter/android/kt;->a(Landroid/widget/TextView;Ljava/lang/String;[Lcom/twitter/internal/android/widget/TypefacesSpan;)V

    goto :goto_0

    :cond_7
    move-object p4, p3

    goto :goto_3

    :pswitch_0
    if-lez v3, :cond_8

    const/4 v1, 0x5

    aget v1, p4, v1

    goto :goto_4

    :cond_8
    const/4 v1, 0x1

    aget v1, p4, v1

    goto :goto_4

    :pswitch_1
    const/4 v1, 0x2

    aget v1, p4, v1

    goto :goto_4

    :pswitch_2
    if-lez v3, :cond_9

    const/4 v1, 0x6

    aget v1, p4, v1

    goto :goto_4

    :cond_9
    const/4 v1, 0x3

    aget v1, p4, v1

    goto :goto_4

    :pswitch_3
    const/4 v1, 0x4

    aget v1, p4, v1

    goto :goto_4

    :cond_a
    iget-object v1, p0, Lcom/twitter/android/kt;->s:[Lcom/twitter/internal/android/widget/TypefacesSpan;

    goto :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;Z)V
    .locals 6

    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/h;->a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;Z)V

    const/4 v0, 0x1

    iget v1, p1, Lcom/twitter/library/util/ao;->g:I

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/twitter/android/kt;->V:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_0
    if-ltz v3, :cond_3

    iget-object v0, p0, Lcom/twitter/android/kt;->V:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/kv;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/kt;->V:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_0
    :goto_1
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_0

    :cond_1
    iget-object v1, v0, Lcom/twitter/android/kv;->d:Landroid/view/View;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/twitter/android/kv;->d:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, v0, Lcom/twitter/android/kv;->d:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/ku;

    iget-object v2, p0, Lcom/twitter/android/kt;->b:Lcom/twitter/library/client/aa;

    iget-wide v4, v0, Lcom/twitter/android/kv;->l:J

    invoke-virtual {v2, v4, v5}, Lcom/twitter/library/client/aa;->a(J)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/twitter/library/api/TwitterUser;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/util/ae;

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/twitter/library/util/ae;->b()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, v1, Lcom/twitter/android/ku;->b:Landroid/widget/ImageView;

    iget-object v1, v2, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v1, Landroid/graphics/Bitmap;

    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_2
    iget-object v1, v0, Lcom/twitter/android/kv;->k:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {v1}, Lcom/twitter/library/api/TwitterUser;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/util/ae;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/twitter/library/util/ae;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/twitter/android/kv;->a:Landroid/widget/ImageView;

    iget-object v0, v1, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1

    :cond_3
    return-void
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 34

    move-object/from16 v2, p3

    check-cast v2, Lcom/twitter/android/ActivityCursor;

    const/4 v3, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    const/4 v3, 0x0

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v26

    const/16 v3, 0xc

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v29

    const/16 v3, 0x10

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-nez v3, :cond_2

    const/4 v3, 0x1

    :goto_0
    if-nez v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/kt;->U:Lcom/twitter/android/util/v;

    invoke-interface {v3}, Lcom/twitter/android/util/v;->a()J

    move-result-wide v7

    cmp-long v3, v4, v7

    if-gtz v3, :cond_3

    :cond_0
    const/4 v3, 0x1

    move/from16 v30, v3

    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/kt;->U:Lcom/twitter/android/util/v;

    invoke-interface {v3, v4, v5}, Lcom/twitter/android/util/v;->a(J)V

    sget-object v3, Lcom/twitter/android/kt;->P:Landroid/util/SparseArray;

    invoke-virtual {v3, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, [I

    sget-object v3, Lcom/twitter/android/kt;->Q:Landroid/util/SparseArray;

    invoke-virtual {v3, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, [I

    sget-object v3, Lcom/twitter/android/kt;->S:Landroid/util/SparseIntArray;

    invoke-virtual {v3, v6}, Landroid/util/SparseIntArray;->get(I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/16 v31, 0x0

    const/4 v4, 0x0

    const/16 v33, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v6, v1}, Lcom/twitter/android/kt;->a(ILandroid/database/Cursor;)Z

    move-result v5

    if-eqz v5, :cond_6

    sget-object v3, Lcom/twitter/android/ActivityCursor$ObjectField;->a:Lcom/twitter/android/ActivityCursor$ObjectField;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/kt;->a(Lcom/twitter/android/ActivityCursor;Lcom/twitter/android/ActivityCursor$ObjectField;)Ljava/util/ArrayList;

    move-result-object v3

    const/4 v4, 0x1

    if-eq v6, v4, :cond_1

    const/4 v4, 0x3

    if-ne v6, v4, :cond_4

    :cond_1
    sget-object v4, Lcom/twitter/android/ActivityCursor$ObjectField;->b:Lcom/twitter/android/ActivityCursor$ObjectField;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v4}, Lcom/twitter/android/kt;->b(Lcom/twitter/android/ActivityCursor;Lcom/twitter/android/ActivityCursor$ObjectField;)Ljava/util/ArrayList;

    move-result-object v2

    :goto_2
    if-eqz v2, :cond_16

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_16

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/provider/Tweet;

    move/from16 v0, v30

    iput-boolean v0, v2, Lcom/twitter/library/provider/Tweet;->M:Z

    move-object v5, v2

    :goto_3
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/library/api/TwitterUser;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/android/kt;->a:Lcom/twitter/android/client/c;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/android/kt;->i:Lcom/twitter/library/util/FriendshipCache;

    move-object/from16 v2, p2

    move-object/from16 v3, p1

    invoke-static/range {v2 .. v8}, Lcom/twitter/android/kv;->a(Landroid/content/Context;Landroid/view/View;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/provider/Tweet;ILcom/twitter/android/client/c;Lcom/twitter/library/util/FriendshipCache;)V

    move-object/from16 v2, p1

    check-cast v2, Lcom/twitter/internal/android/widget/p;

    if-nez v30, :cond_5

    const/4 v3, 0x1

    :goto_4
    invoke-interface {v2, v3}, Lcom/twitter/internal/android/widget/p;->setHighlighted(Z)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/kt;->a:Lcom/twitter/android/client/c;

    new-instance v4, Lcom/twitter/library/scribe/ScribeLog;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/kt;->b:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v5, v2, [Ljava/lang/String;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/android/kt;->T:Lcom/twitter/library/scribe/ScribeAssociation;

    const-string/jumbo v8, "teachable_moment"

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/kv;

    iget-object v2, v2, Lcom/twitter/android/kv;->i:Ljava/lang/String;

    const-string/jumbo v9, "show"

    invoke-static {v7, v8, v2, v9}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v6

    invoke-virtual {v4, v5}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/kt;->T:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v2, v4}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :goto_5
    return-void

    :cond_2
    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_3
    const/4 v3, 0x0

    move/from16 v30, v3

    goto/16 :goto_1

    :cond_4
    sget-object v4, Lcom/twitter/android/ActivityCursor$ObjectField;->c:Lcom/twitter/android/ActivityCursor$ObjectField;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v4}, Lcom/twitter/android/kt;->b(Lcom/twitter/android/ActivityCursor;Lcom/twitter/android/ActivityCursor$ObjectField;)Ljava/util/ArrayList;

    move-result-object v2

    goto/16 :goto_2

    :cond_5
    const/4 v3, 0x0

    goto :goto_4

    :cond_6
    packed-switch v6, :pswitch_data_0

    :cond_7
    :pswitch_0
    move-object/from16 v25, v33

    move-object/from16 v24, v4

    move-object/from16 v23, v31

    :goto_6
    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v29

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/twitter/android/kt;->X:J

    move-wide/from16 v30, v0

    move-object/from16 v22, p0

    move/from16 v28, v6

    invoke-virtual/range {v22 .. v31}, Lcom/twitter/android/kt;->a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/provider/ActivityDataList;JIIJ)V

    goto :goto_5

    :pswitch_1
    invoke-virtual/range {p0 .. p3}, Lcom/twitter/android/kt;->a(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    move-object/from16 v25, v33

    move-object/from16 v24, v4

    move-object/from16 v23, v31

    goto :goto_6

    :pswitch_2
    const/16 v3, 0x13

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    move-object/from16 v0, p0

    iput-wide v3, v0, Lcom/twitter/android/kt;->X:J

    sget-object v3, Lcom/twitter/android/ActivityCursor$ObjectField;->b:Lcom/twitter/android/ActivityCursor$ObjectField;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/kt;->a(Lcom/twitter/android/ActivityCursor;Lcom/twitter/android/ActivityCursor$ObjectField;)Ljava/util/ArrayList;

    move-result-object v3

    sget-object v4, Lcom/twitter/android/ActivityCursor$ObjectField;->a:Lcom/twitter/android/ActivityCursor$ObjectField;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v4}, Lcom/twitter/android/kt;->a(Lcom/twitter/android/ActivityCursor;Lcom/twitter/android/ActivityCursor$ObjectField;)Ljava/util/ArrayList;

    move-result-object v2

    move-object/from16 v7, p1

    check-cast v7, Lcom/twitter/android/widget/ActivityUserView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/twitter/library/api/TwitterUser;

    iget-wide v3, v10, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-virtual {v7, v3, v4}, Lcom/twitter/android/widget/ActivityUserView;->setUserId(J)V

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/android/kt;->a:Lcom/twitter/android/client/c;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/android/kt;->i:Lcom/twitter/library/util/FriendshipCache;

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/twitter/android/kt;->q:J

    invoke-virtual/range {v7 .. v13}, Lcom/twitter/android/widget/ActivityUserView;->a(Lcom/twitter/android/client/c;Lcom/twitter/library/util/FriendshipCache;Lcom/twitter/library/api/TwitterUser;ZJ)V

    invoke-virtual {v7}, Lcom/twitter/android/widget/ActivityUserView;->a()V

    const v3, 0x7f0f025c    # com.twitter.android.R.string.magic_recs_follow_notif

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v5

    move-object/from16 v0, v29

    invoke-virtual {v0, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7}, Lcom/twitter/android/widget/ActivityUserView;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/r;

    iget-object v4, v10, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    iput-object v4, v2, Lcom/twitter/android/r;->c:Ljava/lang/String;

    iput-object v10, v2, Lcom/twitter/android/r;->b:Lcom/twitter/library/api/TwitterUser;

    iget-object v4, v2, Lcom/twitter/android/r;->a:Lcom/twitter/android/widget/ActivityUserView;

    invoke-virtual {v4, v3}, Lcom/twitter/android/widget/ActivityUserView;->setReason(Ljava/lang/String;)V

    move-wide/from16 v0, v26

    iput-wide v0, v2, Lcom/twitter/android/r;->d:J

    iget-object v4, v2, Lcom/twitter/android/r;->e:Lcom/twitter/library/widget/SocialBylineView;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/kt;->a:Lcom/twitter/android/client/c;

    iget-boolean v5, v5, Lcom/twitter/android/client/c;->f:Z

    invoke-virtual {v4, v5}, Lcom/twitter/library/widget/SocialBylineView;->setRenderRTL(Z)V

    iget-object v4, v7, Lcom/twitter/android/widget/ActivityUserView;->m:Lcom/twitter/library/widget/ActionButton;

    iget v5, v10, Lcom/twitter/library/api/TwitterUser;->friendship:I

    invoke-static {v5}, Lcom/twitter/library/provider/ay;->b(I)Z

    move-result v5

    invoke-virtual {v4, v5}, Lcom/twitter/library/widget/ActionButton;->setChecked(Z)V

    iget-object v4, v2, Lcom/twitter/android/r;->e:Lcom/twitter/library/widget/SocialBylineView;

    invoke-virtual {v4, v3}, Lcom/twitter/library/widget/SocialBylineView;->setLabel(Ljava/lang/CharSequence;)V

    iget-object v3, v2, Lcom/twitter/android/r;->e:Lcom/twitter/library/widget/SocialBylineView;

    const v4, 0x7f02023e    # com.twitter.android.R.drawable.ic_social_proof_recommendation_default

    invoke-virtual {v3, v4}, Lcom/twitter/library/widget/SocialBylineView;->setIcon(I)V

    iget-object v2, v2, Lcom/twitter/android/r;->e:Lcom/twitter/library/widget/SocialBylineView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/twitter/library/widget/SocialBylineView;->setVisibility(I)V

    check-cast p1, Lcom/twitter/internal/android/widget/p;

    if-nez v30, :cond_8

    const/4 v2, 0x1

    :goto_7
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Lcom/twitter/internal/android/widget/p;->setHighlighted(Z)V

    move-object/from16 v25, v33

    move-object/from16 v24, v10

    move-object/from16 v23, v31

    goto/16 :goto_6

    :cond_8
    const/4 v2, 0x0

    goto :goto_7

    :pswitch_3
    const/16 v5, 0xd

    if-ne v6, v5, :cond_9

    invoke-direct/range {p0 .. p0}, Lcom/twitter/android/kt;->j()Z

    move-result v5

    if-eqz v5, :cond_9

    sget-object v3, Lcom/twitter/android/ActivityCursor$ObjectField;->a:Lcom/twitter/android/ActivityCursor$ObjectField;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/kt;->a(Lcom/twitter/android/ActivityCursor;Lcom/twitter/android/ActivityCursor$ObjectField;)Ljava/util/ArrayList;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/library/api/TwitterUser;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/android/kt;->a:Lcom/twitter/android/client/c;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/android/kt;->i:Lcom/twitter/library/util/FriendshipCache;

    move-object/from16 v2, p2

    move-object/from16 v3, p1

    invoke-static/range {v2 .. v8}, Lcom/twitter/android/kv;->a(Landroid/content/Context;Landroid/view/View;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/provider/Tweet;ILcom/twitter/android/client/c;Lcom/twitter/library/util/FriendshipCache;)V

    :goto_8
    check-cast p1, Lcom/twitter/internal/android/widget/p;

    if-nez v30, :cond_c

    const/4 v2, 0x1

    :goto_9
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Lcom/twitter/internal/android/widget/p;->setHighlighted(Z)V

    move-object/from16 v25, v33

    move-object/from16 v24, v4

    move-object/from16 v23, v31

    goto/16 :goto_6

    :cond_9
    const/4 v5, 0x5

    if-ne v6, v5, :cond_b

    const/16 v5, 0x12

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    if-lez v5, :cond_b

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-object/from16 v9, v29

    move-object/from16 v10, p3

    move-wide/from16 v11, v26

    move v13, v6

    invoke-virtual/range {v7 .. v14}, Lcom/twitter/android/kt;->a(Landroid/view/View;Landroid/content/res/Resources;Landroid/database/Cursor;JI[I)V

    check-cast p1, Lcom/twitter/internal/android/widget/p;

    if-nez v30, :cond_a

    const/4 v2, 0x1

    :goto_a
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Lcom/twitter/internal/android/widget/p;->setHighlighted(Z)V

    move-object/from16 v25, v33

    move-object/from16 v24, v4

    move-object/from16 v23, v31

    goto/16 :goto_6

    :cond_a
    const/4 v2, 0x0

    goto :goto_a

    :cond_b
    sget-object v5, Lcom/twitter/android/ActivityCursor$ObjectField;->a:Lcom/twitter/android/ActivityCursor$ObjectField;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v5}, Lcom/twitter/android/kt;->a(Lcom/twitter/android/ActivityCursor;Lcom/twitter/android/ActivityCursor$ObjectField;)Ljava/util/ArrayList;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_15

    const/4 v2, 0x0

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/TwitterUser;

    :goto_b
    const/4 v4, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    const/16 v18, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x4

    const/16 v22, 0x0

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v23

    move-object/from16 v15, p0

    move-object/from16 v16, p1

    move-object/from16 v24, v14

    move-object/from16 v25, v14

    move/from16 v28, v6

    invoke-virtual/range {v15 .. v29}, Lcom/twitter/android/kt;->a(Landroid/view/View;Ljava/util/ArrayList;Ljava/util/ArrayList;IIIZI[I[IJILandroid/content/res/Resources;)V

    move-object v4, v2

    goto :goto_8

    :cond_c
    const/4 v2, 0x0

    goto :goto_9

    :pswitch_4
    sget-object v5, Lcom/twitter/android/ActivityCursor$ObjectField;->a:Lcom/twitter/android/ActivityCursor$ObjectField;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v5}, Lcom/twitter/android/kt;->a(Lcom/twitter/android/ActivityCursor;Lcom/twitter/android/ActivityCursor$ObjectField;)Ljava/util/ArrayList;

    move-result-object v17

    sget-object v5, Lcom/twitter/android/ActivityCursor$ObjectField;->b:Lcom/twitter/android/ActivityCursor$ObjectField;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v5}, Lcom/twitter/android/kt;->b(Lcom/twitter/android/ActivityCursor;Lcom/twitter/android/ActivityCursor$ObjectField;)Ljava/util/ArrayList;

    move-result-object v18

    const/4 v2, 0x6

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    const/4 v2, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    const/16 v21, 0x2

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/twitter/android/kt;->a(Ljava/util/ArrayList;)I

    move-result v2

    move/from16 v0, v20

    if-le v0, v2, :cond_d

    const/16 v22, 0x1

    :goto_c
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v23

    move-object/from16 v15, p0

    move-object/from16 v16, p1

    move-object/from16 v24, v14

    move/from16 v28, v6

    invoke-virtual/range {v15 .. v29}, Lcom/twitter/android/kt;->a(Landroid/view/View;Ljava/util/ArrayList;Ljava/util/ArrayList;IIIZI[I[IJILandroid/content/res/Resources;)V

    const/4 v2, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/provider/Tweet;

    move/from16 v0, v30

    iput-boolean v0, v2, Lcom/twitter/library/provider/Tweet;->M:Z

    check-cast p1, Lcom/twitter/internal/android/widget/p;

    if-nez v30, :cond_e

    const/4 v3, 0x1

    :goto_d
    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Lcom/twitter/internal/android/widget/p;->setHighlighted(Z)V

    move-object/from16 v25, v33

    move-object/from16 v24, v4

    move-object/from16 v23, v2

    goto/16 :goto_6

    :cond_d
    const/16 v22, 0x0

    goto :goto_c

    :cond_e
    const/4 v3, 0x0

    goto :goto_d

    :pswitch_5
    sget-object v5, Lcom/twitter/android/ActivityCursor$ObjectField;->a:Lcom/twitter/android/ActivityCursor$ObjectField;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v5}, Lcom/twitter/android/kt;->a(Lcom/twitter/android/ActivityCursor;Lcom/twitter/android/ActivityCursor$ObjectField;)Ljava/util/ArrayList;

    move-result-object v17

    sget-object v5, Lcom/twitter/android/ActivityCursor$ObjectField;->b:Lcom/twitter/android/ActivityCursor$ObjectField;

    sget-object v7, Lcom/twitter/android/ActivityCursor$IdType;->b:Lcom/twitter/android/ActivityCursor$IdType;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v5, v7}, Lcom/twitter/android/kt;->a(Lcom/twitter/android/ActivityCursor;Lcom/twitter/android/ActivityCursor$ObjectField;Lcom/twitter/android/ActivityCursor$IdType;)Ljava/util/ArrayList;

    move-result-object v18

    const/4 v2, 0x6

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    const/4 v2, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    const/16 v21, 0x2

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/twitter/android/kt;->a(Ljava/util/ArrayList;)I

    move-result v2

    move/from16 v0, v20

    if-le v0, v2, :cond_f

    const/16 v22, 0x1

    :goto_e
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v23

    move-object/from16 v15, p0

    move-object/from16 v16, p1

    move-object/from16 v24, v14

    move/from16 v28, v6

    invoke-virtual/range {v15 .. v29}, Lcom/twitter/android/kt;->a(Landroid/view/View;Ljava/util/ArrayList;Ljava/util/ArrayList;IIIZI[I[IJILandroid/content/res/Resources;)V

    const/4 v2, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/provider/Tweet;

    move/from16 v0, v30

    iput-boolean v0, v2, Lcom/twitter/library/provider/Tweet;->M:Z

    check-cast p1, Lcom/twitter/internal/android/widget/p;

    if-nez v30, :cond_10

    const/4 v3, 0x1

    :goto_f
    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Lcom/twitter/internal/android/widget/p;->setHighlighted(Z)V

    move-object/from16 v25, v33

    move-object/from16 v24, v4

    move-object/from16 v23, v2

    goto/16 :goto_6

    :cond_f
    const/16 v22, 0x0

    goto :goto_e

    :cond_10
    const/4 v3, 0x0

    goto :goto_f

    :pswitch_6
    sget-object v5, Lcom/twitter/android/ActivityCursor$ObjectField;->a:Lcom/twitter/android/ActivityCursor$ObjectField;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v5}, Lcom/twitter/android/kt;->a(Lcom/twitter/android/ActivityCursor;Lcom/twitter/android/ActivityCursor$ObjectField;)Ljava/util/ArrayList;

    move-result-object v17

    sget-object v5, Lcom/twitter/android/ActivityCursor$ObjectField;->c:Lcom/twitter/android/ActivityCursor$ObjectField;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v5}, Lcom/twitter/android/kt;->b(Lcom/twitter/android/ActivityCursor;Lcom/twitter/android/ActivityCursor$ObjectField;)Ljava/util/ArrayList;

    move-result-object v18

    const/16 v2, 0x9

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    const/4 v2, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v19

    const/16 v21, 0x2

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/twitter/android/kt;->a(Ljava/util/ArrayList;)I

    move-result v2

    move/from16 v0, v20

    if-le v0, v2, :cond_11

    const/16 v22, 0x1

    :goto_10
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v23

    move-object/from16 v15, p0

    move-object/from16 v16, p1

    move-object/from16 v24, v14

    move/from16 v28, v6

    invoke-virtual/range {v15 .. v29}, Lcom/twitter/android/kt;->a(Landroid/view/View;Ljava/util/ArrayList;Ljava/util/ArrayList;IIIZI[I[IJILandroid/content/res/Resources;)V

    const/4 v2, 0x0

    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/provider/Tweet;

    move/from16 v0, v30

    iput-boolean v0, v2, Lcom/twitter/library/provider/Tweet;->M:Z

    check-cast p1, Lcom/twitter/internal/android/widget/p;

    if-nez v30, :cond_12

    const/4 v3, 0x1

    :goto_11
    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Lcom/twitter/internal/android/widget/p;->setHighlighted(Z)V

    move-object/from16 v25, v33

    move-object/from16 v24, v4

    move-object/from16 v23, v2

    goto/16 :goto_6

    :cond_11
    const/16 v22, 0x0

    goto :goto_10

    :cond_12
    const/4 v3, 0x0

    goto :goto_11

    :pswitch_7
    sget-object v3, Lcom/twitter/android/ActivityCursor$ObjectField;->a:Lcom/twitter/android/ActivityCursor$ObjectField;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/kt;->a(Lcom/twitter/android/ActivityCursor;Lcom/twitter/android/ActivityCursor$ObjectField;)Ljava/util/ArrayList;

    move-result-object v10

    sget-object v3, Lcom/twitter/android/ActivityCursor$ObjectField;->c:Lcom/twitter/android/ActivityCursor$ObjectField;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/kt;->a(Landroid/database/Cursor;Lcom/twitter/android/ActivityCursor$ObjectField;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/ad;

    const/4 v3, 0x0

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/provider/ActivityDataList;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/android/kt;->mContext:Landroid/content/Context;

    const v9, 0x7f020118    # com.twitter.android.R.drawable.ic_activity_list_default

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/android/kt;->a:Lcom/twitter/android/client/c;

    iget-boolean v0, v8, Lcom/twitter/android/client/c;->f:Z

    move/from16 v16, v0

    move-object/from16 v8, p1

    move-wide/from16 v13, v26

    move v15, v6

    invoke-static/range {v7 .. v16}, Lcom/twitter/android/ad;->a(Landroid/content/Context;Landroid/view/View;ILjava/util/ArrayList;Ljava/util/ArrayList;ZJIZ)V

    iput-object v3, v2, Lcom/twitter/android/ad;->i:Lcom/twitter/library/provider/ActivityDataList;

    iget-object v7, v2, Lcom/twitter/android/ad;->b:Landroid/widget/TextView;

    const v8, 0x7f0f0210    # com.twitter.android.R.string.listed_you

    const/4 v2, 0x2

    new-array v9, v2, [Ljava/lang/Object;

    const/4 v11, 0x0

    const/4 v2, 0x0

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/TwitterUser;

    iget-object v2, v2, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    aput-object v2, v9, v11

    const/4 v10, 0x1

    const/4 v2, 0x0

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/provider/ActivityDataList;

    iget-object v2, v2, Lcom/twitter/library/provider/ActivityDataList;->name:Ljava/lang/String;

    aput-object v2, v9, v10

    move-object/from16 v0, v29

    invoke-virtual {v0, v8, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/kt;->s:[Lcom/twitter/internal/android/widget/TypefacesSpan;

    move-object/from16 v0, p0

    invoke-virtual {v0, v7, v2, v5}, Lcom/twitter/android/kt;->a(Landroid/widget/TextView;Ljava/lang/String;[Lcom/twitter/internal/android/widget/TypefacesSpan;)V

    check-cast p1, Lcom/twitter/internal/android/widget/p;

    if-nez v30, :cond_13

    const/4 v2, 0x1

    :goto_12
    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Lcom/twitter/internal/android/widget/p;->setHighlighted(Z)V

    move-object/from16 v25, v3

    move-object/from16 v24, v4

    move-object/from16 v23, v31

    goto/16 :goto_6

    :cond_13
    const/4 v2, 0x0

    goto :goto_12

    :pswitch_8
    const/16 v3, 0x13

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    move-object/from16 v0, p0

    iput-wide v7, v0, Lcom/twitter/android/kt;->X:J

    sget-object v3, Lcom/twitter/android/ActivityCursor$ObjectField;->b:Lcom/twitter/android/ActivityCursor$ObjectField;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/kt;->b(Lcom/twitter/android/ActivityCursor;Lcom/twitter/android/ActivityCursor$ObjectField;)Ljava/util/ArrayList;

    move-result-object v3

    sget-object v5, Lcom/twitter/android/ActivityCursor$ObjectField;->a:Lcom/twitter/android/ActivityCursor$ObjectField;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v5}, Lcom/twitter/android/kt;->a(Lcom/twitter/android/ActivityCursor;Lcom/twitter/android/ActivityCursor$ObjectField;)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v3, :cond_7

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_7

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_7

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/twitter/library/provider/Tweet;

    if-eqz v25, :cond_14

    move/from16 v0, v30

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/twitter/library/provider/Tweet;->M:Z

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v29

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v30

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/twitter/android/kt;->X:J

    move-wide/from16 v31, v0

    move-object/from16 v23, p0

    move-object/from16 v24, p1

    move/from16 v28, v6

    invoke-virtual/range {v23 .. v32}, Lcom/twitter/android/kt;->a(Landroid/view/View;Lcom/twitter/library/provider/Tweet;JIIIJ)V

    move-object/from16 v24, v4

    move-object/from16 v23, v25

    move-object/from16 v25, v33

    goto/16 :goto_6

    :pswitch_9
    sget-object v3, Lcom/twitter/android/ActivityCursor$ObjectField;->c:Lcom/twitter/android/ActivityCursor$ObjectField;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/kt;->b(Lcom/twitter/android/ActivityCursor;Lcom/twitter/android/ActivityCursor$ObjectField;)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_7

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/twitter/library/provider/Tweet;

    if-eqz v25, :cond_14

    move/from16 v0, v30

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/twitter/library/provider/Tweet;->M:Z

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v29

    const/16 v30, -0x1

    move-object/from16 v23, p0

    move-object/from16 v24, p1

    move/from16 v28, v6

    invoke-virtual/range {v23 .. v30}, Lcom/twitter/android/kt;->a(Landroid/view/View;Lcom/twitter/library/provider/Tweet;JIII)V

    move-object/from16 v24, v4

    move-object/from16 v23, v25

    move-object/from16 v25, v33

    goto/16 :goto_6

    :pswitch_a
    sget-object v3, Lcom/twitter/android/ActivityCursor$ObjectField;->b:Lcom/twitter/android/ActivityCursor$ObjectField;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/kt;->b(Lcom/twitter/android/ActivityCursor;Lcom/twitter/android/ActivityCursor$ObjectField;)Ljava/util/ArrayList;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v25

    check-cast v25, Lcom/twitter/library/provider/Tweet;

    move/from16 v0, v30

    move-object/from16 v1, v25

    iput-boolean v0, v1, Lcom/twitter/library/provider/Tweet;->M:Z

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v29

    const/16 v30, -0x1

    move-object/from16 v23, p0

    move-object/from16 v24, p1

    move/from16 v28, v6

    invoke-virtual/range {v23 .. v30}, Lcom/twitter/android/kt;->a(Landroid/view/View;Lcom/twitter/library/provider/Tweet;JIII)V

    move-object/from16 v24, v4

    move-object/from16 v23, v25

    move-object/from16 v25, v33

    goto/16 :goto_6

    :cond_14
    move-object/from16 v24, v4

    move-object/from16 v23, v25

    move-object/from16 v25, v33

    goto/16 :goto_6

    :cond_15
    move-object v2, v4

    goto/16 :goto_b

    :cond_16
    move-object/from16 v5, v31

    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_9
        :pswitch_a
        :pswitch_6
        :pswitch_3
        :pswitch_7
        :pswitch_0
        :pswitch_1
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_a
        :pswitch_4
        :pswitch_4
        :pswitch_8
        :pswitch_2
        :pswitch_8
    .end packed-switch
.end method

.method public getItemViewType(I)I
    .locals 8

    const/16 v3, 0x12

    const/16 v1, 0x9

    const/4 v2, 0x5

    const/4 v7, 0x3

    const/4 v6, 0x1

    invoke-virtual {p0, p1}, Lcom/twitter/android/kt;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ActivityCursor;

    invoke-virtual {v0, v6}, Lcom/twitter/android/ActivityCursor;->getInt(I)I

    move-result v4

    invoke-direct {p0, v4, v0}, Lcom/twitter/android/kt;->a(ILandroid/database/Cursor;)Z

    move-result v5

    if-eqz v5, :cond_1

    const/16 v0, 0xe

    :cond_0
    :goto_0
    return v0

    :cond_1
    packed-switch v4, :pswitch_data_0

    :pswitch_0
    invoke-super {p0, p1}, Lcom/twitter/android/h;->getItemViewType(I)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/16 v0, 0xb

    goto :goto_0

    :pswitch_1
    const/16 v0, 0xa

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_3
    const/16 v0, 0xf

    goto :goto_0

    :pswitch_4
    const/16 v0, 0x10

    goto :goto_0

    :pswitch_5
    const/16 v0, 0x11

    goto :goto_0

    :pswitch_6
    invoke-virtual {v0, v3}, Lcom/twitter/android/ActivityCursor;->getInt(I)I

    move-result v3

    if-lez v3, :cond_7

    invoke-static {v3}, Lcom/twitter/android/kt;->a(I)I

    move-result v4

    iget v5, p0, Lcom/twitter/android/kt;->t:I

    invoke-virtual {v0, v5}, Lcom/twitter/android/ActivityCursor;->getInt(I)I

    move-result v0

    if-le v0, v4, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    if-ne v4, v6, :cond_3

    move v0, v2

    goto :goto_0

    :cond_3
    const/4 v0, 0x2

    if-ne v4, v0, :cond_4

    const/4 v0, 0x6

    goto :goto_0

    :cond_4
    if-ne v4, v7, :cond_5

    if-ne v3, v7, :cond_5

    const/4 v0, 0x7

    goto :goto_0

    :cond_5
    const/4 v0, 0x4

    if-ne v4, v0, :cond_6

    const/16 v0, 0x8

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_0

    :cond_7
    move v0, v2

    goto :goto_0

    :pswitch_7
    invoke-direct {p0}, Lcom/twitter/android/kt;->j()Z

    move-result v0

    if-eqz v0, :cond_8

    const/16 v0, 0xd

    goto :goto_0

    :cond_8
    move v0, v3

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_6
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method

.method public h()V
    .locals 10

    const/4 v9, 0x5

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/twitter/android/kt;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v6

    iget-object v0, p0, Lcom/twitter/android/kt;->W:Ljava/lang/String;

    invoke-interface {v6, v0}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/kt;->getCount()I

    move-result v0

    invoke-static {v0, v9}, Ljava/lang/Math;->min(II)I

    move-result v4

    if-ge v4, v9, :cond_3

    move v1, v2

    :goto_0
    invoke-virtual {p0, v3}, Lcom/twitter/android/kt;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ActivityCursor;

    if-eqz v0, :cond_0

    move v5, v4

    move v4, v3

    :goto_1
    if-lez v5, :cond_0

    invoke-virtual {v0, v2}, Lcom/twitter/android/ActivityCursor;->getInt(I)I

    move-result v7

    const/4 v8, 0x3

    invoke-virtual {v0, v8}, Lcom/twitter/android/ActivityCursor;->getInt(I)I

    move-result v8

    invoke-direct {p0, v7, v0}, Lcom/twitter/android/kt;->a(ILcom/twitter/android/ActivityCursor;)I

    move-result v7

    invoke-static {v7, v2}, Ljava/lang/Math;->max(II)I

    move-result v7

    mul-int/2addr v7, v8

    add-int/2addr v4, v7

    if-lt v4, v9, :cond_5

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/twitter/android/ActivityCursor;->getLong(I)J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/32 v7, 0x240c8400

    sub-long/2addr v4, v7

    cmp-long v0, v0, v4

    if-gez v0, :cond_4

    :goto_2
    move v1, v2

    :cond_0
    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/kt;->W:Ljava/lang/String;

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/kt;->W:Ljava/lang/String;

    invoke-interface {v6, v0, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/kt;->w:Z

    iget-boolean v0, p0, Lcom/twitter/android/kt;->w:Z

    if-eqz v0, :cond_2

    const-string/jumbo v0, "android_teachable_moments_1489"

    invoke-static {v0}, Lkk;->b(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/android/kt;->notifyDataSetChanged()V

    :cond_2
    return-void

    :cond_3
    move v1, v3

    goto :goto_0

    :cond_4
    move v2, v3

    goto :goto_2

    :cond_5
    invoke-virtual {v0}, Lcom/twitter/android/ActivityCursor;->moveToNext()Z

    add-int/lit8 v5, v5, -0x1

    goto :goto_1
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    const v3, 0x7f03000c    # com.twitter.android.R.layout.activity_user_row_view

    const/4 v4, 0x1

    const/4 v6, 0x0

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-direct {p0, v0, p2}, Lcom/twitter/android/kt;->a(ILandroid/database/Cursor;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v0, p0, Lcom/twitter/android/kt;->V:Ljava/util/ArrayList;

    const v2, 0x7f0300d7    # com.twitter.android.R.layout.notifications_teachable_view

    invoke-static {v1, p1, v0, p0, v2}, Lcom/twitter/android/kv;->a(Landroid/view/LayoutInflater;Landroid/content/Context;Ljava/util/ArrayList;Landroid/view/View$OnClickListener;I)Landroid/view/View;

    move-result-object v1

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    packed-switch v0, :pswitch_data_0

    :pswitch_0
    invoke-virtual {p0, v1, p3}, Lcom/twitter/android/kt;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/kt;->j:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0, p1, p2, p3, v1}, Lcom/twitter/android/kt;->a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0300b2    # com.twitter.android.R.layout.magic_recs_follow_view

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/twitter/android/widget/ActivityUserView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcom/twitter/library/widget/UserView;

    const v2, 0x7f020085    # com.twitter.android.R.drawable.btn_follow_action

    invoke-virtual {v0, v2, p0}, Lcom/twitter/library/widget/UserView;->a(ILcom/twitter/library/widget/a;)V

    iget-object v2, v0, Lcom/twitter/library/widget/UserView;->m:Lcom/twitter/library/widget/ActionButton;

    const v3, 0x7f020086    # com.twitter.android.R.drawable.btn_follow_action_bg

    invoke-virtual {v2, v3}, Lcom/twitter/library/widget/ActionButton;->setBackgroundResource(I)V

    new-instance v3, Lcom/twitter/android/r;

    move-object v2, v0

    check-cast v2, Lcom/twitter/android/widget/ActivityUserView;

    invoke-direct {v3, v2}, Lcom/twitter/android/r;-><init>(Lcom/twitter/android/widget/ActivityUserView;)V

    const v2, 0x7f090059    # com.twitter.android.R.id.social_byline

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/UserView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/widget/SocialBylineView;

    iput-object v2, v3, Lcom/twitter/android/r;->e:Lcom/twitter/library/widget/SocialBylineView;

    invoke-virtual {v0, v3}, Lcom/twitter/library/widget/UserView;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v0, p0}, Lcom/twitter/library/widget/UserView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :pswitch_3
    const/16 v2, 0xd

    if-ne v0, v2, :cond_2

    invoke-direct {p0}, Lcom/twitter/android/kt;->j()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v0, p0, Lcom/twitter/android/kt;->V:Ljava/util/ArrayList;

    const v2, 0x7f03000b    # com.twitter.android.R.layout.activity_user_joined_twitter

    invoke-static {v1, p1, v0, p0, v2}, Lcom/twitter/android/kv;->a(Landroid/view/LayoutInflater;Landroid/content/Context;Ljava/util/ArrayList;Landroid/view/View$OnClickListener;I)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/twitter/android/kt;->g:Ljava/util/ArrayList;

    invoke-static {v1, v2, p0, v3, v0}, Lcom/twitter/android/ad;->a(Landroid/view/LayoutInflater;Ljava/util/ArrayList;Landroid/view/View$OnClickListener;II)Landroid/view/View;

    move-result-object v1

    goto :goto_0

    :pswitch_4
    const/16 v2, 0x12

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-lez v2, :cond_4

    const/4 v0, 0x3

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-direct {p0, v2, v0}, Lcom/twitter/android/kt;->a(II)I

    move-result v3

    if-le v0, v3, :cond_3

    :goto_1
    move-object v0, p0

    move-object v2, p1

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/kt;->a(Landroid/view/LayoutInflater;Landroid/content/Context;IZLandroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/n;

    iget-object v2, v0, Lcom/twitter/android/n;->c:Landroid/view/View;

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/twitter/android/kt;->c()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v2

    invoke-virtual {v2, v6, v6, v6, v6}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    iget-object v0, v0, Lcom/twitter/android/n;->c:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    :cond_3
    move v4, v6

    goto :goto_1

    :cond_4
    iget-object v2, p0, Lcom/twitter/android/kt;->g:Ljava/util/ArrayList;

    invoke-static {v1, v2, p0, v3, v0}, Lcom/twitter/android/ad;->a(Landroid/view/LayoutInflater;Ljava/util/ArrayList;Landroid/view/View$OnClickListener;II)Landroid/view/View;

    move-result-object v1

    goto/16 :goto_0

    :pswitch_5
    invoke-virtual {p0, v1, p3}, Lcom/twitter/android/kt;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/kt;->j:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_5
        :pswitch_2
        :pswitch_5
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 12

    const/16 v5, 0xd

    const/4 v11, 0x0

    const/4 v10, 0x0

    const/4 v8, 0x1

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    instance-of v0, v1, Lcom/twitter/android/j;

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/twitter/android/j;

    iget-object v0, v6, Lcom/twitter/android/j;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/TweetView;->getTweet()Lcom/twitter/library/provider/Tweet;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/widget/TweetView;->getScribeItem()Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v7

    iget-object v0, p0, Lcom/twitter/android/kt;->a:Lcom/twitter/android/client/c;

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v3, p0, Lcom/twitter/android/kt;->b:Lcom/twitter/library/client/aa;

    invoke-virtual {v3}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v3, v8, [Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/android/kt;->T:Lcom/twitter/library/scribe/ScribeAssociation;

    const-string/jumbo v5, "tweet"

    const-string/jumbo v8, "tweet"

    const-string/jumbo v9, "click"

    invoke-static {v4, v5, v8, v9}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v10

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/kt;->T:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v2, v11, v1, v3, v11}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/kt;->T:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    iget-object v0, v6, Lcom/twitter/android/j;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/TweetView;->getSocialContextType()I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    const-string/jumbo v2, "mention"

    :goto_0
    iget-object v0, v6, Lcom/twitter/android/j;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/TweetView;->getSocialContextType()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/kt;->p:Lcom/twitter/android/o;

    invoke-interface {v0, v1, v2}, Lcom/twitter/android/o;->a(Lcom/twitter/library/provider/Tweet;Ljava/lang/String;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const-string/jumbo v2, "reply"

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/kt;->p:Lcom/twitter/android/o;

    iget-wide v3, v6, Lcom/twitter/android/j;->a:J

    iget-object v6, v6, Lcom/twitter/android/j;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v6}, Lcom/twitter/library/widget/TweetView;->getSocialContextCount()I

    move-result v6

    iget-wide v8, p0, Lcom/twitter/android/kt;->X:J

    invoke-interface/range {v0 .. v9}, Lcom/twitter/android/o;->a(Lcom/twitter/library/provider/Tweet;Ljava/lang/String;JIILcom/twitter/library/scribe/ScribeItem;J)V

    goto :goto_1

    :cond_2
    instance-of v0, v1, Lcom/twitter/android/ku;

    if-eqz v0, :cond_5

    move-object v0, v1

    check-cast v0, Lcom/twitter/android/ku;

    iget-object v2, v0, Lcom/twitter/android/ku;->d:Lcom/twitter/library/provider/Tweet;

    iget v0, v0, Lcom/twitter/android/ku;->c:I

    if-ne v0, v5, :cond_3

    const-string/jumbo v0, "joined_twitter"

    :goto_2
    iget-object v3, p0, Lcom/twitter/android/kt;->a:Lcom/twitter/android/client/c;

    new-instance v4, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v5, p0, Lcom/twitter/android/kt;->b:Lcom/twitter/library/client/aa;

    invoke-virtual {v5}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v5

    invoke-virtual {v5}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v5, v8, [Ljava/lang/String;

    iget-object v6, p0, Lcom/twitter/android/kt;->T:Lcom/twitter/library/scribe/ScribeAssociation;

    const-string/jumbo v7, "composebox"

    const-string/jumbo v8, "click"

    invoke-static {v6, v0, v7, v8}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v10

    invoke-virtual {v4, v5}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v4, p0, Lcom/twitter/android/kt;->T:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v4}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    if-eqz v2, :cond_4

    iget-object v0, p0, Lcom/twitter/android/kt;->mContext:Landroid/content/Context;

    sget-object v1, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;->c:Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    invoke-static {v0, v1}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Landroid/content/Context;Lcom/twitter/android/composer/ComposerIntentWrapper$Action;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Lcom/twitter/library/provider/ParcelableTweet;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/kt;->b:Lcom/twitter/library/client/aa;

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Ljava/lang/String;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/kt;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/ComposerIntentWrapper;->b(Landroid/content/Context;)V

    goto :goto_1

    :cond_3
    const-string/jumbo v0, "teachable_moment"

    goto :goto_2

    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "@"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    check-cast v1, Lcom/twitter/android/ku;

    iget-object v1, v1, Lcom/twitter/android/ku;->e:Lcom/twitter/library/api/TwitterUser;

    iget-object v1, v1, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/kt;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Landroid/content/Context;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, v11}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Ljava/lang/String;[I)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/kt;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/ComposerIntentWrapper;->b(Landroid/content/Context;)V

    goto/16 :goto_1

    :cond_5
    instance-of v0, v1, Lcom/twitter/android/kv;

    if-eqz v0, :cond_9

    check-cast v1, Lcom/twitter/android/kv;

    iget-object v0, v1, Lcom/twitter/android/kv;->j:Lcom/twitter/library/provider/Tweet;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/twitter/android/kt;->p:Lcom/twitter/android/o;

    iget-object v2, v1, Lcom/twitter/android/kv;->j:Lcom/twitter/library/provider/Tweet;

    iget-object v3, v1, Lcom/twitter/android/kv;->i:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Lcom/twitter/android/o;->a(Lcom/twitter/library/provider/Tweet;Ljava/lang/String;)V

    :cond_6
    :goto_3
    iget v0, v1, Lcom/twitter/android/kv;->h:I

    if-ne v0, v5, :cond_8

    const-string/jumbo v0, "joined_twitter"

    :goto_4
    iget-object v2, p0, Lcom/twitter/android/kt;->a:Lcom/twitter/android/client/c;

    new-instance v3, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v4, p0, Lcom/twitter/android/kt;->b:Lcom/twitter/library/client/aa;

    invoke-virtual {v4}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v4, v8, [Ljava/lang/String;

    iget-object v5, p0, Lcom/twitter/android/kt;->T:Lcom/twitter/library/scribe/ScribeAssociation;

    iget-object v1, v1, Lcom/twitter/android/kv;->i:Ljava/lang/String;

    const-string/jumbo v6, "click"

    invoke-static {v5, v0, v1, v6}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v10

    invoke-virtual {v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/kt;->T:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    goto/16 :goto_1

    :cond_7
    iget-object v0, v1, Lcom/twitter/android/kv;->k:Lcom/twitter/library/api/TwitterUser;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/twitter/android/kt;->p:Lcom/twitter/android/o;

    iget-object v2, v1, Lcom/twitter/android/kv;->k:Lcom/twitter/library/api/TwitterUser;

    iget-wide v2, v2, Lcom/twitter/library/api/TwitterUser;->userId:J

    iget-object v4, v1, Lcom/twitter/android/kv;->k:Lcom/twitter/library/api/TwitterUser;

    iget-object v4, v4, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    invoke-interface {v0, v2, v3, v4}, Lcom/twitter/android/o;->a(JLjava/lang/String;)V

    goto :goto_3

    :cond_8
    const-string/jumbo v0, "teachable_moment"

    goto :goto_4

    :cond_9
    instance-of v0, v1, Lcom/twitter/android/r;

    if-eqz v0, :cond_a

    check-cast v1, Lcom/twitter/android/r;

    const/16 v0, 0x13

    iget-wide v2, p0, Lcom/twitter/android/kt;->X:J

    iget-object v4, v1, Lcom/twitter/android/r;->b:Lcom/twitter/library/api/TwitterUser;

    iget-wide v4, v4, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-static {v0, v2, v3, v4, v5}, Lcom/twitter/library/scribe/ScribeItem;->a(IJJ)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/twitter/android/kt;->mContext:Landroid/content/Context;

    const-class v4, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v3, "user_id"

    iget-object v4, v1, Lcom/twitter/android/r;->b:Lcom/twitter/library/api/TwitterUser;

    iget-wide v4, v4, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "screen_name"

    iget-object v4, v1, Lcom/twitter/android/r;->b:Lcom/twitter/library/api/TwitterUser;

    iget-object v4, v4, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "reason"

    iget-object v4, v1, Lcom/twitter/android/r;->a:Lcom/twitter/android/widget/ActivityUserView;

    invoke-virtual {v4}, Lcom/twitter/android/widget/ActivityUserView;->getReason()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "activity_row_id"

    iget-wide v4, v1, Lcom/twitter/android/r;->d:J

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "scribe_item"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "magic_rec_id"

    iget-wide v3, p0, Lcom/twitter/android/kt;->X:J

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/kt;->a:Lcom/twitter/android/client/c;

    new-instance v3, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v4, p0, Lcom/twitter/android/kt;->b:Lcom/twitter/library/client/aa;

    invoke-virtual {v4}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v4, v8, [Ljava/lang/String;

    iget-object v5, p0, Lcom/twitter/android/kt;->T:Lcom/twitter/library/scribe/ScribeAssociation;

    const-string/jumbo v6, "user_module"

    const-string/jumbo v7, "user"

    const-string/jumbo v8, "profile_click"

    invoke-static {v5, v6, v7, v8}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v10

    invoke-virtual {v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/kt;->T:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    iget-object v0, p0, Lcom/twitter/android/kt;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    :cond_a
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0901fe    # com.twitter.android.R.id.favorite_container

    if-ne v0, v1, :cond_b

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/kt;->mContext:Landroid/content/Context;

    const-class v2, Lcom/twitter/android/ActivitiesActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "activity_type"

    invoke-virtual {v0, v1, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/kt;->a:Lcom/twitter/android/client/c;

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v3, p0, Lcom/twitter/android/kt;->b:Lcom/twitter/library/client/aa;

    invoke-virtual {v3}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v3, v8, [Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/android/kt;->T:Lcom/twitter/library/scribe/ScribeAssociation;

    const-string/jumbo v5, "teachable_moment"

    const-string/jumbo v6, "favorited"

    const-string/jumbo v7, "view_all"

    invoke-static {v4, v5, v6, v7}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v10

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/kt;->T:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    iget-object v1, p0, Lcom/twitter/android/kt;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    :cond_b
    invoke-super {p0, p1}, Lcom/twitter/android/h;->onClick(Landroid/view/View;)V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x12
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic onClick(Lcom/twitter/library/widget/BaseUserView;JI)V
    .locals 0

    check-cast p1, Lcom/twitter/library/widget/UserView;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/twitter/android/kt;->onClick(Lcom/twitter/library/widget/UserView;JI)V

    return-void
.end method

.method public onClick(Lcom/twitter/library/widget/UserView;JI)V
    .locals 8

    const/4 v7, 0x0

    iget-object v1, p0, Lcom/twitter/android/kt;->a:Lcom/twitter/android/client/c;

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserView;->getPromotedContent()Lcom/twitter/library/api/PromotedContent;

    move-result-object v0

    iget-object v2, p1, Lcom/twitter/library/widget/UserView;->m:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v2}, Lcom/twitter/library/widget/ActionButton;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, p2, p3, v0}, Lcom/twitter/android/client/c;->a(JLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/kt;->i:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v0, p2, p3}, Lcom/twitter/library/util/FriendshipCache;->e(J)V

    const-string/jumbo v0, "unfollow"

    :goto_0
    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v3, p0, Lcom/twitter/android/kt;->b:Lcom/twitter/library/client/aa;

    invoke-virtual {v3}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/android/kt;->T:Lcom/twitter/library/scribe/ScribeAssociation;

    const-string/jumbo v5, "user_module"

    const-string/jumbo v6, "user"

    invoke-static {v4, v5, v6, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v7

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    const/16 v2, 0x13

    iget-wide v3, p0, Lcom/twitter/android/kt;->X:J

    invoke-static {v2, v3, v4, p2, p3}, Lcom/twitter/library/scribe/ScribeItem;->a(IJJ)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void

    :cond_0
    invoke-virtual {v1, p2, p3, v7, v0}, Lcom/twitter/android/client/c;->a(JZLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/kt;->i:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v0, p2, p3}, Lcom/twitter/library/util/FriendshipCache;->b(J)V

    const-string/jumbo v0, "follow"

    goto :goto_0
.end method
