.class Lcom/twitter/android/rc;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/PromptView;


# direct methods
.method constructor <init>(Lcom/twitter/android/PromptView;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/rc;->a:Lcom/twitter/android/PromptView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    const/4 v7, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Landroid/content/Context;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v1

    const/high16 v2, 0x14000000

    invoke-virtual {v1, v2}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(I)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v1

    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    iget-object v3, p0, Lcom/twitter/android/rc;->a:Lcom/twitter/android/PromptView;

    invoke-static {v3}, Lcom/twitter/android/PromptView;->a(Lcom/twitter/android/PromptView;)Lcom/twitter/library/api/Prompt;

    move-result-object v3

    iget-object v3, v3, Lcom/twitter/library/api/Prompt;->j:[I

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v4

    aget v3, v3, v4

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/composer/ComposerIntentWrapper;->b(Landroid/net/Uri;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/composer/ComposerIntentWrapper;->b(Landroid/content/Context;)V

    invoke-static {v0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    invoke-static {v0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/twitter/android/rc;->a:Lcom/twitter/android/PromptView;

    invoke-static {v6}, Lcom/twitter/android/PromptView;->b(Lcom/twitter/android/PromptView;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "::photo_prompt::click"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/rc;->a:Lcom/twitter/android/PromptView;

    invoke-virtual {v0, v7, v7}, Lcom/twitter/android/PromptView;->a(Lcom/twitter/library/api/Prompt;Landroid/graphics/Bitmap;)V

    return-void
.end method
