.class Lcom/twitter/android/hr;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/ht;


# instance fields
.field final synthetic a:Lcom/twitter/android/hq;


# direct methods
.method constructor <init>(Lcom/twitter/android/hq;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/hr;->a:Lcom/twitter/android/hq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/maps/model/k;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/hr;->a:Lcom/twitter/android/hq;

    iget-object v0, v0, Lcom/twitter/android/hq;->a:Lcom/twitter/android/GeoDebugActivity;

    iget-object v0, v0, Lcom/twitter/android/GeoDebugActivity;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/hr;->a:Lcom/twitter/android/hq;

    iget-object v0, v0, Lcom/twitter/android/hq;->a:Lcom/twitter/android/GeoDebugActivity;

    iget-object v0, v0, Lcom/twitter/android/GeoDebugActivity;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ib;

    iget-object v1, p0, Lcom/twitter/android/hr;->a:Lcom/twitter/android/hq;

    iget-object v1, v1, Lcom/twitter/android/hq;->a:Lcom/twitter/android/GeoDebugActivity;

    iget-object v1, v1, Lcom/twitter/android/GeoDebugActivity;->c:Lcom/twitter/library/platform/m;

    iget-object v2, v0, Lcom/twitter/android/ib;->a:Landroid/location/Location;

    invoke-virtual {v1, v2}, Lcom/twitter/library/platform/m;->c(Landroid/location/Location;)V

    iget-object v0, v0, Lcom/twitter/android/ib;->b:Lcom/google/android/gms/maps/model/e;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/model/e;->a()V

    invoke-virtual {p1}, Lcom/google/android/gms/maps/model/k;->a()V

    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/gms/maps/model/k;Landroid/location/Location;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/hr;->a:Lcom/twitter/android/hq;

    iget-object v0, v0, Lcom/twitter/android/hq;->a:Lcom/twitter/android/GeoDebugActivity;

    iget-object v0, v0, Lcom/twitter/android/GeoDebugActivity;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/hr;->a:Lcom/twitter/android/hq;

    iget-object v0, v0, Lcom/twitter/android/hq;->a:Lcom/twitter/android/GeoDebugActivity;

    iget-object v0, v0, Lcom/twitter/android/GeoDebugActivity;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ib;

    iget-object v1, p0, Lcom/twitter/android/hr;->a:Lcom/twitter/android/hq;

    iget-object v1, v1, Lcom/twitter/android/hq;->a:Lcom/twitter/android/GeoDebugActivity;

    iget-object v1, v1, Lcom/twitter/android/GeoDebugActivity;->c:Lcom/twitter/library/platform/m;

    iget-object v2, v0, Lcom/twitter/android/ib;->a:Landroid/location/Location;

    invoke-virtual {v1, v2, p2}, Lcom/twitter/library/platform/m;->a(Landroid/location/Location;Landroid/location/Location;)V

    iput-object p2, v0, Lcom/twitter/android/ib;->a:Landroid/location/Location;

    iget-object v1, v0, Lcom/twitter/android/ib;->b:Lcom/google/android/gms/maps/model/e;

    iget-object v0, v0, Lcom/twitter/android/ib;->a:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    float-to-double v2, v0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/maps/model/e;->a(D)V

    :cond_0
    return-void
.end method
