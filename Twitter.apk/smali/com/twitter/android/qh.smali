.class Lcom/twitter/android/qh;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/widget/PipView;

.field final synthetic b:Lcom/twitter/android/qt;

.field final synthetic c:Landroid/support/v4/view/ViewPager;

.field final synthetic d:Lcom/twitter/android/ProfileFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/ProfileFragment;Lcom/twitter/android/widget/PipView;Lcom/twitter/android/qt;Landroid/support/v4/view/ViewPager;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/qh;->d:Lcom/twitter/android/ProfileFragment;

    iput-object p2, p0, Lcom/twitter/android/qh;->a:Lcom/twitter/android/widget/PipView;

    iput-object p3, p0, Lcom/twitter/android/qh;->b:Lcom/twitter/android/qt;

    iput-object p4, p0, Lcom/twitter/android/qh;->c:Landroid/support/v4/view/ViewPager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    const/4 v3, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/twitter/android/qh;->a:Lcom/twitter/android/widget/PipView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/PipView;->getPipOnPosition()I

    move-result v0

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/qh;->b:Lcom/twitter/android/qt;

    invoke-virtual {v0}, Lcom/twitter/android/qt;->getCount()I

    move-result v0

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lcom/twitter/android/qh;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v2, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    iget-object v0, p0, Lcom/twitter/android/qh;->a:Lcom/twitter/android/widget/PipView;

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/PipView;->setPipOnPosition(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/qh;->b:Lcom/twitter/android/qt;

    invoke-virtual {v0}, Lcom/twitter/android/qt;->getCount()I

    move-result v0

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lcom/twitter/android/qh;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    iget-object v0, p0, Lcom/twitter/android/qh;->a:Lcom/twitter/android/widget/PipView;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PipView;->setPipOnPosition(I)V

    goto :goto_0
.end method
