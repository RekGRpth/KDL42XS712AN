.class final Lcom/twitter/android/k;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:Landroid/widget/TextView;

.field public final b:Landroid/view/ViewGroup;

.field public c:J

.field public d:I


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7f090079    # com.twitter.android.R.id.view_all_text

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/k;->a:Landroid/widget/TextView;

    const v0, 0x7f090078    # com.twitter.android.R.id.user_images_container

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/k;->b:Landroid/view/ViewGroup;

    return-void
.end method

.method public static a(Landroid/view/LayoutInflater;Landroid/view/View$OnClickListener;ILjava/util/ArrayList;)Landroid/view/View;
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/k;

    invoke-direct {v1, v0}, Lcom/twitter/android/k;-><init>(Landroid/view/View;)V

    iget-object v2, v1, Lcom/twitter/android/k;->b:Landroid/view/ViewGroup;

    if-eqz v2, :cond_0

    new-instance v2, Ljava/lang/ref/WeakReference;

    iget-object v3, v1, Lcom/twitter/android/k;->b:Landroid/view/ViewGroup;

    invoke-direct {v2, v3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {p3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method public static a(Landroid/view/View;IJILandroid/content/res/Resources;)V
    .locals 1

    invoke-virtual {p0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/k;

    iput-wide p2, v0, Lcom/twitter/android/k;->c:J

    iput p4, v0, Lcom/twitter/android/k;->d:I

    return-void
.end method
