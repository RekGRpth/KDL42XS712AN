.class Lcom/twitter/android/it;
.super Lcom/twitter/library/client/j;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/InviteFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/InviteFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/it;->a:Lcom/twitter/android/InviteFragment;

    invoke-direct {p0}, Lcom/twitter/library/client/j;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;IIILjava/util/ArrayList;[Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/it;->a:Lcom/twitter/android/InviteFragment;

    invoke-static {v0, p2}, Lcom/twitter/android/InviteFragment;->a(Lcom/twitter/android/InviteFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/it;->a:Lcom/twitter/android/InviteFragment;

    invoke-static {v0}, Lcom/twitter/android/InviteFragment;->a(Lcom/twitter/android/InviteFragment;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/it;->a:Lcom/twitter/android/InviteFragment;

    invoke-static {v0}, Lcom/twitter/android/InviteFragment;->b(Lcom/twitter/android/InviteFragment;)Lcom/twitter/android/client/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/android/client/c;->ae()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/android/it;->a:Lcom/twitter/android/InviteFragment;

    invoke-static {v0}, Lcom/twitter/android/InviteFragment;->c(Lcom/twitter/android/InviteFragment;)Lcom/twitter/android/iq;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/iq;->c()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_0
    iget-object v3, p0, Lcom/twitter/android/it;->a:Lcom/twitter/android/InviteFragment;

    invoke-static {v3}, Lcom/twitter/android/InviteFragment;->d(Lcom/twitter/android/InviteFragment;)Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v2}, Lcom/twitter/android/client/c;->F()Ljava/util/regex/Pattern;

    move-result-object v4

    invoke-static {p8, v3, v4, v0}, Lcom/twitter/android/cx;->a(Ljava/util/List;Ljava/util/Map;Ljava/util/regex/Pattern;Z)Ljava/util/ArrayList;

    move-result-object v3

    iget-object v0, p0, Lcom/twitter/android/it;->a:Lcom/twitter/android/InviteFragment;

    invoke-static {v0}, Lcom/twitter/android/InviteFragment;->e(Lcom/twitter/android/InviteFragment;)Lcom/twitter/android/aag;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/it;->a:Lcom/twitter/android/InviteFragment;

    invoke-static {v0}, Lcom/twitter/android/InviteFragment;->e(Lcom/twitter/android/InviteFragment;)Lcom/twitter/android/aag;

    move-result-object v0

    invoke-interface {v0, p5, p6, p7, v3}, Lcom/twitter/android/aag;->a(IIILjava/util/ArrayList;)V

    :cond_0
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_1

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    iget-object v1, p0, Lcom/twitter/android/it;->a:Lcom/twitter/android/InviteFragment;

    invoke-static {v1}, Lcom/twitter/android/InviteFragment;->c(Lcom/twitter/android/InviteFragment;)Lcom/twitter/android/iq;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/iq;->notifyDataSetChanged()V

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/it;->a:Lcom/twitter/android/InviteFragment;

    const/4 v3, 0x3

    invoke-static {v1, v3}, Lcom/twitter/android/InviteFragment;->a(Lcom/twitter/android/InviteFragment;I)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/android/it;->a:Lcom/twitter/android/InviteFragment;

    const/4 v3, 0x0

    invoke-static {v1, v3}, Lcom/twitter/android/InviteFragment;->a(Lcom/twitter/android/InviteFragment;Z)Z

    iget-object v1, p0, Lcom/twitter/android/it;->a:Lcom/twitter/android/InviteFragment;

    invoke-static {v1}, Lcom/twitter/android/InviteFragment;->f(Lcom/twitter/android/InviteFragment;)V

    :cond_2
    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/twitter/android/it;->a:Lcom/twitter/android/InviteFragment;

    invoke-static {v5}, Lcom/twitter/android/InviteFragment;->g(Lcom/twitter/android/InviteFragment;)Lcom/twitter/android/FollowFlowController;

    move-result-object v5

    invoke-virtual {v5}, Lcom/twitter/android/FollowFlowController;->g()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "invite_contacts"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string/jumbo v5, "stream"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string/jumbo v5, ""

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string/jumbo v5, "results"

    aput-object v5, v3, v4

    invoke-virtual {v2, v0, v1, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/it;->a:Lcom/twitter/android/InviteFragment;

    invoke-static {v0}, Lcom/twitter/android/InviteFragment;->h(Lcom/twitter/android/InviteFragment;)Lcom/twitter/android/ir;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/android/ir;->b()V

    :cond_4
    return-void

    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_6
    const/4 v0, 0x0

    goto :goto_1
.end method
