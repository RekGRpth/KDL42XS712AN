.class public Lcom/twitter/android/FilterActivity;
.super Lcom/twitter/android/client/BaseFragmentActivity;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnTouchListener;
.implements Lcom/twitter/android/gm;
.implements Lcom/twitter/library/util/ar;


# static fields
.field protected static final a:I

.field private static final e:[I


# instance fields
.field private A:Lcom/twitter/android/gc;

.field private B:Landroid/view/View;

.field private C:Landroid/view/ViewGroup;

.field private E:Landroid/view/animation/AnimationSet;

.field private F:Z

.field private G:Landroid/widget/ImageView;

.field private H:Landroid/widget/ImageView;

.field private I:Z

.field private final J:[Z

.field private K:I

.field private L:Z

.field private M:Landroid/widget/TextView;

.field private N:I

.field private O:I

.field private P:I

.field private Q:Ljava/lang/String;

.field private final R:Ljava/lang/Runnable;

.field b:Lcom/twitter/android/FilterManager;

.field c:Landroid/view/animation/Animation;

.field d:Landroid/view/animation/Animation;

.field private f:Landroid/net/Uri;

.field private g:Landroid/net/Uri;

.field private h:Lcom/twitter/android/ge;

.field private i:Landroid/view/ViewGroup;

.field private j:Z

.field private k:I

.field private l:Landroid/support/v4/view/ViewPager;

.field private m:Landroid/view/ViewGroup;

.field private n:I

.field private o:Z

.field private p:Z

.field private q:Landroid/widget/ImageButton;

.field private r:Landroid/widget/ImageButton;

.field private s:Landroid/widget/ImageButton;

.field private t:Landroid/widget/GridView;

.field private u:Landroid/view/ViewGroup;

.field private v:Lcom/twitter/android/gb;

.field private w:Lcom/twitter/android/gd;

.field private x:I

.field private y:Z

.field private z:Lcom/twitter/android/widget/PipView;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/16 v1, 0x9

    sget-object v0, Lcom/twitter/library/util/n;->a:[I

    array-length v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    sput v0, Lcom/twitter/android/FilterActivity;->a:I

    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/twitter/android/FilterActivity;->e:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f0f0193    # com.twitter.android.R.string.filter_name_no_filter
        0x7f0f0194    # com.twitter.android.R.string.filter_name_vignette
        0x7f0f0195    # com.twitter.android.R.string.filter_name_warm
        0x7f0f0191    # com.twitter.android.R.string.filter_name_cool
        0x7f0f018d    # com.twitter.android.R.string.filter_name_1963
        0x7f0f018e    # com.twitter.android.R.string.filter_name_1972
        0x7f0f0192    # com.twitter.android.R.string.filter_name_golden_hour
        0x7f0f018f    # com.twitter.android.R.string.filter_name_antique
        0x7f0f0190    # com.twitter.android.R.string.filter_name_bw
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;-><init>()V

    sget v0, Lcom/twitter/android/FilterActivity;->a:I

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/twitter/android/FilterActivity;->J:[Z

    new-instance v0, Lcom/twitter/android/fz;

    invoke-direct {v0, p0}, Lcom/twitter/android/fz;-><init>(Lcom/twitter/android/FilterActivity;)V

    iput-object v0, p0, Lcom/twitter/android/FilterActivity;->R:Ljava/lang/Runnable;

    return-void
.end method

.method private a(Landroid/graphics/Rect;Landroid/graphics/RectF;Z)Landroid/view/animation/AnimationSet;
    .locals 12

    const/high16 v8, 0x40000000    # 2.0f

    const/4 v11, 0x0

    new-instance v10, Landroid/view/animation/AnimationSet;

    invoke-direct {v10, v11}, Landroid/view/animation/AnimationSet;-><init>(Z)V

    if-eqz p3, :cond_0

    new-instance v0, Landroid/view/animation/ScaleAnimation;

    iget v1, p2, Landroid/graphics/RectF;->left:F

    iget v2, p2, Landroid/graphics/RectF;->right:F

    iget v3, p2, Landroid/graphics/RectF;->top:F

    iget v4, p2, Landroid/graphics/RectF;->bottom:F

    iget v5, p2, Landroid/graphics/RectF;->right:F

    iget v6, p2, Landroid/graphics/RectF;->left:F

    sub-float/2addr v5, v6

    div-float/2addr v5, v8

    iget v6, p2, Landroid/graphics/RectF;->bottom:F

    iget v7, p2, Landroid/graphics/RectF;->top:F

    sub-float/2addr v6, v7

    div-float/2addr v6, v8

    invoke-direct/range {v0 .. v6}, Landroid/view/animation/ScaleAnimation;-><init>(FFFFFF)V

    new-instance v1, Landroid/view/animation/TranslateAnimation;

    iget v2, p1, Landroid/graphics/Rect;->left:I

    int-to-float v3, v2

    iget v2, p1, Landroid/graphics/Rect;->right:I

    int-to-float v5, v2

    iget v2, p1, Landroid/graphics/Rect;->top:I

    int-to-float v7, v2

    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    int-to-float v9, v2

    move v2, v11

    move v4, v11

    move v6, v11

    move v8, v11

    invoke-direct/range {v1 .. v9}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    :goto_0
    invoke-virtual {v10, v0}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    invoke-virtual {v10, v1}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    const-wide/16 v0, 0x12c

    invoke-virtual {v10, v0, v1}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    return-object v10

    :cond_0
    new-instance v9, Landroid/view/animation/ScaleAnimation;

    iget v0, p2, Landroid/graphics/RectF;->right:F

    iget v1, p2, Landroid/graphics/RectF;->left:F

    iget v2, p2, Landroid/graphics/RectF;->bottom:F

    iget v3, p2, Landroid/graphics/RectF;->top:F

    invoke-direct {v9, v0, v1, v2, v3}, Landroid/view/animation/ScaleAnimation;-><init>(FFFF)V

    new-instance v0, Landroid/view/animation/TranslateAnimation;

    iget v1, p1, Landroid/graphics/Rect;->right:I

    int-to-float v2, v1

    iget v1, p1, Landroid/graphics/Rect;->left:I

    int-to-float v4, v1

    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    int-to-float v6, v1

    iget v1, p1, Landroid/graphics/Rect;->top:I

    int-to-float v8, v1

    move v1, v11

    move v3, v11

    move v5, v11

    move v7, v11

    invoke-direct/range {v0 .. v8}, Landroid/view/animation/TranslateAnimation;-><init>(IFIFIFIF)V

    move-object v1, v0

    move-object v0, v9

    goto :goto_0
.end method

.method private a(Landroid/content/Intent;)V
    .locals 8

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/FilterActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v1

    iget-boolean v2, p0, Lcom/twitter/android/FilterActivity;->L:Z

    if-eqz v2, :cond_1

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/FilterActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v3, v6, [Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, ":"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/android/FilterActivity;->Q:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ":image_attachment:crop:success"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    iget v3, p0, Lcom/twitter/android/FilterActivity;->K:I

    if-nez v3, :cond_5

    const-string/jumbo v3, "free_aspect"

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->d(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    :cond_0
    :goto_0
    invoke-virtual {v1, v2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_1
    iget v1, p0, Lcom/twitter/android/FilterActivity;->n:I

    if-eqz v1, :cond_2

    sget-object v1, Lcom/twitter/library/util/n;->a:[I

    iget v2, p0, Lcom/twitter/android/FilterActivity;->n:I

    aget v1, v1, v2

    iget-object v2, p0, Lcom/twitter/android/FilterActivity;->b:Lcom/twitter/android/FilterManager;

    invoke-virtual {v2, v1}, Lcom/twitter/android/FilterManager;->b(I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    const-string/jumbo v2, "filter_effect"

    invoke-virtual {p1, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_2
    iget-boolean v1, p0, Lcom/twitter/android/FilterActivity;->o:Z

    if-eqz v1, :cond_3

    const-string/jumbo v1, "filter_enhanced"

    const-string/jumbo v2, "twitter:enhanced"

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_3
    const-string/jumbo v1, "filter_filesize"

    iget-object v2, p0, Lcom/twitter/android/FilterActivity;->f:Landroid/net/Uri;

    invoke-static {p0, v2}, Lcom/twitter/library/util/Util;->d(Landroid/content/Context;Landroid/net/Uri;)F

    move-result v2

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;F)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/FilterActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v7, :cond_8

    const-string/jumbo v1, "filter_orientation"

    const-string/jumbo v2, "2"

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :goto_1
    iget-object v2, p0, Lcom/twitter/android/FilterActivity;->J:[Z

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_9

    aget-boolean v4, v2, v1

    if-eqz v4, :cond_4

    add-int/lit8 v0, v0, 0x1

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_5
    iget v3, p0, Lcom/twitter/android/FilterActivity;->K:I

    if-ne v3, v6, :cond_6

    const-string/jumbo v3, "original_aspect"

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->d(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    goto :goto_0

    :cond_6
    iget v3, p0, Lcom/twitter/android/FilterActivity;->K:I

    if-ne v3, v7, :cond_7

    const-string/jumbo v3, "wide_aspect"

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->d(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    goto :goto_0

    :cond_7
    iget v3, p0, Lcom/twitter/android/FilterActivity;->K:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_0

    const-string/jumbo v3, "square_aspect"

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->d(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    goto :goto_0

    :cond_8
    const-string/jumbo v1, "filter_orientation"

    const-string/jumbo v2, "1"

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    :cond_9
    const-string/jumbo v1, "filter_numseen"

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/FilterActivity;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/FilterActivity;->b(I)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->l:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/ViewPager;->announceForAccessibility(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method private a(ZI)V
    .locals 4

    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->f:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/FilterActivity;->g:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->f:Landroid/net/Uri;

    invoke-static {v0}, Lcom/twitter/library/util/n;->b(Landroid/net/Uri;)Z

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/FilterActivity;->setResult(I)V

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/FilterActivity;->finish()V

    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/FilterActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    if-nez p2, :cond_3

    const-string/jumbo v0, "filter_id"

    sget-object v2, Lcom/twitter/library/util/n;->a:[I

    iget v3, p0, Lcom/twitter/android/FilterActivity;->n:I

    aget v2, v2, v3

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "enhanced"

    iget-boolean v3, p0, Lcom/twitter/android/FilterActivity;->o:Z

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "uri"

    iget-object v3, p0, Lcom/twitter/android/FilterActivity;->f:Landroid/net/Uri;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-direct {p0, v1}, Lcom/twitter/android/FilterActivity;->a(Landroid/content/Intent;)V

    :cond_2
    :goto_1
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/FilterActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_0

    :cond_3
    packed-switch p2, :pswitch_data_0

    const v0, 0x7f0f01db    # com.twitter.android.R.string.image_filter_failed

    :goto_2
    invoke-static {p0, v0}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;I)V

    const-string/jumbo v0, "uri"

    iget-object v2, p0, Lcom/twitter/android/FilterActivity;->g:Landroid/net/Uri;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->f:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/FilterActivity;->g:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->f:Landroid/net/Uri;

    invoke-static {v0}, Lcom/twitter/library/util/n;->b(Landroid/net/Uri;)Z

    goto :goto_1

    :pswitch_0
    const v0, 0x7f0f01de    # com.twitter.android.R.string.image_not_supported

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method private a(ZZ)V
    .locals 8

    const v2, 0x7f020182    # com.twitter.android.R.drawable.ic_filters_all_on

    const/16 v5, 0x8

    const/4 v7, 0x0

    iget-boolean v0, p0, Lcom/twitter/android/FilterActivity;->p:Z

    if-eq p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/FilterActivity;->F:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v0, 0x7f0f0079    # com.twitter.android.R.string.button_show_filters

    invoke-virtual {p0, v0}, Lcom/twitter/android/FilterActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-eqz p1, :cond_3

    iget-object v1, p0, Lcom/twitter/android/FilterActivity;->r:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v1, p0, Lcom/twitter/android/FilterActivity;->C:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/twitter/android/FilterActivity;->t:Landroid/widget/GridView;

    iget-object v3, p0, Lcom/twitter/android/FilterActivity;->C:Landroid/view/ViewGroup;

    iget-object v4, p0, Lcom/twitter/android/FilterActivity;->m:Landroid/view/ViewGroup;

    invoke-virtual {v3, v4}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    iget-object v1, p0, Lcom/twitter/android/FilterActivity;->t:Landroid/widget/GridView;

    invoke-virtual {v1, v7}, Landroid/widget/GridView;->setVisibility(I)V

    iget-object v1, p0, Lcom/twitter/android/FilterActivity;->z:Lcom/twitter/android/widget/PipView;

    invoke-virtual {v1, v5}, Lcom/twitter/android/widget/PipView;->setVisibility(I)V

    const v1, 0x7f0f01bc    # com.twitter.android.R.string.grid_title

    invoke-virtual {p0, v1}, Lcom/twitter/android/FilterActivity;->setTitle(I)V

    iget-object v1, p0, Lcom/twitter/android/FilterActivity;->M:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    if-nez p2, :cond_2

    iget-object v1, p0, Lcom/twitter/android/FilterActivity;->C:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/twitter/android/FilterActivity;->m:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/FilterActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/FilterActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, ":"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/twitter/android/FilterActivity;->Q:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ":image_attachment:grid:impression"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :goto_1
    iput-boolean p1, p0, Lcom/twitter/android/FilterActivity;->p:Z

    iget-object v1, p0, Lcom/twitter/android/FilterActivity;->r:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    if-eqz p2, :cond_0

    invoke-direct {p0, p1}, Lcom/twitter/android/FilterActivity;->b(Z)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/twitter/android/FilterActivity;->r:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v1, p0, Lcom/twitter/android/FilterActivity;->C:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/twitter/android/FilterActivity;->m:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2, v7}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    iget-object v1, p0, Lcom/twitter/android/FilterActivity;->m:Landroid/view/ViewGroup;

    invoke-virtual {v1, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    const v1, 0x7f0f0134    # com.twitter.android.R.string.edit_photo_title

    invoke-virtual {p0, v1}, Lcom/twitter/android/FilterActivity;->setTitle(I)V

    iget-object v1, p0, Lcom/twitter/android/FilterActivity;->M:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/twitter/android/FilterActivity;->M:Landroid/widget/TextView;

    sget-object v2, Lcom/twitter/android/FilterActivity;->e:[I

    iget v3, p0, Lcom/twitter/android/FilterActivity;->n:I

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    if-nez p2, :cond_4

    iget-object v1, p0, Lcom/twitter/android/FilterActivity;->z:Lcom/twitter/android/widget/PipView;

    invoke-virtual {v1, v7}, Lcom/twitter/android/widget/PipView;->setVisibility(I)V

    iget-object v1, p0, Lcom/twitter/android/FilterActivity;->C:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/twitter/android/FilterActivity;->t:Landroid/widget/GridView;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const v2, 0x7f0f03c4    # com.twitter.android.R.string.selected_status

    invoke-virtual {p0, v2}, Lcom/twitter/android/FilterActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ". "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private a(ZZLandroid/net/Uri;Z)V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez p1, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/FilterActivity;->I:Z

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->A:Lcom/twitter/android/gc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->A:Lcom/twitter/android/gc;

    invoke-virtual {v0, v1}, Lcom/twitter/android/gc;->cancel(Z)Z

    :cond_0
    if-eqz p4, :cond_3

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->b:Lcom/twitter/android/FilterManager;

    invoke-virtual {v0, v1}, Lcom/twitter/android/FilterManager;->a(Z)V

    invoke-virtual {p0, v2}, Lcom/twitter/android/FilterActivity;->showDialog(I)V

    :cond_1
    :goto_1
    new-instance v0, Lcom/twitter/android/gc;

    invoke-direct {v0, p0, p3, p1}, Lcom/twitter/android/gc;-><init>(Lcom/twitter/android/FilterActivity;Landroid/net/Uri;Z)V

    iput-object v0, p0, Lcom/twitter/android/FilterActivity;->A:Lcom/twitter/android/gc;

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->A:Lcom/twitter/android/gc;

    new-array v1, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/gc;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->B:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method static synthetic a(Lcom/twitter/android/FilterActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/FilterActivity;->p:Z

    return v0
.end method

.method static synthetic a(Lcom/twitter/android/FilterActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/FilterActivity;->F:Z

    return p1
.end method

.method static synthetic b(Lcom/twitter/android/FilterActivity;)Landroid/view/ViewGroup;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->m:Landroid/view/ViewGroup;

    return-object v0
.end method

.method private b(I)V
    .locals 4

    iget-object v1, p0, Lcom/twitter/android/FilterActivity;->i:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-boolean v0, p0, Lcom/twitter/android/FilterActivity;->p:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->z:Lcom/twitter/android/widget/PipView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PipView;->setVisibility(I)V

    :cond_1
    if-nez p1, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/FilterActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBar;->b()V

    :goto_1
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/FilterActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBar;->c()V

    goto :goto_1
.end method

.method private b(Z)V
    .locals 23

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/FilterActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/twitter/android/FilterActivity;->t:Landroid/widget/GridView;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/twitter/android/FilterActivity;->n:I

    const v5, 0x7f0c0065    # com.twitter.android.R.dimen.filter_grid_view_divider

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    float-to-int v13, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/FilterActivity;->b:Lcom/twitter/android/FilterManager;

    invoke-virtual {v5}, Lcom/twitter/android/FilterManager;->d()Lcom/twitter/android/gk;

    move-result-object v5

    iget v7, v5, Lcom/twitter/android/gk;->b:I

    iget v14, v5, Lcom/twitter/android/gk;->a:I

    iget v15, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v0, v4, Landroid/util/DisplayMetrics;->heightPixels:I

    move/from16 v16, v0

    int-to-float v4, v14

    int-to-float v5, v7

    div-float v17, v4, v5

    int-to-float v4, v15

    move/from16 v0, v16

    int-to-float v5, v0

    div-float v18, v4, v5

    div-int/lit8 v4, v15, 0x3

    int-to-float v0, v4

    move/from16 v19, v0

    const v4, 0x7f0c0064    # com.twitter.android.R.dimen.filter_grid_name_height

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    float-to-int v4, v4

    int-to-float v8, v4

    const v4, 0x7f0c0062    # com.twitter.android.R.dimen.filter_controls_height

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    int-to-float v9, v3

    const/high16 v3, 0x3f800000    # 1.0f

    cmpl-float v3, v17, v3

    if-lez v3, :cond_0

    const/4 v3, 0x1

    move v11, v3

    :goto_0
    move/from16 v0, v16

    if-lt v0, v15, :cond_1

    const/4 v3, 0x1

    :goto_1
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/twitter/android/FilterActivity;->F:Z

    invoke-virtual {v12}, Landroid/widget/GridView;->getFirstVisiblePosition()I

    move-result v4

    sub-int v4, v6, v4

    invoke-virtual {v12, v4}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v10

    if-eqz v10, :cond_3

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/android/FilterActivity;->N:I

    int-to-float v4, v4

    :goto_2
    invoke-virtual {v10}, Landroid/view/View;->getLeft()I

    move-result v5

    sub-int/2addr v5, v13

    int-to-float v5, v5

    invoke-virtual {v10}, Landroid/view/View;->getTop()I

    move-result v6

    sub-int/2addr v6, v13

    int-to-float v6, v6

    add-float/2addr v6, v4

    move v8, v4

    move v9, v5

    move v10, v6

    :goto_3
    cmpl-float v4, v17, v18

    if-lez v4, :cond_6

    int-to-float v4, v15

    int-to-float v5, v14

    div-float/2addr v4, v5

    int-to-float v5, v7

    mul-float/2addr v5, v4

    int-to-float v6, v14

    mul-float/2addr v4, v6

    :goto_4
    float-to-int v6, v4

    sub-int v6, v15, v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v14, v6

    float-to-int v6, v5

    sub-int v6, v16, v6

    div-int/lit8 v6, v6, 0x2

    int-to-float v0, v6

    move/from16 v17, v0

    if-eqz v11, :cond_7

    sub-float v6, v4, v5

    const/high16 v7, 0x40000000    # 2.0f

    div-float v7, v6, v7

    const/4 v6, 0x0

    :goto_5
    if-eqz v11, :cond_8

    int-to-float v4, v13

    sub-float v4, v19, v4

    div-float v4, v5, v4

    :goto_6
    add-float v5, v14, v7

    div-float/2addr v5, v4

    add-float v18, v17, v6

    div-float v18, v18, v4

    int-to-float v0, v13

    move/from16 v20, v0

    sub-float v5, v5, v20

    int-to-float v13, v13

    sub-float v13, v18, v13

    new-instance v18, Landroid/graphics/Rect;

    const/16 v20, 0x0

    const/16 v21, 0x0

    sub-float v5, v9, v5

    float-to-int v5, v5

    sub-float v13, v10, v13

    float-to-int v13, v13

    move-object/from16 v0, v18

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-direct {v0, v1, v2, v5, v13}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v13, Landroid/graphics/RectF;

    const/high16 v5, 0x3f800000    # 1.0f

    const/high16 v20, 0x3f800000    # 1.0f

    const/high16 v21, 0x3f800000    # 1.0f

    div-float v21, v21, v4

    const/high16 v22, 0x3f800000    # 1.0f

    div-float v4, v22, v4

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v13, v5, v0, v1, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, p1

    invoke-direct {v0, v1, v13, v2}, Lcom/twitter/android/FilterActivity;->a(Landroid/graphics/Rect;Landroid/graphics/RectF;Z)Landroid/view/animation/AnimationSet;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/FilterActivity;->m:Landroid/view/ViewGroup;

    invoke-virtual {v5, v4}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    move/from16 v0, v16

    invoke-direct {v4, v15, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/high16 v5, 0x3f800000    # 1.0f

    iput v5, v13, Landroid/graphics/RectF;->bottom:F

    iput v5, v13, Landroid/graphics/RectF;->top:F

    iput v5, v13, Landroid/graphics/RectF;->right:F

    iput v5, v13, Landroid/graphics/RectF;->left:F

    if-eqz v11, :cond_9

    const/4 v5, 0x0

    move-object/from16 v0, v18

    iput v5, v0, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, v18

    iput v5, v0, Landroid/graphics/Rect;->top:I

    neg-int v5, v15

    move-object/from16 v0, v18

    iput v5, v0, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, v18

    iget v5, v0, Landroid/graphics/Rect;->left:I

    float-to-int v0, v7

    move/from16 v20, v0

    add-int v5, v5, v20

    float-to-int v0, v14

    move/from16 v20, v0

    add-int v5, v5, v20

    move-object/from16 v0, v18

    iput v5, v0, Landroid/graphics/Rect;->right:I

    :goto_7
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/FilterActivity;->m:Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/FilterActivity;->G:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/FilterActivity;->m:Landroid/view/ViewGroup;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/FilterActivity;->l:Landroid/support/v4/view/ViewPager;

    move-object/from16 v22, v0

    invoke-virtual/range {v21 .. v22}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v21

    add-int/lit8 v21, v21, 0x1

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v5, v0, v1, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, p1

    invoke-direct {v0, v1, v13, v2}, Lcom/twitter/android/FilterActivity;->a(Landroid/graphics/Rect;Landroid/graphics/RectF;Z)Landroid/view/animation/AnimationSet;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/FilterActivity;->G:Landroid/widget/ImageView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    if-eqz v11, :cond_a

    const/4 v5, 0x0

    move-object/from16 v0, v18

    iput v5, v0, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, v18

    iput v5, v0, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, v18

    iput v15, v0, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, v18

    iget v5, v0, Landroid/graphics/Rect;->left:I

    float-to-int v6, v7

    sub-int/2addr v5, v6

    float-to-int v6, v14

    sub-int/2addr v5, v6

    move-object/from16 v0, v18

    iput v5, v0, Landroid/graphics/Rect;->right:I

    :goto_8
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/FilterActivity;->m:Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/FilterActivity;->H:Landroid/widget/ImageView;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/android/FilterActivity;->m:Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/twitter/android/FilterActivity;->l:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v7, v11}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    invoke-virtual {v5, v6, v7, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, p1

    invoke-direct {v0, v1, v13, v2}, Lcom/twitter/android/FilterActivity;->a(Landroid/graphics/Rect;Landroid/graphics/RectF;Z)Landroid/view/animation/AnimationSet;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/FilterActivity;->v:Lcom/twitter/android/gb;

    invoke-virtual {v4, v5}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/FilterActivity;->H:Landroid/widget/ImageView;

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    if-eqz v3, :cond_b

    int-to-float v3, v15

    div-float v5, v3, v19

    sub-int v3, v15, v16

    div-int/lit8 v3, v3, 0x2

    int-to-float v4, v3

    const/4 v3, 0x0

    :goto_9
    mul-float v6, v9, v5

    add-float/2addr v3, v6

    float-to-int v3, v3

    neg-int v3, v3

    move-object/from16 v0, v18

    iput v3, v0, Landroid/graphics/Rect;->left:I

    sub-float v3, v10, v8

    mul-float/2addr v3, v5

    add-float/2addr v3, v4

    float-to-int v3, v3

    neg-int v3, v3

    move-object/from16 v0, v18

    iput v3, v0, Landroid/graphics/Rect;->top:I

    const/4 v3, 0x0

    move-object/from16 v0, v18

    iput v3, v0, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, v18

    iput v3, v0, Landroid/graphics/Rect;->right:I

    iput v5, v13, Landroid/graphics/RectF;->top:F

    iput v5, v13, Landroid/graphics/RectF;->left:F

    const/high16 v3, 0x3f800000    # 1.0f

    iput v3, v13, Landroid/graphics/RectF;->bottom:F

    iput v3, v13, Landroid/graphics/RectF;->right:F

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move/from16 v2, p1

    invoke-direct {v0, v1, v13, v2}, Lcom/twitter/android/FilterActivity;->a(Landroid/graphics/Rect;Landroid/graphics/RectF;Z)Landroid/view/animation/AnimationSet;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/twitter/android/FilterActivity;->E:Landroid/view/animation/AnimationSet;

    if-eqz p1, :cond_c

    new-instance v3, Landroid/view/animation/AlphaAnimation;

    const/4 v4, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-direct {v3, v4, v5}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    :goto_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/FilterActivity;->E:Landroid/view/animation/AnimationSet;

    invoke-virtual {v4, v3}, Landroid/view/animation/AnimationSet;->addAnimation(Landroid/view/animation/Animation;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/FilterActivity;->E:Landroid/view/animation/AnimationSet;

    const-wide/16 v4, 0x12c

    invoke-virtual {v3, v4, v5}, Landroid/view/animation/AnimationSet;->setDuration(J)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/FilterActivity;->E:Landroid/view/animation/AnimationSet;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/FilterActivity;->v:Lcom/twitter/android/gb;

    invoke-virtual {v3, v4}, Landroid/view/animation/AnimationSet;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/FilterActivity;->E:Landroid/view/animation/AnimationSet;

    invoke-virtual {v12, v3}, Landroid/widget/GridView;->startAnimation(Landroid/view/animation/Animation;)V

    return-void

    :cond_0
    const/4 v3, 0x0

    move v11, v3

    goto/16 :goto_0

    :cond_1
    const/4 v3, 0x0

    goto/16 :goto_1

    :cond_2
    const/4 v4, 0x0

    goto/16 :goto_2

    :cond_3
    invoke-virtual {v12, v6}, Landroid/widget/GridView;->setSelection(I)V

    rem-int/lit8 v4, v6, 0x3

    int-to-float v4, v4

    mul-float v5, v19, v4

    if-eqz v3, :cond_4

    move/from16 v0, v16

    int-to-float v4, v0

    add-float v9, v19, v8

    const/high16 v10, 0x40400000    # 3.0f

    mul-float/2addr v9, v10

    sub-float/2addr v4, v9

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v4, v9

    add-float v8, v8, v19

    div-int/lit8 v6, v6, 0x3

    int-to-float v6, v6

    mul-float/2addr v6, v8

    add-float/2addr v6, v4

    move v8, v4

    move v9, v5

    move v10, v6

    goto/16 :goto_3

    :cond_4
    const/4 v4, 0x0

    sget v10, Lcom/twitter/android/FilterActivity;->a:I

    add-int/lit8 v10, v10, -0x3

    if-lt v6, v10, :cond_5

    move/from16 v0, v16

    int-to-float v6, v0

    add-float v8, v8, v19

    sub-float/2addr v6, v8

    sub-float/2addr v6, v9

    move v8, v4

    move v9, v5

    move v10, v6

    goto/16 :goto_3

    :cond_5
    int-to-float v6, v13

    move v8, v4

    move v9, v5

    move v10, v6

    goto/16 :goto_3

    :cond_6
    move/from16 v0, v16

    int-to-float v4, v0

    int-to-float v5, v7

    div-float v5, v4, v5

    int-to-float v4, v14

    mul-float/2addr v4, v5

    int-to-float v6, v7

    mul-float/2addr v5, v6

    goto/16 :goto_4

    :cond_7
    const/4 v7, 0x0

    sub-float v6, v5, v4

    const/high16 v18, 0x40000000    # 2.0f

    div-float v6, v6, v18

    goto/16 :goto_5

    :cond_8
    int-to-float v5, v13

    sub-float v5, v19, v5

    div-float/2addr v4, v5

    goto/16 :goto_6

    :cond_9
    const/4 v5, 0x0

    move-object/from16 v0, v18

    iput v5, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, v18

    iput v5, v0, Landroid/graphics/Rect;->left:I

    move/from16 v0, v16

    neg-int v5, v0

    move-object/from16 v0, v18

    iput v5, v0, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, v18

    iget v5, v0, Landroid/graphics/Rect;->top:I

    move/from16 v0, v17

    float-to-int v0, v0

    move/from16 v20, v0

    add-int v5, v5, v20

    float-to-int v0, v6

    move/from16 v20, v0

    add-int v5, v5, v20

    move-object/from16 v0, v18

    iput v5, v0, Landroid/graphics/Rect;->bottom:I

    goto/16 :goto_7

    :cond_a
    const/4 v5, 0x0

    move-object/from16 v0, v18

    iput v5, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, v18

    iput v5, v0, Landroid/graphics/Rect;->left:I

    move/from16 v0, v16

    move-object/from16 v1, v18

    iput v0, v1, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, v18

    iget v5, v0, Landroid/graphics/Rect;->top:I

    move/from16 v0, v17

    float-to-int v7, v0

    sub-int/2addr v5, v7

    float-to-int v6, v6

    sub-int/2addr v5, v6

    move-object/from16 v0, v18

    iput v5, v0, Landroid/graphics/Rect;->bottom:I

    goto/16 :goto_8

    :cond_b
    move/from16 v0, v16

    int-to-float v3, v0

    div-float v5, v3, v19

    sub-int v3, v16, v15

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    const/4 v4, 0x0

    goto/16 :goto_9

    :cond_c
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    goto/16 :goto_a
.end method

.method static synthetic b()[I
    .locals 1

    sget-object v0, Lcom/twitter/android/FilterActivity;->e:[I

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/FilterActivity;)Landroid/view/ViewGroup;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->u:Landroid/view/ViewGroup;

    return-object v0
.end method

.method private c(Z)V
    .locals 2

    iget-boolean v0, p0, Lcom/twitter/android/FilterActivity;->j:Z

    if-eq v0, p1, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/FilterActivity;->p:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->c:Landroid/view/animation/Animation;

    iget-object v1, p0, Lcom/twitter/android/FilterActivity;->i:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    invoke-virtual {p0}, Lcom/twitter/android/FilterActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBar;->b()V

    :goto_1
    iput-boolean p1, p0, Lcom/twitter/android/FilterActivity;->j:Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->d:Landroid/view/animation/Animation;

    invoke-virtual {p0}, Lcom/twitter/android/FilterActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/android/widget/ToolBar;->c()V

    iget-object v1, p0, Lcom/twitter/android/FilterActivity;->i:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_1
.end method

.method static synthetic d(Lcom/twitter/android/FilterActivity;)Lcom/twitter/android/widget/PipView;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->z:Lcom/twitter/android/widget/PipView;

    return-object v0
.end method

.method private d(Z)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->q:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->r:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->s:Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Landroid/widget/ImageButton;->setEnabled(Z)V

    return-void
.end method

.method static synthetic e(Lcom/twitter/android/FilterActivity;)Landroid/widget/GridView;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->t:Landroid/widget/GridView;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/FilterActivity;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->G:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/android/FilterActivity;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->H:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/android/FilterActivity;)Landroid/view/animation/AnimationSet;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->E:Landroid/view/animation/AnimationSet;

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/android/FilterActivity;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->R:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic j(Lcom/twitter/android/FilterActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/FilterActivity;->o:Z

    return v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 2

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    const v1, 0x7f030073    # com.twitter.android.R.layout.filter_pager

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->a(Z)V

    return-object v0
.end method

.method a(I)V
    .locals 2

    iput p1, p0, Lcom/twitter/android/FilterActivity;->n:I

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/FilterActivity;->a(ZZ)V

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->z:Lcom/twitter/android/widget/PipView;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/PipView;->setPipOnPosition(I)V

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->l:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    return-void
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 9

    const-wide/16 v7, 0x96

    const/16 v6, 0x8

    const/high16 v5, -0x1000000

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/FilterActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v0, "uri"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/twitter/android/FilterActivity;->f:Landroid/net/Uri;

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->f:Landroid/net/Uri;

    iput-object v0, p0, Lcom/twitter/android/FilterActivity;->g:Landroid/net/Uri;

    const v0, 0x7f090179    # com.twitter.android.R.id.filter_gallery_control

    invoke-virtual {p0, v0}, Lcom/twitter/android/FilterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/FilterActivity;->i:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->i:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    const-string/jumbo v0, "title_icon_res_id"

    invoke-virtual {v1, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/FilterActivity;->O:I

    const-string/jumbo v0, "done_menu_item_res_id"

    const v2, 0x7f11000b    # com.twitter.android.R.menu.filter

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/FilterActivity;->P:I

    const-string/jumbo v0, "scribe_section"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/FilterActivity;->Q:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->i:Landroid/view/ViewGroup;

    const v1, 0x7f09016e    # com.twitter.android.R.id.filter_actionbar

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, p0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f09017a    # com.twitter.android.R.id.enhance

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/twitter/android/FilterActivity;->q:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/twitter/android/FilterActivity;->q:Landroid/widget/ImageButton;

    invoke-virtual {v1, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/twitter/android/FilterActivity;->q:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    const v1, 0x7f09017b    # com.twitter.android.R.id.grid

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/twitter/android/FilterActivity;->r:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/twitter/android/FilterActivity;->r:Landroid/widget/ImageButton;

    invoke-virtual {v1, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/twitter/android/FilterActivity;->r:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    const v1, 0x7f09017c    # com.twitter.android.R.id.crop

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/twitter/android/FilterActivity;->s:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->s:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->s:Landroid/widget/ImageButton;

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    const v0, 0x7f090178    # com.twitter.android.R.id.filter_grid

    invoke-virtual {p0, v0}, Lcom/twitter/android/FilterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/twitter/android/FilterActivity;->t:Landroid/widget/GridView;

    const v0, 0x7f09017d    # com.twitter.android.R.id.pip_layout

    invoke-virtual {p0, v0}, Lcom/twitter/android/FilterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/PipView;

    iput-object v0, p0, Lcom/twitter/android/FilterActivity;->z:Lcom/twitter/android/widget/PipView;

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->z:Lcom/twitter/android/widget/PipView;

    invoke-virtual {v0, v3}, Lcom/twitter/android/widget/PipView;->setPipOnPosition(I)V

    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/FilterActivity;->G:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->G:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/FilterActivity;->H:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->H:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    const v0, 0x7f09017e    # com.twitter.android.R.id.filter_label

    invoke-virtual {p0, v0}, Lcom/twitter/android/FilterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/FilterActivity;->M:Landroid/widget/TextView;

    const v0, 0x7f09017f    # com.twitter.android.R.id.spinner

    invoke-virtual {p0, v0}, Lcom/twitter/android/FilterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/FilterActivity;->B:Landroid/view/View;

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->B:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->t:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/FilterActivity;->C:Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lcom/twitter/android/FilterActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v4, :cond_1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/lit8 v0, v0, 0x3

    :goto_0
    iget-object v2, p0, Lcom/twitter/android/FilterActivity;->t:Landroid/widget/GridView;

    invoke-virtual {v2, v0}, Landroid/widget/GridView;->setColumnWidth(I)V

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->t:Landroid/widget/GridView;

    new-instance v2, Lcom/twitter/android/ga;

    invoke-direct {v2, p0}, Lcom/twitter/android/ga;-><init>(Lcom/twitter/android/FilterActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->t:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/FilterActivity;->u:Landroid/view/ViewGroup;

    const v0, 0x7f090177    # com.twitter.android.R.id.pager_parent

    invoke-virtual {p0, v0}, Lcom/twitter/android/FilterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/FilterActivity;->m:Landroid/view/ViewGroup;

    const v0, 0x7f0900bb    # com.twitter.android.R.id.pager

    invoke-virtual {p0, v0}, Lcom/twitter/android/FilterActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/twitter/android/FilterActivity;->l:Landroid/support/v4/view/ViewPager;

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->l:Landroid/support/v4/view/ViewPager;

    const v2, 0x7f0c0066    # com.twitter.android.R.dimen.filter_pager_view_divider

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    if-eqz p1, :cond_2

    const-string/jumbo v0, "controls"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/FilterActivity;->j:Z

    const-string/jumbo v0, "grid_on"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/FilterActivity;->p:Z

    const-string/jumbo v0, "enhance_on"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/FilterActivity;->o:Z

    const-string/jumbo v0, "current_effect"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/FilterActivity;->n:I

    const-string/jumbo v0, "image_uri"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/twitter/android/FilterActivity;->f:Landroid/net/Uri;

    const-string/jumbo v0, "scribe_section"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/FilterActivity;->Q:Ljava/lang/String;

    :goto_1
    invoke-virtual {p0}, Lcom/twitter/android/FilterActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/twitter/android/FilterManager;->a(Landroid/support/v4/app/FragmentManager;Landroid/content/Context;)Lcom/twitter/android/FilterManager;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/FilterActivity;->b:Lcom/twitter/android/FilterManager;

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->b:Lcom/twitter/android/FilterManager;

    invoke-virtual {v0, p0}, Lcom/twitter/android/FilterManager;->a(Lcom/twitter/android/gm;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->B:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    :goto_2
    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/FilterActivity;->k:I

    new-instance v0, Lcom/twitter/android/gb;

    invoke-direct {v0, p0}, Lcom/twitter/android/gb;-><init>(Lcom/twitter/android/FilterActivity;)V

    iput-object v0, p0, Lcom/twitter/android/FilterActivity;->v:Lcom/twitter/android/gb;

    const v0, 0x7f040007    # com.twitter.android.R.anim.fade_in

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/FilterActivity;->v:Lcom/twitter/android/gb;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-virtual {v0, v4}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    invoke-virtual {v0, v7, v8}, Landroid/view/animation/Animation;->setDuration(J)V

    iput-object v0, p0, Lcom/twitter/android/FilterActivity;->c:Landroid/view/animation/Animation;

    const v0, 0x7f040008    # com.twitter.android.R.anim.fade_out

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/FilterActivity;->v:Lcom/twitter/android/gb;

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-virtual {v0, v4}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    invoke-virtual {v0, v7, v8}, Landroid/view/animation/Animation;->setDuration(J)V

    iput-object v0, p0, Lcom/twitter/android/FilterActivity;->d:Landroid/view/animation/Animation;

    iget-boolean v0, p0, Lcom/twitter/android/FilterActivity;->p:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->u:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/twitter/android/FilterActivity;->m:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->t:Landroid/widget/GridView;

    invoke-virtual {v0, v3}, Landroid/widget/GridView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->r:Landroid/widget/ImageButton;

    const v1, 0x7f020182    # com.twitter.android.R.drawable.ic_filters_all_on

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    const v0, 0x7f0f01bc    # com.twitter.android.R.string.grid_title

    invoke-virtual {p0, v0}, Lcom/twitter/android/FilterActivity;->setTitle(I)V

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->M:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_3
    iget-boolean v0, p0, Lcom/twitter/android/FilterActivity;->o:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->q:Landroid/widget/ImageButton;

    const v1, 0x7f0201a1    # com.twitter.android.R.drawable.ic_filters_enhance_on

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    div-int/lit8 v0, v0, 0x3

    goto/16 :goto_0

    :cond_2
    iput v3, p0, Lcom/twitter/android/FilterActivity;->n:I

    iput-boolean v4, p0, Lcom/twitter/android/FilterActivity;->j:Z

    iput-boolean v3, p0, Lcom/twitter/android/FilterActivity;->o:Z

    iput-boolean v3, p0, Lcom/twitter/android/FilterActivity;->p:Z

    goto/16 :goto_1

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->b:Lcom/twitter/android/FilterManager;

    iget-boolean v1, p0, Lcom/twitter/android/FilterActivity;->o:Z

    iget-object v2, p0, Lcom/twitter/android/FilterActivity;->f:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/FilterManager;->a(ZLandroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->f:Landroid/net/Uri;

    invoke-direct {p0, v3, v4, v0, v3}, Lcom/twitter/android/FilterActivity;->a(ZZLandroid/net/Uri;Z)V

    goto/16 :goto_2

    :cond_4
    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->b:Lcom/twitter/android/FilterManager;

    invoke-virtual {v0}, Lcom/twitter/android/FilterManager;->d()Lcom/twitter/android/gk;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/FilterActivity;->b:Lcom/twitter/android/FilterManager;

    invoke-virtual {v1}, Lcom/twitter/android/FilterManager;->c()[Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/FilterActivity;->a(Lcom/twitter/android/gk;[Landroid/graphics/Bitmap;)V

    goto/16 :goto_2

    :cond_5
    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->m:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->u:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/twitter/android/FilterActivity;->t:Landroid/widget/GridView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    const v0, 0x7f0f0134    # com.twitter.android.R.string.edit_photo_title

    invoke-virtual {p0, v0}, Lcom/twitter/android/FilterActivity;->setTitle(I)V

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->M:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->M:Landroid/widget/TextView;

    sget-object v1, Lcom/twitter/android/FilterActivity;->e:[I

    iget v2, p0, Lcom/twitter/android/FilterActivity;->n:I

    aget v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_3
.end method

.method protected a(Lcom/twitter/android/gk;[Landroid/graphics/Bitmap;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/android/FilterActivity;->a(Lcom/twitter/android/gk;[Landroid/graphics/Bitmap;I)V

    return-void
.end method

.method protected a(Lcom/twitter/android/gk;[Landroid/graphics/Bitmap;I)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/FilterActivity;->A:Lcom/twitter/android/gc;

    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->b:Lcom/twitter/android/FilterManager;

    invoke-virtual {v0}, Lcom/twitter/android/FilterManager;->b()V

    invoke-static {p0}, Lcom/twitter/library/util/Util;->g(Landroid/content/Context;)I

    move-result v0

    div-int/lit8 v2, v0, 0x10

    iget v0, p1, Lcom/twitter/android/gk;->a:I

    iget v1, p1, Lcom/twitter/android/gk;->b:I

    iget-object v3, p1, Lcom/twitter/android/gk;->c:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v3}, Lcom/twitter/library/util/n;->a(IILandroid/graphics/Bitmap$Config;)I

    move-result v1

    if-eqz p2, :cond_6

    array-length v0, p2

    if-eqz v0, :cond_6

    aget-object v0, p2, v4

    if-eqz v0, :cond_6

    aget-object v0, p2, v4

    invoke-static {v0}, Lcom/twitter/library/util/n;->a(Landroid/graphics/Bitmap;)I

    move-result v0

    array-length v3, p2

    mul-int/2addr v0, v3

    add-int/2addr v0, v1

    :goto_0
    sub-int v0, v2, v0

    div-int/2addr v0, v1

    invoke-static {v4, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    const/4 v1, 0x3

    div-int/lit8 v0, v0, 0x2

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/FilterActivity;->b:Lcom/twitter/android/FilterManager;

    mul-int/lit8 v2, v0, 0x2

    add-int/lit8 v2, v2, 0x1

    sget v3, Lcom/twitter/android/FilterActivity;->a:I

    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/FilterManager;->a(II)V

    iget-object v1, p0, Lcom/twitter/android/FilterActivity;->l:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    iget v0, p0, Lcom/twitter/android/FilterActivity;->n:I

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->z:Lcom/twitter/android/widget/PipView;

    iget v2, p0, Lcom/twitter/android/FilterActivity;->n:I

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/PipView;->setPipOnPosition(I)V

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->h:Lcom/twitter/android/ge;

    if-nez v0, :cond_2

    new-instance v0, Lcom/twitter/android/ge;

    invoke-direct {v0, p0}, Lcom/twitter/android/ge;-><init>(Lcom/twitter/android/FilterActivity;)V

    iput-object v0, p0, Lcom/twitter/android/FilterActivity;->h:Lcom/twitter/android/ge;

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->h:Lcom/twitter/android/ge;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    invoke-virtual {v1, p0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    :goto_1
    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->b:Lcom/twitter/android/FilterManager;

    invoke-virtual {v0}, Lcom/twitter/android/FilterManager;->a()V

    :cond_0
    :goto_2
    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->w:Lcom/twitter/android/gd;

    if-nez v0, :cond_5

    new-instance v0, Lcom/twitter/android/gd;

    invoke-virtual {p0}, Lcom/twitter/android/FilterActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/gd;-><init>(Lcom/twitter/android/FilterActivity;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/FilterActivity;->w:Lcom/twitter/android/gd;

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->t:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/twitter/android/FilterActivity;->w:Lcom/twitter/android/gd;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    :cond_1
    :goto_3
    invoke-direct {p0, v5}, Lcom/twitter/android/FilterActivity;->d(Z)V

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->B:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v4}, Lcom/twitter/android/FilterActivity;->removeDialog(I)V

    return-void

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->h:Lcom/twitter/android/ge;

    invoke-virtual {v0}, Lcom/twitter/android/ge;->notifyDataSetChanged()V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->h:Lcom/twitter/android/ge;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->h:Lcom/twitter/android/ge;

    invoke-virtual {v0}, Lcom/twitter/android/ge;->notifyDataSetChanged()V

    goto :goto_2

    :cond_4
    iget-boolean v0, p0, Lcom/twitter/android/FilterActivity;->I:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, v5, p3}, Lcom/twitter/android/FilterActivity;->a(ZI)V

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->w:Lcom/twitter/android/gd;

    invoke-virtual {v0}, Lcom/twitter/android/gd;->notifyDataSetChanged()V

    goto :goto_3

    :cond_6
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;)V
    .locals 0

    return-void
.end method

.method public a(Z)V
    .locals 2

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->b:Lcom/twitter/android/FilterManager;

    invoke-virtual {v0}, Lcom/twitter/android/FilterManager;->d()Lcom/twitter/android/gk;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/FilterActivity;->b:Lcom/twitter/android/FilterManager;

    invoke-virtual {v1}, Lcom/twitter/android/FilterManager;->c()[Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/FilterActivity;->a(Lcom/twitter/android/gk;[Landroid/graphics/Bitmap;)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v1, v1, v0}, Lcom/twitter/android/FilterActivity;->a(Lcom/twitter/android/gk;[Landroid/graphics/Bitmap;I)V

    goto :goto_0
.end method

.method protected a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z

    iget v0, p0, Lcom/twitter/android/FilterActivity;->O:I

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/FilterActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/twitter/android/FilterActivity;->O:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/internal/android/widget/ToolBar;->setIcon(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    iget v0, p0, Lcom/twitter/android/FilterActivity;->P:I

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    const/4 v0, 0x1

    return v0
.end method

.method public a(Lhn;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Lhn;->a()I

    move-result v0

    const v3, 0x7f0901e2    # com.twitter.android.R.id.done

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-direct {p0, v0, v2}, Lcom/twitter/android/FilterActivity;->a(ZI)V

    return v1

    :cond_0
    move v0, v2

    goto :goto_0
.end method

.method public finish()V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->A:Lcom/twitter/android/gc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->A:Lcom/twitter/android/gc;

    invoke-virtual {v0, v1}, Lcom/twitter/android/gc;->cancel(Z)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/FilterActivity;->A:Lcom/twitter/android/gc;

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->b:Lcom/twitter/android/FilterManager;

    invoke-virtual {v0, v1}, Lcom/twitter/android/FilterManager;->a(Z)V

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->finish()V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/client/BaseFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    if-ne p1, v3, :cond_2

    const/4 v0, -0x1

    if-ne p2, v0, :cond_2

    iput-boolean v3, p0, Lcom/twitter/android/FilterActivity;->L:Z

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->f:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/FilterActivity;->g:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->f:Landroid/net/Uri;

    invoke-static {v0}, Lcom/twitter/library/util/n;->b(Landroid/net/Uri;)Z

    :cond_0
    const-string/jumbo v0, "uri"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/twitter/android/FilterActivity;->f:Landroid/net/Uri;

    const-string/jumbo v0, "crop_type"

    invoke-virtual {p3, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/FilterActivity;->K:I

    invoke-direct {p0, v2}, Lcom/twitter/android/FilterActivity;->d(Z)V

    iget-boolean v0, p0, Lcom/twitter/android/FilterActivity;->p:Z

    if-eqz v0, :cond_1

    invoke-direct {p0, v2, v2}, Lcom/twitter/android/FilterActivity;->a(ZZ)V

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->f:Landroid/net/Uri;

    invoke-direct {p0, v2, v2, v0, v3}, Lcom/twitter/android/FilterActivity;->a(ZZLandroid/net/Uri;Z)V

    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->b:Lcom/twitter/android/FilterManager;

    invoke-virtual {v0}, Lcom/twitter/android/FilterManager;->a()V

    invoke-direct {p0, v3}, Lcom/twitter/android/FilterActivity;->d(Z)V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, v0}, Lcom/twitter/android/FilterActivity;->a(ZI)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v3, 0x7f09017a    # com.twitter.android.R.id.enhance

    if-ne v0, v3, :cond_2

    invoke-direct {p0, v2}, Lcom/twitter/android/FilterActivity;->d(Z)V

    const v0, 0x7f0f006c    # com.twitter.android.R.string.button_enhance

    invoke-virtual {p0, v0}, Lcom/twitter/android/FilterActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-boolean v3, p0, Lcom/twitter/android/FilterActivity;->o:Z

    if-eqz v3, :cond_1

    iput-boolean v2, p0, Lcom/twitter/android/FilterActivity;->o:Z

    iget-object v3, p0, Lcom/twitter/android/FilterActivity;->q:Landroid/widget/ImageButton;

    const v4, 0x7f0201a0    # com.twitter.android.R.drawable.ic_filters_enhance_off

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setImageResource(I)V

    :goto_0
    iget-object v3, p0, Lcom/twitter/android/FilterActivity;->q:Landroid/widget/ImageButton;

    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->f:Landroid/net/Uri;

    invoke-direct {p0, v1, v1, v0, v2}, Lcom/twitter/android/FilterActivity;->a(ZZLandroid/net/Uri;Z)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    iput-boolean v1, p0, Lcom/twitter/android/FilterActivity;->o:Z

    iget-object v3, p0, Lcom/twitter/android/FilterActivity;->q:Landroid/widget/ImageButton;

    const v4, 0x7f0201a1    # com.twitter.android.R.drawable.ic_filters_enhance_on

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setImageResource(I)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const v4, 0x7f0f03c4    # com.twitter.android.R.string.selected_status

    invoke-virtual {p0, v4}, Lcom/twitter/android/FilterActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ". "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const v3, 0x7f09017c    # com.twitter.android.R.id.crop

    if-ne v0, v3, :cond_4

    invoke-direct {p0, v2}, Lcom/twitter/android/FilterActivity;->d(Z)V

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->b:Lcom/twitter/android/FilterManager;

    invoke-virtual {v0}, Lcom/twitter/android/FilterManager;->b()V

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->A:Lcom/twitter/android/gc;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->A:Lcom/twitter/android/gc;

    invoke-virtual {v0, v1}, Lcom/twitter/android/gc;->cancel(Z)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/FilterActivity;->A:Lcom/twitter/android/gc;

    :cond_3
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/FilterCropActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "uri"

    iget-object v3, p0, Lcom/twitter/android/FilterActivity;->g:Landroid/net/Uri;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "filter_id"

    sget-object v3, Lcom/twitter/library/util/n;->a:[I

    iget v4, p0, Lcom/twitter/android/FilterActivity;->n:I

    aget v3, v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "enhance"

    iget-boolean v3, p0, Lcom/twitter/android/FilterActivity;->o:Z

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const/high16 v2, 0x10000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/FilterActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1

    :cond_4
    const v3, 0x7f09017b    # com.twitter.android.R.id.grid

    if-ne v0, v3, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/FilterActivity;->p:Z

    if-nez v0, :cond_5

    move v0, v1

    :goto_2
    invoke-direct {p0, v0, v1}, Lcom/twitter/android/FilterActivity;->a(ZZ)V

    goto/16 :goto_1

    :cond_5
    move v0, v2

    goto :goto_2
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    const v1, 0x7f0f00e6    # com.twitter.android.R.string.cropping_image

    invoke-virtual {p0, v1}, Lcom/twitter/android/FilterActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->A:Lcom/twitter/android/gc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->A:Lcom/twitter/android/gc;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/gc;->cancel(Z)Z

    :cond_0
    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onDestroy()V

    return-void
.end method

.method public onPageScrollStateChanged(I)V
    .locals 0

    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0

    return-void
.end method

.method public onPageSelected(I)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->J:[Z

    const/4 v1, 0x1

    aput-boolean v1, v0, p1

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->M:Landroid/widget/TextView;

    sget-object v1, Lcom/twitter/android/FilterActivity;->e:[I

    aget v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    iput p1, p0, Lcom/twitter/android/FilterActivity;->n:I

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->z:Lcom/twitter/android/widget/PipView;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/PipView;->setPipOnPosition(I)V

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->b:Lcom/twitter/android/FilterManager;

    sget-object v1, Lcom/twitter/library/util/n;->a:[I

    aget v1, v1, p1

    invoke-virtual {v0, v1}, Lcom/twitter/android/FilterManager;->a(I)V

    sget-object v0, Lcom/twitter/android/FilterActivity;->e:[I

    aget v0, v0, p1

    invoke-virtual {p0, v0}, Lcom/twitter/android/FilterActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/FilterActivity;->a(Ljava/lang/String;)V

    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->onPostCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/FilterActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBar;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/FilterActivity;->N:I

    iget-boolean v0, p0, Lcom/twitter/android/FilterActivity;->j:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/android/FilterActivity;->b(I)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/twitter/android/FilterActivity;->b(I)V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "controls"

    iget-boolean v1, p0, Lcom/twitter/android/FilterActivity;->j:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "grid_on"

    iget-boolean v1, p0, Lcom/twitter/android/FilterActivity;->p:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "enhance_on"

    iget-boolean v1, p0, Lcom/twitter/android/FilterActivity;->o:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "current_effect"

    iget v1, p0, Lcom/twitter/android/FilterActivity;->n:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "image_uri"

    iget-object v1, p0, Lcom/twitter/android/FilterActivity;->f:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v0, "scribe_section"

    iget-object v1, p0, Lcom/twitter/android/FilterActivity;->Q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    and-int/lit16 v2, v2, 0xff

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/twitter/android/FilterActivity;->p:Z

    if-eqz v0, :cond_3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iget-object v2, p0, Lcom/twitter/android/FilterActivity;->t:Landroid/widget/GridView;

    invoke-virtual {v2}, Landroid/widget/GridView;->getTop()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/view/MotionEvent;->setLocation(FF)V

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->t:Landroid/widget/GridView;

    invoke-virtual {v0, p2}, Landroid/widget/GridView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_1
    return v0

    :pswitch_0
    iput v3, p0, Lcom/twitter/android/FilterActivity;->x:I

    iput-boolean v0, p0, Lcom/twitter/android/FilterActivity;->y:Z

    goto :goto_0

    :pswitch_1
    iget-boolean v2, p0, Lcom/twitter/android/FilterActivity;->y:Z

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/twitter/android/FilterActivity;->j:Z

    if-nez v2, :cond_2

    :goto_2
    invoke-direct {p0, v0}, Lcom/twitter/android/FilterActivity;->c(Z)V

    :cond_1
    iput v1, p0, Lcom/twitter/android/FilterActivity;->x:I

    iput-boolean v1, p0, Lcom/twitter/android/FilterActivity;->y:Z

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_2

    :pswitch_2
    iget v0, p0, Lcom/twitter/android/FilterActivity;->x:I

    sub-int v0, v3, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v2, p0, Lcom/twitter/android/FilterActivity;->k:I

    if-lt v0, v2, :cond_0

    iput-boolean v1, p0, Lcom/twitter/android/FilterActivity;->y:Z

    goto :goto_0

    :cond_3
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iget-object v2, p0, Lcom/twitter/android/FilterActivity;->l:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getTop()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/view/MotionEvent;->setLocation(FF)V

    iget-object v0, p0, Lcom/twitter/android/FilterActivity;->l:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p2}, Landroid/support/v4/view/ViewPager;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
