.class public Lcom/twitter/android/MessagesDestroyThreadDialogFragment;
.super Lcom/twitter/android/widget/PromptDialogFragment;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/widget/PromptDialogFragment;-><init>()V

    return-void
.end method

.method public static a(I)Lcom/twitter/android/MessagesDestroyThreadDialogFragment;
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/twitter/android/MessagesDestroyThreadDialogFragment;->a(ILjava/lang/String;)Lcom/twitter/android/MessagesDestroyThreadDialogFragment;

    move-result-object v0

    return-object v0
.end method

.method public static a(ILjava/lang/String;)Lcom/twitter/android/MessagesDestroyThreadDialogFragment;
    .locals 3

    new-instance v0, Lcom/twitter/android/MessagesDestroyThreadDialogFragment;

    invoke-direct {v0}, Lcom/twitter/android/MessagesDestroyThreadDialogFragment;-><init>()V

    invoke-virtual {v0, p0}, Lcom/twitter/android/MessagesDestroyThreadDialogFragment;->c(I)V

    invoke-direct {v0, p1}, Lcom/twitter/android/MessagesDestroyThreadDialogFragment;->c(Ljava/lang/String;)Lcom/twitter/android/MessagesDestroyThreadDialogFragment;

    move-result-object v1

    const v2, 0x7f0f0285    # com.twitter.android.R.string.messages_delete_conversation

    invoke-virtual {v1, v2}, Lcom/twitter/android/MessagesDestroyThreadDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    const v2, 0x7f0f0286    # com.twitter.android.R.string.messages_delete_conversation_question

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->e(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    const v2, 0x7f0f0571    # com.twitter.android.R.string.yes

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->g(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    const v2, 0x7f0f029d    # com.twitter.android.R.string.no

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->i(I)Lcom/twitter/android/widget/PromptDialogFragment;

    return-object v0
.end method

.method private c(Ljava/lang/String;)Lcom/twitter/android/MessagesDestroyThreadDialogFragment;
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/MessagesDestroyThreadDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "thread_id"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-object p0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/MessagesDestroyThreadDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "thread_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
