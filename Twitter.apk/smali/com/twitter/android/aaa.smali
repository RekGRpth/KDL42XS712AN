.class public Lcom/twitter/android/aaa;
.super Landroid/support/v4/widget/CursorAdapter;
.source "Twttr"


# instance fields
.field private final a:I

.field private final b:I

.field protected final c:Lcom/twitter/android/client/c;

.field protected final d:Ljava/util/ArrayList;

.field protected e:Z

.field final f:Lcom/twitter/library/widget/a;

.field final g:Lcom/twitter/library/util/FriendshipCache;

.field final h:Z

.field i:Landroid/widget/Button;

.field j:J

.field private final k:I

.field private final l:Z

.field private final m:Z

.field private final n:Ljava/util/HashMap;

.field private final o:Landroid/view/animation/Animation;

.field private final p:Lcom/twitter/android/aab;

.field private final q:Lcom/twitter/library/client/aa;

.field private r:Landroid/widget/TextView;

.field private s:Landroid/view/View$OnClickListener;

.field private t:Lcom/twitter/android/ob;

.field private u:Lcom/twitter/library/view/c;

.field private v:Z

.field private w:Z

.field private x:Z

.field private y:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;IILcom/twitter/library/widget/a;Lcom/twitter/library/util/FriendshipCache;IIZZ)V
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Landroid/support/v4/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/aaa;->d:Ljava/util/ArrayList;

    invoke-static {p1}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/aaa;->c:Lcom/twitter/android/client/c;

    iput p3, p0, Lcom/twitter/android/aaa;->a:I

    iput-object p4, p0, Lcom/twitter/android/aaa;->f:Lcom/twitter/library/widget/a;

    iput-object p5, p0, Lcom/twitter/android/aaa;->g:Lcom/twitter/library/util/FriendshipCache;

    iput p6, p0, Lcom/twitter/android/aaa;->b:I

    iput p7, p0, Lcom/twitter/android/aaa;->k:I

    if-gtz p6, :cond_0

    if-lez p7, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/aaa;->h:Z

    invoke-static {p1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/aaa;->q:Lcom/twitter/library/client/aa;

    iget-object v0, p0, Lcom/twitter/android/aaa;->q:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/twitter/android/aaa;->j:J

    iput-boolean v1, p0, Lcom/twitter/android/aaa;->v:Z

    iput-boolean p8, p0, Lcom/twitter/android/aaa;->l:Z

    iput-boolean p9, p0, Lcom/twitter/android/aaa;->m:Z

    iput-boolean v2, p0, Lcom/twitter/android/aaa;->e:Z

    iput-boolean v1, p0, Lcom/twitter/android/aaa;->w:Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/aaa;->n:Ljava/util/HashMap;

    const v0, 0x7f040007    # com.twitter.android.R.anim.fade_in

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/aaa;->o:Landroid/view/animation/Animation;

    new-instance v0, Lcom/twitter/android/aab;

    invoke-direct {v0, p0}, Lcom/twitter/android/aab;-><init>(Lcom/twitter/android/aaa;)V

    iput-object v0, p0, Lcom/twitter/android/aaa;->p:Lcom/twitter/android/aab;

    invoke-static {}, Lgl;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/aaa;->x:Z

    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method private a(Landroid/view/View;JJ)V
    .locals 5

    invoke-virtual {p1}, Landroid/view/View;->clearAnimation()V

    iget-object v0, p0, Lcom/twitter/android/aaa;->n:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v1, p4, v3

    if-nez v1, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v1, p2, v3

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/aaa;->n:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/twitter/android/aaa;->o:Landroid/view/animation/Animation;

    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/aaa;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/aaa;->c()V

    return-void
.end method

.method private a(J)Z
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/aaa;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/aaa;->q:Lcom/twitter/library/client/aa;

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/twitter/library/provider/az;->a(Landroid/content/Context;J)Lcom/twitter/library/provider/az;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/provider/az;->a(J)Lcom/twitter/library/api/TwitterUser;

    move-result-object v0

    iget-boolean v0, v0, Lcom/twitter/library/api/TwitterUser;->isLifelineInstitution:Z

    return v0
.end method

.method static synthetic a(Lcom/twitter/android/aaa;J)Z
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/aaa;->a(J)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/twitter/android/aaa;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/aaa;->w:Z

    return v0
.end method

.method private c()V
    .locals 4

    const/4 v1, 0x1

    iget-boolean v0, p0, Lcom/twitter/android/aaa;->h:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/aaa;->i:Landroid/widget/Button;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/aaa;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/aaa;->mCursor:Landroid/database/Cursor;

    const/4 v2, 0x2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iget-object v0, p0, Lcom/twitter/android/aaa;->g:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/util/FriendshipCache;->a(J)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/aaa;->g:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/util/FriendshipCache;->h(J)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/twitter/library/provider/ay;->b(I)Z

    move-result v0

    :goto_0
    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/twitter/android/aaa;->mCursor:Landroid/database/Cursor;

    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/twitter/android/aaa;->i:Landroid/widget/Button;

    if-nez v0, :cond_4

    :goto_2
    invoke-virtual {v2, v1}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_2
    return-void

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/aaa;->mCursor:Landroid/database/Cursor;

    const/4 v2, 0x7

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Lcom/twitter/library/provider/ay;->b(I)Z

    move-result v0

    goto :goto_0

    :cond_4
    const/4 v1, 0x0

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public a()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method protected a(Lcom/twitter/library/widget/UserView;)Lcom/twitter/library/widget/UserView;
    .locals 3

    iget-boolean v0, p0, Lcom/twitter/android/aaa;->m:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/aaa;->f:Lcom/twitter/library/widget/a;

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/UserView;->setActionButtonClickListener(Lcom/twitter/library/widget/a;)V

    iget-object v0, p0, Lcom/twitter/android/aaa;->f:Lcom/twitter/library/widget/a;

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/UserView;->setCheckBoxClickListener(Lcom/twitter/library/widget/a;)V

    :cond_0
    :goto_0
    new-instance v0, Lcom/twitter/android/zx;

    invoke-direct {v0, p1}, Lcom/twitter/android/zx;-><init>(Lcom/twitter/library/widget/BaseUserView;)V

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/UserView;->setTag(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/twitter/android/aaa;->d:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object p1

    :cond_1
    iget v0, p0, Lcom/twitter/android/aaa;->a:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/twitter/android/aaa;->a:I

    iget-object v1, p0, Lcom/twitter/android/aaa;->p:Lcom/twitter/android/aab;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/library/widget/UserView;->a(ILcom/twitter/library/widget/a;)V

    const v0, 0x7f020085    # com.twitter.android.R.drawable.btn_follow_action

    iget v1, p0, Lcom/twitter/android/aaa;->a:I

    if-ne v0, v1, :cond_3

    iget-object v0, p1, Lcom/twitter/library/widget/UserView;->m:Lcom/twitter/library/widget/ActionButton;

    const v1, 0x7f020086    # com.twitter.android.R.drawable.btn_follow_action_bg

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/ActionButton;->setBackgroundResource(I)V

    :cond_2
    :goto_1
    iget-boolean v0, p0, Lcom/twitter/android/aaa;->w:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/aaa;->x:Z

    if-eqz v0, :cond_4

    invoke-static {}, Lgl;->c()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/aaa;->f:Lcom/twitter/library/widget/a;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/library/widget/UserView;->b(ILcom/twitter/library/widget/a;)V

    iget-object v0, p1, Lcom/twitter/library/widget/UserView;->n:Lcom/twitter/library/widget/ActionButton;

    invoke-static {}, Lgl;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/ActionButton;->setBackgroundResource(I)V

    goto :goto_0

    :cond_3
    const v0, 0x7f02008e    # com.twitter.android.R.drawable.btn_media_tag_follow_action

    iget v1, p0, Lcom/twitter/android/aaa;->a:I

    if-ne v0, v1, :cond_2

    iget-object v0, p1, Lcom/twitter/library/widget/UserView;->m:Lcom/twitter/library/widget/ActionButton;

    const v1, 0x7f02008f    # com.twitter.android.R.drawable.btn_media_tag_follow_action_bg

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/ActionButton;->setBackgroundResource(I)V

    goto :goto_1

    :cond_4
    const v0, 0x7f02007e    # com.twitter.android.R.drawable.btn_favorite_action

    iget-object v1, p0, Lcom/twitter/android/aaa;->f:Lcom/twitter/library/widget/a;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/library/widget/UserView;->b(ILcom/twitter/library/widget/a;)V

    iget-object v0, p1, Lcom/twitter/library/widget/UserView;->n:Lcom/twitter/library/widget/ActionButton;

    const v1, 0x7f02007f    # com.twitter.android.R.drawable.btn_favorite_action_bg

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/ActionButton;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public a(JJ)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/aaa;->n:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/aaa;->s:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public a(Lcom/twitter/android/ob;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/aaa;->t:Lcom/twitter/android/ob;

    return-void
.end method

.method public a(Lcom/twitter/library/view/c;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/aaa;->u:Lcom/twitter/library/view/c;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Bio click listener already set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/twitter/android/aaa;->u:Lcom/twitter/library/view/c;

    return-void
.end method

.method protected a(Lcom/twitter/library/widget/BaseUserView;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/api/PromotedContent;JLjava/lang/String;IZZI)V
    .locals 4

    invoke-virtual {p1, p2, p3}, Lcom/twitter/library/widget/BaseUserView;->setUserId(J)V

    invoke-virtual {p1, p5, p6}, Lcom/twitter/library/widget/BaseUserView;->a(Ljava/lang/String;Ljava/lang/String;)V

    move/from16 v0, p14

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/BaseUserView;->setProtected(Z)V

    move/from16 v0, p15

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/BaseUserView;->setVerified(Z)V

    if-eqz p4, :cond_1

    iget-object v1, p0, Lcom/twitter/android/aaa;->c:Lcom/twitter/android/client/c;

    iget-object v1, v1, Lcom/twitter/android/client/c;->a:Lcom/twitter/library/widget/ap;

    invoke-virtual {p0}, Lcom/twitter/android/aaa;->a()I

    move-result v2

    invoke-interface {v1, v2, p4}, Lcom/twitter/library/widget/ap;->a(ILjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/twitter/library/widget/BaseUserView;->setUserImage(Landroid/graphics/Bitmap;)V

    :goto_0
    iget-boolean v1, p0, Lcom/twitter/android/aaa;->e:Z

    if-eqz v1, :cond_2

    invoke-virtual {p1, p7, p8}, Lcom/twitter/library/widget/BaseUserView;->a(Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;)V

    :goto_1
    iget-object v1, p0, Lcom/twitter/android/aaa;->c:Lcom/twitter/android/client/c;

    iget-boolean v1, v1, Lcom/twitter/android/client/c;->f:Z

    invoke-virtual {p1, p9, v1}, Lcom/twitter/library/widget/BaseUserView;->a(Lcom/twitter/library/api/PromotedContent;Z)V

    invoke-virtual {p1}, Lcom/twitter/library/widget/BaseUserView;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/zx;

    if-eqz p6, :cond_3

    :goto_2
    iput-object p6, v1, Lcom/twitter/android/zx;->h:Ljava/lang/String;

    iput-object p4, v1, Lcom/twitter/android/zx;->g:Ljava/lang/String;

    iput-wide p10, v1, Lcom/twitter/android/zx;->c:J

    move-object/from16 v0, p12

    iput-object v0, v1, Lcom/twitter/android/zx;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/aaa;->t:Lcom/twitter/android/ob;

    if-eqz v2, :cond_0

    new-instance v2, Landroid/os/Bundle;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Landroid/os/Bundle;-><init>(I)V

    const-string/jumbo v3, "position"

    move/from16 v0, p16

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v3, p0, Lcom/twitter/android/aaa;->t:Lcom/twitter/android/ob;

    invoke-interface {v3, p1, p9, v2}, Lcom/twitter/android/ob;->a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V

    :cond_0
    move/from16 v0, p13

    iput v0, v1, Lcom/twitter/android/zx;->e:I

    return-void

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/twitter/library/widget/BaseUserView;->setUserImage(Landroid/graphics/Bitmap;)V

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Lcom/twitter/library/widget/BaseUserView;->a(Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;)V

    goto :goto_1

    :cond_3
    move-object p6, p5

    goto :goto_2
.end method

.method protected a(Lcom/twitter/library/widget/BaseUserView;Landroid/database/Cursor;J)V
    .locals 18

    const-string/jumbo v1, "token"

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    const/16 v1, 0xf

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    :goto_0
    const/4 v1, 0x6

    move-object/from16 v0, p2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/4 v2, 0x5

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v2, 0x4

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v2, 0x3

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/16 v2, 0x8

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/16 v2, 0x9

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/twitter/library/api/TweetEntities;

    const/16 v2, 0xa

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/twitter/library/api/PromotedContent;

    const/4 v2, 0x0

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-long v11, v2

    const/4 v2, 0x7

    move-object/from16 v0, p2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    and-int/lit8 v2, v1, 0x1

    if-eqz v2, :cond_1

    const/4 v15, 0x1

    :goto_1
    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_2

    const/16 v16, 0x1

    :goto_2
    invoke-interface/range {p2 .. p2}, Landroid/database/Cursor;->getPosition()I

    move-result v17

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move-wide/from16 v3, p3

    invoke-virtual/range {v1 .. v17}, Lcom/twitter/android/aaa;->a(Lcom/twitter/library/widget/BaseUserView;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/api/PromotedContent;JLjava/lang/String;IZZI)V

    return-void

    :cond_0
    const/4 v13, 0x0

    goto :goto_0

    :cond_1
    const/4 v15, 0x0

    goto :goto_1

    :cond_2
    const/16 v16, 0x0

    goto :goto_2
.end method

.method public a(Ljava/util/HashMap;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/aaa;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_0
    if-ltz v2, :cond_2

    iget-object v0, p0, Lcom/twitter/android/aaa;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/zx;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/aaa;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_0
    :goto_1
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/twitter/android/zx;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/util/ae;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/twitter/library/util/ae;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/twitter/android/zx;->b:Lcom/twitter/library/widget/BaseUserView;

    iget-object v0, v1, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v3, v0}, Lcom/twitter/library/widget/BaseUserView;->setUserImage(Landroid/graphics/Bitmap;)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 10

    const/4 v4, 0x0

    const/4 v2, 0x0

    new-instance v5, Lcom/twitter/library/provider/ParcelableMatrixCursor;

    sget-object v0, Lcom/twitter/library/provider/cm;->a:[Ljava/lang/String;

    invoke-direct {v5, v0}, Lcom/twitter/library/provider/ParcelableMatrixCursor;-><init>([Ljava/lang/String;)V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v2

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {v5}, Lcom/twitter/library/provider/ParcelableMatrixCursor;->a()Lcom/twitter/library/provider/r;

    move-result-object v7

    add-int/lit8 v3, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v1}, Lcom/twitter/library/provider/r;->a(Ljava/lang/Object;)Lcom/twitter/library/provider/r;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v1}, Lcom/twitter/library/provider/r;->a(Ljava/lang/Object;)Lcom/twitter/library/provider/r;

    iget-wide v8, v0, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v7, v1}, Lcom/twitter/library/provider/r;->a(Ljava/lang/Object;)Lcom/twitter/library/provider/r;

    iget-object v1, v0, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    invoke-virtual {v7, v1}, Lcom/twitter/library/provider/r;->a(Ljava/lang/Object;)Lcom/twitter/library/provider/r;

    iget-object v1, v0, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    invoke-virtual {v7, v1}, Lcom/twitter/library/provider/r;->a(Ljava/lang/Object;)Lcom/twitter/library/provider/r;

    iget-object v1, v0, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    invoke-virtual {v7, v1}, Lcom/twitter/library/provider/r;->a(Ljava/lang/Object;)Lcom/twitter/library/provider/r;

    invoke-static {v0}, Lcom/twitter/library/provider/az;->a(Lcom/twitter/library/api/TwitterUser;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v1}, Lcom/twitter/library/provider/r;->a(Ljava/lang/Object;)Lcom/twitter/library/provider/r;

    iget v1, v0, Lcom/twitter/library/api/TwitterUser;->friendship:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v7, v1}, Lcom/twitter/library/provider/r;->a(Ljava/lang/Object;)Lcom/twitter/library/provider/r;

    iget-object v1, v0, Lcom/twitter/library/api/TwitterUser;->profileDescription:Ljava/lang/String;

    invoke-virtual {v7, v1}, Lcom/twitter/library/provider/r;->a(Ljava/lang/Object;)Lcom/twitter/library/provider/r;

    iget-object v1, v0, Lcom/twitter/library/api/TwitterUser;->descriptionEntities:Lcom/twitter/library/api/TweetEntities;

    if-nez v1, :cond_0

    move-object v1, v4

    :goto_1
    invoke-virtual {v7, v1}, Lcom/twitter/library/provider/r;->a(Ljava/lang/Object;)Lcom/twitter/library/provider/r;

    iget-object v1, v0, Lcom/twitter/library/api/TwitterUser;->promotedContent:Lcom/twitter/library/api/PromotedContent;

    if-nez v1, :cond_1

    move-object v0, v4

    :goto_2
    invoke-virtual {v7, v0}, Lcom/twitter/library/provider/r;->a(Ljava/lang/Object;)Lcom/twitter/library/provider/r;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/twitter/library/provider/r;->a(Ljava/lang/Object;)Lcom/twitter/library/provider/r;

    move v1, v3

    goto :goto_0

    :cond_0
    iget-object v1, v0, Lcom/twitter/library/api/TwitterUser;->descriptionEntities:Lcom/twitter/library/api/TweetEntities;

    invoke-virtual {v1}, Lcom/twitter/library/api/TweetEntities;->a()[B

    move-result-object v1

    goto :goto_1

    :cond_1
    iget-object v0, v0, Lcom/twitter/library/api/TwitterUser;->promotedContent:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {v0}, Lcom/twitter/library/api/PromotedContent;->c()[B

    move-result-object v0

    goto :goto_2

    :cond_2
    invoke-virtual {p0, v5}, Lcom/twitter/android/aaa;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    return-void
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/aaa;->h:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/support/v4/widget/CursorAdapter;->areAllItemsEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(J)Ljava/lang/Long;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/aaa;->n:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    return-object v0
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/aaa;->n:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    return-void
.end method

.method public b(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/aaa;->v:Z

    return-void
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 7

    const/4 v6, 0x0

    const/4 v0, 0x2

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/aaa;->a(Landroid/view/View;JJ)V

    check-cast p1, Lcom/twitter/library/widget/UserView;

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/zx;

    invoke-virtual {p0, p1, p3, v2, v3}, Lcom/twitter/android/aaa;->a(Lcom/twitter/library/widget/BaseUserView;Landroid/database/Cursor;J)V

    iget v1, p0, Lcom/twitter/android/aaa;->a:I

    if-lez v1, :cond_0

    iget-boolean v1, p0, Lcom/twitter/android/aaa;->v:Z

    if-nez v1, :cond_1

    iget-wide v4, p0, Lcom/twitter/android/aaa;->j:J

    cmp-long v1, v4, v2

    if-nez v1, :cond_1

    iget-object v0, p1, Lcom/twitter/library/widget/UserView;->m:Lcom/twitter/library/widget/ActionButton;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/ActionButton;->setVisibility(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p1, Lcom/twitter/library/widget/UserView;->m:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v1, v6}, Lcom/twitter/library/widget/ActionButton;->setVisibility(I)V

    iget-object v1, p0, Lcom/twitter/android/aaa;->g:Lcom/twitter/library/util/FriendshipCache;

    if-eqz v1, :cond_0

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/util/FriendshipCache;->a(J)Z

    move-result v4

    if-nez v4, :cond_2

    iget v0, v0, Lcom/twitter/android/zx;->e:I

    invoke-virtual {v1, v2, v3, v0}, Lcom/twitter/library/util/FriendshipCache;->b(JI)V

    :cond_2
    iget-object v0, p1, Lcom/twitter/library/widget/UserView;->m:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/util/FriendshipCache;->i(J)Z

    move-result v4

    invoke-virtual {v0, v4}, Lcom/twitter/library/widget/ActionButton;->setChecked(Z)V

    iget-boolean v0, p0, Lcom/twitter/android/aaa;->w:Z

    if-eqz v0, :cond_3

    const/4 v0, 0x6

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iget-object v4, p1, Lcom/twitter/library/widget/UserView;->m:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v4}, Lcom/twitter/library/widget/ActionButton;->isChecked()Z

    move-result v4

    if-eqz v4, :cond_5

    and-int/lit8 v0, v0, 0x8

    if-nez v0, :cond_5

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/util/FriendshipCache;->h(J)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/util/af;->a(Ljava/lang/Integer;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lcom/twitter/android/aaa;->x:Z

    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/twitter/library/widget/UserView;->n:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/util/FriendshipCache;->k(J)Z

    move-result v4

    invoke-virtual {v0, v4}, Lcom/twitter/library/widget/ActionButton;->setChecked(Z)V

    :goto_1
    iget-object v0, p1, Lcom/twitter/library/widget/UserView;->n:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v0, v6}, Lcom/twitter/library/widget/ActionButton;->setVisibility(I)V

    :cond_3
    :goto_2
    iget-boolean v0, p0, Lcom/twitter/android/aaa;->y:Z

    if-eqz v0, :cond_0

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/util/FriendshipCache;->h(J)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/util/af;->a(Ljava/lang/Integer;)Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/UserView;->setMuted(Z)V

    goto :goto_0

    :cond_4
    iget-object v0, p1, Lcom/twitter/library/widget/UserView;->n:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/util/FriendshipCache;->j(J)Z

    move-result v4

    invoke-virtual {v0, v4}, Lcom/twitter/library/widget/ActionButton;->setChecked(Z)V

    goto :goto_1

    :cond_5
    iget-object v0, p1, Lcom/twitter/library/widget/UserView;->n:Lcom/twitter/library/widget/ActionButton;

    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Lcom/twitter/library/widget/ActionButton;->setVisibility(I)V

    goto :goto_2
.end method

.method public c(J)Ljava/lang/Long;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/aaa;->n:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    return-object v0
.end method

.method public c(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/aaa;->e:Z

    return-void
.end method

.method public d(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/aaa;->w:Z

    return-void
.end method

.method public e(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/aaa;->y:Z

    return-void
.end method

.method public getCount()I
    .locals 2

    invoke-super {p0}, Landroid/support/v4/widget/CursorAdapter;->getCount()I

    move-result v0

    iget-boolean v1, p0, Lcom/twitter/android/aaa;->h:Z

    if-eqz v1, :cond_0

    if-lez v0, :cond_1

    add-int/lit8 v0, v0, 0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/aaa;->h:Z

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    add-int/lit8 v0, p1, -0x1

    invoke-super {p0, v0}, Landroid/support/v4/widget/CursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-super {p0, p1}, Landroid/support/v4/widget/CursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 3

    const-wide/16 v1, 0x0

    iget-boolean v0, p0, Lcom/twitter/android/aaa;->h:Z

    if-eqz v0, :cond_1

    if-nez p1, :cond_0

    move-wide v0, v1

    :goto_0
    return-wide v0

    :cond_0
    add-int/lit8 p1, p1, -0x1

    :cond_1
    invoke-super {p0, p1}, Landroid/support/v4/widget/CursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    if-eqz v0, :cond_2

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    goto :goto_0

    :cond_2
    move-wide v0, v1

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/aaa;->h:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    const/4 v6, 0x0

    const v3, 0x7f090184    # com.twitter.android.R.id.follow_all

    const/4 v5, 0x0

    const/16 v4, 0x8

    iget-boolean v0, p0, Lcom/twitter/android/aaa;->h:Z

    if-eqz v0, :cond_5

    if-nez p1, :cond_4

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    if-nez p2, :cond_1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f030077    # com.twitter.android.R.layout.follow_all_header

    invoke-virtual {v0, v2, p3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/twitter/android/aaa;->i:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/twitter/android/aaa;->c()V

    iget-object v0, p0, Lcom/twitter/android/aaa;->s:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/aaa;->i:Landroid/widget/Button;

    iget-object v2, p0, Lcom/twitter/android/aaa;->s:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    const v0, 0x7f090183    # com.twitter.android.R.id.found_people

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/aaa;->r:Landroid/widget/TextView;

    iget-boolean v0, p0, Lcom/twitter/android/aaa;->l:Z

    if-nez v0, :cond_1

    invoke-virtual {p2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    invoke-super {p0}, Landroid/support/v4/widget/CursorAdapter;->getCount()I

    move-result v2

    const v0, 0x7f0900da    # com.twitter.android.R.id.subtitle

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget v3, p0, Lcom/twitter/android/aaa;->k:I

    if-lez v3, :cond_2

    iget v3, p0, Lcom/twitter/android/aaa;->k:I

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    :goto_0
    iget v0, p0, Lcom/twitter/android/aaa;->b:I

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/aaa;->r:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v3, p0, Lcom/twitter/android/aaa;->b:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-virtual {v1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    return-object p2

    :cond_2
    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/aaa;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/aaa;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_4
    add-int/lit8 v0, p1, -0x1

    invoke-super {p0, v0, p2, p3}, Landroid/support/v4/widget/CursorAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto :goto_1

    :cond_5
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/widget/CursorAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto :goto_1
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/aaa;->h:Z

    if-eqz v0, :cond_1

    if-eqz p1, :cond_0

    add-int/lit8 v0, p1, -0x1

    invoke-super {p0, v0}, Landroid/support/v4/widget/CursorAdapter;->isEnabled(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-super {p0, p1}, Landroid/support/v4/widget/CursorAdapter;->isEnabled(I)Z

    move-result v0

    goto :goto_0
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    iget-boolean v0, p0, Lcom/twitter/android/aaa;->m:Z

    if-eqz v0, :cond_0

    const v0, 0x7f030029    # com.twitter.android.R.layout.checkable_user_row_view

    :goto_0
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/UserView;

    invoke-virtual {p0, v0}, Lcom/twitter/android/aaa;->a(Lcom/twitter/library/widget/UserView;)Lcom/twitter/library/widget/UserView;

    move-result-object v0

    return-object v0

    :cond_0
    const v0, 0x7f03017d    # com.twitter.android.R.layout.user_row_view

    goto :goto_0
.end method

.method public swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/aaa;->q:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/aaa;->j:J

    invoke-super {p0, p1}, Landroid/support/v4/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
