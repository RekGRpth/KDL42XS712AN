.class Lcom/twitter/android/wv;
.super Landroid/widget/Filter;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/TweetBoxFragment;


# direct methods
.method private constructor <init>(Lcom/twitter/android/TweetBoxFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/wv;->a:Lcom/twitter/android/TweetBoxFragment;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/TweetBoxFragment;Lcom/twitter/android/wq;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/wv;-><init>(Lcom/twitter/android/TweetBoxFragment;)V

    return-void
.end method


# virtual methods
.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 7

    const/4 v4, 0x0

    const/4 v2, 0x0

    new-instance v6, Landroid/widget/Filter$FilterResults;

    invoke-direct {v6}, Landroid/widget/Filter$FilterResults;-><init>()V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-interface {p1, v0, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/util/x;->v:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    :goto_0
    if-nez v1, :cond_0

    invoke-interface {p1, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    const/16 v1, 0x40

    if-ne v0, v1, :cond_4

    :cond_0
    sget-object v1, Lcom/twitter/android/provider/SuggestionsProvider;->c:Landroid/net/Uri;

    :goto_1
    iget-object v0, p0, Lcom/twitter/android/wv;->a:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    iput v1, v6, Landroid/widget/Filter$FilterResults;->count:I

    iput-object v0, v6, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    :cond_1
    return-object v6

    :cond_2
    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v3

    if-lez v3, :cond_3

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_3
    move-object v3, v2

    goto :goto_0

    :cond_4
    invoke-interface {p1, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    const/16 v1, 0x23

    if-ne v0, v1, :cond_5

    sget-object v1, Lcom/twitter/android/provider/SuggestionsProvider;->d:Landroid/net/Uri;

    goto :goto_1

    :cond_5
    move-object v1, v2

    goto :goto_1
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Landroid/database/Cursor;

    iget-object v1, p0, Lcom/twitter/android/wv;->a:Lcom/twitter/android/TweetBoxFragment;

    invoke-virtual {v1}, Lcom/twitter/android/TweetBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-interface {p1, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    const/16 v3, 0x40

    if-eq v2, v3, :cond_0

    sget-object v2, Lcom/twitter/library/util/x;->v:Ljava/util/regex/Pattern;

    invoke-virtual {v2, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_0
    iget-object v2, p0, Lcom/twitter/android/wv;->a:Lcom/twitter/android/TweetBoxFragment;

    iget-object v3, p0, Lcom/twitter/android/wv;->a:Lcom/twitter/android/TweetBoxFragment;

    iget-object v3, v3, Lcom/twitter/android/TweetBoxFragment;->d:Lcom/twitter/android/aac;

    iput-object v3, v2, Lcom/twitter/android/TweetBoxFragment;->c:Landroid/support/v4/widget/CursorAdapter;

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    const/4 v3, 0x4

    if-lt v2, v3, :cond_5

    iget-object v2, p0, Lcom/twitter/android/wv;->a:Lcom/twitter/android/TweetBoxFragment;

    iget-object v2, v2, Lcom/twitter/android/TweetBoxFragment;->d:Lcom/twitter/android/aac;

    invoke-virtual {v2, v4}, Lcom/twitter/android/aac;->a(Z)V

    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/twitter/android/wv;->a:Lcom/twitter/android/TweetBoxFragment;

    iget-object v2, v2, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    iget-object v3, p0, Lcom/twitter/android/wv;->a:Lcom/twitter/android/TweetBoxFragment;

    iget-object v3, v3, Lcom/twitter/android/TweetBoxFragment;->c:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v2, v3}, Lcom/twitter/internal/android/widget/PopupEditText;->setAdapter(Landroid/widget/ListAdapter;)V

    :cond_2
    iget-object v2, p0, Lcom/twitter/android/wv;->a:Lcom/twitter/android/TweetBoxFragment;

    iget-object v2, v2, Lcom/twitter/android/TweetBoxFragment;->c:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v2}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v1, v2}, Landroid/app/Activity;->stopManagingCursor(Landroid/database/Cursor;)V

    :cond_3
    if-eqz v0, :cond_4

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startManagingCursor(Landroid/database/Cursor;)V

    :cond_4
    iget-object v1, p0, Lcom/twitter/android/wv;->a:Lcom/twitter/android/TweetBoxFragment;

    iget-object v1, v1, Lcom/twitter/android/TweetBoxFragment;->c:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v1, v0}, Landroid/support/v4/widget/CursorAdapter;->changeCursor(Landroid/database/Cursor;)V

    return-void

    :cond_5
    iget-object v2, p0, Lcom/twitter/android/wv;->a:Lcom/twitter/android/TweetBoxFragment;

    iget-object v2, v2, Lcom/twitter/android/TweetBoxFragment;->d:Lcom/twitter/android/aac;

    iget-object v3, p0, Lcom/twitter/android/wv;->a:Lcom/twitter/android/TweetBoxFragment;

    invoke-static {v3}, Lcom/twitter/android/TweetBoxFragment;->k(Lcom/twitter/android/TweetBoxFragment;)Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/twitter/android/aac;->a(Z)V

    goto :goto_0

    :cond_6
    invoke-interface {p1, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    const/16 v3, 0x23

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcom/twitter/android/wv;->a:Lcom/twitter/android/TweetBoxFragment;

    iget-object v3, p0, Lcom/twitter/android/wv;->a:Lcom/twitter/android/TweetBoxFragment;

    iget-object v3, v3, Lcom/twitter/android/TweetBoxFragment;->e:Lcom/twitter/android/ww;

    iput-object v3, v2, Lcom/twitter/android/TweetBoxFragment;->c:Landroid/support/v4/widget/CursorAdapter;

    goto :goto_0
.end method
