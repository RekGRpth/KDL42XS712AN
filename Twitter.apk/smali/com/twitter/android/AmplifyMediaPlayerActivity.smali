.class public Lcom/twitter/android/AmplifyMediaPlayerActivity;
.super Lcom/twitter/android/client/BaseFragmentActivity;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Lcom/twitter/library/amplify/h;


# instance fields
.field private final a:Lcom/twitter/library/card/element/Player;

.field private b:Lcom/twitter/library/amplify/AmplifyPlayer;

.field private c:Lcom/twitter/library/provider/Tweet;

.field private d:Lcom/twitter/android/ah;

.field private e:Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;

.field private f:Landroid/net/Uri;

.field private g:Lcom/twitter/library/client/Session;

.field private h:Lcom/twitter/android/client/c;

.field private i:Lcom/twitter/android/ai;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;-><init>()V

    new-instance v0, Lcom/twitter/library/card/element/Player;

    invoke-direct {v0}, Lcom/twitter/library/card/element/Player;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->a:Lcom/twitter/library/card/element/Player;

    new-instance v0, Lcom/twitter/android/ai;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/ai;-><init>(Lcom/twitter/android/AmplifyMediaPlayerActivity;Lcom/twitter/android/ag;)V

    iput-object v0, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->i:Lcom/twitter/android/ai;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/AmplifyMediaPlayerActivity;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->f:Landroid/net/Uri;

    return-object p1
.end method

.method static synthetic a(Lcom/twitter/android/AmplifyMediaPlayerActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->g:Lcom/twitter/library/client/Session;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/AmplifyMediaPlayerActivity;)Lcom/twitter/library/provider/Tweet;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->c:Lcom/twitter/library/provider/Tweet;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/AmplifyMediaPlayerActivity;)Lcom/twitter/library/card/element/Player;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->a:Lcom/twitter/library/card/element/Player;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/AmplifyMediaPlayerActivity;)Lcom/twitter/library/amplify/AmplifyPlayer;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->b:Lcom/twitter/library/amplify/AmplifyPlayer;

    return-object v0
.end method

.method private f()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v1, "fav"

    iget-object v2, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->c:Lcom/twitter/library/provider/Tweet;

    iget-boolean v2, v2, Lcom/twitter/library/provider/Tweet;->l:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string/jumbo v1, "rt"

    iget-object v2, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->c:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v2}, Lcom/twitter/library/provider/Tweet;->e()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/AmplifyMediaPlayerActivity;->setResult(ILandroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V

    invoke-virtual {p0}, Lcom/twitter/android/AmplifyMediaPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "stream_url"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/AmplifyMediaPlayerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "tw"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/provider/Tweet;

    iput-object v0, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->c:Lcom/twitter/library/provider/Tweet;

    :goto_0
    invoke-static {p0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->g:Lcom/twitter/library/client/Session;

    invoke-static {p0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->h:Lcom/twitter/android/client/c;

    iget-object v0, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->a:Lcom/twitter/library/card/element/Player;

    iput-object v1, v0, Lcom/twitter/library/card/element/Player;->streamUrl:Ljava/lang/String;

    new-instance v0, Lcom/twitter/library/amplify/o;

    iget-object v2, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->a:Lcom/twitter/library/card/element/Player;

    invoke-direct {v0, p0, v2}, Lcom/twitter/library/amplify/o;-><init>(Landroid/content/Context;Lcom/twitter/library/card/element/Player;)V

    iget-object v2, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->a:Lcom/twitter/library/card/element/Player;

    invoke-virtual {v2, v0}, Lcom/twitter/library/card/element/Player;->a(Lcom/twitter/library/card/element/h;)V

    invoke-virtual {v0}, Lcom/twitter/library/amplify/o;->i()Lcom/twitter/library/amplify/AmplifyPlayer;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->b:Lcom/twitter/library/amplify/AmplifyPlayer;

    iget-object v2, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->b:Lcom/twitter/library/amplify/AmplifyPlayer;

    iget-object v3, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->c:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v2, v3}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Lcom/twitter/library/provider/Tweet;)V

    iget-object v2, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->b:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v2, p0}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Lcom/twitter/library/amplify/h;)V

    iget-object v2, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->b:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v2, v5}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Z)V

    new-instance v2, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;

    iget-object v3, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->a:Lcom/twitter/library/card/element/Player;

    invoke-direct {v2, p0, v3, v4, v5}, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;-><init>(Landroid/content/Context;Lcom/twitter/library/card/element/Player;ZZ)V

    iput-object v2, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->e:Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;

    iget-object v2, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->e:Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;

    invoke-virtual {v0, v2}, Lcom/twitter/library/amplify/o;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->e:Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;

    invoke-virtual {v0, v4}, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->setIsFullScreenToggleAllowed(Z)V

    iget-object v0, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->e:Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;

    invoke-virtual {p0, v0}, Lcom/twitter/android/AmplifyMediaPlayerActivity;->setContentView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->h:Lcom/twitter/android/client/c;

    iget-object v2, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->i:Lcom/twitter/android/ai;

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/j;)V

    if-eqz v1, :cond_1

    new-instance v0, Lcom/twitter/android/ah;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/ah;-><init>(Lcom/twitter/android/AmplifyMediaPlayerActivity;Lcom/twitter/android/ag;)V

    iput-object v0, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->d:Lcom/twitter/android/ah;

    iget-object v0, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->d:Lcom/twitter/android/ah;

    new-array v1, v4, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/ah;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_1
    return-void

    :cond_0
    const-string/jumbo v0, "tw"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/provider/Tweet;

    iput-object v0, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->c:Lcom/twitter/library/provider/Tweet;

    goto :goto_0

    :cond_1
    const v0, 0x7f0f0268    # com.twitter.android.R.string.media_player_error_default

    invoke-static {p0, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/twitter/android/AmplifyMediaPlayerActivity;->finish()V

    goto :goto_1
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 2

    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/twitter/library/provider/Tweet;

    invoke-direct {v0, p2}, Lcom/twitter/library/provider/Tweet;-><init>(Landroid/database/Cursor;)V

    iput-object v0, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->c:Lcom/twitter/library/provider/Tweet;

    iget-object v0, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->b:Lcom/twitter/library/amplify/AmplifyPlayer;

    iget-object v1, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->c:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Lcom/twitter/library/provider/Tweet;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/card/element/Player;Z)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/provider/Tweet;ZZ)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, v0, p1, p2, p3}, Lcom/twitter/android/amplify/a;->a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeAssociation;Lcom/twitter/library/provider/Tweet;ZZ)V

    return-void
.end method

.method public a(Ljava/lang/String;ILcom/twitter/library/amplify/model/AmplifyVideo;DIZLjava/lang/String;Z)V
    .locals 12

    iget-object v1, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->c:Lcom/twitter/library/provider/Tweet;

    const/4 v2, 0x0

    move-object v0, p0

    move-object v3, p1

    move v4, p2

    move-object v5, p3

    move-wide/from16 v6, p4

    move/from16 v8, p6

    move/from16 v9, p7

    move-object/from16 v10, p8

    move/from16 v11, p9

    invoke-static/range {v0 .. v11}, Lcom/twitter/android/amplify/a;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;ILcom/twitter/library/amplify/model/AmplifyVideo;DIZLjava/lang/String;Z)V

    return-void
.end method

.method public onBackPressed()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/AmplifyMediaPlayerActivity;->f()V

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onBackPressed()V

    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->f:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    const-string/jumbo v1, "status_groups_retweets_view"

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v3, Lcom/twitter/library/provider/Tweet;->b:[Ljava/lang/String;

    :goto_0
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    iget-object v2, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->f:Landroid/net/Uri;

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_0
    sget-object v3, Lcom/twitter/library/provider/Tweet;->a:[Ljava/lang/String;

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    invoke-direct {p0}, Lcom/twitter/android/AmplifyMediaPlayerActivity;->f()V

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onDestroy()V

    iget-object v0, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->h:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->i:Lcom/twitter/android/ai;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/j;)V

    iget-object v0, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->b:Lcom/twitter/library/amplify/AmplifyPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->b:Lcom/twitter/library/amplify/AmplifyPlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/AmplifyPlayer;->c(Z)V

    :cond_0
    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/AmplifyMediaPlayerActivity;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0

    return-void
.end method

.method protected onPause()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onPause()V

    iget-object v0, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->d:Lcom/twitter/android/ah;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->d:Lcom/twitter/android/ah;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/ah;->cancel(Z)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->d:Lcom/twitter/android/ah;

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->b:Lcom/twitter/library/amplify/AmplifyPlayer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->b:Lcom/twitter/library/amplify/AmplifyPlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/AmplifyPlayer;->h(Z)V

    :cond_1
    return-void
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onResume()V

    iget-object v0, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->b:Lcom/twitter/library/amplify/AmplifyPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->b:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->C()V

    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "tw"

    iget-object v1, p0, Lcom/twitter/android/AmplifyMediaPlayerActivity;->c:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method
