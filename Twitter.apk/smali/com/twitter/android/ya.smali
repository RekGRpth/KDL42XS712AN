.class Lcom/twitter/android/ya;
.super Landroid/os/AsyncTask;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/TweetSettingsActivity;


# direct methods
.method private constructor <init>(Lcom/twitter/android/TweetSettingsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/ya;->a:Lcom/twitter/android/TweetSettingsActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/TweetSettingsActivity;Lcom/twitter/android/xx;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/ya;-><init>(Lcom/twitter/android/TweetSettingsActivity;)V

    return-void
.end method

.method private a(Z)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/ya;->a:Lcom/twitter/android/TweetSettingsActivity;

    invoke-static {v0}, Lcom/twitter/android/TweetSettingsActivity;->a(Lcom/twitter/android/TweetSettingsActivity;)Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/library/api/UserSettings;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v1, v0, Lcom/twitter/library/api/UserSettings;->o:Z

    if-eq v1, p1, :cond_0

    iput-boolean p1, v0, Lcom/twitter/library/api/UserSettings;->o:Z

    iget-object v1, p0, Lcom/twitter/android/ya;->a:Lcom/twitter/android/TweetSettingsActivity;

    invoke-static {v1}, Lcom/twitter/android/TweetSettingsActivity;->b(Lcom/twitter/android/TweetSettingsActivity;)Lcom/twitter/android/client/c;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/ya;->a:Lcom/twitter/android/TweetSettingsActivity;

    invoke-static {v2}, Lcom/twitter/android/TweetSettingsActivity;->a(Lcom/twitter/android/TweetSettingsActivity;)Lcom/twitter/library/client/Session;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/api/UserSettings;Z)Ljava/lang/String;

    :cond_0
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/ya;->a:Lcom/twitter/android/TweetSettingsActivity;

    invoke-static {v0}, Lcom/twitter/android/TweetSettingsActivity;->a(Lcom/twitter/android/TweetSettingsActivity;)Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/ya;->a:Lcom/twitter/android/TweetSettingsActivity;

    invoke-static {v0, v1}, Lcom/twitter/library/platform/PushService;->f(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    iget-object v2, p0, Lcom/twitter/android/ya;->a:Lcom/twitter/android/TweetSettingsActivity;

    invoke-virtual {v2}, Lcom/twitter/android/TweetSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/android/client/aw;->a(Landroid/content/Context;)Lcom/twitter/android/client/aw;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/ya;->a:Lcom/twitter/android/TweetSettingsActivity;

    invoke-static {v3}, Lcom/twitter/android/TweetSettingsActivity;->c(Lcom/twitter/android/TweetSettingsActivity;)Z

    move-result v3

    if-eqz v3, :cond_2

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v1, v0}, Lcom/twitter/android/client/aw;->a(Ljava/lang/String;Z)V

    :goto_1
    iget-object v0, p0, Lcom/twitter/android/ya;->a:Lcom/twitter/android/TweetSettingsActivity;

    invoke-static {v0}, Lcom/twitter/android/TweetSettingsActivity;->d(Lcom/twitter/android/TweetSettingsActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ya;->a:Lcom/twitter/android/TweetSettingsActivity;

    invoke-static {v0}, Lcom/twitter/android/TweetSettingsActivity;->c(Lcom/twitter/android/TweetSettingsActivity;)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/twitter/android/ya;->a(Z)V

    :cond_0
    const/4 v0, 0x0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-virtual {v2, v1}, Lcom/twitter/android/client/aw;->b(Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/ya;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
