.class Lcom/twitter/android/uw;
.super Landroid/database/DataSetObserver;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/uh;

.field final synthetic b:Lcom/twitter/android/client/c;

.field final synthetic c:Lcom/twitter/android/TimelineFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/TimelineFragment;Lcom/twitter/android/uh;Lcom/twitter/android/client/c;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/uw;->c:Lcom/twitter/android/TimelineFragment;

    iput-object p2, p0, Lcom/twitter/android/uw;->a:Lcom/twitter/android/uh;

    iput-object p3, p0, Lcom/twitter/android/uw;->b:Lcom/twitter/android/client/c;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/uw;->c:Lcom/twitter/android/TimelineFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TimelineFragment;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/uw;->c:Lcom/twitter/android/TimelineFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TimelineFragment;->F()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/uw;->a:Lcom/twitter/android/uh;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/uh;->b(I)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/uw;->b:Lcom/twitter/android/client/c;

    invoke-virtual {v2, v0, v1}, Lcom/twitter/android/client/c;->r(J)Ljava/lang/String;

    :cond_0
    return-void
.end method
