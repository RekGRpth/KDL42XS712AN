.class public Lcom/twitter/android/MediaTagActivity;
.super Lcom/twitter/android/client/BaseFragmentActivity;
.source "Twttr"


# instance fields
.field private a:Lcom/twitter/android/MediaTagFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;-><init>()V

    return-void
.end method

.method private a(Ljava/util/ArrayList;Landroid/net/Uri;)V
    .locals 4

    new-instance v0, Lcom/twitter/android/MediaTagFragment;

    invoke-direct {v0}, Lcom/twitter/android/MediaTagFragment;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/MediaTagActivity;->a:Lcom/twitter/android/MediaTagFragment;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v1, "photo_tags"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const-string/jumbo v1, "uri"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v1, p0, Lcom/twitter/android/MediaTagActivity;->a:Lcom/twitter/android/MediaTagFragment;

    invoke-virtual {v1, v0}, Lcom/twitter/android/MediaTagFragment;->setArguments(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/MediaTagActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0901d7    # com.twitter.android.R.id.user_suggestion_container

    iget-object v2, p0, Lcom/twitter/android/MediaTagActivity;->a:Lcom/twitter/android/MediaTagFragment;

    const-string/jumbo v3, "user_select"

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/android/MediaTagActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/client/f;

    const-string/jumbo v2, "media_tags"

    invoke-direct {v1, p0, v0, v2}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v0

    const-string/jumbo v1, "recent_tags"

    invoke-virtual {v0, v1, p1}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/twitter/library/client/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->d()V

    return-void
.end method

.method private f()Ljava/util/List;
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/android/MediaTagActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/client/f;

    const-string/jumbo v2, "media_tags"

    invoke-direct {v1, p0, v0, v2}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "recent_tags"

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 2

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    const v1, 0x7f0300ba    # com.twitter.android.R.layout.media_tag_activity

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->a(Z)V

    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/android/MediaTagActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/MediaTagActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string/jumbo v2, "user_select"

    invoke-virtual {v0, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/MediaTagFragment;

    iput-object v0, p0, Lcom/twitter/android/MediaTagActivity;->a:Lcom/twitter/android/MediaTagFragment;

    iget-object v0, p0, Lcom/twitter/android/MediaTagActivity;->a:Lcom/twitter/android/MediaTagFragment;

    if-nez v0, :cond_0

    const-string/jumbo v0, "photo_tags"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    const-string/jumbo v2, "uri"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/MediaTagActivity;->a(Ljava/util/ArrayList;Landroid/net/Uri;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/MediaTagActivity;->a:Lcom/twitter/android/MediaTagFragment;

    invoke-direct {p0}, Lcom/twitter/android/MediaTagActivity;->f()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/MediaTagFragment;->a(Ljava/util/List;)V

    return-void
.end method

.method public a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 1

    const v0, 0x7f110016    # com.twitter.android.R.menu.media_tagging

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z

    move-result v0

    return v0
.end method

.method public a(Lhn;)Z
    .locals 9

    const v3, 0x7f090321    # com.twitter.android.R.id.done_tagging

    const v2, 0x7f090045    # com.twitter.android.R.id.home

    const/16 v8, 0x14

    const/4 v1, 0x1

    const/4 v7, 0x0

    invoke-virtual {p1}, Lhn;->a()I

    move-result v0

    if-eq v0, v3, :cond_0

    if-ne v0, v2, :cond_8

    :cond_0
    if-ne v0, v3, :cond_7

    iget-object v0, p0, Lcom/twitter/android/MediaTagActivity;->a:Lcom/twitter/android/MediaTagFragment;

    invoke-virtual {v0}, Lcom/twitter/android/MediaTagFragment;->b()Ljava/util/ArrayList;

    move-result-object v2

    const/4 v0, -0x1

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v4, "photo_tags"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0, v0, v3}, Lcom/twitter/android/MediaTagActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    new-instance v3, Ljava/util/LinkedHashSet;

    invoke-direct {v3}, Ljava/util/LinkedHashSet;-><init>()V

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaTag;

    iget-wide v5, v0, Lcom/twitter/library/api/MediaTag;->userId:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/twitter/android/MediaTagActivity;->f()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v3, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-le v3, v8, :cond_3

    invoke-interface {v0, v7, v8}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    :cond_3
    invoke-direct {p0, v0}, Lcom/twitter/android/MediaTagActivity;->a(Ljava/util/List;)V

    :cond_4
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {p0}, Lcom/twitter/android/MediaTagActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0f0270    # com.twitter.android.R.string.media_tag_no_people_tagged

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {p0}, Lcom/twitter/android/MediaTagActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_5
    :goto_2
    invoke-virtual {p0}, Lcom/twitter/android/MediaTagActivity;->finish()V

    move v0, v1

    :goto_3
    return v0

    :cond_6
    invoke-virtual {p0}, Lcom/twitter/android/MediaTagActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e0006    # com.twitter.android.R.plurals.media_tag_tagged

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_7
    if-ne v0, v2, :cond_5

    invoke-virtual {p0, v7}, Lcom/twitter/android/MediaTagActivity;->setResult(I)V

    goto :goto_2

    :cond_8
    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lhn;)Z

    move-result v0

    goto :goto_3
.end method
