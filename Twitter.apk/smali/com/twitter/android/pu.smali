.class Lcom/twitter/android/pu;
.super Lcom/twitter/android/client/ap;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/PostActivity;


# direct methods
.method public constructor <init>(Lcom/twitter/android/PostActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/pu;->a:Lcom/twitter/android/PostActivity;

    invoke-direct {p0, p1}, Lcom/twitter/android/client/ap;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/api/geo/b;)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p1, Lcom/twitter/library/api/geo/b;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/twitter/library/api/geo/b;->h()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/pu;->a:Lcom/twitter/android/PostActivity;

    iget-object v2, v2, Lcom/twitter/android/PostActivity;->r:Lcom/twitter/android/PostStorage;

    invoke-virtual {v2}, Lcom/twitter/android/PostStorage;->c()Lcom/twitter/android/PostStorage$LocationItem;

    move-result-object v2

    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {v2}, Lcom/twitter/android/PostStorage$LocationItem;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/pu;->a:Lcom/twitter/android/PostActivity;

    iget-boolean v0, v0, Lcom/twitter/android/PostActivity;->z:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/pu;->a:Lcom/twitter/android/PostActivity;

    iget-object v0, v0, Lcom/twitter/android/PostActivity;->B:Lcom/twitter/android/util/z;

    invoke-virtual {v0, v1}, Lcom/twitter/android/util/z;->a(Ljava/util/List;)V

    iget-object v0, p0, Lcom/twitter/android/pu;->a:Lcom/twitter/android/PostActivity;

    iget-object v0, v0, Lcom/twitter/android/PostActivity;->B:Lcom/twitter/android/util/z;

    invoke-virtual {v0}, Lcom/twitter/android/util/z;->d()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/pu;->a:Lcom/twitter/android/PostActivity;

    iget-boolean v1, v1, Lcom/twitter/android/PostActivity;->u:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/pu;->a:Lcom/twitter/android/PostActivity;

    iget-object v1, v1, Lcom/twitter/android/PostActivity;->q:Lcom/twitter/android/widget/PoiFragment;

    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/PoiFragment;->b(Ljava/util/List;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/pu;->a:Lcom/twitter/android/PostActivity;

    iget-object v0, v0, Lcom/twitter/android/PostActivity;->q:Lcom/twitter/android/widget/PoiFragment;

    invoke-virtual {v0, v4}, Lcom/twitter/android/widget/PoiFragment;->b(Z)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/geo/TwitterPlace;

    iget-object v1, v0, Lcom/twitter/library/api/geo/TwitterPlace;->fullName:Ljava/lang/String;

    iget-object v0, v0, Lcom/twitter/library/api/geo/TwitterPlace;->placeId:Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Lcom/twitter/android/PostStorage$LocationItem;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/pu;->a:Lcom/twitter/android/PostActivity;

    invoke-virtual {v0, v1}, Lcom/twitter/android/PostActivity;->b(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/api/geo/c;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/pu;->a:Lcom/twitter/android/PostActivity;

    iget-boolean v0, v0, Lcom/twitter/android/PostActivity;->z:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/pu;->a:Lcom/twitter/android/PostActivity;

    iget-object v0, v0, Lcom/twitter/android/PostActivity;->B:Lcom/twitter/android/util/z;

    invoke-virtual {p1}, Lcom/twitter/library/api/geo/c;->h()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/pu;->a:Lcom/twitter/android/PostActivity;

    iget-object v2, v2, Lcom/twitter/android/PostActivity;->r:Lcom/twitter/android/PostStorage;

    invoke-virtual {v2}, Lcom/twitter/android/PostStorage;->a()Landroid/location/Location;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/util/z;->a(Ljava/util/List;Landroid/location/Location;)V

    iget-object v0, p0, Lcom/twitter/android/pu;->a:Lcom/twitter/android/PostActivity;

    iget-object v0, v0, Lcom/twitter/android/PostActivity;->q:Lcom/twitter/android/widget/PoiFragment;

    iget-object v1, p0, Lcom/twitter/android/pu;->a:Lcom/twitter/android/PostActivity;

    iget-object v1, v1, Lcom/twitter/android/PostActivity;->B:Lcom/twitter/android/util/z;

    invoke-virtual {v1}, Lcom/twitter/android/util/z;->e()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PoiFragment;->a(Ljava/util/List;)V

    iget-object v0, p0, Lcom/twitter/android/pu;->a:Lcom/twitter/android/PostActivity;

    iget-object v0, v0, Lcom/twitter/android/PostActivity;->q:Lcom/twitter/android/widget/PoiFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PoiFragment;->b(Z)V

    goto :goto_0
.end method
