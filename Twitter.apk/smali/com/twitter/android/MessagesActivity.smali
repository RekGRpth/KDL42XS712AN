.class public Lcom/twitter/android/MessagesActivity;
.super Lcom/twitter/android/BaseMessagesActivity;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/md;
.implements Lcom/twitter/android/mm;
.implements Lcom/twitter/android/widget/ce;


# static fields
.field private static final f:Ljava/util/HashMap;

.field private static final g:Ljava/util/HashMap;


# instance fields
.field private h:I

.field private i:Lcom/twitter/android/MessagesFragment;

.field private j:Landroid/support/v4/widget/SlidingPaneLayout;

.field private k:Lcom/twitter/android/mg;

.field private l:Ljava/lang/Boolean;

.field private m:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const v7, 0x7f090324    # com.twitter.android.R.id.menu_messages_delete

    const v6, 0x7f090323    # com.twitter.android.R.id.dm_mark_as_read

    const v5, 0x7f090322    # com.twitter.android.R.id.menu_compose_dm

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/twitter/android/MessagesActivity;->f:Ljava/util/HashMap;

    sget-object v0, Lcom/twitter/android/MessagesActivity;->f:Ljava/util/HashMap;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/MessagesActivity;->f:Ljava/util/HashMap;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/MessagesActivity;->f:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/twitter/android/MessagesActivity;->g:Ljava/util/HashMap;

    sget-object v0, Lcom/twitter/android/MessagesActivity;->g:Ljava/util/HashMap;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/MessagesActivity;->g:Ljava/util/HashMap;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/MessagesActivity;->g:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/BaseMessagesActivity;-><init>()V

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/MessagesActivity;->l:Ljava/lang/Boolean;

    return-void
.end method

.method public static a(Landroid/content/Context;)Ljava/lang/Class;
    .locals 1

    invoke-static {p0}, Lcom/twitter/library/api/conversations/ae;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-class v0, Lcom/twitter/android/DMInboxActivity;

    :goto_0
    return-object v0

    :cond_0
    const-class v0, Lcom/twitter/android/MessagesActivity;

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/MessagesActivity;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/MessagesActivity;->m:Ljava/lang/Integer;

    return-object p1
.end method

.method static synthetic a(Lcom/twitter/android/MessagesActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/MessagesActivity;->g()V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/MessagesActivity;)Landroid/support/v4/widget/SlidingPaneLayout;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->j:Landroid/support/v4/widget/SlidingPaneLayout;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/MessagesActivity;)I
    .locals 1

    iget v0, p0, Lcom/twitter/android/MessagesActivity;->h:I

    return v0
.end method

.method private g()V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->l:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->j:Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/SlidingPaneLayout;->isSlideable()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/twitter/android/MessagesActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v2

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->m:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    sget-object v0, Lcom/twitter/android/MessagesActivity;->g:Ljava/util/HashMap;

    :goto_0
    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v1, v0}, Lhn;->b(Z)Lhn;

    goto :goto_1

    :cond_1
    sget-object v0, Lcom/twitter/android/MessagesActivity;->f:Ljava/util/HashMap;

    goto :goto_0

    :cond_2
    invoke-virtual {v2}, Lcom/twitter/internal/android/widget/ToolBar;->requestLayout()V

    :cond_3
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 5

    const/4 v4, 0x0

    const v0, 0x7f030079    # com.twitter.android.R.layout.fragment_list_layout

    invoke-static {}, Lcom/twitter/library/client/App;->r()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->aA()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "android_two_pane_dms_1840"

    invoke-static {v1}, Lkk;->b(Ljava/lang/String;)V

    const-string/jumbo v1, "android_two_pane_dms_1840"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "two_pane_disabled"

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const v0, 0x7f0300cb    # com.twitter.android.R.layout.messages_two_pane

    :cond_0
    new-instance v1, Lcom/twitter/android/client/z;

    invoke-direct {v1, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/z;->d(I)V

    invoke-virtual {v1, v4}, Lcom/twitter/android/client/z;->a(Z)V

    invoke-virtual {v1, v4}, Lcom/twitter/android/client/z;->a(I)V

    return-object v1
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 6

    const/4 v3, 0x1

    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/BaseMessagesActivity;->a(Landroid/content/DialogInterface;II)V

    if-ne p2, v3, :cond_0

    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/MessagesActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/MessagesActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "messages:inbox::mark_all_as_read:click"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->l()Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public a(Landroid/content/Intent;)V
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->l:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->j:Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/SlidingPaneLayout;->isSlideable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->j:Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/SlidingPaneLayout;->closePane()Z

    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "user_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const-string/jumbo v2, "user_fullname"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "user_name"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "user_profile_image"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "keyboard_open"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/MessagesActivity;->a(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0, p1}, Lcom/twitter/android/MessagesActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 13

    const/4 v12, 0x0

    const v11, 0x7f0901ec    # com.twitter.android.R.id.messages_fragment_container

    const/4 v5, 0x0

    const/4 v6, 0x1

    const-wide/16 v9, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/MessagesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v11}, Lcom/twitter/android/MessagesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    move v0, v6

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/MessagesActivity;->l:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->l:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    const-string/jumbo v0, "user_id"

    invoke-virtual {v1, v0, v9, v10}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    const-string/jumbo v2, "user_name"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "android.intent.extra.TEXT"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    cmp-long v0, v7, v9

    if-gtz v0, :cond_0

    if-nez v2, :cond_0

    if-eqz v3, :cond_2

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/MessagesDetailActivity;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/twitter/android/MessagesActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/twitter/android/MessagesActivity;->finish()V

    :goto_1
    return-void

    :cond_1
    move v0, v5

    goto :goto_0

    :cond_2
    if-nez p1, :cond_a

    if-eqz v1, :cond_a

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    :goto_2
    if-nez v0, :cond_9

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    move-object v4, v0

    :goto_3
    invoke-super {p0, p1, p2}, Lcom/twitter/android/BaseMessagesActivity;->a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V

    const-string/jumbo v0, "user_id"

    invoke-virtual {v4, v0, v9, v10}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->l:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_8

    const-string/jumbo v0, "pane_focused"

    invoke-virtual {v4, v0, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/MessagesActivity;->m:Ljava/lang/Integer;

    const v0, 0x7f0901eb    # com.twitter.android.R.id.sliding_layout

    invoke-virtual {p0, v0}, Lcom/twitter/android/MessagesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/SlidingPaneLayout;

    iput-object v0, p0, Lcom/twitter/android/MessagesActivity;->j:Landroid/support/v4/widget/SlidingPaneLayout;

    iput v11, p0, Lcom/twitter/android/MessagesActivity;->h:I

    invoke-virtual {p0, v4, p1}, Lcom/twitter/android/MessagesActivity;->a(Landroid/os/Bundle;Landroid/os/Bundle;)V

    const-string/jumbo v0, "android.intent.extra.TEXT"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v2, v2, v9

    if-gtz v2, :cond_3

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    :cond_3
    const-string/jumbo v0, "user_fullname"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v0, "user_name"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v0, "user_profile_image"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/MessagesActivity;->a(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    :goto_4
    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->j:Landroid/support/v4/widget/SlidingPaneLayout;

    new-instance v1, Lcom/twitter/android/me;

    invoke-direct {v1, p0}, Lcom/twitter/android/me;-><init>(Lcom/twitter/android/MessagesActivity;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SlidingPaneLayout;->setPanelSlideListener(Landroid/support/v4/widget/SlidingPaneLayout$PanelSlideListener;)V

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->j:Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-virtual {p0}, Lcom/twitter/android/MessagesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0022    # com.twitter.android.R.color.clear

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SlidingPaneLayout;->setSliderFadeColor(I)V

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->j:Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-virtual {p0}, Lcom/twitter/android/MessagesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c009a    # com.twitter.android.R.dimen.message_two_pane_parallax_distance

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SlidingPaneLayout;->setParallaxDistance(I)V

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->j:Landroid/support/v4/widget/SlidingPaneLayout;

    const v1, 0x7f0202e0    # com.twitter.android.R.drawable.shadow_black

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SlidingPaneLayout;->setShadowResource(I)V

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->j:Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-virtual {p0}, Lcom/twitter/android/MessagesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0004    # com.twitter.android.R.color.border_gray

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SlidingPaneLayout;->setSliderFadeColor(I)V

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->j:Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/SlidingPaneLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/mf;

    invoke-direct {v1, p0}, Lcom/twitter/android/mf;-><init>(Lcom/twitter/android/MessagesActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    :goto_5
    invoke-virtual {p0}, Lcom/twitter/android/MessagesActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string/jumbo v1, "mmessages"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/MessagesFragment;

    iput-object v0, p0, Lcom/twitter/android/MessagesActivity;->i:Lcom/twitter/android/MessagesFragment;

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->i:Lcom/twitter/android/MessagesFragment;

    if-nez v0, :cond_5

    new-instance v0, Lcom/twitter/android/MessagesFragment;

    invoke-direct {v0}, Lcom/twitter/android/MessagesFragment;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/MessagesActivity;->i:Lcom/twitter/android/MessagesFragment;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v1, "extra_use_placeholder"

    iget-object v2, p0, Lcom/twitter/android/MessagesActivity;->l:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v1, p0, Lcom/twitter/android/MessagesActivity;->l:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_4

    const-string/jumbo v1, "chmode"

    invoke-virtual {v0, v1, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_4
    iget-object v1, p0, Lcom/twitter/android/MessagesActivity;->i:Lcom/twitter/android/MessagesFragment;

    invoke-virtual {v1, v0}, Lcom/twitter/android/MessagesFragment;->setArguments(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/MessagesActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget v1, p0, Lcom/twitter/android/MessagesActivity;->h:I

    iget-object v2, p0, Lcom/twitter/android/MessagesActivity;->i:Lcom/twitter/android/MessagesFragment;

    const-string/jumbo v3, "mmessages"

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    :cond_5
    new-instance v0, Lcom/twitter/android/mg;

    invoke-direct {v0, p0}, Lcom/twitter/android/mg;-><init>(Lcom/twitter/android/MessagesActivity;)V

    iput-object v0, p0, Lcom/twitter/android/MessagesActivity;->k:Lcom/twitter/android/mg;

    invoke-virtual {p0}, Lcom/twitter/android/MessagesActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/MessagesActivity;->k:Lcom/twitter/android/mg;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/z;)V

    goto/16 :goto_1

    :cond_6
    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->e:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->e:Ljava/lang/String;

    invoke-virtual {p0, v0, v12}, Lcom/twitter/android/MessagesActivity;->a(Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/Fragment;

    goto/16 :goto_4

    :cond_7
    const-string/jumbo v0, "mempty"

    invoke-virtual {p0, v0, v12}, Lcom/twitter/android/MessagesActivity;->a(Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/Fragment;

    goto/16 :goto_4

    :cond_8
    const v0, 0x7f0900e3    # com.twitter.android.R.id.fragment_container

    iput v0, p0, Lcom/twitter/android/MessagesActivity;->h:I

    invoke-virtual {p0}, Lcom/twitter/android/MessagesActivity;->e()V

    goto/16 :goto_5

    :cond_9
    move-object v4, v0

    goto/16 :goto_3

    :cond_a
    move-object v0, p1

    goto/16 :goto_2
.end method

.method public a(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 4

    invoke-super/range {p0 .. p5}, Lcom/twitter/android/BaseMessagesActivity;->a(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->l:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->i:Lcom/twitter/android/MessagesFragment;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->i:Lcom/twitter/android/MessagesFragment;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/twitter/android/MessagesFragment;->a(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/twitter/android/BaseMessagesActivity;->a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z

    const v0, 0x7f110017    # com.twitter.android.R.menu.message_inbox_toolbar

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    const/4 v0, 0x1

    return v0
.end method

.method public a(Lhn;)Z
    .locals 5

    const/4 v0, 0x1

    invoke-virtual {p1}, Lhn;->a()I

    move-result v1

    const v2, 0x7f090322    # com.twitter.android.R.id.menu_compose_dm

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/twitter/android/MessagesActivity;->l:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/MessagesActivity;->j_()V

    iget-object v1, p0, Lcom/twitter/android/MessagesActivity;->j:Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-virtual {v1}, Landroid/support/v4/widget/SlidingPaneLayout;->closePane()Z

    :goto_0
    return v0

    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/MessagesDetailActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "owner_id"

    invoke-virtual {p0}, Lcom/twitter/android/MessagesActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/MessagesActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_1
    const v2, 0x7f090323    # com.twitter.android.R.id.dm_mark_as_read

    if-ne v1, v2, :cond_2

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    const v2, 0x7f0f0289    # com.twitter.android.R.string.messages_mark_all_read_confirmation

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->e(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    const v2, 0x7f0f0571    # com.twitter.android.R.string.yes

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->g(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    const v2, 0x7f0f029d    # com.twitter.android.R.string.no

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->i(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Lcom/twitter/android/widget/ce;)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/MessagesActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    goto :goto_0

    :cond_2
    invoke-super {p0, p1}, Lcom/twitter/android/BaseMessagesActivity;->a(Lhn;)Z

    move-result v0

    goto :goto_0
.end method

.method public a_(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->l:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string/jumbo v0, "mempty"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/MessagesActivity;->a(Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/Fragment;

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->i:Lcom/twitter/android/MessagesFragment;

    invoke-virtual {v0}, Lcom/twitter/android/MessagesFragment;->e()V

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->i:Lcom/twitter/android/MessagesFragment;

    invoke-virtual {v0}, Lcom/twitter/android/MessagesFragment;->q()V

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->j:Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/SlidingPaneLayout;->isSlideable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->j:Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/SlidingPaneLayout;->openPane()Z

    goto :goto_0
.end method

.method public d()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->l:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0}, Lcom/twitter/android/BaseMessagesActivity;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected e()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->l:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->j:Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/SlidingPaneLayout;->isSlideable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->j:Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/SlidingPaneLayout;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const v0, 0x7f0f01d0    # com.twitter.android.R.string.home_direct_messages

    invoke-virtual {p0, v0}, Lcom/twitter/android/MessagesActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/MessagesActivity;->setTitle(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/MessagesActivity;->a(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_1
    invoke-super {p0}, Lcom/twitter/android/BaseMessagesActivity;->e()V

    goto :goto_0
.end method

.method public f()V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/android/MessagesActivity;->j_()V

    return-void
.end method

.method protected h_()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->i:Lcom/twitter/android/MessagesFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->i:Lcom/twitter/android/MessagesFragment;

    invoke-virtual {v0}, Lcom/twitter/android/MessagesFragment;->aa()V

    :cond_0
    return-void
.end method

.method protected j()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->l:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->j:Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/SlidingPaneLayout;->isOpen()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->j:Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/SlidingPaneLayout;->openPane()Z

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Lcom/twitter/android/BaseMessagesActivity;->j()V

    goto :goto_0
.end method

.method protected m_()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->i:Lcom/twitter/android/MessagesFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->i:Lcom/twitter/android/MessagesFragment;

    invoke-virtual {v0}, Lcom/twitter/android/MessagesFragment;->Z()V

    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->l:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->j:Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/SlidingPaneLayout;->isOpen()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->j:Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/SlidingPaneLayout;->openPane()Z

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Lcom/twitter/android/BaseMessagesActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/BaseMessagesActivity;->onDestroy()V

    invoke-virtual {p0}, Lcom/twitter/android/MessagesActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/MessagesActivity;->k:Lcom/twitter/android/mg;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/aa;->b(Lcom/twitter/library/client/z;)V

    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/twitter/android/BaseMessagesActivity;->onNewIntent(Landroid/content/Intent;)V

    invoke-virtual {p0, p1}, Lcom/twitter/android/MessagesActivity;->setIntent(Landroid/content/Intent;)V

    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/BaseMessagesActivity;->onPostCreate(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->l:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->e:Ljava/lang/String;

    const-string/jumbo v1, "mempty"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->j:Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/SlidingPaneLayout;->openPane()Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->j:Landroid/support/v4/widget/SlidingPaneLayout;

    invoke-virtual {v0}, Landroid/support/v4/widget/SlidingPaneLayout;->closePane()Z

    goto :goto_0
.end method

.method public onResume()V
    .locals 5

    invoke-super {p0}, Lcom/twitter/android/BaseMessagesActivity;->onResume()V

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->l:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/twitter/android/MessagesActivity;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->i:Lcom/twitter/android/MessagesFragment;

    iget-wide v1, p0, Lcom/twitter/android/MessagesActivity;->d:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/MessagesActivity;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/MessagesActivity;->a:Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/android/MessagesActivity;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/android/MessagesFragment;->a(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-static {p0}, Lcom/twitter/android/client/aw;->a(Landroid/content/Context;)Lcom/twitter/android/client/aw;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/MessagesActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/aw;->d(Ljava/lang/String;)V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/BaseMessagesActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/twitter/android/MessagesActivity;->m:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "pane_focused"

    iget-object v1, p0, Lcom/twitter/android/MessagesActivity;->m:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    return-void
.end method
