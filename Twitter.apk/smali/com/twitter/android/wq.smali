.class Lcom/twitter/android/wq;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/TweetBoxFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/TweetBoxFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/wq;->a:Lcom/twitter/android/TweetBoxFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/wq;->a:Lcom/twitter/android/TweetBoxFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetBoxFragment;->a(Lcom/twitter/android/TweetBoxFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/wq;->a:Lcom/twitter/android/TweetBoxFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/android/TweetBoxFragment;->a(Lcom/twitter/android/TweetBoxFragment;Z)Z

    iget-object v0, p0, Lcom/twitter/android/wq;->a:Lcom/twitter/android/TweetBoxFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetBoxFragment;->b(Lcom/twitter/android/TweetBoxFragment;)[Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/wq;->a:Lcom/twitter/android/TweetBoxFragment;

    iget-object v0, v0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    new-instance v1, Lcom/twitter/android/wr;

    invoke-direct {v1, p0}, Lcom/twitter/android/wr;-><init>(Lcom/twitter/android/wq;)V

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/PopupEditText;->post(Ljava/lang/Runnable;)Z

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/wq;->a:Lcom/twitter/android/TweetBoxFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetBoxFragment;->d(Lcom/twitter/android/TweetBoxFragment;)Lcom/twitter/android/wz;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/wq;->a:Lcom/twitter/android/TweetBoxFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetBoxFragment;->d(Lcom/twitter/android/TweetBoxFragment;)Lcom/twitter/android/wz;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/android/wz;->p_()V

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/wq;->a:Lcom/twitter/android/TweetBoxFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetBoxFragment;->d(Lcom/twitter/android/TweetBoxFragment;)Lcom/twitter/android/wz;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/wq;->a:Lcom/twitter/android/TweetBoxFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetBoxFragment;->d(Lcom/twitter/android/TweetBoxFragment;)Lcom/twitter/android/wz;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/twitter/android/wz;->a(Z)V

    :cond_2
    return-void
.end method
