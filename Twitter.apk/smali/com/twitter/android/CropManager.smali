.class public Lcom/twitter/android/CropManager;
.super Landroid/support/v4/app/Fragment;
.source "Twttr"


# instance fields
.field protected a:Lcom/twitter/android/dc;

.field private b:Lcom/twitter/android/df;

.field private c:Lcom/twitter/android/dd;

.field private d:Ljava/lang/ref/WeakReference;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method public static a(Landroid/support/v4/app/FragmentManager;)Lcom/twitter/android/CropManager;
    .locals 3

    const-string/jumbo v0, "CropManager"

    invoke-virtual {p0, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/CropManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/android/CropManager;

    invoke-direct {v0}, Lcom/twitter/android/CropManager;-><init>()V

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    const-string/jumbo v2, "CropManager"

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/FragmentTransaction;->add(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    :cond_0
    return-object v0
.end method

.method private a(Landroid/net/Uri;Landroid/graphics/Rect;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/android/CropManager;->c:Lcom/twitter/android/dd;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/CropManager;->c:Lcom/twitter/android/dd;

    invoke-virtual {v0}, Lcom/twitter/android/dd;->a()I

    move-result v0

    iput-object v2, p0, Lcom/twitter/android/CropManager;->c:Lcom/twitter/android/dd;

    :cond_0
    move v1, v0

    iget-object v0, p0, Lcom/twitter/android/CropManager;->d:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/CropManager;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/de;

    :goto_0
    if-eqz v0, :cond_1

    invoke-interface {v0, p1, p2, v1}, Lcom/twitter/android/de;->a(Landroid/net/Uri;Landroid/graphics/Rect;I)V

    :cond_1
    return-void

    :cond_2
    move-object v0, v2

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/CropManager;Landroid/net/Uri;Landroid/graphics/Rect;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/CropManager;->a(Landroid/net/Uri;Landroid/graphics/Rect;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/CropManager;Lcom/twitter/android/dc;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/CropManager;->a(Lcom/twitter/android/dc;)V

    return-void
.end method

.method private a(Lcom/twitter/android/dc;)V
    .locals 2

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/twitter/android/CropManager;->a:Lcom/twitter/android/dc;

    iput-object v0, p0, Lcom/twitter/android/CropManager;->b:Lcom/twitter/android/df;

    iget-object v1, p0, Lcom/twitter/android/CropManager;->d:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/CropManager;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/de;

    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_0

    if-eqz p1, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-interface {v1, v0}, Lcom/twitter/android/de;->a(Z)V

    :cond_0
    return-void

    :cond_1
    move-object v1, v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public a(Lcom/twitter/android/de;)V
    .locals 1

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/CropManager;->d:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method public a()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/CropManager;->a:Lcom/twitter/android/dc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/CropManager;->a:Lcom/twitter/android/dc;

    iget-object v0, v0, Lcom/twitter/android/dc;->a:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Landroid/net/Uri;IZ)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/twitter/android/CropManager;->b:Lcom/twitter/android/df;

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    new-instance v2, Lcom/twitter/android/df;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, p0, v3, p3, p4}, Lcom/twitter/android/df;-><init>(Lcom/twitter/android/CropManager;Landroid/content/Context;IZ)V

    iput-object v2, p0, Lcom/twitter/android/CropManager;->b:Lcom/twitter/android/df;

    iget-object v2, p0, Lcom/twitter/android/CropManager;->b:Lcom/twitter/android/df;

    new-array v3, v1, [Landroid/net/Uri;

    aput-object p2, v3, v0

    invoke-virtual {v2, v3}, Lcom/twitter/android/df;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move v0, v1

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Landroid/net/Uri;Landroid/graphics/Rect;I)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/twitter/android/CropManager;->c:Lcom/twitter/android/dd;

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    new-instance v2, Lcom/twitter/android/dd;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, p0, v3, p3, p4}, Lcom/twitter/android/dd;-><init>(Lcom/twitter/android/CropManager;Landroid/content/Context;Landroid/graphics/Rect;I)V

    iput-object v2, p0, Lcom/twitter/android/CropManager;->c:Lcom/twitter/android/dd;

    iget-object v2, p0, Lcom/twitter/android/CropManager;->c:Lcom/twitter/android/dd;

    new-array v3, v1, [Landroid/net/Uri;

    aput-object p2, v3, v0

    invoke-virtual {v2, v3}, Lcom/twitter/android/dd;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move v0, v1

    goto :goto_0
.end method

.method public b(Lcom/twitter/android/de;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/CropManager;->d:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/CropManager;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/CropManager;->d:Ljava/lang/ref/WeakReference;

    :cond_0
    return-void
.end method

.method public b()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/CropManager;->b:Lcom/twitter/android/df;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/CropManager;->c:Lcom/twitter/android/dd;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()F
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/CropManager;->a:Lcom/twitter/android/dc;

    iget v0, v0, Lcom/twitter/android/dc;->c:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/twitter/android/CropManager;->a:Lcom/twitter/android/dc;

    iget v1, v1, Lcom/twitter/android/dc;->b:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/CropManager;->setRetainInstance(Z)V

    return-void
.end method

.method public onDestroy()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/CropManager;->b:Lcom/twitter/android/df;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/CropManager;->b:Lcom/twitter/android/df;

    invoke-virtual {v0, v1}, Lcom/twitter/android/df;->cancel(Z)Z

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/CropManager;->c:Lcom/twitter/android/dd;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/CropManager;->c:Lcom/twitter/android/dd;

    invoke-virtual {v0, v1}, Lcom/twitter/android/dd;->cancel(Z)Z

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/CropManager;->a:Lcom/twitter/android/dc;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/CropManager;->a:Lcom/twitter/android/dc;

    iget-object v0, v0, Lcom/twitter/android/dc;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_2
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    return-void
.end method
