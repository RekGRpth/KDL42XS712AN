.class Lcom/twitter/android/rb;
.super Lcom/twitter/android/widget/ah;
.source "Twttr"


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/twitter/android/client/c;

.field private final d:Lcom/twitter/android/to;

.field private final e:Lcom/twitter/library/client/aa;

.field private f:Lcom/twitter/library/api/TwitterUser;

.field private final g:Lcom/twitter/library/scribe/ScribeAssociation;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeAssociation;Lcom/twitter/android/ye;)V
    .locals 3

    const/4 v0, 0x6

    invoke-direct {p0, p3, v0}, Lcom/twitter/android/widget/ah;-><init>(Landroid/widget/ListAdapter;I)V

    iput-object p1, p0, Lcom/twitter/android/rb;->b:Landroid/content/Context;

    invoke-static {p1}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/rb;->c:Lcom/twitter/android/client/c;

    invoke-static {p1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/rb;->e:Lcom/twitter/library/client/aa;

    new-instance v0, Lcom/twitter/android/to;

    const v1, 0x7f0f04fb    # com.twitter.android.R.string.tweets_view_all

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/to;-><init>(Ljava/lang/String;Landroid/content/Intent;)V

    iput-object v0, p0, Lcom/twitter/android/rb;->d:Lcom/twitter/android/to;

    iput-object p2, p0, Lcom/twitter/android/rb;->g:Lcom/twitter/library/scribe/ScribeAssociation;

    return-void
.end method


# virtual methods
.method protected a(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected a()Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected a(I)Ljava/lang/Object;
    .locals 10

    const/4 v9, 0x0

    new-instance v1, Lcom/twitter/library/provider/Tweet;

    iget-object v0, p0, Lcom/twitter/android/rb;->a:Landroid/widget/ListAdapter;

    check-cast v0, Lcom/twitter/android/ye;

    invoke-virtual {v0, p1}, Lcom/twitter/android/ye;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    invoke-direct {v1, v0}, Lcom/twitter/library/provider/Tweet;-><init>(Landroid/database/Cursor;)V

    iget-object v0, p0, Lcom/twitter/android/rb;->c:Lcom/twitter/android/client/c;

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v3, p0, Lcom/twitter/android/rb;->e:Lcom/twitter/library/client/aa;

    invoke-virtual {v3}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/twitter/android/rb;->g:Lcom/twitter/library/scribe/ScribeAssociation;

    const-string/jumbo v6, "tweet"

    const-string/jumbo v7, "tweet"

    const-string/jumbo v8, "click"

    invoke-static {v5, v6, v7, v8}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/rb;->g:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v2, v9, v1, v3, v9}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/rb;->g:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/android/rb;->b:Landroid/content/Context;

    const-class v3, Lcom/twitter/android/TweetActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "tw"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "association"

    iget-object v2, p0, Lcom/twitter/android/rb;->g:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/api/TwitterUser;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/rb;->f:Lcom/twitter/library/api/TwitterUser;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/rb;->f:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {v0, p1}, Lcom/twitter/library/api/TwitterUser;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/rb;->a:Landroid/widget/ListAdapter;

    check-cast v0, Lcom/twitter/android/ye;

    iget-object v1, p1, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    iget-object v2, p1, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/ye;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iput-object p1, p0, Lcom/twitter/android/rb;->f:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {p0}, Lcom/twitter/android/rb;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/rb;->a:Landroid/widget/ListAdapter;

    check-cast v0, Lcom/twitter/android/ye;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, v1}, Lcom/twitter/android/ye;->a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;Z)V

    return-void
.end method

.method protected b(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    const v0, 0x7f030136    # com.twitter.android.R.layout.section_simple_row_view

    iget-object v1, p0, Lcom/twitter/android/rb;->d:Lcom/twitter/android/to;

    iget-object v2, p0, Lcom/twitter/android/rb;->c:Lcom/twitter/android/client/c;

    invoke-virtual {v2}, Lcom/twitter/android/client/c;->V()F

    move-result v2

    invoke-static {v0, p1, p2, v1, v2}, Lcom/twitter/android/tp;->a(ILandroid/view/View;Landroid/view/ViewGroup;Lcom/twitter/android/to;F)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected b()Ljava/lang/Object;
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/rb;->f:Lcom/twitter/library/api/TwitterUser;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/rb;->b:Landroid/content/Context;

    const-class v2, Lcom/twitter/android/TimelineActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "owner_id"

    iget-object v2, p0, Lcom/twitter/android/rb;->f:Lcom/twitter/library/api/TwitterUser;

    iget-wide v2, v2, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "type"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "title"

    const v2, 0x7f0f032f    # com.twitter.android.R.string.profile_tab_title_timeline

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0
.end method

.method protected c()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/rb;->a:Landroid/widget/ListAdapter;

    check-cast v0, Lcom/twitter/android/ye;

    invoke-virtual {v0}, Lcom/twitter/android/ye;->h()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/rb;->a:Landroid/widget/ListAdapter;

    check-cast v0, Lcom/twitter/android/ye;

    invoke-virtual {v0}, Lcom/twitter/android/ye;->i()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0}, Lcom/twitter/android/widget/ah;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
