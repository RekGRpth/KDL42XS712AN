.class public Lcom/twitter/android/VerifyLoginActivity;
.super Lcom/twitter/android/client/BaseFragmentActivity;
.source "Twttr"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/view/View$OnClickListener;


# instance fields
.field a:Ljava/lang/String;

.field private b:Lcom/twitter/android/widget/ProgressDialogFragment;

.field private c:Lcom/twitter/library/network/LoginVerificationRequiredResponse;

.field private d:Landroid/os/Handler;

.field private e:Landroid/widget/Button;

.field private f:Landroid/widget/Button;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/EditText;

.field private i:I

.field private j:J

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Lcom/twitter/library/client/Session;

.field private n:Lcom/twitter/library/client/ac;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;-><init>()V

    new-instance v0, Lcom/twitter/android/aal;

    invoke-direct {v0, p0}, Lcom/twitter/android/aal;-><init>(Lcom/twitter/android/VerifyLoginActivity;)V

    iput-object v0, p0, Lcom/twitter/android/VerifyLoginActivity;->n:Lcom/twitter/library/client/ac;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/VerifyLoginActivity;)I
    .locals 1

    iget v0, p0, Lcom/twitter/android/VerifyLoginActivity;->i:I

    return v0
.end method

.method static synthetic a(Lcom/twitter/android/VerifyLoginActivity;J)J
    .locals 0

    iput-wide p1, p0, Lcom/twitter/android/VerifyLoginActivity;->j:J

    return-wide p1
.end method

.method static synthetic a(Lcom/twitter/android/VerifyLoginActivity;Ljava/lang/String;II)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/VerifyLoginActivity;->a(Ljava/lang/String;II)V

    return-void
.end method

.method private a(Ljava/lang/String;II)V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/16 v0, 0x58

    if-ne p3, v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/VerifyLoginActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/VerifyLoginActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v7, [Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "::rate_limit"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_0
    new-instance v0, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/VerifyLoginActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v1, v7, [Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "::failure"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->d(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/VerifyLoginActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/VerifyLoginActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/VerifyLoginActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/VerifyLoginActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/VerifyLoginActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/VerifyLoginActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/VerifyLoginActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/VerifyLoginActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/VerifyLoginActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/VerifyLoginActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/VerifyLoginActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private f()V
    .locals 6

    const/4 v5, 0x0

    const/4 v3, 0x1

    iget v0, p0, Lcom/twitter/android/VerifyLoginActivity;->i:I

    if-ne v0, v3, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/VerifyLoginActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/VerifyLoginActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "native_login:sms_verification::back_button:click"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/VerifyLoginActivity;->m:Lcom/twitter/library/client/Session;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/VerifyLoginActivity;->m:Lcom/twitter/library/client/Session;

    sget-object v1, Lcom/twitter/library/client/Session$LoginStatus;->a:Lcom/twitter/library/client/Session$LoginStatus;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/library/client/Session$LoginStatus;)V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/VerifyLoginActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/VerifyLoginActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "native_login:push_verification::back_button:click"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic g(Lcom/twitter/android/VerifyLoginActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/VerifyLoginActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 7

    const v0, 0x7f0f0552    # com.twitter.android.R.string.verifying_login

    invoke-static {v0}, Lcom/twitter/android/widget/ProgressDialogFragment;->a(I)Lcom/twitter/android/widget/ProgressDialogFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/VerifyLoginActivity;->b:Lcom/twitter/android/widget/ProgressDialogFragment;

    iget-object v0, p0, Lcom/twitter/android/VerifyLoginActivity;->b:Lcom/twitter/android/widget/ProgressDialogFragment;

    invoke-virtual {p0}, Lcom/twitter/android/VerifyLoginActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/ProgressDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    iget-object v0, p0, Lcom/twitter/android/VerifyLoginActivity;->h:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/twitter/android/VerifyLoginActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/VerifyLoginActivity;->m:Lcom/twitter/library/client/Session;

    iget-object v2, p0, Lcom/twitter/android/VerifyLoginActivity;->c:Lcom/twitter/library/network/LoginVerificationRequiredResponse;

    iget-wide v2, v2, Lcom/twitter/library/network/LoginVerificationRequiredResponse;->a:J

    iget-object v4, p0, Lcom/twitter/android/VerifyLoginActivity;->c:Lcom/twitter/library/network/LoginVerificationRequiredResponse;

    iget-object v4, v4, Lcom/twitter/library/network/LoginVerificationRequiredResponse;->b:Ljava/lang/String;

    iget-object v6, p0, Lcom/twitter/android/VerifyLoginActivity;->n:Lcom/twitter/library/client/ac;

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/Session;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/library/client/ac;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/VerifyLoginActivity;->a:Ljava/lang/String;

    return-void
.end method

.method static synthetic h(Lcom/twitter/android/VerifyLoginActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/VerifyLoginActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private h()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/VerifyLoginActivity;->e:Landroid/widget/Button;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/VerifyLoginActivity;->g:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/VerifyLoginActivity;->e:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/VerifyLoginActivity;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/VerifyLoginActivity;->h:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/VerifyLoginActivity;->f:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    return-void
.end method

.method static synthetic i(Lcom/twitter/android/VerifyLoginActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/VerifyLoginActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic j(Lcom/twitter/android/VerifyLoginActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/VerifyLoginActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic k(Lcom/twitter/android/VerifyLoginActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/VerifyLoginActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method private k()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/VerifyLoginActivity;->h:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic l(Lcom/twitter/android/VerifyLoginActivity;)J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/android/VerifyLoginActivity;->j:J

    return-wide v0
.end method

.method static synthetic m(Lcom/twitter/android/VerifyLoginActivity;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/VerifyLoginActivity;->d:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic n(Lcom/twitter/android/VerifyLoginActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/VerifyLoginActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic o(Lcom/twitter/android/VerifyLoginActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/VerifyLoginActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic p(Lcom/twitter/android/VerifyLoginActivity;)Lcom/twitter/android/widget/ProgressDialogFragment;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/VerifyLoginActivity;->b:Lcom/twitter/android/widget/ProgressDialogFragment;

    return-object v0
.end method

.method static synthetic q(Lcom/twitter/android/VerifyLoginActivity;)Lcom/twitter/library/client/aa;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/VerifyLoginActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v0

    return-object v0
.end method

.method static synthetic r(Lcom/twitter/android/VerifyLoginActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/VerifyLoginActivity;->m:Lcom/twitter/library/client/Session;

    return-object v0
.end method

.method static synthetic s(Lcom/twitter/android/VerifyLoginActivity;)Lcom/twitter/library/network/LoginVerificationRequiredResponse;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/VerifyLoginActivity;->c:Lcom/twitter/library/network/LoginVerificationRequiredResponse;

    return-object v0
.end method

.method static synthetic t(Lcom/twitter/android/VerifyLoginActivity;)Lcom/twitter/library/client/ac;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/VerifyLoginActivity;->n:Lcom/twitter/library/client/ac;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/VerifyLoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "login_verification_required_response"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/network/LoginVerificationRequiredResponse;

    iput-object v0, p0, Lcom/twitter/android/VerifyLoginActivity;->c:Lcom/twitter/library/network/LoginVerificationRequiredResponse;

    iget-object v0, p0, Lcom/twitter/android/VerifyLoginActivity;->c:Lcom/twitter/library/network/LoginVerificationRequiredResponse;

    iget v0, v0, Lcom/twitter/library/network/LoginVerificationRequiredResponse;->c:I

    iput v0, p0, Lcom/twitter/android/VerifyLoginActivity;->i:I

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/z;->a(Z)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/z;->c(Z)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/z;->b(Z)V

    iget v1, p0, Lcom/twitter/android/VerifyLoginActivity;->i:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    const v1, 0x7f030182    # com.twitter.android.R.layout.verify_login_push

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(I)V

    :goto_0
    return-object v0

    :cond_0
    const v1, 0x7f030183    # com.twitter.android.R.layout.verify_login_sms

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(I)V

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 6

    const/4 v5, 0x0

    const/4 v3, 0x1

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/VerifyLoginActivity;->d:Landroid/os/Handler;

    const v0, 0x7f0902cc    # com.twitter.android.R.id.login_code

    invoke-virtual {p0, v0}, Lcom/twitter/android/VerifyLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/twitter/android/VerifyLoginActivity;->h:Landroid/widget/EditText;

    const v0, 0x7f0902ce    # com.twitter.android.R.id.verify_login_send

    invoke-virtual {p0, v0}, Lcom/twitter/android/VerifyLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/twitter/android/VerifyLoginActivity;->f:Landroid/widget/Button;

    const v0, 0x7f0902cb    # com.twitter.android.R.id.backup_code_label_text

    invoke-virtual {p0, v0}, Lcom/twitter/android/VerifyLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/VerifyLoginActivity;->g:Landroid/widget/TextView;

    const v0, 0x7f0902cd    # com.twitter.android.R.id.show_backup_code_input

    invoke-virtual {p0, v0}, Lcom/twitter/android/VerifyLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/twitter/android/VerifyLoginActivity;->e:Landroid/widget/Button;

    iget-object v0, p0, Lcom/twitter/android/VerifyLoginActivity;->h:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/twitter/android/VerifyLoginActivity;->f:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/twitter/android/VerifyLoginActivity;->e:Landroid/widget/Button;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/VerifyLoginActivity;->e:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    iget v0, p0, Lcom/twitter/android/VerifyLoginActivity;->i:I

    if-ne v0, v3, :cond_1

    invoke-direct {p0}, Lcom/twitter/android/VerifyLoginActivity;->h()V

    invoke-virtual {p0}, Lcom/twitter/android/VerifyLoginActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/VerifyLoginActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "native_login:sms_verification:::impression"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :goto_0
    const v0, 0x7f0902cf    # com.twitter.android.R.id.need_help

    invoke-virtual {p0, v0}, Lcom/twitter/android/VerifyLoginActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v1

    or-int/lit8 v1, v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setPaintFlags(I)V

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-wide/16 v0, 0x7d0

    iput-wide v0, p0, Lcom/twitter/android/VerifyLoginActivity;->j:J

    invoke-virtual {p0}, Lcom/twitter/android/VerifyLoginActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "username"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/VerifyLoginActivity;->k:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/VerifyLoginActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v1

    if-eqz p1, :cond_2

    const-string/jumbo v0, "reqId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/VerifyLoginActivity;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/VerifyLoginActivity;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/VerifyLoginActivity;->n:Lcom/twitter/library/client/ac;

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/client/aa;->a(Ljava/lang/String;Lcom/twitter/library/client/ah;)V

    const-string/jumbo v0, "session_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/VerifyLoginActivity;->l:Ljava/lang/String;

    :goto_1
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/VerifyLoginActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/VerifyLoginActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "native_login:push_verification:::impression"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string/jumbo v1, "session_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/VerifyLoginActivity;->l:Ljava/lang/String;

    goto :goto_1
.end method

.method public a(Lhn;)Z
    .locals 2

    invoke-virtual {p1}, Lhn;->a()I

    move-result v0

    const v1, 0x7f090045    # com.twitter.android.R.id.home

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/VerifyLoginActivity;->f()V

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lhn;)Z

    move-result v0

    return v0
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/VerifyLoginActivity;->f:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/twitter/android/VerifyLoginActivity;->k()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public onBackPressed()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/VerifyLoginActivity;->f()V

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onBackPressed()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    const/4 v5, 0x0

    const/4 v3, 0x1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0902ce    # com.twitter.android.R.id.verify_login_send

    if-ne v0, v1, :cond_2

    invoke-direct {p0}, Lcom/twitter/android/VerifyLoginActivity;->g()V

    iget v0, p0, Lcom/twitter/android/VerifyLoginActivity;->i:I

    if-ne v0, v3, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/VerifyLoginActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/VerifyLoginActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "native_login:sms_verification:login_code::send"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/VerifyLoginActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/VerifyLoginActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "native_login:push_verification:backup_code::send"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const v1, 0x7f0902cd    # com.twitter.android.R.id.show_backup_code_input

    if-ne v0, v1, :cond_3

    invoke-direct {p0}, Lcom/twitter/android/VerifyLoginActivity;->h()V

    invoke-virtual {p0}, Lcom/twitter/android/VerifyLoginActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/VerifyLoginActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "native_login:push_verification::enter_code_button:click"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const v1, 0x7f0902cf    # com.twitter.android.R.id.need_help

    if-ne v0, v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.intent.action.VIEW"

    const v2, 0x7f0f0255    # com.twitter.android.R.string.login_verification_troubleshooting_url

    invoke-virtual {p0, v2}, Lcom/twitter/android/VerifyLoginActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/VerifyLoginActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onDestroy()V

    invoke-virtual {p0}, Lcom/twitter/android/VerifyLoginActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/VerifyLoginActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/aa;->e(Ljava/lang/String;)V

    return-void
.end method

.method public onResume()V
    .locals 4

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onResume()V

    invoke-virtual {p0}, Lcom/twitter/android/VerifyLoginActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/VerifyLoginActivity;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/aa;->c(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/VerifyLoginActivity;->m:Lcom/twitter/library/client/Session;

    iget-object v1, p0, Lcom/twitter/android/VerifyLoginActivity;->m:Lcom/twitter/library/client/Session;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/VerifyLoginActivity;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/aa;->a(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/VerifyLoginActivity;->m:Lcom/twitter/library/client/Session;

    iget-object v0, p0, Lcom/twitter/android/VerifyLoginActivity;->m:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/VerifyLoginActivity;->l:Ljava/lang/String;

    :cond_0
    const-wide/16 v0, 0x7d0

    iput-wide v0, p0, Lcom/twitter/android/VerifyLoginActivity;->j:J

    iget v0, p0, Lcom/twitter/android/VerifyLoginActivity;->i:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/VerifyLoginActivity;->d:Landroid/os/Handler;

    new-instance v1, Lcom/twitter/android/aak;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/twitter/android/aak;-><init>(Lcom/twitter/android/VerifyLoginActivity;Lcom/twitter/android/aaj;)V

    iget-wide v2, p0, Lcom/twitter/android/VerifyLoginActivity;->j:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_1
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "reqId"

    iget-object v1, p0, Lcom/twitter/android/VerifyLoginActivity;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "session_id"

    iget-object v1, p0, Lcom/twitter/android/VerifyLoginActivity;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method
