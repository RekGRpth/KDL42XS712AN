.class public Lcom/twitter/android/SignedOutFragment;
.super Lcom/twitter/android/TimelineFragment;
.source "Twttr"


# static fields
.field public static a:Ljava/lang/String;

.field public static b:Ljava/lang/String;

.field public static c:Ljava/lang/String;


# instance fields
.field private g:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string/jumbo v0, "loggedOutActionType"

    sput-object v0, Lcom/twitter/android/SignedOutFragment;->a:Ljava/lang/String;

    const-string/jumbo v0, "profile_photo"

    sput-object v0, Lcom/twitter/android/SignedOutFragment;->b:Ljava/lang/String;

    const-string/jumbo v0, "banner"

    sput-object v0, Lcom/twitter/android/SignedOutFragment;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/twitter/android/TimelineFragment;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/twitter/android/SignedOutFragment;->g:J

    return-void
.end method


# virtual methods
.method public a(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 0

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    sget-object v0, Lcom/twitter/android/tm;->a:Lcom/twitter/android/widget/SignedOutDialogFragment;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/DialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    const v1, 0x7f0901bc    # com.twitter.android.R.id.logged_out_user_info

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method protected a_(I)V
    .locals 2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    const/4 v0, 0x5

    if-ne p1, v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/SignedOutFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    check-cast v0, Lcom/twitter/refresh/widget/RefreshableListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/refresh/widget/RefreshableListView;->a(Z)V

    :goto_0
    return-void

    :cond_1
    invoke-super {p0, p1}, Lcom/twitter/android/TimelineFragment;->a_(I)V

    goto :goto_0
.end method

.method protected a_(Z)V
    .locals 0

    return-void
.end method

.method protected b(I)V
    .locals 1

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    const/4 v0, 0x5

    if-ne p1, v0, :cond_2

    :cond_0
    const/4 v0, 0x0

    invoke-super {p0, v0}, Lcom/twitter/android/TimelineFragment;->a_(Z)V

    iget-object v0, p0, Lcom/twitter/android/SignedOutFragment;->T:Lcom/twitter/android/client/ag;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/SignedOutFragment;->T:Lcom/twitter/android/client/ag;

    invoke-interface {v0}, Lcom/twitter/android/client/ag;->d()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-super {p0, p1}, Lcom/twitter/android/TimelineFragment;->b(I)V

    goto :goto_0
.end method

.method protected o()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f09027a    # com.twitter.android.R.id.sign_in

    if-ne v0, v1, :cond_1

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/SignedOutFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/LoginActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/SignedOutFragment;->startActivity(Landroid/content/Intent;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v1, 0x7f090286    # com.twitter.android.R.id.signup

    if-ne v0, v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/SignedOutFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/SignUpActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/SignedOutFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    const/4 v0, 0x0

    invoke-super {p0, p1}, Lcom/twitter/android/TimelineFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/SignedOutFragment;->l(Z)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/SignedOutFragment;->k(Z)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/SignedOutFragment;->setRetainInstance(Z)V

    return-void
.end method

.method public onResume()V
    .locals 6

    invoke-super {p0}, Lcom/twitter/android/TimelineFragment;->onResume()V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/twitter/android/SignedOutFragment;->g:J

    const-wide/32 v4, 0xea60

    add-long/2addr v2, v4

    cmp-long v2, v2, v0

    if-gez v2, :cond_0

    const/4 v2, 0x4

    invoke-virtual {p0, v2}, Lcom/twitter/android/SignedOutFragment;->c(I)Z

    iput-wide v0, p0, Lcom/twitter/android/SignedOutFragment;->g:J

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/SignedOutFragment;->P()Z

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1, p2}, Lcom/twitter/android/TimelineFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/SignedOutFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030146    # com.twitter.android.R.layout.start_signed_out

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/SignedOutFragment;->V()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    const v1, 0x7f09027a    # com.twitter.android.R.id.sign_in

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f090286    # com.twitter.android.R.id.signup

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v0, 0x0

    invoke-super {p0, v0}, Lcom/twitter/android/TimelineFragment;->a_(Z)V

    return-void
.end method

.method protected q()Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/SignedOutFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/SignedOutFragment;->f:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/SignedOutFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->getCount()I

    move-result v0

    const/16 v1, 0x14

    if-ge v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected y()Lcom/twitter/android/yb;
    .locals 2

    new-instance v0, Lcom/twitter/android/tm;

    iget-object v1, p0, Lcom/twitter/android/SignedOutFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/tm;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/library/scribe/ScribeAssociation;)V

    return-object v0
.end method

.method protected z()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
