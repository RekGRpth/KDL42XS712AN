.class public Lcom/twitter/android/MessagesTimelineActivity;
.super Lcom/twitter/android/ListFragmentActivity;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/ListFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Intent;Lcom/twitter/library/client/e;)Lcom/twitter/android/iu;
    .locals 2

    new-instance v0, Lcom/twitter/android/iu;

    new-instance v1, Lcom/twitter/android/MessagesFragment;

    invoke-direct {v1}, Lcom/twitter/android/MessagesFragment;-><init>()V

    invoke-direct {v0, v1}, Lcom/twitter/android/iu;-><init>(Lcom/twitter/android/client/BaseListFragment;)V

    return-object v0
.end method

.method protected a(Landroid/content/Intent;)Ljava/lang/CharSequence;
    .locals 1

    const v0, 0x7f0f01d0    # com.twitter.android.R.string.home_direct_messages

    invoke-virtual {p0, v0}, Lcom/twitter/android/MessagesTimelineActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
