.class public Lcom/twitter/android/UsersActivity;
.super Lcom/twitter/android/client/BaseFragmentActivity;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 1

    new-instance v0, Lcom/twitter/android/zz;

    invoke-direct {v0, p0}, Lcom/twitter/android/zz;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 5

    invoke-virtual {p0}, Lcom/twitter/android/UsersActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    check-cast p2, Lcom/twitter/android/zz;

    if-nez p1, :cond_1

    iget-boolean v2, p2, Lcom/twitter/android/zz;->a:Z

    invoke-static {v0, v2}, Lcom/twitter/android/UsersFragment;->a(Landroid/content/Intent;Z)Landroid/os/Bundle;

    move-result-object v2

    const-string/jumbo v3, "follow"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string/jumbo v3, "follow"

    const-string/jumbo v4, "com.twitter.android.intent.action.FOLLOW"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_0
    new-instance v1, Lcom/twitter/android/UsersFragment;

    invoke-direct {v1}, Lcom/twitter/android/UsersFragment;-><init>()V

    invoke-virtual {v1, v2}, Lcom/twitter/android/UsersFragment;->setArguments(Landroid/os/Bundle;)V

    iget v3, p2, Lcom/twitter/android/zz;->b:I

    packed-switch v3, :pswitch_data_0

    :goto_0
    :pswitch_0
    invoke-virtual {p0}, Lcom/twitter/android/UsersActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    const v3, 0x7f0900e3    # com.twitter.android.R.id.fragment_container

    invoke-virtual {v2, v3, v1}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    :cond_1
    iget v1, p2, Lcom/twitter/android/zz;->b:I

    packed-switch v1, :pswitch_data_1

    :pswitch_1
    const v0, 0x7f0f0538    # com.twitter.android.R.string.users_pick_friend_title

    invoke-virtual {p0, v0}, Lcom/twitter/android/UsersActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/UsersActivity;->setTitle(Ljava/lang/CharSequence;)V

    :goto_1
    return-void

    :pswitch_2
    const-string/jumbo v3, "empty_desc"

    const v4, 0x7f0f0159    # com.twitter.android.R.string.empty_wtf

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    :pswitch_3
    const-string/jumbo v3, "empty_desc"

    const v4, 0x7f0f014b    # com.twitter.android.R.string.empty_find_friends

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    :pswitch_4
    const-string/jumbo v3, "empty_desc"

    const v4, 0x7f0f014d    # com.twitter.android.R.string.empty_incoming_friendships

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0

    :pswitch_5
    const v0, 0x7f0f0326    # com.twitter.android.R.string.profile_friends

    invoke-virtual {p0, v0}, Lcom/twitter/android/UsersActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/UsersActivity;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_1

    :pswitch_6
    const v0, 0x7f0f0325    # com.twitter.android.R.string.profile_followers

    invoke-virtual {p0, v0}, Lcom/twitter/android/UsersActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/UsersActivity;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_1

    :pswitch_7
    const-string/jumbo v1, "category_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/UsersActivity;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_1

    :pswitch_8
    const v0, 0x7f0f00d0    # com.twitter.android.R.string.contacts_title

    invoke-virtual {p0, v0}, Lcom/twitter/android/UsersActivity;->setTitle(I)V

    goto :goto_1

    :pswitch_9
    const v0, 0x7f0f0560    # com.twitter.android.R.string.who_to_follow_title

    invoke-virtual {p0, v0}, Lcom/twitter/android/UsersActivity;->setTitle(I)V

    goto :goto_1

    :pswitch_a
    const v0, 0x7f0f018a    # com.twitter.android.R.string.favoriters_title

    invoke-virtual {p0, v0}, Lcom/twitter/android/UsersActivity;->setTitle(I)V

    goto :goto_1

    :pswitch_b
    const v0, 0x7f0f0384    # com.twitter.android.R.string.retweeters_title

    invoke-virtual {p0, v0}, Lcom/twitter/android/UsersActivity;->setTitle(I)V

    goto :goto_1

    :pswitch_c
    const v0, 0x7f0f01a3    # com.twitter.android.R.string.follow_requests_title

    invoke-virtual {p0, v0}, Lcom/twitter/android/UsersActivity;->setTitle(I)V

    goto :goto_1

    :pswitch_d
    const v1, 0x7f0f044d    # com.twitter.android.R.string.similar_to_title

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string/jumbo v4, "username"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/twitter/android/UsersActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/UsersActivity;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_7
        :pswitch_8
        :pswitch_1
        :pswitch_9
        :pswitch_d
        :pswitch_a
        :pswitch_b
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_c
        :pswitch_9
        :pswitch_1
        :pswitch_9
        :pswitch_9
        :pswitch_1
        :pswitch_7
        :pswitch_1
        :pswitch_1
        :pswitch_9
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 4

    invoke-virtual {p0}, Lcom/twitter/android/UsersActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f0900e3    # com.twitter.android.R.id.fragment_container

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/UsersFragment;

    invoke-virtual {v0}, Lcom/twitter/android/UsersFragment;->q()Lcom/twitter/library/util/FriendshipCache;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/util/FriendshipCache;->a()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, -0x1

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v3, "friendship_cache"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/UsersActivity;->setResult(ILandroid/content/Intent;)V

    :cond_0
    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onBackPressed()V

    return-void
.end method
