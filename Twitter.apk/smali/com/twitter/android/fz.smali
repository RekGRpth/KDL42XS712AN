.class Lcom/twitter/android/fz;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/twitter/android/FilterActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/FilterActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/fz;->a:Lcom/twitter/android/FilterActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/fz;->a:Lcom/twitter/android/FilterActivity;

    invoke-static {v0}, Lcom/twitter/android/FilterActivity;->a(Lcom/twitter/android/FilterActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/fz;->a:Lcom/twitter/android/FilterActivity;

    invoke-static {v0}, Lcom/twitter/android/FilterActivity;->c(Lcom/twitter/android/FilterActivity;)Landroid/view/ViewGroup;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/fz;->a:Lcom/twitter/android/FilterActivity;

    invoke-static {v1}, Lcom/twitter/android/FilterActivity;->b(Lcom/twitter/android/FilterActivity;)Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/fz;->a:Lcom/twitter/android/FilterActivity;

    invoke-static {v0}, Lcom/twitter/android/FilterActivity;->b(Lcom/twitter/android/FilterActivity;)Landroid/view/ViewGroup;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/fz;->a:Lcom/twitter/android/FilterActivity;

    invoke-static {v1}, Lcom/twitter/android/FilterActivity;->f(Lcom/twitter/android/FilterActivity;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/twitter/android/fz;->a:Lcom/twitter/android/FilterActivity;

    invoke-static {v0}, Lcom/twitter/android/FilterActivity;->b(Lcom/twitter/android/FilterActivity;)Landroid/view/ViewGroup;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/fz;->a:Lcom/twitter/android/FilterActivity;

    invoke-static {v1}, Lcom/twitter/android/FilterActivity;->g(Lcom/twitter/android/FilterActivity;)Landroid/widget/ImageView;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/twitter/android/fz;->a:Lcom/twitter/android/FilterActivity;

    invoke-static {v0, v2}, Lcom/twitter/android/FilterActivity;->a(Lcom/twitter/android/FilterActivity;Z)Z

    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/fz;->a:Lcom/twitter/android/FilterActivity;

    invoke-static {v0}, Lcom/twitter/android/FilterActivity;->d(Lcom/twitter/android/FilterActivity;)Lcom/twitter/android/widget/PipView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/PipView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/fz;->a:Lcom/twitter/android/FilterActivity;

    invoke-static {v0}, Lcom/twitter/android/FilterActivity;->c(Lcom/twitter/android/FilterActivity;)Landroid/view/ViewGroup;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/fz;->a:Lcom/twitter/android/FilterActivity;

    invoke-static {v1}, Lcom/twitter/android/FilterActivity;->e(Lcom/twitter/android/FilterActivity;)Landroid/widget/GridView;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    goto :goto_0
.end method
