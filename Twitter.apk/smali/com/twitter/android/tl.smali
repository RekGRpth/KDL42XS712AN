.class Lcom/twitter/android/tl;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/client/ak;


# instance fields
.field final synthetic a:Lcom/twitter/android/SignUpActivity;


# direct methods
.method public constructor <init>(Lcom/twitter/android/SignUpActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;IILcom/twitter/library/api/t;)V
    .locals 10

    const/4 v0, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    iget-object v1, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    invoke-static {v1}, Lcom/twitter/android/SignUpActivity;->d(Lcom/twitter/android/SignUpActivity;)Lcom/twitter/android/client/c;

    move-result-object v4

    iget-object v1, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    invoke-virtual {v1, v9}, Lcom/twitter/android/SignUpActivity;->dismissDialog(I)V

    const/4 v1, 0x2

    if-ne p2, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    const-class v3, Lcom/twitter/android/LoginActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "screen_name"

    iget-object v3, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    iget-object v3, v3, Lcom/twitter/android/SignUpActivity;->c:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v3}, Lcom/twitter/internal/android/widget/PopupEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "password"

    iget-object v3, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    iget-object v3, v3, Lcom/twitter/android/SignUpActivity;->d:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/SignUpActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    invoke-virtual {v0}, Lcom/twitter/android/SignUpActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    if-eqz p4, :cond_5

    iget-object v1, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    iget-object v5, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    iget-object v5, v5, Lcom/twitter/android/SignUpActivity;->a:Landroid/widget/EditText;

    iget-object v6, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    iget-object v6, v6, Lcom/twitter/android/SignUpActivity;->g:Landroid/widget/TextView;

    iget-object v7, p4, Lcom/twitter/library/api/t;->a:Ljava/lang/String;

    invoke-virtual {v1, v5, v6, v7}, Lcom/twitter/android/SignUpActivity;->a(Landroid/widget/EditText;Landroid/widget/TextView;Ljava/lang/String;)V

    iget-object v1, p4, Lcom/twitter/library/api/t;->a:Ljava/lang/String;

    if-eqz v1, :cond_1

    new-array v0, v9, [Ljava/lang/String;

    const-string/jumbo v1, "signup:form:fullname::failure"

    aput-object v1, v0, v8

    invoke-virtual {v4, v2, v3, v0}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    iget-object v1, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    iget-object v1, v1, Lcom/twitter/android/SignUpActivity;->a:Landroid/widget/EditText;

    invoke-virtual {v0, v1, v8}, Lcom/twitter/android/SignUpActivity;->a(Landroid/widget/EditText;Z)V

    iget-object v0, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    iget-object v0, v0, Lcom/twitter/android/SignUpActivity;->a:Landroid/widget/EditText;

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    iget-object v5, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    iget-object v5, v5, Lcom/twitter/android/SignUpActivity;->b:Landroid/widget/EditText;

    iget-object v6, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    iget-object v6, v6, Lcom/twitter/android/SignUpActivity;->h:Landroid/widget/TextView;

    iget-object v7, p4, Lcom/twitter/library/api/t;->b:Ljava/lang/String;

    invoke-virtual {v1, v5, v6, v7}, Lcom/twitter/android/SignUpActivity;->a(Landroid/widget/EditText;Landroid/widget/TextView;Ljava/lang/String;)V

    iget-object v1, p4, Lcom/twitter/library/api/t;->b:Ljava/lang/String;

    if-eqz v1, :cond_2

    new-array v1, v9, [Ljava/lang/String;

    const-string/jumbo v5, "signup:form:email::failure"

    aput-object v5, v1, v8

    invoke-virtual {v4, v2, v3, v1}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v1, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    iget-object v5, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    iget-object v5, v5, Lcom/twitter/android/SignUpActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v1, v5, v8}, Lcom/twitter/android/SignUpActivity;->a(Landroid/widget/EditText;Z)V

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    iget-object v0, v0, Lcom/twitter/android/SignUpActivity;->b:Landroid/widget/EditText;

    :cond_2
    iget-object v1, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    iget-object v5, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    iget-object v5, v5, Lcom/twitter/android/SignUpActivity;->c:Lcom/twitter/internal/android/widget/PopupEditText;

    iget-object v6, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    iget-object v6, v6, Lcom/twitter/android/SignUpActivity;->i:Landroid/widget/TextView;

    iget-object v7, p4, Lcom/twitter/library/api/t;->c:Ljava/lang/String;

    invoke-virtual {v1, v5, v6, v7}, Lcom/twitter/android/SignUpActivity;->a(Landroid/widget/EditText;Landroid/widget/TextView;Ljava/lang/String;)V

    iget-object v1, p4, Lcom/twitter/library/api/t;->c:Ljava/lang/String;

    if-eqz v1, :cond_3

    new-array v1, v9, [Ljava/lang/String;

    const-string/jumbo v5, "signup:form:screen_name::failure"

    aput-object v5, v1, v8

    invoke-virtual {v4, v2, v3, v1}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v1, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    iget-object v5, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    iget-object v5, v5, Lcom/twitter/android/SignUpActivity;->c:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v1, v5, v8}, Lcom/twitter/android/SignUpActivity;->a(Landroid/widget/EditText;Z)V

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    iget-object v0, v0, Lcom/twitter/android/SignUpActivity;->c:Lcom/twitter/internal/android/widget/PopupEditText;

    :cond_3
    iget-object v1, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    iget-object v5, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    iget-object v5, v5, Lcom/twitter/android/SignUpActivity;->d:Landroid/widget/EditText;

    iget-object v6, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    iget-object v6, v6, Lcom/twitter/android/SignUpActivity;->j:Landroid/widget/TextView;

    iget-object v7, p4, Lcom/twitter/library/api/t;->d:Ljava/lang/String;

    invoke-virtual {v1, v5, v6, v7}, Lcom/twitter/android/SignUpActivity;->a(Landroid/widget/EditText;Landroid/widget/TextView;Ljava/lang/String;)V

    iget-object v1, p4, Lcom/twitter/library/api/t;->d:Ljava/lang/String;

    if-eqz v1, :cond_6

    new-array v1, v9, [Ljava/lang/String;

    const-string/jumbo v5, "signup:form:password::failure"

    aput-object v5, v1, v8

    invoke-virtual {v4, v2, v3, v1}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v1, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    iget-object v5, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    iget-object v5, v5, Lcom/twitter/android/SignUpActivity;->d:Landroid/widget/EditText;

    invoke-virtual {v1, v5, v8}, Lcom/twitter/android/SignUpActivity;->a(Landroid/widget/EditText;Z)V

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    iget-object v0, v0, Lcom/twitter/android/SignUpActivity;->d:Landroid/widget/EditText;

    move-object v1, v0

    :goto_1
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    iget-object v0, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    const v2, 0x7f090139    # com.twitter.android.R.id.scroll_view

    invoke-virtual {v0, v2}, Lcom/twitter/android/SignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    invoke-virtual {v1}, Landroid/widget/EditText;->getTop()I

    move-result v1

    invoke-virtual {v0, v8, v1}, Landroid/widget/ScrollView;->smoothScrollTo(II)V

    goto/16 :goto_0

    :cond_4
    new-array v0, v9, [Ljava/lang/String;

    const-string/jumbo v1, "signup:form:::failure"

    aput-object v1, v0, v8

    invoke-virtual {v4, v2, v3, v0}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    invoke-virtual {v0}, Lcom/twitter/android/SignUpActivity;->g()V

    goto/16 :goto_0

    :cond_5
    new-array v1, v9, [Ljava/lang/String;

    const-string/jumbo v5, "signup:form:::failure"

    aput-object v5, v1, v8

    invoke-virtual {v4, v2, v3, v1}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v1, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    invoke-virtual {v1, v9}, Lcom/twitter/android/SignUpActivity;->dismissDialog(I)V

    iget-object v1, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    iput-object v0, v1, Lcom/twitter/android/SignUpActivity;->r:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    invoke-virtual {v0}, Lcom/twitter/android/SignUpActivity;->g()V

    goto/16 :goto_0

    :cond_6
    move-object v1, v0

    goto :goto_1
.end method

.method public a(Lcom/twitter/library/client/Session;Lcom/twitter/library/api/t;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    invoke-static {v0}, Lcom/twitter/android/SignUpActivity;->e(Lcom/twitter/android/SignUpActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "signup:form:captcha::show"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    iget-object v1, p2, Lcom/twitter/library/api/t;->e:Ljava/lang/String;

    iput-object v1, v0, Lcom/twitter/android/SignUpActivity;->r:Ljava/lang/String;

    new-instance v0, Lcom/twitter/android/tk;

    iget-object v1, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    invoke-direct {v0, v1}, Lcom/twitter/android/tk;-><init>(Lcom/twitter/android/SignUpActivity;)V

    new-array v1, v6, [Ljava/lang/String;

    iget-object v2, p2, Lcom/twitter/library/api/t;->f:Ljava/lang/String;

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, Lcom/twitter/android/tk;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;)V
    .locals 7

    const/4 v6, 0x1

    iget-object v0, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    invoke-static {v0}, Lcom/twitter/android/SignUpActivity;->a(Lcom/twitter/android/SignUpActivity;)Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v2, v0, v1}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v3, v6, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "signup:form:::success"

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    invoke-static {v3}, Lcom/twitter/android/SignUpActivity;->b(Lcom/twitter/android/SignUpActivity;)Lcom/twitter/android/client/c;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    iget-boolean v4, v4, Lcom/twitter/android/SignUpActivity;->u:Z

    if-eqz v4, :cond_0

    const-string/jumbo v4, "sso_sdk"

    invoke-virtual {v2, v4}, Lcom/twitter/library/scribe/ScribeLog;->h(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    :cond_0
    iget-object v4, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    invoke-virtual {v4}, Lcom/twitter/android/SignUpActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/twitter/android/util/q;->a(Landroid/content/Context;)Lcom/twitter/android/util/r;

    move-result-object v4

    invoke-interface {v4}, Lcom/twitter/android/util/r;->a()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Lcom/twitter/android/util/r;->c()V

    :cond_1
    invoke-virtual {v3, v2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    const-string/jumbo v2, "signup:form:::success"

    invoke-virtual {v3, v0, v1, v2}, Lcom/twitter/android/client/c;->c(JLjava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    invoke-virtual {v0, v6}, Lcom/twitter/android/SignUpActivity;->dismissDialog(I)V

    invoke-virtual {v3}, Lcom/twitter/android/client/c;->p()V

    iget-object v0, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    invoke-static {v0}, Lcom/twitter/android/SignUpActivity;->c(Lcom/twitter/android/SignUpActivity;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/twitter/android/SignUpActivity;->showDialog(I)V

    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    const/4 v1, -0x1

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v3, "sb_account_name"

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/SignUpActivity;->setResult(ILandroid/content/Intent;)V

    iget-object v0, p0, Lcom/twitter/android/tl;->a:Lcom/twitter/android/SignUpActivity;

    invoke-virtual {v0}, Lcom/twitter/android/SignUpActivity;->d()V

    goto :goto_0
.end method
