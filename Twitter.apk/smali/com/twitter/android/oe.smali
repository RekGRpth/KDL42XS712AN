.class Lcom/twitter/android/oe;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/client/ag;


# instance fields
.field final synthetic a:Lcom/twitter/android/OneFactorLoginActivity;


# direct methods
.method public constructor <init>(Lcom/twitter/android/OneFactorLoginActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/oe;->a:Lcom/twitter/android/OneFactorLoginActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;Lcom/twitter/library/service/e;Lcom/twitter/library/network/OneFactorLoginResponse;[I)V
    .locals 6

    const/4 v4, 0x1

    const/4 v1, 0x0

    if-eqz p4, :cond_0

    array-length v0, p4

    if-nez v0, :cond_2

    :cond_0
    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/twitter/android/oe;->a:Lcom/twitter/android/OneFactorLoginActivity;

    invoke-static {v2}, Lcom/twitter/android/OneFactorLoginActivity;->c(Lcom/twitter/android/OneFactorLoginActivity;)Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    move-result-object v2

    sget-object v3, Lcom/twitter/android/OneFactorLoginActivity$LoginState;->a:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    if-ne v2, v3, :cond_1

    invoke-virtual {p2}, Lcom/twitter/library/service/e;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v0, p0, Lcom/twitter/android/oe;->a:Lcom/twitter/android/OneFactorLoginActivity;

    invoke-static {v0}, Lcom/twitter/android/OneFactorLoginActivity;->k(Lcom/twitter/android/OneFactorLoginActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    new-array v4, v4, [Ljava/lang/String;

    const-string/jumbo v5, "login::1fa::submit"

    aput-object v5, v4, v1

    invoke-virtual {v0, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/oe;->a:Lcom/twitter/android/OneFactorLoginActivity;

    invoke-static {v0, p3}, Lcom/twitter/android/OneFactorLoginActivity;->a(Lcom/twitter/android/OneFactorLoginActivity;Lcom/twitter/library/network/OneFactorLoginResponse;)Lcom/twitter/library/network/OneFactorLoginResponse;

    sget-object v0, Lcom/twitter/android/OneFactorLoginActivity$LoginState;->b:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    iget-object v1, p0, Lcom/twitter/android/oe;->a:Lcom/twitter/android/OneFactorLoginActivity;

    invoke-static {v1, p1}, Lcom/twitter/android/OneFactorLoginActivity;->a(Lcom/twitter/android/OneFactorLoginActivity;Lcom/twitter/library/client/Session;)Lcom/twitter/library/client/Session;

    :goto_1
    iget-object v1, p0, Lcom/twitter/android/oe;->a:Lcom/twitter/android/OneFactorLoginActivity;

    invoke-static {v1, v0}, Lcom/twitter/android/OneFactorLoginActivity;->a(Lcom/twitter/android/OneFactorLoginActivity;Lcom/twitter/android/OneFactorLoginActivity$LoginState;)V

    :cond_1
    return-void

    :cond_2
    aget v0, p4, v1

    goto :goto_0

    :cond_3
    const/16 v2, 0x10d

    if-ne v0, v2, :cond_4

    iget-object v0, p0, Lcom/twitter/android/oe;->a:Lcom/twitter/android/OneFactorLoginActivity;

    invoke-static {v0}, Lcom/twitter/android/OneFactorLoginActivity;->l(Lcom/twitter/android/OneFactorLoginActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    new-array v4, v4, [Ljava/lang/String;

    const-string/jumbo v5, "login::1fa::ineligible"

    aput-object v5, v4, v1

    invoke-virtual {v0, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    sget-object v0, Lcom/twitter/android/OneFactorLoginActivity$LoginState;->f:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    goto :goto_1

    :cond_4
    const/16 v1, 0x20

    if-ne v0, v1, :cond_5

    sget-object v0, Lcom/twitter/android/OneFactorLoginActivity$LoginState;->g:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    goto :goto_1

    :cond_5
    sget-object v0, Lcom/twitter/android/OneFactorLoginActivity$LoginState;->i:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    goto :goto_1
.end method

.method public a(Lcom/twitter/library/service/e;Lcom/twitter/library/client/Session;[ILjava/lang/String;)V
    .locals 6

    const/4 v4, 0x1

    const/4 v1, 0x0

    if-eqz p3, :cond_0

    array-length v0, p3

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    invoke-virtual {p1}, Lcom/twitter/library/service/e;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v0, p0, Lcom/twitter/android/oe;->a:Lcom/twitter/android/OneFactorLoginActivity;

    invoke-static {v0}, Lcom/twitter/android/OneFactorLoginActivity;->m(Lcom/twitter/android/OneFactorLoginActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    new-array v4, v4, [Ljava/lang/String;

    const-string/jumbo v5, "login::1fa::success"

    aput-object v5, v4, v1

    invoke-virtual {v0, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v1, "session"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string/jumbo v1, "user"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/oe;->a:Lcom/twitter/android/OneFactorLoginActivity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/twitter/android/OneFactorLoginActivity;->setResult(ILandroid/content/Intent;)V

    iget-object v0, p0, Lcom/twitter/android/oe;->a:Lcom/twitter/android/OneFactorLoginActivity;

    invoke-virtual {v0}, Lcom/twitter/android/OneFactorLoginActivity;->finish()V

    :goto_1
    return-void

    :cond_1
    aget v0, p3, v1

    goto :goto_0

    :cond_2
    const/16 v2, 0xec

    if-ne v0, v2, :cond_3

    iget-object v0, p0, Lcom/twitter/android/oe;->a:Lcom/twitter/android/OneFactorLoginActivity;

    invoke-static {v0}, Lcom/twitter/android/OneFactorLoginActivity;->n(Lcom/twitter/android/OneFactorLoginActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    new-array v4, v4, [Ljava/lang/String;

    const-string/jumbo v5, "login::1fa::failure"

    aput-object v5, v4, v1

    invoke-virtual {v0, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/oe;->a:Lcom/twitter/android/OneFactorLoginActivity;

    sget-object v1, Lcom/twitter/android/OneFactorLoginActivity$LoginState;->h:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    invoke-static {v0, v1}, Lcom/twitter/android/OneFactorLoginActivity;->a(Lcom/twitter/android/OneFactorLoginActivity;Lcom/twitter/android/OneFactorLoginActivity$LoginState;)V

    :goto_2
    iget-object v0, p0, Lcom/twitter/android/oe;->a:Lcom/twitter/android/OneFactorLoginActivity;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/twitter/android/OneFactorLoginActivity;->setResult(I)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/oe;->a:Lcom/twitter/android/OneFactorLoginActivity;

    invoke-static {v0}, Lcom/twitter/android/OneFactorLoginActivity;->o(Lcom/twitter/android/OneFactorLoginActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    new-array v4, v4, [Ljava/lang/String;

    const-string/jumbo v5, "login::1fa::error"

    aput-object v5, v4, v1

    invoke-virtual {v0, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/oe;->a:Lcom/twitter/android/OneFactorLoginActivity;

    sget-object v1, Lcom/twitter/android/OneFactorLoginActivity$LoginState;->i:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    invoke-static {v0, v1}, Lcom/twitter/android/OneFactorLoginActivity;->a(Lcom/twitter/android/OneFactorLoginActivity;Lcom/twitter/android/OneFactorLoginActivity$LoginState;)V

    goto :goto_2
.end method

.method public a(Ljava/lang/String;Ljava/lang/Boolean;[I)V
    .locals 0

    return-void
.end method
