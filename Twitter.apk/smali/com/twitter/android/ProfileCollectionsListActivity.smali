.class public Lcom/twitter/android/ProfileCollectionsListActivity;
.super Lcom/twitter/android/client/BaseFragmentActivity;
.source "Twttr"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# instance fields
.field private a:I

.field private b:J

.field private c:Ljava/lang/String;

.field private d:Lcom/twitter/android/gs;

.field private e:Landroid/widget/RadioGroup;

.field private f:Landroid/widget/RadioButton;

.field private g:Landroid/widget/RadioButton;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;-><init>()V

    return-void
.end method

.method private a([Lcom/twitter/android/gr;Landroid/content/Intent;IILjava/lang/String;)V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v1, "shim_size"

    iget v2, p0, Lcom/twitter/android/ProfileCollectionsListActivity;->a:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v1, "list_type"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    new-instance v1, Lcom/twitter/android/gr;

    const-class v2, Lcom/twitter/android/CollectionsListFragment;

    invoke-direct {v1, v0, v2, p5}, Lcom/twitter/android/gr;-><init>(Landroid/os/Bundle;Ljava/lang/Class;Ljava/lang/String;)V

    aput-object v1, p1, p4

    aget-object v0, p1, p4

    invoke-virtual {v0, p0}, Lcom/twitter/android/gr;->a(Landroid/support/v4/app/FragmentActivity;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/CollectionsListFragment;

    invoke-virtual {v0, p2}, Lcom/twitter/android/CollectionsListFragment;->a(Landroid/content/Intent;)Lcom/twitter/android/client/BaseListFragment;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/BaseListFragment;->j(Z)Lcom/twitter/android/client/BaseListFragment;

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 2

    const/4 v1, 0x0

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->a(I)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->a(Z)V

    const v1, 0x7f030033    # com.twitter.android.R.layout.collections_list_activity

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(I)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(Z)V

    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 12

    const/4 v11, 0x0

    const/4 v10, 0x2

    const/4 v7, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/ProfileCollectionsListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const v0, 0x7f090251    # com.twitter.android.R.id.scope_option

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileCollectionsListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/twitter/android/ProfileCollectionsListActivity;->e:Landroid/widget/RadioGroup;

    const v0, 0x7f090252    # com.twitter.android.R.id.scope_option_1

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileCollectionsListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/twitter/android/ProfileCollectionsListActivity;->f:Landroid/widget/RadioButton;

    const v0, 0x7f090253    # com.twitter.android.R.id.scope_option_2

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileCollectionsListActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    iput-object v0, p0, Lcom/twitter/android/ProfileCollectionsListActivity;->g:Landroid/widget/RadioButton;

    const v0, 0x7f0f0328    # com.twitter.android.R.string.profile_tab_title_collections_owned_by

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileCollectionsListActivity;->setTitle(I)V

    iget-object v0, p0, Lcom/twitter/android/ProfileCollectionsListActivity;->e:Landroid/widget/RadioGroup;

    invoke-virtual {v0, p0}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    iget-object v0, p0, Lcom/twitter/android/ProfileCollectionsListActivity;->f:Landroid/widget/RadioButton;

    const v1, 0x7f0f0329    # com.twitter.android.R.string.profile_tab_title_created

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setText(I)V

    iget-object v0, p0, Lcom/twitter/android/ProfileCollectionsListActivity;->g:Landroid/widget/RadioButton;

    const v1, 0x7f0f032b    # com.twitter.android.R.string.profile_tab_title_following

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setText(I)V

    if-nez p1, :cond_3

    const-string/jumbo v0, "timelines_created"

    iput-object v0, p0, Lcom/twitter/android/ProfileCollectionsListActivity;->c:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/ProfileCollectionsListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "user_id"

    const-wide/16 v4, 0x0

    invoke-virtual {v0, v1, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/ProfileCollectionsListActivity;->b:J

    :goto_0
    invoke-static {}, Lgj;->f()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/twitter/android/ProfileCollectionsListActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c009d    # com.twitter.android.R.dimen.nav_bar_height

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ProfileCollectionsListActivity;->a:I

    :goto_1
    iget-object v0, p0, Lcom/twitter/android/ProfileCollectionsListActivity;->d:Lcom/twitter/android/gs;

    if-nez v0, :cond_1

    new-array v1, v10, [Lcom/twitter/android/gr;

    const-string/jumbo v5, "timelines_created"

    move-object v0, p0

    move v4, v3

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/ProfileCollectionsListActivity;->a([Lcom/twitter/android/gr;Landroid/content/Intent;IILjava/lang/String;)V

    invoke-static {}, Lgj;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v9, "timelines_following"

    move-object v4, p0

    move-object v5, v1

    move-object v6, v2

    move v8, v7

    invoke-direct/range {v4 .. v9}, Lcom/twitter/android/ProfileCollectionsListActivity;->a([Lcom/twitter/android/gr;Landroid/content/Intent;IILjava/lang/String;)V

    :cond_0
    new-instance v0, Lcom/twitter/android/gs;

    const v2, 0x7f0900e3    # com.twitter.android.R.id.fragment_container

    invoke-direct {v0, p0, v2, v1}, Lcom/twitter/android/gs;-><init>(Landroid/support/v4/app/FragmentActivity;I[Lcom/twitter/android/gr;)V

    iput-object v0, p0, Lcom/twitter/android/ProfileCollectionsListActivity;->d:Lcom/twitter/android/gs;

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/ProfileCollectionsListActivity;->d:Lcom/twitter/android/gs;

    iget-object v1, p0, Lcom/twitter/android/ProfileCollectionsListActivity;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/gs;->a(Ljava/lang/String;)Z

    if-nez p1, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/ProfileCollectionsListActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/ProfileCollectionsListActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/String;

    const-string/jumbo v5, "profile"

    aput-object v5, v4, v3

    const-string/jumbo v3, "custom_timeline_list"

    aput-object v3, v4, v7

    aput-object v11, v4, v10

    const/4 v3, 0x3

    aput-object v11, v4, v3

    const/4 v3, 0x4

    const-string/jumbo v5, "impression"

    aput-object v5, v4, v3

    invoke-virtual {v0, v1, v2, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_2
    return-void

    :cond_3
    const-string/jumbo v0, "state_current_tag"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ProfileCollectionsListActivity;->c:Ljava/lang/String;

    const-string/jumbo v0, "user_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/ProfileCollectionsListActivity;->b:J

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/twitter/android/ProfileCollectionsListActivity;->e:Landroid/widget/RadioGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setVisibility(I)V

    goto :goto_1
.end method

.method public a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z

    const v0, 0x7f110005    # com.twitter.android.R.menu.collections_list

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    iget-wide v3, p0, Lcom/twitter/android/ProfileCollectionsListActivity;->b:J

    invoke-virtual {p0}, Lcom/twitter/android/ProfileCollectionsListActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v5

    cmp-long v0, v3, v5

    if-nez v0, :cond_1

    invoke-static {}, Lgj;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    if-nez v0, :cond_0

    const v0, 0x7f090305    # com.twitter.android.R.id.menu_create_collection

    invoke-virtual {p2, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v0

    invoke-virtual {v0, v2}, Lhn;->b(Z)Lhn;

    :cond_0
    return v1

    :cond_1
    move v0, v2

    goto :goto_0
.end method

.method public a(Lhn;)Z
    .locals 2

    invoke-virtual {p1}, Lhn;->a()I

    move-result v0

    const v1, 0x7f090305    # com.twitter.android.R.id.menu_create_collection

    if-ne v0, v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/CollectionCreateEditActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/ProfileCollectionsListActivity;->startActivity(Landroid/content/Intent;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lhn;)Z

    move-result v0

    goto :goto_0
.end method

.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 2

    const v0, 0x7f090252    # com.twitter.android.R.id.scope_option_1

    if-ne p2, v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ProfileCollectionsListActivity;->d:Lcom/twitter/android/gs;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/gs;->a(I)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v0, 0x7f090253    # com.twitter.android.R.id.scope_option_2

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ProfileCollectionsListActivity;->d:Lcom/twitter/android/gs;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/gs;->a(I)Z

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "user_id"

    iget-wide v1, p0, Lcom/twitter/android/ProfileCollectionsListActivity;->b:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    iget-object v0, p0, Lcom/twitter/android/ProfileCollectionsListActivity;->d:Lcom/twitter/android/gs;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "state_current_tag"

    iget-object v1, p0, Lcom/twitter/android/ProfileCollectionsListActivity;->d:Lcom/twitter/android/gs;

    invoke-virtual {v1}, Lcom/twitter/android/gs;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
