.class Lcom/twitter/android/fi;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/EditProfileOnboardingActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/EditProfileOnboardingActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/fi;->a:Lcom/twitter/android/EditProfileOnboardingActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 4

    const/4 v2, 0x4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/fi;->a:Lcom/twitter/android/EditProfileOnboardingActivity;

    invoke-static {v0}, Lcom/twitter/android/EditProfileOnboardingActivity;->a(Lcom/twitter/android/EditProfileOnboardingActivity;)Landroid/widget/TextView;

    move-result-object v3

    if-eqz p2, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/fi;->a:Lcom/twitter/android/EditProfileOnboardingActivity;

    invoke-static {v0}, Lcom/twitter/android/EditProfileOnboardingActivity;->b(Lcom/twitter/android/EditProfileOnboardingActivity;)Landroid/view/View;

    move-result-object v0

    if-eqz p2, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method
