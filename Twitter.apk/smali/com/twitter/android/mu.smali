.class Lcom/twitter/android/mu;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/widget/ce;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/twitter/library/api/TweetEntities;

.field final synthetic c:J

.field final synthetic d:Lcom/twitter/android/MessagesThreadFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/MessagesThreadFragment;Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;J)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/mu;->d:Lcom/twitter/android/MessagesThreadFragment;

    iput-object p2, p0, Lcom/twitter/android/mu;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/twitter/android/mu;->b:Lcom/twitter/library/api/TweetEntities;

    iput-wide p4, p0, Lcom/twitter/android/mu;->c:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/DialogInterface;II)V
    .locals 6

    const/4 v5, 0x0

    packed-switch p3, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/mu;->d:Lcom/twitter/android/MessagesThreadFragment;

    iget-object v1, p0, Lcom/twitter/android/mu;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/mu;->b:Lcom/twitter/library/api/TweetEntities;

    invoke-static {v0, v1, v2}, Lcom/twitter/android/MessagesThreadFragment;->a(Lcom/twitter/android/MessagesThreadFragment;Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/mu;->d:Lcom/twitter/android/MessagesThreadFragment;

    invoke-static {v0}, Lcom/twitter/android/MessagesThreadFragment;->b(Lcom/twitter/android/MessagesThreadFragment;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/mu;->d:Lcom/twitter/android/MessagesThreadFragment;

    invoke-static {v1}, Lcom/twitter/android/MessagesThreadFragment;->c(Lcom/twitter/android/MessagesThreadFragment;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "messages:thread:message::delete"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v1, p0, Lcom/twitter/android/mu;->d:Lcom/twitter/android/MessagesThreadFragment;

    iget-wide v2, p0, Lcom/twitter/android/mu;->c:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/client/c;->c(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/twitter/android/MessagesThreadFragment;->a(Lcom/twitter/android/MessagesThreadFragment;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0476    # com.twitter.android.R.string.spam_message_title

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f08000b    # com.twitter.android.R.array.mark_message_spam

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->f(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/mu;->d:Lcom/twitter/android/MessagesThreadFragment;

    invoke-virtual {v0, v1, v5}, Lcom/twitter/android/widget/PromptDialogFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    iget-object v1, p0, Lcom/twitter/android/mu;->d:Lcom/twitter/android/MessagesThreadFragment;

    iget-wide v2, p0, Lcom/twitter/android/mu;->c:J

    invoke-static {v1, v2, v3}, Lcom/twitter/android/MessagesThreadFragment;->a(Lcom/twitter/android/MessagesThreadFragment;J)J

    iget-object v1, p0, Lcom/twitter/android/mu;->d:Lcom/twitter/android/MessagesThreadFragment;

    invoke-virtual {v1}, Lcom/twitter/android/MessagesThreadFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
