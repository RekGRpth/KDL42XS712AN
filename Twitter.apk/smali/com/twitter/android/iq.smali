.class public Lcom/twitter/android/iq;
.super Landroid/widget/ArrayAdapter;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/SectionIndexer;
.implements Lcom/twitter/library/widget/i;


# instance fields
.field protected a:I

.field private final b:I

.field private final c:I

.field private final d:Ljava/util/ArrayList;

.field private final e:Lcom/twitter/library/client/aa;

.field private f:Z

.field private g:Landroid/widget/Button;

.field private h:Landroid/widget/CheckBox;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/TextView;

.field private k:[Ljava/lang/String;

.field private l:Lcom/twitter/android/client/c;

.field private m:Z

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Z

.field private q:Z

.field private r:Lcom/twitter/android/ir;


# direct methods
.method public constructor <init>(Landroid/content/Context;IILjava/util/ArrayList;Ljava/lang/String;Z)V
    .locals 3

    const/4 v2, 0x1

    const v0, 0x7f03009f    # com.twitter.android.R.layout.invite_row_view

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, p4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-direct {p0, p1, v0, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput v2, p0, Lcom/twitter/android/iq;->a:I

    iput-boolean v2, p0, Lcom/twitter/android/iq;->f:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/iq;->m:Z

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/twitter/android/iq;->n:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/twitter/android/iq;->q:Z

    new-instance v0, Lcom/twitter/android/is;

    invoke-direct {v0}, Lcom/twitter/android/is;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/iq;->r:Lcom/twitter/android/ir;

    iput p2, p0, Lcom/twitter/android/iq;->b:I

    iput p3, p0, Lcom/twitter/android/iq;->c:I

    iput-object p4, p0, Lcom/twitter/android/iq;->d:Ljava/util/ArrayList;

    invoke-static {p1}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/iq;->l:Lcom/twitter/android/client/c;

    invoke-static {p1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/iq;->e:Lcom/twitter/library/client/aa;

    iput-object p5, p0, Lcom/twitter/android/iq;->o:Ljava/lang/String;

    iput-boolean p6, p0, Lcom/twitter/android/iq;->p:Z

    invoke-virtual {p0}, Lcom/twitter/android/iq;->getSections()[Ljava/lang/Object;

    new-instance v0, Lcom/twitter/android/is;

    invoke-direct {v0}, Lcom/twitter/android/is;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/iq;->r:Lcom/twitter/android/ir;

    iget-boolean v0, p0, Lcom/twitter/android/iq;->p:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    iput v0, p0, Lcom/twitter/android/iq;->a:I

    :cond_0
    return-void
.end method


# virtual methods
.method a()V
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/iq;->g:Landroid/widget/Button;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/iq;->g:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/twitter/android/iq;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f0f0513    # com.twitter.android.R.string.unselect_all

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setText(I)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/iq;->h:Landroid/widget/CheckBox;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/iq;->h:Landroid/widget/CheckBox;

    invoke-virtual {p0}, Lcom/twitter/android/iq;->b()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/iq;->i:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/iq;->i:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/twitter/android/iq;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0263    # com.twitter.android.R.string.matched_contacts_format

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-super {p0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    return-void

    :cond_3
    const v0, 0x7f0f03c2    # com.twitter.android.R.string.select_all

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/ir;)V
    .locals 0

    if-nez p1, :cond_0

    new-instance p1, Lcom/twitter/android/is;

    invoke-direct {p1}, Lcom/twitter/android/is;-><init>()V

    :cond_0
    iput-object p1, p0, Lcom/twitter/android/iq;->r:Lcom/twitter/android/ir;

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/iq;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iput-object v0, p0, Lcom/twitter/android/iq;->n:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/iq;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 1

    iput-boolean p1, p0, Lcom/twitter/android/iq;->f:Z

    iget-object v0, p0, Lcom/twitter/android/iq;->g:Landroid/widget/Button;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/iq;->g:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    :cond_0
    return-void
.end method

.method public b(I)Lcom/twitter/library/api/TwitterContact;
    .locals 1

    iget v0, p0, Lcom/twitter/android/iq;->a:I

    if-ge p1, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget v0, p0, Lcom/twitter/android/iq;->a:I

    sub-int v0, p1, v0

    invoke-super {p0, v0}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterContact;

    goto :goto_0
.end method

.method public b()Z
    .locals 4

    const/4 v1, 0x0

    invoke-super {p0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_1

    invoke-super {p0, v2}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterContact;

    iget-boolean v0, v0, Lcom/twitter/library/api/TwitterContact;->c:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public c()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/iq;->q:Z

    return v0
.end method

.method public d()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/iq;->m:Z

    return v0
.end method

.method public getCount()I
    .locals 2

    invoke-super {p0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/iq;->n:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    iget v1, p0, Lcom/twitter/android/iq;->a:I

    add-int/2addr v0, v1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/iq;->b(I)Lcom/twitter/library/api/TwitterContact;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    iget v0, p0, Lcom/twitter/android/iq;->a:I

    if-ge p1, v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget v0, p0, Lcom/twitter/android/iq;->a:I

    sub-int v0, p1, v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 1

    const/4 v0, 0x1

    if-nez p1, :cond_0

    :goto_0
    return v0

    :cond_0
    if-ne p1, v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/android/iq;->p:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getPositionForSection(I)I
    .locals 5

    const/4 v1, 0x0

    if-lez p1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/iq;->k:[Ljava/lang/String;

    array-length v0, v0

    if-ge p1, v0, :cond_1

    invoke-super {p0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/iq;->k:[Ljava/lang/String;

    aget-object v4, v0, p1

    invoke-super {p0, v2}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterContact;

    invoke-virtual {v0}, Lcom/twitter/library/api/TwitterContact;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/android/iq;->a:I

    add-int/2addr v0, v2

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public getSectionForPosition(I)I
    .locals 4

    const/4 v1, 0x0

    iget v0, p0, Lcom/twitter/android/iq;->a:I

    sub-int v0, p1, v0

    if-ltz v0, :cond_1

    invoke-super {p0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    invoke-super {p0, v0}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterContact;

    invoke-virtual {v0}, Lcom/twitter/library/api/TwitterContact;->a()Ljava/lang/String;

    move-result-object v2

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/twitter/android/iq;->k:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    iget-object v3, p0, Lcom/twitter/android/iq;->k:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public getSections()[Ljava/lang/Object;
    .locals 4

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-super {p0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-super {p0, v1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterContact;

    invoke-virtual {v0}, Lcom/twitter/library/api/TwitterContact;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/android/iq;->k:[Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/iq;->k:[Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/android/iq;->k:[Ljava/lang/String;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    const/16 v6, 0x8

    const/4 v5, 0x1

    const/4 v1, 0x0

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    iget v0, p0, Lcom/twitter/android/iq;->a:I

    if-ge p1, v0, :cond_7

    if-nez p1, :cond_5

    invoke-super {p0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v3

    if-nez p2, :cond_1

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v4, 0x7f03009e    # com.twitter.android.R.layout.invite_all_header

    invoke-virtual {v0, v4, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    const v0, 0x7f0900da    # com.twitter.android.R.id.subtitle

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget v4, p0, Lcom/twitter/android/iq;->c:I

    if-lez v4, :cond_0

    iget v4, p0, Lcom/twitter/android/iq;->c:I

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    const v0, 0x7f090183    # com.twitter.android.R.id.found_people

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/iq;->j:Landroid/widget/TextView;

    const v0, 0x7f0901ac    # com.twitter.android.R.id.select_all

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/twitter/android/iq;->g:Landroid/widget/Button;

    iget-boolean v0, p0, Lcom/twitter/android/iq;->p:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/iq;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/iq;->g:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setVisibility(I)V

    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/iq;->a()V

    iget-boolean v0, p0, Lcom/twitter/android/iq;->p:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/iq;->j:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget v4, p0, Lcom/twitter/android/iq;->b:I

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v1

    invoke-virtual {v2, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    :goto_1
    return-object p2

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/iq;->g:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/twitter/android/iq;->g:Landroid/widget/Button;

    iget-boolean v4, p0, Lcom/twitter/android/iq;->f:Z

    invoke-virtual {v0, v4}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v4, p0, Lcom/twitter/android/iq;->g:Landroid/widget/Button;

    if-nez v3, :cond_4

    const/4 v0, 0x4

    :goto_2
    invoke-virtual {v4, v0}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_2

    :cond_5
    if-ne p1, v5, :cond_2

    iget-boolean v0, p0, Lcom/twitter/android/iq;->p:Z

    if-eqz v0, :cond_2

    if-nez p2, :cond_6

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f030137    # com.twitter.android.R.layout.select_all_check_bar

    invoke-virtual {v0, v2, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    const v0, 0x7f090268    # com.twitter.android.R.id.select_all_checkbox

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/twitter/android/iq;->h:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/twitter/android/iq;->h:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090267    # com.twitter.android.R.id.friend_count

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/iq;->i:Landroid/widget/TextView;

    :cond_6
    invoke-virtual {p0}, Lcom/twitter/android/iq;->a()V

    goto :goto_1

    :cond_7
    if-nez p2, :cond_8

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f03009f    # com.twitter.android.R.layout.invite_row_view

    invoke-virtual {v0, v2, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    :goto_3
    move-object v0, v1

    check-cast v0, Lcom/twitter/library/widget/InviteView;

    iget v2, p0, Lcom/twitter/android/iq;->a:I

    sub-int v3, p1, v2

    invoke-super {p0, v3}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/TwitterContact;

    invoke-virtual {v0, v3}, Lcom/twitter/library/widget/InviteView;->setId(I)V

    iget-object v3, v2, Lcom/twitter/library/api/TwitterContact;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/twitter/library/widget/InviteView;->setName(Ljava/lang/String;)V

    iget-object v3, v2, Lcom/twitter/library/api/TwitterContact;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/twitter/library/widget/InviteView;->setEmail(Ljava/lang/String;)V

    iget-boolean v2, v2, Lcom/twitter/library/api/TwitterContact;->c:Z

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/InviteView;->setChecked(Z)V

    invoke-virtual {v0, p0}, Lcom/twitter/library/widget/InviteView;->setOnViewClickListener(Lcom/twitter/library/widget/i;)V

    move-object p2, v1

    goto :goto_1

    :cond_8
    move-object v1, p2

    goto :goto_3
.end method

.method public getViewTypeCount()I
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/iq;->p:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public notifyDataSetChanged()V
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/iq;->setNotifyOnChange(Z)V

    invoke-virtual {p0}, Lcom/twitter/android/iq;->clear()V

    iget-object v1, p0, Lcom/twitter/android/iq;->n:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/iq;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterContact;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p0, v0}, Lcom/twitter/android/iq;->add(Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    iget-object v3, v0, Lcom/twitter/library/api/TwitterContact;->a:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, v0, Lcom/twitter/library/api/TwitterContact;->b:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_2
    invoke-virtual {p0, v0}, Lcom/twitter/android/iq;->add(Ljava/lang/Object;)V

    goto :goto_0

    :cond_3
    invoke-super {p0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8

    const v4, 0x7f090268    # com.twitter.android.R.id.select_all_checkbox

    const v3, 0x7f0901ac    # com.twitter.android.R.id.select_all

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    if-eq v0, v3, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    if-ne v0, v4, :cond_5

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    if-ne v0, v3, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/iq;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    move v3, v0

    :goto_1
    iput-boolean v3, p0, Lcom/twitter/android/iq;->q:Z

    invoke-super {p0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v5

    move v4, v2

    :goto_2
    if-ge v4, v5, :cond_4

    invoke-super {p0, v4}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterContact;

    iput-boolean v3, v0, Lcom/twitter/library/api/TwitterContact;->c:Z

    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_2

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    if-ne v0, v4, :cond_3

    check-cast p1, Landroid/widget/CheckBox;

    invoke-virtual {p1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    move v3, v0

    goto :goto_1

    :cond_3
    move v3, v2

    goto :goto_1

    :cond_4
    invoke-virtual {p0}, Lcom/twitter/android/iq;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/twitter/android/iq;->r:Lcom/twitter/android/ir;

    invoke-interface {v0}, Lcom/twitter/android/ir;->b()V

    if-eqz v3, :cond_6

    iget-object v0, p0, Lcom/twitter/android/iq;->l:Lcom/twitter/android/client/c;

    iget-object v3, p0, Lcom/twitter/android/iq;->e:Lcom/twitter/library/client/aa;

    invoke-virtual {v3}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    new-array v5, v1, [Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/twitter/android/iq;->o:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ":invite_contacts::select_all:click"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {v0, v3, v4, v5}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :goto_3
    iput-boolean v1, p0, Lcom/twitter/android/iq;->m:Z

    :cond_5
    return-void

    :cond_6
    iget-object v0, p0, Lcom/twitter/android/iq;->l:Lcom/twitter/android/client/c;

    iget-object v3, p0, Lcom/twitter/android/iq;->e:Lcom/twitter/library/client/aa;

    invoke-virtual {v3}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    new-array v5, v1, [Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/twitter/android/iq;->o:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ":invite_contacts::unselect_all:click"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {v0, v3, v4, v5}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_3
.end method

.method public onClick(Lcom/twitter/library/widget/InviteView;I)V
    .locals 2

    invoke-super {p0, p2}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterContact;

    invoke-virtual {p1}, Lcom/twitter/library/widget/InviteView;->a()Z

    move-result v1

    iput-boolean v1, v0, Lcom/twitter/library/api/TwitterContact;->c:Z

    invoke-virtual {p0}, Lcom/twitter/android/iq;->a()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/iq;->m:Z

    iget-object v0, p0, Lcom/twitter/android/iq;->r:Lcom/twitter/android/ir;

    invoke-interface {v0}, Lcom/twitter/android/ir;->b()V

    return-void
.end method

.method public bridge synthetic onClick(Ljava/lang/Object;I)V
    .locals 0

    check-cast p1, Lcom/twitter/library/widget/InviteView;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/iq;->onClick(Lcom/twitter/library/widget/InviteView;I)V

    return-void
.end method
