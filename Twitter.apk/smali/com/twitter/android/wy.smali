.class Lcom/twitter/android/wy;
.super Lcom/twitter/library/client/j;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/TweetBoxFragment;


# direct methods
.method private constructor <init>(Lcom/twitter/android/TweetBoxFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/wy;->a:Lcom/twitter/android/TweetBoxFragment;

    invoke-direct {p0}, Lcom/twitter/library/client/j;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/TweetBoxFragment;Lcom/twitter/android/wq;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/wy;-><init>(Lcom/twitter/android/TweetBoxFragment;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;IZLjava/lang/String;Lcom/twitter/library/api/search/TwitterTypeAheadGroup;)V
    .locals 2

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_0

    if-nez p6, :cond_0

    packed-switch p5, :pswitch_data_0

    :goto_0
    invoke-virtual {p8}, Lcom/twitter/library/api/search/TwitterTypeAheadGroup;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/wy;->a:Lcom/twitter/android/TweetBoxFragment;

    iget-object v0, v0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/PopupEditText;->a(Z)V

    :cond_0
    return-void

    :pswitch_0
    iget-object v0, p8, Lcom/twitter/library/api/search/TwitterTypeAheadGroup;->c:Ljava/util/ArrayList;

    invoke-static {p7, v0}, Lcom/twitter/android/provider/SuggestionsProvider;->b(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p8, Lcom/twitter/library/api/search/TwitterTypeAheadGroup;->a:Ljava/util/ArrayList;

    invoke-static {p7, v0}, Lcom/twitter/android/provider/SuggestionsProvider;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
