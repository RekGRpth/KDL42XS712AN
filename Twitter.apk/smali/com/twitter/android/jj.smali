.class Lcom/twitter/android/jj;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/client/ac;


# instance fields
.field final synthetic a:Lcom/twitter/android/LoginChallengeActivity;


# direct methods
.method public constructor <init>(Lcom/twitter/android/LoginChallengeActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/jj;->a:Lcom/twitter/android/LoginChallengeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;II[IZ)V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/twitter/android/jj;->a:Lcom/twitter/android/LoginChallengeActivity;

    invoke-virtual {v0}, Lcom/twitter/android/LoginChallengeActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/jj;->a:Lcom/twitter/android/LoginChallengeActivity;

    invoke-static {v0}, Lcom/twitter/android/LoginChallengeActivity;->m(Lcom/twitter/android/LoginChallengeActivity;)Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    const/4 v0, 0x2

    if-ne p2, v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/jj;->a:Lcom/twitter/android/LoginChallengeActivity;

    const v1, 0x7f0f0495    # com.twitter.android.R.string.sync_contacts_account_create_error

    invoke-virtual {v0, v1}, Lcom/twitter/android/LoginChallengeActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/jj;->a:Lcom/twitter/android/LoginChallengeActivity;

    invoke-virtual {v0}, Lcom/twitter/android/LoginChallengeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v4, "accountAuthenticatorResponse"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/AccountAuthenticatorResponse;

    if-eqz v0, :cond_1

    const/16 v4, 0x190

    invoke-virtual {v0, v4, v1}, Landroid/accounts/AccountAuthenticatorResponse;->onError(ILjava/lang/String;)V

    :cond_1
    move-object v0, v1

    :goto_1
    iget-object v1, p0, Lcom/twitter/android/jj;->a:Lcom/twitter/android/LoginChallengeActivity;

    invoke-static {v1, v0, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    sget-object v0, Lcom/twitter/library/client/Session$LoginStatus;->a:Lcom/twitter/library/client/Session$LoginStatus;

    invoke-virtual {p1, v0}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/library/client/Session$LoginStatus;)V

    iget-object v0, p0, Lcom/twitter/android/jj;->a:Lcom/twitter/android/LoginChallengeActivity;

    invoke-virtual {v0, v6}, Lcom/twitter/android/LoginChallengeActivity;->setResult(I)V

    iget-object v0, p0, Lcom/twitter/android/jj;->a:Lcom/twitter/android/LoginChallengeActivity;

    invoke-static {v0}, Lcom/twitter/android/LoginChallengeActivity;->r(Lcom/twitter/android/LoginChallengeActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v4, "login::::failure"

    aput-object v4, v1, v6

    invoke-virtual {v0, v2, v3, v1}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/jj;->a:Lcom/twitter/android/LoginChallengeActivity;

    invoke-virtual {v0}, Lcom/twitter/android/LoginChallengeActivity;->finish()V

    goto :goto_0

    :cond_2
    if-eqz p4, :cond_3

    const/16 v0, 0x58

    invoke-static {p4, v0}, Lcom/twitter/library/util/Util;->a([II)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/jj;->a:Lcom/twitter/android/LoginChallengeActivity;

    const v1, 0x7f0f01b4    # com.twitter.android.R.string.generic_error

    invoke-virtual {v0, v1}, Lcom/twitter/android/LoginChallengeActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/jj;->a:Lcom/twitter/android/LoginChallengeActivity;

    invoke-static {v1}, Lcom/twitter/android/LoginChallengeActivity;->n(Lcom/twitter/android/LoginChallengeActivity;)Lcom/twitter/android/client/c;

    move-result-object v1

    new-array v4, v7, [Ljava/lang/String;

    const-string/jumbo v5, "login_challenge::::rate_limit"

    aput-object v5, v4, v6

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/jj;->a:Lcom/twitter/android/LoginChallengeActivity;

    iget-object v1, p0, Lcom/twitter/android/jj;->a:Lcom/twitter/android/LoginChallengeActivity;

    invoke-static {v1}, Lcom/twitter/android/LoginChallengeActivity;->o(Lcom/twitter/android/LoginChallengeActivity;)I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/twitter/android/jj;->a:Lcom/twitter/android/LoginChallengeActivity;

    invoke-static {v2}, Lcom/twitter/android/LoginChallengeActivity;->p(Lcom/twitter/android/LoginChallengeActivity;)F

    move-result v2

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-static {v0, v1}, Lcom/twitter/android/LoginChallengeActivity;->a(Lcom/twitter/android/LoginChallengeActivity;I)I

    iget-object v0, p0, Lcom/twitter/android/jj;->a:Lcom/twitter/android/LoginChallengeActivity;

    invoke-static {v0}, Lcom/twitter/android/LoginChallengeActivity;->q(Lcom/twitter/android/LoginChallengeActivity;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/ji;

    iget-object v2, p0, Lcom/twitter/android/jj;->a:Lcom/twitter/android/LoginChallengeActivity;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/twitter/android/ji;-><init>(Lcom/twitter/android/LoginChallengeActivity;Lcom/twitter/android/jh;)V

    iget-object v2, p0, Lcom/twitter/android/jj;->a:Lcom/twitter/android/LoginChallengeActivity;

    invoke-static {v2}, Lcom/twitter/android/LoginChallengeActivity;->o(Lcom/twitter/android/LoginChallengeActivity;)I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;Z)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/twitter/android/jj;->a:Lcom/twitter/android/LoginChallengeActivity;

    invoke-virtual {v0}, Lcom/twitter/android/LoginChallengeActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/jj;->a:Lcom/twitter/android/LoginChallengeActivity;

    invoke-virtual {v0}, Lcom/twitter/android/LoginChallengeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "android.intent.extra.INTENT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/twitter/android/jj;->a:Lcom/twitter/android/LoginChallengeActivity;

    invoke-virtual {v1, v0}, Lcom/twitter/android/LoginChallengeActivity;->startActivity(Landroid/content/Intent;)V

    :cond_1
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/jj;->a:Lcom/twitter/android/LoginChallengeActivity;

    invoke-virtual {v0}, Lcom/twitter/android/LoginChallengeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "accountAuthenticatorResponse"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/AccountAuthenticatorResponse;

    if-eqz v0, :cond_2

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v3, "authAccount"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "accountType"

    sget-object v3, Lcom/twitter/library/util/a;->a:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "account_user_info"

    invoke-virtual {v2, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Landroid/accounts/AccountAuthenticatorResponse;->onResult(Landroid/os/Bundle;)V

    :cond_2
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v1, "sb_account_name"

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "session"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "user"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/jj;->a:Lcom/twitter/android/LoginChallengeActivity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/twitter/android/LoginChallengeActivity;->setResult(ILandroid/content/Intent;)V

    iget-object v0, p0, Lcom/twitter/android/jj;->a:Lcom/twitter/android/LoginChallengeActivity;

    invoke-static {v0}, Lcom/twitter/android/LoginChallengeActivity;->i(Lcom/twitter/android/LoginChallengeActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/jj;->a:Lcom/twitter/android/LoginChallengeActivity;

    invoke-static {v1}, Lcom/twitter/android/LoginChallengeActivity;->j(Lcom/twitter/android/LoginChallengeActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "login_challenge::::success"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "login::::success"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/jj;->a:Lcom/twitter/android/LoginChallengeActivity;

    invoke-static {v0}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;)Lcom/twitter/android/util/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/android/util/d;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/jj;->a:Lcom/twitter/android/LoginChallengeActivity;

    invoke-static {v0}, Lcom/twitter/android/LoginChallengeActivity;->k(Lcom/twitter/android/LoginChallengeActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/android/client/c;->c(Lcom/twitter/library/client/Session;)Ljava/lang/String;

    :goto_1
    iget-object v0, p0, Lcom/twitter/android/jj;->a:Lcom/twitter/android/LoginChallengeActivity;

    invoke-virtual {v0}, Lcom/twitter/android/LoginChallengeActivity;->finish()V

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/jj;->a:Lcom/twitter/android/LoginChallengeActivity;

    invoke-static {v0}, Lcom/twitter/android/LoginChallengeActivity;->l(Lcom/twitter/android/LoginChallengeActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/android/client/c;->d(Lcom/twitter/library/client/Session;)Ljava/lang/String;

    goto :goto_1
.end method
