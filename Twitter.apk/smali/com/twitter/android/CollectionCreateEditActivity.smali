.class public Lcom/twitter/android/CollectionCreateEditActivity;
.super Lcom/twitter/android/client/BaseFragmentActivity;
.source "Twttr"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Lcom/twitter/android/widget/ce;


# instance fields
.field private a:Landroid/widget/EditText;

.field private b:Landroid/widget/EditText;

.field private c:Z

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;-><init>()V

    return-void
.end method

.method private a(I)V
    .locals 2

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/CollectionCreateEditActivity;->a:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;Landroid/view/View;Z)V

    invoke-static {p1}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const/high16 v1, 0x7f0f0000    # com.twitter.android.R.string.abandon_changes_question

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->e(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0111    # com.twitter.android.R.string.discard

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->g(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0089    # com.twitter.android.R.string.cancel

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->i(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/android/CollectionCreateEditActivity;->c:Z

    if-eqz v1, :cond_0

    const v1, 0x7f0f009f    # com.twitter.android.R.string.collections_edit_collection

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    :goto_1
    invoke-virtual {p0}, Lcom/twitter/android/CollectionCreateEditActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    goto :goto_0

    :cond_0
    const v1, 0x7f0f00a5    # com.twitter.android.R.string.collections_new_collection

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/CollectionCreateEditActivity;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private g()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/CollectionCreateEditActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private h()Z
    .locals 3

    const/4 v0, 0x1

    invoke-direct {p0}, Lcom/twitter/android/CollectionCreateEditActivity;->f()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/CollectionCreateEditActivity;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0}, Lcom/twitter/android/CollectionCreateEditActivity;->g()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/CollectionCreateEditActivity;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private k()Z
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/CollectionCreateEditActivity;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 2

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    const v1, 0x7f030045    # com.twitter.android.R.layout.create_edit_collection_view

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->a(Z)V

    return-object v0
.end method

.method protected a(IILcom/twitter/library/service/b;)V
    .locals 2

    const/4 v1, 0x1

    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/client/BaseFragmentActivity;->a(IILcom/twitter/library/service/b;)V

    invoke-virtual {p3}, Lcom/twitter/library/service/b;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/CollectionCreateEditActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/twitter/android/CollectionCreateEditActivity;->c:Z

    if-eqz v0, :cond_1

    const v0, 0x7f0f00aa    # com.twitter.android.R.string.collections_update_fail

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :goto_1
    invoke-virtual {p0}, Lcom/twitter/android/CollectionCreateEditActivity;->V()V

    goto :goto_0

    :cond_1
    const v0, 0x7f0f009b    # com.twitter.android.R.string.collections_create_fail

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 1

    const/4 v0, -0x1

    packed-switch p2, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    if-ne p3, v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/CollectionCreateEditActivity;->finish()V

    goto :goto_0

    :pswitch_1
    if-ne p3, v0, :cond_0

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->j()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V

    const v0, 0x7f0f00a5    # com.twitter.android.R.string.collections_new_collection

    invoke-virtual {p0, v0}, Lcom/twitter/android/CollectionCreateEditActivity;->setTitle(I)V

    invoke-virtual {p0}, Lcom/twitter/android/CollectionCreateEditActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "timeline_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "timeline_id"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/CollectionCreateEditActivity;->d:Ljava/lang/String;

    iput-boolean v4, p0, Lcom/twitter/android/CollectionCreateEditActivity;->c:Z

    const-string/jumbo v1, "timeline_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/CollectionCreateEditActivity;->e:Ljava/lang/String;

    const-string/jumbo v1, "timeline_description"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/CollectionCreateEditActivity;->f:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/CollectionCreateEditActivity;->e:Ljava/lang/String;

    if-nez v0, :cond_1

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/twitter/android/CollectionCreateEditActivity;->e:Ljava/lang/String;

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/CollectionCreateEditActivity;->f:Ljava/lang/String;

    if-nez v0, :cond_2

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/twitter/android/CollectionCreateEditActivity;->f:Ljava/lang/String;

    :cond_2
    const v0, 0x7f090097    # com.twitter.android.R.id.name

    invoke-virtual {p0, v0}, Lcom/twitter/android/CollectionCreateEditActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/twitter/android/CollectionCreateEditActivity;->a:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/twitter/android/CollectionCreateEditActivity;->a:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/twitter/android/CollectionCreateEditActivity;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f0900dd    # com.twitter.android.R.id.description

    invoke-virtual {p0, v0}, Lcom/twitter/android/CollectionCreateEditActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/twitter/android/CollectionCreateEditActivity;->b:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/twitter/android/CollectionCreateEditActivity;->b:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/twitter/android/CollectionCreateEditActivity;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/CollectionCreateEditActivity;->a:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/twitter/android/CollectionCreateEditActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/twitter/android/CollectionCreateEditActivity;->a:Landroid/widget/EditText;

    new-array v1, v4, [Landroid/text/InputFilter;

    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->S()I

    move-result v3

    invoke-direct {v2, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    iget-object v0, p0, Lcom/twitter/android/CollectionCreateEditActivity;->b:Landroid/widget/EditText;

    new-array v1, v4, [Landroid/text/InputFilter;

    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->T()I

    move-result v3

    invoke-direct {v2, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, v1, v5

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    return-void
.end method

.method protected a(Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 3

    const/4 v1, 0x1

    const v0, 0x7f090141    # com.twitter.android.R.id.save

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v2

    invoke-direct {p0}, Lcom/twitter/android/CollectionCreateEditActivity;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/CollectionCreateEditActivity;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Lhn;->c(Z)Lhn;

    return v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 1

    const v0, 0x7f110022    # com.twitter.android.R.menu.toolbar_save

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    const/4 v0, 0x1

    return v0
.end method

.method public a(Lhn;)Z
    .locals 11

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v10, 0x0

    invoke-virtual {p1}, Lhn;->a()I

    move-result v0

    const v1, 0x7f090141    # com.twitter.android.R.id.save

    if-ne v0, v1, :cond_2

    invoke-virtual {p1, v10}, Lhn;->c(Z)Lhn;

    new-instance v0, Ljg;

    invoke-virtual {p0}, Lcom/twitter/android/CollectionCreateEditActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {p0}, Lcom/twitter/android/CollectionCreateEditActivity;->f()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0}, Lcom/twitter/android/CollectionCreateEditActivity;->g()Ljava/lang/String;

    move-result-object v4

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Ljg;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/twitter/android/CollectionCreateEditActivity;->c:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/CollectionCreateEditActivity;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljg;->a(Ljava/lang/String;)V

    const-string/jumbo v1, "update"

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/CollectionCreateEditActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/CollectionCreateEditActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    const/4 v8, 0x5

    new-array v8, v8, [Ljava/lang/String;

    const-string/jumbo v9, "me"

    aput-object v9, v8, v10

    aput-object v5, v8, v7

    aput-object v5, v8, v6

    const/4 v5, 0x3

    const-string/jumbo v9, "custom_timeline"

    aput-object v9, v8, v5

    const/4 v5, 0x4

    aput-object v1, v8, v5

    invoke-virtual {v2, v3, v4, v8}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/twitter/android/CollectionCreateEditActivity;->c:Z

    if-eqz v1, :cond_1

    move v1, v6

    :goto_1
    invoke-virtual {p0, v0, v1, v10}, Lcom/twitter/android/CollectionCreateEditActivity;->a(Lcom/twitter/library/service/b;II)Z

    :goto_2
    return v7

    :cond_0
    const-string/jumbo v1, "create"

    goto :goto_0

    :cond_1
    move v1, v7

    goto :goto_1

    :cond_2
    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lhn;)Z

    move-result v7

    goto :goto_2
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/android/CollectionCreateEditActivity;->V()V

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method protected j()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/CollectionCreateEditActivity;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/twitter/android/CollectionCreateEditActivity;->a(I)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->j()V

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/CollectionCreateEditActivity;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/android/CollectionCreateEditActivity;->a(I)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method
