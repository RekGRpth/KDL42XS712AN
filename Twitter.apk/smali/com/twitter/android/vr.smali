.class Lcom/twitter/android/vr;
.super Lcom/twitter/library/client/j;
.source "Twttr"


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Landroid/content/Context;

.field final synthetic c:Lcom/twitter/android/vn;


# direct methods
.method constructor <init>(Lcom/twitter/android/vn;Ljava/lang/String;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/vr;->c:Lcom/twitter/android/vn;

    iput-object p2, p0, Lcom/twitter/android/vr;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/twitter/android/vr;->b:Landroid/content/Context;

    invoke-direct {p0}, Lcom/twitter/library/client/j;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;J)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/vr;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/vr;->a:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/vr;->c:Lcom/twitter/android/vn;

    iget-object v0, v0, Lcom/twitter/android/vn;->a:Lcom/twitter/android/client/c;

    invoke-virtual {v0, p0}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/j;)V

    const/16 v0, 0xc8

    if-eq p3, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/vr;->b:Landroid/content/Context;

    const v1, 0x7f0f04ee    # com.twitter.android.R.string.tweets_retweet_error

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;JI)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/vr;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/vr;->a:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/vr;->c:Lcom/twitter/android/vn;

    iget-object v0, v0, Lcom/twitter/android/vn;->a:Lcom/twitter/android/client/c;

    invoke-virtual {v0, p0}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/j;)V

    const/16 v0, 0xc8

    if-eq p3, v0, :cond_0

    const/16 v0, 0x194

    if-eq p3, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/vr;->b:Landroid/content/Context;

    const v1, 0x7f0f04ec    # com.twitter.android.R.string.tweets_remove_favorite_error

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;I[ILjava/lang/String;JI)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/vr;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/vr;->a:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/vr;->c:Lcom/twitter/android/vn;

    iget-object v0, v0, Lcom/twitter/android/vn;->a:Lcom/twitter/android/client/c;

    invoke-virtual {v0, p0}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/j;)V

    const/16 v0, 0xc8

    if-eq p3, v0, :cond_0

    const/16 v0, 0x8b

    invoke-static {p4, v0}, Lcom/twitter/library/util/Util;->a([II)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/vr;->b:Landroid/content/Context;

    const v1, 0x7f0f04dd    # com.twitter.android.R.string.tweets_add_favorite_error

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method

.method public b(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;J)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/vr;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/vr;->a:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/vr;->c:Lcom/twitter/android/vn;

    iget-object v0, v0, Lcom/twitter/android/vn;->a:Lcom/twitter/android/client/c;

    invoke-virtual {v0, p0}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/j;)V

    const/16 v0, 0xc8

    if-eq p3, v0, :cond_0

    const/16 v0, 0x194

    if-eq p3, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/vr;->b:Landroid/content/Context;

    const v1, 0x7f0f04e0    # com.twitter.android.R.string.tweets_delete_status_error

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method
