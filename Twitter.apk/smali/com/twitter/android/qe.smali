.class Lcom/twitter/android/qe;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/ob;


# instance fields
.field final synthetic a:Lcom/twitter/android/ProfileFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/ProfileFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/qe;->a:Lcom/twitter/android/ProfileFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;Lcom/twitter/library/provider/Tweet;Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/twitter/android/qe;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/android/ProfileFragment;)Ljava/util/HashSet;

    move-result-object v0

    iget-wide v1, p2, Lcom/twitter/library/provider/Tweet;->o:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/qe;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->b(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    invoke-static {v3, p2, v0, v3}, Lcom/twitter/library/scribe/ScribeItem;->a(Landroid/content/Context;Lcom/twitter/library/provider/ParcelableTweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    const-string/jumbo v1, "position"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/twitter/library/scribe/ScribeItem;->g:I

    iget-object v1, p0, Lcom/twitter/android/qe;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v1}, Lcom/twitter/android/ProfileFragment;->c(Lcom/twitter/android/ProfileFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p2, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/qe;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v1}, Lcom/twitter/android/ProfileFragment;->d(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/android/client/c;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/api/PromotedEvent;->a:Lcom/twitter/library/api/PromotedEvent;

    invoke-virtual {v1, v2, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/library/api/PromotedContent;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V
    .locals 0

    check-cast p2, Lcom/twitter/library/provider/Tweet;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/qe;->a(Landroid/view/View;Lcom/twitter/library/provider/Tweet;Landroid/os/Bundle;)V

    return-void
.end method
