.class Lcom/twitter/android/eg;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;


# instance fields
.field final synthetic a:Lcom/twitter/android/DMInboxFragment;


# direct methods
.method private constructor <init>(Lcom/twitter/android/DMInboxFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/eg;->a:Lcom/twitter/android/DMInboxFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/DMInboxFragment;Lcom/twitter/android/ec;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/eg;-><init>(Lcom/twitter/android/DMInboxFragment;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 7

    const v6, 0x7f090124    # com.twitter.android.R.id.import_contacts_btn

    const v5, 0x7f090123    # com.twitter.android.R.id.scan_contacts_desc

    const v4, 0x7f090120    # com.twitter.android.R.id.scan_contacts_view

    const/16 v3, 0x8

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/eg;->a:Lcom/twitter/android/DMInboxFragment;

    iget-object v0, v0, Lcom/twitter/android/DMInboxFragment;->a:Lcom/twitter/android/eh;

    invoke-virtual {v0, p2}, Lcom/twitter/android/eh;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    iget-object v0, p0, Lcom/twitter/android/eg;->a:Lcom/twitter/android/DMInboxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/DMInboxFragment;->getView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/eg;->a:Lcom/twitter/android/DMInboxFragment;

    iget-object v1, v1, Lcom/twitter/android/DMInboxFragment;->a:Lcom/twitter/android/eh;

    invoke-virtual {v1}, Lcom/twitter/android/eh;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/eg;->a:Lcom/twitter/android/DMInboxFragment;

    invoke-virtual {v1}, Lcom/twitter/android/DMInboxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0118    # com.twitter.android.R.string.dm_empty_follows_empty_callout_text

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/eg;->a:Lcom/twitter/android/DMInboxFragment;

    invoke-virtual {v1}, Lcom/twitter/android/DMInboxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f0116    # com.twitter.android.R.string.dm_empty_callout_text

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7

    const/4 v4, 0x0

    new-instance v0, Landroid/support/v4/content/CursorLoader;

    iget-object v1, p0, Lcom/twitter/android/eg;->a:Lcom/twitter/android/DMInboxFragment;

    invoke-virtual {v1}, Lcom/twitter/android/DMInboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/twitter/android/provider/SuggestionsProvider;->e:Landroid/net/Uri;

    sget-object v3, Lcom/twitter/android/provider/m;->a:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/eg;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/eg;->a:Lcom/twitter/android/DMInboxFragment;

    iget-object v0, v0, Lcom/twitter/android/DMInboxFragment;->a:Lcom/twitter/android/eh;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/eh;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    return-void
.end method
