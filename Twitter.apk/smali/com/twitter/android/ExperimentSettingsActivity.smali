.class public Lcom/twitter/android/ExperimentSettingsActivity;
.super Lcom/twitter/android/client/BasePreferenceActivity;
.source "Twttr"


# instance fields
.field private a:Landroid/content/res/Resources;

.field private b:Landroid/preference/PreferenceCategory;

.field private c:Landroid/content/SharedPreferences;

.field private d:Landroid/content/SharedPreferences;

.field private e:J

.field private f:Lcom/twitter/android/fu;

.field private g:Lcom/twitter/android/fv;

.field private h:Lcom/twitter/android/fx;

.field private i:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/client/BasePreferenceActivity;-><init>()V

    new-instance v0, Lcom/twitter/android/fu;

    invoke-direct {v0, p0}, Lcom/twitter/android/fu;-><init>(Lcom/twitter/android/ExperimentSettingsActivity;)V

    iput-object v0, p0, Lcom/twitter/android/ExperimentSettingsActivity;->f:Lcom/twitter/android/fu;

    new-instance v0, Lcom/twitter/android/fv;

    invoke-direct {v0, p0}, Lcom/twitter/android/fv;-><init>(Lcom/twitter/android/ExperimentSettingsActivity;)V

    iput-object v0, p0, Lcom/twitter/android/ExperimentSettingsActivity;->g:Lcom/twitter/android/fv;

    return-void
.end method

.method private a(Ljava/lang/String;[Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Landroid/preference/ListPreference;
    .locals 5

    const/4 v4, 0x0

    new-instance v1, Landroid/preference/ListPreference;

    invoke-direct {v1, p0}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;)V

    const-string/jumbo v0, "all"

    invoke-virtual {p4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0f03e8    # com.twitter.android.R.string.settings_lo_experiment_current_bucket_summary

    :goto_0
    invoke-virtual {v1, p1}, Landroid/preference/ListPreference;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, p1}, Landroid/preference/ListPreference;->setDialogTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, p1}, Landroid/preference/ListPreference;->setKey(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    invoke-virtual {v1, p2}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    invoke-virtual {v1, p3}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/twitter/android/ExperimentSettingsActivity;->a:Landroid/content/res/Resources;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p3, v3, v4

    invoke-virtual {v2, v0, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v4}, Landroid/preference/ListPreference;->setPersistent(Z)V

    iget-object v0, p0, Lcom/twitter/android/ExperimentSettingsActivity;->f:Lcom/twitter/android/fu;

    invoke-virtual {v1, v0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    return-object v1

    :cond_0
    const v0, 0x7f0f03d7    # com.twitter.android.R.string.settings_experiment_current_bucket_summary

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/ExperimentSettingsActivity;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ExperimentSettingsActivity;->i:Ljava/util/ArrayList;

    return-object v0
.end method

.method private a(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/Bucket;

    iget-object v0, v0, Lcom/twitter/library/api/Bucket;->name:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const-string/jumbo v0, "unassigned"

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-object v1
.end method

.method private a()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/ExperimentSettingsActivity;->c:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "experiment_search"

    const-string/jumbo v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v0, "experiment_search"

    invoke-virtual {p0, v0}, Lcom/twitter/android/ExperimentSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    const v1, 0x7f0f03db    # com.twitter.android.R.string.settings_experiment_search_default_title

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(I)V

    :goto_0
    return-void

    :cond_0
    const-string/jumbo v1, "experiment_search"

    invoke-virtual {p0, v1}, Lcom/twitter/android/ExperimentSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private b()V
    .locals 8

    iget-object v0, p0, Lcom/twitter/android/ExperimentSettingsActivity;->b:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->removeAll()V

    iget-wide v0, p0, Lcom/twitter/android/ExperimentSettingsActivity;->e:J

    invoke-static {v0, v1}, Lju;->b(J)Ljava/util/HashMap;

    move-result-object v0

    const-wide/16 v1, 0x0

    const-string/jumbo v3, "all"

    invoke-static {v1, v2, v3}, Lju;->a(JLjava/lang/String;)Ljava/util/HashMap;

    move-result-object v3

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    iget-object v0, p0, Lcom/twitter/android/ExperimentSettingsActivity;->c:Landroid/content/SharedPreferences;

    const-string/jumbo v2, "experiment_search"

    const-string/jumbo v4, ""

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/Experiment;

    iget-object v1, v0, Lcom/twitter/library/api/Experiment;->key:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v0, v0, Lcom/twitter/library/api/Experiment;->key:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/Experiment;

    move-object v1, v0

    :goto_1
    iget-object v0, v1, Lcom/twitter/library/api/Experiment;->key:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, v1, Lcom/twitter/library/api/Experiment;->buckets:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/twitter/android/ExperimentSettingsActivity;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v6

    iget-object v0, v1, Lcom/twitter/library/api/Experiment;->bucketName:Ljava/lang/String;

    if-nez v0, :cond_3

    const-string/jumbo v0, "unassigned"

    move-object v2, v0

    :goto_2
    iget-object v7, v1, Lcom/twitter/library/api/Experiment;->key:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/CharSequence;

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/CharSequence;

    iget-object v1, v1, Lcom/twitter/library/api/Experiment;->experimentType:Ljava/lang/String;

    invoke-direct {p0, v7, v0, v2, v1}, Lcom/twitter/android/ExperimentSettingsActivity;->a(Ljava/lang/String;[Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Landroid/preference/ListPreference;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ExperimentSettingsActivity;->b:Landroid/preference/PreferenceCategory;

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    goto :goto_0

    :cond_3
    iget-object v0, v1, Lcom/twitter/library/api/Experiment;->bucketName:Ljava/lang/String;

    move-object v2, v0

    goto :goto_2

    :cond_4
    move-object v1, v0

    goto :goto_1
.end method

.method static synthetic b(Lcom/twitter/android/ExperimentSettingsActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/ExperimentSettingsActivity;->b()V

    return-void
.end method

.method static synthetic c(Lcom/twitter/android/ExperimentSettingsActivity;)J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/android/ExperimentSettingsActivity;->e:J

    return-wide v0
.end method

.method static synthetic d(Lcom/twitter/android/ExperimentSettingsActivity;)Landroid/content/res/Resources;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ExperimentSettingsActivity;->a:Landroid/content/res/Resources;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/ExperimentSettingsActivity;)Landroid/content/SharedPreferences;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ExperimentSettingsActivity;->d:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/ExperimentSettingsActivity;)Lcom/twitter/android/client/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ExperimentSettingsActivity;->Q:Lcom/twitter/android/client/c;

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/android/ExperimentSettingsActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/ExperimentSettingsActivity;->d()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/android/ExperimentSettingsActivity;)Lcom/twitter/android/client/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ExperimentSettingsActivity;->Q:Lcom/twitter/android/client/c;

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/android/ExperimentSettingsActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/ExperimentSettingsActivity;->a()V

    return-void
.end method


# virtual methods
.method protected a(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/ExperimentSettingsActivity;->i:Ljava/util/ArrayList;

    new-instance v1, Lcom/twitter/android/client/PendingRequest;

    invoke-direct {v1, p1}, Lcom/twitter/android/client/PendingRequest;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method protected b(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 4

    iget-object v2, p0, Lcom/twitter/android/ExperimentSettingsActivity;->i:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/PendingRequest;

    iget-object v0, v0, Lcom/twitter/android/client/PendingRequest;->a:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/PendingRequest;

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Lcom/twitter/android/client/BasePreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    if-nez p1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/android/ExperimentSettingsActivity;->i:Ljava/util/ArrayList;

    :goto_0
    const v0, 0x7f0f03df    # com.twitter.android.R.string.settings_experiment_title

    invoke-virtual {p0, v0}, Lcom/twitter/android/ExperimentSettingsActivity;->setTitle(I)V

    const v0, 0x7f06000a    # com.twitter.android.R.xml.experiment_preferences

    invoke-virtual {p0, v0}, Lcom/twitter/android/ExperimentSettingsActivity;->addPreferencesFromResource(I)V

    invoke-virtual {p0}, Lcom/twitter/android/ExperimentSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ExperimentSettingsActivity;->a:Landroid/content/res/Resources;

    invoke-virtual {p0}, Lcom/twitter/android/ExperimentSettingsActivity;->d()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/ExperimentSettingsActivity;->e:J

    invoke-virtual {p0}, Lcom/twitter/android/ExperimentSettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ExperimentSettingsActivity;->c:Landroid/content/SharedPreferences;

    const-string/jumbo v0, "disable"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/android/ExperimentSettingsActivity;->e:J

    invoke-static {p0, v2, v3}, Lju;->c(Landroid/content/Context;J)Z

    move-result v2

    if-nez v2, :cond_1

    const v2, 0x7f0f03d5    # com.twitter.android.R.string.settings_experiement_disabled

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setTitle(I)V

    :goto_1
    const-string/jumbo v0, "abd_buckets"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/twitter/android/ExperimentSettingsActivity;->b:Landroid/preference/PreferenceCategory;

    const-string/jumbo v0, "abd"

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Lcom/twitter/android/ExperimentSettingsActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ExperimentSettingsActivity;->d:Landroid/content/SharedPreferences;

    new-instance v0, Lcom/twitter/android/fx;

    invoke-direct {v0, p0}, Lcom/twitter/android/fx;-><init>(Lcom/twitter/android/ExperimentSettingsActivity;)V

    iput-object v0, p0, Lcom/twitter/android/ExperimentSettingsActivity;->h:Lcom/twitter/android/fx;

    const-string/jumbo v0, "reset"

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/fw;

    invoke-direct {v1, p0}, Lcom/twitter/android/fw;-><init>(Lcom/twitter/android/ExperimentSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    iget-object v0, p0, Lcom/twitter/android/ExperimentSettingsActivity;->Q:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/ExperimentSettingsActivity;->h:Lcom/twitter/android/fx;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/j;)V

    iget-object v0, p0, Lcom/twitter/android/ExperimentSettingsActivity;->c:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/twitter/android/ExperimentSettingsActivity;->g:Lcom/twitter/android/fv;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    invoke-direct {p0}, Lcom/twitter/android/ExperimentSettingsActivity;->a()V

    iget-object v0, p0, Lcom/twitter/android/ExperimentSettingsActivity;->Q:Lcom/twitter/android/client/c;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/client/c;->l(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ExperimentSettingsActivity;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/ExperimentSettingsActivity;->Q:Lcom/twitter/android/client/c;

    iget-wide v1, p0, Lcom/twitter/android/ExperimentSettingsActivity;->e:J

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/client/c;->l(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ExperimentSettingsActivity;->a(Ljava/lang/String;)V

    return-void

    :cond_0
    const-string/jumbo v0, "pending_reqs"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ExperimentSettingsActivity;->i:Ljava/util/ArrayList;

    goto/16 :goto_0

    :cond_1
    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_1
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/client/BasePreferenceActivity;->onPause()V

    iget-object v0, p0, Lcom/twitter/android/ExperimentSettingsActivity;->h:Lcom/twitter/android/fx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ExperimentSettingsActivity;->Q:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/ExperimentSettingsActivity;->h:Lcom/twitter/android/fx;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/j;)V

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/client/BasePreferenceActivity;->onResume()V

    iget-object v0, p0, Lcom/twitter/android/ExperimentSettingsActivity;->h:Lcom/twitter/android/fx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ExperimentSettingsActivity;->Q:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/ExperimentSettingsActivity;->h:Lcom/twitter/android/fx;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/j;)V

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/client/BasePreferenceActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "pending_reqs"

    iget-object v1, p0, Lcom/twitter/android/ExperimentSettingsActivity;->i:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
