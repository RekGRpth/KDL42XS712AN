.class public Lcom/twitter/android/StartOptInActivity;
.super Lcom/twitter/android/client/BaseFragmentActivity;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Z

.field private b:Z

.field private c:Lcom/twitter/library/util/m;

.field private d:Landroid/widget/ImageView;

.field private e:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/StartOptInActivity;)Lcom/twitter/library/util/m;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/StartOptInActivity;->c:Lcom/twitter/library/util/m;

    return-object v0
.end method

.method private a(Landroid/graphics/Bitmap;)V
    .locals 3

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/StartOptInActivity;->d:Landroid/widget/ImageView;

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/twitter/android/StartOptInActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/StartOptInActivity;->d:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/twitter/android/StartOptInActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0031    # com.twitter.android.R.color.dark_gray

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/StartOptInActivity;Landroid/graphics/Bitmap;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/StartOptInActivity;->a(Landroid/graphics/Bitmap;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->c(I)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/z;->a(Z)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/z;->b(Z)V

    const v1, 0x7f030145    # com.twitter.android.R.layout.start_opt_in

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(I)V

    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 11

    const/4 v10, 0x0

    const v9, 0x7f090285    # com.twitter.android.R.id.opt_in_cancel

    const v8, 0x7f090284    # com.twitter.android.R.id.opt_in_confirm

    const/4 v6, 0x0

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V

    invoke-virtual {p0}, Lcom/twitter/android/StartOptInActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/StartOptInActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v0, "extra_should_register_phone"

    invoke-virtual {v2, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/StartOptInActivity;->a:Z

    const-string/jumbo v0, "extra_may_upload_contacts"

    invoke-virtual {v2, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/StartOptInActivity;->b:Z

    const-string/jumbo v0, "extra_flow_steps"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/StartOptInActivity;->e:[Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/StartOptInActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v5, "opt_in::::impression"

    aput-object v5, v0, v6

    invoke-virtual {v1, v3, v4, v0}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    const v0, 0x7f09005a    # com.twitter.android.R.id.toolbar

    invoke-virtual {p0, v0}, Lcom/twitter/android/StartOptInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ToolBar;

    invoke-virtual {v0, v10}, Lcom/twitter/internal/android/widget/ToolBar;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/twitter/android/StartOptInActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0022    # com.twitter.android.R.color.clear

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/twitter/internal/android/widget/ToolBar;->setBackgroundColor(I)V

    invoke-virtual {p0, v8}, Lcom/twitter/android/StartOptInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, v9}, Lcom/twitter/android/StartOptInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const-string/jumbo v0, "extra_title"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v0, "extra_subtitle"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v0, "extra_confirm_text"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v0, "extra_cancel_text"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v0, "extra_header_url"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v0, "extra_header_title"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v3, :cond_0

    const v0, 0x7f090282    # com.twitter.android.R.id.opt_in_title

    invoke-virtual {p0, v0}, Lcom/twitter/android/StartOptInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/TypefacesTextView;

    invoke-virtual {v0, v3}, Lcom/twitter/internal/android/widget/TypefacesTextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v3}, Lcom/twitter/internal/android/widget/TypefacesTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_0
    if-eqz v4, :cond_1

    const v0, 0x7f090283    # com.twitter.android.R.id.opt_in_subtitle

    invoke-virtual {p0, v0}, Lcom/twitter/android/StartOptInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/TypefacesTextView;

    invoke-virtual {v0, v4}, Lcom/twitter/internal/android/widget/TypefacesTextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v4}, Lcom/twitter/internal/android/widget/TypefacesTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_1
    if-eqz v5, :cond_2

    invoke-virtual {p0, v8}, Lcom/twitter/android/StartOptInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_2
    if-eqz v6, :cond_3

    invoke-virtual {p0, v9}, Lcom/twitter/android/StartOptInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_3
    if-eqz v7, :cond_4

    const v0, 0x7f090148    # com.twitter.android.R.id.header_image

    invoke-virtual {p0, v0}, Lcom/twitter/android/StartOptInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/StartOptInActivity;->d:Landroid/widget/ImageView;

    new-instance v0, Lcom/twitter/library/util/m;

    iget v1, v1, Lcom/twitter/android/client/c;->d:F

    invoke-static {v7, v1}, Lcom/twitter/library/util/Util;->a(Ljava/lang/String;F)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/library/util/m;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/android/StartOptInActivity;->c:Lcom/twitter/library/util/m;

    invoke-virtual {p0}, Lcom/twitter/android/StartOptInActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/StartOptInActivity;->c:Lcom/twitter/library/util/m;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/util/m;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/StartOptInActivity;->a(Landroid/graphics/Bitmap;)V

    :cond_4
    if-eqz v2, :cond_5

    const v0, 0x7f090281    # com.twitter.android.R.id.header_title

    invoke-virtual {p0, v0}, Lcom/twitter/android/StartOptInActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/TypefacesTextView;

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/TypefacesTextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/TypefacesTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_5
    new-instance v0, Lcom/twitter/android/tx;

    invoke-direct {v0, p0, v10}, Lcom/twitter/android/tx;-><init>(Lcom/twitter/android/StartOptInActivity;Lcom/twitter/android/tw;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/StartOptInActivity;->a(Lcom/twitter/library/client/j;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 9

    const/4 v5, 0x1

    invoke-static {p0}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;)Lcom/twitter/android/util/d;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    new-instance v2, Lcom/twitter/android/FollowFlowController;

    invoke-direct {v2}, Lcom/twitter/android/FollowFlowController;-><init>()V

    sget-object v3, Lcom/twitter/android/FollowFlowController$Initiator;->a:Lcom/twitter/android/FollowFlowController$Initiator;

    invoke-virtual {v2, v3}, Lcom/twitter/android/FollowFlowController;->a(Lcom/twitter/android/FollowFlowController$Initiator;)Lcom/twitter/android/FollowFlowController;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/twitter/android/FollowFlowController;->a(Z)Lcom/twitter/android/FollowFlowController;

    move-result-object v2

    iget-boolean v3, p0, Lcom/twitter/android/StartOptInActivity;->a:Z

    invoke-virtual {v2, v3}, Lcom/twitter/android/FollowFlowController;->b(Z)Lcom/twitter/android/FollowFlowController;

    move-result-object v2

    iget-boolean v3, p0, Lcom/twitter/android/StartOptInActivity;->b:Z

    invoke-virtual {v2, v3}, Lcom/twitter/android/FollowFlowController;->c(Z)Lcom/twitter/android/FollowFlowController;

    move-result-object v2

    invoke-interface {v1}, Lcom/twitter/android/util/d;->b()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/twitter/android/FollowFlowController;->d(Z)Lcom/twitter/android/FollowFlowController;

    move-result-object v2

    const v3, 0x7f090284    # com.twitter.android.R.id.opt_in_confirm

    if-ne v0, v3, :cond_0

    const-string/jumbo v0, "next"

    sget-object v1, Lcom/twitter/android/FollowFlowController$Initiator;->d:Lcom/twitter/android/FollowFlowController$Initiator;

    invoke-virtual {v2, v1}, Lcom/twitter/android/FollowFlowController;->a(Lcom/twitter/android/FollowFlowController$Initiator;)Lcom/twitter/android/FollowFlowController;

    move-result-object v1

    iget-object v3, p0, Lcom/twitter/android/StartOptInActivity;->e:[Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/twitter/android/FollowFlowController;->a([Ljava/lang/String;)Lcom/twitter/android/FollowFlowController;

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/StartOptInActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/StartOptInActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "opt_in::::"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-virtual {v1, v3, v4, v5}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Lcom/twitter/android/FollowFlowController;->b(Landroid/app/Activity;)V

    return-void

    :cond_0
    const-string/jumbo v0, "cancel"

    sget-object v3, Lcom/twitter/android/FollowFlowController$Initiator;->a:Lcom/twitter/android/FollowFlowController$Initiator;

    invoke-virtual {v2, v3}, Lcom/twitter/android/FollowFlowController;->a(Lcom/twitter/android/FollowFlowController$Initiator;)Lcom/twitter/android/FollowFlowController;

    move-result-object v3

    invoke-interface {v1, v5}, Lcom/twitter/android/util/d;->a(Z)[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/twitter/android/FollowFlowController;->a([Ljava/lang/String;)Lcom/twitter/android/FollowFlowController;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/twitter/android/FollowFlowController;->a(I)Lcom/twitter/android/FollowFlowController;

    goto :goto_0
.end method
