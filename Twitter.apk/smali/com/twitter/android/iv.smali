.class final Lcom/twitter/android/iv;
.super Landroid/content/AsyncQueryHandler;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/ListTabActivity;


# direct methods
.method public constructor <init>(Lcom/twitter/android/ListTabActivity;Landroid/content/Context;)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/android/iv;->a:Lcom/twitter/android/ListTabActivity;

    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    return-void
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 6

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterTopic$TwitterList;

    iget-object v1, p0, Lcom/twitter/android/iv;->a:Lcom/twitter/android/ListTabActivity;

    iget v0, v0, Lcom/twitter/library/api/TwitterTopic$TwitterList;->followStatus:I

    iput v0, v1, Lcom/twitter/android/ListTabActivity;->f:I

    iget-object v0, p0, Lcom/twitter/android/iv;->a:Lcom/twitter/android/ListTabActivity;

    iget v0, v0, Lcom/twitter/android/ListTabActivity;->f:I

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/iv;->a:Lcom/twitter/android/ListTabActivity;

    invoke-virtual {v0}, Lcom/twitter/android/ListTabActivity;->V()V

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/iv;->a:Lcom/twitter/android/ListTabActivity;

    iget v0, v0, Lcom/twitter/android/ListTabActivity;->f:I

    packed-switch v0, :pswitch_data_0

    :cond_1
    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    return-void

    :cond_2
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/iv;->a:Lcom/twitter/android/ListTabActivity;

    invoke-static {v0}, Lcom/twitter/android/ListTabActivity;->a(Lcom/twitter/android/ListTabActivity;)Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/iv;->a:Lcom/twitter/android/ListTabActivity;

    iget-wide v0, v0, Lcom/twitter/android/ListTabActivity;->c:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/iv;->a:Lcom/twitter/android/ListTabActivity;

    invoke-static {v0}, Lcom/twitter/android/ListTabActivity;->b(Lcom/twitter/android/ListTabActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/twitter/android/iv;->a:Lcom/twitter/android/ListTabActivity;

    iget-wide v2, v2, Lcom/twitter/android/ListTabActivity;->b:J

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/client/c;->c(IJJ)Ljava/lang/String;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private b(Landroid/database/Cursor;)V
    .locals 2

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterTopic$TwitterList;

    iget-object v1, p0, Lcom/twitter/android/iv;->a:Lcom/twitter/android/ListTabActivity;

    iget v0, v0, Lcom/twitter/library/api/TwitterTopic$TwitterList;->followStatus:I

    iput v0, v1, Lcom/twitter/android/ListTabActivity;->f:I

    iget-object v0, p0, Lcom/twitter/android/iv;->a:Lcom/twitter/android/ListTabActivity;

    iget v0, v0, Lcom/twitter/android/ListTabActivity;->f:I

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/iv;->a:Lcom/twitter/android/ListTabActivity;

    invoke-virtual {v0}, Lcom/twitter/android/ListTabActivity;->V()V

    :cond_1
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    return-void

    :cond_2
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 1

    if-nez p3, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/iv;->a:Lcom/twitter/android/ListTabActivity;

    invoke-virtual {v0}, Lcom/twitter/android/ListTabActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-direct {p0, p3}, Lcom/twitter/android/iv;->a(Landroid/database/Cursor;)V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, p3}, Lcom/twitter/android/iv;->b(Landroid/database/Cursor;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
