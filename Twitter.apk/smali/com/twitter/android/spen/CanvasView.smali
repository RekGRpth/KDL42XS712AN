.class public Lcom/twitter/android/spen/CanvasView;
.super Landroid/widget/FrameLayout;
.source "Twttr"


# instance fields
.field private a:Landroid/net/Uri;

.field private b:I

.field private c:Z

.field private d:Lcom/twitter/android/spen/f;

.field private e:Z

.field private f:I

.field private g:I

.field private h:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

.field private i:Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

.field private j:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

.field private k:Z

.field private l:Lcom/twitter/android/spen/g;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/spen/CanvasView;->e:Z

    invoke-direct {p0}, Lcom/twitter/android/spen/CanvasView;->e()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/spen/CanvasView;->e:Z

    invoke-direct {p0}, Lcom/twitter/android/spen/CanvasView;->e()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/spen/CanvasView;->e:Z

    invoke-direct {p0}, Lcom/twitter/android/spen/CanvasView;->e()V

    return-void
.end method

.method private static a(FFFF)F
    .locals 3

    const/high16 v0, 0x3f800000    # 1.0f

    div-float v1, p2, p0

    div-float v2, p3, p1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    cmpl-float v2, v1, v0

    if-lez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private a(I)Landroid/graphics/Point;
    .locals 5

    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v3, "window"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v0, p0, Lcom/twitter/android/spen/CanvasView;->g:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/android/spen/CanvasView;->f:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/android/spen/CanvasView;->g:I

    iget v3, p0, Lcom/twitter/android/spen/CanvasView;->f:I

    if-ne v0, v3, :cond_1

    iget v0, p0, Lcom/twitter/android/spen/CanvasView;->g:I

    if-le v0, p1, :cond_1

    :cond_0
    iput p1, v1, Landroid/graphics/Point;->x:I

    iput p1, v1, Landroid/graphics/Point;->y:I

    :goto_0
    return-object v1

    :cond_1
    iget v0, p0, Lcom/twitter/android/spen/CanvasView;->g:I

    int-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, v1, Landroid/graphics/Point;->x:I

    iget v0, p0, Lcom/twitter/android/spen/CanvasView;->f:I

    int-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, v1, Landroid/graphics/Point;->y:I

    iget v0, p0, Lcom/twitter/android/spen/CanvasView;->g:I

    int-to-float v0, v0

    iget v3, p0, Lcom/twitter/android/spen/CanvasView;->f:I

    int-to-float v3, v3

    iget v2, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasView;->getMeasuredHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-static {v0, v3, v2, v4}, Lcom/twitter/android/spen/CanvasView;->a(FFFF)F

    move-result v0

    iget v2, p0, Lcom/twitter/android/spen/CanvasView;->g:I

    int-to-float v2, v2

    mul-float/2addr v2, v0

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iput v2, v1, Landroid/graphics/Point;->x:I

    iget v2, p0, Lcom/twitter/android/spen/CanvasView;->f:I

    int-to-float v2, v2

    mul-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, v1, Landroid/graphics/Point;->y:I

    const/high16 v0, 0x3f800000    # 1.0f

    iget v2, v1, Landroid/graphics/Point;->x:I

    iget v3, v1, Landroid/graphics/Point;->y:I

    mul-int/2addr v2, v3

    const/16 v3, 0x9c4

    if-ge v2, v3, :cond_2

    const v0, 0x451c4000    # 2500.0f

    iget v2, v1, Landroid/graphics/Point;->x:I

    iget v3, v1, Landroid/graphics/Point;->y:I

    mul-int/2addr v2, v3

    int-to-float v2, v2

    div-float/2addr v0, v2

    :cond_2
    iget v2, v1, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    mul-float/2addr v2, v0

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iput v2, v1, Landroid/graphics/Point;->x:I

    iget v2, v1, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    mul-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, v1, Landroid/graphics/Point;->y:I

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/spen/CanvasView;Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;)Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/spen/CanvasView;->i:Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    return-object p1
.end method

.method static synthetic a(Lcom/twitter/android/spen/CanvasView;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/spen/CanvasView;->j:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    return-object p1
.end method

.method static synthetic a(Lcom/twitter/android/spen/CanvasView;)Lcom/twitter/android/spen/f;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasView;->d:Lcom/twitter/android/spen/f;

    return-object v0
.end method

.method private a(Ljava/io/File;Landroid/graphics/Bitmap;)Z
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x64

    invoke-virtual {p2, v0, v2, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const/4 v0, 0x1

    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    :goto_0
    return v0

    :catch_0
    move-exception v1

    :goto_1
    invoke-static {v0}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_2
    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_2

    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_1
.end method

.method static synthetic b(Landroid/content/Context;Landroid/net/Uri;IZ)Landroid/graphics/Bitmap;
    .locals 1

    invoke-static {p0, p1, p2, p3}, Lcom/twitter/android/spen/CanvasView;->c(Landroid/content/Context;Landroid/net/Uri;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/spen/CanvasView;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/spen/CanvasView;->e:Z

    return v0
.end method

.method private static c(Landroid/content/Context;Landroid/net/Uri;IZ)Landroid/graphics/Bitmap;
    .locals 3

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {p0, p1}, Lkw;->a(Landroid/content/Context;Landroid/net/Uri;)Lkw;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Lkw;->b(II)Lkw;

    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v2, v0}, Lkw;->a(Landroid/graphics/Bitmap$Config;)Lkw;

    invoke-virtual {v2}, Lkw;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-static {p0}, Lcom/twitter/media/filters/c;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    :try_start_0
    invoke-static {p0, p1, v0, p2, p3}, Lcom/twitter/media/filters/c;->a(Landroid/content/Context;Landroid/net/Uri;Landroid/graphics/Bitmap;IZ)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-static {v1}, Lcom/crashlytics/android/d;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/twitter/android/spen/CanvasView;)Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasView;->a:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/spen/CanvasView;)I
    .locals 1

    iget v0, p0, Lcom/twitter/android/spen/CanvasView;->b:I

    return v0
.end method

.method private e()V
    .locals 1

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/twitter/android/spen/CanvasView;->setVisibility(I)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/spen/CanvasView;->setEnabled(Z)V

    return-void
.end method

.method static synthetic e(Lcom/twitter/android/spen/CanvasView;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/spen/CanvasView;->c:Z

    return v0
.end method

.method private f()V
    .locals 2

    new-instance v0, Lcom/twitter/android/spen/e;

    invoke-direct {v0, p0}, Lcom/twitter/android/spen/e;-><init>(Lcom/twitter/android/spen/CanvasView;)V

    iget-object v1, p0, Lcom/twitter/android/spen/CanvasView;->j:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->setHistoryListener(Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryListener;)V

    return-void
.end method

.method static synthetic f(Lcom/twitter/android/spen/CanvasView;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/spen/CanvasView;->k:Z

    return v0
.end method

.method static synthetic g(Lcom/twitter/android/spen/CanvasView;)Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasView;->i:Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    return-object v0
.end method

.method private g()Ljava/lang/String;
    .locals 2

    invoke-direct {p0}, Lcom/twitter/android/spen/CanvasView;->getTempNoteFile()Ljava/io/File;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/spen/CanvasView;->i:Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/twitter/android/spen/CanvasView;->i:Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->save(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method private getTempNoteFile()Ljava/io/File;
    .locals 3

    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v1

    const-string/jumbo v2, "temp_note.sam"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/android/spen/CanvasView;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasView;->j:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/android/spen/CanvasView;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/spen/CanvasView;->f()V

    return-void
.end method

.method static synthetic j(Lcom/twitter/android/spen/CanvasView;)Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasView;->h:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/spen/CanvasView;->h:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasView;->h:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasView;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasView;->getMeasuredHeight()I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasView;->j:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/spen/CanvasView;->k:Z

    if-nez v0, :cond_2

    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasView;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasView;->getMeasuredHeight()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;-><init>(Landroid/content/Context;II)V

    iput-object v0, p0, Lcom/twitter/android/spen/CanvasView;->i:Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasView;->i:Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->appendPage()Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/spen/CanvasView;->j:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasView;->j:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasView;->j:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->clearHistory()V

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/spen/CanvasView;->h:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    iget-object v1, p0, Lcom/twitter/android/spen/CanvasView;->j:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v0, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setPageDoc(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z

    invoke-direct {p0}, Lcom/twitter/android/spen/CanvasView;->f()V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/spen/CanvasView;->h:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v7, v7, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setZoom(FFF)V

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasView;->h:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->update()V

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasView;->a:Landroid/net/Uri;

    if-eqz v0, :cond_4

    new-instance v0, Lcom/twitter/android/spen/g;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/spen/g;-><init>(Lcom/twitter/android/spen/CanvasView;Lcom/twitter/android/spen/e;)V

    iput-object v0, p0, Lcom/twitter/android/spen/CanvasView;->l:Lcom/twitter/android/spen/g;

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasView;->l:Lcom/twitter/android/spen/g;

    new-array v1, v5, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/spen/g;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_1
    iget-object v0, p0, Lcom/twitter/android/spen/CanvasView;->d:Lcom/twitter/android/spen/f;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasView;->d:Lcom/twitter/android/spen/f;

    invoke-interface {v0}, Lcom/twitter/android/spen/f;->a()V

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/spen/CanvasView;->h:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    invoke-virtual {p0, v0}, Lcom/twitter/android/spen/CanvasView;->addView(Landroid/view/View;)V

    return-void

    :cond_2
    invoke-direct {p0}, Lcom/twitter/android/spen/CanvasView;->getTempNoteFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasView;->getMeasuredWidth()I

    move-result v4

    invoke-direct {v1, v2, v3, v4, v6}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;-><init>(Landroid/content/Context;Ljava/lang/String;II)V

    iput-object v1, p0, Lcom/twitter/android/spen/CanvasView;->i:Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    iget-object v1, p0, Lcom/twitter/android/spen/CanvasView;->i:Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    invoke-virtual {v1, v5}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->getPage(I)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/spen/CanvasView;->j:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    :cond_3
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    goto :goto_0

    :cond_4
    invoke-virtual {p0, v5}, Lcom/twitter/android/spen/CanvasView;->setVisibility(I)V

    invoke-virtual {p0, v6}, Lcom/twitter/android/spen/CanvasView;->setEnabled(Z)V

    goto :goto_1
.end method

.method public a(IIII)V
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;-><init>()V

    int-to-float v1, p3

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    iput v3, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->type:I

    new-instance v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-direct {v1}, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;-><init>()V

    iput p4, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    int-to-float v2, p2

    iput v2, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    iget-object v2, p0, Lcom/twitter/android/spen/CanvasView;->h:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    invoke-virtual {v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    iget-object v1, p0, Lcom/twitter/android/spen/CanvasView;->h:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setEraserSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasView;->h:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setToolTypeAction(II)V

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasView;->h:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setToolTypeAction(II)V

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasView;->h:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setToolTypeAction(II)V

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasView;->h:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    invoke-virtual {v0, v3, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setToolTypeAction(II)V

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasView;->h:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    const/4 v1, 0x4

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setToolTypeAction(II)V

    return-void
.end method

.method public a(Landroid/content/Context;Landroid/net/Uri;IZ)V
    .locals 2

    iput-object p2, p0, Lcom/twitter/android/spen/CanvasView;->a:Landroid/net/Uri;

    iput p3, p0, Lcom/twitter/android/spen/CanvasView;->b:I

    iput-boolean p4, p0, Lcom/twitter/android/spen/CanvasView;->c:Z

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasView;->a:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasView;->a:Landroid/net/Uri;

    invoke-static {p1, v0}, Lkw;->a(Landroid/content/Context;Landroid/net/Uri;)Lkw;

    move-result-object v0

    invoke-virtual {v0}, Lkw;->c()Landroid/graphics/Rect;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    iput v1, p0, Lcom/twitter/android/spen/CanvasView;->g:I

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/spen/CanvasView;->f:I

    :cond_0
    return-void
.end method

.method public a(Ljava/io/File;)Z
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/twitter/android/spen/CanvasView;->h:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->captureCurrentView(Z)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-direct {p0, p1, v1}, Lcom/twitter/android/spen/CanvasView;->a(Ljava/io/File;Landroid/graphics/Bitmap;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v1

    invoke-static {v1}, Lcom/crashlytics/android/d;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public b()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/spen/CanvasView;->e:Z

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasView;->h:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasView;->h:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->close()V

    iput-object v1, p0, Lcom/twitter/android/spen/CanvasView;->h:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/spen/CanvasView;->i:Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/spen/CanvasView;->i:Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/spen/CanvasView;->i:Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public c()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasView;->j:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->removeAllObject()V

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasView;->j:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->clearHistory()V

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasView;->h:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->update()V

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasView;->d:Lcom/twitter/android/spen/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasView;->d:Lcom/twitter/android/spen/f;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/twitter/android/spen/f;->a(Z)V

    :cond_0
    return-void
.end method

.method public d()V
    .locals 2

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/twitter/android/spen/CanvasView;->e:Z

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasView;->l:Lcom/twitter/android/spen/g;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasView;->l:Lcom/twitter/android/spen/g;

    invoke-virtual {v0, v1}, Lcom/twitter/android/spen/g;->cancel(Z)Z

    :cond_0
    return-void
.end method

.method public getListener()Lcom/twitter/android/spen/f;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasView;->d:Lcom/twitter/android/spen/f;

    return-object v0
.end method

.method protected onMeasure(II)V
    .locals 2

    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/twitter/android/spen/CanvasView;->a(I)Landroid/graphics/Point;

    move-result-object v0

    iget v1, v0, Landroid/graphics/Point;->x:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/spen/CanvasView;->setMeasuredDimension(II)V

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    invoke-direct {p0}, Lcom/twitter/android/spen/CanvasView;->getTempNoteFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/spen/CanvasView;->k:Z

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/spen/CanvasView;->g()Ljava/lang/String;

    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method

.method public setListener(Lcom/twitter/android/spen/f;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/spen/CanvasView;->d:Lcom/twitter/android/spen/f;

    return-void
.end method
