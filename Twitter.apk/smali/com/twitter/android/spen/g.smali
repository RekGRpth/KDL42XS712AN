.class Lcom/twitter/android/spen/g;
.super Landroid/os/AsyncTask;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/spen/CanvasView;


# direct methods
.method private constructor <init>(Lcom/twitter/android/spen/CanvasView;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/spen/g;->a:Lcom/twitter/android/spen/CanvasView;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/spen/CanvasView;Lcom/twitter/android/spen/e;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/spen/g;-><init>(Lcom/twitter/android/spen/CanvasView;)V

    return-void
.end method

.method private a()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/spen/g;->a:Lcom/twitter/android/spen/CanvasView;

    invoke-virtual {v0}, Lcom/twitter/android/spen/CanvasView;->getListener()Lcom/twitter/android/spen/f;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/spen/g;->a:Lcom/twitter/android/spen/CanvasView;

    new-instance v2, Lcom/twitter/android/spen/h;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/spen/h;-><init>(Lcom/twitter/android/spen/g;Lcom/twitter/android/spen/f;)V

    invoke-virtual {v1, v2}, Lcom/twitter/android/spen/CanvasView;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 7

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/twitter/android/spen/g;->a:Lcom/twitter/android/spen/CanvasView;

    invoke-static {v0}, Lcom/twitter/android/spen/CanvasView;->b(Lcom/twitter/android/spen/CanvasView;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-object v6

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/spen/g;->a:Lcom/twitter/android/spen/CanvasView;

    invoke-virtual {v0}, Lcom/twitter/android/spen/CanvasView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/spen/g;->a:Lcom/twitter/android/spen/CanvasView;

    invoke-static {v1}, Lcom/twitter/android/spen/CanvasView;->c(Lcom/twitter/android/spen/CanvasView;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/spen/g;->a:Lcom/twitter/android/spen/CanvasView;

    invoke-static {v2}, Lcom/twitter/android/spen/CanvasView;->d(Lcom/twitter/android/spen/CanvasView;)I

    move-result v2

    iget-object v3, p0, Lcom/twitter/android/spen/g;->a:Lcom/twitter/android/spen/CanvasView;

    invoke-static {v3}, Lcom/twitter/android/spen/CanvasView;->e(Lcom/twitter/android/spen/CanvasView;)Z

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/android/spen/CanvasView;->b(Landroid/content/Context;Landroid/net/Uri;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/twitter/android/spen/g;->a()V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/twitter/android/spen/g;->a:Lcom/twitter/android/spen/CanvasView;

    invoke-static {v1}, Lcom/twitter/android/spen/CanvasView;->b(Lcom/twitter/android/spen/CanvasView;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/spen/g;->a:Lcom/twitter/android/spen/CanvasView;

    invoke-virtual {v1}, Lcom/twitter/android/spen/CanvasView;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lcom/twitter/android/spen/g;->a:Lcom/twitter/android/spen/CanvasView;

    invoke-virtual {v2}, Lcom/twitter/android/spen/CanvasView;->getMeasuredHeight()I

    move-result v2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/spen/g;->a:Lcom/twitter/android/spen/CanvasView;

    invoke-static {v1}, Lcom/twitter/android/spen/CanvasView;->f(Lcom/twitter/android/spen/CanvasView;)Z

    move-result v1

    if-nez v1, :cond_3

    :try_start_0
    iget-object v1, p0, Lcom/twitter/android/spen/g;->a:Lcom/twitter/android/spen/CanvasView;

    new-instance v2, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    iget-object v3, p0, Lcom/twitter/android/spen/g;->a:Lcom/twitter/android/spen/CanvasView;

    invoke-virtual {v3}, Lcom/twitter/android/spen/CanvasView;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/spen/g;->a:Lcom/twitter/android/spen/CanvasView;

    invoke-virtual {v4}, Lcom/twitter/android/spen/CanvasView;->getMeasuredWidth()I

    move-result v4

    iget-object v5, p0, Lcom/twitter/android/spen/g;->a:Lcom/twitter/android/spen/CanvasView;

    invoke-virtual {v5}, Lcom/twitter/android/spen/CanvasView;->getMeasuredHeight()I

    move-result v5

    invoke-direct {v2, v3, v4, v5}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;-><init>(Landroid/content/Context;II)V

    invoke-static {v1, v2}, Lcom/twitter/android/spen/CanvasView;->a(Lcom/twitter/android/spen/CanvasView;Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;)Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v1, p0, Lcom/twitter/android/spen/g;->a:Lcom/twitter/android/spen/CanvasView;

    iget-object v2, p0, Lcom/twitter/android/spen/g;->a:Lcom/twitter/android/spen/CanvasView;

    invoke-static {v2}, Lcom/twitter/android/spen/CanvasView;->g(Lcom/twitter/android/spen/CanvasView;)Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenNoteDoc;->appendPage()Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/twitter/android/spen/CanvasView;->a(Lcom/twitter/android/spen/CanvasView;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    :cond_3
    iget-object v1, p0, Lcom/twitter/android/spen/g;->a:Lcom/twitter/android/spen/CanvasView;

    invoke-static {v1}, Lcom/twitter/android/spen/CanvasView;->h(Lcom/twitter/android/spen/CanvasView;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->setVolatileBackgroundImage(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/twitter/android/spen/g;->a:Lcom/twitter/android/spen/CanvasView;

    invoke-static {v0}, Lcom/twitter/android/spen/CanvasView;->h(Lcom/twitter/android/spen/CanvasView;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->setBackgroundImageMode(I)V

    iget-object v0, p0, Lcom/twitter/android/spen/g;->a:Lcom/twitter/android/spen/CanvasView;

    invoke-static {v0}, Lcom/twitter/android/spen/CanvasView;->h(Lcom/twitter/android/spen/CanvasView;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->clearHistory()V

    iget-object v0, p0, Lcom/twitter/android/spen/g;->a:Lcom/twitter/android/spen/CanvasView;

    invoke-static {v0}, Lcom/twitter/android/spen/CanvasView;->i(Lcom/twitter/android/spen/CanvasView;)V

    iget-object v0, p0, Lcom/twitter/android/spen/g;->a:Lcom/twitter/android/spen/CanvasView;

    invoke-static {v0}, Lcom/twitter/android/spen/CanvasView;->b(Lcom/twitter/android/spen/CanvasView;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/spen/g;->a:Lcom/twitter/android/spen/CanvasView;

    invoke-static {v0}, Lcom/twitter/android/spen/CanvasView;->j(Lcom/twitter/android/spen/CanvasView;)Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/spen/g;->a:Lcom/twitter/android/spen/CanvasView;

    invoke-static {v1}, Lcom/twitter/android/spen/CanvasView;->h(Lcom/twitter/android/spen/CanvasView;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setPageDoc(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z

    goto/16 :goto_0

    :catch_0
    move-exception v0

    invoke-direct {p0}, Lcom/twitter/android/spen/g;->a()V

    goto/16 :goto_0
.end method

.method protected a(Ljava/lang/Void;)V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/android/spen/g;->a:Lcom/twitter/android/spen/CanvasView;

    invoke-static {v0}, Lcom/twitter/android/spen/CanvasView;->b(Lcom/twitter/android/spen/CanvasView;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/spen/g;->a:Lcom/twitter/android/spen/CanvasView;

    invoke-static {v0}, Lcom/twitter/android/spen/CanvasView;->j(Lcom/twitter/android/spen/CanvasView;)Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2, v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setZoom(FFF)V

    iget-object v0, p0, Lcom/twitter/android/spen/g;->a:Lcom/twitter/android/spen/CanvasView;

    invoke-static {v0}, Lcom/twitter/android/spen/CanvasView;->j(Lcom/twitter/android/spen/CanvasView;)Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->update()V

    iget-object v0, p0, Lcom/twitter/android/spen/g;->a:Lcom/twitter/android/spen/CanvasView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/spen/CanvasView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/spen/g;->a:Lcom/twitter/android/spen/CanvasView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/spen/CanvasView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/twitter/android/spen/g;->a:Lcom/twitter/android/spen/CanvasView;

    invoke-virtual {v0}, Lcom/twitter/android/spen/CanvasView;->getListener()Lcom/twitter/android/spen/f;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/spen/g;->a:Lcom/twitter/android/spen/CanvasView;

    invoke-virtual {v0}, Lcom/twitter/android/spen/CanvasView;->getListener()Lcom/twitter/android/spen/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/android/spen/f;->b()V

    :cond_0
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/spen/g;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/spen/g;->a(Ljava/lang/Void;)V

    return-void
.end method
