.class Lcom/twitter/android/spen/a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/spen/CanvasActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/spen/CanvasActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/spen/a;->a:Lcom/twitter/android/spen/CanvasActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/spen/a;->a:Lcom/twitter/android/spen/CanvasActivity;

    invoke-static {v0}, Lcom/twitter/android/spen/CanvasActivity;->a(Lcom/twitter/android/spen/CanvasActivity;)Lcom/twitter/android/spen/CanvasView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/spen/CanvasView;->a()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/twitter/android/spen/a;->a:Lcom/twitter/android/spen/CanvasActivity;

    invoke-static {v0}, Lcom/twitter/android/spen/CanvasActivity;->a(Lcom/twitter/android/spen/CanvasActivity;)Lcom/twitter/android/spen/CanvasView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/spen/CanvasView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {v0}, Lcom/crashlytics/android/d;->a(Ljava/lang/Throwable;)V

    iget-object v0, p0, Lcom/twitter/android/spen/a;->a:Lcom/twitter/android/spen/CanvasActivity;

    const v1, 0x7f0f0223    # com.twitter.android.R.string.load_spen_canvas_failure

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/twitter/android/spen/a;->a:Lcom/twitter/android/spen/CanvasActivity;

    const/16 v1, -0xb

    invoke-virtual {v0, v1}, Lcom/twitter/android/spen/CanvasActivity;->setResult(I)V

    iget-object v0, p0, Lcom/twitter/android/spen/a;->a:Lcom/twitter/android/spen/CanvasActivity;

    invoke-virtual {v0}, Lcom/twitter/android/spen/CanvasActivity;->finish()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lcom/twitter/android/spen/a;->a:Lcom/twitter/android/spen/CanvasActivity;

    invoke-static {v0}, Lcom/twitter/android/spen/CanvasActivity;->a(Lcom/twitter/android/spen/CanvasActivity;)Lcom/twitter/android/spen/CanvasView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/spen/CanvasView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/twitter/android/spen/a;->a:Lcom/twitter/android/spen/CanvasActivity;

    invoke-static {v1}, Lcom/twitter/android/spen/CanvasActivity;->a(Lcom/twitter/android/spen/CanvasActivity;)Lcom/twitter/android/spen/CanvasView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/spen/CanvasView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    throw v0
.end method
