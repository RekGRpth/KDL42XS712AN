.class public Lcom/twitter/android/spen/StrokeSizeView;
.super Landroid/view/View;
.source "Twttr"


# static fields
.field private static a:Landroid/graphics/Paint;


# instance fields
.field private b:I

.field private c:I

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lcom/twitter/android/spen/StrokeSizeView;->a:Landroid/graphics/Paint;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-direct {p0}, Lcom/twitter/android/spen/StrokeSizeView;->a()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0}, Lcom/twitter/android/spen/StrokeSizeView;->a()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0}, Lcom/twitter/android/spen/StrokeSizeView;->a()V

    return-void
.end method

.method private a()V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/spen/StrokeSizeView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0021    # com.twitter.android.R.color.canvas_stroke_view_border

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/spen/StrokeSizeView;->b:I

    invoke-virtual {p0}, Lcom/twitter/android/spen/StrokeSizeView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b001a    # com.twitter.android.R.color.canvas_color_black

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/spen/StrokeSizeView;->c:I

    return-void
.end method


# virtual methods
.method public getStrokeSizePercentage()I
    .locals 1

    iget v0, p0, Lcom/twitter/android/spen/StrokeSizeView;->d:I

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5

    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v2, 0x40000000    # 2.0f

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    invoke-virtual {p0}, Lcom/twitter/android/spen/StrokeSizeView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v2

    invoke-virtual {p0}, Lcom/twitter/android/spen/StrokeSizeView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    sub-float/2addr v0, v2

    sget-object v1, Lcom/twitter/android/spen/StrokeSizeView;->a:Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setFlags(I)V

    iget v1, p0, Lcom/twitter/android/spen/StrokeSizeView;->c:I

    if-eqz v1, :cond_0

    sget-object v1, Lcom/twitter/android/spen/StrokeSizeView;->a:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    sget-object v1, Lcom/twitter/android/spen/StrokeSizeView;->a:Landroid/graphics/Paint;

    iget v2, p0, Lcom/twitter/android/spen/StrokeSizeView;->c:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v1, Lcom/twitter/android/spen/StrokeSizeView;->a:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    :goto_0
    iget v1, p0, Lcom/twitter/android/spen/StrokeSizeView;->d:I

    int-to-float v1, v1

    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v1, v2

    mul-float/2addr v1, v0

    const/4 v2, 0x0

    cmpl-float v2, v1, v2

    if-lez v2, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/spen/StrokeSizeView;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/twitter/android/spen/StrokeSizeView;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v1, v1

    sget-object v4, Lcom/twitter/android/spen/StrokeSizeView;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3, v1, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    :goto_1
    sget-object v1, Lcom/twitter/android/spen/StrokeSizeView;->a:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    sget-object v1, Lcom/twitter/android/spen/StrokeSizeView;->a:Landroid/graphics/Paint;

    iget v2, p0, Lcom/twitter/android/spen/StrokeSizeView;->b:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v1, Lcom/twitter/android/spen/StrokeSizeView;->a:Landroid/graphics/Paint;

    const/high16 v2, 0x40400000    # 3.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    invoke-virtual {p0}, Lcom/twitter/android/spen/StrokeSizeView;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/twitter/android/spen/StrokeSizeView;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sget-object v3, Lcom/twitter/android/spen/StrokeSizeView;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v0, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    return-void

    :cond_0
    sget-object v1, Lcom/twitter/android/spen/StrokeSizeView;->a:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    sget-object v1, Lcom/twitter/android/spen/StrokeSizeView;->a:Landroid/graphics/Paint;

    const v2, -0x333334

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v1, Lcom/twitter/android/spen/StrokeSizeView;->a:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/spen/StrokeSizeView;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/twitter/android/spen/StrokeSizeView;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sget-object v3, Lcom/twitter/android/spen/StrokeSizeView;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v3}, Landroid/graphics/Canvas;->drawPoint(FFLandroid/graphics/Paint;)V

    goto :goto_1
.end method

.method public setStrokeColor(I)V
    .locals 0

    iput p1, p0, Lcom/twitter/android/spen/StrokeSizeView;->c:I

    invoke-virtual {p0}, Lcom/twitter/android/spen/StrokeSizeView;->invalidate()V

    return-void
.end method

.method public setStrokeSizePercentage(I)V
    .locals 1

    if-gez p1, :cond_1

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/spen/StrokeSizeView;->invalidate()V

    return-void

    :cond_1
    const/16 v0, 0x64

    if-gt p1, v0, :cond_0

    iput p1, p0, Lcom/twitter/android/spen/StrokeSizeView;->d:I

    goto :goto_0
.end method
