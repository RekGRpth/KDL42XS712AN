.class Lcom/twitter/android/up;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7f0f0505    # com.twitter.android.R.string.twitter_username

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/up;->a:Ljava/lang/String;

    const v0, 0x7f0f0500    # com.twitter.android.R.string.twitter_name

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/up;->b:Ljava/lang/String;

    const v0, 0x7f0f0501    # com.twitter.android.R.string.twitter_profile_url

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/up;->c:Ljava/lang/String;

    const v0, 0x7f0f04ff    # com.twitter.android.R.string.twitter_discover_content

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/up;->d:Ljava/lang/String;

    return-void
.end method
