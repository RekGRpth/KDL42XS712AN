.class Lcom/twitter/android/le;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/MediaTagFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/MediaTagFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/le;->a:Lcom/twitter/android/MediaTagFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/le;->a:Lcom/twitter/android/MediaTagFragment;

    invoke-static {v0}, Lcom/twitter/android/MediaTagFragment;->a(Lcom/twitter/android/MediaTagFragment;)Lcom/twitter/android/widget/DraggableHeaderLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/le;->a:Lcom/twitter/android/MediaTagFragment;

    invoke-static {v1}, Lcom/twitter/android/MediaTagFragment;->a(Lcom/twitter/android/MediaTagFragment;)Lcom/twitter/android/widget/DraggableHeaderLayout;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/widget/DraggableHeaderLayout;->getVisibleHeaderHeight()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/DraggableHeaderLayout;->setMinVisibleHeaderHeight(I)V

    iget-object v0, p0, Lcom/twitter/android/le;->a:Lcom/twitter/android/MediaTagFragment;

    invoke-static {v0}, Lcom/twitter/android/MediaTagFragment;->a(Lcom/twitter/android/MediaTagFragment;)Lcom/twitter/android/widget/DraggableHeaderLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/widget/DraggableHeaderLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    return-void
.end method
