.class final Lcom/twitter/android/gi;
.super Landroid/os/AsyncTask;
.source "Twttr"


# instance fields
.field private a:Ljava/lang/ref/WeakReference;

.field private b:Ljava/lang/ref/WeakReference;

.field private c:Lcom/twitter/media/filters/Filters;

.field private d:[Landroid/graphics/Bitmap;

.field private final e:Z

.field private final f:Z

.field private g:I

.field private h:I

.field private i:Lcom/twitter/android/gk;

.field private j:Landroid/net/Uri;

.field private k:Lcom/twitter/android/gk;

.field private l:Z


# direct methods
.method public constructor <init>(Lcom/twitter/android/FilterManager;Landroid/net/Uri;Lcom/twitter/android/gk;Lcom/twitter/android/gk;ZLcom/twitter/android/gm;)V
    .locals 3

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/gi;->a:Ljava/lang/ref/WeakReference;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p6}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/gi;->b:Ljava/lang/ref/WeakReference;

    iget-object v0, p1, Lcom/twitter/android/FilterManager;->c:Lcom/twitter/media/filters/Filters;

    iput-object v0, p0, Lcom/twitter/android/gi;->c:Lcom/twitter/media/filters/Filters;

    iget-object v0, p0, Lcom/twitter/android/gi;->c:Lcom/twitter/media/filters/Filters;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/gi;->e:Z

    iget-object v0, p1, Lcom/twitter/android/FilterManager;->f:[Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/twitter/android/gi;->d:[Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/twitter/android/gi;->d:[Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/twitter/android/gi;->f:Z

    iget v0, p1, Lcom/twitter/android/FilterManager;->d:I

    iput v0, p0, Lcom/twitter/android/gi;->g:I

    if-eqz p4, :cond_2

    :goto_2
    iput-object p4, p0, Lcom/twitter/android/gi;->i:Lcom/twitter/android/gk;

    iput-object p2, p0, Lcom/twitter/android/gi;->j:Landroid/net/Uri;

    iput-object p3, p0, Lcom/twitter/android/gi;->k:Lcom/twitter/android/gk;

    iput-boolean p5, p0, Lcom/twitter/android/gi;->l:Z

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1

    :cond_2
    iget-object p4, p1, Lcom/twitter/android/FilterManager;->e:Lcom/twitter/android/gk;

    goto :goto_2
.end method

.method private a(Landroid/net/Uri;IIZ)I
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/gi;->a()V

    iget-object v0, p0, Lcom/twitter/android/gi;->c:Lcom/twitter/media/filters/Filters;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/gi;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/gi;->c:Lcom/twitter/media/filters/Filters;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/twitter/media/filters/Filters;->a(Landroid/net/Uri;IIZ)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a()V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/gi;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/FilterManager;

    iget-object v1, p0, Lcom/twitter/android/gi;->c:Lcom/twitter/media/filters/Filters;

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/gi;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/twitter/media/filters/Filters;

    invoke-direct {v1}, Lcom/twitter/media/filters/Filters;-><init>()V

    iget-object v0, v0, Lcom/twitter/android/FilterManager;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f070004    # com.twitter.android.R.raw.filter_resources

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->openRawResourceFd(I)Landroid/content/res/AssetFileDescriptor;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/gi;->isCancelled()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v1, v0, v2}, Lcom/twitter/media/filters/Filters;->a(Landroid/content/Context;Landroid/content/res/AssetFileDescriptor;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-object v1, p0, Lcom/twitter/android/gi;->c:Lcom/twitter/media/filters/Filters;

    :cond_0
    return-void
.end method

.method private a(ZZ)V
    .locals 7

    const/4 v6, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/gi;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/FilterManager;

    if-eqz v0, :cond_3

    if-nez p1, :cond_3

    if-eqz p2, :cond_3

    const/4 v1, 0x1

    move v3, v1

    :goto_0
    if-eqz v3, :cond_4

    iget-object v1, p0, Lcom/twitter/android/gi;->c:Lcom/twitter/media/filters/Filters;

    iput-object v1, v0, Lcom/twitter/android/FilterManager;->c:Lcom/twitter/media/filters/Filters;

    iget v1, p0, Lcom/twitter/android/gi;->h:I

    iput v1, v0, Lcom/twitter/android/FilterManager;->d:I

    iget-object v1, p0, Lcom/twitter/android/gi;->d:[Landroid/graphics/Bitmap;

    iput-object v1, v0, Lcom/twitter/android/FilterManager;->f:[Landroid/graphics/Bitmap;

    iget-boolean v1, p0, Lcom/twitter/android/gi;->l:Z

    iput-boolean v1, v0, Lcom/twitter/android/FilterManager;->h:Z

    iget-object v1, p0, Lcom/twitter/android/gi;->i:Lcom/twitter/android/gk;

    iput-object v1, v0, Lcom/twitter/android/FilterManager;->e:Lcom/twitter/android/gk;

    iget-object v1, p0, Lcom/twitter/android/gi;->j:Landroid/net/Uri;

    iput-object v1, v0, Lcom/twitter/android/FilterManager;->g:Landroid/net/Uri;

    :cond_0
    :goto_1
    if-eqz v0, :cond_1

    iput-object v6, v0, Lcom/twitter/android/FilterManager;->j:Lcom/twitter/android/gi;

    iput-boolean v2, v0, Lcom/twitter/android/FilterManager;->i:Z

    :cond_1
    if-nez p1, :cond_2

    iget-object v0, p0, Lcom/twitter/android/gi;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/gm;

    if-eqz v0, :cond_2

    invoke-interface {v0, v3}, Lcom/twitter/android/gm;->a(Z)V

    :cond_2
    return-void

    :cond_3
    move v3, v2

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/twitter/android/gi;->c:Lcom/twitter/media/filters/Filters;

    if-eqz v1, :cond_6

    :try_start_0
    iget-boolean v1, p0, Lcom/twitter/android/gi;->e:Z

    if-eqz v1, :cond_5

    if-eqz p1, :cond_9

    :cond_5
    iget-object v1, p0, Lcom/twitter/android/gi;->c:Lcom/twitter/media/filters/Filters;

    invoke-virtual {v1}, Lcom/twitter/media/filters/Filters;->a()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/twitter/android/gi;->c:Lcom/twitter/media/filters/Filters;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :cond_6
    :goto_2
    iget-object v1, p0, Lcom/twitter/android/gi;->d:[Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/twitter/android/gi;->f:Z

    if-eqz v1, :cond_7

    if-eqz p1, :cond_0

    :cond_7
    sget v4, Lcom/twitter/android/FilterActivity;->a:I

    move v1, v2

    :goto_3
    if-ge v1, v4, :cond_a

    iget-object v5, p0, Lcom/twitter/android/gi;->d:[Landroid/graphics/Bitmap;

    aget-object v5, v5, v1

    if-eqz v5, :cond_8

    iget-object v5, p0, Lcom/twitter/android/gi;->d:[Landroid/graphics/Bitmap;

    aget-object v5, v5, v1

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->recycle()V

    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_9
    :try_start_1
    invoke-direct {p0}, Lcom/twitter/android/gi;->b()V

    iget v1, p0, Lcom/twitter/android/gi;->h:I

    if-lez v1, :cond_6

    iget-object v1, p0, Lcom/twitter/android/gi;->c:Lcom/twitter/media/filters/Filters;

    iget v4, p0, Lcom/twitter/android/gi;->h:I

    invoke-virtual {v1, v4}, Lcom/twitter/media/filters/Filters;->a(I)V

    const/4 v1, 0x0

    iput v1, p0, Lcom/twitter/android/gi;->h:I
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :catch_0
    move-exception v1

    invoke-static {v1}, Lcom/crashlytics/android/d;->a(Ljava/lang/Throwable;)V

    goto :goto_2

    :cond_a
    iput-object v6, p0, Lcom/twitter/android/gi;->d:[Landroid/graphics/Bitmap;

    goto :goto_1
.end method

.method private a(Landroid/net/Uri;Lcom/twitter/android/gk;Z)[Landroid/graphics/Bitmap;
    .locals 10

    const/4 v3, 0x0

    iget v4, p2, Lcom/twitter/android/gk;->a:I

    iget v5, p2, Lcom/twitter/android/gk;->b:I

    iget-object v6, p2, Lcom/twitter/android/gk;->c:Landroid/graphics/Bitmap$Config;

    sget v7, Lcom/twitter/android/FilterActivity;->a:I

    iget-object v0, p0, Lcom/twitter/android/gi;->d:[Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    new-array v0, v7, [Landroid/graphics/Bitmap;

    move-object v1, v0

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/gi;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    :goto_1
    return-object v1

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/gi;->d:[Landroid/graphics/Bitmap;

    move-object v1, v0

    goto :goto_0

    :cond_2
    :try_start_0
    invoke-direct {p0, p1, v4, v5, p3}, Lcom/twitter/android/gi;->a(Landroid/net/Uri;IIZ)I
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_8

    if-lez v2, :cond_0

    iget-object v0, p0, Lcom/twitter/android/gi;->c:Lcom/twitter/media/filters/Filters;

    invoke-virtual {v0, v2}, Lcom/twitter/media/filters/Filters;->a(I)V

    goto :goto_1

    :cond_3
    :try_start_1
    aget-object v0, v1, v3

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v8

    if-ne v8, v4, :cond_4

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v8

    if-eq v8, v5, :cond_6

    :cond_4
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_5
    invoke-static {v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    aput-object v0, v1, v3

    :cond_6
    aget-object v0, v1, v3

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/twitter/android/gi;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/FilterManager;

    if-eqz v0, :cond_7

    iget-object v0, v0, Lcom/twitter/android/FilterManager;->a:Landroid/content/Context;

    new-instance v8, Lcom/twitter/library/util/BitmapNotCreatedException;

    const-string/jumbo v9, "Unable to create image grid"

    invoke-direct {v8, v9}, Lcom/twitter/library/util/BitmapNotCreatedException;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v8}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Ljava/lang/Throwable;)V

    :cond_7
    :goto_2
    add-int/lit8 v3, v3, 0x1

    :cond_8
    if-ge v3, v7, :cond_a

    invoke-virtual {p0}, Lcom/twitter/android/gi;->isCancelled()Z
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-eqz v0, :cond_3

    if-lez v2, :cond_0

    iget-object v0, p0, Lcom/twitter/android/gi;->c:Lcom/twitter/media/filters/Filters;

    invoke-virtual {v0, v2}, Lcom/twitter/media/filters/Filters;->a(I)V

    goto :goto_1

    :cond_9
    :try_start_2
    invoke-virtual {p0}, Lcom/twitter/android/gi;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/twitter/android/gi;->c:Lcom/twitter/media/filters/Filters;

    sget-object v8, Lcom/twitter/library/util/n;->a:[I

    aget v8, v8, v3

    aget-object v9, v1, v3

    invoke-virtual {v0, v8, v2, v9}, Lcom/twitter/media/filters/Filters;->a(IILandroid/graphics/Bitmap;)Z
    :try_end_2
    .catch Ljava/lang/OutOfMemoryError; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_2

    :catch_0
    move-exception v0

    :goto_3
    :try_start_3
    invoke-static {v0}, Lcom/crashlytics/android/d;->a(Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-lez v2, :cond_0

    iget-object v0, p0, Lcom/twitter/android/gi;->c:Lcom/twitter/media/filters/Filters;

    invoke-virtual {v0, v2}, Lcom/twitter/media/filters/Filters;->a(I)V

    goto :goto_1

    :cond_a
    if-lez v2, :cond_0

    iget-object v0, p0, Lcom/twitter/android/gi;->c:Lcom/twitter/media/filters/Filters;

    invoke-virtual {v0, v2}, Lcom/twitter/media/filters/Filters;->a(I)V

    goto :goto_1

    :catchall_0
    move-exception v0

    move v2, v3

    :goto_4
    if-lez v2, :cond_b

    iget-object v1, p0, Lcom/twitter/android/gi;->c:Lcom/twitter/media/filters/Filters;

    invoke-virtual {v1, v2}, Lcom/twitter/media/filters/Filters;->a(I)V

    :cond_b
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_4

    :catch_1
    move-exception v0

    move v2, v3

    goto :goto_3
.end method

.method private b()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/gi;->c:Lcom/twitter/media/filters/Filters;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/android/gi;->g:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/gi;->c:Lcom/twitter/media/filters/Filters;

    iget v1, p0, Lcom/twitter/android/gi;->g:I

    invoke-virtual {v0, v1}, Lcom/twitter/media/filters/Filters;->a(I)V

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/gi;->g:I

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Boolean;
    .locals 6

    const/4 v0, 0x0

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/gi;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/twitter/android/gi;->i:Lcom/twitter/android/gk;

    invoke-direct {p0}, Lcom/twitter/android/gi;->b()V

    iget-object v2, p0, Lcom/twitter/android/gi;->j:Landroid/net/Uri;

    iget-object v3, p0, Lcom/twitter/android/gi;->k:Lcom/twitter/android/gk;

    iget-boolean v4, p0, Lcom/twitter/android/gi;->l:Z

    invoke-direct {p0, v2, v3, v4}, Lcom/twitter/android/gi;->a(Landroid/net/Uri;Lcom/twitter/android/gk;Z)[Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/gi;->d:[Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/twitter/android/gi;->c:Lcom/twitter/media/filters/Filters;

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/gi;->isCancelled()Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/gi;->c:Lcom/twitter/media/filters/Filters;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/gi;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/gi;->c:Lcom/twitter/media/filters/Filters;

    invoke-virtual {v1}, Lcom/twitter/media/filters/Filters;->a()V

    iput-object v5, p0, Lcom/twitter/android/gi;->c:Lcom/twitter/media/filters/Filters;

    goto :goto_0

    :cond_3
    :try_start_1
    iget-object v2, p0, Lcom/twitter/android/gi;->j:Landroid/net/Uri;

    iget v3, v1, Lcom/twitter/android/gk;->a:I

    iget v1, v1, Lcom/twitter/android/gk;->b:I

    iget-boolean v4, p0, Lcom/twitter/android/gi;->l:Z

    invoke-direct {p0, v2, v3, v1, v4}, Lcom/twitter/android/gi;->a(Landroid/net/Uri;IIZ)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/gi;->h:I

    iget v1, p0, Lcom/twitter/android/gi;->h:I

    if-lez v1, :cond_4

    const/4 v0, 0x1

    :cond_4
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/gi;->c:Lcom/twitter/media/filters/Filters;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/gi;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/gi;->c:Lcom/twitter/media/filters/Filters;

    invoke-virtual {v1}, Lcom/twitter/media/filters/Filters;->a()V

    iput-object v5, p0, Lcom/twitter/android/gi;->c:Lcom/twitter/media/filters/Filters;

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {v0}, Lcom/crashlytics/android/d;->a(Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v0, p0, Lcom/twitter/android/gi;->c:Lcom/twitter/media/filters/Filters;

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/twitter/android/gi;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/android/gi;->c:Lcom/twitter/media/filters/Filters;

    invoke-virtual {v0}, Lcom/twitter/media/filters/Filters;->a()V

    iput-object v5, p0, Lcom/twitter/android/gi;->c:Lcom/twitter/media/filters/Filters;

    :cond_5
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/twitter/android/gi;->c:Lcom/twitter/media/filters/Filters;

    if-eqz v1, :cond_6

    invoke-virtual {p0}, Lcom/twitter/android/gi;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/twitter/android/gi;->c:Lcom/twitter/media/filters/Filters;

    invoke-virtual {v1}, Lcom/twitter/media/filters/Filters;->a()V

    iput-object v5, p0, Lcom/twitter/android/gi;->c:Lcom/twitter/media/filters/Filters;

    :cond_6
    throw v0
.end method

.method public a(Lcom/twitter/android/gm;)V
    .locals 1

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/gi;->b:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method protected a(Ljava/lang/Boolean;)V
    .locals 2

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v1, v0}, Lcom/twitter/android/gi;->a(ZZ)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method protected b(Ljava/lang/Boolean;)V
    .locals 2

    const/4 v1, 0x1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-direct {p0, v1, v0}, Lcom/twitter/android/gi;->a(ZZ)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/gi;->a([Ljava/lang/Void;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/twitter/android/gi;->b(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/twitter/android/gi;->a(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/gi;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/FilterManager;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/FilterManager;->a(Z)V

    invoke-virtual {v0}, Lcom/twitter/android/FilterManager;->b()V

    :cond_0
    return-void
.end method
