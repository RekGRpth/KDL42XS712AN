.class Lcom/twitter/android/vh;
.super Landroid/widget/Filter;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/TrendLocationsFragment;


# direct methods
.method public constructor <init>(Lcom/twitter/android/TrendLocationsFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/vh;->a:Lcom/twitter/android/TrendLocationsFragment;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method


# virtual methods
.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 7

    const/4 v2, 0x0

    new-instance v6, Landroid/widget/Filter$FilterResults;

    invoke-direct {v6}, Landroid/widget/Filter$FilterResults;-><init>()V

    iget-object v0, p0, Lcom/twitter/android/vh;->a:Lcom/twitter/android/TrendLocationsFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TrendLocationsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/provider/SuggestionsProvider;->f:Landroid/net/Uri;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    iput v1, v6, Landroid/widget/Filter$FilterResults;->count:I

    iput-object v0, v6, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    :cond_0
    return-object v6
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/vh;->a:Lcom/twitter/android/TrendLocationsFragment;

    invoke-static {v0}, Lcom/twitter/android/TrendLocationsFragment;->a(Lcom/twitter/android/TrendLocationsFragment;)Landroid/support/v4/widget/CursorAdapter;

    move-result-object v1

    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Landroid/database/Cursor;

    invoke-virtual {v1, v0}, Landroid/support/v4/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    return-void
.end method
