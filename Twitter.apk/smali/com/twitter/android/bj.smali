.class public Lcom/twitter/android/bj;
.super Landroid/os/Handler;
.source "Twttr"


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;


# direct methods
.method public constructor <init>(Lcom/twitter/android/BaseSignUpActivity;)V
    .locals 1

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/bj;->a:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 2

    invoke-virtual {p0, p1}, Lcom/twitter/android/bj;->removeMessages(I)V

    const-wide/16 v0, 0x1f4

    invoke-virtual {p0, p1, v0, v1}, Lcom/twitter/android/bj;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method

.method public b(I)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/twitter/android/bj;->removeMessages(I)V

    invoke-virtual {p0, p1}, Lcom/twitter/android/bj;->sendEmptyMessage(I)Z

    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/twitter/android/bj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/BaseSignUpActivity;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    :goto_1
    invoke-virtual {v0}, Lcom/twitter/android/BaseSignUpActivity;->a()V

    goto :goto_0

    :pswitch_0
    iget-object v1, v0, Lcom/twitter/android/BaseSignUpActivity;->a:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->length()I

    move-result v1

    if-lez v1, :cond_1

    move v1, v2

    :goto_2
    iget-object v4, v0, Lcom/twitter/android/BaseSignUpActivity;->a:Landroid/widget/EditText;

    invoke-virtual {v0, v4, v1}, Lcom/twitter/android/BaseSignUpActivity;->a(Landroid/widget/EditText;Z)V

    if-eqz v1, :cond_2

    iput v2, v0, Lcom/twitter/android/BaseSignUpActivity;->k:I

    :goto_3
    invoke-virtual {v0}, Lcom/twitter/android/BaseSignUpActivity;->b()V

    goto :goto_1

    :cond_1
    move v1, v3

    goto :goto_2

    :cond_2
    iput v3, v0, Lcom/twitter/android/BaseSignUpActivity;->k:I

    goto :goto_3

    :pswitch_1
    iget-object v1, v0, Lcom/twitter/android/BaseSignUpActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/util/x;->h:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {}, Lcom/twitter/library/client/App;->o()Z

    move-result v2

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v2, v4}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;ZLjava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {v0}, Lcom/twitter/android/BaseSignUpActivity;->a(Lcom/twitter/android/BaseSignUpActivity;)Lcom/twitter/android/client/c;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/twitter/android/client/c;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/BaseSignUpActivity;->d(Lcom/twitter/android/BaseSignUpActivity;Ljava/lang/String;)V

    :goto_4
    invoke-virtual {v0}, Lcom/twitter/android/BaseSignUpActivity;->b()V

    goto :goto_1

    :cond_3
    iget-object v1, v0, Lcom/twitter/android/BaseSignUpActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->hasFocus()Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, v0, Lcom/twitter/android/BaseSignUpActivity;->b:Landroid/widget/EditText;

    iget-object v2, v0, Lcom/twitter/android/BaseSignUpActivity;->h:Landroid/widget/TextView;

    const v4, 0x7f0f0442    # com.twitter.android.R.string.signup_error_email

    invoke-virtual {v0, v4}, Lcom/twitter/android/BaseSignUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v4}, Lcom/twitter/android/BaseSignUpActivity;->a(Landroid/widget/EditText;Landroid/widget/TextView;Ljava/lang/String;)V

    :cond_4
    iput v3, v0, Lcom/twitter/android/BaseSignUpActivity;->l:I

    goto :goto_4

    :pswitch_2
    iget-object v1, v0, Lcom/twitter/android/BaseSignUpActivity;->c:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v1}, Lcom/twitter/internal/android/widget/PopupEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/util/x;->i:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-static {v0}, Lcom/twitter/android/BaseSignUpActivity;->b(Lcom/twitter/android/BaseSignUpActivity;)Lcom/twitter/android/client/c;

    move-result-object v2

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1, v4, v4}, Lcom/twitter/android/client/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/BaseSignUpActivity;->e(Lcom/twitter/android/BaseSignUpActivity;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_5
    iget-object v1, v0, Lcom/twitter/android/BaseSignUpActivity;->c:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v1}, Lcom/twitter/internal/android/widget/PopupEditText;->hasFocus()Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, v0, Lcom/twitter/android/BaseSignUpActivity;->c:Lcom/twitter/internal/android/widget/PopupEditText;

    iget-object v2, v0, Lcom/twitter/android/BaseSignUpActivity;->i:Landroid/widget/TextView;

    const v4, 0x7f0f0444    # com.twitter.android.R.string.signup_error_username

    invoke-virtual {v0, v4}, Lcom/twitter/android/BaseSignUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v4}, Lcom/twitter/android/BaseSignUpActivity;->a(Landroid/widget/EditText;Landroid/widget/TextView;Ljava/lang/String;)V

    :cond_6
    iput v3, v0, Lcom/twitter/android/BaseSignUpActivity;->m:I

    goto/16 :goto_1

    :pswitch_3
    iget-object v1, v0, Lcom/twitter/android/BaseSignUpActivity;->d:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->length()I

    move-result v1

    const/4 v4, 0x6

    if-lt v1, v4, :cond_7

    move v1, v2

    :goto_5
    iget-object v4, v0, Lcom/twitter/android/BaseSignUpActivity;->d:Landroid/widget/EditText;

    invoke-virtual {v0, v4, v1}, Lcom/twitter/android/BaseSignUpActivity;->a(Landroid/widget/EditText;Z)V

    if-eqz v1, :cond_8

    iput v2, v0, Lcom/twitter/android/BaseSignUpActivity;->n:I

    goto/16 :goto_1

    :cond_7
    move v1, v3

    goto :goto_5

    :cond_8
    iget-object v1, v0, Lcom/twitter/android/BaseSignUpActivity;->d:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->hasFocus()Z

    move-result v1

    if-nez v1, :cond_9

    iget-object v1, v0, Lcom/twitter/android/BaseSignUpActivity;->d:Landroid/widget/EditText;

    iget-object v2, v0, Lcom/twitter/android/BaseSignUpActivity;->j:Landroid/widget/TextView;

    const v4, 0x7f0f0443    # com.twitter.android.R.string.signup_error_password

    invoke-virtual {v0, v4}, Lcom/twitter/android/BaseSignUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v4}, Lcom/twitter/android/BaseSignUpActivity;->a(Landroid/widget/EditText;Landroid/widget/TextView;Ljava/lang/String;)V

    :cond_9
    iput v3, v0, Lcom/twitter/android/BaseSignUpActivity;->n:I

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
