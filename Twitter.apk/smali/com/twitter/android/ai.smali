.class Lcom/twitter/android/ai;
.super Lcom/twitter/library/client/j;
.source "Twttr"


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;


# direct methods
.method private constructor <init>(Lcom/twitter/android/AmplifyMediaPlayerActivity;)V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/library/client/j;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/ai;->a:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/AmplifyMediaPlayerActivity;Lcom/twitter/android/ag;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/ai;-><init>(Lcom/twitter/android/AmplifyMediaPlayerActivity;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;J)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/ai;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/AmplifyMediaPlayerActivity;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {v0}, Lcom/twitter/android/AmplifyMediaPlayerActivity;->a(Lcom/twitter/android/AmplifyMediaPlayerActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    invoke-static {p5, p6, v1, v2}, Lcom/twitter/library/provider/w;->a(JJ)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/AmplifyMediaPlayerActivity;->a(Lcom/twitter/android/AmplifyMediaPlayerActivity;Landroid/net/Uri;)Landroid/net/Uri;

    invoke-virtual {v0}, Lcom/twitter/android/AmplifyMediaPlayerActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_0
.end method

.method public b(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;J)V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/ai;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/AmplifyMediaPlayerActivity;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {v0}, Lcom/twitter/android/AmplifyMediaPlayerActivity;->b(Lcom/twitter/android/AmplifyMediaPlayerActivity;)Lcom/twitter/library/provider/Tweet;

    move-result-object v1

    iget-wide v1, v1, Lcom/twitter/library/provider/Tweet;->o:J

    invoke-static {v0}, Lcom/twitter/android/AmplifyMediaPlayerActivity;->a(Lcom/twitter/android/AmplifyMediaPlayerActivity;)Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-static {v1, v2, v3, v4}, Lcom/twitter/library/provider/w;->b(JJ)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/AmplifyMediaPlayerActivity;->a(Lcom/twitter/android/AmplifyMediaPlayerActivity;Landroid/net/Uri;)Landroid/net/Uri;

    invoke-virtual {v0}, Lcom/twitter/android/AmplifyMediaPlayerActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_0
.end method
