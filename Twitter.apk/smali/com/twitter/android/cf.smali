.class Lcom/twitter/android/cf;
.super Lcom/twitter/android/card/a;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/CardPreviewerFragment;


# direct methods
.method public constructor <init>(Lcom/twitter/android/CardPreviewerFragment;Landroid/app/Activity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/cf;->a:Lcom/twitter/android/CardPreviewerFragment;

    invoke-direct {p0, p2}, Lcom/twitter/android/card/a;-><init>(Landroid/app/Activity;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/twitter/android/card/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/cf;->a:Lcom/twitter/android/CardPreviewerFragment;

    invoke-static {v0, p1, p2}, Lcom/twitter/android/CardPreviewerFragment;->a(Lcom/twitter/android/CardPreviewerFragment;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public b()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public d()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public e()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/cf;->a:Lcom/twitter/android/CardPreviewerFragment;

    const v1, 0x7f0f0058    # com.twitter.android.R.string.button_action_fave

    invoke-static {v0, v1}, Lcom/twitter/android/CardPreviewerFragment;->a(Lcom/twitter/android/CardPreviewerFragment;I)V

    return-void
.end method

.method public f()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/cf;->a:Lcom/twitter/android/CardPreviewerFragment;

    const v1, 0x7f0f005e    # com.twitter.android.R.string.button_action_retweet

    invoke-static {v0, v1}, Lcom/twitter/android/CardPreviewerFragment;->a(Lcom/twitter/android/CardPreviewerFragment;I)V

    return-void
.end method

.method public g()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/cf;->a:Lcom/twitter/android/CardPreviewerFragment;

    const v1, 0x7f0f005f    # com.twitter.android.R.string.button_action_share

    invoke-static {v0, v1}, Lcom/twitter/android/CardPreviewerFragment;->a(Lcom/twitter/android/CardPreviewerFragment;I)V

    return-void
.end method

.method public h()V
    .locals 0

    invoke-super {p0}, Lcom/twitter/android/card/a;->h()V

    return-void
.end method

.method public i()V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0}, Lcom/twitter/android/card/a;->i()V

    iget-object v0, p0, Lcom/twitter/android/cf;->a:Lcom/twitter/android/CardPreviewerFragment;

    invoke-static {v0}, Lcom/twitter/android/CardPreviewerFragment;->f(Lcom/twitter/android/CardPreviewerFragment;)Lcom/twitter/android/cf;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/cf;->o()Lcom/twitter/android/card/p;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/android/card/p;->a()Z

    iget-object v0, p0, Lcom/twitter/android/cf;->a:Lcom/twitter/android/CardPreviewerFragment;

    invoke-static {v0}, Lcom/twitter/android/CardPreviewerFragment;->f(Lcom/twitter/android/CardPreviewerFragment;)Lcom/twitter/android/cf;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/twitter/android/cf;->a(Lcom/twitter/android/card/p;)V

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/cf;->p()Lcom/twitter/library/card/Card;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/twitter/android/card/n;->a()Lcom/twitter/android/card/n;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/cf;->a:Lcom/twitter/android/CardPreviewerFragment;

    invoke-static {v2}, Lcom/twitter/android/CardPreviewerFragment;->f(Lcom/twitter/android/CardPreviewerFragment;)Lcom/twitter/android/cf;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/twitter/android/card/n;->a(Lcom/twitter/library/card/k;Lcom/twitter/library/card/Card;)V

    invoke-virtual {p0, v3}, Lcom/twitter/android/cf;->a(Lcom/twitter/library/card/Card;)V

    :cond_1
    return-void
.end method
