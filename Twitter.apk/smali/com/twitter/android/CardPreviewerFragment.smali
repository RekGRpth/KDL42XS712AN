.class public Lcom/twitter/android/CardPreviewerFragment;
.super Landroid/support/v4/app/Fragment;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/card/q;


# instance fields
.field private a:Landroid/os/Handler;

.field private b:Landroid/view/ViewGroup;

.field private c:Landroid/view/View;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/Button;

.field private f:Landroid/widget/RelativeLayout;

.field private g:Landroid/widget/TextView;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/view/View;

.field private j:Lcom/twitter/library/provider/Tweet;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Lcom/twitter/android/cf;

.field private p:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method private a(I)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/CardPreviewerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, p1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    const/16 v1, 0x30

    invoke-virtual {v0, v1, v2, v2}, Landroid/widget/Toast;->setGravity(III)V

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method private a(IIII)V
    .locals 7

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/twitter/android/CardPreviewerFragment;->g:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/CardPreviewerFragment;->m:Ljava/lang/String;

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/CardPreviewerFragment;->h:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/CardPreviewerFragment;->n:Ljava/lang/String;

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/CardPreviewerFragment;->f:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iput-object p1, p0, Lcom/twitter/android/CardPreviewerFragment;->i:Landroid/view/View;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/CardPreviewerFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/CardPreviewerFragment;->b()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/CardPreviewerFragment;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/CardPreviewerFragment;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/CardPreviewerFragment;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/CardPreviewerFragment;->c(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/CardPreviewerFragment;Ljava/lang/String;Lcom/twitter/internal/network/k;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/CardPreviewerFragment;->a(Ljava/lang/String;Lcom/twitter/internal/network/k;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/CardPreviewerFragment;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/CardPreviewerFragment;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/CardPreviewerFragment;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/CardPreviewerFragment;->a(Z)V

    return-void
.end method

.method private a(Ljava/lang/String;Lcom/twitter/internal/network/k;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p2, Lcom/twitter/internal/network/k;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p2, Lcom/twitter/internal/network/k;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/CardPreviewerFragment;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/CardPreviewerFragment;->b(Ljava/lang/String;)V

    return-void
.end method

.method private a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/CardPreviewerFragment;->e:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/CardPreviewerFragment;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/CardPreviewerFragment;->d:Landroid/widget/TextView;

    return-object v0
.end method

.method private b()V
    .locals 4

    const/4 v3, 0x0

    invoke-static {}, Lcom/twitter/android/card/n;->a()Lcom/twitter/android/card/n;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/CardPreviewerFragment;->o:Lcom/twitter/android/cf;

    invoke-virtual {v1}, Lcom/twitter/android/cf;->o()Lcom/twitter/android/card/p;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/twitter/android/card/p;->a()Z

    iget-object v1, p0, Lcom/twitter/android/CardPreviewerFragment;->o:Lcom/twitter/android/cf;

    invoke-virtual {v1, v3}, Lcom/twitter/android/cf;->a(Lcom/twitter/android/card/p;)V

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/CardPreviewerFragment;->o:Lcom/twitter/android/cf;

    invoke-virtual {v1}, Lcom/twitter/android/cf;->p()Lcom/twitter/library/card/Card;

    move-result-object v1

    if-eqz v1, :cond_1

    iget v2, v1, Lcom/twitter/library/card/Card;->id:I

    invoke-virtual {v0, v2}, Lcom/twitter/android/card/n;->a(I)Lcom/twitter/library/card/CardState;

    iget-object v2, p0, Lcom/twitter/android/CardPreviewerFragment;->o:Lcom/twitter/android/cf;

    invoke-virtual {v0, v2, v1}, Lcom/twitter/android/card/n;->a(Lcom/twitter/library/card/k;Lcom/twitter/library/card/Card;)V

    iget-object v0, p0, Lcom/twitter/android/CardPreviewerFragment;->o:Lcom/twitter/android/cf;

    invoke-virtual {v0, v3}, Lcom/twitter/android/cf;->a(Lcom/twitter/library/card/Card;)V

    :cond_1
    const-string/jumbo v0, "Load Card"

    invoke-static {v0, v3}, Lcom/twitter/library/card/CardDebugLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/twitter/android/ce;

    invoke-direct {v0, p0}, Lcom/twitter/android/ce;-><init>(Lcom/twitter/android/CardPreviewerFragment;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/ce;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/CardPreviewerFragment;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/CardPreviewerFragment;->b(Ljava/lang/String;)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/twitter/library/card/CardDebugLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/twitter/android/CardPreviewerFragment;->d(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic c(Lcom/twitter/android/CardPreviewerFragment;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/CardPreviewerFragment;->l:Ljava/lang/String;

    return-object v0
.end method

.method private c()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/CardPreviewerFragment;->d:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/CardPreviewerFragment;->d:Landroid/widget/TextView;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private c(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/twitter/library/card/CardDebugLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private d()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/CardPreviewerFragment;->c:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/twitter/android/CardPreviewerFragment;->a(Landroid/view/View;)V

    return-void
.end method

.method static synthetic d(Lcom/twitter/android/CardPreviewerFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/CardPreviewerFragment;->c()V

    return-void
.end method

.method private d(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/CardPreviewerFragment;->a:Landroid/os/Handler;

    new-instance v1, Lcom/twitter/android/bx;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/bx;-><init>(Lcom/twitter/android/CardPreviewerFragment;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private e()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/CardPreviewerFragment;->i:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/CardPreviewerFragment;->f:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/twitter/android/CardPreviewerFragment;->i:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->removeView(Landroid/view/View;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/CardPreviewerFragment;->i:Landroid/view/View;

    :cond_0
    return-void
.end method

.method static synthetic e(Lcom/twitter/android/CardPreviewerFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/CardPreviewerFragment;->e()V

    return-void
.end method

.method static synthetic f(Lcom/twitter/android/CardPreviewerFragment;)Lcom/twitter/android/cf;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/CardPreviewerFragment;->o:Lcom/twitter/android/cf;

    return-object v0
.end method

.method private f()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/CardPreviewerFragment;->o:Lcom/twitter/android/cf;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/cf;->a(Lcom/twitter/android/card/p;)V

    invoke-direct {p0}, Lcom/twitter/android/CardPreviewerFragment;->e()V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/android/CardPreviewerFragment;->a(Z)V

    return-void
.end method

.method static synthetic g(Lcom/twitter/android/CardPreviewerFragment;)Lcom/twitter/library/provider/Tweet;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/CardPreviewerFragment;->j:Lcom/twitter/library/provider/Tweet;

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/android/CardPreviewerFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/CardPreviewerFragment;->d()V

    return-void
.end method

.method static synthetic i(Lcom/twitter/android/CardPreviewerFragment;)Landroid/view/ViewGroup;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/CardPreviewerFragment;->b:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic j(Lcom/twitter/android/CardPreviewerFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/CardPreviewerFragment;->p:Z

    return v0
.end method

.method static synthetic k(Lcom/twitter/android/CardPreviewerFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/CardPreviewerFragment;->f()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/CardPreviewerFragment;->f()V

    return-void
.end method

.method public a(Lcom/twitter/library/card/Card;)V
    .locals 4

    invoke-direct {p0}, Lcom/twitter/android/CardPreviewerFragment;->f()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Card Loaded: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/library/card/Card;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/library/card/CardDebugLog;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/CardPreviewerFragment;->o:Lcom/twitter/android/cf;

    invoke-virtual {v0, p1}, Lcom/twitter/android/cf;->a(Lcom/twitter/library/card/Card;)V

    iget-object v0, p0, Lcom/twitter/android/CardPreviewerFragment;->o:Lcom/twitter/android/cf;

    invoke-virtual {p1, v0}, Lcom/twitter/library/card/Card;->a(Lcom/twitter/library/card/j;)V

    invoke-virtual {p1}, Lcom/twitter/library/card/Card;->j()V

    iget-object v0, p0, Lcom/twitter/android/CardPreviewerFragment;->o:Lcom/twitter/android/cf;

    invoke-virtual {v0}, Lcom/twitter/android/cf;->w()V

    invoke-virtual {p1}, Lcom/twitter/library/card/Card;->d()Lcom/twitter/library/card/CardView;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/CardPreviewerFragment;->a(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/twitter/android/CardPreviewerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/CardPreviewerFragment;->k:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/client/f;

    const-string/jumbo v3, "settings"

    invoke-direct {v2, v0, v1, v3}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v0

    const-string/jumbo v1, "developer_settings_enabled"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;Z)Lcom/twitter/library/client/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->d()V

    :cond_0
    new-instance v0, Lcom/twitter/android/bz;

    invoke-direct {v0, p0}, Lcom/twitter/android/bz;-><init>(Lcom/twitter/android/CardPreviewerFragment;)V

    invoke-virtual {p1, v0}, Lcom/twitter/library/card/Card;->a(Lcom/twitter/library/card/element/c;)V

    iget v1, v0, Lcom/twitter/android/bz;->a:I

    iget v2, v0, Lcom/twitter/android/bz;->b:I

    iget v3, v0, Lcom/twitter/android/bz;->c:I

    iget v0, v0, Lcom/twitter/android/bz;->d:I

    invoke-direct {p0, v1, v2, v3, v0}, Lcom/twitter/android/CardPreviewerFragment;->a(IIII)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/CardPreviewerFragment;->f()V

    invoke-direct {p0, p1}, Lcom/twitter/android/CardPreviewerFragment;->b(Ljava/lang/String;)V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    invoke-virtual {p0}, Lcom/twitter/android/CardPreviewerFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v1, "host"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/CardPreviewerFragment;->k:Ljava/lang/String;

    :cond_0
    const v0, 0x7f030023    # com.twitter.android.R.layout.card_previewer_fragment

    invoke-virtual {p1, v0, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/CardPreviewerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/BaseFragmentActivity;

    invoke-virtual {v0}, Lcom/twitter/android/client/BaseFragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/twitter/android/CardPreviewerFragment;->a:Landroid/os/Handler;

    iput-object p2, p0, Lcom/twitter/android/CardPreviewerFragment;->b:Landroid/view/ViewGroup;

    const v1, 0x7f030020    # com.twitter.android.R.layout.card_loading_view

    iget-object v4, p0, Lcom/twitter/android/CardPreviewerFragment;->b:Landroid/view/ViewGroup;

    invoke-virtual {p1, v1, v4, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/CardPreviewerFragment;->c:Landroid/view/View;

    const v1, 0x7f0900bf    # com.twitter.android.R.id.error_banner_text

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/twitter/android/CardPreviewerFragment;->d:Landroid/widget/TextView;

    const v1, 0x7f0900bc    # com.twitter.android.R.id.reload_button

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/twitter/android/CardPreviewerFragment;->e:Landroid/widget/Button;

    const v1, 0x7f0900c0    # com.twitter.android.R.id.preview_content

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout;

    iput-object v1, p0, Lcom/twitter/android/CardPreviewerFragment;->f:Landroid/widget/RelativeLayout;

    const v1, 0x7f0900bd    # com.twitter.android.R.id.element_count

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/twitter/android/CardPreviewerFragment;->g:Landroid/widget/TextView;

    const v1, 0x7f0900be    # com.twitter.android.R.id.element_depth

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/twitter/android/CardPreviewerFragment;->h:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0f00f7    # com.twitter.android.R.string.developer_card_previewer_element_count_label

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/twitter/android/CardPreviewerFragment;->m:Ljava/lang/String;

    const v4, 0x7f0f00f8    # com.twitter.android.R.string.developer_card_previewer_element_depth_label

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/CardPreviewerFragment;->n:Ljava/lang/String;

    invoke-direct {p0, v6, v6, v6, v6}, Lcom/twitter/android/CardPreviewerFragment;->a(IIII)V

    new-instance v1, Lcom/twitter/library/provider/Tweet;

    invoke-direct {v1}, Lcom/twitter/library/provider/Tweet;-><init>()V

    iput-object v1, p0, Lcom/twitter/android/CardPreviewerFragment;->j:Lcom/twitter/library/provider/Tweet;

    iget-object v1, p0, Lcom/twitter/android/CardPreviewerFragment;->j:Lcom/twitter/library/provider/Tweet;

    new-instance v4, Lcom/twitter/library/api/TwitterStatusCard;

    invoke-direct {v4}, Lcom/twitter/library/api/TwitterStatusCard;-><init>()V

    iput-object v4, v1, Lcom/twitter/library/provider/Tweet;->O:Lcom/twitter/library/api/TwitterStatusCard;

    invoke-static {v3}, Lcom/twitter/library/network/aa;->a(Landroid/content/Context;)Lcom/twitter/library/network/aa;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/network/aa;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/twitter/android/CardPreviewerFragment;->k:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    const v3, 0x7f0f008e    # com.twitter.android.R.string.cards_previewer_portal_url

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v1, v4, v6

    invoke-virtual {p0, v3, v4}, Lcom/twitter/android/CardPreviewerFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/CardPreviewerFragment;->l:Ljava/lang/String;

    iput-boolean v7, p0, Lcom/twitter/android/CardPreviewerFragment;->p:Z

    :goto_0
    new-instance v1, Lcom/twitter/android/cf;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/cf;-><init>(Lcom/twitter/android/CardPreviewerFragment;Landroid/app/Activity;)V

    iput-object v1, p0, Lcom/twitter/android/CardPreviewerFragment;->o:Lcom/twitter/android/cf;

    iget-object v1, p0, Lcom/twitter/android/CardPreviewerFragment;->o:Lcom/twitter/android/cf;

    iget-object v3, p0, Lcom/twitter/android/CardPreviewerFragment;->j:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v1, v3}, Lcom/twitter/android/cf;->a(Lcom/twitter/library/provider/Tweet;)V

    iget-object v1, p0, Lcom/twitter/android/CardPreviewerFragment;->o:Lcom/twitter/android/cf;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lcom/twitter/android/client/a;)V

    invoke-direct {p0}, Lcom/twitter/android/CardPreviewerFragment;->b()V

    iget-object v0, p0, Lcom/twitter/android/CardPreviewerFragment;->e:Landroid/widget/Button;

    new-instance v1, Lcom/twitter/android/bw;

    invoke-direct {v1, p0}, Lcom/twitter/android/bw;-><init>(Lcom/twitter/android/CardPreviewerFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object v2

    :cond_1
    const v3, 0x7f0f008d    # com.twitter.android.R.string.cards_previewer_devkit_url

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/twitter/android/CardPreviewerFragment;->k:Ljava/lang/String;

    aput-object v5, v4, v6

    aput-object v1, v4, v7

    invoke-virtual {p0, v3, v4}, Lcom/twitter/android/CardPreviewerFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/CardPreviewerFragment;->l:Ljava/lang/String;

    goto :goto_0
.end method
