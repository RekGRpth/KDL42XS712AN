.class Lcom/twitter/android/kh;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/MainActivity;


# direct methods
.method private constructor <init>(Lcom/twitter/android/MainActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/kh;->a:Lcom/twitter/android/MainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/MainActivity;Lcom/twitter/android/jt;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/kh;-><init>(Lcom/twitter/android/MainActivity;)V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/twitter/android/kh;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v0}, Lcom/twitter/android/MainActivity;->d(Lcom/twitter/android/MainActivity;)Lcom/twitter/android/kf;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/twitter/android/kf;->a(I)I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/kh;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v1}, Lcom/twitter/android/MainActivity;->d(Lcom/twitter/android/MainActivity;)Lcom/twitter/android/kf;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/kf;->c()I

    move-result v1

    if-ltz v0, :cond_1

    if-lt v0, v1, :cond_0

    const/4 v2, -0x1

    if-ne v1, v2, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/kh;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v1}, Lcom/twitter/android/MainActivity;->a(Lcom/twitter/android/MainActivity;)Lcom/twitter/android/kr;

    move-result-object v1

    const-string/jumbo v2, "perch"

    invoke-virtual {v1, v0, v2, v3, v3}, Lcom/twitter/android/kr;->a(ILjava/lang/String;Lhb;Lhb;)V

    iget-object v1, p0, Lcom/twitter/android/kh;->a:Lcom/twitter/android/MainActivity;

    iget-object v1, v1, Lcom/twitter/android/MainActivity;->m:Landroid/support/v4/view/ViewPager;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    iget-object v0, p0, Lcom/twitter/android/kh;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v0}, Lcom/twitter/android/MainActivity;->C(Lcom/twitter/android/MainActivity;)Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->f()V

    iget-object v0, p0, Lcom/twitter/android/kh;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v0}, Lcom/twitter/android/MainActivity;->B(Lcom/twitter/android/MainActivity;)Lcom/mobeta/android/dslv/DragSortListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p3, v1}, Lcom/mobeta/android/dslv/DragSortListView;->setItemChecked(IZ)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/kg;

    invoke-virtual {v0}, Lcom/twitter/android/kg;->b()V

    goto :goto_0
.end method
