.class final Lcom/crashlytics/android/internal/cn;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/app/Application$ActivityLifecycleCallbacks;


# instance fields
.field private synthetic a:Lcom/crashlytics/android/internal/cm;


# direct methods
.method constructor <init>(Lcom/crashlytics/android/internal/cm;)V
    .locals 0

    iput-object p1, p0, Lcom/crashlytics/android/internal/cn;->a:Lcom/crashlytics/android/internal/cm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onActivityCreated(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 1

    iget-object v0, p0, Lcom/crashlytics/android/internal/cn;->a:Lcom/crashlytics/android/internal/cm;

    iget-object v0, v0, Lcom/crashlytics/android/internal/cm;->a:Lcom/crashlytics/android/internal/cl;

    invoke-static {v0, p1}, Lcom/crashlytics/android/internal/cl;->a(Lcom/crashlytics/android/internal/cl;Landroid/app/Activity;)Lcom/crashlytics/android/internal/cl;

    return-void
.end method

.method public final onActivityDestroyed(Landroid/app/Activity;)V
    .locals 0

    return-void
.end method

.method public final onActivityPaused(Landroid/app/Activity;)V
    .locals 0

    return-void
.end method

.method public final onActivityResumed(Landroid/app/Activity;)V
    .locals 1

    iget-object v0, p0, Lcom/crashlytics/android/internal/cn;->a:Lcom/crashlytics/android/internal/cm;

    iget-object v0, v0, Lcom/crashlytics/android/internal/cm;->a:Lcom/crashlytics/android/internal/cl;

    invoke-static {v0, p1}, Lcom/crashlytics/android/internal/cl;->a(Lcom/crashlytics/android/internal/cl;Landroid/app/Activity;)Lcom/crashlytics/android/internal/cl;

    return-void
.end method

.method public final onActivitySaveInstanceState(Landroid/app/Activity;Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public final onActivityStarted(Landroid/app/Activity;)V
    .locals 1

    iget-object v0, p0, Lcom/crashlytics/android/internal/cn;->a:Lcom/crashlytics/android/internal/cm;

    iget-object v0, v0, Lcom/crashlytics/android/internal/cm;->a:Lcom/crashlytics/android/internal/cl;

    invoke-static {v0, p1}, Lcom/crashlytics/android/internal/cl;->a(Lcom/crashlytics/android/internal/cl;Landroid/app/Activity;)Lcom/crashlytics/android/internal/cl;

    return-void
.end method

.method public final onActivityStopped(Landroid/app/Activity;)V
    .locals 0

    return-void
.end method
