.class public final Lcom/crashlytics/android/d;
.super Lcom/crashlytics/android/internal/ck;
.source "Twttr"


# static fields
.field private static j:Landroid/content/ContextWrapper;

.field private static k:Ljava/lang/String;

.field private static l:Ljava/lang/String;

.field private static m:Ljava/lang/String;

.field private static n:Ljava/lang/String;

.field private static o:Ljava/lang/String;

.field private static p:Ljava/lang/String;

.field private static q:Ljava/lang/String;

.field private static r:Z

.field private static s:Lcom/crashlytics/android/t;

.field private static t:Lcom/crashlytics/android/internal/bu;

.field private static u:F

.field private static v:Lcom/crashlytics/android/d;


# instance fields
.field private final a:J

.field private final b:Ljava/util/concurrent/ConcurrentHashMap;

.field private c:Lcom/crashlytics/android/e;

.field private d:Lcom/crashlytics/android/bc;

.field private e:Lcom/crashlytics/android/internal/bo;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/crashlytics/android/d;->r:Z

    const/4 v0, 0x0

    sput-object v0, Lcom/crashlytics/android/d;->s:Lcom/crashlytics/android/t;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/crashlytics/android/internal/ck;-><init>()V

    iput-object v0, p0, Lcom/crashlytics/android/d;->e:Lcom/crashlytics/android/internal/bo;

    iput-object v0, p0, Lcom/crashlytics/android/d;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/crashlytics/android/d;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/crashlytics/android/d;->h:Ljava/lang/String;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/crashlytics/android/d;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/crashlytics/android/d;->a:J

    return-void
.end method

.method static synthetic a(Lcom/crashlytics/android/d;FI)I
    .locals 1

    int-to-float v0, p2

    mul-float/2addr v0, p1

    float-to-int v0, v0

    return v0
.end method

.method private a(Lcom/crashlytics/android/ac;)Lcom/crashlytics/android/ak;
    .locals 11

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/crashlytics/android/d;->i:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/crashlytics/android/internal/bd;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    sget-object v0, Lcom/crashlytics/android/d;->l:Ljava/lang/String;

    invoke-static {v0}, Lcom/crashlytics/android/internal/ai;->a(Ljava/lang/String;)Lcom/crashlytics/android/internal/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/crashlytics/android/internal/ai;->a()I

    move-result v7

    new-instance v0, Lcom/crashlytics/android/ak;

    sget-object v1, Lcom/crashlytics/android/d;->p:Ljava/lang/String;

    sget-object v2, Lcom/crashlytics/android/d;->k:Ljava/lang/String;

    sget-object v3, Lcom/crashlytics/android/d;->o:Ljava/lang/String;

    sget-object v4, Lcom/crashlytics/android/d;->n:Ljava/lang/String;

    sget-object v6, Lcom/crashlytics/android/d;->m:Ljava/lang/String;

    sget-object v8, Lcom/crashlytics/android/d;->q:Ljava/lang/String;

    const-string/jumbo v9, "0"

    move-object v10, p1

    invoke-direct/range {v0 .. v10}, Lcom/crashlytics/android/ak;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/crashlytics/android/ac;)V

    return-object v0
.end method

.method static synthetic a(Lcom/crashlytics/android/d;)Lcom/crashlytics/android/bc;
    .locals 1

    iget-object v0, p0, Lcom/crashlytics/android/d;->d:Lcom/crashlytics/android/bc;

    return-object v0
.end method

.method public static declared-synchronized a()Lcom/crashlytics/android/d;
    .locals 3

    const-class v1, Lcom/crashlytics/android/d;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/crashlytics/android/internal/cl;->a()Lcom/crashlytics/android/internal/cl;

    move-result-object v0

    const-class v2, Lcom/crashlytics/android/d;

    invoke-virtual {v0, v2}, Lcom/crashlytics/android/internal/cl;->a(Ljava/lang/Class;)Lcom/crashlytics/android/internal/ck;

    move-result-object v0

    check-cast v0, Lcom/crashlytics/android/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :goto_0
    monitor-exit v1

    return-object v0

    :cond_0
    :try_start_1
    sget-object v0, Lcom/crashlytics/android/d;->v:Lcom/crashlytics/android/d;

    if-nez v0, :cond_1

    new-instance v0, Lcom/crashlytics/android/d;

    invoke-direct {v0}, Lcom/crashlytics/android/d;-><init>()V

    sput-object v0, Lcom/crashlytics/android/d;->v:Lcom/crashlytics/android/d;

    :cond_1
    sget-object v0, Lcom/crashlytics/android/d;->v:Lcom/crashlytics/android/d;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 5

    invoke-static {}, Lcom/crashlytics/android/d;->a()Lcom/crashlytics/android/d;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/crashlytics/android/d;->d:Lcom/crashlytics/android/bc;

    if-nez v1, :cond_1

    :cond_0
    invoke-static {}, Lcom/crashlytics/android/internal/cl;->a()Lcom/crashlytics/android/internal/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/crashlytics/android/internal/cl;->b()Lcom/crashlytics/android/internal/ci;

    move-result-object v0

    const-string/jumbo v1, "Crashlytics must be initialized by calling Crashlytics.start(Context) prior to logging messages."

    const/4 v2, 0x0

    invoke-interface {v0, p1, v1, v2}, Lcom/crashlytics/android/internal/ci;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    return-void

    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v3, v0, Lcom/crashlytics/android/d;->a:J

    sub-long/2addr v1, v3

    iget-object v0, v0, Lcom/crashlytics/android/d;->d:Lcom/crashlytics/android/bc;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Lcom/crashlytics/android/internal/bd;->b(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/crashlytics/android/bc;->a(JLjava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {p0, v0}, Lcom/crashlytics/android/d;->a(Landroid/content/Context;F)V

    return-void
.end method

.method public static a(Landroid/content/Context;F)V
    .locals 3

    sput p1, Lcom/crashlytics/android/d;->u:F

    invoke-static {p0}, Lcom/crashlytics/android/internal/bd;->d(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/crashlytics/android/internal/cl;->a()Lcom/crashlytics/android/internal/cl;

    move-result-object v0

    new-instance v1, Lcom/crashlytics/android/internal/a;

    invoke-direct {v1}, Lcom/crashlytics/android/internal/a;-><init>()V

    invoke-virtual {v0, v1}, Lcom/crashlytics/android/internal/cl;->a(Lcom/crashlytics/android/internal/ci;)V

    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/crashlytics/android/internal/ck;

    const/4 v1, 0x0

    invoke-static {}, Lcom/crashlytics/android/d;->a()Lcom/crashlytics/android/d;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lcom/crashlytics/android/internal/c;

    invoke-direct {v2}, Lcom/crashlytics/android/internal/c;-><init>()V

    aput-object v2, v0, v1

    invoke-static {p0, v0}, Lcom/crashlytics/android/internal/cl;->a(Landroid/content/Context;[Lcom/crashlytics/android/internal/ck;)V

    return-void
.end method

.method public static a(Lcom/crashlytics/android/t;)V
    .locals 2

    sget-object v0, Lcom/crashlytics/android/d;->s:Lcom/crashlytics/android/t;

    if-eq v0, p0, :cond_0

    sput-object p0, Lcom/crashlytics/android/d;->s:Lcom/crashlytics/android/t;

    sget-object v0, Lcom/crashlytics/android/d;->t:Lcom/crashlytics/android/internal/bu;

    if-eqz v0, :cond_0

    if-nez p0, :cond_1

    sget-object v0, Lcom/crashlytics/android/d;->t:Lcom/crashlytics/android/internal/bu;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/crashlytics/android/internal/bu;->a(Lcom/crashlytics/android/internal/af;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/crashlytics/android/d;->t:Lcom/crashlytics/android/internal/bu;

    new-instance v1, Lcom/crashlytics/android/ar;

    invoke-direct {v1, p0}, Lcom/crashlytics/android/ar;-><init>(Lcom/crashlytics/android/t;)V

    invoke-virtual {v0, v1}, Lcom/crashlytics/android/internal/bu;->a(Lcom/crashlytics/android/internal/af;)V

    goto :goto_0
.end method

.method static a(Ljava/lang/String;)V
    .locals 2

    invoke-static {}, Lcom/crashlytics/android/internal/cl;->a()Lcom/crashlytics/android/internal/cl;

    move-result-object v0

    const-class v1, Lcom/crashlytics/android/internal/c;

    invoke-virtual {v0, v1}, Lcom/crashlytics/android/internal/cl;->a(Ljava/lang/Class;)Lcom/crashlytics/android/internal/ck;

    move-result-object v0

    check-cast v0, Lcom/crashlytics/android/internal/c;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/crashlytics/android/internal/bh;

    invoke-direct {v1, p0}, Lcom/crashlytics/android/internal/bh;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/crashlytics/android/internal/c;->a(Lcom/crashlytics/android/internal/bh;)V

    :cond_0
    return-void
.end method

.method private declared-synchronized a(Ljava/lang/String;Landroid/content/Context;F)V
    .locals 6

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/crashlytics/android/d;->j:Landroid/content/ContextWrapper;

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/crashlytics/android/internal/cl;->a()Lcom/crashlytics/android/internal/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/crashlytics/android/internal/cl;->b()Lcom/crashlytics/android/internal/ci;

    move-result-object v0

    const-string/jumbo v1, "Crashlytics"

    const-string/jumbo v2, "Crashlytics already started, ignoring re-initialization attempt."

    invoke-interface {v0, v1, v2}, Lcom/crashlytics/android/internal/ci;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    sput-object p1, Lcom/crashlytics/android/d;->p:Ljava/lang/String;

    new-instance v0, Landroid/content/ContextWrapper;

    invoke-virtual {p2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/crashlytics/android/d;->j:Landroid/content/ContextWrapper;

    new-instance v0, Lcom/crashlytics/android/internal/bu;

    invoke-static {}, Lcom/crashlytics/android/internal/cl;->a()Lcom/crashlytics/android/internal/cl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/crashlytics/android/internal/cl;->b()Lcom/crashlytics/android/internal/ci;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/crashlytics/android/internal/bu;-><init>(Lcom/crashlytics/android/internal/ci;)V

    sput-object v0, Lcom/crashlytics/android/d;->t:Lcom/crashlytics/android/internal/bu;

    invoke-static {}, Lcom/crashlytics/android/internal/cl;->a()Lcom/crashlytics/android/internal/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/crashlytics/android/internal/cl;->b()Lcom/crashlytics/android/internal/ci;

    move-result-object v0

    const-string/jumbo v2, "Crashlytics"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Initializing Crashlytics "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/crashlytics/android/d;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/crashlytics/android/internal/ci;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    sget-object v0, Lcom/crashlytics/android/d;->j:Landroid/content/ContextWrapper;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getPackageName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/crashlytics/android/d;->k:Ljava/lang/String;

    sget-object v0, Lcom/crashlytics/android/d;->j:Landroid/content/ContextWrapper;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    sget-object v2, Lcom/crashlytics/android/d;->k:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->getInstallerPackageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/crashlytics/android/d;->l:Ljava/lang/String;

    invoke-static {}, Lcom/crashlytics/android/internal/cl;->a()Lcom/crashlytics/android/internal/cl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/crashlytics/android/internal/cl;->b()Lcom/crashlytics/android/internal/ci;

    move-result-object v2

    const-string/jumbo v3, "Crashlytics"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Installer package name is: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v5, Lcom/crashlytics/android/d;->l:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/crashlytics/android/internal/ci;->a(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v2, Lcom/crashlytics/android/d;->k:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget v2, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/crashlytics/android/d;->n:Ljava/lang/String;

    iget-object v2, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    if-nez v2, :cond_2

    const-string/jumbo v0, "0.0"

    :goto_1
    sput-object v0, Lcom/crashlytics/android/d;->o:Ljava/lang/String;

    invoke-virtual {p2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p2}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/crashlytics/android/d;->m:Ljava/lang/String;

    invoke-virtual {p2}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/crashlytics/android/d;->q:Ljava/lang/String;

    invoke-static {p2}, Lcom/crashlytics/android/internal/bd;->i(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/crashlytics/android/d;->i:Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_2
    :try_start_3
    new-instance v0, Lcom/crashlytics/android/internal/bo;

    sget-object v2, Lcom/crashlytics/android/d;->j:Landroid/content/ContextWrapper;

    invoke-direct {v0, v2}, Lcom/crashlytics/android/internal/bo;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/crashlytics/android/d;->e:Lcom/crashlytics/android/internal/bo;

    iget-object v0, p0, Lcom/crashlytics/android/d;->e:Lcom/crashlytics/android/internal/bo;

    invoke-virtual {v0}, Lcom/crashlytics/android/internal/bo;->h()Ljava/lang/String;

    new-instance v0, Lcom/crashlytics/android/al;

    iget-object v2, p0, Lcom/crashlytics/android/d;->i:Ljava/lang/String;

    sget-object v3, Lcom/crashlytics/android/d;->j:Landroid/content/ContextWrapper;

    const-string/jumbo v4, "com.crashlytics.RequireBuildId"

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Lcom/crashlytics/android/internal/bd;->a(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v3

    invoke-direct {v0, v2, v3}, Lcom/crashlytics/android/al;-><init>(Ljava/lang/String;Z)V

    sget-object v2, Lcom/crashlytics/android/d;->k:Ljava/lang/String;

    invoke-virtual {v0, p1, v2}, Lcom/crashlytics/android/al;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    invoke-static {}, Lcom/crashlytics/android/internal/cl;->a()Lcom/crashlytics/android/internal/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/crashlytics/android/internal/cl;->b()Lcom/crashlytics/android/internal/ci;

    move-result-object v0

    const-string/jumbo v2, "Crashlytics"

    const-string/jumbo v3, "Installing exception handler..."

    invoke-interface {v0, v2, v3}, Lcom/crashlytics/android/internal/ci;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/crashlytics/android/bc;

    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v2

    iget-object v3, p0, Lcom/crashlytics/android/d;->c:Lcom/crashlytics/android/e;

    iget-object v4, p0, Lcom/crashlytics/android/d;->i:Ljava/lang/String;

    invoke-direct {v0, v2, v3, v4}, Lcom/crashlytics/android/bc;-><init>(Ljava/lang/Thread$UncaughtExceptionHandler;Lcom/crashlytics/android/e;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/crashlytics/android/d;->d:Lcom/crashlytics/android/bc;

    iget-object v0, p0, Lcom/crashlytics/android/d;->d:Lcom/crashlytics/android/bc;

    invoke-virtual {v0}, Lcom/crashlytics/android/bc;->f()Z

    move-result v1

    iget-object v0, p0, Lcom/crashlytics/android/d;->d:Lcom/crashlytics/android/bc;

    invoke-virtual {v0}, Lcom/crashlytics/android/bc;->d()V

    iget-object v0, p0, Lcom/crashlytics/android/d;->d:Lcom/crashlytics/android/bc;

    invoke-virtual {v0}, Lcom/crashlytics/android/bc;->c()V

    iget-object v0, p0, Lcom/crashlytics/android/d;->d:Lcom/crashlytics/android/bc;

    invoke-virtual {v0}, Lcom/crashlytics/android/bc;->h()V

    iget-object v0, p0, Lcom/crashlytics/android/d;->d:Lcom/crashlytics/android/bc;

    invoke-static {v0}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    invoke-static {}, Lcom/crashlytics/android/internal/cl;->a()Lcom/crashlytics/android/internal/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/crashlytics/android/internal/cl;->b()Lcom/crashlytics/android/internal/ci;

    move-result-object v0

    const-string/jumbo v2, "Crashlytics"

    const-string/jumbo v3, "Successfully installed exception handler."

    invoke-interface {v0, v2, v3}, Lcom/crashlytics/android/internal/ci;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :goto_3
    :try_start_5
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v2, 0x1

    invoke-direct {v0, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/crashlytics/android/ba;

    invoke-direct {v3, p0, p2, p3, v0}, Lcom/crashlytics/android/ba;-><init>(Lcom/crashlytics/android/d;Landroid/content/Context;FLjava/util/concurrent/CountDownLatch;)V

    const-string/jumbo v4, "Crashlytics Initializer"

    invoke-direct {v2, v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/crashlytics/android/internal/cl;->a()Lcom/crashlytics/android/internal/cl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/crashlytics/android/internal/cl;->b()Lcom/crashlytics/android/internal/ci;

    move-result-object v1

    const-string/jumbo v2, "Crashlytics"

    const-string/jumbo v3, "Crashlytics detected incomplete initialization on previous app launch. Will initialize synchronously."

    invoke-interface {v1, v2, v3}, Lcom/crashlytics/android/internal/ci;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    const-wide/16 v1, 0xfa0

    :try_start_6
    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/crashlytics/android/internal/cl;->a()Lcom/crashlytics/android/internal/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/crashlytics/android/internal/cl;->b()Lcom/crashlytics/android/internal/ci;

    move-result-object v0

    const-string/jumbo v1, "Crashlytics"

    const-string/jumbo v2, "Crashlytics initialization was not completed in the allotted time."

    invoke-interface {v0, v1, v2}, Lcom/crashlytics/android/internal/ci;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    :try_start_7
    invoke-static {}, Lcom/crashlytics/android/internal/cl;->a()Lcom/crashlytics/android/internal/cl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/crashlytics/android/internal/cl;->b()Lcom/crashlytics/android/internal/ci;

    move-result-object v1

    const-string/jumbo v2, "Crashlytics"

    const-string/jumbo v3, "Crashlytics was interrupted during initialization."

    invoke-interface {v1, v2, v3, v0}, Lcom/crashlytics/android/internal/ci;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_8
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_1

    :catch_1
    move-exception v0

    :try_start_9
    invoke-static {}, Lcom/crashlytics/android/internal/cl;->a()Lcom/crashlytics/android/internal/cl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/crashlytics/android/internal/cl;->b()Lcom/crashlytics/android/internal/ci;

    move-result-object v2

    const-string/jumbo v3, "Crashlytics"

    const-string/jumbo v4, "Error setting up app properties"

    invoke-interface {v2, v3, v4, v0}, Lcom/crashlytics/android/internal/ci;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_2

    :catch_2
    move-exception v0

    invoke-static {}, Lcom/crashlytics/android/internal/cl;->a()Lcom/crashlytics/android/internal/cl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/crashlytics/android/internal/cl;->b()Lcom/crashlytics/android/internal/ci;

    move-result-object v2

    const-string/jumbo v3, "Crashlytics"

    const-string/jumbo v4, "There was a problem installing the exception handler."

    invoke-interface {v2, v3, v4, v0}, Lcom/crashlytics/android/internal/ci;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto/16 :goto_3
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    if-nez p0, :cond_1

    sget-object v0, Lcom/crashlytics/android/d;->j:Landroid/content/ContextWrapper;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/crashlytics/android/d;->j:Landroid/content/ContextWrapper;

    invoke-static {v0}, Lcom/crashlytics/android/internal/bd;->f(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Custom attribute key cannot be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {}, Lcom/crashlytics/android/internal/cl;->a()Lcom/crashlytics/android/internal/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/crashlytics/android/internal/cl;->b()Lcom/crashlytics/android/internal/ci;

    move-result-object v0

    const-string/jumbo v1, "Crashlytics"

    const-string/jumbo v2, "Attempting to set custom attribute with null key, ignoring."

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/crashlytics/android/internal/ci;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    return-void

    :cond_1
    invoke-static {p0}, Lcom/crashlytics/android/d;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/crashlytics/android/d;->a()Lcom/crashlytics/android/d;

    move-result-object v0

    iget-object v0, v0, Lcom/crashlytics/android/d;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->size()I

    move-result v0

    const/16 v2, 0x40

    if-lt v0, v2, :cond_2

    invoke-static {}, Lcom/crashlytics/android/d;->a()Lcom/crashlytics/android/d;

    move-result-object v0

    iget-object v0, v0, Lcom/crashlytics/android/d;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_2
    if-nez p1, :cond_3

    const-string/jumbo v0, ""

    :goto_1
    invoke-static {}, Lcom/crashlytics/android/d;->a()Lcom/crashlytics/android/d;

    move-result-object v2

    iget-object v2, v2, Lcom/crashlytics/android/d;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2, v1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_3
    invoke-static {p1}, Lcom/crashlytics/android/d;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    invoke-static {}, Lcom/crashlytics/android/internal/cl;->a()Lcom/crashlytics/android/internal/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/crashlytics/android/internal/cl;->b()Lcom/crashlytics/android/internal/ci;

    move-result-object v0

    const-string/jumbo v1, "Crashlytics"

    const-string/jumbo v2, "Exceeded maximum number of custom attributes (64)"

    invoke-interface {v0, v1, v2}, Lcom/crashlytics/android/internal/ci;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/Throwable;)V
    .locals 4

    invoke-static {}, Lcom/crashlytics/android/d;->a()Lcom/crashlytics/android/d;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/crashlytics/android/d;->d:Lcom/crashlytics/android/bc;

    if-nez v1, :cond_1

    :cond_0
    invoke-static {}, Lcom/crashlytics/android/internal/cl;->a()Lcom/crashlytics/android/internal/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/crashlytics/android/internal/cl;->b()Lcom/crashlytics/android/internal/ci;

    move-result-object v0

    const-string/jumbo v1, "Crashlytics"

    const-string/jumbo v2, "Crashlytics must be initialized by calling Crashlytics.start(Context) prior to logging exceptions."

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/crashlytics/android/internal/ci;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_0
    return-void

    :cond_1
    if-nez p0, :cond_2

    invoke-static {}, Lcom/crashlytics/android/internal/cl;->a()Lcom/crashlytics/android/internal/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/crashlytics/android/internal/cl;->b()Lcom/crashlytics/android/internal/ci;

    move-result-object v0

    const/4 v1, 0x5

    const-string/jumbo v2, "Crashlytics"

    const-string/jumbo v3, "Crashlytics is ignoring a request to log a null exception."

    invoke-interface {v0, v1, v2, v3}, Lcom/crashlytics/android/internal/ci;->a(ILjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, v0, Lcom/crashlytics/android/d;->d:Lcom/crashlytics/android/bc;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lcom/crashlytics/android/bc;->a(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method static a(Z)V
    .locals 3

    invoke-static {}, Lcom/crashlytics/android/internal/bd;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "always_send_reports_opt_in"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method static synthetic a(Lcom/crashlytics/android/d;Landroid/app/Activity;Lcom/crashlytics/android/internal/ar;)Z
    .locals 6

    new-instance v4, Lcom/crashlytics/android/ab;

    invoke-direct {v4, p1, p2}, Lcom/crashlytics/android/ab;-><init>(Landroid/content/Context;Lcom/crashlytics/android/internal/ar;)V

    new-instance v3, Lcom/crashlytics/android/bb;

    const/4 v0, 0x0

    invoke-direct {v3, p0, v0}, Lcom/crashlytics/android/bb;-><init>(Lcom/crashlytics/android/d;B)V

    new-instance v0, Lcom/crashlytics/android/aw;

    move-object v1, p0

    move-object v2, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/crashlytics/android/aw;-><init>(Lcom/crashlytics/android/d;Landroid/app/Activity;Lcom/crashlytics/android/bb;Lcom/crashlytics/android/ab;Lcom/crashlytics/android/internal/ar;)V

    invoke-virtual {p1, v0}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    invoke-static {}, Lcom/crashlytics/android/internal/cl;->a()Lcom/crashlytics/android/internal/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/crashlytics/android/internal/cl;->b()Lcom/crashlytics/android/internal/ci;

    move-result-object v0

    const-string/jumbo v1, "Crashlytics"

    const-string/jumbo v2, "Waiting for user opt-in."

    invoke-interface {v0, v1, v2}, Lcom/crashlytics/android/internal/ci;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/crashlytics/android/bb;->b()V

    invoke-virtual {v3}, Lcom/crashlytics/android/bb;->a()Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/crashlytics/android/d;Landroid/content/Context;F)Z
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/crashlytics/android/d;->b(Landroid/content/Context;F)Z

    move-result v0

    return v0
.end method

.method static b(Ljava/lang/String;)V
    .locals 2

    invoke-static {}, Lcom/crashlytics/android/internal/cl;->a()Lcom/crashlytics/android/internal/cl;

    move-result-object v0

    const-class v1, Lcom/crashlytics/android/internal/c;

    invoke-virtual {v0, v1}, Lcom/crashlytics/android/internal/cl;->a(Ljava/lang/Class;)Lcom/crashlytics/android/internal/ck;

    move-result-object v0

    check-cast v0, Lcom/crashlytics/android/internal/c;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/crashlytics/android/internal/bg;

    invoke-direct {v1, p0}, Lcom/crashlytics/android/internal/bg;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/crashlytics/android/internal/c;->a(Lcom/crashlytics/android/internal/bg;)V

    :cond_0
    return-void
.end method

.method private b(Landroid/content/Context;F)Z
    .locals 10

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/crashlytics/android/d;->w()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/crashlytics/android/internal/bd;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v9

    :try_start_0
    invoke-static {}, Lcom/crashlytics/android/internal/at;->a()Lcom/crashlytics/android/internal/at;

    move-result-object v0

    sget-object v2, Lcom/crashlytics/android/d;->t:Lcom/crashlytics/android/internal/bu;

    sget-object v3, Lcom/crashlytics/android/d;->n:Ljava/lang/String;

    sget-object v4, Lcom/crashlytics/android/d;->o:Ljava/lang/String;

    invoke-static {}, Lcom/crashlytics/android/d;->l()Ljava/lang/String;

    move-result-object v5

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/crashlytics/android/internal/at;->a(Landroid/content/Context;Lcom/crashlytics/android/internal/bu;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/crashlytics/android/internal/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/crashlytics/android/internal/at;->c()Z

    invoke-static {}, Lcom/crashlytics/android/internal/at;->a()Lcom/crashlytics/android/internal/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/crashlytics/android/internal/at;->b()Lcom/crashlytics/android/internal/az;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v2, v0

    :goto_0
    if-eqz v2, :cond_7

    :try_start_1
    iget-object v0, v2, Lcom/crashlytics/android/internal/az;->a:Lcom/crashlytics/android/internal/am;

    const-string/jumbo v1, "new"

    iget-object v3, v0, Lcom/crashlytics/android/internal/am;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/crashlytics/android/d;->w()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v9}, Lcom/crashlytics/android/ac;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/crashlytics/android/ac;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/crashlytics/android/d;->a(Lcom/crashlytics/android/ac;)Lcom/crashlytics/android/ak;

    move-result-object v1

    new-instance v3, Lcom/crashlytics/android/x;

    invoke-static {}, Lcom/crashlytics/android/d;->l()Ljava/lang/String;

    move-result-object v4

    iget-object v0, v0, Lcom/crashlytics/android/internal/am;->b:Ljava/lang/String;

    sget-object v5, Lcom/crashlytics/android/d;->t:Lcom/crashlytics/android/internal/bu;

    invoke-direct {v3, v4, v0, v5}, Lcom/crashlytics/android/x;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/crashlytics/android/internal/bu;)V

    invoke-virtual {v3, v1}, Lcom/crashlytics/android/x;->a(Lcom/crashlytics/android/ak;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/crashlytics/android/internal/at;->a()Lcom/crashlytics/android/internal/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/crashlytics/android/internal/at;->d()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    :goto_1
    move v1, v0

    :goto_2
    :try_start_2
    iget-object v0, v2, Lcom/crashlytics/android/internal/az;->d:Lcom/crashlytics/android/internal/aq;

    iget-boolean v0, v0, Lcom/crashlytics/android/internal/aq;->b:Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    if-eqz v1, :cond_6

    if-eqz v0, :cond_6

    :try_start_3
    iget-object v0, p0, Lcom/crashlytics/android/d;->d:Lcom/crashlytics/android/bc;

    invoke-virtual {v0}, Lcom/crashlytics/android/bc;->b()Z

    move-result v0

    and-int/lit8 v7, v0, 0x1

    invoke-virtual {p0}, Lcom/crashlytics/android/d;->t()Lcom/crashlytics/android/z;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/crashlytics/android/af;

    invoke-direct {v1, v0}, Lcom/crashlytics/android/af;-><init>(Lcom/crashlytics/android/z;)V

    invoke-virtual {v1, p2}, Lcom/crashlytics/android/af;->a(F)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    :cond_0
    :goto_4
    if-eqz v6, :cond_1

    invoke-static {}, Lcom/crashlytics/android/internal/cl;->a()Lcom/crashlytics/android/internal/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/crashlytics/android/internal/cl;->b()Lcom/crashlytics/android/internal/ci;

    move-result-object v0

    const-string/jumbo v1, "Crashlytics"

    const-string/jumbo v2, "Crash reporting disabled."

    invoke-interface {v0, v1, v2}, Lcom/crashlytics/android/internal/ci;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return v7

    :catch_0
    move-exception v0

    invoke-static {}, Lcom/crashlytics/android/internal/cl;->a()Lcom/crashlytics/android/internal/cl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/crashlytics/android/internal/cl;->b()Lcom/crashlytics/android/internal/ci;

    move-result-object v1

    const-string/jumbo v2, "Crashlytics"

    const-string/jumbo v3, "Error dealing with settings"

    invoke-interface {v1, v2, v3, v0}, Lcom/crashlytics/android/internal/ci;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v2, v8

    goto :goto_0

    :cond_2
    :try_start_4
    invoke-static {}, Lcom/crashlytics/android/internal/cl;->a()Lcom/crashlytics/android/internal/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/crashlytics/android/internal/cl;->b()Lcom/crashlytics/android/internal/ci;

    move-result-object v0

    const-string/jumbo v1, "Crashlytics"

    const-string/jumbo v3, "Failed to create app with Crashlytics service."

    const/4 v4, 0x0

    invoke-interface {v0, v1, v3, v4}, Lcom/crashlytics/android/internal/ci;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v6

    goto :goto_1

    :cond_3
    const-string/jumbo v1, "configured"

    iget-object v3, v0, Lcom/crashlytics/android/internal/am;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {}, Lcom/crashlytics/android/internal/at;->a()Lcom/crashlytics/android/internal/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/crashlytics/android/internal/at;->d()Z

    move-result v0

    goto :goto_1

    :cond_4
    iget-boolean v1, v0, Lcom/crashlytics/android/internal/am;->d:Z

    if-eqz v1, :cond_5

    invoke-static {}, Lcom/crashlytics/android/internal/cl;->a()Lcom/crashlytics/android/internal/cl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/crashlytics/android/internal/cl;->b()Lcom/crashlytics/android/internal/ci;

    move-result-object v1

    const-string/jumbo v3, "Crashlytics"

    const-string/jumbo v4, "Server says an update is required - forcing a full App update."

    invoke-interface {v1, v3, v4}, Lcom/crashlytics/android/internal/ci;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/crashlytics/android/d;->w()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v9}, Lcom/crashlytics/android/ac;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/crashlytics/android/ac;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/crashlytics/android/d;->a(Lcom/crashlytics/android/ac;)Lcom/crashlytics/android/ak;

    move-result-object v1

    new-instance v3, Lcom/crashlytics/android/ai;

    invoke-static {}, Lcom/crashlytics/android/d;->l()Ljava/lang/String;

    move-result-object v4

    iget-object v0, v0, Lcom/crashlytics/android/internal/am;->b:Ljava/lang/String;

    sget-object v5, Lcom/crashlytics/android/d;->t:Lcom/crashlytics/android/internal/bu;

    invoke-direct {v3, v4, v0, v5}, Lcom/crashlytics/android/ai;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/crashlytics/android/internal/bu;)V

    invoke-virtual {v3, v1}, Lcom/crashlytics/android/ai;->a(Lcom/crashlytics/android/ak;)Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    :cond_5
    move v0, v7

    goto/16 :goto_1

    :catch_1
    move-exception v0

    invoke-static {}, Lcom/crashlytics/android/internal/cl;->a()Lcom/crashlytics/android/internal/cl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/crashlytics/android/internal/cl;->b()Lcom/crashlytics/android/internal/ci;

    move-result-object v1

    const-string/jumbo v3, "Crashlytics"

    const-string/jumbo v4, "Error performing auto configuration."

    invoke-interface {v1, v3, v4, v0}, Lcom/crashlytics/android/internal/ci;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v1, v6

    goto/16 :goto_2

    :catch_2
    move-exception v0

    invoke-static {}, Lcom/crashlytics/android/internal/cl;->a()Lcom/crashlytics/android/internal/cl;

    move-result-object v2

    invoke-virtual {v2}, Lcom/crashlytics/android/internal/cl;->b()Lcom/crashlytics/android/internal/ci;

    move-result-object v2

    const-string/jumbo v3, "Crashlytics"

    const-string/jumbo v4, "Error getting collect reports setting."

    invoke-interface {v2, v3, v4, v0}, Lcom/crashlytics/android/internal/ci;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v6

    goto/16 :goto_3

    :catch_3
    move-exception v0

    invoke-static {}, Lcom/crashlytics/android/internal/cl;->a()Lcom/crashlytics/android/internal/cl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/crashlytics/android/internal/cl;->b()Lcom/crashlytics/android/internal/ci;

    move-result-object v1

    const-string/jumbo v2, "Crashlytics"

    const-string/jumbo v3, "Error sending crash report"

    invoke-interface {v1, v2, v3, v0}, Lcom/crashlytics/android/internal/ci;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_4

    :cond_6
    move v6, v7

    goto/16 :goto_4

    :cond_7
    move v0, v6

    move v1, v6

    goto/16 :goto_3
.end method

.method public static c()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-static {}, Lcom/crashlytics/android/d;->a()Lcom/crashlytics/android/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/crashlytics/android/d;->f()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static c(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x3

    const-string/jumbo v1, "Crashlytics"

    invoke-static {v0, v1, p0}, Lcom/crashlytics/android/d;->a(ILjava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public static d(Ljava/lang/String;)V
    .locals 2

    invoke-static {}, Lcom/crashlytics/android/d;->a()Lcom/crashlytics/android/d;

    move-result-object v0

    invoke-static {p0}, Lcom/crashlytics/android/d;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/crashlytics/android/d;->h:Ljava/lang/String;

    return-void
.end method

.method private static e(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const/16 v1, 0x400

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    :cond_0
    return-object p0
.end method

.method static g()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/crashlytics/android/d;->k:Ljava/lang/String;

    return-object v0
.end method

.method static h()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/crashlytics/android/d;->l:Ljava/lang/String;

    return-object v0
.end method

.method static i()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/crashlytics/android/d;->o:Ljava/lang/String;

    return-object v0
.end method

.method static j()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/crashlytics/android/d;->n:Ljava/lang/String;

    return-object v0
.end method

.method static k()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/crashlytics/android/d;->m:Ljava/lang/String;

    return-object v0
.end method

.method static l()Ljava/lang/String;
    .locals 2

    sget-object v0, Lcom/crashlytics/android/d;->j:Landroid/content/ContextWrapper;

    const-string/jumbo v1, "com.crashlytics.ApiEndpoint"

    invoke-static {v0, v1}, Lcom/crashlytics/android/internal/bd;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static n()Z
    .locals 3

    invoke-static {}, Lcom/crashlytics/android/internal/bd;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "always_send_reports_opt_in"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method static synthetic v()Lcom/crashlytics/android/internal/bu;
    .locals 1

    sget-object v0, Lcom/crashlytics/android/d;->t:Lcom/crashlytics/android/internal/bu;

    return-object v0
.end method


# virtual methods
.method final b()Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/crashlytics/android/d;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method final d()Lcom/crashlytics/android/internal/bo;
    .locals 1

    iget-object v0, p0, Lcom/crashlytics/android/d;->e:Lcom/crashlytics/android/internal/bo;

    return-object v0
.end method

.method protected final e()V
    .locals 4

    invoke-super {p0}, Lcom/crashlytics/android/internal/ck;->w()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/crashlytics/android/internal/cj;->a(Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    sget v2, Lcom/crashlytics/android/d;->u:F

    invoke-direct {p0, v1, v0, v2}, Lcom/crashlytics/android/d;->a(Ljava/lang/String;Landroid/content/Context;F)V
    :try_end_0
    .catch Lcom/crashlytics/android/CrashlyticsMissingDependencyException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    invoke-static {}, Lcom/crashlytics/android/internal/cl;->a()Lcom/crashlytics/android/internal/cl;

    move-result-object v1

    invoke-virtual {v1}, Lcom/crashlytics/android/internal/cl;->b()Lcom/crashlytics/android/internal/ci;

    move-result-object v1

    const-string/jumbo v2, "Crashlytics"

    const-string/jumbo v3, "Crashlytics was not started due to an exception during initialization"

    invoke-interface {v1, v2, v3, v0}, Lcom/crashlytics/android/internal/ci;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lcom/crashlytics/android/internal/cl;->a()Lcom/crashlytics/android/internal/cl;

    move-result-object v0

    invoke-virtual {v0}, Lcom/crashlytics/android/internal/cl;->f()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method final m()Z
    .locals 3

    invoke-static {}, Lcom/crashlytics/android/internal/at;->a()Lcom/crashlytics/android/internal/at;

    move-result-object v0

    new-instance v1, Lcom/crashlytics/android/as;

    invoke-direct {v1, p0}, Lcom/crashlytics/android/as;-><init>(Lcom/crashlytics/android/d;)V

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/crashlytics/android/internal/at;->a(Lcom/crashlytics/android/internal/aw;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method final o()Lcom/crashlytics/android/bc;
    .locals 1

    iget-object v0, p0, Lcom/crashlytics/android/d;->d:Lcom/crashlytics/android/bc;

    return-object v0
.end method

.method final p()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/crashlytics/android/d;->e:Lcom/crashlytics/android/internal/bo;

    invoke-virtual {v0}, Lcom/crashlytics/android/internal/bo;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/crashlytics/android/d;->f:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final q()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/crashlytics/android/d;->e:Lcom/crashlytics/android/internal/bo;

    invoke-virtual {v0}, Lcom/crashlytics/android/internal/bo;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/crashlytics/android/d;->g:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final r()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/crashlytics/android/d;->e:Lcom/crashlytics/android/internal/bo;

    invoke-virtual {v0}, Lcom/crashlytics/android/internal/bo;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/crashlytics/android/d;->h:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final s()Z
    .locals 3

    invoke-static {}, Lcom/crashlytics/android/internal/at;->a()Lcom/crashlytics/android/internal/at;

    move-result-object v0

    new-instance v1, Lcom/crashlytics/android/at;

    invoke-direct {v1, p0}, Lcom/crashlytics/android/at;-><init>(Lcom/crashlytics/android/d;)V

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/crashlytics/android/internal/at;->a(Lcom/crashlytics/android/internal/aw;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method final t()Lcom/crashlytics/android/z;
    .locals 3

    invoke-static {}, Lcom/crashlytics/android/internal/at;->a()Lcom/crashlytics/android/internal/at;

    move-result-object v0

    new-instance v1, Lcom/crashlytics/android/au;

    invoke-direct {v1, p0}, Lcom/crashlytics/android/au;-><init>(Lcom/crashlytics/android/d;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/crashlytics/android/internal/at;->a(Lcom/crashlytics/android/internal/aw;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/crashlytics/android/z;

    return-object v0
.end method

.method final u()Lcom/crashlytics/android/internal/as;
    .locals 3

    invoke-static {}, Lcom/crashlytics/android/internal/at;->a()Lcom/crashlytics/android/internal/at;

    move-result-object v0

    new-instance v1, Lcom/crashlytics/android/av;

    invoke-direct {v1, p0}, Lcom/crashlytics/android/av;-><init>(Lcom/crashlytics/android/d;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/crashlytics/android/internal/at;->a(Lcom/crashlytics/android/internal/aw;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/crashlytics/android/internal/as;

    return-object v0
.end method
