.class Lcom/google/android/gms/maps/p;
.super Lcom/google/android/gms/dynamic/b;


# instance fields
.field protected a:Lcom/google/android/gms/dynamic/o;

.field private final b:Landroid/view/ViewGroup;

.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/android/gms/maps/GoogleMapOptions;


# direct methods
.method constructor <init>(Landroid/view/ViewGroup;Landroid/content/Context;Lcom/google/android/gms/maps/GoogleMapOptions;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/dynamic/b;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/maps/p;->b:Landroid/view/ViewGroup;

    iput-object p2, p0, Lcom/google/android/gms/maps/p;->c:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/gms/maps/p;->d:Lcom/google/android/gms/maps/GoogleMapOptions;

    return-void
.end method


# virtual methods
.method protected a(Lcom/google/android/gms/dynamic/o;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/maps/p;->a:Lcom/google/android/gms/dynamic/o;

    invoke-virtual {p0}, Lcom/google/android/gms/maps/p;->g()V

    return-void
.end method

.method public g()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/maps/p;->a:Lcom/google/android/gms/dynamic/o;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/maps/p;->a()Lcom/google/android/gms/dynamic/a;

    move-result-object v0

    if-nez v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/maps/p;->c:Landroid/content/Context;

    invoke-static {v0}, Lct;->a(Landroid/content/Context;)Lbf;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/maps/p;->c:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/dynamic/n;->a(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/k;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/maps/p;->d:Lcom/google/android/gms/maps/GoogleMapOptions;

    invoke-interface {v0, v1, v2}, Lbf;->a(Lcom/google/android/gms/dynamic/k;Lcom/google/android/gms/maps/GoogleMapOptions;)Las;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/maps/p;->a:Lcom/google/android/gms/dynamic/o;

    new-instance v2, Lcom/google/android/gms/maps/o;

    iget-object v3, p0, Lcom/google/android/gms/maps/p;->b:Landroid/view/ViewGroup;

    invoke-direct {v2, v3, v0}, Lcom/google/android/gms/maps/o;-><init>(Landroid/view/ViewGroup;Las;)V

    invoke-interface {v1, v2}, Lcom/google/android/gms/dynamic/o;->a(Lcom/google/android/gms/dynamic/a;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/common/GooglePlayServicesNotAvailableException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1

    :catch_1
    move-exception v0

    goto :goto_0
.end method
