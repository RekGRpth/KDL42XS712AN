.class public interface abstract Lcom/google/android/gms/internal/ep;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract a()Landroid/location/Location;
.end method

.method public abstract a(JZLandroid/app/PendingIntent;)V
.end method

.method public abstract a(Landroid/app/PendingIntent;)V
.end method

.method public abstract a(Landroid/app/PendingIntent;Lcom/google/android/gms/internal/el;Ljava/lang/String;)V
.end method

.method public abstract a(Landroid/location/Location;)V
.end method

.method public abstract a(Lcom/google/android/gms/internal/el;Ljava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/LocationRequest;Landroid/app/PendingIntent;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/LocationRequest;Lcom/google/android/gms/location/f;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/LocationRequest;Lcom/google/android/gms/location/f;Ljava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/f;)V
.end method

.method public abstract a(Ljava/util/List;Landroid/app/PendingIntent;Lcom/google/android/gms/internal/el;Ljava/lang/String;)V
.end method

.method public abstract a(Z)V
.end method

.method public abstract a([Ljava/lang/String;Lcom/google/android/gms/internal/el;Ljava/lang/String;)V
.end method

.method public abstract b(Landroid/app/PendingIntent;)V
.end method
