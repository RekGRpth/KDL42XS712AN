.class public final Lcom/google/android/gms/internal/cr;
.super Lcom/google/android/gms/internal/cz;


# instance fields
.field private a:Lcom/google/android/gms/internal/cm;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/cm;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/internal/cz;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/internal/cr;->a:Lcom/google/android/gms/internal/cm;

    return-void
.end method


# virtual methods
.method public a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 2

    const-string/jumbo v0, "onPostInitComplete can be called only once per call to getServiceFromBroker"

    iget-object v1, p0, Lcom/google/android/gms/internal/cr;->a:Lcom/google/android/gms/internal/cm;

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/dm;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/internal/cr;->a:Lcom/google/android/gms/internal/cm;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/internal/cm;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/cr;->a:Lcom/google/android/gms/internal/cm;

    return-void
.end method
