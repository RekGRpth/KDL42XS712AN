.class public final Lcom/google/android/gms/internal/bh;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/internal/ak;


# instance fields
.field public final a:I

.field public final b:Lcom/google/android/gms/internal/be;

.field public final c:Lcom/google/android/gms/internal/gd;

.field public final d:Lcom/google/android/gms/internal/al;

.field public final e:Lcom/google/android/gms/internal/cq;

.field public final f:Lcom/google/android/gms/internal/e;

.field public final g:Ljava/lang/String;

.field public final h:Z

.field public final i:Ljava/lang/String;

.field public final j:Lcom/google/android/gms/internal/ao;

.field public final k:I

.field public final l:I

.field public final m:Ljava/lang/String;

.field public final n:Lcom/google/android/gms/internal/co;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/ak;

    invoke-direct {v0}, Lcom/google/android/gms/internal/ak;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/bh;->CREATOR:Lcom/google/android/gms/internal/ak;

    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/internal/be;Landroid/os/IBinder;Landroid/os/IBinder;Landroid/os/IBinder;Landroid/os/IBinder;Ljava/lang/String;ZLjava/lang/String;Landroid/os/IBinder;IILjava/lang/String;Lcom/google/android/gms/internal/co;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/bh;->a:I

    iput-object p2, p0, Lcom/google/android/gms/internal/bh;->b:Lcom/google/android/gms/internal/be;

    invoke-static {p3}, Lcom/google/android/gms/dynamic/l;->a(Landroid/os/IBinder;)Lcom/google/android/gms/dynamic/k;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/dynamic/n;->a(Lcom/google/android/gms/dynamic/k;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/gd;

    iput-object v0, p0, Lcom/google/android/gms/internal/bh;->c:Lcom/google/android/gms/internal/gd;

    invoke-static {p4}, Lcom/google/android/gms/dynamic/l;->a(Landroid/os/IBinder;)Lcom/google/android/gms/dynamic/k;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/dynamic/n;->a(Lcom/google/android/gms/dynamic/k;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/al;

    iput-object v0, p0, Lcom/google/android/gms/internal/bh;->d:Lcom/google/android/gms/internal/al;

    invoke-static {p5}, Lcom/google/android/gms/dynamic/l;->a(Landroid/os/IBinder;)Lcom/google/android/gms/dynamic/k;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/dynamic/n;->a(Lcom/google/android/gms/dynamic/k;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/cq;

    iput-object v0, p0, Lcom/google/android/gms/internal/bh;->e:Lcom/google/android/gms/internal/cq;

    invoke-static {p6}, Lcom/google/android/gms/dynamic/l;->a(Landroid/os/IBinder;)Lcom/google/android/gms/dynamic/k;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/dynamic/n;->a(Lcom/google/android/gms/dynamic/k;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/e;

    iput-object v0, p0, Lcom/google/android/gms/internal/bh;->f:Lcom/google/android/gms/internal/e;

    iput-object p7, p0, Lcom/google/android/gms/internal/bh;->g:Ljava/lang/String;

    iput-boolean p8, p0, Lcom/google/android/gms/internal/bh;->h:Z

    iput-object p9, p0, Lcom/google/android/gms/internal/bh;->i:Ljava/lang/String;

    invoke-static {p10}, Lcom/google/android/gms/dynamic/l;->a(Landroid/os/IBinder;)Lcom/google/android/gms/dynamic/k;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/dynamic/n;->a(Lcom/google/android/gms/dynamic/k;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/ao;

    iput-object v0, p0, Lcom/google/android/gms/internal/bh;->j:Lcom/google/android/gms/internal/ao;

    iput p11, p0, Lcom/google/android/gms/internal/bh;->k:I

    iput p12, p0, Lcom/google/android/gms/internal/bh;->l:I

    iput-object p13, p0, Lcom/google/android/gms/internal/bh;->m:Ljava/lang/String;

    iput-object p14, p0, Lcom/google/android/gms/internal/bh;->n:Lcom/google/android/gms/internal/co;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/internal/be;Lcom/google/android/gms/internal/gd;Lcom/google/android/gms/internal/al;Lcom/google/android/gms/internal/ao;Lcom/google/android/gms/internal/co;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/bh;->a:I

    iput-object p1, p0, Lcom/google/android/gms/internal/bh;->b:Lcom/google/android/gms/internal/be;

    iput-object p2, p0, Lcom/google/android/gms/internal/bh;->c:Lcom/google/android/gms/internal/gd;

    iput-object p3, p0, Lcom/google/android/gms/internal/bh;->d:Lcom/google/android/gms/internal/al;

    iput-object v1, p0, Lcom/google/android/gms/internal/bh;->e:Lcom/google/android/gms/internal/cq;

    iput-object v1, p0, Lcom/google/android/gms/internal/bh;->f:Lcom/google/android/gms/internal/e;

    iput-object v1, p0, Lcom/google/android/gms/internal/bh;->g:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/bh;->h:Z

    iput-object v1, p0, Lcom/google/android/gms/internal/bh;->i:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/internal/bh;->j:Lcom/google/android/gms/internal/ao;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/internal/bh;->k:I

    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/gms/internal/bh;->l:I

    iput-object v1, p0, Lcom/google/android/gms/internal/bh;->m:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/gms/internal/bh;->n:Lcom/google/android/gms/internal/co;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/internal/gd;Lcom/google/android/gms/internal/al;Lcom/google/android/gms/internal/ao;Lcom/google/android/gms/internal/cq;ZILcom/google/android/gms/internal/co;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/bh;->a:I

    iput-object v1, p0, Lcom/google/android/gms/internal/bh;->b:Lcom/google/android/gms/internal/be;

    iput-object p1, p0, Lcom/google/android/gms/internal/bh;->c:Lcom/google/android/gms/internal/gd;

    iput-object p2, p0, Lcom/google/android/gms/internal/bh;->d:Lcom/google/android/gms/internal/al;

    iput-object p4, p0, Lcom/google/android/gms/internal/bh;->e:Lcom/google/android/gms/internal/cq;

    iput-object v1, p0, Lcom/google/android/gms/internal/bh;->f:Lcom/google/android/gms/internal/e;

    iput-object v1, p0, Lcom/google/android/gms/internal/bh;->g:Ljava/lang/String;

    iput-boolean p5, p0, Lcom/google/android/gms/internal/bh;->h:Z

    iput-object v1, p0, Lcom/google/android/gms/internal/bh;->i:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/internal/bh;->j:Lcom/google/android/gms/internal/ao;

    iput p6, p0, Lcom/google/android/gms/internal/bh;->k:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/internal/bh;->l:I

    iput-object v1, p0, Lcom/google/android/gms/internal/bh;->m:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/gms/internal/bh;->n:Lcom/google/android/gms/internal/co;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/internal/gd;Lcom/google/android/gms/internal/al;Lcom/google/android/gms/internal/e;Lcom/google/android/gms/internal/ao;Lcom/google/android/gms/internal/cq;ZILjava/lang/String;Lcom/google/android/gms/internal/co;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/bh;->a:I

    iput-object v1, p0, Lcom/google/android/gms/internal/bh;->b:Lcom/google/android/gms/internal/be;

    iput-object p1, p0, Lcom/google/android/gms/internal/bh;->c:Lcom/google/android/gms/internal/gd;

    iput-object p2, p0, Lcom/google/android/gms/internal/bh;->d:Lcom/google/android/gms/internal/al;

    iput-object p5, p0, Lcom/google/android/gms/internal/bh;->e:Lcom/google/android/gms/internal/cq;

    iput-object p3, p0, Lcom/google/android/gms/internal/bh;->f:Lcom/google/android/gms/internal/e;

    iput-object v1, p0, Lcom/google/android/gms/internal/bh;->g:Ljava/lang/String;

    iput-boolean p6, p0, Lcom/google/android/gms/internal/bh;->h:Z

    iput-object v1, p0, Lcom/google/android/gms/internal/bh;->i:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/internal/bh;->j:Lcom/google/android/gms/internal/ao;

    iput p7, p0, Lcom/google/android/gms/internal/bh;->k:I

    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/gms/internal/bh;->l:I

    iput-object p8, p0, Lcom/google/android/gms/internal/bh;->m:Ljava/lang/String;

    iput-object p9, p0, Lcom/google/android/gms/internal/bh;->n:Lcom/google/android/gms/internal/co;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/internal/gd;Lcom/google/android/gms/internal/al;Lcom/google/android/gms/internal/e;Lcom/google/android/gms/internal/ao;Lcom/google/android/gms/internal/cq;ZILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/internal/co;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/bh;->a:I

    iput-object v1, p0, Lcom/google/android/gms/internal/bh;->b:Lcom/google/android/gms/internal/be;

    iput-object p1, p0, Lcom/google/android/gms/internal/bh;->c:Lcom/google/android/gms/internal/gd;

    iput-object p2, p0, Lcom/google/android/gms/internal/bh;->d:Lcom/google/android/gms/internal/al;

    iput-object p5, p0, Lcom/google/android/gms/internal/bh;->e:Lcom/google/android/gms/internal/cq;

    iput-object p3, p0, Lcom/google/android/gms/internal/bh;->f:Lcom/google/android/gms/internal/e;

    iput-object p9, p0, Lcom/google/android/gms/internal/bh;->g:Ljava/lang/String;

    iput-boolean p6, p0, Lcom/google/android/gms/internal/bh;->h:Z

    iput-object p8, p0, Lcom/google/android/gms/internal/bh;->i:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/internal/bh;->j:Lcom/google/android/gms/internal/ao;

    iput p7, p0, Lcom/google/android/gms/internal/bh;->k:I

    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/gms/internal/bh;->l:I

    iput-object v1, p0, Lcom/google/android/gms/internal/bh;->m:Ljava/lang/String;

    iput-object p10, p0, Lcom/google/android/gms/internal/bh;->n:Lcom/google/android/gms/internal/co;

    return-void
.end method

.method public static a(Landroid/content/Intent;)Lcom/google/android/gms/internal/bh;
    .locals 2

    :try_start_0
    const-string/jumbo v0, "com.google.android.gms.ads.inernal.overlay.AdOverlayInfo"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    const-class v1, Lcom/google/android/gms/internal/bh;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    const-string/jumbo v1, "com.google.android.gms.ads.inernal.overlay.AdOverlayInfo"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/bh;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Intent;Lcom/google/android/gms/internal/bh;)V
    .locals 2

    new-instance v0, Landroid/os/Bundle;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(I)V

    const-string/jumbo v1, "com.google.android.gms.ads.inernal.overlay.AdOverlayInfo"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v1, "com.google.android.gms.ads.inernal.overlay.AdOverlayInfo"

    invoke-virtual {p0, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    return-void
.end method


# virtual methods
.method a()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/bh;->c:Lcom/google/android/gms/internal/gd;

    invoke-static {v0}, Lcom/google/android/gms/dynamic/n;->a(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/k;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/dynamic/k;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method b()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/bh;->d:Lcom/google/android/gms/internal/al;

    invoke-static {v0}, Lcom/google/android/gms/dynamic/n;->a(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/k;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/dynamic/k;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method c()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/bh;->e:Lcom/google/android/gms/internal/cq;

    invoke-static {v0}, Lcom/google/android/gms/dynamic/n;->a(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/k;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/dynamic/k;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method d()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/bh;->f:Lcom/google/android/gms/internal/e;

    invoke-static {v0}, Lcom/google/android/gms/dynamic/n;->a(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/k;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/dynamic/k;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method e()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/bh;->j:Lcom/google/android/gms/internal/ao;

    invoke-static {v0}, Lcom/google/android/gms/dynamic/n;->a(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/k;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/dynamic/k;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/internal/ak;->a(Lcom/google/android/gms/internal/bh;Landroid/os/Parcel;I)V

    return-void
.end method
