.class public abstract Lcom/google/android/gms/internal/ax;
.super Ljava/lang/Object;


# instance fields
.field private final a:Ljava/lang/Runnable;

.field private volatile b:Ljava/lang/Thread;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/android/gms/internal/ay;

    invoke-direct {v0, p0}, Lcom/google/android/gms/internal/ay;-><init>(Lcom/google/android/gms/internal/ax;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/ax;->a:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/internal/ax;Ljava/lang/Thread;)Ljava/lang/Thread;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/ax;->b:Ljava/lang/Thread;

    return-object p1
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/ax;->a:Ljava/lang/Runnable;

    invoke-static {v0}, Lcom/google/android/gms/internal/az;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public abstract b()V
.end method
