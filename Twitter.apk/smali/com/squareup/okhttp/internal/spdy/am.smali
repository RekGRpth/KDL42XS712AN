.class final Lcom/squareup/okhttp/internal/spdy/am;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lga;


# static fields
.field static final synthetic a:Z


# instance fields
.field final synthetic b:Lcom/squareup/okhttp/internal/spdy/ak;

.field private c:Z

.field private d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/squareup/okhttp/internal/spdy/am;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Lcom/squareup/okhttp/internal/spdy/ak;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/okhttp/internal/spdy/am;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/squareup/okhttp/internal/spdy/am;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/spdy/am;->d:Z

    return v0
.end method

.method static synthetic a(Lcom/squareup/okhttp/internal/spdy/am;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/squareup/okhttp/internal/spdy/am;->d:Z

    return p1
.end method

.method static synthetic b(Lcom/squareup/okhttp/internal/spdy/am;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/spdy/am;->c:Z

    return v0
.end method


# virtual methods
.method public a()V
    .locals 2

    sget-boolean v0, Lcom/squareup/okhttp/internal/spdy/am;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/am;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/am;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/am;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/spdy/ak;->f(Lcom/squareup/okhttp/internal/spdy/ak;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/am;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/spdy/ak;->a(Lcom/squareup/okhttp/internal/spdy/ak;)Lcom/squareup/okhttp/internal/spdy/y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/spdy/y;->d()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Lfo;J)V
    .locals 8

    const-wide/16 v6, 0x0

    sget-boolean v0, Lcom/squareup/okhttp/internal/spdy/am;->a:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/am;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/am;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/spdy/ak;->f(Lcom/squareup/okhttp/internal/spdy/ak;)V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/am;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    iget-wide v2, v0, Lcom/squareup/okhttp/internal/spdy/ak;->b:J

    invoke-static {v2, v3, p2, p3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/am;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    iget-wide v2, v0, Lcom/squareup/okhttp/internal/spdy/ak;->b:J

    sub-long/2addr v2, v4

    iput-wide v2, v0, Lcom/squareup/okhttp/internal/spdy/ak;->b:J

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sub-long/2addr p2, v4

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/am;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/spdy/ak;->a(Lcom/squareup/okhttp/internal/spdy/ak;)Lcom/squareup/okhttp/internal/spdy/y;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/am;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-static {v1}, Lcom/squareup/okhttp/internal/spdy/ak;->b(Lcom/squareup/okhttp/internal/spdy/ak;)I

    move-result v1

    const/4 v2, 0x0

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/okhttp/internal/spdy/y;->a(IZLfo;J)V

    :cond_1
    cmp-long v0, p2, v6

    if-lez v0, :cond_2

    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/am;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    monitor-enter v1

    :goto_0
    :try_start_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/am;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    iget-wide v2, v0, Lcom/squareup/okhttp/internal/spdy/ak;->b:J

    cmp-long v0, v2, v6

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/am;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    new-instance v0, Ljava/io/InterruptedIOException;

    invoke-direct {v0}, Ljava/io/InterruptedIOException;-><init>()V

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_2
    return-void
.end method

.method public close()V
    .locals 6

    const/4 v2, 0x1

    sget-boolean v0, Lcom/squareup/okhttp/internal/spdy/am;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/am;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/am;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/spdy/am;->c:Z

    if-eqz v0, :cond_1

    monitor-exit v1

    :goto_0
    return-void

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/am;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    iget-object v0, v0, Lcom/squareup/okhttp/internal/spdy/ak;->c:Lcom/squareup/okhttp/internal/spdy/am;

    iget-boolean v0, v0, Lcom/squareup/okhttp/internal/spdy/am;->d:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/am;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/spdy/ak;->a(Lcom/squareup/okhttp/internal/spdy/ak;)Lcom/squareup/okhttp/internal/spdy/y;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/am;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-static {v1}, Lcom/squareup/okhttp/internal/spdy/ak;->b(Lcom/squareup/okhttp/internal/spdy/ak;)I

    move-result v1

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/squareup/okhttp/internal/spdy/y;->a(IZLfo;J)V

    :cond_2
    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/am;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/squareup/okhttp/internal/spdy/am;->c:Z

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/am;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/spdy/ak;->a(Lcom/squareup/okhttp/internal/spdy/ak;)Lcom/squareup/okhttp/internal/spdy/y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/spdy/y;->d()V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/am;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/spdy/ak;->e(Lcom/squareup/okhttp/internal/spdy/ak;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
