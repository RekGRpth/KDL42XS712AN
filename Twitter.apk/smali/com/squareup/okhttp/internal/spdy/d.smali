.class public final Lcom/squareup/okhttp/internal/spdy/d;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final a:Lfi;

.field public static final b:Lfi;

.field public static final c:Lfi;

.field public static final d:Lfi;

.field public static final e:Lfi;

.field public static final f:Lfi;

.field public static final g:Lfi;


# instance fields
.field public final h:Lfi;

.field public final i:Lfi;

.field final j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string/jumbo v0, ":status"

    invoke-static {v0}, Lfi;->a(Ljava/lang/String;)Lfi;

    move-result-object v0

    sput-object v0, Lcom/squareup/okhttp/internal/spdy/d;->a:Lfi;

    const-string/jumbo v0, ":method"

    invoke-static {v0}, Lfi;->a(Ljava/lang/String;)Lfi;

    move-result-object v0

    sput-object v0, Lcom/squareup/okhttp/internal/spdy/d;->b:Lfi;

    const-string/jumbo v0, ":path"

    invoke-static {v0}, Lfi;->a(Ljava/lang/String;)Lfi;

    move-result-object v0

    sput-object v0, Lcom/squareup/okhttp/internal/spdy/d;->c:Lfi;

    const-string/jumbo v0, ":scheme"

    invoke-static {v0}, Lfi;->a(Ljava/lang/String;)Lfi;

    move-result-object v0

    sput-object v0, Lcom/squareup/okhttp/internal/spdy/d;->d:Lfi;

    const-string/jumbo v0, ":authority"

    invoke-static {v0}, Lfi;->a(Ljava/lang/String;)Lfi;

    move-result-object v0

    sput-object v0, Lcom/squareup/okhttp/internal/spdy/d;->e:Lfi;

    const-string/jumbo v0, ":host"

    invoke-static {v0}, Lfi;->a(Ljava/lang/String;)Lfi;

    move-result-object v0

    sput-object v0, Lcom/squareup/okhttp/internal/spdy/d;->f:Lfi;

    const-string/jumbo v0, ":version"

    invoke-static {v0}, Lfi;->a(Ljava/lang/String;)Lfi;

    move-result-object v0

    sput-object v0, Lcom/squareup/okhttp/internal/spdy/d;->g:Lfi;

    return-void
.end method

.method public constructor <init>(Lfi;Lfi;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/okhttp/internal/spdy/d;->h:Lfi;

    iput-object p2, p0, Lcom/squareup/okhttp/internal/spdy/d;->i:Lfi;

    invoke-virtual {p1}, Lfi;->e()I

    move-result v0

    add-int/lit8 v0, v0, 0x20

    invoke-virtual {p2}, Lfi;->e()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/squareup/okhttp/internal/spdy/d;->j:I

    return-void
.end method

.method public constructor <init>(Lfi;Ljava/lang/String;)V
    .locals 1

    invoke-static {p2}, Lfi;->a(Ljava/lang/String;)Lfi;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/squareup/okhttp/internal/spdy/d;-><init>(Lfi;Lfi;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Lfi;->a(Ljava/lang/String;)Lfi;

    move-result-object v0

    invoke-static {p2}, Lfi;->a(Ljava/lang/String;)Lfi;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/squareup/okhttp/internal/spdy/d;-><init>(Lfi;Lfi;)V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, Lcom/squareup/okhttp/internal/spdy/d;

    if-eqz v1, :cond_0

    check-cast p1, Lcom/squareup/okhttp/internal/spdy/d;

    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/d;->h:Lfi;

    iget-object v2, p1, Lcom/squareup/okhttp/internal/spdy/d;->h:Lfi;

    invoke-virtual {v1, v2}, Lfi;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/d;->i:Lfi;

    iget-object v2, p1, Lcom/squareup/okhttp/internal/spdy/d;->i:Lfi;

    invoke-virtual {v1, v2}, Lfi;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/d;->h:Lfi;

    invoke-virtual {v0}, Lfi;->hashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/d;->i:Lfi;

    invoke-virtual {v1}, Lfi;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const-string/jumbo v0, "%s: %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/squareup/okhttp/internal/spdy/d;->h:Lfi;

    invoke-virtual {v3}, Lfi;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/squareup/okhttp/internal/spdy/d;->i:Lfi;

    invoke-virtual {v3}, Lfi;->a()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
