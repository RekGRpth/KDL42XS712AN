.class final Lcom/squareup/okhttp/internal/spdy/j;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/squareup/okhttp/internal/spdy/a;


# instance fields
.field final a:Lcom/squareup/okhttp/internal/spdy/f;

.field private final b:Lfh;

.field private final c:Lcom/squareup/okhttp/internal/spdy/i;

.field private final d:Z


# direct methods
.method constructor <init>(Lfh;IZ)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/okhttp/internal/spdy/j;->b:Lfh;

    iput-boolean p3, p0, Lcom/squareup/okhttp/internal/spdy/j;->d:Z

    new-instance v0, Lcom/squareup/okhttp/internal/spdy/i;

    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/j;->b:Lfh;

    invoke-direct {v0, v1}, Lcom/squareup/okhttp/internal/spdy/i;-><init>(Lfh;)V

    iput-object v0, p0, Lcom/squareup/okhttp/internal/spdy/j;->c:Lcom/squareup/okhttp/internal/spdy/i;

    new-instance v0, Lcom/squareup/okhttp/internal/spdy/f;

    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/j;->c:Lcom/squareup/okhttp/internal/spdy/i;

    invoke-direct {v0, p3, p2, v1}, Lcom/squareup/okhttp/internal/spdy/f;-><init>(ZILgb;)V

    iput-object v0, p0, Lcom/squareup/okhttp/internal/spdy/j;->a:Lcom/squareup/okhttp/internal/spdy/f;

    return-void
.end method

.method private a(SBI)Ljava/util/List;
    .locals 2

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/j;->c:Lcom/squareup/okhttp/internal/spdy/i;

    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/j;->c:Lcom/squareup/okhttp/internal/spdy/i;

    iput p1, v1, Lcom/squareup/okhttp/internal/spdy/i;->d:I

    iput p1, v0, Lcom/squareup/okhttp/internal/spdy/i;->a:I

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/j;->c:Lcom/squareup/okhttp/internal/spdy/i;

    iput-byte p2, v0, Lcom/squareup/okhttp/internal/spdy/i;->b:B

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/j;->c:Lcom/squareup/okhttp/internal/spdy/i;

    iput p3, v0, Lcom/squareup/okhttp/internal/spdy/i;->c:I

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/j;->a:Lcom/squareup/okhttp/internal/spdy/f;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/spdy/f;->a()V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/j;->a:Lcom/squareup/okhttp/internal/spdy/f;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/spdy/f;->b()V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/j;->a:Lcom/squareup/okhttp/internal/spdy/f;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/spdy/f;->c()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/squareup/okhttp/internal/spdy/b;SBI)V
    .locals 8

    const/4 v4, -0x1

    const/4 v1, 0x0

    if-nez p4, :cond_0

    const-string/jumbo v0, "PROTOCOL_ERROR: TYPE_HEADERS streamId == 0"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/squareup/okhttp/internal/spdy/h;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_0
    and-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_1

    const/4 v2, 0x1

    :goto_0
    and-int/lit8 v0, p3, 0x8

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/j;->b:Lfh;

    invoke-interface {v0}, Lfh;->i()I

    move-result v0

    const v3, 0x7fffffff

    and-int v5, v0, v3

    add-int/lit8 v0, p2, -0x4

    int-to-short p2, v0

    :goto_1
    invoke-direct {p0, p2, p3, p4}, Lcom/squareup/okhttp/internal/spdy/j;->a(SBI)Ljava/util/List;

    move-result-object v6

    sget-object v7, Lcom/squareup/okhttp/internal/spdy/HeadersMode;->d:Lcom/squareup/okhttp/internal/spdy/HeadersMode;

    move-object v0, p1

    move v3, p4

    invoke-interface/range {v0 .. v7}, Lcom/squareup/okhttp/internal/spdy/b;->a(ZZIIILjava/util/List;Lcom/squareup/okhttp/internal/spdy/HeadersMode;)V

    return-void

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v5, v4

    goto :goto_1
.end method

.method private b(Lcom/squareup/okhttp/internal/spdy/b;SBI)V
    .locals 2

    and-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/j;->b:Lfh;

    invoke-interface {p1, v0, p4, v1, p2}, Lcom/squareup/okhttp/internal/spdy/b;->a(ZILfh;I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Lcom/squareup/okhttp/internal/spdy/b;SBI)V
    .locals 4

    const/4 v3, 0x0

    const/4 v0, 0x4

    if-eq p2, v0, :cond_0

    const-string/jumbo v0, "TYPE_PRIORITY length: %d != 4"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/okhttp/internal/spdy/h;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_0
    if-nez p4, :cond_1

    const-string/jumbo v0, "TYPE_PRIORITY streamId == 0"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/squareup/okhttp/internal/spdy/h;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/j;->b:Lfh;

    invoke-interface {v0}, Lfh;->i()I

    move-result v0

    const v1, 0x7fffffff

    and-int/2addr v0, v1

    invoke-interface {p1, p4, v0}, Lcom/squareup/okhttp/internal/spdy/b;->a(II)V

    return-void
.end method

.method private d(Lcom/squareup/okhttp/internal/spdy/b;SBI)V
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v0, 0x4

    if-eq p2, v0, :cond_0

    const-string/jumbo v0, "TYPE_RST_STREAM length: %d != 4"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/okhttp/internal/spdy/h;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_0
    if-nez p4, :cond_1

    const-string/jumbo v0, "TYPE_RST_STREAM streamId == 0"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/squareup/okhttp/internal/spdy/h;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/j;->b:Lfh;

    invoke-interface {v0}, Lfh;->i()I

    move-result v0

    invoke-static {v0}, Lcom/squareup/okhttp/internal/spdy/ErrorCode;->b(I)Lcom/squareup/okhttp/internal/spdy/ErrorCode;

    move-result-object v1

    if-nez v1, :cond_2

    const-string/jumbo v1, "TYPE_RST_STREAM unexpected error code: %d"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/squareup/okhttp/internal/spdy/h;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_2
    invoke-interface {p1, p4, v1}, Lcom/squareup/okhttp/internal/spdy/b;->a(ILcom/squareup/okhttp/internal/spdy/ErrorCode;)V

    return-void
.end method

.method private e(Lcom/squareup/okhttp/internal/spdy/b;SBI)V
    .locals 6

    const/4 v1, 0x0

    if-eqz p4, :cond_0

    const-string/jumbo v0, "TYPE_SETTINGS streamId != 0"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/squareup/okhttp/internal/spdy/h;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_0
    and-int/lit8 v0, p3, 0x1

    if-eqz v0, :cond_3

    if-eqz p2, :cond_1

    const-string/jumbo v0, "FRAME_SIZE_ERROR ack frame should be empty!"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/squareup/okhttp/internal/spdy/h;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_1
    invoke-interface {p1}, Lcom/squareup/okhttp/internal/spdy/b;->b()V

    :cond_2
    :goto_0
    return-void

    :cond_3
    rem-int/lit8 v0, p2, 0x8

    if-eqz v0, :cond_4

    const-string/jumbo v0, "TYPE_SETTINGS length %% 8 != 0: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v3

    aput-object v3, v2, v1

    invoke-static {v0, v2}, Lcom/squareup/okhttp/internal/spdy/h;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_4
    new-instance v2, Lcom/squareup/okhttp/internal/spdy/u;

    invoke-direct {v2}, Lcom/squareup/okhttp/internal/spdy/u;-><init>()V

    move v0, v1

    :goto_1
    if-ge v0, p2, :cond_5

    iget-object v3, p0, Lcom/squareup/okhttp/internal/spdy/j;->b:Lfh;

    invoke-interface {v3}, Lfh;->i()I

    move-result v3

    iget-object v4, p0, Lcom/squareup/okhttp/internal/spdy/j;->b:Lfh;

    invoke-interface {v4}, Lfh;->i()I

    move-result v4

    const v5, 0xffffff

    and-int/2addr v3, v5

    invoke-virtual {v2, v3, v1, v4}, Lcom/squareup/okhttp/internal/spdy/u;->a(III)Lcom/squareup/okhttp/internal/spdy/u;

    add-int/lit8 v0, v0, 0x8

    goto :goto_1

    :cond_5
    invoke-interface {p1, v1, v2}, Lcom/squareup/okhttp/internal/spdy/b;->a(ZLcom/squareup/okhttp/internal/spdy/u;)V

    invoke-virtual {v2}, Lcom/squareup/okhttp/internal/spdy/u;->c()I

    move-result v0

    if-ltz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/j;->a:Lcom/squareup/okhttp/internal/spdy/f;

    invoke-virtual {v2}, Lcom/squareup/okhttp/internal/spdy/u;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/internal/spdy/f;->a(I)V

    goto :goto_0
.end method

.method private f(Lcom/squareup/okhttp/internal/spdy/b;SBI)V
    .locals 2

    if-nez p4, :cond_0

    const-string/jumbo v0, "PROTOCOL_ERROR: TYPE_PUSH_PROMISE streamId == 0"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/squareup/okhttp/internal/spdy/h;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/j;->b:Lfh;

    invoke-interface {v0}, Lfh;->i()I

    move-result v0

    const v1, 0x7fffffff

    and-int/2addr v0, v1

    add-int/lit8 v1, p2, -0x4

    int-to-short v1, v1

    invoke-direct {p0, v1, p3, p4}, Lcom/squareup/okhttp/internal/spdy/j;->a(SBI)Ljava/util/List;

    move-result-object v1

    invoke-interface {p1, p4, v0, v1}, Lcom/squareup/okhttp/internal/spdy/b;->a(IILjava/util/List;)V

    return-void
.end method

.method private g(Lcom/squareup/okhttp/internal/spdy/b;SBI)V
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/16 v2, 0x8

    if-eq p2, v2, :cond_0

    const-string/jumbo v2, "TYPE_PING length != 8: %s"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v3

    aput-object v3, v0, v1

    invoke-static {v2, v0}, Lcom/squareup/okhttp/internal/spdy/h;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_0
    if-eqz p4, :cond_1

    const-string/jumbo v0, "TYPE_PING streamId != 0"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/squareup/okhttp/internal/spdy/h;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_1
    iget-object v2, p0, Lcom/squareup/okhttp/internal/spdy/j;->b:Lfh;

    invoke-interface {v2}, Lfh;->i()I

    move-result v2

    iget-object v3, p0, Lcom/squareup/okhttp/internal/spdy/j;->b:Lfh;

    invoke-interface {v3}, Lfh;->i()I

    move-result v3

    and-int/lit8 v4, p3, 0x1

    if-eqz v4, :cond_2

    :goto_0
    invoke-interface {p1, v0, v2, v3}, Lcom/squareup/okhttp/internal/spdy/b;->a(ZII)V

    return-void

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method private h(Lcom/squareup/okhttp/internal/spdy/b;SBI)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/16 v0, 0x8

    if-ge p2, v0, :cond_0

    const-string/jumbo v0, "TYPE_GOAWAY length < 8: %s"

    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/squareup/okhttp/internal/spdy/h;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_0
    if-eqz p4, :cond_1

    const-string/jumbo v0, "TYPE_GOAWAY streamId != 0"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/squareup/okhttp/internal/spdy/h;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/j;->b:Lfh;

    invoke-interface {v0}, Lfh;->i()I

    move-result v1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/j;->b:Lfh;

    invoke-interface {v0}, Lfh;->i()I

    move-result v0

    add-int/lit8 v2, p2, -0x8

    invoke-static {v0}, Lcom/squareup/okhttp/internal/spdy/ErrorCode;->b(I)Lcom/squareup/okhttp/internal/spdy/ErrorCode;

    move-result-object v3

    if-nez v3, :cond_2

    const-string/jumbo v1, "TYPE_GOAWAY unexpected error code: %d"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v4

    invoke-static {v1, v2}, Lcom/squareup/okhttp/internal/spdy/h;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_2
    sget-object v0, Lfi;->a:Lfi;

    if-lez v2, :cond_3

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/j;->b:Lfh;

    int-to-long v4, v2

    invoke-interface {v0, v4, v5}, Lfh;->c(J)Lfi;

    move-result-object v0

    :cond_3
    invoke-interface {p1, v1, v3, v0}, Lcom/squareup/okhttp/internal/spdy/b;->a(ILcom/squareup/okhttp/internal/spdy/ErrorCode;Lfi;)V

    return-void
.end method

.method private i(Lcom/squareup/okhttp/internal/spdy/b;SBI)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x4

    if-eq p2, v0, :cond_0

    const-string/jumbo v0, "TYPE_WINDOW_UPDATE length !=4: %s"

    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/squareup/okhttp/internal/spdy/h;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/j;->b:Lfh;

    invoke-interface {v0}, Lfh;->i()I

    move-result v0

    const v1, 0x7fffffff

    and-int/2addr v0, v1

    int-to-long v0, v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    const-string/jumbo v2, "windowSizeIncrement was 0"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lcom/squareup/okhttp/internal/spdy/h;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_1
    invoke-interface {p1, p4, v0, v1}, Lcom/squareup/okhttp/internal/spdy/b;->a(IJ)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/spdy/j;->d:Z

    if-eqz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/j;->b:Lfh;

    invoke-static {}, Lcom/squareup/okhttp/internal/spdy/h;->b()Lfi;

    move-result-object v1

    invoke-virtual {v1}, Lfi;->e()I

    move-result v1

    int-to-long v1, v1

    invoke-interface {v0, v1, v2}, Lfh;->c(J)Lfi;

    move-result-object v0

    invoke-static {}, Lcom/squareup/okhttp/internal/spdy/h;->b()Lfi;

    move-result-object v1

    invoke-virtual {v1, v0}, Lfi;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "Expected a connection header but was %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v0}, Lfi;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/squareup/okhttp/internal/spdy/h;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0
.end method

.method public a(Lcom/squareup/okhttp/internal/spdy/b;)Z
    .locals 5

    :try_start_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/j;->b:Lfh;

    invoke-interface {v0}, Lfh;->i()I

    move-result v0

    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/j;->b:Lfh;

    invoke-interface {v1}, Lfh;->i()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    const/high16 v2, 0x3fff0000    # 1.9921875f

    and-int/2addr v2, v0

    shr-int/lit8 v2, v2, 0x10

    int-to-short v2, v2

    const v3, 0xff00

    and-int/2addr v3, v0

    shr-int/lit8 v3, v3, 0x8

    int-to-byte v3, v3

    and-int/lit16 v0, v0, 0xff

    int-to-byte v0, v0

    const v4, 0x7fffffff

    and-int/2addr v1, v4

    packed-switch v3, :pswitch_data_0

    :pswitch_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/j;->b:Lfh;

    int-to-long v1, v2

    invoke-interface {v0, v1, v2}, Lfh;->b(J)V

    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_1

    :pswitch_1
    invoke-direct {p0, p1, v2, v0, v1}, Lcom/squareup/okhttp/internal/spdy/j;->b(Lcom/squareup/okhttp/internal/spdy/b;SBI)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, p1, v2, v0, v1}, Lcom/squareup/okhttp/internal/spdy/j;->a(Lcom/squareup/okhttp/internal/spdy/b;SBI)V

    goto :goto_0

    :pswitch_3
    invoke-direct {p0, p1, v2, v0, v1}, Lcom/squareup/okhttp/internal/spdy/j;->c(Lcom/squareup/okhttp/internal/spdy/b;SBI)V

    goto :goto_0

    :pswitch_4
    invoke-direct {p0, p1, v2, v0, v1}, Lcom/squareup/okhttp/internal/spdy/j;->d(Lcom/squareup/okhttp/internal/spdy/b;SBI)V

    goto :goto_0

    :pswitch_5
    invoke-direct {p0, p1, v2, v0, v1}, Lcom/squareup/okhttp/internal/spdy/j;->e(Lcom/squareup/okhttp/internal/spdy/b;SBI)V

    goto :goto_0

    :pswitch_6
    invoke-direct {p0, p1, v2, v0, v1}, Lcom/squareup/okhttp/internal/spdy/j;->f(Lcom/squareup/okhttp/internal/spdy/b;SBI)V

    goto :goto_0

    :pswitch_7
    invoke-direct {p0, p1, v2, v0, v1}, Lcom/squareup/okhttp/internal/spdy/j;->g(Lcom/squareup/okhttp/internal/spdy/b;SBI)V

    goto :goto_0

    :pswitch_8
    invoke-direct {p0, p1, v2, v0, v1}, Lcom/squareup/okhttp/internal/spdy/j;->h(Lcom/squareup/okhttp/internal/spdy/b;SBI)V

    goto :goto_0

    :pswitch_9
    invoke-direct {p0, p1, v2, v0, v1}, Lcom/squareup/okhttp/internal/spdy/j;->i(Lcom/squareup/okhttp/internal/spdy/b;SBI)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_9
    .end packed-switch
.end method

.method public close()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/j;->b:Lfh;

    invoke-interface {v0}, Lfh;->close()V

    return-void
.end method
