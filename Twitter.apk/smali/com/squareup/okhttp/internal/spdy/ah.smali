.class Lcom/squareup/okhttp/internal/spdy/ah;
.super Lex;
.source "Twttr"

# interfaces
.implements Lcom/squareup/okhttp/internal/spdy/b;


# instance fields
.field final synthetic a:Lcom/squareup/okhttp/internal/spdy/y;


# direct methods
.method private constructor <init>(Lcom/squareup/okhttp/internal/spdy/y;)V
    .locals 4

    iput-object p1, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    const-string/jumbo v0, "OkHttp %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Lcom/squareup/okhttp/internal/spdy/y;->a(Lcom/squareup/okhttp/internal/spdy/y;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lex;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/okhttp/internal/spdy/y;Lcom/squareup/okhttp/internal/spdy/z;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/okhttp/internal/spdy/ah;-><init>(Lcom/squareup/okhttp/internal/spdy/y;)V

    return-void
.end method

.method private c()V
    .locals 6

    invoke-static {}, Lcom/squareup/okhttp/internal/spdy/y;->f()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Lcom/squareup/okhttp/internal/spdy/aj;

    const-string/jumbo v2, "OkHttp %s ACK Settings"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    invoke-static {v5}, Lcom/squareup/okhttp/internal/spdy/y;->a(Lcom/squareup/okhttp/internal/spdy/y;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-direct {v1, p0, v2, v3}, Lcom/squareup/okhttp/internal/spdy/aj;-><init>(Lcom/squareup/okhttp/internal/spdy/ah;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 5

    sget-object v0, Lcom/squareup/okhttp/internal/spdy/ErrorCode;->g:Lcom/squareup/okhttp/internal/spdy/ErrorCode;

    sget-object v2, Lcom/squareup/okhttp/internal/spdy/ErrorCode;->g:Lcom/squareup/okhttp/internal/spdy/ErrorCode;

    :try_start_0
    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    iget-boolean v1, v1, Lcom/squareup/okhttp/internal/spdy/y;->b:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    iget-object v1, v1, Lcom/squareup/okhttp/internal/spdy/y;->g:Lcom/squareup/okhttp/internal/spdy/a;

    invoke-interface {v1}, Lcom/squareup/okhttp/internal/spdy/a;->a()V

    :cond_0
    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    iget-object v1, v1, Lcom/squareup/okhttp/internal/spdy/y;->g:Lcom/squareup/okhttp/internal/spdy/a;

    invoke-interface {v1, p0}, Lcom/squareup/okhttp/internal/spdy/a;->a(Lcom/squareup/okhttp/internal/spdy/b;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v0, Lcom/squareup/okhttp/internal/spdy/ErrorCode;->a:Lcom/squareup/okhttp/internal/spdy/ErrorCode;

    sget-object v1, Lcom/squareup/okhttp/internal/spdy/ErrorCode;->l:Lcom/squareup/okhttp/internal/spdy/ErrorCode;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v2, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    invoke-static {v2, v0, v1}, Lcom/squareup/okhttp/internal/spdy/y;->a(Lcom/squareup/okhttp/internal/spdy/y;Lcom/squareup/okhttp/internal/spdy/ErrorCode;Lcom/squareup/okhttp/internal/spdy/ErrorCode;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    :goto_0
    return-void

    :catch_0
    move-exception v1

    :try_start_2
    sget-object v1, Lcom/squareup/okhttp/internal/spdy/ErrorCode;->b:Lcom/squareup/okhttp/internal/spdy/ErrorCode;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    sget-object v0, Lcom/squareup/okhttp/internal/spdy/ErrorCode;->b:Lcom/squareup/okhttp/internal/spdy/ErrorCode;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    iget-object v2, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    invoke-static {v2, v1, v0}, Lcom/squareup/okhttp/internal/spdy/y;->a(Lcom/squareup/okhttp/internal/spdy/y;Lcom/squareup/okhttp/internal/spdy/ErrorCode;Lcom/squareup/okhttp/internal/spdy/ErrorCode;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0

    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_1
    :try_start_5
    iget-object v3, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    invoke-static {v3, v1, v2}, Lcom/squareup/okhttp/internal/spdy/y;->a(Lcom/squareup/okhttp/internal/spdy/y;Lcom/squareup/okhttp/internal/spdy/ErrorCode;Lcom/squareup/okhttp/internal/spdy/ErrorCode;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    :goto_2
    throw v0

    :catch_2
    move-exception v1

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_1

    :catch_3
    move-exception v0

    goto :goto_0
.end method

.method public a(II)V
    .locals 0

    return-void
.end method

.method public a(IILjava/util/List;)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    invoke-static {v0, p2, p3}, Lcom/squareup/okhttp/internal/spdy/y;->a(Lcom/squareup/okhttp/internal/spdy/y;ILjava/util/List;)V

    return-void
.end method

.method public a(IJ)V
    .locals 4

    if-nez p1, :cond_1

    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    iget-wide v2, v0, Lcom/squareup/okhttp/internal/spdy/y;->d:J

    add-long/2addr v2, p2

    iput-wide v2, v0, Lcom/squareup/okhttp/internal/spdy/y;->d:J

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1

    :cond_0
    :goto_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    invoke-virtual {v0, p1}, Lcom/squareup/okhttp/internal/spdy/y;->a(I)Lcom/squareup/okhttp/internal/spdy/ak;

    move-result-object v1

    if-eqz v1, :cond_0

    monitor-enter v1

    :try_start_1
    invoke-virtual {v1, p2, p3}, Lcom/squareup/okhttp/internal/spdy/ak;->b(J)V

    monitor-exit v1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    throw v0
.end method

.method public a(ILcom/squareup/okhttp/internal/spdy/ErrorCode;)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    invoke-static {v0, p1}, Lcom/squareup/okhttp/internal/spdy/y;->a(Lcom/squareup/okhttp/internal/spdy/y;I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    invoke-static {v0, p1, p2}, Lcom/squareup/okhttp/internal/spdy/y;->a(Lcom/squareup/okhttp/internal/spdy/y;ILcom/squareup/okhttp/internal/spdy/ErrorCode;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    invoke-virtual {v0, p1}, Lcom/squareup/okhttp/internal/spdy/y;->b(I)Lcom/squareup/okhttp/internal/spdy/ak;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Lcom/squareup/okhttp/internal/spdy/ak;->c(Lcom/squareup/okhttp/internal/spdy/ErrorCode;)V

    goto :goto_0
.end method

.method public a(ILcom/squareup/okhttp/internal/spdy/ErrorCode;Lfi;)V
    .locals 4

    invoke-virtual {p3}, Lfi;->e()I

    move-result v0

    if-lez v0, :cond_0

    :cond_0
    iget-object v2, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/okhttp/internal/spdy/y;->b(Lcom/squareup/okhttp/internal/spdy/y;Z)Z

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/spdy/y;->e(Lcom/squareup/okhttp/internal/spdy/y;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-le v1, p1, :cond_1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-virtual {v1}, Lcom/squareup/okhttp/internal/spdy/ak;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/okhttp/internal/spdy/ak;

    sget-object v1, Lcom/squareup/okhttp/internal/spdy/ErrorCode;->k:Lcom/squareup/okhttp/internal/spdy/ErrorCode;

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/internal/spdy/ak;->c(Lcom/squareup/okhttp/internal/spdy/ErrorCode;)V

    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public a(ZII)V
    .locals 3

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    invoke-static {v0, p2}, Lcom/squareup/okhttp/internal/spdy/y;->c(Lcom/squareup/okhttp/internal/spdy/y;I)Lcom/squareup/okhttp/internal/spdy/r;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/spdy/r;->b()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {v0, v1, p2, p3, v2}, Lcom/squareup/okhttp/internal/spdy/y;->b(Lcom/squareup/okhttp/internal/spdy/y;ZIILcom/squareup/okhttp/internal/spdy/r;)V

    goto :goto_0
.end method

.method public a(ZILfh;I)V
    .locals 2

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    invoke-static {v0, p2}, Lcom/squareup/okhttp/internal/spdy/y;->a(Lcom/squareup/okhttp/internal/spdy/y;I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    invoke-static {v0, p2, p3, p4, p1}, Lcom/squareup/okhttp/internal/spdy/y;->a(Lcom/squareup/okhttp/internal/spdy/y;ILfh;IZ)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    invoke-virtual {v0, p2}, Lcom/squareup/okhttp/internal/spdy/y;->a(I)Lcom/squareup/okhttp/internal/spdy/ak;

    move-result-object v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    sget-object v1, Lcom/squareup/okhttp/internal/spdy/ErrorCode;->c:Lcom/squareup/okhttp/internal/spdy/ErrorCode;

    invoke-virtual {v0, p2, v1}, Lcom/squareup/okhttp/internal/spdy/y;->a(ILcom/squareup/okhttp/internal/spdy/ErrorCode;)V

    int-to-long v0, p4

    invoke-interface {p3, v0, v1}, Lfh;->b(J)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0, p3, p4}, Lcom/squareup/okhttp/internal/spdy/ak;->a(Lfh;I)V

    if-eqz p1, :cond_0

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/spdy/ak;->g()V

    goto :goto_0
.end method

.method public a(ZLcom/squareup/okhttp/internal/spdy/u;)V
    .locals 8

    const-wide/16 v3, 0x0

    const/4 v0, 0x0

    iget-object v5, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    monitor-enter v5

    :try_start_0
    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    iget-object v1, v1, Lcom/squareup/okhttp/internal/spdy/y;->f:Lcom/squareup/okhttp/internal/spdy/u;

    const/high16 v2, 0x10000

    invoke-virtual {v1, v2}, Lcom/squareup/okhttp/internal/spdy/u;->d(I)I

    move-result v1

    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    iget-object v2, v2, Lcom/squareup/okhttp/internal/spdy/y;->f:Lcom/squareup/okhttp/internal/spdy/u;

    invoke-virtual {v2}, Lcom/squareup/okhttp/internal/spdy/u;->a()V

    :cond_0
    iget-object v2, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    iget-object v2, v2, Lcom/squareup/okhttp/internal/spdy/y;->f:Lcom/squareup/okhttp/internal/spdy/u;

    invoke-virtual {v2, p2}, Lcom/squareup/okhttp/internal/spdy/u;->a(Lcom/squareup/okhttp/internal/spdy/u;)V

    iget-object v2, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    invoke-virtual {v2}, Lcom/squareup/okhttp/internal/spdy/y;->a()Lcom/squareup/okhttp/Protocol;

    move-result-object v2

    sget-object v6, Lcom/squareup/okhttp/Protocol;->a:Lcom/squareup/okhttp/Protocol;

    if-ne v2, v6, :cond_1

    invoke-direct {p0}, Lcom/squareup/okhttp/internal/spdy/ah;->c()V

    :cond_1
    iget-object v2, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    iget-object v2, v2, Lcom/squareup/okhttp/internal/spdy/y;->f:Lcom/squareup/okhttp/internal/spdy/u;

    const/high16 v6, 0x10000

    invoke-virtual {v2, v6}, Lcom/squareup/okhttp/internal/spdy/u;->d(I)I

    move-result v2

    const/4 v6, -0x1

    if-eq v2, v6, :cond_5

    if-eq v2, v1, :cond_5

    sub-int v1, v2, v1

    int-to-long v1, v1

    iget-object v6, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    invoke-static {v6}, Lcom/squareup/okhttp/internal/spdy/y;->g(Lcom/squareup/okhttp/internal/spdy/y;)Z

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    invoke-virtual {v6, v1, v2}, Lcom/squareup/okhttp/internal/spdy/y;->a(J)V

    iget-object v6, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    const/4 v7, 0x1

    invoke-static {v6, v7}, Lcom/squareup/okhttp/internal/spdy/y;->a(Lcom/squareup/okhttp/internal/spdy/y;Z)Z

    :cond_2
    iget-object v6, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    invoke-static {v6}, Lcom/squareup/okhttp/internal/spdy/y;->e(Lcom/squareup/okhttp/internal/spdy/y;)Ljava/util/Map;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Map;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_3

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/spdy/y;->e(Lcom/squareup/okhttp/internal/spdy/y;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    iget-object v6, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    invoke-static {v6}, Lcom/squareup/okhttp/internal/spdy/y;->e(Lcom/squareup/okhttp/internal/spdy/y;)Ljava/util/Map;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Map;->size()I

    move-result v6

    new-array v6, v6, [Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-interface {v0, v6}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/okhttp/internal/spdy/ak;

    :cond_3
    :goto_0
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz v0, :cond_4

    cmp-long v0, v1, v3

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/spdy/y;->e(Lcom/squareup/okhttp/internal/spdy/y;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/okhttp/internal/spdy/ak;

    monitor-enter v0

    :try_start_1
    invoke-virtual {v0, v1, v2}, Lcom/squareup/okhttp/internal/spdy/ak;->b(J)V

    monitor-exit v0

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_4
    return-void

    :cond_5
    move-wide v1, v3

    goto :goto_0
.end method

.method public a(ZZIIILjava/util/List;Lcom/squareup/okhttp/internal/spdy/HeadersMode;)V
    .locals 8

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    invoke-static {v0, p3}, Lcom/squareup/okhttp/internal/spdy/y;->a(Lcom/squareup/okhttp/internal/spdy/y;I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    invoke-static {v0, p3, p6, p2}, Lcom/squareup/okhttp/internal/spdy/y;->a(Lcom/squareup/okhttp/internal/spdy/y;ILjava/util/List;Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v7, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    monitor-enter v7

    :try_start_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/spdy/y;->b(Lcom/squareup/okhttp/internal/spdy/y;)Z

    move-result v0

    if-eqz v0, :cond_2

    monitor-exit v7

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    invoke-virtual {v0, p3}, Lcom/squareup/okhttp/internal/spdy/y;->a(I)Lcom/squareup/okhttp/internal/spdy/ak;

    move-result-object v0

    if-nez v0, :cond_6

    invoke-virtual {p7}, Lcom/squareup/okhttp/internal/spdy/HeadersMode;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    sget-object v1, Lcom/squareup/okhttp/internal/spdy/ErrorCode;->c:Lcom/squareup/okhttp/internal/spdy/ErrorCode;

    invoke-virtual {v0, p3, v1}, Lcom/squareup/okhttp/internal/spdy/y;->a(ILcom/squareup/okhttp/internal/spdy/ErrorCode;)V

    monitor-exit v7

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/spdy/y;->c(Lcom/squareup/okhttp/internal/spdy/y;)I

    move-result v0

    if-gt p3, v0, :cond_4

    monitor-exit v7

    goto :goto_0

    :cond_4
    rem-int/lit8 v0, p3, 0x2

    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    invoke-static {v1}, Lcom/squareup/okhttp/internal/spdy/y;->d(Lcom/squareup/okhttp/internal/spdy/y;)I

    move-result v1

    rem-int/lit8 v1, v1, 0x2

    if-ne v0, v1, :cond_5

    monitor-exit v7

    goto :goto_0

    :cond_5
    new-instance v0, Lcom/squareup/okhttp/internal/spdy/ak;

    iget-object v2, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    move v1, p3

    move v3, p1

    move v4, p2

    move v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/squareup/okhttp/internal/spdy/ak;-><init>(ILcom/squareup/okhttp/internal/spdy/y;ZZILjava/util/List;)V

    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    invoke-static {v1, p3}, Lcom/squareup/okhttp/internal/spdy/y;->b(Lcom/squareup/okhttp/internal/spdy/y;I)I

    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    invoke-static {v1}, Lcom/squareup/okhttp/internal/spdy/y;->e(Lcom/squareup/okhttp/internal/spdy/y;)Ljava/util/Map;

    move-result-object v1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lcom/squareup/okhttp/internal/spdy/y;->f()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    new-instance v2, Lcom/squareup/okhttp/internal/spdy/ai;

    const-string/jumbo v3, "OkHttp %s stream %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    invoke-static {v6}, Lcom/squareup/okhttp/internal/spdy/y;->a(Lcom/squareup/okhttp/internal/spdy/y;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-direct {v2, p0, v3, v4, v0}, Lcom/squareup/okhttp/internal/spdy/ai;-><init>(Lcom/squareup/okhttp/internal/spdy/ah;Ljava/lang/String;[Ljava/lang/Object;Lcom/squareup/okhttp/internal/spdy/ak;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    monitor-exit v7

    goto/16 :goto_0

    :cond_6
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p7}, Lcom/squareup/okhttp/internal/spdy/HeadersMode;->b()Z

    move-result v1

    if-eqz v1, :cond_7

    sget-object v1, Lcom/squareup/okhttp/internal/spdy/ErrorCode;->b:Lcom/squareup/okhttp/internal/spdy/ErrorCode;

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/internal/spdy/ak;->b(Lcom/squareup/okhttp/internal/spdy/ErrorCode;)V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ah;->a:Lcom/squareup/okhttp/internal/spdy/y;

    invoke-virtual {v0, p3}, Lcom/squareup/okhttp/internal/spdy/y;->b(I)Lcom/squareup/okhttp/internal/spdy/ak;

    goto/16 :goto_0

    :cond_7
    invoke-virtual {v0, p6, p7}, Lcom/squareup/okhttp/internal/spdy/ak;->a(Ljava/util/List;Lcom/squareup/okhttp/internal/spdy/HeadersMode;)V

    if-eqz p2, :cond_0

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/spdy/ak;->g()V

    goto/16 :goto_0
.end method

.method public b()V
    .locals 0

    return-void
.end method
