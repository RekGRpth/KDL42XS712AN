.class public final Lcom/squareup/okhttp/internal/spdy/y;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Closeable;


# static fields
.field static final synthetic k:Z

.field private static final l:Ljava/util/concurrent/ExecutorService;


# instance fields
.field final a:Lcom/squareup/okhttp/Protocol;

.field final b:Z

.field c:J

.field d:J

.field final e:Lcom/squareup/okhttp/internal/spdy/u;

.field final f:Lcom/squareup/okhttp/internal/spdy/u;

.field final g:Lcom/squareup/okhttp/internal/spdy/a;

.field final h:Lcom/squareup/okhttp/internal/spdy/c;

.field final i:J

.field final j:Lcom/squareup/okhttp/internal/spdy/ah;

.field private final m:Lcom/squareup/okhttp/internal/spdy/m;

.field private final n:Ljava/util/Map;

.field private final o:Ljava/lang/String;

.field private p:I

.field private q:I

.field private r:Z

.field private s:J

.field private t:Ljava/util/Map;

.field private final u:Lcom/squareup/okhttp/internal/spdy/s;

.field private v:I

.field private w:Z

.field private final x:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v7, 0x1

    const/4 v1, 0x0

    const-class v0, Lcom/squareup/okhttp/internal/spdy/y;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v7

    :goto_0
    sput-boolean v0, Lcom/squareup/okhttp/internal/spdy/y;->k:Z

    new-instance v0, Ljava/util/concurrent/ThreadPoolExecutor;

    const v2, 0x7fffffff

    const-wide/16 v3, 0x3c

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v6, Ljava/util/concurrent/SynchronousQueue;

    invoke-direct {v6}, Ljava/util/concurrent/SynchronousQueue;-><init>()V

    const-string/jumbo v8, "OkHttp SpdyConnection"

    invoke-static {v8, v7}, Lfd;->a(Ljava/lang/String;Z)Ljava/util/concurrent/ThreadFactory;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    sput-object v0, Lcom/squareup/okhttp/internal/spdy/y;->l:Ljava/util/concurrent/ExecutorService;

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private constructor <init>(Lcom/squareup/okhttp/internal/spdy/ag;)V
    .locals 6

    const/4 v2, 0x2

    const/4 v1, 0x1

    const/4 v5, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->n:Ljava/util/Map;

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/squareup/okhttp/internal/spdy/y;->s:J

    const-wide/16 v3, 0x0

    iput-wide v3, p0, Lcom/squareup/okhttp/internal/spdy/y;->c:J

    new-instance v0, Lcom/squareup/okhttp/internal/spdy/u;

    invoke-direct {v0}, Lcom/squareup/okhttp/internal/spdy/u;-><init>()V

    iput-object v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->e:Lcom/squareup/okhttp/internal/spdy/u;

    new-instance v0, Lcom/squareup/okhttp/internal/spdy/u;

    invoke-direct {v0}, Lcom/squareup/okhttp/internal/spdy/u;-><init>()V

    iput-object v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->f:Lcom/squareup/okhttp/internal/spdy/u;

    iput-boolean v5, p0, Lcom/squareup/okhttp/internal/spdy/y;->w:Z

    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->x:Ljava/util/Set;

    invoke-static {p1}, Lcom/squareup/okhttp/internal/spdy/ag;->a(Lcom/squareup/okhttp/internal/spdy/ag;)Lcom/squareup/okhttp/Protocol;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->a:Lcom/squareup/okhttp/Protocol;

    invoke-static {p1}, Lcom/squareup/okhttp/internal/spdy/ag;->b(Lcom/squareup/okhttp/internal/spdy/ag;)Lcom/squareup/okhttp/internal/spdy/s;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->u:Lcom/squareup/okhttp/internal/spdy/s;

    invoke-static {p1}, Lcom/squareup/okhttp/internal/spdy/ag;->c(Lcom/squareup/okhttp/internal/spdy/ag;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->b:Z

    invoke-static {p1}, Lcom/squareup/okhttp/internal/spdy/ag;->d(Lcom/squareup/okhttp/internal/spdy/ag;)Lcom/squareup/okhttp/internal/spdy/m;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->m:Lcom/squareup/okhttp/internal/spdy/m;

    invoke-static {p1}, Lcom/squareup/okhttp/internal/spdy/ag;->c(Lcom/squareup/okhttp/internal/spdy/ag;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iput v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->q:I

    invoke-static {p1}, Lcom/squareup/okhttp/internal/spdy/ag;->c(Lcom/squareup/okhttp/internal/spdy/ag;)Z

    move-result v0

    if-eqz v0, :cond_2

    :goto_1
    iput v1, p0, Lcom/squareup/okhttp/internal/spdy/y;->v:I

    invoke-static {p1}, Lcom/squareup/okhttp/internal/spdy/ag;->c(Lcom/squareup/okhttp/internal/spdy/ag;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->e:Lcom/squareup/okhttp/internal/spdy/u;

    const/4 v1, 0x7

    const/high16 v2, 0x1000000

    invoke-virtual {v0, v1, v5, v2}, Lcom/squareup/okhttp/internal/spdy/u;->a(III)Lcom/squareup/okhttp/internal/spdy/u;

    :cond_0
    invoke-static {p1}, Lcom/squareup/okhttp/internal/spdy/ag;->e(Lcom/squareup/okhttp/internal/spdy/ag;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->o:Ljava/lang/String;

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->a:Lcom/squareup/okhttp/Protocol;

    sget-object v1, Lcom/squareup/okhttp/Protocol;->a:Lcom/squareup/okhttp/Protocol;

    if-ne v0, v1, :cond_3

    new-instance v0, Lcom/squareup/okhttp/internal/spdy/h;

    invoke-direct {v0}, Lcom/squareup/okhttp/internal/spdy/h;-><init>()V

    :goto_2
    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/y;->f:Lcom/squareup/okhttp/internal/spdy/u;

    const/high16 v2, 0x10000

    invoke-virtual {v1, v2}, Lcom/squareup/okhttp/internal/spdy/u;->d(I)I

    move-result v1

    int-to-long v1, v1

    iput-wide v1, p0, Lcom/squareup/okhttp/internal/spdy/y;->d:J

    invoke-static {p1}, Lcom/squareup/okhttp/internal/spdy/ag;->f(Lcom/squareup/okhttp/internal/spdy/ag;)Lfh;

    move-result-object v1

    iget-boolean v2, p0, Lcom/squareup/okhttp/internal/spdy/y;->b:Z

    invoke-interface {v0, v1, v2}, Lcom/squareup/okhttp/internal/spdy/ao;->a(Lfh;Z)Lcom/squareup/okhttp/internal/spdy/a;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/okhttp/internal/spdy/y;->g:Lcom/squareup/okhttp/internal/spdy/a;

    invoke-static {p1}, Lcom/squareup/okhttp/internal/spdy/ag;->g(Lcom/squareup/okhttp/internal/spdy/ag;)Lfg;

    move-result-object v1

    iget-boolean v2, p0, Lcom/squareup/okhttp/internal/spdy/y;->b:Z

    invoke-interface {v0, v1, v2}, Lcom/squareup/okhttp/internal/spdy/ao;->a(Lfg;Z)Lcom/squareup/okhttp/internal/spdy/c;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/okhttp/internal/spdy/y;->h:Lcom/squareup/okhttp/internal/spdy/c;

    invoke-interface {v0}, Lcom/squareup/okhttp/internal/spdy/ao;->a()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->i:J

    new-instance v0, Lcom/squareup/okhttp/internal/spdy/ah;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/okhttp/internal/spdy/ah;-><init>(Lcom/squareup/okhttp/internal/spdy/y;Lcom/squareup/okhttp/internal/spdy/z;)V

    iput-object v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->j:Lcom/squareup/okhttp/internal/spdy/ah;

    new-instance v0, Ljava/lang/Thread;

    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/y;->j:Lcom/squareup/okhttp/internal/spdy/ah;

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->a:Lcom/squareup/okhttp/Protocol;

    sget-object v1, Lcom/squareup/okhttp/Protocol;->b:Lcom/squareup/okhttp/Protocol;

    if-ne v0, v1, :cond_4

    new-instance v0, Lcom/squareup/okhttp/internal/spdy/v;

    invoke-direct {v0}, Lcom/squareup/okhttp/internal/spdy/v;-><init>()V

    goto :goto_2

    :cond_4
    new-instance v0, Ljava/lang/AssertionError;

    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/y;->a:Lcom/squareup/okhttp/Protocol;

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method synthetic constructor <init>(Lcom/squareup/okhttp/internal/spdy/ag;Lcom/squareup/okhttp/internal/spdy/z;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/okhttp/internal/spdy/y;-><init>(Lcom/squareup/okhttp/internal/spdy/ag;)V

    return-void
.end method

.method private a(ILjava/util/List;ZZ)Lcom/squareup/okhttp/internal/spdy/ak;
    .locals 17

    if-nez p3, :cond_0

    const/4 v5, 0x1

    :goto_0
    if-nez p4, :cond_1

    const/4 v6, 0x1

    :goto_1
    const/4 v7, -0x1

    const/4 v14, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/squareup/okhttp/internal/spdy/y;->h:Lcom/squareup/okhttp/internal/spdy/c;

    move-object/from16 v16, v0

    monitor-enter v16

    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/squareup/okhttp/internal/spdy/y;->r:Z

    if-eqz v2, :cond_2

    new-instance v2, Ljava/io/IOException;

    const-string/jumbo v3, "shutdown"

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    :catchall_0
    move-exception v2

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v2

    :catchall_1
    move-exception v2

    monitor-exit v16
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v2

    :cond_0
    const/4 v5, 0x0

    goto :goto_0

    :cond_1
    const/4 v6, 0x0

    goto :goto_1

    :cond_2
    :try_start_3
    move-object/from16 v0, p0

    iget v3, v0, Lcom/squareup/okhttp/internal/spdy/y;->q:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/squareup/okhttp/internal/spdy/y;->q:I

    add-int/lit8 v2, v2, 0x2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/squareup/okhttp/internal/spdy/y;->q:I

    new-instance v2, Lcom/squareup/okhttp/internal/spdy/ak;

    move-object/from16 v4, p0

    move-object/from16 v8, p2

    invoke-direct/range {v2 .. v8}, Lcom/squareup/okhttp/internal/spdy/ak;-><init>(ILcom/squareup/okhttp/internal/spdy/y;ZZILjava/util/List;)V

    invoke-virtual {v2}, Lcom/squareup/okhttp/internal/spdy/ak;->a()Z

    move-result v4

    if-eqz v4, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/squareup/okhttp/internal/spdy/y;->n:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v4, v8, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/squareup/okhttp/internal/spdy/y;->a(Z)V

    :cond_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    if-nez p1, :cond_5

    :try_start_4
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/squareup/okhttp/internal/spdy/y;->h:Lcom/squareup/okhttp/internal/spdy/c;

    move v9, v5

    move v10, v6

    move v11, v3

    move/from16 v12, p1

    move v13, v7

    move-object/from16 v15, p2

    invoke-interface/range {v8 .. v15}, Lcom/squareup/okhttp/internal/spdy/c;->a(ZZIIIILjava/util/List;)V

    :goto_2
    monitor-exit v16
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-nez p3, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/squareup/okhttp/internal/spdy/y;->h:Lcom/squareup/okhttp/internal/spdy/c;

    invoke-interface {v3}, Lcom/squareup/okhttp/internal/spdy/c;->c()V

    :cond_4
    return-object v2

    :cond_5
    :try_start_5
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/squareup/okhttp/internal/spdy/y;->b:Z

    if-eqz v4, :cond_6

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "client streams shouldn\'t have associated stream IDs"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/squareup/okhttp/internal/spdy/y;->h:Lcom/squareup/okhttp/internal/spdy/c;

    move/from16 v0, p1

    move-object/from16 v1, p2

    invoke-interface {v4, v0, v3, v1}, Lcom/squareup/okhttp/internal/spdy/c;->a(IILjava/util/List;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_2
.end method

.method static synthetic a(Lcom/squareup/okhttp/internal/spdy/y;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->o:Ljava/lang/String;

    return-object v0
.end method

.method private a(ILfh;IZ)V
    .locals 9

    new-instance v5, Lfo;

    invoke-direct {v5}, Lfo;-><init>()V

    int-to-long v0, p3

    invoke-interface {p2, v0, v1}, Lfh;->a(J)V

    int-to-long v0, p3

    invoke-interface {p2, v5, v0, v1}, Lfh;->b(Lfo;J)J

    invoke-virtual {v5}, Lfo;->l()J

    move-result-wide v0

    int-to-long v2, p3

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5}, Lfo;->l()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " != "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    sget-object v8, Lcom/squareup/okhttp/internal/spdy/y;->l:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/squareup/okhttp/internal/spdy/ae;

    const-string/jumbo v2, "OkHttp %s Push Data[%s]"

    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/squareup/okhttp/internal/spdy/y;->o:Ljava/lang/String;

    aput-object v4, v3, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    move-object v1, p0

    move v4, p1

    move v6, p3

    move v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/squareup/okhttp/internal/spdy/ae;-><init>(Lcom/squareup/okhttp/internal/spdy/y;Ljava/lang/String;[Ljava/lang/Object;ILfo;IZ)V

    invoke-interface {v8, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method

.method private a(ILjava/util/List;)V
    .locals 7

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->x:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/squareup/okhttp/internal/spdy/ErrorCode;->b:Lcom/squareup/okhttp/internal/spdy/ErrorCode;

    invoke-virtual {p0, p1, v0}, Lcom/squareup/okhttp/internal/spdy/y;->a(ILcom/squareup/okhttp/internal/spdy/ErrorCode;)V

    monitor-exit p0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->x:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-object v6, Lcom/squareup/okhttp/internal/spdy/y;->l:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/squareup/okhttp/internal/spdy/ac;

    const-string/jumbo v2, "OkHttp %s Push Request[%s]"

    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/squareup/okhttp/internal/spdy/y;->o:Ljava/lang/String;

    aput-object v4, v3, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    move-object v1, p0

    move v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/okhttp/internal/spdy/ac;-><init>(Lcom/squareup/okhttp/internal/spdy/y;Ljava/lang/String;[Ljava/lang/Object;ILjava/util/List;)V

    invoke-interface {v6, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private a(ILjava/util/List;Z)V
    .locals 8

    sget-object v7, Lcom/squareup/okhttp/internal/spdy/y;->l:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/squareup/okhttp/internal/spdy/ad;

    const-string/jumbo v2, "OkHttp %s Push Headers[%s]"

    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/squareup/okhttp/internal/spdy/y;->o:Ljava/lang/String;

    aput-object v4, v3, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    move-object v1, p0

    move v4, p1

    move-object v5, p2

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/squareup/okhttp/internal/spdy/ad;-><init>(Lcom/squareup/okhttp/internal/spdy/y;Ljava/lang/String;[Ljava/lang/Object;ILjava/util/List;Z)V

    invoke-interface {v7, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method

.method private a(Lcom/squareup/okhttp/internal/spdy/ErrorCode;Lcom/squareup/okhttp/internal/spdy/ErrorCode;)V
    .locals 7

    const/4 v3, 0x0

    const/4 v2, 0x0

    sget-boolean v0, Lcom/squareup/okhttp/internal/spdy/y;->k:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/squareup/okhttp/internal/spdy/y;->a(Lcom/squareup/okhttp/internal/spdy/ErrorCode;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v2

    :goto_0
    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    iget-object v4, p0, Lcom/squareup/okhttp/internal/spdy/y;->n:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v4

    new-array v4, v4, [Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-interface {v0, v4}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/okhttp/internal/spdy/ak;

    iget-object v4, p0, Lcom/squareup/okhttp/internal/spdy/y;->n:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->clear()V

    const/4 v4, 0x0

    invoke-direct {p0, v4}, Lcom/squareup/okhttp/internal/spdy/y;->a(Z)V

    move-object v5, v0

    :goto_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->t:Ljava/util/Map;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->t:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/okhttp/internal/spdy/y;->t:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    new-array v2, v2, [Lcom/squareup/okhttp/internal/spdy/r;

    invoke-interface {v0, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/okhttp/internal/spdy/r;

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/squareup/okhttp/internal/spdy/y;->t:Ljava/util/Map;

    move-object v4, v0

    :goto_2
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v5, :cond_3

    array-length v6, v5

    move v2, v3

    move-object v0, v1

    :goto_3
    if-ge v2, v6, :cond_2

    aget-object v1, v5, v2

    :try_start_2
    invoke-virtual {v1, p2}, Lcom/squareup/okhttp/internal/spdy/ak;->a(Lcom/squareup/okhttp/internal/spdy/ErrorCode;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :cond_1
    :goto_4
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    :catch_0
    move-exception v0

    move-object v1, v0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    :catch_1
    move-exception v1

    if-eqz v0, :cond_1

    move-object v0, v1

    goto :goto_4

    :cond_2
    move-object v1, v0

    :cond_3
    if-eqz v4, :cond_4

    array-length v2, v4

    move v0, v3

    :goto_5
    if-ge v0, v2, :cond_4

    aget-object v3, v4, v0

    invoke-virtual {v3}, Lcom/squareup/okhttp/internal/spdy/r;->c()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_4
    :try_start_4
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->g:Lcom/squareup/okhttp/internal/spdy/a;

    invoke-interface {v0}, Lcom/squareup/okhttp/internal/spdy/a;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    :goto_6
    :try_start_5
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->h:Lcom/squareup/okhttp/internal/spdy/c;

    invoke-interface {v0}, Lcom/squareup/okhttp/internal/spdy/c;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    move-object v0, v1

    :cond_5
    :goto_7
    if-eqz v0, :cond_6

    throw v0

    :catch_2
    move-exception v0

    move-object v1, v0

    goto :goto_6

    :catch_3
    move-exception v0

    if-eqz v1, :cond_5

    move-object v0, v1

    goto :goto_7

    :cond_6
    return-void

    :cond_7
    move-object v4, v2

    goto :goto_2

    :cond_8
    move-object v5, v2

    goto :goto_1
.end method

.method static synthetic a(Lcom/squareup/okhttp/internal/spdy/y;ILcom/squareup/okhttp/internal/spdy/ErrorCode;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/squareup/okhttp/internal/spdy/y;->c(ILcom/squareup/okhttp/internal/spdy/ErrorCode;)V

    return-void
.end method

.method static synthetic a(Lcom/squareup/okhttp/internal/spdy/y;ILfh;IZ)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/okhttp/internal/spdy/y;->a(ILfh;IZ)V

    return-void
.end method

.method static synthetic a(Lcom/squareup/okhttp/internal/spdy/y;ILjava/util/List;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/squareup/okhttp/internal/spdy/y;->a(ILjava/util/List;)V

    return-void
.end method

.method static synthetic a(Lcom/squareup/okhttp/internal/spdy/y;ILjava/util/List;Z)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/okhttp/internal/spdy/y;->a(ILjava/util/List;Z)V

    return-void
.end method

.method static synthetic a(Lcom/squareup/okhttp/internal/spdy/y;Lcom/squareup/okhttp/internal/spdy/ErrorCode;Lcom/squareup/okhttp/internal/spdy/ErrorCode;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/squareup/okhttp/internal/spdy/y;->a(Lcom/squareup/okhttp/internal/spdy/ErrorCode;Lcom/squareup/okhttp/internal/spdy/ErrorCode;)V

    return-void
.end method

.method static synthetic a(Lcom/squareup/okhttp/internal/spdy/y;ZIILcom/squareup/okhttp/internal/spdy/r;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/okhttp/internal/spdy/y;->b(ZIILcom/squareup/okhttp/internal/spdy/r;)V

    return-void
.end method

.method private declared-synchronized a(Z)V
    .locals 2

    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    :goto_0
    iput-wide v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->s:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    const-wide v0, 0x7fffffffffffffffL

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(ZIILcom/squareup/okhttp/internal/spdy/r;)V
    .locals 9

    sget-object v8, Lcom/squareup/okhttp/internal/spdy/y;->l:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/squareup/okhttp/internal/spdy/ab;

    const-string/jumbo v2, "OkHttp %s ping %08x%08x"

    const/4 v1, 0x3

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/squareup/okhttp/internal/spdy/y;->o:Ljava/lang/String;

    aput-object v4, v3, v1

    const/4 v1, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    const/4 v1, 0x2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    move-object v1, p0

    move v4, p1

    move v5, p2

    move v6, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/squareup/okhttp/internal/spdy/ab;-><init>(Lcom/squareup/okhttp/internal/spdy/y;Ljava/lang/String;[Ljava/lang/Object;ZIILcom/squareup/okhttp/internal/spdy/r;)V

    invoke-interface {v8, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method

.method static synthetic a(Lcom/squareup/okhttp/internal/spdy/y;I)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/squareup/okhttp/internal/spdy/y;->d(I)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/squareup/okhttp/internal/spdy/y;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/squareup/okhttp/internal/spdy/y;->w:Z

    return p1
.end method

.method static synthetic b(Lcom/squareup/okhttp/internal/spdy/y;I)I
    .locals 0

    iput p1, p0, Lcom/squareup/okhttp/internal/spdy/y;->p:I

    return p1
.end method

.method static synthetic b(Lcom/squareup/okhttp/internal/spdy/y;ZIILcom/squareup/okhttp/internal/spdy/r;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/squareup/okhttp/internal/spdy/y;->a(ZIILcom/squareup/okhttp/internal/spdy/r;)V

    return-void
.end method

.method private b(ZIILcom/squareup/okhttp/internal/spdy/r;)V
    .locals 2

    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/y;->h:Lcom/squareup/okhttp/internal/spdy/c;

    monitor-enter v1

    if-eqz p4, :cond_0

    :try_start_0
    invoke-virtual {p4}, Lcom/squareup/okhttp/internal/spdy/r;->a()V

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->h:Lcom/squareup/okhttp/internal/spdy/c;

    invoke-interface {v0, p1, p2, p3}, Lcom/squareup/okhttp/internal/spdy/c;->a(ZII)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic b(Lcom/squareup/okhttp/internal/spdy/y;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->r:Z

    return v0
.end method

.method static synthetic b(Lcom/squareup/okhttp/internal/spdy/y;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/squareup/okhttp/internal/spdy/y;->r:Z

    return p1
.end method

.method static synthetic c(Lcom/squareup/okhttp/internal/spdy/y;)I
    .locals 1

    iget v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->p:I

    return v0
.end method

.method private declared-synchronized c(I)Lcom/squareup/okhttp/internal/spdy/r;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->t:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->t:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/okhttp/internal/spdy/r;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic c(Lcom/squareup/okhttp/internal/spdy/y;I)Lcom/squareup/okhttp/internal/spdy/r;
    .locals 1

    invoke-direct {p0, p1}, Lcom/squareup/okhttp/internal/spdy/y;->c(I)Lcom/squareup/okhttp/internal/spdy/r;

    move-result-object v0

    return-object v0
.end method

.method private c(ILcom/squareup/okhttp/internal/spdy/ErrorCode;)V
    .locals 7

    sget-object v6, Lcom/squareup/okhttp/internal/spdy/y;->l:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/squareup/okhttp/internal/spdy/af;

    const-string/jumbo v2, "OkHttp %s Push Reset[%s]"

    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/squareup/okhttp/internal/spdy/y;->o:Ljava/lang/String;

    aput-object v4, v3, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    move-object v1, p0

    move v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/okhttp/internal/spdy/af;-><init>(Lcom/squareup/okhttp/internal/spdy/y;Ljava/lang/String;[Ljava/lang/Object;ILcom/squareup/okhttp/internal/spdy/ErrorCode;)V

    invoke-interface {v6, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method

.method static synthetic d(Lcom/squareup/okhttp/internal/spdy/y;)I
    .locals 1

    iget v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->q:I

    return v0
.end method

.method private d(I)Z
    .locals 2

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->a:Lcom/squareup/okhttp/Protocol;

    sget-object v1, Lcom/squareup/okhttp/Protocol;->a:Lcom/squareup/okhttp/Protocol;

    if-ne v0, v1, :cond_0

    if-eqz p1, :cond_0

    and-int/lit8 v0, p1, 0x1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic e(Lcom/squareup/okhttp/internal/spdy/y;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->n:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic f(Lcom/squareup/okhttp/internal/spdy/y;)Lcom/squareup/okhttp/internal/spdy/m;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->m:Lcom/squareup/okhttp/internal/spdy/m;

    return-object v0
.end method

.method static synthetic f()Ljava/util/concurrent/ExecutorService;
    .locals 1

    sget-object v0, Lcom/squareup/okhttp/internal/spdy/y;->l:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method static synthetic g(Lcom/squareup/okhttp/internal/spdy/y;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->w:Z

    return v0
.end method

.method static synthetic h(Lcom/squareup/okhttp/internal/spdy/y;)Lcom/squareup/okhttp/internal/spdy/s;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->u:Lcom/squareup/okhttp/internal/spdy/s;

    return-object v0
.end method

.method static synthetic i(Lcom/squareup/okhttp/internal/spdy/y;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->x:Ljava/util/Set;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/squareup/okhttp/Protocol;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->a:Lcom/squareup/okhttp/Protocol;

    return-object v0
.end method

.method declared-synchronized a(I)Lcom/squareup/okhttp/internal/spdy/ak;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->n:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/okhttp/internal/spdy/ak;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Ljava/util/List;ZZ)Lcom/squareup/okhttp/internal/spdy/ak;
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/squareup/okhttp/internal/spdy/y;->a(ILjava/util/List;ZZ)Lcom/squareup/okhttp/internal/spdy/ak;

    move-result-object v0

    return-object v0
.end method

.method a(IJ)V
    .locals 8

    sget-object v7, Lcom/squareup/okhttp/internal/spdy/y;->l:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/squareup/okhttp/internal/spdy/aa;

    const-string/jumbo v2, "OkHttp Window Update %s stream %d"

    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/squareup/okhttp/internal/spdy/y;->o:Ljava/lang/String;

    aput-object v4, v3, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    move-object v1, p0

    move v4, p1

    move-wide v5, p2

    invoke-direct/range {v0 .. v6}, Lcom/squareup/okhttp/internal/spdy/aa;-><init>(Lcom/squareup/okhttp/internal/spdy/y;Ljava/lang/String;[Ljava/lang/Object;IJ)V

    invoke-interface {v7, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method

.method a(ILcom/squareup/okhttp/internal/spdy/ErrorCode;)V
    .locals 7

    sget-object v6, Lcom/squareup/okhttp/internal/spdy/y;->l:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/squareup/okhttp/internal/spdy/z;

    const-string/jumbo v2, "OkHttp %s stream %d"

    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/squareup/okhttp/internal/spdy/y;->o:Ljava/lang/String;

    aput-object v4, v3, v1

    const/4 v1, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    move-object v1, p0

    move v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/squareup/okhttp/internal/spdy/z;-><init>(Lcom/squareup/okhttp/internal/spdy/y;Ljava/lang/String;[Ljava/lang/Object;ILcom/squareup/okhttp/internal/spdy/ErrorCode;)V

    invoke-interface {v6, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    return-void
.end method

.method public a(IZLfo;J)V
    .locals 9

    const/4 v1, 0x0

    const-wide/16 v7, 0x0

    cmp-long v0, p4, v7

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->h:Lcom/squareup/okhttp/internal/spdy/c;

    invoke-interface {v0, p2, p1, p3, v1}, Lcom/squareup/okhttp/internal/spdy/c;->a(ZILfo;I)V

    :cond_0
    return-void

    :cond_1
    :try_start_0
    iget-wide v2, p0, Lcom/squareup/okhttp/internal/spdy/y;->d:J

    invoke-static {p4, p5, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    iget-wide v4, p0, Lcom/squareup/okhttp/internal/spdy/y;->i:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-int v2, v2

    iget-wide v3, p0, Lcom/squareup/okhttp/internal/spdy/y;->d:J

    int-to-long v5, v2

    sub-long/2addr v3, v5

    iput-wide v3, p0, Lcom/squareup/okhttp/internal/spdy/y;->d:J

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    int-to-long v3, v2

    sub-long/2addr p4, v3

    iget-object v3, p0, Lcom/squareup/okhttp/internal/spdy/y;->h:Lcom/squareup/okhttp/internal/spdy/c;

    if-eqz p2, :cond_3

    cmp-long v0, p4, v7

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v3, v0, p1, p3, v2}, Lcom/squareup/okhttp/internal/spdy/c;->a(ZILfo;I)V

    :cond_2
    cmp-long v0, p4, v7

    if-lez v0, :cond_0

    monitor-enter p0

    :goto_1
    :try_start_1
    iget-wide v2, p0, Lcom/squareup/okhttp/internal/spdy/y;->d:J

    cmp-long v0, v2, v7

    if-gtz v0, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_2
    new-instance v0, Ljava/io/InterruptedIOException;

    invoke-direct {v0}, Ljava/io/InterruptedIOException;-><init>()V

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method a(J)V
    .locals 2

    iget-wide v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->d:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->d:J

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    :cond_0
    return-void
.end method

.method public a(Lcom/squareup/okhttp/internal/spdy/ErrorCode;)V
    .locals 4

    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/y;->h:Lcom/squareup/okhttp/internal/spdy/c;

    monitor-enter v1

    :try_start_0
    monitor-enter p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->r:Z

    if-eqz v0, :cond_0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    :try_start_3
    iput-boolean v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->r:Z

    iget v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->p:I

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    iget-object v2, p0, Lcom/squareup/okhttp/internal/spdy/y;->h:Lcom/squareup/okhttp/internal/spdy/c;

    sget-object v3, Lfd;->a:[B

    invoke-interface {v2, v0, p1, v3}, Lcom/squareup/okhttp/internal/spdy/c;->a(ILcom/squareup/okhttp/internal/spdy/ErrorCode;[B)V

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_5
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method

.method declared-synchronized b(I)Lcom/squareup/okhttp/internal/spdy/ak;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->n:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/okhttp/internal/spdy/ak;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/y;->n:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/squareup/okhttp/internal/spdy/y;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method b(ILcom/squareup/okhttp/internal/spdy/ErrorCode;)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->h:Lcom/squareup/okhttp/internal/spdy/c;

    invoke-interface {v0, p1, p2}, Lcom/squareup/okhttp/internal/spdy/c;->a(ILcom/squareup/okhttp/internal/spdy/ErrorCode;)V

    return-void
.end method

.method public declared-synchronized b()Z
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->s:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide v2, 0x7fffffffffffffffL

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c()J
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->s:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public close()V
    .locals 2

    sget-object v0, Lcom/squareup/okhttp/internal/spdy/ErrorCode;->a:Lcom/squareup/okhttp/internal/spdy/ErrorCode;

    sget-object v1, Lcom/squareup/okhttp/internal/spdy/ErrorCode;->l:Lcom/squareup/okhttp/internal/spdy/ErrorCode;

    invoke-direct {p0, v0, v1}, Lcom/squareup/okhttp/internal/spdy/y;->a(Lcom/squareup/okhttp/internal/spdy/ErrorCode;Lcom/squareup/okhttp/internal/spdy/ErrorCode;)V

    return-void
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->h:Lcom/squareup/okhttp/internal/spdy/c;

    invoke-interface {v0}, Lcom/squareup/okhttp/internal/spdy/c;->c()V

    return-void
.end method

.method public e()V
    .locals 2

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->h:Lcom/squareup/okhttp/internal/spdy/c;

    invoke-interface {v0}, Lcom/squareup/okhttp/internal/spdy/c;->a()V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/y;->h:Lcom/squareup/okhttp/internal/spdy/c;

    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/y;->e:Lcom/squareup/okhttp/internal/spdy/u;

    invoke-interface {v0, v1}, Lcom/squareup/okhttp/internal/spdy/c;->a(Lcom/squareup/okhttp/internal/spdy/u;)V

    return-void
.end method
