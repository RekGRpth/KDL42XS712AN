.class final enum Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;
.super Ljava/lang/Enum;
.source "Twttr"


# static fields
.field public static final enum a:Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;

.field public static final enum b:Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;

.field private static final synthetic c:[Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;


# instance fields
.field private final codes:[I

.field private final lengths:[B

.field private final root:Lcom/squareup/okhttp/internal/spdy/l;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;

    const-string/jumbo v1, "REQUEST"

    invoke-static {}, Lcom/squareup/okhttp/internal/spdy/Huffman;->a()[I

    move-result-object v2

    invoke-static {}, Lcom/squareup/okhttp/internal/spdy/Huffman;->b()[B

    move-result-object v3

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;-><init>(Ljava/lang/String;I[I[B)V

    sput-object v0, Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;->a:Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;

    new-instance v0, Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;

    const-string/jumbo v1, "RESPONSE"

    invoke-static {}, Lcom/squareup/okhttp/internal/spdy/Huffman;->c()[I

    move-result-object v2

    invoke-static {}, Lcom/squareup/okhttp/internal/spdy/Huffman;->d()[B

    move-result-object v3

    invoke-direct {v0, v1, v5, v2, v3}, Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;-><init>(Ljava/lang/String;I[I[B)V

    sput-object v0, Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;->b:Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;

    sget-object v1, Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;->a:Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;

    aput-object v1, v0, v4

    sget-object v1, Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;->b:Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;

    aput-object v1, v0, v5

    sput-object v0, Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;->c:[Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I[I[B)V
    .locals 1

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lcom/squareup/okhttp/internal/spdy/l;

    invoke-direct {v0}, Lcom/squareup/okhttp/internal/spdy/l;-><init>()V

    iput-object v0, p0, Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;->root:Lcom/squareup/okhttp/internal/spdy/l;

    invoke-direct {p0, p3, p4}, Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;->a([I[B)V

    iput-object p3, p0, Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;->codes:[I

    iput-object p4, p0, Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;->lengths:[B

    return-void
.end method

.method private a(IIB)V
    .locals 6

    new-instance v3, Lcom/squareup/okhttp/internal/spdy/l;

    invoke-direct {v3, p1, p3}, Lcom/squareup/okhttp/internal/spdy/l;-><init>(II)V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;->root:Lcom/squareup/okhttp/internal/spdy/l;

    move-object v2, v0

    :goto_0
    const/16 v0, 0x8

    if-le p3, v0, :cond_2

    add-int/lit8 v0, p3, -0x8

    int-to-byte p3, v0

    ushr-int v0, p2, p3

    and-int/lit16 v0, v0, 0xff

    invoke-static {v2}, Lcom/squareup/okhttp/internal/spdy/l;->a(Lcom/squareup/okhttp/internal/spdy/l;)[Lcom/squareup/okhttp/internal/spdy/l;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "invalid dictionary: prefix not unique"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {v2}, Lcom/squareup/okhttp/internal/spdy/l;->a(Lcom/squareup/okhttp/internal/spdy/l;)[Lcom/squareup/okhttp/internal/spdy/l;

    move-result-object v1

    aget-object v1, v1, v0

    if-nez v1, :cond_1

    invoke-static {v2}, Lcom/squareup/okhttp/internal/spdy/l;->a(Lcom/squareup/okhttp/internal/spdy/l;)[Lcom/squareup/okhttp/internal/spdy/l;

    move-result-object v1

    new-instance v4, Lcom/squareup/okhttp/internal/spdy/l;

    invoke-direct {v4}, Lcom/squareup/okhttp/internal/spdy/l;-><init>()V

    aput-object v4, v1, v0

    :cond_1
    invoke-static {v2}, Lcom/squareup/okhttp/internal/spdy/l;->a(Lcom/squareup/okhttp/internal/spdy/l;)[Lcom/squareup/okhttp/internal/spdy/l;

    move-result-object v1

    aget-object v0, v1, v0

    move-object v2, v0

    goto :goto_0

    :cond_2
    rsub-int/lit8 v0, p3, 0x8

    shl-int v1, p2, v0

    and-int/lit16 v1, v1, 0xff

    const/4 v4, 0x1

    shl-int/2addr v4, v0

    move v0, v1

    :goto_1
    add-int v5, v1, v4

    if-ge v0, v5, :cond_3

    invoke-static {v2}, Lcom/squareup/okhttp/internal/spdy/l;->a(Lcom/squareup/okhttp/internal/spdy/l;)[Lcom/squareup/okhttp/internal/spdy/l;

    move-result-object v5

    aput-object v3, v5, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    return-void
.end method

.method private a([I[B)V
    .locals 3

    const/4 v0, 0x0

    :goto_0
    array-length v1, p2

    if-ge v0, v1, :cond_0

    aget v1, p1, v0

    aget-byte v2, p2, v0

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;->a(IIB)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;
    .locals 1

    const-class v0, Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;

    return-object v0
.end method

.method public static values()[Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;
    .locals 1

    sget-object v0, Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;->c:[Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;

    invoke-virtual {v0}, [Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;

    return-object v0
.end method


# virtual methods
.method a(Lfi;)Lfi;
    .locals 1

    invoke-virtual {p1}, Lfi;->f()[B

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;->a([B)[B

    move-result-object v0

    invoke-static {v0}, Lfi;->a([B)Lfi;

    move-result-object v0

    return-object v0
.end method

.method a([B)[B
    .locals 6

    const/4 v0, 0x0

    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;->root:Lcom/squareup/okhttp/internal/spdy/l;

    move v2, v0

    move-object v3, v1

    move v1, v0

    :goto_0
    array-length v5, p1

    if-ge v0, v5, :cond_3

    aget-byte v5, p1, v0

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v2, v5

    add-int/lit8 v1, v1, 0x8

    :goto_1
    const/16 v5, 0x8

    if-lt v1, v5, :cond_1

    add-int/lit8 v5, v1, -0x8

    ushr-int v5, v2, v5

    and-int/lit16 v5, v5, 0xff

    invoke-static {v3}, Lcom/squareup/okhttp/internal/spdy/l;->a(Lcom/squareup/okhttp/internal/spdy/l;)[Lcom/squareup/okhttp/internal/spdy/l;

    move-result-object v3

    aget-object v3, v3, v5

    invoke-static {v3}, Lcom/squareup/okhttp/internal/spdy/l;->a(Lcom/squareup/okhttp/internal/spdy/l;)[Lcom/squareup/okhttp/internal/spdy/l;

    move-result-object v5

    if-nez v5, :cond_0

    invoke-static {v3}, Lcom/squareup/okhttp/internal/spdy/l;->b(Lcom/squareup/okhttp/internal/spdy/l;)I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V

    invoke-static {v3}, Lcom/squareup/okhttp/internal/spdy/l;->c(Lcom/squareup/okhttp/internal/spdy/l;)I

    move-result v3

    sub-int/2addr v1, v3

    iget-object v3, p0, Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;->root:Lcom/squareup/okhttp/internal/spdy/l;

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, -0x8

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    invoke-static {v0}, Lcom/squareup/okhttp/internal/spdy/l;->b(Lcom/squareup/okhttp/internal/spdy/l;)I

    move-result v3

    invoke-virtual {v4, v3}, Ljava/io/ByteArrayOutputStream;->write(I)V

    invoke-static {v0}, Lcom/squareup/okhttp/internal/spdy/l;->c(Lcom/squareup/okhttp/internal/spdy/l;)I

    move-result v0

    sub-int/2addr v1, v0

    iget-object v3, p0, Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;->root:Lcom/squareup/okhttp/internal/spdy/l;

    :cond_3
    if-lez v1, :cond_4

    rsub-int/lit8 v0, v1, 0x8

    shl-int v0, v2, v0

    and-int/lit16 v0, v0, 0xff

    invoke-static {v3}, Lcom/squareup/okhttp/internal/spdy/l;->a(Lcom/squareup/okhttp/internal/spdy/l;)[Lcom/squareup/okhttp/internal/spdy/l;

    move-result-object v3

    aget-object v0, v3, v0

    invoke-static {v0}, Lcom/squareup/okhttp/internal/spdy/l;->a(Lcom/squareup/okhttp/internal/spdy/l;)[Lcom/squareup/okhttp/internal/spdy/l;

    move-result-object v3

    if-nez v3, :cond_4

    invoke-static {v0}, Lcom/squareup/okhttp/internal/spdy/l;->c(Lcom/squareup/okhttp/internal/spdy/l;)I

    move-result v3

    if-le v3, v1, :cond_2

    :cond_4
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method
