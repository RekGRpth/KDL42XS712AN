.class Lcom/squareup/okhttp/internal/spdy/q;
.super Ljava/util/zip/Inflater;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/squareup/okhttp/internal/spdy/o;


# direct methods
.method constructor <init>(Lcom/squareup/okhttp/internal/spdy/o;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/okhttp/internal/spdy/q;->a:Lcom/squareup/okhttp/internal/spdy/o;

    invoke-direct {p0}, Ljava/util/zip/Inflater;-><init>()V

    return-void
.end method


# virtual methods
.method public inflate([BII)I
    .locals 2

    invoke-super {p0, p1, p2, p3}, Ljava/util/zip/Inflater;->inflate([BII)I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/spdy/q;->needsDictionary()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Lcom/squareup/okhttp/internal/spdy/v;->a:[B

    invoke-virtual {p0, v0}, Lcom/squareup/okhttp/internal/spdy/q;->setDictionary([B)V

    invoke-super {p0, p1, p2, p3}, Ljava/util/zip/Inflater;->inflate([BII)I

    move-result v0

    :cond_0
    return v0
.end method
