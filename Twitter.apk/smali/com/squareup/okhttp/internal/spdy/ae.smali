.class Lcom/squareup/okhttp/internal/spdy/ae;
.super Lex;
.source "Twttr"


# instance fields
.field final synthetic a:I

.field final synthetic b:Lfo;

.field final synthetic c:I

.field final synthetic d:Z

.field final synthetic e:Lcom/squareup/okhttp/internal/spdy/y;


# direct methods
.method varargs constructor <init>(Lcom/squareup/okhttp/internal/spdy/y;Ljava/lang/String;[Ljava/lang/Object;ILfo;IZ)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/okhttp/internal/spdy/ae;->e:Lcom/squareup/okhttp/internal/spdy/y;

    iput p4, p0, Lcom/squareup/okhttp/internal/spdy/ae;->a:I

    iput-object p5, p0, Lcom/squareup/okhttp/internal/spdy/ae;->b:Lfo;

    iput p6, p0, Lcom/squareup/okhttp/internal/spdy/ae;->c:I

    iput-boolean p7, p0, Lcom/squareup/okhttp/internal/spdy/ae;->d:Z

    invoke-direct {p0, p2, p3}, Lex;-><init>(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 5

    :try_start_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ae;->e:Lcom/squareup/okhttp/internal/spdy/y;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/spdy/y;->h(Lcom/squareup/okhttp/internal/spdy/y;)Lcom/squareup/okhttp/internal/spdy/s;

    move-result-object v0

    iget v1, p0, Lcom/squareup/okhttp/internal/spdy/ae;->a:I

    iget-object v2, p0, Lcom/squareup/okhttp/internal/spdy/ae;->b:Lfo;

    iget v3, p0, Lcom/squareup/okhttp/internal/spdy/ae;->c:I

    iget-boolean v4, p0, Lcom/squareup/okhttp/internal/spdy/ae;->d:Z

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/squareup/okhttp/internal/spdy/s;->a(ILfh;IZ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/ae;->e:Lcom/squareup/okhttp/internal/spdy/y;

    iget-object v1, v1, Lcom/squareup/okhttp/internal/spdy/y;->h:Lcom/squareup/okhttp/internal/spdy/c;

    iget v2, p0, Lcom/squareup/okhttp/internal/spdy/ae;->a:I

    sget-object v3, Lcom/squareup/okhttp/internal/spdy/ErrorCode;->l:Lcom/squareup/okhttp/internal/spdy/ErrorCode;

    invoke-interface {v1, v2, v3}, Lcom/squareup/okhttp/internal/spdy/c;->a(ILcom/squareup/okhttp/internal/spdy/ErrorCode;)V

    :cond_0
    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/spdy/ae;->d:Z

    if-eqz v0, :cond_2

    :cond_1
    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/ae;->e:Lcom/squareup/okhttp/internal/spdy/y;

    monitor-enter v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ae;->e:Lcom/squareup/okhttp/internal/spdy/y;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/spdy/y;->i(Lcom/squareup/okhttp/internal/spdy/y;)Ljava/util/Set;

    move-result-object v0

    iget v2, p0, Lcom/squareup/okhttp/internal/spdy/ae;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    monitor-exit v1

    :cond_2
    :goto_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method
