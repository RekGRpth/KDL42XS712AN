.class public final Lcom/squareup/okhttp/internal/spdy/ak;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field static final synthetic d:Z


# instance fields
.field a:J

.field b:J

.field final c:Lcom/squareup/okhttp/internal/spdy/am;

.field private final e:I

.field private final f:Lcom/squareup/okhttp/internal/spdy/y;

.field private final g:I

.field private h:J

.field private final i:Ljava/util/List;

.field private j:Ljava/util/List;

.field private final k:Lcom/squareup/okhttp/internal/spdy/an;

.field private l:Lcom/squareup/okhttp/internal/spdy/ErrorCode;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/squareup/okhttp/internal/spdy/ak;->d:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(ILcom/squareup/okhttp/internal/spdy/y;ZZILjava/util/List;)V
    .locals 4

    const-wide/16 v0, 0x0

    const/4 v3, 0x0

    const/high16 v2, 0x10000

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide v0, p0, Lcom/squareup/okhttp/internal/spdy/ak;->a:J

    iput-wide v0, p0, Lcom/squareup/okhttp/internal/spdy/ak;->h:J

    iput-object v3, p0, Lcom/squareup/okhttp/internal/spdy/ak;->l:Lcom/squareup/okhttp/internal/spdy/ErrorCode;

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "connection == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p6, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "requestHeaders == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iput p1, p0, Lcom/squareup/okhttp/internal/spdy/ak;->e:I

    iput-object p2, p0, Lcom/squareup/okhttp/internal/spdy/ak;->f:Lcom/squareup/okhttp/internal/spdy/y;

    iget-object v0, p2, Lcom/squareup/okhttp/internal/spdy/y;->f:Lcom/squareup/okhttp/internal/spdy/u;

    invoke-virtual {v0, v2}, Lcom/squareup/okhttp/internal/spdy/u;->d(I)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/squareup/okhttp/internal/spdy/ak;->b:J

    new-instance v0, Lcom/squareup/okhttp/internal/spdy/an;

    iget-object v1, p2, Lcom/squareup/okhttp/internal/spdy/y;->e:Lcom/squareup/okhttp/internal/spdy/u;

    invoke-virtual {v1, v2}, Lcom/squareup/okhttp/internal/spdy/u;->d(I)I

    move-result v1

    int-to-long v1, v1

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/squareup/okhttp/internal/spdy/an;-><init>(Lcom/squareup/okhttp/internal/spdy/ak;JLcom/squareup/okhttp/internal/spdy/al;)V

    iput-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ak;->k:Lcom/squareup/okhttp/internal/spdy/an;

    new-instance v0, Lcom/squareup/okhttp/internal/spdy/am;

    invoke-direct {v0, p0}, Lcom/squareup/okhttp/internal/spdy/am;-><init>(Lcom/squareup/okhttp/internal/spdy/ak;)V

    iput-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ak;->c:Lcom/squareup/okhttp/internal/spdy/am;

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ak;->k:Lcom/squareup/okhttp/internal/spdy/an;

    invoke-static {v0, p4}, Lcom/squareup/okhttp/internal/spdy/an;->a(Lcom/squareup/okhttp/internal/spdy/an;Z)Z

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ak;->c:Lcom/squareup/okhttp/internal/spdy/am;

    invoke-static {v0, p3}, Lcom/squareup/okhttp/internal/spdy/am;->a(Lcom/squareup/okhttp/internal/spdy/am;Z)Z

    iput p5, p0, Lcom/squareup/okhttp/internal/spdy/ak;->g:I

    iput-object p6, p0, Lcom/squareup/okhttp/internal/spdy/ak;->i:Ljava/util/List;

    return-void
.end method

.method static synthetic a(Lcom/squareup/okhttp/internal/spdy/ak;)Lcom/squareup/okhttp/internal/spdy/y;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ak;->f:Lcom/squareup/okhttp/internal/spdy/y;

    return-object v0
.end method

.method static synthetic b(Lcom/squareup/okhttp/internal/spdy/ak;)I
    .locals 1

    iget v0, p0, Lcom/squareup/okhttp/internal/spdy/ak;->e:I

    return v0
.end method

.method static synthetic c(Lcom/squareup/okhttp/internal/spdy/ak;)J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/okhttp/internal/spdy/ak;->h:J

    return-wide v0
.end method

.method static synthetic d(Lcom/squareup/okhttp/internal/spdy/ak;)Lcom/squareup/okhttp/internal/spdy/ErrorCode;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ak;->l:Lcom/squareup/okhttp/internal/spdy/ErrorCode;

    return-object v0
.end method

.method private d(Lcom/squareup/okhttp/internal/spdy/ErrorCode;)Z
    .locals 2

    const/4 v0, 0x0

    sget-boolean v1, Lcom/squareup/okhttp/internal/spdy/ak;->d:Z

    if-nez v1, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/ak;->l:Lcom/squareup/okhttp/internal/spdy/ErrorCode;

    if-eqz v1, :cond_1

    monitor-exit p0

    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/ak;->k:Lcom/squareup/okhttp/internal/spdy/an;

    invoke-static {v1}, Lcom/squareup/okhttp/internal/spdy/an;->a(Lcom/squareup/okhttp/internal/spdy/an;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/ak;->c:Lcom/squareup/okhttp/internal/spdy/am;

    invoke-static {v1}, Lcom/squareup/okhttp/internal/spdy/am;->a(Lcom/squareup/okhttp/internal/spdy/am;)Z

    move-result v1

    if-eqz v1, :cond_2

    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    iput-object p1, p0, Lcom/squareup/okhttp/internal/spdy/ak;->l:Lcom/squareup/okhttp/internal/spdy/ErrorCode;

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ak;->f:Lcom/squareup/okhttp/internal/spdy/y;

    iget v1, p0, Lcom/squareup/okhttp/internal/spdy/ak;->e:I

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/internal/spdy/y;->b(I)Lcom/squareup/okhttp/internal/spdy/ak;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic e(Lcom/squareup/okhttp/internal/spdy/ak;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/okhttp/internal/spdy/ak;->h()V

    return-void
.end method

.method static synthetic f(Lcom/squareup/okhttp/internal/spdy/ak;)V
    .locals 0

    invoke-direct {p0}, Lcom/squareup/okhttp/internal/spdy/ak;->i()V

    return-void
.end method

.method private h()V
    .locals 2

    sget-boolean v0, Lcom/squareup/okhttp/internal/spdy/ak;->d:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ak;->k:Lcom/squareup/okhttp/internal/spdy/an;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/spdy/an;->a(Lcom/squareup/okhttp/internal/spdy/an;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ak;->k:Lcom/squareup/okhttp/internal/spdy/an;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/spdy/an;->b(Lcom/squareup/okhttp/internal/spdy/an;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ak;->c:Lcom/squareup/okhttp/internal/spdy/am;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/spdy/am;->a(Lcom/squareup/okhttp/internal/spdy/am;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ak;->c:Lcom/squareup/okhttp/internal/spdy/am;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/spdy/am;->b(Lcom/squareup/okhttp/internal/spdy/am;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/spdy/ak;->a()Z

    move-result v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/squareup/okhttp/internal/spdy/ErrorCode;->l:Lcom/squareup/okhttp/internal/spdy/ErrorCode;

    invoke-virtual {p0, v0}, Lcom/squareup/okhttp/internal/spdy/ak;->a(Lcom/squareup/okhttp/internal/spdy/ErrorCode;)V

    :cond_2
    :goto_1
    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_4
    if-nez v1, :cond_2

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ak;->f:Lcom/squareup/okhttp/internal/spdy/y;

    iget v1, p0, Lcom/squareup/okhttp/internal/spdy/ak;->e:I

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/internal/spdy/y;->b(I)Lcom/squareup/okhttp/internal/spdy/ak;

    goto :goto_1
.end method

.method private i()V
    .locals 3

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ak;->c:Lcom/squareup/okhttp/internal/spdy/am;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/spdy/am;->b(Lcom/squareup/okhttp/internal/spdy/am;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "stream closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ak;->c:Lcom/squareup/okhttp/internal/spdy/am;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/spdy/am;->a(Lcom/squareup/okhttp/internal/spdy/am;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "stream finished"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ak;->l:Lcom/squareup/okhttp/internal/spdy/ErrorCode;

    if-eqz v0, :cond_2

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "stream was reset: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/okhttp/internal/spdy/ak;->l:Lcom/squareup/okhttp/internal/spdy/ErrorCode;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-void
.end method


# virtual methods
.method public a(J)V
    .locals 0

    iput-wide p1, p0, Lcom/squareup/okhttp/internal/spdy/ak;->h:J

    return-void
.end method

.method public a(Lcom/squareup/okhttp/internal/spdy/ErrorCode;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/squareup/okhttp/internal/spdy/ak;->d(Lcom/squareup/okhttp/internal/spdy/ErrorCode;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ak;->f:Lcom/squareup/okhttp/internal/spdy/y;

    iget v1, p0, Lcom/squareup/okhttp/internal/spdy/ak;->e:I

    invoke-virtual {v0, v1, p1}, Lcom/squareup/okhttp/internal/spdy/y;->b(ILcom/squareup/okhttp/internal/spdy/ErrorCode;)V

    goto :goto_0
.end method

.method a(Lfh;I)V
    .locals 3

    sget-boolean v0, Lcom/squareup/okhttp/internal/spdy/ak;->d:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ak;->k:Lcom/squareup/okhttp/internal/spdy/an;

    int-to-long v1, p2

    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/okhttp/internal/spdy/an;->a(Lfh;J)V

    return-void
.end method

.method a(Ljava/util/List;Lcom/squareup/okhttp/internal/spdy/HeadersMode;)V
    .locals 4

    sget-boolean v0, Lcom/squareup/okhttp/internal/spdy/ak;->d:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    const/4 v1, 0x0

    const/4 v0, 0x1

    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/squareup/okhttp/internal/spdy/ak;->j:Ljava/util/List;

    if-nez v2, :cond_3

    invoke-virtual {p2}, Lcom/squareup/okhttp/internal/spdy/HeadersMode;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v1, Lcom/squareup/okhttp/internal/spdy/ErrorCode;->b:Lcom/squareup/okhttp/internal/spdy/ErrorCode;

    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_5

    invoke-virtual {p0, v1}, Lcom/squareup/okhttp/internal/spdy/ak;->b(Lcom/squareup/okhttp/internal/spdy/ErrorCode;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    :try_start_1
    iput-object p1, p0, Lcom/squareup/okhttp/internal/spdy/ak;->j:Ljava/util/List;

    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/spdy/ak;->a()Z

    move-result v0

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_3
    :try_start_2
    invoke-virtual {p2}, Lcom/squareup/okhttp/internal/spdy/HeadersMode;->d()Z

    move-result v2

    if-eqz v2, :cond_4

    sget-object v1, Lcom/squareup/okhttp/internal/spdy/ErrorCode;->e:Lcom/squareup/okhttp/internal/spdy/ErrorCode;

    goto :goto_0

    :cond_4
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v3, p0, Lcom/squareup/okhttp/internal/spdy/ak;->j:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-interface {v2, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iput-object v2, p0, Lcom/squareup/okhttp/internal/spdy/ak;->j:Ljava/util/List;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :cond_5
    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ak;->f:Lcom/squareup/okhttp/internal/spdy/y;

    iget v1, p0, Lcom/squareup/okhttp/internal/spdy/ak;->e:I

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/internal/spdy/y;->b(I)Lcom/squareup/okhttp/internal/spdy/ak;

    goto :goto_1
.end method

.method public declared-synchronized a()Z
    .locals 2

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/ak;->l:Lcom/squareup/okhttp/internal/spdy/ErrorCode;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/ak;->k:Lcom/squareup/okhttp/internal/spdy/an;

    invoke-static {v1}, Lcom/squareup/okhttp/internal/spdy/an;->a(Lcom/squareup/okhttp/internal/spdy/an;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/ak;->k:Lcom/squareup/okhttp/internal/spdy/an;

    invoke-static {v1}, Lcom/squareup/okhttp/internal/spdy/an;->b(Lcom/squareup/okhttp/internal/spdy/an;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_2
    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/ak;->c:Lcom/squareup/okhttp/internal/spdy/am;

    invoke-static {v1}, Lcom/squareup/okhttp/internal/spdy/am;->a(Lcom/squareup/okhttp/internal/spdy/am;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/ak;->c:Lcom/squareup/okhttp/internal/spdy/am;

    invoke-static {v1}, Lcom/squareup/okhttp/internal/spdy/am;->b(Lcom/squareup/okhttp/internal/spdy/am;)Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/ak;->j:Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v1, :cond_0

    :cond_4
    const/4 v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method b(J)V
    .locals 2

    iget-wide v0, p0, Lcom/squareup/okhttp/internal/spdy/ak;->b:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/squareup/okhttp/internal/spdy/ak;->b:J

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    :cond_0
    return-void
.end method

.method public b(Lcom/squareup/okhttp/internal/spdy/ErrorCode;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/squareup/okhttp/internal/spdy/ak;->d(Lcom/squareup/okhttp/internal/spdy/ErrorCode;)Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ak;->f:Lcom/squareup/okhttp/internal/spdy/y;

    iget v1, p0, Lcom/squareup/okhttp/internal/spdy/ak;->e:I

    invoke-virtual {v0, v1, p1}, Lcom/squareup/okhttp/internal/spdy/y;->a(ILcom/squareup/okhttp/internal/spdy/ErrorCode;)V

    goto :goto_0
.end method

.method public b()Z
    .locals 4

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget v0, p0, Lcom/squareup/okhttp/internal/spdy/ak;->e:I

    and-int/lit8 v0, v0, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/squareup/okhttp/internal/spdy/ak;->f:Lcom/squareup/okhttp/internal/spdy/y;

    iget-boolean v3, v3, Lcom/squareup/okhttp/internal/spdy/y;->b:Z

    if-ne v3, v0, :cond_1

    :goto_1
    return v1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public declared-synchronized c()Ljava/util/List;
    .locals 10

    const-wide/16 v2, 0x0

    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/squareup/okhttp/internal/spdy/ak;->h:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_4

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    const-wide/32 v4, 0xf4240

    div-long/2addr v0, v4

    iget-wide v4, p0, Lcom/squareup/okhttp/internal/spdy/ak;->h:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    :try_start_1
    iget-object v6, p0, Lcom/squareup/okhttp/internal/spdy/ak;->j:Ljava/util/List;

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/squareup/okhttp/internal/spdy/ak;->l:Lcom/squareup/okhttp/internal/spdy/ErrorCode;

    if-nez v6, :cond_2

    iget-wide v6, p0, Lcom/squareup/okhttp/internal/spdy/ak;->h:J

    cmp-long v6, v6, v2

    if-nez v6, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    new-instance v1, Ljava/io/InterruptedIOException;

    invoke-direct {v1}, Ljava/io/InterruptedIOException;-><init>()V

    invoke-virtual {v1, v0}, Ljava/io/InterruptedIOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    throw v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    cmp-long v6, v4, v2

    if-lez v6, :cond_1

    :try_start_3
    invoke-virtual {p0, v4, v5}, Ljava/lang/Object;->wait(J)V

    iget-wide v4, p0, Lcom/squareup/okhttp/internal/spdy/ak;->h:J

    add-long/2addr v4, v0

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    const-wide/32 v8, 0xf4240

    div-long/2addr v6, v8

    sub-long/2addr v4, v6

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/net/SocketTimeoutException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Read response header timeout. readTimeoutMillis: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/squareup/okhttp/internal/spdy/ak;->h:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/SocketTimeoutException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ak;->j:Ljava/util/List;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ak;->j:Ljava/util/List;
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return-object v0

    :cond_3
    :try_start_4
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "stream was reset: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/okhttp/internal/spdy/ak;->l:Lcom/squareup/okhttp/internal/spdy/ErrorCode;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_4
    move-wide v0, v2

    move-wide v4, v2

    goto :goto_0
.end method

.method declared-synchronized c(Lcom/squareup/okhttp/internal/spdy/ErrorCode;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ak;->l:Lcom/squareup/okhttp/internal/spdy/ErrorCode;

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/squareup/okhttp/internal/spdy/ak;->l:Lcom/squareup/okhttp/internal/spdy/ErrorCode;

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public d()J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/okhttp/internal/spdy/ak;->h:J

    return-wide v0
.end method

.method public e()Lgb;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ak;->k:Lcom/squareup/okhttp/internal/spdy/an;

    return-object v0
.end method

.method public f()Lga;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ak;->j:Ljava/util/List;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/spdy/ak;->b()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "reply before requesting the sink"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ak;->c:Lcom/squareup/okhttp/internal/spdy/am;

    return-object v0
.end method

.method g()V
    .locals 2

    sget-boolean v0, Lcom/squareup/okhttp/internal/spdy/ak;->d:Z

    if-nez v0, :cond_0

    invoke-static {p0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ak;->k:Lcom/squareup/okhttp/internal/spdy/an;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/okhttp/internal/spdy/an;->a(Lcom/squareup/okhttp/internal/spdy/an;Z)Z

    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/spdy/ak;->a()Z

    move-result v0

    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ak;->f:Lcom/squareup/okhttp/internal/spdy/y;

    iget v1, p0, Lcom/squareup/okhttp/internal/spdy/ak;->e:I

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/internal/spdy/y;->b(I)Lcom/squareup/okhttp/internal/spdy/ak;

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
