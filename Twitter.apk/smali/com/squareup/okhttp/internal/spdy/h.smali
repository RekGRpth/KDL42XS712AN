.class public final Lcom/squareup/okhttp/internal/spdy/h;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/squareup/okhttp/internal/spdy/ao;


# static fields
.field private static final a:Lfi;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string/jumbo v0, "PRI * HTTP/2.0\r\n\r\nSM\r\n\r\n"

    invoke-static {v0}, Lfi;->a(Ljava/lang/String;)Lfi;

    move-result-object v0

    sput-object v0, Lcom/squareup/okhttp/internal/spdy/h;->a:Lfi;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;
    .locals 1

    invoke-static {p0, p1}, Lcom/squareup/okhttp/internal/spdy/h;->d(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b()Lfi;
    .locals 1

    sget-object v0, Lcom/squareup/okhttp/internal/spdy/h;->a:Lfi;

    return-object v0
.end method

.method static synthetic b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/IllegalArgumentException;
    .locals 1

    invoke-static {p0, p1}, Lcom/squareup/okhttp/internal/spdy/h;->c(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    return-object v0
.end method

.method private static varargs c(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/IllegalArgumentException;
    .locals 2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static varargs d(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;
    .locals 2

    new-instance v0, Ljava/io/IOException;

    invoke-static {p0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public a()I
    .locals 1

    const/16 v0, 0x3fff

    return v0
.end method

.method public a(Lfh;Z)Lcom/squareup/okhttp/internal/spdy/a;
    .locals 2

    new-instance v0, Lcom/squareup/okhttp/internal/spdy/j;

    const/16 v1, 0x1000

    invoke-direct {v0, p1, v1, p2}, Lcom/squareup/okhttp/internal/spdy/j;-><init>(Lfh;IZ)V

    return-object v0
.end method

.method public a(Lfg;Z)Lcom/squareup/okhttp/internal/spdy/c;
    .locals 1

    new-instance v0, Lcom/squareup/okhttp/internal/spdy/k;

    invoke-direct {v0, p1, p2}, Lcom/squareup/okhttp/internal/spdy/k;-><init>(Lfg;Z)V

    return-object v0
.end method
