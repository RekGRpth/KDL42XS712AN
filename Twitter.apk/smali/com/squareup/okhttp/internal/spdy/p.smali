.class Lcom/squareup/okhttp/internal/spdy/p;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lgb;


# instance fields
.field final synthetic a:Lfh;

.field final synthetic b:Lcom/squareup/okhttp/internal/spdy/o;


# direct methods
.method constructor <init>(Lcom/squareup/okhttp/internal/spdy/o;Lfh;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/okhttp/internal/spdy/p;->b:Lcom/squareup/okhttp/internal/spdy/o;

    iput-object p2, p0, Lcom/squareup/okhttp/internal/spdy/p;->a:Lfh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public b(Lfo;J)J
    .locals 5

    const-wide/16 v0, -0x1

    iget-object v2, p0, Lcom/squareup/okhttp/internal/spdy/p;->b:Lcom/squareup/okhttp/internal/spdy/o;

    invoke-static {v2}, Lcom/squareup/okhttp/internal/spdy/o;->a(Lcom/squareup/okhttp/internal/spdy/o;)I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-wide v0

    :cond_1
    iget-object v2, p0, Lcom/squareup/okhttp/internal/spdy/p;->a:Lfh;

    iget-object v3, p0, Lcom/squareup/okhttp/internal/spdy/p;->b:Lcom/squareup/okhttp/internal/spdy/o;

    invoke-static {v3}, Lcom/squareup/okhttp/internal/spdy/o;->a(Lcom/squareup/okhttp/internal/spdy/o;)I

    move-result v3

    int-to-long v3, v3

    invoke-static {p2, p3, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v3

    invoke-interface {v2, p1, v3, v4}, Lfh;->b(Lfo;J)J

    move-result-wide v2

    cmp-long v4, v2, v0

    if-eqz v4, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/p;->b:Lcom/squareup/okhttp/internal/spdy/o;

    invoke-static {v0, v2, v3}, Lcom/squareup/okhttp/internal/spdy/o;->a(Lcom/squareup/okhttp/internal/spdy/o;J)I

    move-wide v0, v2

    goto :goto_0
.end method

.method public close()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/p;->a:Lfh;

    invoke-interface {v0}, Lfh;->close()V

    return-void
.end method
