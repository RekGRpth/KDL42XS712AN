.class public Lcom/squareup/okhttp/internal/spdy/ag;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lfh;

.field private c:Lfg;

.field private d:Lcom/squareup/okhttp/internal/spdy/m;

.field private e:Lcom/squareup/okhttp/Protocol;

.field private f:Lcom/squareup/okhttp/internal/spdy/s;

.field private g:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;ZLfh;Lfg;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lcom/squareup/okhttp/internal/spdy/m;->a:Lcom/squareup/okhttp/internal/spdy/m;

    iput-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ag;->d:Lcom/squareup/okhttp/internal/spdy/m;

    sget-object v0, Lcom/squareup/okhttp/Protocol;->b:Lcom/squareup/okhttp/Protocol;

    iput-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ag;->e:Lcom/squareup/okhttp/Protocol;

    sget-object v0, Lcom/squareup/okhttp/internal/spdy/s;->a:Lcom/squareup/okhttp/internal/spdy/s;

    iput-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ag;->f:Lcom/squareup/okhttp/internal/spdy/s;

    iput-object p1, p0, Lcom/squareup/okhttp/internal/spdy/ag;->a:Ljava/lang/String;

    iput-boolean p2, p0, Lcom/squareup/okhttp/internal/spdy/ag;->g:Z

    iput-object p3, p0, Lcom/squareup/okhttp/internal/spdy/ag;->b:Lfh;

    iput-object p4, p0, Lcom/squareup/okhttp/internal/spdy/ag;->c:Lfg;

    return-void
.end method

.method static synthetic a(Lcom/squareup/okhttp/internal/spdy/ag;)Lcom/squareup/okhttp/Protocol;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ag;->e:Lcom/squareup/okhttp/Protocol;

    return-object v0
.end method

.method static synthetic b(Lcom/squareup/okhttp/internal/spdy/ag;)Lcom/squareup/okhttp/internal/spdy/s;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ag;->f:Lcom/squareup/okhttp/internal/spdy/s;

    return-object v0
.end method

.method static synthetic c(Lcom/squareup/okhttp/internal/spdy/ag;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/spdy/ag;->g:Z

    return v0
.end method

.method static synthetic d(Lcom/squareup/okhttp/internal/spdy/ag;)Lcom/squareup/okhttp/internal/spdy/m;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ag;->d:Lcom/squareup/okhttp/internal/spdy/m;

    return-object v0
.end method

.method static synthetic e(Lcom/squareup/okhttp/internal/spdy/ag;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ag;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/squareup/okhttp/internal/spdy/ag;)Lfh;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ag;->b:Lfh;

    return-object v0
.end method

.method static synthetic g(Lcom/squareup/okhttp/internal/spdy/ag;)Lfg;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/ag;->c:Lfg;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/squareup/okhttp/Protocol;)Lcom/squareup/okhttp/internal/spdy/ag;
    .locals 0

    iput-object p1, p0, Lcom/squareup/okhttp/internal/spdy/ag;->e:Lcom/squareup/okhttp/Protocol;

    return-object p0
.end method

.method public a()Lcom/squareup/okhttp/internal/spdy/y;
    .locals 2

    new-instance v0, Lcom/squareup/okhttp/internal/spdy/y;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/okhttp/internal/spdy/y;-><init>(Lcom/squareup/okhttp/internal/spdy/ag;Lcom/squareup/okhttp/internal/spdy/z;)V

    return-object v0
.end method
