.class final Lcom/squareup/okhttp/internal/spdy/g;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lfo;


# direct methods
.method constructor <init>(Lfo;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/okhttp/internal/spdy/g;->a:Lfo;

    return-void
.end method


# virtual methods
.method a(III)V
    .locals 3

    if-ge p1, p2, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/g;->a:Lfo;

    or-int v1, p3, p1

    invoke-virtual {v0, v1}, Lfo;->c(I)Lfo;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/g;->a:Lfo;

    or-int v1, p3, p2

    invoke-virtual {v0, v1}, Lfo;->c(I)Lfo;

    sub-int v0, p1, p2

    :goto_1
    const/16 v1, 0x80

    if-lt v0, v1, :cond_1

    and-int/lit8 v1, v0, 0x7f

    iget-object v2, p0, Lcom/squareup/okhttp/internal/spdy/g;->a:Lfo;

    or-int/lit16 v1, v1, 0x80

    invoke-virtual {v2, v1}, Lfo;->c(I)Lfo;

    ushr-int/lit8 v0, v0, 0x7

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/g;->a:Lfo;

    invoke-virtual {v1, v0}, Lfo;->c(I)Lfo;

    goto :goto_0
.end method

.method a(Lfi;)V
    .locals 3

    invoke-virtual {p1}, Lfi;->e()I

    move-result v0

    const/16 v1, 0x7f

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/squareup/okhttp/internal/spdy/g;->a(III)V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/g;->a:Lfo;

    invoke-virtual {v0, p1}, Lfo;->b(Lfi;)Lfo;

    return-void
.end method

.method a(Ljava/util/List;)V
    .locals 5

    const/16 v4, 0x40

    const/4 v0, 0x0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/okhttp/internal/spdy/d;

    iget-object v3, v0, Lcom/squareup/okhttp/internal/spdy/d;->h:Lfi;

    invoke-static {}, Lcom/squareup/okhttp/internal/spdy/e;->b()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    const/16 v3, 0x3f

    invoke-virtual {p0, v0, v3, v4}, Lcom/squareup/okhttp/internal/spdy/g;->a(III)V

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/okhttp/internal/spdy/d;

    iget-object v0, v0, Lcom/squareup/okhttp/internal/spdy/d;->i:Lfi;

    invoke-virtual {p0, v0}, Lcom/squareup/okhttp/internal/spdy/g;->a(Lfi;)V

    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/g;->a:Lfo;

    invoke-virtual {v0, v4}, Lfo;->c(I)Lfo;

    invoke-virtual {p0, v3}, Lcom/squareup/okhttp/internal/spdy/g;->a(Lfi;)V

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/okhttp/internal/spdy/d;

    iget-object v0, v0, Lcom/squareup/okhttp/internal/spdy/d;->i:Lfi;

    invoke-virtual {p0, v0}, Lcom/squareup/okhttp/internal/spdy/g;->a(Lfi;)V

    goto :goto_1

    :cond_1
    return-void
.end method
