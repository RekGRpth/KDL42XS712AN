.class final Lcom/squareup/okhttp/internal/spdy/k;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/squareup/okhttp/internal/spdy/c;


# instance fields
.field private final a:Lfg;

.field private final b:Z

.field private final c:Lfo;

.field private final d:Lcom/squareup/okhttp/internal/spdy/g;

.field private e:Z


# direct methods
.method constructor <init>(Lfg;Z)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/okhttp/internal/spdy/k;->a:Lfg;

    iput-boolean p2, p0, Lcom/squareup/okhttp/internal/spdy/k;->b:Z

    new-instance v0, Lfo;

    invoke-direct {v0}, Lfo;-><init>()V

    iput-object v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->c:Lfo;

    new-instance v0, Lcom/squareup/okhttp/internal/spdy/g;

    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/k;->c:Lfo;

    invoke-direct {v0, v1}, Lcom/squareup/okhttp/internal/spdy/g;-><init>(Lfo;)V

    iput-object v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->d:Lcom/squareup/okhttp/internal/spdy/g;

    return-void
.end method

.method private a(ZIILjava/util/List;)V
    .locals 5

    const/4 v4, -0x1

    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->e:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->c:Lfo;

    invoke-virtual {v0}, Lfo;->l()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->d:Lcom/squareup/okhttp/internal/spdy/g;

    invoke-virtual {v0, p4}, Lcom/squareup/okhttp/internal/spdy/g;->a(Ljava/util/List;)V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->c:Lfo;

    invoke-virtual {v0}, Lfo;->l()J

    move-result-wide v0

    long-to-int v0, v0

    const/4 v2, 0x1

    const/4 v1, 0x4

    if-eqz p1, :cond_2

    const/4 v1, 0x5

    int-to-byte v1, v1

    :cond_2
    if-eq p3, v4, :cond_3

    or-int/lit8 v1, v1, 0x8

    int-to-byte v1, v1

    :cond_3
    if-eq p3, v4, :cond_4

    add-int/lit8 v0, v0, 0x4

    :cond_4
    invoke-virtual {p0, v0, v2, v1, p2}, Lcom/squareup/okhttp/internal/spdy/k;->a(IBBI)V

    if-eq p3, v4, :cond_5

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->a:Lfg;

    const v1, 0x7fffffff

    and-int/2addr v1, p3

    invoke-interface {v0, v1}, Lfg;->b(I)Lfg;

    :cond_5
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->a:Lfg;

    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/k;->c:Lfo;

    iget-object v2, p0, Lcom/squareup/okhttp/internal/spdy/k;->c:Lfo;

    invoke-virtual {v2}, Lfo;->l()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Lfg;->a(Lfo;J)V

    return-void
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->e:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->b:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_1

    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->a:Lfg;

    invoke-static {}, Lcom/squareup/okhttp/internal/spdy/h;->b()Lfi;

    move-result-object v1

    invoke-virtual {v1}, Lfi;->f()[B

    move-result-object v1

    invoke-interface {v0, v1}, Lfg;->a([B)Lfg;

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->a:Lfg;

    invoke-interface {v0}, Lfg;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method a(IBBI)V
    .locals 4

    const/4 v1, 0x1

    const/4 v3, 0x0

    const/16 v0, 0x3fff

    if-le p1, v0, :cond_0

    const-string/jumbo v0, "FRAME_SIZE_ERROR length > 16383: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/okhttp/internal/spdy/h;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    :cond_0
    const/high16 v0, -0x80000000

    and-int/2addr v0, p4

    if-eqz v0, :cond_1

    const-string/jumbo v0, "reserved bit set: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/squareup/okhttp/internal/spdy/h;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->a:Lfg;

    and-int/lit16 v1, p1, 0x3fff

    shl-int/lit8 v1, v1, 0x10

    and-int/lit16 v2, p2, 0xff

    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v1, v2

    and-int/lit16 v2, p3, 0xff

    or-int/2addr v1, v2

    invoke-interface {v0, v1}, Lfg;->b(I)Lfg;

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->a:Lfg;

    const v1, 0x7fffffff

    and-int/2addr v1, p4

    invoke-interface {v0, v1}, Lfg;->b(I)Lfg;

    return-void
.end method

.method a(IBLfo;I)V
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0, p4, v0, p2, p1}, Lcom/squareup/okhttp/internal/spdy/k;->a(IBBI)V

    if-lez p4, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->a:Lfg;

    int-to-long v1, p4

    invoke-interface {v0, p3, v1, v2}, Lfg;->a(Lfo;J)V

    :cond_0
    return-void
.end method

.method public declared-synchronized a(IILjava/util/List;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->e:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->c:Lfo;

    invoke-virtual {v0}, Lfo;->l()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->d:Lcom/squareup/okhttp/internal/spdy/g;

    invoke-virtual {v0, p3}, Lcom/squareup/okhttp/internal/spdy/g;->a(Ljava/util/List;)V

    const-wide/16 v0, 0x4

    iget-object v2, p0, Lcom/squareup/okhttp/internal/spdy/k;->c:Lfo;

    invoke-virtual {v2}, Lfo;->l()J

    move-result-wide v2

    add-long/2addr v0, v2

    long-to-int v0, v0

    const/4 v1, 0x5

    const/4 v2, 0x4

    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/squareup/okhttp/internal/spdy/k;->a(IBBI)V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->a:Lfg;

    const v1, 0x7fffffff

    and-int/2addr v1, p2

    invoke-interface {v0, v1}, Lfg;->b(I)Lfg;

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->a:Lfg;

    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/k;->c:Lfo;

    iget-object v2, p0, Lcom/squareup/okhttp/internal/spdy/k;->c:Lfo;

    invoke-virtual {v2}, Lfo;->l()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Lfg;->a(Lfo;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method public declared-synchronized a(IJ)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->e:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-eqz v0, :cond_1

    const-wide/32 v0, 0x7fffffff

    cmp-long v0, p2, v0

    if-lez v0, :cond_2

    :cond_1
    :try_start_1
    const-string/jumbo v0, "windowSizeIncrement == 0 || windowSizeIncrement > 0x7fffffffL: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/squareup/okhttp/internal/spdy/h;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    :cond_2
    const/4 v0, 0x4

    const/16 v1, 0x9

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/squareup/okhttp/internal/spdy/k;->a(IBBI)V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->a:Lfg;

    long-to-int v1, p2

    invoke-interface {v0, v1}, Lfg;->b(I)Lfg;

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->a:Lfg;

    invoke-interface {v0}, Lfg;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method public declared-synchronized a(ILcom/squareup/okhttp/internal/spdy/ErrorCode;)V
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->e:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget v0, p2, Lcom/squareup/okhttp/internal/spdy/ErrorCode;->spdyRstCode:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    const/4 v0, 0x4

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/squareup/okhttp/internal/spdy/k;->a(IBBI)V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->a:Lfg;

    iget v1, p2, Lcom/squareup/okhttp/internal/spdy/ErrorCode;->httpCode:I

    invoke-interface {v0, v1}, Lfg;->b(I)Lfg;

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->a:Lfg;

    invoke-interface {v0}, Lfg;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method public declared-synchronized a(ILcom/squareup/okhttp/internal/spdy/ErrorCode;[B)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->e:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget v0, p2, Lcom/squareup/okhttp/internal/spdy/ErrorCode;->httpCode:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    const-string/jumbo v0, "errorCode.httpCode == -1"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/squareup/okhttp/internal/spdy/h;->b(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    :cond_1
    array-length v0, p3

    add-int/lit8 v0, v0, 0x8

    const/4 v1, 0x7

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/squareup/okhttp/internal/spdy/k;->a(IBBI)V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->a:Lfg;

    invoke-interface {v0, p1}, Lfg;->b(I)Lfg;

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->a:Lfg;

    iget v1, p2, Lcom/squareup/okhttp/internal/spdy/ErrorCode;->httpCode:I

    invoke-interface {v0, v1}, Lfg;->b(I)Lfg;

    array-length v0, p3

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->a:Lfg;

    invoke-interface {v0, p3}, Lfg;->a([B)Lfg;

    :cond_2
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->a:Lfg;

    invoke-interface {v0}, Lfg;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method public declared-synchronized a(Lcom/squareup/okhttp/internal/spdy/u;)V
    .locals 5

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/squareup/okhttp/internal/spdy/k;->e:Z

    if-eqz v1, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    invoke-virtual {p1}, Lcom/squareup/okhttp/internal/spdy/u;->b()I

    move-result v1

    mul-int/lit8 v1, v1, 0x8

    const/4 v2, 0x4

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/squareup/okhttp/internal/spdy/k;->a(IBBI)V

    :goto_0
    const/16 v1, 0xa

    if-ge v0, v1, :cond_2

    invoke-virtual {p1, v0}, Lcom/squareup/okhttp/internal/spdy/u;->a(I)Z

    move-result v1

    if-nez v1, :cond_1

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/k;->a:Lfg;

    const v2, 0xffffff

    and-int/2addr v2, v0

    invoke-interface {v1, v2}, Lfg;->b(I)Lfg;

    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/k;->a:Lfg;

    invoke-virtual {p1, v0}, Lcom/squareup/okhttp/internal/spdy/u;->b(I)I

    move-result v2

    invoke-interface {v1, v2}, Lfg;->b(I)Lfg;

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->a:Lfg;

    invoke-interface {v0}, Lfg;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method public declared-synchronized a(ZII)V
    .locals 4

    const/4 v0, 0x0

    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/squareup/okhttp/internal/spdy/k;->e:Z

    if-eqz v1, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    const/16 v1, 0x8

    const/4 v2, 0x6

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    :cond_1
    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {p0, v1, v2, v0, v3}, Lcom/squareup/okhttp/internal/spdy/k;->a(IBBI)V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->a:Lfg;

    invoke-interface {v0, p2}, Lfg;->b(I)Lfg;

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->a:Lfg;

    invoke-interface {v0, p3}, Lfg;->b(I)Lfg;

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->a:Lfg;

    invoke-interface {v0}, Lfg;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method public declared-synchronized a(ZILfo;I)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->e:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    const/4 v0, 0x0

    if-eqz p1, :cond_1

    const/4 v0, 0x1

    int-to-byte v0, v0

    :cond_1
    :try_start_1
    invoke-virtual {p0, p2, v0, p3, p4}, Lcom/squareup/okhttp/internal/spdy/k;->a(IBLfo;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method public declared-synchronized a(ZZIIIILjava/util/List;)V
    .locals 2

    monitor-enter p0

    if-eqz p2, :cond_0

    :try_start_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->e:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-direct {p0, p1, p3, p5, p7}, Lcom/squareup/okhttp/internal/spdy/k;->a(ZIILjava/util/List;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method public declared-synchronized b()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->e:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x4

    const/4 v2, 0x1

    const/4 v3, 0x0

    :try_start_1
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/squareup/okhttp/internal/spdy/k;->a(IBBI)V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->a:Lfg;

    invoke-interface {v0}, Lfg;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method public declared-synchronized c()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->e:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->a:Lfg;

    invoke-interface {v0}, Lfg;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method public declared-synchronized close()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->e:Z

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/k;->a:Lfg;

    invoke-interface {v0}, Lfg;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
