.class public final Lcom/squareup/okhttp/internal/http/i;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final a:Lcom/squareup/okhttp/g;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/squareup/okhttp/internal/http/j;

    invoke-direct {v0}, Lcom/squareup/okhttp/internal/http/j;-><init>()V

    sput-object v0, Lcom/squareup/okhttp/internal/http/i;->a:Lcom/squareup/okhttp/g;

    return-void
.end method

.method public static a(Lcom/squareup/okhttp/g;Lcom/squareup/okhttp/internal/http/ag;Ljava/net/Proxy;)Lcom/squareup/okhttp/internal/http/aa;
    .locals 6

    const/4 v2, 0x0

    const/16 v5, 0x197

    invoke-virtual {p1}, Lcom/squareup/okhttp/internal/http/ag;->c()I

    move-result v0

    const/16 v1, 0x191

    if-ne v0, v1, :cond_0

    const-string/jumbo v1, "WWW-Authenticate"

    const-string/jumbo v0, "Authorization"

    :goto_0
    invoke-virtual {p1}, Lcom/squareup/okhttp/internal/http/ag;->g()Lcom/squareup/okhttp/internal/http/f;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/squareup/okhttp/internal/http/i;->a(Lcom/squareup/okhttp/internal/http/f;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    move-object v0, v2

    :goto_1
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/okhttp/internal/http/ag;->c()I

    move-result v0

    if-ne v0, v5, :cond_1

    const-string/jumbo v1, "Proxy-Authenticate"

    const-string/jumbo v0, "Proxy-Authorization"

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_2
    invoke-virtual {p1}, Lcom/squareup/okhttp/internal/http/ag;->a()Lcom/squareup/okhttp/internal/http/aa;

    move-result-object v3

    invoke-virtual {p1}, Lcom/squareup/okhttp/internal/http/ag;->c()I

    move-result v4

    if-ne v4, v5, :cond_3

    invoke-virtual {v3}, Lcom/squareup/okhttp/internal/http/aa;->a()Ljava/net/URL;

    move-result-object v4

    invoke-interface {p0, p2, v4, v1}, Lcom/squareup/okhttp/g;->b(Ljava/net/Proxy;Ljava/net/URL;Ljava/util/List;)Lcom/squareup/okhttp/i;

    move-result-object v1

    :goto_2
    if-nez v1, :cond_4

    move-object v0, v2

    goto :goto_1

    :cond_3
    invoke-virtual {v3}, Lcom/squareup/okhttp/internal/http/aa;->a()Ljava/net/URL;

    move-result-object v4

    invoke-interface {p0, p2, v4, v1}, Lcom/squareup/okhttp/g;->a(Ljava/net/Proxy;Ljava/net/URL;Ljava/util/List;)Lcom/squareup/okhttp/i;

    move-result-object v1

    goto :goto_2

    :cond_4
    invoke-virtual {v3}, Lcom/squareup/okhttp/internal/http/aa;->f()Lcom/squareup/okhttp/internal/http/ad;

    move-result-object v2

    invoke-virtual {v1}, Lcom/squareup/okhttp/i;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lcom/squareup/okhttp/internal/http/ad;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/ad;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/ad;->a()Lcom/squareup/okhttp/internal/http/aa;

    move-result-object v0

    goto :goto_1
.end method

.method private static a(Lcom/squareup/okhttp/internal/http/f;Ljava/lang/String;)Ljava/util/List;
    .locals 9

    const/4 v4, 0x0

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    move v6, v4

    :goto_0
    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/http/f;->a()I

    move-result v0

    if-ge v6, v0, :cond_2

    invoke-virtual {p0, v6}, Lcom/squareup/okhttp/internal/http/f;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v6}, Lcom/squareup/okhttp/internal/http/f;->b(I)Ljava/lang/String;

    move-result-object v0

    move v1, v4

    :goto_1
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_0

    const-string/jumbo v2, " "

    invoke-static {v0, v1, v2}, Lcom/squareup/okhttp/internal/http/e;->a(Ljava/lang/String;ILjava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v8

    invoke-static {v0, v2}, Lcom/squareup/okhttp/internal/http/e;->a(Ljava/lang/String;I)I

    move-result v2

    const/4 v1, 0x1

    const-string/jumbo v3, "realm=\""

    const-string/jumbo v5, "realm=\""

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual/range {v0 .. v5}, Ljava/lang/String;->regionMatches(ZILjava/lang/String;II)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "realm=\""

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v1, v2

    const-string/jumbo v2, "\""

    invoke-static {v0, v1, v2}, Lcom/squareup/okhttp/internal/http/e;->a(Ljava/lang/String;ILjava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    add-int/lit8 v1, v2, 0x1

    const-string/jumbo v2, ","

    invoke-static {v0, v1, v2}, Lcom/squareup/okhttp/internal/http/e;->a(Ljava/lang/String;ILjava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/squareup/okhttp/internal/http/e;->a(Ljava/lang/String;I)I

    move-result v1

    new-instance v2, Lcom/squareup/okhttp/h;

    invoke-direct {v2, v8, v3}, Lcom/squareup/okhttp/h;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v7, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    return-object v7
.end method
