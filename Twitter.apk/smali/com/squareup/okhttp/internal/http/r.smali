.class Lcom/squareup/okhttp/internal/http/r;
.super Lcom/squareup/okhttp/internal/http/m;
.source "Twttr"

# interfaces
.implements Lgb;


# instance fields
.field final synthetic d:Lcom/squareup/okhttp/internal/http/k;

.field private e:Z


# direct methods
.method constructor <init>(Lcom/squareup/okhttp/internal/http/k;Ljava/net/CacheRequest;)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/okhttp/internal/http/r;->d:Lcom/squareup/okhttp/internal/http/k;

    invoke-direct {p0, p1, p2}, Lcom/squareup/okhttp/internal/http/m;-><init>(Lcom/squareup/okhttp/internal/http/k;Ljava/net/CacheRequest;)V

    return-void
.end method


# virtual methods
.method public b(Lfo;J)J
    .locals 5

    const-wide/16 v0, -0x1

    const-wide/16 v2, 0x0

    cmp-long v2, p2, v2

    if-gez v2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "byteCount < 0: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-boolean v2, p0, Lcom/squareup/okhttp/internal/http/r;->b:Z

    if-eqz v2, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-boolean v2, p0, Lcom/squareup/okhttp/internal/http/r;->e:Z

    if-eqz v2, :cond_2

    :goto_0
    return-wide v0

    :cond_2
    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/r;->d:Lcom/squareup/okhttp/internal/http/k;

    invoke-static {v2}, Lcom/squareup/okhttp/internal/http/k;->f(Lcom/squareup/okhttp/internal/http/k;)Lfh;

    move-result-object v2

    invoke-interface {v2, p1, p2, p3}, Lfh;->b(Lfo;J)J

    move-result-wide v2

    cmp-long v4, v2, v0

    if-nez v4, :cond_3

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/squareup/okhttp/internal/http/r;->e:Z

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/squareup/okhttp/internal/http/r;->a(Z)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0, p1, v2, v3}, Lcom/squareup/okhttp/internal/http/r;->a(Lfo;J)V

    move-wide v0, v2

    goto :goto_0
.end method

.method public close()V
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/http/r;->b:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/http/r;->e:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/http/r;->a()V

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/squareup/okhttp/internal/http/r;->b:Z

    goto :goto_0
.end method
