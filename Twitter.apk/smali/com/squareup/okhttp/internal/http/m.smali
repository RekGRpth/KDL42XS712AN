.class Lcom/squareup/okhttp/internal/http/m;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field protected final a:Ljava/io/OutputStream;

.field protected b:Z

.field final synthetic c:Lcom/squareup/okhttp/internal/http/k;

.field private final d:Ljava/net/CacheRequest;


# direct methods
.method constructor <init>(Lcom/squareup/okhttp/internal/http/k;Ljava/net/CacheRequest;)V
    .locals 2

    const/4 v0, 0x0

    iput-object p1, p0, Lcom/squareup/okhttp/internal/http/m;->c:Lcom/squareup/okhttp/internal/http/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/net/CacheRequest;->getBody()Ljava/io/OutputStream;

    move-result-object v1

    :goto_0
    if-nez v1, :cond_0

    move-object p2, v0

    :cond_0
    iput-object v1, p0, Lcom/squareup/okhttp/internal/http/m;->a:Ljava/io/OutputStream;

    iput-object p2, p0, Lcom/squareup/okhttp/internal/http/m;->d:Ljava/net/CacheRequest;

    return-void

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method


# virtual methods
.method protected final a()V
    .locals 2

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/m;->d:Ljava/net/CacheRequest;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/m;->d:Ljava/net/CacheRequest;

    invoke-virtual {v0}, Ljava/net/CacheRequest;->abort()V

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/m;->c:Lcom/squareup/okhttp/internal/http/k;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/http/k;->d(Lcom/squareup/okhttp/internal/http/k;)Lcom/squareup/okhttp/c;

    move-result-object v0

    invoke-static {v0}, Lfd;->a(Ljava/io/Closeable;)V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/m;->c:Lcom/squareup/okhttp/internal/http/k;

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/squareup/okhttp/internal/http/k;->a(Lcom/squareup/okhttp/internal/http/k;I)I

    return-void
.end method

.method protected final a(Lfo;J)V
    .locals 6

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/m;->a:Ljava/io/OutputStream;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lfo;->l()J

    move-result-wide v0

    sub-long v1, v0, p2

    iget-object v5, p0, Lcom/squareup/okhttp/internal/http/m;->a:Ljava/io/OutputStream;

    move-object v0, p1

    move-wide v3, p2

    invoke-static/range {v0 .. v5}, Lfr;->a(Lfo;JJLjava/io/OutputStream;)V

    :cond_0
    return-void
.end method

.method protected final a(Z)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/m;->c:Lcom/squareup/okhttp/internal/http/k;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/http/k;->b(Lcom/squareup/okhttp/internal/http/k;)I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/m;->c:Lcom/squareup/okhttp/internal/http/k;

    invoke-static {v2}, Lcom/squareup/okhttp/internal/http/k;->b(Lcom/squareup/okhttp/internal/http/k;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/m;->d:Ljava/net/CacheRequest;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/m;->a:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/m;->c:Lcom/squareup/okhttp/internal/http/k;

    invoke-static {v0, v2}, Lcom/squareup/okhttp/internal/http/k;->a(Lcom/squareup/okhttp/internal/http/k;I)I

    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/m;->c:Lcom/squareup/okhttp/internal/http/k;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/http/k;->c(Lcom/squareup/okhttp/internal/http/k;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/m;->c:Lcom/squareup/okhttp/internal/http/k;

    invoke-static {v0, v2}, Lcom/squareup/okhttp/internal/http/k;->b(Lcom/squareup/okhttp/internal/http/k;I)I

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/m;->c:Lcom/squareup/okhttp/internal/http/k;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/http/k;->e(Lcom/squareup/okhttp/internal/http/k;)Lcom/squareup/okhttp/d;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/m;->c:Lcom/squareup/okhttp/internal/http/k;

    invoke-static {v1}, Lcom/squareup/okhttp/internal/http/k;->d(Lcom/squareup/okhttp/internal/http/k;)Lcom/squareup/okhttp/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/d;->a(Lcom/squareup/okhttp/c;)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/m;->c:Lcom/squareup/okhttp/internal/http/k;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/http/k;->c(Lcom/squareup/okhttp/internal/http/k;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/m;->c:Lcom/squareup/okhttp/internal/http/k;

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/squareup/okhttp/internal/http/k;->a(Lcom/squareup/okhttp/internal/http/k;I)I

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/m;->c:Lcom/squareup/okhttp/internal/http/k;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/http/k;->d(Lcom/squareup/okhttp/internal/http/k;)Lcom/squareup/okhttp/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/c;->close()V

    goto :goto_0
.end method
