.class public final Lcom/squareup/okhttp/internal/http/x;
.super Lcom/squareup/okhttp/internal/http/d;
.source "Twttr"


# instance fields
.field private final a:Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;


# direct methods
.method public constructor <init>(Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/okhttp/internal/http/d;-><init>(Ljava/net/HttpURLConnection;)V

    iput-object p1, p0, Lcom/squareup/okhttp/internal/http/x;->a:Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;

    return-void
.end method

.method public constructor <init>(Ljava/net/URL;Lcom/squareup/okhttp/j;)V
    .locals 1

    new-instance v0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;

    invoke-direct {v0, p1, p2}, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;-><init>(Ljava/net/URL;Lcom/squareup/okhttp/j;)V

    invoke-direct {p0, v0}, Lcom/squareup/okhttp/internal/http/x;-><init>(Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;)V

    return-void
.end method


# virtual methods
.method protected a()Lcom/squareup/okhttp/f;
    .locals 2

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/x;->a:Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;

    iget-object v0, v0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->c:Lcom/squareup/okhttp/internal/http/u;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Connection has not yet been established"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/x;->a:Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;

    iget-object v0, v0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->c:Lcom/squareup/okhttp/internal/http/u;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/u;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/x;->a:Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;

    iget-object v0, v0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->c:Lcom/squareup/okhttp/internal/http/u;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/u;->h()Lcom/squareup/okhttp/internal/http/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/ag;->f()Lcom/squareup/okhttp/f;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/x;->a:Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;

    iget-object v0, v0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->d:Lcom/squareup/okhttp/f;

    goto :goto_0
.end method

.method public bridge synthetic addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-super {p0, p1, p2}, Lcom/squareup/okhttp/internal/http/d;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic connect()V
    .locals 0

    invoke-super {p0}, Lcom/squareup/okhttp/internal/http/d;->connect()V

    return-void
.end method

.method public bridge synthetic disconnect()V
    .locals 0

    invoke-super {p0}, Lcom/squareup/okhttp/internal/http/d;->disconnect()V

    return-void
.end method

.method public bridge synthetic getAllowUserInteraction()Z
    .locals 1

    invoke-super {p0}, Lcom/squareup/okhttp/internal/http/d;->getAllowUserInteraction()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic getCipherSuite()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lcom/squareup/okhttp/internal/http/d;->getCipherSuite()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getConnectTimeout()I
    .locals 1

    invoke-super {p0}, Lcom/squareup/okhttp/internal/http/d;->getConnectTimeout()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getContent()Ljava/lang/Object;
    .locals 1

    invoke-super {p0}, Lcom/squareup/okhttp/internal/http/d;->getContent()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getContent([Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1

    invoke-super {p0, p1}, Lcom/squareup/okhttp/internal/http/d;->getContent([Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getContentEncoding()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lcom/squareup/okhttp/internal/http/d;->getContentEncoding()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getContentLength()I
    .locals 1

    invoke-super {p0}, Lcom/squareup/okhttp/internal/http/d;->getContentLength()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getContentType()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lcom/squareup/okhttp/internal/http/d;->getContentType()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDate()J
    .locals 2

    invoke-super {p0}, Lcom/squareup/okhttp/internal/http/d;->getDate()J

    move-result-wide v0

    return-wide v0
.end method

.method public bridge synthetic getDefaultUseCaches()Z
    .locals 1

    invoke-super {p0}, Lcom/squareup/okhttp/internal/http/d;->getDefaultUseCaches()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic getDoInput()Z
    .locals 1

    invoke-super {p0}, Lcom/squareup/okhttp/internal/http/d;->getDoInput()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic getDoOutput()Z
    .locals 1

    invoke-super {p0}, Lcom/squareup/okhttp/internal/http/d;->getDoOutput()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic getErrorStream()Ljava/io/InputStream;
    .locals 1

    invoke-super {p0}, Lcom/squareup/okhttp/internal/http/d;->getErrorStream()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getExpiration()J
    .locals 2

    invoke-super {p0}, Lcom/squareup/okhttp/internal/http/d;->getExpiration()J

    move-result-wide v0

    return-wide v0
.end method

.method public bridge synthetic getHeaderField(I)Ljava/lang/String;
    .locals 1

    invoke-super {p0, p1}, Lcom/squareup/okhttp/internal/http/d;->getHeaderField(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getHeaderField(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-super {p0, p1}, Lcom/squareup/okhttp/internal/http/d;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getHeaderFieldDate(Ljava/lang/String;J)J
    .locals 2

    invoke-super {p0, p1, p2, p3}, Lcom/squareup/okhttp/internal/http/d;->getHeaderFieldDate(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public bridge synthetic getHeaderFieldInt(Ljava/lang/String;I)I
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/squareup/okhttp/internal/http/d;->getHeaderFieldInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public bridge synthetic getHeaderFieldKey(I)Ljava/lang/String;
    .locals 1

    invoke-super {p0, p1}, Lcom/squareup/okhttp/internal/http/d;->getHeaderFieldKey(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getHeaderFields()Ljava/util/Map;
    .locals 1

    invoke-super {p0}, Lcom/squareup/okhttp/internal/http/d;->getHeaderFields()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getHostnameVerifier()Ljavax/net/ssl/HostnameVerifier;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/x;->a:Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;

    iget-object v0, v0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->a:Lcom/squareup/okhttp/j;

    invoke-virtual {v0}, Lcom/squareup/okhttp/j;->h()Ljavax/net/ssl/HostnameVerifier;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getIfModifiedSince()J
    .locals 2

    invoke-super {p0}, Lcom/squareup/okhttp/internal/http/d;->getIfModifiedSince()J

    move-result-wide v0

    return-wide v0
.end method

.method public bridge synthetic getInputStream()Ljava/io/InputStream;
    .locals 1

    invoke-super {p0}, Lcom/squareup/okhttp/internal/http/d;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getInstanceFollowRedirects()Z
    .locals 1

    invoke-super {p0}, Lcom/squareup/okhttp/internal/http/d;->getInstanceFollowRedirects()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic getLastModified()J
    .locals 2

    invoke-super {p0}, Lcom/squareup/okhttp/internal/http/d;->getLastModified()J

    move-result-wide v0

    return-wide v0
.end method

.method public bridge synthetic getLocalCertificates()[Ljava/security/cert/Certificate;
    .locals 1

    invoke-super {p0}, Lcom/squareup/okhttp/internal/http/d;->getLocalCertificates()[Ljava/security/cert/Certificate;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getLocalPrincipal()Ljava/security/Principal;
    .locals 1

    invoke-super {p0}, Lcom/squareup/okhttp/internal/http/d;->getLocalPrincipal()Ljava/security/Principal;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getOutputStream()Ljava/io/OutputStream;
    .locals 1

    invoke-super {p0}, Lcom/squareup/okhttp/internal/http/d;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getPeerPrincipal()Ljava/security/Principal;
    .locals 1

    invoke-super {p0}, Lcom/squareup/okhttp/internal/http/d;->getPeerPrincipal()Ljava/security/Principal;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getPermission()Ljava/security/Permission;
    .locals 1

    invoke-super {p0}, Lcom/squareup/okhttp/internal/http/d;->getPermission()Ljava/security/Permission;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getReadTimeout()I
    .locals 1

    invoke-super {p0}, Lcom/squareup/okhttp/internal/http/d;->getReadTimeout()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getRequestMethod()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lcom/squareup/okhttp/internal/http/d;->getRequestMethod()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getRequestProperties()Ljava/util/Map;
    .locals 1

    invoke-super {p0}, Lcom/squareup/okhttp/internal/http/d;->getRequestProperties()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getRequestProperty(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-super {p0, p1}, Lcom/squareup/okhttp/internal/http/d;->getRequestProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getResponseCode()I
    .locals 1

    invoke-super {p0}, Lcom/squareup/okhttp/internal/http/d;->getResponseCode()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getResponseMessage()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lcom/squareup/okhttp/internal/http/d;->getResponseMessage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSSLSocketFactory()Ljavax/net/ssl/SSLSocketFactory;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/x;->a:Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;

    iget-object v0, v0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->a:Lcom/squareup/okhttp/j;

    invoke-virtual {v0}, Lcom/squareup/okhttp/j;->g()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getServerCertificates()[Ljava/security/cert/Certificate;
    .locals 1

    invoke-super {p0}, Lcom/squareup/okhttp/internal/http/d;->getServerCertificates()[Ljava/security/cert/Certificate;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getURL()Ljava/net/URL;
    .locals 1

    invoke-super {p0}, Lcom/squareup/okhttp/internal/http/d;->getURL()Ljava/net/URL;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getUseCaches()Z
    .locals 1

    invoke-super {p0}, Lcom/squareup/okhttp/internal/http/d;->getUseCaches()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic setAllowUserInteraction(Z)V
    .locals 0

    invoke-super {p0, p1}, Lcom/squareup/okhttp/internal/http/d;->setAllowUserInteraction(Z)V

    return-void
.end method

.method public bridge synthetic setChunkedStreamingMode(I)V
    .locals 0

    invoke-super {p0, p1}, Lcom/squareup/okhttp/internal/http/d;->setChunkedStreamingMode(I)V

    return-void
.end method

.method public bridge synthetic setConnectTimeout(I)V
    .locals 0

    invoke-super {p0, p1}, Lcom/squareup/okhttp/internal/http/d;->setConnectTimeout(I)V

    return-void
.end method

.method public bridge synthetic setDefaultUseCaches(Z)V
    .locals 0

    invoke-super {p0, p1}, Lcom/squareup/okhttp/internal/http/d;->setDefaultUseCaches(Z)V

    return-void
.end method

.method public bridge synthetic setDoInput(Z)V
    .locals 0

    invoke-super {p0, p1}, Lcom/squareup/okhttp/internal/http/d;->setDoInput(Z)V

    return-void
.end method

.method public bridge synthetic setDoOutput(Z)V
    .locals 0

    invoke-super {p0, p1}, Lcom/squareup/okhttp/internal/http/d;->setDoOutput(Z)V

    return-void
.end method

.method public bridge synthetic setFixedLengthStreamingMode(I)V
    .locals 0

    invoke-super {p0, p1}, Lcom/squareup/okhttp/internal/http/d;->setFixedLengthStreamingMode(I)V

    return-void
.end method

.method public setHostnameVerifier(Ljavax/net/ssl/HostnameVerifier;)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/x;->a:Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;

    iget-object v0, v0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->a:Lcom/squareup/okhttp/j;

    invoke-virtual {v0, p1}, Lcom/squareup/okhttp/j;->a(Ljavax/net/ssl/HostnameVerifier;)Lcom/squareup/okhttp/j;

    return-void
.end method

.method public bridge synthetic setIfModifiedSince(J)V
    .locals 0

    invoke-super {p0, p1, p2}, Lcom/squareup/okhttp/internal/http/d;->setIfModifiedSince(J)V

    return-void
.end method

.method public bridge synthetic setInstanceFollowRedirects(Z)V
    .locals 0

    invoke-super {p0, p1}, Lcom/squareup/okhttp/internal/http/d;->setInstanceFollowRedirects(Z)V

    return-void
.end method

.method public bridge synthetic setReadTimeout(I)V
    .locals 0

    invoke-super {p0, p1}, Lcom/squareup/okhttp/internal/http/d;->setReadTimeout(I)V

    return-void
.end method

.method public bridge synthetic setRequestMethod(Ljava/lang/String;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/squareup/okhttp/internal/http/d;->setRequestMethod(Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-super {p0, p1, p2}, Lcom/squareup/okhttp/internal/http/d;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/x;->a:Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;

    iget-object v0, v0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->a:Lcom/squareup/okhttp/j;

    invoke-virtual {v0, p1}, Lcom/squareup/okhttp/j;->a(Ljavax/net/ssl/SSLSocketFactory;)Lcom/squareup/okhttp/j;

    return-void
.end method

.method public bridge synthetic setUseCaches(Z)V
    .locals 0

    invoke-super {p0, p1}, Lcom/squareup/okhttp/internal/http/d;->setUseCaches(Z)V

    return-void
.end method

.method public bridge synthetic toString()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lcom/squareup/okhttp/internal/http/d;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic usingProxy()Z
    .locals 1

    invoke-super {p0}, Lcom/squareup/okhttp/internal/http/d;->usingProxy()Z

    move-result v0

    return v0
.end method
