.class public final Lcom/squareup/okhttp/internal/http/k;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final g:[B

.field private static final h:[B


# instance fields
.field private final a:Lcom/squareup/okhttp/d;

.field private final b:Lcom/squareup/okhttp/c;

.field private final c:Lfh;

.field private final d:Lfg;

.field private e:I

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x10

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/squareup/okhttp/internal/http/k;->g:[B

    const/4 v0, 0x5

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/squareup/okhttp/internal/http/k;->h:[B

    return-void

    :array_0
    .array-data 1
        0x30t
        0x31t
        0x32t
        0x33t
        0x34t
        0x35t
        0x36t
        0x37t
        0x38t
        0x39t
        0x61t
        0x62t
        0x63t
        0x64t
        0x65t
        0x66t
    .end array-data

    :array_1
    .array-data 1
        0x30t
        0xdt
        0xat
        0xdt
        0xat
    .end array-data
.end method

.method public constructor <init>(Lcom/squareup/okhttp/d;Lcom/squareup/okhttp/c;Lfh;Lfg;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/squareup/okhttp/internal/http/k;->e:I

    iput v0, p0, Lcom/squareup/okhttp/internal/http/k;->f:I

    iput-object p1, p0, Lcom/squareup/okhttp/internal/http/k;->a:Lcom/squareup/okhttp/d;

    iput-object p2, p0, Lcom/squareup/okhttp/internal/http/k;->b:Lcom/squareup/okhttp/c;

    iput-object p3, p0, Lcom/squareup/okhttp/internal/http/k;->c:Lfh;

    iput-object p4, p0, Lcom/squareup/okhttp/internal/http/k;->d:Lfg;

    return-void
.end method

.method static synthetic a(Lcom/squareup/okhttp/internal/http/k;I)I
    .locals 0

    iput p1, p0, Lcom/squareup/okhttp/internal/http/k;->e:I

    return p1
.end method

.method static synthetic a(Lcom/squareup/okhttp/internal/http/k;)Lfg;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/k;->d:Lfg;

    return-object v0
.end method

.method static synthetic b(Lcom/squareup/okhttp/internal/http/k;)I
    .locals 1

    iget v0, p0, Lcom/squareup/okhttp/internal/http/k;->e:I

    return v0
.end method

.method static synthetic b(Lcom/squareup/okhttp/internal/http/k;I)I
    .locals 0

    iput p1, p0, Lcom/squareup/okhttp/internal/http/k;->f:I

    return p1
.end method

.method static synthetic c(Lcom/squareup/okhttp/internal/http/k;)I
    .locals 1

    iget v0, p0, Lcom/squareup/okhttp/internal/http/k;->f:I

    return v0
.end method

.method static synthetic d(Lcom/squareup/okhttp/internal/http/k;)Lcom/squareup/okhttp/c;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/k;->b:Lcom/squareup/okhttp/c;

    return-object v0
.end method

.method static synthetic e(Lcom/squareup/okhttp/internal/http/k;)Lcom/squareup/okhttp/d;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/k;->a:Lcom/squareup/okhttp/d;

    return-object v0
.end method

.method static synthetic f(Lcom/squareup/okhttp/internal/http/k;)Lfh;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/k;->c:Lfh;

    return-object v0
.end method

.method static synthetic h()[B
    .locals 1

    sget-object v0, Lcom/squareup/okhttp/internal/http/k;->h:[B

    return-object v0
.end method

.method static synthetic i()[B
    .locals 1

    sget-object v0, Lcom/squareup/okhttp/internal/http/k;->g:[B

    return-object v0
.end method


# virtual methods
.method public a(J)Lga;
    .locals 3

    iget v0, p0, Lcom/squareup/okhttp/internal/http/k;->e:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/squareup/okhttp/internal/http/k;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x2

    iput v0, p0, Lcom/squareup/okhttp/internal/http/k;->e:I

    new-instance v0, Lcom/squareup/okhttp/internal/http/p;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/squareup/okhttp/internal/http/p;-><init>(Lcom/squareup/okhttp/internal/http/k;JLcom/squareup/okhttp/internal/http/l;)V

    return-object v0
.end method

.method public a(Ljava/net/CacheRequest;)Lgb;
    .locals 3

    iget v0, p0, Lcom/squareup/okhttp/internal/http/k;->e:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/squareup/okhttp/internal/http/k;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x5

    iput v0, p0, Lcom/squareup/okhttp/internal/http/k;->e:I

    new-instance v0, Lcom/squareup/okhttp/internal/http/r;

    invoke-direct {v0, p0, p1}, Lcom/squareup/okhttp/internal/http/r;-><init>(Lcom/squareup/okhttp/internal/http/k;Ljava/net/CacheRequest;)V

    return-object v0
.end method

.method public a(Ljava/net/CacheRequest;J)Lgb;
    .locals 3

    iget v0, p0, Lcom/squareup/okhttp/internal/http/k;->e:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/squareup/okhttp/internal/http/k;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x5

    iput v0, p0, Lcom/squareup/okhttp/internal/http/k;->e:I

    new-instance v0, Lcom/squareup/okhttp/internal/http/q;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/squareup/okhttp/internal/http/q;-><init>(Lcom/squareup/okhttp/internal/http/k;Ljava/net/CacheRequest;J)V

    return-object v0
.end method

.method public a(Ljava/net/CacheRequest;Lcom/squareup/okhttp/internal/http/u;)Lgb;
    .locals 3

    iget v0, p0, Lcom/squareup/okhttp/internal/http/k;->e:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/squareup/okhttp/internal/http/k;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x5

    iput v0, p0, Lcom/squareup/okhttp/internal/http/k;->e:I

    new-instance v0, Lcom/squareup/okhttp/internal/http/o;

    invoke-direct {v0, p0, p1, p2}, Lcom/squareup/okhttp/internal/http/o;-><init>(Lcom/squareup/okhttp/internal/http/k;Ljava/net/CacheRequest;Lcom/squareup/okhttp/internal/http/u;)V

    return-object v0
.end method

.method public a()V
    .locals 2

    const/4 v0, 0x1

    iput v0, p0, Lcom/squareup/okhttp/internal/http/k;->f:I

    iget v0, p0, Lcom/squareup/okhttp/internal/http/k;->e:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/squareup/okhttp/internal/http/k;->f:I

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/k;->a:Lcom/squareup/okhttp/d;

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/k;->b:Lcom/squareup/okhttp/c;

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/d;->a(Lcom/squareup/okhttp/c;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/squareup/okhttp/internal/http/ap;)V
    .locals 3

    iget v0, p0, Lcom/squareup/okhttp/internal/http/k;->e:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/squareup/okhttp/internal/http/k;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x3

    iput v0, p0, Lcom/squareup/okhttp/internal/http/k;->e:I

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/k;->d:Lfg;

    invoke-virtual {p1, v0}, Lcom/squareup/okhttp/internal/http/ap;->a(Lfg;)V

    return-void
.end method

.method public a(Lcom/squareup/okhttp/internal/http/f;Ljava/lang/String;)V
    .locals 3

    iget v0, p0, Lcom/squareup/okhttp/internal/http/k;->e:I

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/squareup/okhttp/internal/http/k;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/k;->d:Lfg;

    invoke-interface {v0, p2}, Lfg;->a(Ljava/lang/String;)Lfg;

    move-result-object v0

    const-string/jumbo v1, "\r\n"

    invoke-interface {v0, v1}, Lfg;->a(Ljava/lang/String;)Lfg;

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Lcom/squareup/okhttp/internal/http/f;->a()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/k;->d:Lfg;

    invoke-virtual {p1, v0}, Lcom/squareup/okhttp/internal/http/f;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lfg;->a(Ljava/lang/String;)Lfg;

    move-result-object v1

    const-string/jumbo v2, ": "

    invoke-interface {v1, v2}, Lfg;->a(Ljava/lang/String;)Lfg;

    move-result-object v1

    invoke-virtual {p1, v0}, Lcom/squareup/okhttp/internal/http/f;->b(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lfg;->a(Ljava/lang/String;)Lfg;

    move-result-object v1

    const-string/jumbo v2, "\r\n"

    invoke-interface {v1, v2}, Lfg;->a(Ljava/lang/String;)Lfg;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/k;->d:Lfg;

    const-string/jumbo v1, "\r\n"

    invoke-interface {v0, v1}, Lfg;->a(Ljava/lang/String;)Lfg;

    const/4 v0, 0x1

    iput v0, p0, Lcom/squareup/okhttp/internal/http/k;->e:I

    return-void
.end method

.method public a(Lcom/squareup/okhttp/internal/http/h;)V
    .locals 2

    :goto_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/k;->c:Lfh;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lfh;->a(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1, v0}, Lcom/squareup/okhttp/internal/http/h;->a(Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/h;

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(Lgb;I)Z
    .locals 3

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/k;->b:Lcom/squareup/okhttp/c;

    invoke-virtual {v0}, Lcom/squareup/okhttp/c;->c()Ljava/net/Socket;

    move-result-object v1

    :try_start_0
    invoke-virtual {v1}, Ljava/net/Socket;->getSoTimeout()I

    move-result v2

    invoke-virtual {v1, p2}, Ljava/net/Socket;->setSoTimeout(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-static {p1, p2}, Lfd;->a(Lgb;I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    :try_start_2
    invoke-virtual {v1, v2}, Ljava/net/Socket;->setSoTimeout(I)V

    :goto_0
    return v0

    :catchall_0
    move-exception v0

    invoke-virtual {v1, v2}, Ljava/net/Socket;->setSoTimeout(I)V

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    const/4 v0, 0x2

    iput v0, p0, Lcom/squareup/okhttp/internal/http/k;->f:I

    iget v0, p0, Lcom/squareup/okhttp/internal/http/k;->e:I

    if-nez v0, :cond_0

    const/4 v0, 0x6

    iput v0, p0, Lcom/squareup/okhttp/internal/http/k;->e:I

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/k;->b:Lcom/squareup/okhttp/c;

    invoke-virtual {v0}, Lcom/squareup/okhttp/c;->close()V

    :cond_0
    return-void
.end method

.method public c()Z
    .locals 2

    iget v0, p0, Lcom/squareup/okhttp/internal/http/k;->e:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/k;->d:Lfg;

    invoke-interface {v0}, Lfg;->a()V

    return-void
.end method

.method public e()Lcom/squareup/okhttp/internal/http/aj;
    .locals 6

    const/4 v5, 0x1

    iget v0, p0, Lcom/squareup/okhttp/internal/http/k;->e:I

    if-eq v0, v5, :cond_0

    iget v0, p0, Lcom/squareup/okhttp/internal/http/k;->e:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/squareup/okhttp/internal/http/k;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/k;->c:Lfh;

    invoke-interface {v0, v5}, Lfh;->a(Z)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/squareup/okhttp/internal/http/at;

    invoke-direct {v1, v0}, Lcom/squareup/okhttp/internal/http/at;-><init>(Ljava/lang/String;)V

    new-instance v0, Lcom/squareup/okhttp/internal/http/aj;

    invoke-direct {v0}, Lcom/squareup/okhttp/internal/http/aj;-><init>()V

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/internal/http/aj;->a(Lcom/squareup/okhttp/internal/http/at;)Lcom/squareup/okhttp/internal/http/aj;

    move-result-object v0

    new-instance v2, Lcom/squareup/okhttp/internal/http/h;

    invoke-direct {v2}, Lcom/squareup/okhttp/internal/http/h;-><init>()V

    invoke-virtual {p0, v2}, Lcom/squareup/okhttp/internal/http/k;->a(Lcom/squareup/okhttp/internal/http/h;)V

    sget-object v3, Lcom/squareup/okhttp/internal/http/y;->e:Ljava/lang/String;

    sget-object v4, Lcom/squareup/okhttp/Protocol;->c:Lcom/squareup/okhttp/Protocol;

    iget-object v4, v4, Lcom/squareup/okhttp/Protocol;->name:Lfi;

    invoke-virtual {v4}, Lfi;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/squareup/okhttp/internal/http/h;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/h;

    invoke-virtual {v2}, Lcom/squareup/okhttp/internal/http/h;->a()Lcom/squareup/okhttp/internal/http/f;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/okhttp/internal/http/aj;->a(Lcom/squareup/okhttp/internal/http/f;)Lcom/squareup/okhttp/internal/http/aj;

    invoke-virtual {v1}, Lcom/squareup/okhttp/internal/http/at;->c()I

    move-result v1

    const/16 v2, 0x64

    if-eq v1, v2, :cond_0

    const/4 v1, 0x4

    iput v1, p0, Lcom/squareup/okhttp/internal/http/k;->e:I

    return-object v0
.end method

.method public f()Lga;
    .locals 3

    iget v0, p0, Lcom/squareup/okhttp/internal/http/k;->e:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/squareup/okhttp/internal/http/k;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, 0x2

    iput v0, p0, Lcom/squareup/okhttp/internal/http/k;->e:I

    new-instance v0, Lcom/squareup/okhttp/internal/http/n;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/okhttp/internal/http/n;-><init>(Lcom/squareup/okhttp/internal/http/k;Lcom/squareup/okhttp/internal/http/l;)V

    return-object v0
.end method

.method public g()V
    .locals 3

    const/4 v0, 0x0

    const-wide/16 v1, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/squareup/okhttp/internal/http/k;->a(Ljava/net/CacheRequest;J)Lgb;

    return-void
.end method
