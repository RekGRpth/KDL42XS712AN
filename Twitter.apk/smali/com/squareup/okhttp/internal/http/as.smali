.class Lcom/squareup/okhttp/internal/http/as;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lgb;


# instance fields
.field private final a:Lcom/squareup/okhttp/internal/spdy/ak;

.field private final b:Lgb;

.field private final c:Ljava/net/CacheRequest;

.field private final d:Ljava/io/OutputStream;

.field private e:Z

.field private f:Z


# direct methods
.method constructor <init>(Lcom/squareup/okhttp/internal/spdy/ak;Ljava/net/CacheRequest;)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/okhttp/internal/http/as;->a:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-virtual {p1}, Lcom/squareup/okhttp/internal/spdy/ak;->e()Lgb;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/okhttp/internal/http/as;->b:Lgb;

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/net/CacheRequest;->getBody()Ljava/io/OutputStream;

    move-result-object v1

    :goto_0
    if-nez v1, :cond_0

    move-object p2, v0

    :cond_0
    iput-object v1, p0, Lcom/squareup/okhttp/internal/http/as;->d:Ljava/io/OutputStream;

    iput-object p2, p0, Lcom/squareup/okhttp/internal/http/as;->c:Ljava/net/CacheRequest;

    return-void

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method private a()Z
    .locals 5

    :try_start_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/as;->a:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/spdy/ak;->d()J

    move-result-wide v1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/as;->a:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/okhttp/internal/spdy/ak;->a(J)V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/as;->a:Lcom/squareup/okhttp/internal/spdy/ak;

    const-wide/16 v3, 0x64

    invoke-virtual {v0, v3, v4}, Lcom/squareup/okhttp/internal/spdy/ak;->a(J)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v0, 0x64

    :try_start_1
    invoke-static {p0, v0}, Lfd;->a(Lgb;I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x1

    :try_start_2
    iget-object v3, p0, Lcom/squareup/okhttp/internal/http/as;->a:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-virtual {v3, v1, v2}, Lcom/squareup/okhttp/internal/spdy/ak;->a(J)V

    :goto_0
    return v0

    :catchall_0
    move-exception v0

    iget-object v3, p0, Lcom/squareup/okhttp/internal/http/as;->a:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-virtual {v3, v1, v2}, Lcom/squareup/okhttp/internal/spdy/ak;->a(J)V

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public b(Lfo;J)J
    .locals 6

    const-wide/16 v0, -0x1

    const-wide/16 v2, 0x0

    cmp-long v2, p2, v2

    if-gez v2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "byteCount < 0: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-boolean v2, p0, Lcom/squareup/okhttp/internal/http/as;->f:Z

    if-eqz v2, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-boolean v2, p0, Lcom/squareup/okhttp/internal/http/as;->e:Z

    if-eqz v2, :cond_3

    move-wide v3, v0

    :cond_2
    :goto_0
    return-wide v3

    :cond_3
    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/as;->b:Lgb;

    invoke-interface {v2, p1, p2, p3}, Lgb;->b(Lfo;J)J

    move-result-wide v3

    cmp-long v2, v3, v0

    if-nez v2, :cond_5

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/squareup/okhttp/internal/http/as;->e:Z

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/as;->c:Ljava/net/CacheRequest;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/as;->d:Ljava/io/OutputStream;

    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    :cond_4
    move-wide v3, v0

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/as;->d:Ljava/io/OutputStream;

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lfo;->l()J

    move-result-wide v0

    sub-long v1, v0, v3

    iget-object v5, p0, Lcom/squareup/okhttp/internal/http/as;->d:Ljava/io/OutputStream;

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lfr;->a(Lfo;JJLjava/io/OutputStream;)V

    goto :goto_0
.end method

.method public close()V
    .locals 2

    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/http/as;->f:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/http/as;->e:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/as;->d:Ljava/io/OutputStream;

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/squareup/okhttp/internal/http/as;->a()Z

    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/squareup/okhttp/internal/http/as;->f:Z

    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/http/as;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/as;->a:Lcom/squareup/okhttp/internal/spdy/ak;

    sget-object v1, Lcom/squareup/okhttp/internal/spdy/ErrorCode;->l:Lcom/squareup/okhttp/internal/spdy/ErrorCode;

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/internal/spdy/ak;->b(Lcom/squareup/okhttp/internal/spdy/ErrorCode;)V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/as;->c:Ljava/net/CacheRequest;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/as;->c:Ljava/net/CacheRequest;

    invoke-virtual {v0}, Ljava/net/CacheRequest;->abort()V

    goto :goto_0
.end method
