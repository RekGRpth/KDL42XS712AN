.class final Lcom/squareup/okhttp/internal/http/p;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lga;


# instance fields
.field final synthetic a:Lcom/squareup/okhttp/internal/http/k;

.field private b:Z

.field private c:J


# direct methods
.method private constructor <init>(Lcom/squareup/okhttp/internal/http/k;J)V
    .locals 0

    iput-object p1, p0, Lcom/squareup/okhttp/internal/http/p;->a:Lcom/squareup/okhttp/internal/http/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p2, p0, Lcom/squareup/okhttp/internal/http/p;->c:J

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/okhttp/internal/http/k;JLcom/squareup/okhttp/internal/http/l;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/okhttp/internal/http/p;-><init>(Lcom/squareup/okhttp/internal/http/k;J)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/http/p;->b:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/p;->a:Lcom/squareup/okhttp/internal/http/k;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/http/k;->a(Lcom/squareup/okhttp/internal/http/k;)Lfg;

    move-result-object v0

    invoke-interface {v0}, Lfg;->a()V

    goto :goto_0
.end method

.method public a(Lfo;J)V
    .locals 6

    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/http/p;->b:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p1}, Lfo;->l()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    move-wide v4, p2

    invoke-static/range {v0 .. v5}, Lfd;->a(JJJ)V

    iget-wide v0, p0, Lcom/squareup/okhttp/internal/http/p;->c:J

    cmp-long v0, p2, v0

    if-lez v0, :cond_1

    new-instance v0, Ljava/net/ProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "expected "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/squareup/okhttp/internal/http/p;->c:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " bytes but received "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/p;->a:Lcom/squareup/okhttp/internal/http/k;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/http/k;->a(Lcom/squareup/okhttp/internal/http/k;)Lfg;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lfg;->a(Lfo;J)V

    iget-wide v0, p0, Lcom/squareup/okhttp/internal/http/p;->c:J

    sub-long/2addr v0, p2

    iput-wide v0, p0, Lcom/squareup/okhttp/internal/http/p;->c:J

    return-void
.end method

.method public close()V
    .locals 4

    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/http/p;->b:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/squareup/okhttp/internal/http/p;->b:Z

    iget-wide v0, p0, Lcom/squareup/okhttp/internal/http/p;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    new-instance v0, Ljava/net/ProtocolException;

    const-string/jumbo v1, "unexpected end of stream"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/p;->a:Lcom/squareup/okhttp/internal/http/k;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/squareup/okhttp/internal/http/k;->a(Lcom/squareup/okhttp/internal/http/k;I)I

    goto :goto_0
.end method
