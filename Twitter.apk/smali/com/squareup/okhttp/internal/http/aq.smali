.class public final Lcom/squareup/okhttp/internal/http/aq;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/squareup/okhttp/a;

.field private final b:Ljava/net/URI;

.field private final c:Ljava/net/ProxySelector;

.field private final d:Lcom/squareup/okhttp/d;

.field private final e:Lev;

.field private final f:Lcom/squareup/okhttp/n;

.field private g:Ljava/net/Proxy;

.field private h:Ljava/net/InetSocketAddress;

.field private i:Z

.field private j:Ljava/net/Proxy;

.field private k:Ljava/util/Iterator;

.field private l:[Ljava/net/InetAddress;

.field private m:I

.field private n:I

.field private o:I

.field private final p:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/squareup/okhttp/a;Ljava/net/URI;Ljava/net/ProxySelector;Lcom/squareup/okhttp/d;Lev;Lcom/squareup/okhttp/n;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/squareup/okhttp/internal/http/aq;->o:I

    iput-object p1, p0, Lcom/squareup/okhttp/internal/http/aq;->a:Lcom/squareup/okhttp/a;

    iput-object p2, p0, Lcom/squareup/okhttp/internal/http/aq;->b:Ljava/net/URI;

    iput-object p3, p0, Lcom/squareup/okhttp/internal/http/aq;->c:Ljava/net/ProxySelector;

    iput-object p4, p0, Lcom/squareup/okhttp/internal/http/aq;->d:Lcom/squareup/okhttp/d;

    iput-object p5, p0, Lcom/squareup/okhttp/internal/http/aq;->e:Lev;

    iput-object p6, p0, Lcom/squareup/okhttp/internal/http/aq;->f:Lcom/squareup/okhttp/n;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/aq;->p:Ljava/util/List;

    invoke-virtual {p1}, Lcom/squareup/okhttp/a;->d()Ljava/net/Proxy;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lcom/squareup/okhttp/internal/http/aq;->a(Ljava/net/URI;Ljava/net/Proxy;)V

    return-void
.end method

.method private a(Ljava/net/Proxy;)V
    .locals 4

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/aq;->l:[Ljava/net/InetAddress;

    invoke-virtual {p1}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v0

    sget-object v1, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/aq;->b:Ljava/net/URI;

    invoke-virtual {v0}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/aq;->b:Ljava/net/URI;

    invoke-static {v1}, Lfd;->a(Ljava/net/URI;)I

    move-result v1

    iput v1, p0, Lcom/squareup/okhttp/internal/http/aq;->n:I

    :goto_0
    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/aq;->e:Lev;

    invoke-interface {v1, v0}, Lev;->a(Ljava/lang/String;)[Ljava/net/InetAddress;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/aq;->l:[Ljava/net/InetAddress;

    const/4 v0, 0x0

    iput v0, p0, Lcom/squareup/okhttp/internal/http/aq;->m:I

    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/net/Proxy;->address()Ljava/net/SocketAddress;

    move-result-object v0

    instance-of v1, v0, Ljava/net/InetSocketAddress;

    if-nez v1, :cond_1

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Proxy.address() is not an InetSocketAddress: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    check-cast v0, Ljava/net/InetSocketAddress;

    invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getHostName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getPort()I

    move-result v0

    iput v0, p0, Lcom/squareup/okhttp/internal/http/aq;->n:I

    move-object v0, v1

    goto :goto_0
.end method

.method private a(Ljava/net/URI;Ljava/net/Proxy;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/squareup/okhttp/internal/http/aq;->i:Z

    if-eqz p2, :cond_1

    iput-object p2, p0, Lcom/squareup/okhttp/internal/http/aq;->j:Ljava/net/Proxy;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/aq;->c:Ljava/net/ProxySelector;

    invoke-virtual {v0, p1}, Ljava/net/ProxySelector;->select(Ljava/net/URI;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/aq;->k:Ljava/util/Iterator;

    goto :goto_0
.end method

.method private c()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/http/aq;->i:Z

    return v0
.end method

.method private d()Ljava/net/Proxy;
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/aq;->j:Ljava/net/Proxy;

    if-eqz v0, :cond_0

    iput-boolean v3, p0, Lcom/squareup/okhttp/internal/http/aq;->i:Z

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/aq;->j:Ljava/net/Proxy;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/aq;->k:Ljava/util/Iterator;

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/aq;->k:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/aq;->k:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/Proxy;

    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v1

    sget-object v2, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-eq v1, v2, :cond_1

    goto :goto_0

    :cond_2
    iput-boolean v3, p0, Lcom/squareup/okhttp/internal/http/aq;->i:Z

    sget-object v0, Ljava/net/Proxy;->NO_PROXY:Ljava/net/Proxy;

    goto :goto_0
.end method

.method private e()Z
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/aq;->l:[Ljava/net/InetAddress;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()Ljava/net/InetSocketAddress;
    .locals 4

    new-instance v0, Ljava/net/InetSocketAddress;

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/aq;->l:[Ljava/net/InetAddress;

    iget v2, p0, Lcom/squareup/okhttp/internal/http/aq;->m:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/squareup/okhttp/internal/http/aq;->m:I

    aget-object v1, v1, v2

    iget v2, p0, Lcom/squareup/okhttp/internal/http/aq;->n:I

    invoke-direct {v0, v1, v2}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    iget v1, p0, Lcom/squareup/okhttp/internal/http/aq;->m:I

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/aq;->l:[Ljava/net/InetAddress;

    array-length v2, v2

    if-ne v1, v2, :cond_0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/squareup/okhttp/internal/http/aq;->l:[Ljava/net/InetAddress;

    const/4 v1, 0x0

    iput v1, p0, Lcom/squareup/okhttp/internal/http/aq;->m:I

    :cond_0
    return-object v0
.end method

.method private g()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/aq;->a:Lcom/squareup/okhttp/a;

    invoke-virtual {v0}, Lcom/squareup/okhttp/a;->b()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput v0, p0, Lcom/squareup/okhttp/internal/http/aq;->o:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h()Z
    .locals 2

    iget v0, p0, Lcom/squareup/okhttp/internal/http/aq;->o:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i()I
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget v2, p0, Lcom/squareup/okhttp/internal/http/aq;->o:I

    if-ne v2, v0, :cond_0

    iput v1, p0, Lcom/squareup/okhttp/internal/http/aq;->o:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/squareup/okhttp/internal/http/aq;->o:I

    if-nez v0, :cond_1

    const/4 v0, -0x1

    iput v0, p0, Lcom/squareup/okhttp/internal/http/aq;->o:I

    move v0, v1

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method private j()Z
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/aq;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private k()Lcom/squareup/okhttp/m;
    .locals 2

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/aq;->p:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/okhttp/m;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/squareup/okhttp/a;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/aq;->a:Lcom/squareup/okhttp/a;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lcom/squareup/okhttp/c;
    .locals 5

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/aq;->d:Lcom/squareup/okhttp/d;

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/aq;->a:Lcom/squareup/okhttp/a;

    invoke-virtual {v1, v2}, Lcom/squareup/okhttp/d;->a(Lcom/squareup/okhttp/a;)Lcom/squareup/okhttp/c;

    move-result-object v1

    if-eqz v1, :cond_2

    const-string/jumbo v2, "GET"

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lcom/squareup/okhttp/c;->e()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move-object v0, v1

    :goto_1
    return-object v0

    :cond_1
    invoke-virtual {v1}, Lcom/squareup/okhttp/c;->close()V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/squareup/okhttp/internal/http/aq;->h()Z

    move-result v1

    if-nez v1, :cond_6

    invoke-direct {p0}, Lcom/squareup/okhttp/internal/http/aq;->e()Z

    move-result v1

    if-nez v1, :cond_5

    invoke-direct {p0}, Lcom/squareup/okhttp/internal/http/aq;->c()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-direct {p0}, Lcom/squareup/okhttp/internal/http/aq;->j()Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    :cond_3
    new-instance v0, Lcom/squareup/okhttp/c;

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/aq;->d:Lcom/squareup/okhttp/d;

    invoke-direct {p0}, Lcom/squareup/okhttp/internal/http/aq;->k()Lcom/squareup/okhttp/m;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/squareup/okhttp/c;-><init>(Lcom/squareup/okhttp/d;Lcom/squareup/okhttp/m;)V

    goto :goto_1

    :cond_4
    invoke-direct {p0}, Lcom/squareup/okhttp/internal/http/aq;->d()Ljava/net/Proxy;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/okhttp/internal/http/aq;->g:Ljava/net/Proxy;

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/aq;->g:Ljava/net/Proxy;

    invoke-direct {p0, v1}, Lcom/squareup/okhttp/internal/http/aq;->a(Ljava/net/Proxy;)V

    :cond_5
    invoke-direct {p0}, Lcom/squareup/okhttp/internal/http/aq;->f()Ljava/net/InetSocketAddress;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/okhttp/internal/http/aq;->h:Ljava/net/InetSocketAddress;

    invoke-direct {p0}, Lcom/squareup/okhttp/internal/http/aq;->g()V

    :cond_6
    invoke-direct {p0}, Lcom/squareup/okhttp/internal/http/aq;->i()I

    move-result v1

    if-ne v1, v0, :cond_7

    :goto_2
    new-instance v1, Lcom/squareup/okhttp/m;

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/aq;->a:Lcom/squareup/okhttp/a;

    iget-object v3, p0, Lcom/squareup/okhttp/internal/http/aq;->g:Ljava/net/Proxy;

    iget-object v4, p0, Lcom/squareup/okhttp/internal/http/aq;->h:Ljava/net/InetSocketAddress;

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/squareup/okhttp/m;-><init>(Lcom/squareup/okhttp/a;Ljava/net/Proxy;Ljava/net/InetSocketAddress;Z)V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/aq;->f:Lcom/squareup/okhttp/n;

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/n;->c(Lcom/squareup/okhttp/m;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/aq;->p:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, p1}, Lcom/squareup/okhttp/internal/http/aq;->a(Ljava/lang/String;)Lcom/squareup/okhttp/c;

    move-result-object v0

    goto :goto_1

    :cond_7
    const/4 v0, 0x0

    goto :goto_2

    :cond_8
    new-instance v0, Lcom/squareup/okhttp/c;

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/aq;->d:Lcom/squareup/okhttp/d;

    invoke-direct {v0, v2, v1}, Lcom/squareup/okhttp/c;-><init>(Lcom/squareup/okhttp/d;Lcom/squareup/okhttp/m;)V

    goto :goto_1
.end method

.method public a(Lcom/squareup/okhttp/c;Ljava/io/IOException;)V
    .locals 5

    const/4 v0, 0x1

    invoke-virtual {p1}, Lcom/squareup/okhttp/c;->n()I

    move-result v1

    if-lez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/squareup/okhttp/c;->b()Lcom/squareup/okhttp/m;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/okhttp/m;->b()Ljava/net/Proxy;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v2

    sget-object v3, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-eq v2, v3, :cond_2

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/aq;->c:Ljava/net/ProxySelector;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/aq;->c:Ljava/net/ProxySelector;

    iget-object v3, p0, Lcom/squareup/okhttp/internal/http/aq;->b:Ljava/net/URI;

    invoke-virtual {v1}, Lcom/squareup/okhttp/m;->b()Ljava/net/Proxy;

    move-result-object v4

    invoke-virtual {v4}, Ljava/net/Proxy;->address()Ljava/net/SocketAddress;

    move-result-object v4

    invoke-virtual {v2, v3, v4, p2}, Ljava/net/ProxySelector;->connectFailed(Ljava/net/URI;Ljava/net/SocketAddress;Ljava/io/IOException;)V

    :cond_2
    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/aq;->f:Lcom/squareup/okhttp/n;

    invoke-virtual {v2, v1}, Lcom/squareup/okhttp/n;->a(Lcom/squareup/okhttp/m;)V

    invoke-direct {p0}, Lcom/squareup/okhttp/internal/http/aq;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    instance-of v1, p2, Ljavax/net/ssl/SSLHandshakeException;

    if-nez v1, :cond_0

    instance-of v1, p2, Ljavax/net/ssl/SSLProtocolException;

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/squareup/okhttp/internal/http/aq;->i()I

    move-result v1

    if-ne v1, v0, :cond_3

    :goto_1
    new-instance v1, Lcom/squareup/okhttp/m;

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/aq;->a:Lcom/squareup/okhttp/a;

    iget-object v3, p0, Lcom/squareup/okhttp/internal/http/aq;->g:Ljava/net/Proxy;

    iget-object v4, p0, Lcom/squareup/okhttp/internal/http/aq;->h:Ljava/net/InetSocketAddress;

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/squareup/okhttp/m;-><init>(Lcom/squareup/okhttp/a;Ljava/net/Proxy;Ljava/net/InetSocketAddress;Z)V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/aq;->f:Lcom/squareup/okhttp/n;

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/n;->a(Lcom/squareup/okhttp/m;)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public b()Z
    .locals 1

    invoke-direct {p0}, Lcom/squareup/okhttp/internal/http/aq;->h()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/okhttp/internal/http/aq;->e()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/okhttp/internal/http/aq;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/okhttp/internal/http/aq;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
