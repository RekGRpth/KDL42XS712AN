.class public final Lcom/squareup/okhttp/internal/http/aa;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Ljava/net/URL;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/squareup/okhttp/internal/http/f;

.field private final d:Lcom/squareup/okhttp/internal/http/ac;

.field private final e:Ljava/lang/Object;

.field private volatile f:Lcom/squareup/okhttp/internal/http/ae;

.field private volatile g:Ljava/net/URI;

.field private volatile h:Lcom/squareup/okhttp/b;


# direct methods
.method private constructor <init>(Lcom/squareup/okhttp/internal/http/ad;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/squareup/okhttp/internal/http/ad;->a(Lcom/squareup/okhttp/internal/http/ad;)Ljava/net/URL;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/aa;->a:Ljava/net/URL;

    invoke-static {p1}, Lcom/squareup/okhttp/internal/http/ad;->b(Lcom/squareup/okhttp/internal/http/ad;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/aa;->b:Ljava/lang/String;

    invoke-static {p1}, Lcom/squareup/okhttp/internal/http/ad;->c(Lcom/squareup/okhttp/internal/http/ad;)Lcom/squareup/okhttp/internal/http/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/h;->a()Lcom/squareup/okhttp/internal/http/f;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/aa;->c:Lcom/squareup/okhttp/internal/http/f;

    invoke-static {p1}, Lcom/squareup/okhttp/internal/http/ad;->d(Lcom/squareup/okhttp/internal/http/ad;)Lcom/squareup/okhttp/internal/http/ac;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/aa;->d:Lcom/squareup/okhttp/internal/http/ac;

    invoke-static {p1}, Lcom/squareup/okhttp/internal/http/ad;->e(Lcom/squareup/okhttp/internal/http/ad;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/squareup/okhttp/internal/http/ad;->e(Lcom/squareup/okhttp/internal/http/ad;)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/aa;->e:Ljava/lang/Object;

    return-void

    :cond_0
    move-object v0, p0

    goto :goto_0
.end method

.method synthetic constructor <init>(Lcom/squareup/okhttp/internal/http/ad;Lcom/squareup/okhttp/internal/http/ab;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/okhttp/internal/http/aa;-><init>(Lcom/squareup/okhttp/internal/http/ad;)V

    return-void
.end method

.method static synthetic a(Lcom/squareup/okhttp/internal/http/aa;)Ljava/net/URL;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/aa;->a:Ljava/net/URL;

    return-object v0
.end method

.method static synthetic b(Lcom/squareup/okhttp/internal/http/aa;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/aa;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/squareup/okhttp/internal/http/aa;)Lcom/squareup/okhttp/internal/http/ac;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/aa;->d:Lcom/squareup/okhttp/internal/http/ac;

    return-object v0
.end method

.method static synthetic d(Lcom/squareup/okhttp/internal/http/aa;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/aa;->e:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic e(Lcom/squareup/okhttp/internal/http/aa;)Lcom/squareup/okhttp/internal/http/f;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/aa;->c:Lcom/squareup/okhttp/internal/http/f;

    return-object v0
.end method

.method private l()Lcom/squareup/okhttp/internal/http/ae;
    .locals 2

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/aa;->f:Lcom/squareup/okhttp/internal/http/ae;

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/squareup/okhttp/internal/http/ae;

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/aa;->c:Lcom/squareup/okhttp/internal/http/f;

    invoke-direct {v0, v1}, Lcom/squareup/okhttp/internal/http/ae;-><init>(Lcom/squareup/okhttp/internal/http/f;)V

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/aa;->f:Lcom/squareup/okhttp/internal/http/ae;

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/aa;->c:Lcom/squareup/okhttp/internal/http/f;

    invoke-virtual {v0, p1}, Lcom/squareup/okhttp/internal/http/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/net/URL;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/aa;->a:Ljava/net/URL;

    return-object v0
.end method

.method public b()Ljava/net/URI;
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/aa;->g:Ljava/net/URI;

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ley;->a()Ley;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/aa;->a:Ljava/net/URL;

    invoke-virtual {v0, v1}, Ley;->a(Ljava/net/URL;)Ljava/net/URI;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/aa;->g:Ljava/net/URI;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    invoke-virtual {v0}, Ljava/net/URISyntaxException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/aa;->b:Ljava/lang/String;

    return-object v0
.end method

.method public d()Lcom/squareup/okhttp/internal/http/f;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/aa;->c:Lcom/squareup/okhttp/internal/http/f;

    return-object v0
.end method

.method public e()Lcom/squareup/okhttp/internal/http/ac;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/aa;->d:Lcom/squareup/okhttp/internal/http/ac;

    return-object v0
.end method

.method public f()Lcom/squareup/okhttp/internal/http/ad;
    .locals 2

    new-instance v0, Lcom/squareup/okhttp/internal/http/ad;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/okhttp/internal/http/ad;-><init>(Lcom/squareup/okhttp/internal/http/aa;Lcom/squareup/okhttp/internal/http/ab;)V

    return-object v0
.end method

.method public g()Lcom/squareup/okhttp/internal/http/f;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/aa;->c:Lcom/squareup/okhttp/internal/http/f;

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/squareup/okhttp/internal/http/aa;->l()Lcom/squareup/okhttp/internal/http/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/okhttp/internal/http/ae;->a(Lcom/squareup/okhttp/internal/http/ae;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/squareup/okhttp/internal/http/aa;->l()Lcom/squareup/okhttp/internal/http/ae;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/okhttp/internal/http/ae;->b(Lcom/squareup/okhttp/internal/http/ae;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public j()Lcom/squareup/okhttp/b;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/aa;->h:Lcom/squareup/okhttp/b;

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/aa;->c:Lcom/squareup/okhttp/internal/http/f;

    invoke-static {v0}, Lcom/squareup/okhttp/b;->a(Lcom/squareup/okhttp/internal/http/f;)Lcom/squareup/okhttp/b;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/aa;->h:Lcom/squareup/okhttp/b;

    goto :goto_0
.end method

.method public k()Z
    .locals 2

    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/http/aa;->a()Ljava/net/URL;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "https"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
