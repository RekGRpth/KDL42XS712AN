.class Lcom/squareup/okhttp/internal/http/q;
.super Lcom/squareup/okhttp/internal/http/m;
.source "Twttr"

# interfaces
.implements Lgb;


# instance fields
.field final synthetic d:Lcom/squareup/okhttp/internal/http/k;

.field private e:J


# direct methods
.method public constructor <init>(Lcom/squareup/okhttp/internal/http/k;Ljava/net/CacheRequest;J)V
    .locals 4

    iput-object p1, p0, Lcom/squareup/okhttp/internal/http/q;->d:Lcom/squareup/okhttp/internal/http/k;

    invoke-direct {p0, p1, p2}, Lcom/squareup/okhttp/internal/http/m;-><init>(Lcom/squareup/okhttp/internal/http/k;Ljava/net/CacheRequest;)V

    iput-wide p3, p0, Lcom/squareup/okhttp/internal/http/q;->e:J

    iget-wide v0, p0, Lcom/squareup/okhttp/internal/http/q;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/squareup/okhttp/internal/http/q;->a(Z)V

    :cond_0
    return-void
.end method


# virtual methods
.method public b(Lfo;J)J
    .locals 7

    const-wide/16 v0, -0x1

    const-wide/16 v5, 0x0

    cmp-long v2, p2, v5

    if-gez v2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "byteCount < 0: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-boolean v2, p0, Lcom/squareup/okhttp/internal/http/q;->b:Z

    if-eqz v2, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-wide v2, p0, Lcom/squareup/okhttp/internal/http/q;->e:J

    cmp-long v2, v2, v5

    if-nez v2, :cond_2

    :goto_0
    return-wide v0

    :cond_2
    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/q;->d:Lcom/squareup/okhttp/internal/http/k;

    invoke-static {v2}, Lcom/squareup/okhttp/internal/http/k;->f(Lcom/squareup/okhttp/internal/http/k;)Lfh;

    move-result-object v2

    iget-wide v3, p0, Lcom/squareup/okhttp/internal/http/q;->e:J

    invoke-static {v3, v4, p2, p3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v3

    invoke-interface {v2, p1, v3, v4}, Lfh;->b(Lfo;J)J

    move-result-wide v2

    cmp-long v0, v2, v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/http/q;->a()V

    new-instance v0, Ljava/net/ProtocolException;

    const-string/jumbo v1, "unexpected end of stream"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget-wide v0, p0, Lcom/squareup/okhttp/internal/http/q;->e:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/squareup/okhttp/internal/http/q;->e:J

    invoke-virtual {p0, p1, v2, v3}, Lcom/squareup/okhttp/internal/http/q;->a(Lfo;J)V

    iget-wide v0, p0, Lcom/squareup/okhttp/internal/http/q;->e:J

    cmp-long v0, v0, v5

    if-nez v0, :cond_4

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/squareup/okhttp/internal/http/q;->a(Z)V

    :cond_4
    move-wide v0, v2

    goto :goto_0
.end method

.method public close()V
    .locals 4

    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/http/q;->b:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-wide v0, p0, Lcom/squareup/okhttp/internal/http/q;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/q;->d:Lcom/squareup/okhttp/internal/http/k;

    const/16 v1, 0x64

    invoke-virtual {v0, p0, v1}, Lcom/squareup/okhttp/internal/http/k;->a(Lgb;I)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/http/q;->a()V

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/squareup/okhttp/internal/http/q;->b:Z

    goto :goto_0
.end method
