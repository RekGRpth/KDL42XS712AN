.class public Lcom/squareup/okhttp/internal/http/al;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/squareup/okhttp/l;


# instance fields
.field private final a:Ljava/net/ResponseCache;


# direct methods
.method public constructor <init>(Ljava/net/ResponseCache;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/okhttp/internal/http/al;->a:Ljava/net/ResponseCache;

    return-void
.end method

.method private static a(Lcom/squareup/okhttp/internal/http/f;Ljava/io/InputStream;)Lcom/squareup/okhttp/internal/http/ai;
    .locals 1

    new-instance v0, Lcom/squareup/okhttp/internal/http/am;

    invoke-direct {v0, p0, p1}, Lcom/squareup/okhttp/internal/http/am;-><init>(Lcom/squareup/okhttp/internal/http/f;Ljava/io/InputStream;)V

    return-object v0
.end method

.method private static a(Ljava/net/CacheResponse;)Lcom/squareup/okhttp/internal/http/f;
    .locals 5

    invoke-virtual {p0}, Ljava/net/CacheResponse;->getHeaders()Ljava/util/Map;

    move-result-object v0

    new-instance v2, Lcom/squareup/okhttp/internal/http/h;

    invoke-direct {v2}, Lcom/squareup/okhttp/internal/http/h;-><init>()V

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Lcom/squareup/okhttp/internal/http/h;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/h;

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Lcom/squareup/okhttp/internal/http/h;->a()Lcom/squareup/okhttp/internal/http/f;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b()Ljava/lang/RuntimeException;
    .locals 1

    invoke-static {}, Lcom/squareup/okhttp/internal/http/al;->f()Ljava/lang/RuntimeException;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/net/CacheResponse;)Ljava/lang/String;
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Ljava/net/CacheResponse;->getHeaders()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move-object v0, v1

    :goto_0
    return-object v0

    :cond_1
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method private static b(Lcom/squareup/okhttp/internal/http/ag;)Ljava/net/HttpURLConnection;
    .locals 2

    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/http/ag;->a()Lcom/squareup/okhttp/internal/http/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/aa;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/okhttp/internal/http/ao;

    new-instance v1, Lcom/squareup/okhttp/internal/http/an;

    invoke-direct {v1, p0}, Lcom/squareup/okhttp/internal/http/an;-><init>(Lcom/squareup/okhttp/internal/http/ag;)V

    invoke-direct {v0, v1}, Lcom/squareup/okhttp/internal/http/ao;-><init>(Lcom/squareup/okhttp/internal/http/an;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/squareup/okhttp/internal/http/an;

    invoke-direct {v0, p0}, Lcom/squareup/okhttp/internal/http/an;-><init>(Lcom/squareup/okhttp/internal/http/ag;)V

    goto :goto_0
.end method

.method static synthetic c()Ljava/lang/RuntimeException;
    .locals 1

    invoke-static {}, Lcom/squareup/okhttp/internal/http/al;->g()Ljava/lang/RuntimeException;

    move-result-object v0

    return-object v0
.end method

.method private c(Lcom/squareup/okhttp/internal/http/aa;)Ljava/net/CacheResponse;
    .locals 4

    invoke-static {p1}, Lcom/squareup/okhttp/internal/http/al;->d(Lcom/squareup/okhttp/internal/http/aa;)Ljava/util/Map;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/al;->a:Ljava/net/ResponseCache;

    invoke-virtual {p1}, Lcom/squareup/okhttp/internal/http/aa;->b()Ljava/net/URI;

    move-result-object v2

    invoke-virtual {p1}, Lcom/squareup/okhttp/internal/http/aa;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, Ljava/net/ResponseCache;->get(Ljava/net/URI;Ljava/lang/String;Ljava/util/Map;)Ljava/net/CacheResponse;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d()Ljava/lang/RuntimeException;
    .locals 1

    invoke-static {}, Lcom/squareup/okhttp/internal/http/al;->i()Ljava/lang/RuntimeException;

    move-result-object v0

    return-object v0
.end method

.method private static d(Lcom/squareup/okhttp/internal/http/aa;)Ljava/util/Map;
    .locals 2

    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/http/aa;->d()Lcom/squareup/okhttp/internal/http/f;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/okhttp/internal/http/y;->a(Lcom/squareup/okhttp/internal/http/f;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e()Ljava/lang/RuntimeException;
    .locals 1

    invoke-static {}, Lcom/squareup/okhttp/internal/http/al;->h()Ljava/lang/RuntimeException;

    move-result-object v0

    return-object v0
.end method

.method private static f()Ljava/lang/RuntimeException;
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "ResponseCache cannot modify the request."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static g()Ljava/lang/RuntimeException;
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "ResponseCache cannot access request headers"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static h()Ljava/lang/RuntimeException;
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "ResponseCache cannot access SSL internals"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static i()Ljava/lang/RuntimeException;
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string/jumbo v1, "ResponseCache cannot access the response body."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public a(Lcom/squareup/okhttp/internal/http/aa;)Lcom/squareup/okhttp/internal/http/ag;
    .locals 4

    invoke-direct {p0, p1}, Lcom/squareup/okhttp/internal/http/al;->c(Lcom/squareup/okhttp/internal/http/aa;)Ljava/net/CacheResponse;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v3, Lcom/squareup/okhttp/internal/http/aj;

    invoke-direct {v3}, Lcom/squareup/okhttp/internal/http/aj;-><init>()V

    invoke-virtual {v3, p1}, Lcom/squareup/okhttp/internal/http/aj;->a(Lcom/squareup/okhttp/internal/http/aa;)Lcom/squareup/okhttp/internal/http/aj;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/http/al;->b(Ljava/net/CacheResponse;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/squareup/okhttp/internal/http/aj;->a(Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/aj;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/http/al;->a(Ljava/net/CacheResponse;)Lcom/squareup/okhttp/internal/http/f;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/squareup/okhttp/internal/http/aj;->a(Lcom/squareup/okhttp/internal/http/f;)Lcom/squareup/okhttp/internal/http/aj;

    sget-object v2, Lcom/squareup/okhttp/ResponseSource;->a:Lcom/squareup/okhttp/ResponseSource;

    invoke-virtual {v3, v2}, Lcom/squareup/okhttp/internal/http/aj;->a(Lcom/squareup/okhttp/ResponseSource;)Lcom/squareup/okhttp/internal/http/aj;

    invoke-virtual {v0}, Ljava/net/CacheResponse;->getBody()Ljava/io/InputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/squareup/okhttp/internal/http/al;->a(Lcom/squareup/okhttp/internal/http/f;Ljava/io/InputStream;)Lcom/squareup/okhttp/internal/http/ai;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/squareup/okhttp/internal/http/aj;->a(Lcom/squareup/okhttp/internal/http/ai;)Lcom/squareup/okhttp/internal/http/aj;

    instance-of v1, v0, Ljava/net/SecureCacheResponse;

    if-eqz v1, :cond_2

    check-cast v0, Ljava/net/SecureCacheResponse;

    :try_start_0
    invoke-virtual {v0}, Ljava/net/SecureCacheResponse;->getServerCertificateChain()Ljava/util/List;
    :try_end_0
    .catch Ljavax/net/ssl/SSLPeerUnverifiedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_1
    invoke-virtual {v0}, Ljava/net/SecureCacheResponse;->getLocalCertificateChain()Ljava/util/List;

    move-result-object v2

    if-nez v2, :cond_1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    :cond_1
    invoke-virtual {v0}, Ljava/net/SecureCacheResponse;->getCipherSuite()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v1, v2}, Lcom/squareup/okhttp/f;->a(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Lcom/squareup/okhttp/f;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/squareup/okhttp/internal/http/aj;->a(Lcom/squareup/okhttp/f;)Lcom/squareup/okhttp/internal/http/aj;

    :cond_2
    invoke-virtual {v3}, Lcom/squareup/okhttp/internal/http/aj;->a()Lcom/squareup/okhttp/internal/http/ag;

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    goto :goto_1
.end method

.method public a(Lcom/squareup/okhttp/internal/http/ag;)Ljava/net/CacheRequest;
    .locals 3

    invoke-virtual {p1}, Lcom/squareup/okhttp/internal/http/ag;->a()Lcom/squareup/okhttp/internal/http/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/aa;->b()Ljava/net/URI;

    move-result-object v0

    invoke-static {p1}, Lcom/squareup/okhttp/internal/http/al;->b(Lcom/squareup/okhttp/internal/http/ag;)Ljava/net/HttpURLConnection;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/al;->a:Ljava/net/ResponseCache;

    invoke-virtual {v2, v0, v1}, Ljava/net/ResponseCache;->put(Ljava/net/URI;Ljava/net/URLConnection;)Ljava/net/CacheRequest;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 0

    return-void
.end method

.method public a(Lcom/squareup/okhttp/ResponseSource;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/squareup/okhttp/internal/http/ag;Lcom/squareup/okhttp/internal/http/ag;)V
    .locals 0

    return-void
.end method

.method public b(Lcom/squareup/okhttp/internal/http/aa;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
