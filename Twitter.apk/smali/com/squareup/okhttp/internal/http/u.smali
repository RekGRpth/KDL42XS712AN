.class public Lcom/squareup/okhttp/internal/http/u;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field final a:Lcom/squareup/okhttp/j;

.field b:J

.field public final c:Z

.field private d:Lcom/squareup/okhttp/c;

.field private e:Lcom/squareup/okhttp/internal/http/aq;

.field private f:Lcom/squareup/okhttp/m;

.field private g:Lcom/squareup/okhttp/internal/http/au;

.field private h:Z

.field private i:Lcom/squareup/okhttp/internal/http/aa;

.field private j:Lcom/squareup/okhttp/internal/http/aa;

.field private k:Lga;

.field private l:Lfg;

.field private m:Lcom/squareup/okhttp/ResponseSource;

.field private n:Lcom/squareup/okhttp/internal/http/ag;

.field private o:Lgb;

.field private p:Lgb;

.field private q:Ljava/io/InputStream;

.field private r:Lcom/squareup/okhttp/internal/http/ag;

.field private s:Ljava/net/CacheRequest;


# direct methods
.method public constructor <init>(Lcom/squareup/okhttp/j;Lcom/squareup/okhttp/internal/http/aa;ZLcom/squareup/okhttp/c;Lcom/squareup/okhttp/internal/http/aq;Lcom/squareup/okhttp/internal/http/ap;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/squareup/okhttp/internal/http/u;->b:J

    iput-object p1, p0, Lcom/squareup/okhttp/internal/http/u;->a:Lcom/squareup/okhttp/j;

    iput-object p2, p0, Lcom/squareup/okhttp/internal/http/u;->i:Lcom/squareup/okhttp/internal/http/aa;

    iput-object p2, p0, Lcom/squareup/okhttp/internal/http/u;->j:Lcom/squareup/okhttp/internal/http/aa;

    iput-boolean p3, p0, Lcom/squareup/okhttp/internal/http/u;->c:Z

    iput-object p4, p0, Lcom/squareup/okhttp/internal/http/u;->d:Lcom/squareup/okhttp/c;

    iput-object p5, p0, Lcom/squareup/okhttp/internal/http/u;->e:Lcom/squareup/okhttp/internal/http/aq;

    if-eqz p4, :cond_0

    invoke-virtual {p4}, Lcom/squareup/okhttp/c;->b()Lcom/squareup/okhttp/m;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->f:Lcom/squareup/okhttp/m;

    iput-object p6, p0, Lcom/squareup/okhttp/internal/http/u;->k:Lga;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/squareup/okhttp/internal/http/aq;)Lcom/squareup/okhttp/c;
    .locals 4

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->j:Lcom/squareup/okhttp/internal/http/aa;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/aa;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/squareup/okhttp/internal/http/aq;->a(Ljava/lang/String;)Lcom/squareup/okhttp/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/c;->a()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->a:Lcom/squareup/okhttp/j;

    invoke-virtual {v1}, Lcom/squareup/okhttp/j;->a()I

    move-result v1

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/u;->a:Lcom/squareup/okhttp/j;

    invoke-virtual {v2}, Lcom/squareup/okhttp/j;->b()I

    move-result v2

    invoke-direct {p0}, Lcom/squareup/okhttp/internal/http/u;->v()Lcom/squareup/okhttp/o;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/okhttp/c;->a(IILcom/squareup/okhttp/o;)V

    invoke-virtual {v0}, Lcom/squareup/okhttp/c;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->a:Lcom/squareup/okhttp/j;

    invoke-virtual {v1}, Lcom/squareup/okhttp/j;->j()Lcom/squareup/okhttp/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/okhttp/d;->b(Lcom/squareup/okhttp/c;)V

    :cond_0
    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->a:Lcom/squareup/okhttp/j;

    invoke-virtual {v1}, Lcom/squareup/okhttp/j;->l()Lcom/squareup/okhttp/n;

    move-result-object v1

    invoke-virtual {v0}, Lcom/squareup/okhttp/c;->b()Lcom/squareup/okhttp/m;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/squareup/okhttp/n;->b(Lcom/squareup/okhttp/m;)V

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    invoke-virtual {v0}, Lcom/squareup/okhttp/c;->j()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->a:Lcom/squareup/okhttp/j;

    invoke-virtual {v1}, Lcom/squareup/okhttp/j;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/c;->b(I)V

    goto :goto_0
.end method

.method private static a(Lcom/squareup/okhttp/internal/http/ag;Lcom/squareup/okhttp/internal/http/ag;)Lcom/squareup/okhttp/internal/http/ag;
    .locals 7

    const/4 v1, 0x0

    new-instance v2, Lcom/squareup/okhttp/internal/http/h;

    invoke-direct {v2}, Lcom/squareup/okhttp/internal/http/h;-><init>()V

    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/http/ag;->g()Lcom/squareup/okhttp/internal/http/f;

    move-result-object v3

    move v0, v1

    :goto_0
    invoke-virtual {v3}, Lcom/squareup/okhttp/internal/http/f;->a()I

    move-result v4

    if-ge v0, v4, :cond_3

    invoke-virtual {v3, v0}, Lcom/squareup/okhttp/internal/http/f;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0}, Lcom/squareup/okhttp/internal/http/f;->b(I)Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "Warning"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    const-string/jumbo v6, "1"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-static {v4}, Lcom/squareup/okhttp/internal/http/u;->a(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {p1, v4}, Lcom/squareup/okhttp/internal/http/ag;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-nez v6, :cond_0

    :cond_2
    invoke-virtual {v2, v4, v5}, Lcom/squareup/okhttp/internal/http/h;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/h;

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/squareup/okhttp/internal/http/ag;->g()Lcom/squareup/okhttp/internal/http/f;

    move-result-object v0

    :goto_2
    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/f;->a()I

    move-result v3

    if-ge v1, v3, :cond_5

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/internal/http/f;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/squareup/okhttp/internal/http/u;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/internal/http/f;->b(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/squareup/okhttp/internal/http/h;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/h;

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_5
    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/http/ag;->i()Lcom/squareup/okhttp/internal/http/aj;

    move-result-object v0

    invoke-virtual {v2}, Lcom/squareup/okhttp/internal/http/h;->a()Lcom/squareup/okhttp/internal/http/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/internal/http/aj;->a(Lcom/squareup/okhttp/internal/http/f;)Lcom/squareup/okhttp/internal/http/aj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/aj;->a()Lcom/squareup/okhttp/internal/http/ag;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/net/URL;)Ljava/lang/String;
    .locals 2

    invoke-static {p0}, Lfd;->a(Ljava/net/URL;)I

    move-result v0

    invoke-virtual {p0}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lfd;->a(Ljava/lang/String;)I

    move-result v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/net/URL;->getPort()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lgb;)V
    .locals 3

    iput-object p1, p0, Lcom/squareup/okhttp/internal/http/u;->o:Lgb;

    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/http/u;->h:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "gzip"

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->n:Lcom/squareup/okhttp/internal/http/ag;

    const-string/jumbo v2, "Content-Encoding"

    invoke-virtual {v1, v2}, Lcom/squareup/okhttp/internal/http/ag;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->n:Lcom/squareup/okhttp/internal/http/ag;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/ag;->i()Lcom/squareup/okhttp/internal/http/aj;

    move-result-object v0

    const-string/jumbo v1, "Content-Encoding"

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/internal/http/aj;->b(Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/aj;

    move-result-object v0

    const-string/jumbo v1, "Content-Length"

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/internal/http/aj;->b(Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/aj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/aj;->a()Lcom/squareup/okhttp/internal/http/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->n:Lcom/squareup/okhttp/internal/http/ag;

    new-instance v0, Lfm;

    invoke-direct {v0, p1}, Lfm;-><init>(Lgb;)V

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->p:Lgb;

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/squareup/okhttp/internal/http/u;->p:Lgb;

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 1

    const-string/jumbo v0, "Connection"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "Keep-Alive"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "Proxy-Authenticate"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "Proxy-Authorization"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "TE"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "Trailers"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "Transfer-Encoding"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "Upgrade"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/io/IOException;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    instance-of v0, p1, Ljavax/net/ssl/SSLHandshakeException;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/io/IOException;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    instance-of v0, v0, Ljava/security/cert/CertificateException;

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    instance-of v3, p1, Ljava/net/ProtocolException;

    if-nez v0, :cond_1

    if-nez v3, :cond_1

    :goto_1
    return v1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public static p()Ljava/lang/String;
    .locals 2

    const-string/jumbo v0, "http.agent"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Java"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "java.version"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private r()Lcom/squareup/okhttp/internal/http/ag;
    .locals 2

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->n:Lcom/squareup/okhttp/internal/http/ag;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/ag;->i()Lcom/squareup/okhttp/internal/http/aj;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/internal/http/aj;->a(Lcom/squareup/okhttp/internal/http/ai;)Lcom/squareup/okhttp/internal/http/aj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/aj;->a()Lcom/squareup/okhttp/internal/http/ag;

    move-result-object v0

    return-object v0
.end method

.method private s()V
    .locals 8

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->d:Lcom/squareup/okhttp/c;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->e:Lcom/squareup/okhttp/internal/http/aq;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->j:Lcom/squareup/okhttp/internal/http/aa;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/aa;->a()Ljava/net/URL;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    new-instance v0, Ljava/net/UnknownHostException;

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->j:Lcom/squareup/okhttp/internal/http/aa;

    invoke-virtual {v1}, Lcom/squareup/okhttp/internal/http/aa;->a()Ljava/net/URL;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/UnknownHostException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->j:Lcom/squareup/okhttp/internal/http/aa;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/aa;->k()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->a:Lcom/squareup/okhttp/j;

    invoke-virtual {v0}, Lcom/squareup/okhttp/j;->g()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v3

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->a:Lcom/squareup/okhttp/j;

    invoke-virtual {v0}, Lcom/squareup/okhttp/j;->h()Ljavax/net/ssl/HostnameVerifier;

    move-result-object v4

    :goto_0
    new-instance v0, Lcom/squareup/okhttp/a;

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/u;->j:Lcom/squareup/okhttp/internal/http/aa;

    invoke-virtual {v2}, Lcom/squareup/okhttp/internal/http/aa;->a()Ljava/net/URL;

    move-result-object v2

    invoke-static {v2}, Lfd;->a(Ljava/net/URL;)I

    move-result v2

    iget-object v5, p0, Lcom/squareup/okhttp/internal/http/u;->a:Lcom/squareup/okhttp/j;

    invoke-virtual {v5}, Lcom/squareup/okhttp/j;->i()Lcom/squareup/okhttp/g;

    move-result-object v5

    iget-object v6, p0, Lcom/squareup/okhttp/internal/http/u;->a:Lcom/squareup/okhttp/j;

    invoke-virtual {v6}, Lcom/squareup/okhttp/j;->c()Ljava/net/Proxy;

    move-result-object v6

    iget-object v7, p0, Lcom/squareup/okhttp/internal/http/u;->a:Lcom/squareup/okhttp/j;

    invoke-virtual {v7}, Lcom/squareup/okhttp/j;->m()Ljava/util/List;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lcom/squareup/okhttp/a;-><init>(Ljava/lang/String;ILjavax/net/ssl/SSLSocketFactory;Ljavax/net/ssl/HostnameVerifier;Lcom/squareup/okhttp/g;Ljava/net/Proxy;Ljava/util/List;)V

    new-instance v1, Lcom/squareup/okhttp/internal/http/aq;

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/u;->j:Lcom/squareup/okhttp/internal/http/aa;

    invoke-virtual {v2}, Lcom/squareup/okhttp/internal/http/aa;->b()Ljava/net/URI;

    move-result-object v3

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/u;->a:Lcom/squareup/okhttp/j;

    invoke-virtual {v2}, Lcom/squareup/okhttp/j;->d()Ljava/net/ProxySelector;

    move-result-object v4

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/u;->a:Lcom/squareup/okhttp/j;

    invoke-virtual {v2}, Lcom/squareup/okhttp/j;->j()Lcom/squareup/okhttp/d;

    move-result-object v5

    sget-object v6, Lev;->a:Lev;

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/u;->a:Lcom/squareup/okhttp/j;

    invoke-virtual {v2}, Lcom/squareup/okhttp/j;->l()Lcom/squareup/okhttp/n;

    move-result-object v7

    move-object v2, v0

    invoke-direct/range {v1 .. v7}, Lcom/squareup/okhttp/internal/http/aq;-><init>(Lcom/squareup/okhttp/a;Ljava/net/URI;Ljava/net/ProxySelector;Lcom/squareup/okhttp/d;Lev;Lcom/squareup/okhttp/n;)V

    iput-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->e:Lcom/squareup/okhttp/internal/http/aq;

    :goto_1
    invoke-virtual {v0}, Lcom/squareup/okhttp/a;->c()Ljava/util/List;

    move-result-object v1

    sget-object v2, Lcom/squareup/okhttp/Protocol;->b:Lcom/squareup/okhttp/Protocol;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->a:Lcom/squareup/okhttp/j;

    invoke-virtual {v1, v0}, Lcom/squareup/okhttp/j;->a(Lcom/squareup/okhttp/a;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->e:Lcom/squareup/okhttp/internal/http/aq;

    invoke-direct {p0, v1}, Lcom/squareup/okhttp/internal/http/u;->a(Lcom/squareup/okhttp/internal/http/aq;)Lcom/squareup/okhttp/c;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->d:Lcom/squareup/okhttp/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->a:Lcom/squareup/okhttp/j;

    invoke-virtual {v1, v0}, Lcom/squareup/okhttp/j;->b(Lcom/squareup/okhttp/a;)V

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :goto_2
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->d:Lcom/squareup/okhttp/c;

    invoke-virtual {v0}, Lcom/squareup/okhttp/c;->b()Lcom/squareup/okhttp/m;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->f:Lcom/squareup/okhttp/m;

    return-void

    :cond_3
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->e:Lcom/squareup/okhttp/internal/http/aq;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/aq;->a()Lcom/squareup/okhttp/a;

    move-result-object v0

    goto :goto_1

    :catchall_0
    move-exception v1

    :try_start_2
    iget-object v3, p0, Lcom/squareup/okhttp/internal/http/u;->a:Lcom/squareup/okhttp/j;

    invoke-virtual {v3, v0}, Lcom/squareup/okhttp/j;->b(Lcom/squareup/okhttp/a;)V

    throw v1

    :catchall_1
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    :cond_4
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->e:Lcom/squareup/okhttp/internal/http/aq;

    invoke-direct {p0, v0}, Lcom/squareup/okhttp/internal/http/u;->a(Lcom/squareup/okhttp/internal/http/aq;)Lcom/squareup/okhttp/c;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->d:Lcom/squareup/okhttp/c;

    goto :goto_2

    :cond_5
    move-object v3, v4

    goto/16 :goto_0
.end method

.method private t()V
    .locals 3

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->a:Lcom/squareup/okhttp/j;

    invoke-virtual {v0}, Lcom/squareup/okhttp/j;->f()Lcom/squareup/okhttp/l;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->n:Lcom/squareup/okhttp/internal/http/ag;

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/u;->j:Lcom/squareup/okhttp/internal/http/aa;

    invoke-static {v1, v2}, Lcom/squareup/okhttp/internal/http/a;->a(Lcom/squareup/okhttp/internal/http/ag;Lcom/squareup/okhttp/internal/http/aa;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->j:Lcom/squareup/okhttp/internal/http/aa;

    invoke-interface {v0, v1}, Lcom/squareup/okhttp/l;->b(Lcom/squareup/okhttp/internal/http/aa;)Z

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/squareup/okhttp/internal/http/u;->r()Lcom/squareup/okhttp/internal/http/ag;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/squareup/okhttp/l;->a(Lcom/squareup/okhttp/internal/http/ag;)Ljava/net/CacheRequest;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->s:Ljava/net/CacheRequest;

    goto :goto_0
.end method

.method private u()V
    .locals 4

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->j:Lcom/squareup/okhttp/internal/http/aa;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/aa;->f()Lcom/squareup/okhttp/internal/http/ad;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->j:Lcom/squareup/okhttp/internal/http/aa;

    invoke-virtual {v1}, Lcom/squareup/okhttp/internal/http/aa;->h()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {}, Lcom/squareup/okhttp/internal/http/u;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/internal/http/ad;->a(Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/ad;

    :cond_0
    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->j:Lcom/squareup/okhttp/internal/http/aa;

    const-string/jumbo v2, "Host"

    invoke-virtual {v1, v2}, Lcom/squareup/okhttp/internal/http/aa;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    const-string/jumbo v1, "Host"

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/u;->j:Lcom/squareup/okhttp/internal/http/aa;

    invoke-virtual {v2}, Lcom/squareup/okhttp/internal/http/aa;->a()Ljava/net/URL;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/okhttp/internal/http/u;->a(Ljava/net/URL;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/okhttp/internal/http/ad;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/ad;

    :cond_1
    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->d:Lcom/squareup/okhttp/c;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->d:Lcom/squareup/okhttp/c;

    invoke-virtual {v1}, Lcom/squareup/okhttp/c;->k()I

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->j:Lcom/squareup/okhttp/internal/http/aa;

    const-string/jumbo v2, "Connection"

    invoke-virtual {v1, v2}, Lcom/squareup/okhttp/internal/http/aa;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_3

    const-string/jumbo v1, "Connection"

    const-string/jumbo v2, "Keep-Alive"

    invoke-virtual {v0, v1, v2}, Lcom/squareup/okhttp/internal/http/ad;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/ad;

    :cond_3
    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->j:Lcom/squareup/okhttp/internal/http/aa;

    const-string/jumbo v2, "Accept-Encoding"

    invoke-virtual {v1, v2}, Lcom/squareup/okhttp/internal/http/aa;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_4

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/squareup/okhttp/internal/http/u;->h:Z

    const-string/jumbo v1, "Accept-Encoding"

    const-string/jumbo v2, "gzip"

    invoke-virtual {v0, v1, v2}, Lcom/squareup/okhttp/internal/http/ad;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/ad;

    :cond_4
    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/http/u;->c()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->j:Lcom/squareup/okhttp/internal/http/aa;

    const-string/jumbo v2, "Content-Type"

    invoke-virtual {v1, v2}, Lcom/squareup/okhttp/internal/http/aa;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_5

    const-string/jumbo v1, "Content-Type"

    const-string/jumbo v2, "application/x-www-form-urlencoded"

    invoke-virtual {v0, v1, v2}, Lcom/squareup/okhttp/internal/http/ad;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/ad;

    :cond_5
    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->a:Lcom/squareup/okhttp/j;

    invoke-virtual {v1}, Lcom/squareup/okhttp/j;->e()Ljava/net/CookieHandler;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/ad;->a()Lcom/squareup/okhttp/internal/http/aa;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/okhttp/internal/http/aa;->d()Lcom/squareup/okhttp/internal/http/f;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/squareup/okhttp/internal/http/y;->a(Lcom/squareup/okhttp/internal/http/f;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/okhttp/internal/http/u;->j:Lcom/squareup/okhttp/internal/http/aa;

    invoke-virtual {v3}, Lcom/squareup/okhttp/internal/http/aa;->b()Ljava/net/URI;

    move-result-object v3

    invoke-virtual {v1, v3, v2}, Ljava/net/CookieHandler;->get(Ljava/net/URI;Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/okhttp/internal/http/y;->a(Lcom/squareup/okhttp/internal/http/ad;Ljava/util/Map;)V

    :cond_6
    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/ad;->a()Lcom/squareup/okhttp/internal/http/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->j:Lcom/squareup/okhttp/internal/http/aa;

    return-void
.end method

.method private v()Lcom/squareup/okhttp/o;
    .locals 5

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->j:Lcom/squareup/okhttp/internal/http/aa;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/aa;->k()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->j:Lcom/squareup/okhttp/internal/http/aa;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/aa;->h()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/squareup/okhttp/internal/http/u;->p()Ljava/lang/String;

    move-result-object v0

    :cond_1
    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->j:Lcom/squareup/okhttp/internal/http/aa;

    invoke-virtual {v1}, Lcom/squareup/okhttp/internal/http/aa;->a()Ljava/net/URL;

    move-result-object v2

    new-instance v1, Lcom/squareup/okhttp/o;

    invoke-virtual {v2}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2}, Lfd;->a(Ljava/net/URL;)I

    move-result v2

    iget-object v4, p0, Lcom/squareup/okhttp/internal/http/u;->j:Lcom/squareup/okhttp/internal/http/aa;

    invoke-virtual {v4}, Lcom/squareup/okhttp/internal/http/aa;->i()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v3, v2, v0, v4}, Lcom/squareup/okhttp/o;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/io/IOException;)Lcom/squareup/okhttp/internal/http/u;
    .locals 7

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->e:Lcom/squareup/okhttp/internal/http/aq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->d:Lcom/squareup/okhttp/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->e:Lcom/squareup/okhttp/internal/http/aq;

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->d:Lcom/squareup/okhttp/c;

    invoke-virtual {v0, v1, p1}, Lcom/squareup/okhttp/internal/http/aq;->a(Lcom/squareup/okhttp/c;Ljava/io/IOException;)V

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->k:Lga;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->k:Lga;

    instance-of v0, v0, Lcom/squareup/okhttp/internal/http/ap;

    if-eqz v0, :cond_5

    :cond_1
    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->e:Lcom/squareup/okhttp/internal/http/aq;

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->d:Lcom/squareup/okhttp/c;

    if-eqz v1, :cond_4

    :cond_2
    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->e:Lcom/squareup/okhttp/internal/http/aq;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->e:Lcom/squareup/okhttp/internal/http/aq;

    invoke-virtual {v1}, Lcom/squareup/okhttp/internal/http/aq;->b()Z

    move-result v1

    if-eqz v1, :cond_4

    :cond_3
    invoke-direct {p0, p1}, Lcom/squareup/okhttp/internal/http/u;->b(Ljava/io/IOException;)Z

    move-result v1

    if-eqz v1, :cond_4

    if-nez v0, :cond_6

    :cond_4
    const/4 v0, 0x0

    :goto_1
    return-object v0

    :cond_5
    const/4 v0, 0x0

    goto :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/http/u;->n()Lcom/squareup/okhttp/c;

    move-result-object v4

    new-instance v0, Lcom/squareup/okhttp/internal/http/u;

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->a:Lcom/squareup/okhttp/j;

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/u;->i:Lcom/squareup/okhttp/internal/http/aa;

    iget-boolean v3, p0, Lcom/squareup/okhttp/internal/http/u;->c:Z

    iget-object v5, p0, Lcom/squareup/okhttp/internal/http/u;->e:Lcom/squareup/okhttp/internal/http/aq;

    iget-object v6, p0, Lcom/squareup/okhttp/internal/http/u;->k:Lga;

    check-cast v6, Lcom/squareup/okhttp/internal/http/ap;

    invoke-direct/range {v0 .. v6}, Lcom/squareup/okhttp/internal/http/u;-><init>(Lcom/squareup/okhttp/j;Lcom/squareup/okhttp/internal/http/aa;ZLcom/squareup/okhttp/c;Lcom/squareup/okhttp/internal/http/aq;Lcom/squareup/okhttp/internal/http/ap;)V

    goto :goto_1
.end method

.method public final a()V
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->m:Lcom/squareup/okhttp/ResponseSource;

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->g:Lcom/squareup/okhttp/internal/http/au;

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_2
    invoke-direct {p0}, Lcom/squareup/okhttp/internal/http/u;->u()V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->a:Lcom/squareup/okhttp/j;

    invoke-virtual {v0}, Lcom/squareup/okhttp/j;->f()Lcom/squareup/okhttp/l;

    move-result-object v2

    if-eqz v2, :cond_7

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->j:Lcom/squareup/okhttp/internal/http/aa;

    invoke-interface {v2, v0}, Lcom/squareup/okhttp/l;->a(Lcom/squareup/okhttp/internal/http/aa;)Lcom/squareup/okhttp/internal/http/ag;

    move-result-object v0

    :goto_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    new-instance v5, Lcom/squareup/okhttp/internal/http/c;

    iget-object v6, p0, Lcom/squareup/okhttp/internal/http/u;->j:Lcom/squareup/okhttp/internal/http/aa;

    invoke-direct {v5, v3, v4, v6, v0}, Lcom/squareup/okhttp/internal/http/c;-><init>(JLcom/squareup/okhttp/internal/http/aa;Lcom/squareup/okhttp/internal/http/ag;)V

    invoke-virtual {v5}, Lcom/squareup/okhttp/internal/http/c;->a()Lcom/squareup/okhttp/internal/http/a;

    move-result-object v3

    iget-object v4, v3, Lcom/squareup/okhttp/internal/http/a;->c:Lcom/squareup/okhttp/ResponseSource;

    iput-object v4, p0, Lcom/squareup/okhttp/internal/http/u;->m:Lcom/squareup/okhttp/ResponseSource;

    iget-object v4, v3, Lcom/squareup/okhttp/internal/http/a;->a:Lcom/squareup/okhttp/internal/http/aa;

    iput-object v4, p0, Lcom/squareup/okhttp/internal/http/u;->j:Lcom/squareup/okhttp/internal/http/aa;

    if-eqz v2, :cond_3

    iget-object v4, p0, Lcom/squareup/okhttp/internal/http/u;->m:Lcom/squareup/okhttp/ResponseSource;

    invoke-interface {v2, v4}, Lcom/squareup/okhttp/l;->a(Lcom/squareup/okhttp/ResponseSource;)V

    :cond_3
    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/u;->m:Lcom/squareup/okhttp/ResponseSource;

    sget-object v4, Lcom/squareup/okhttp/ResponseSource;->c:Lcom/squareup/okhttp/ResponseSource;

    if-eq v2, v4, :cond_4

    iget-object v2, v3, Lcom/squareup/okhttp/internal/http/a;->b:Lcom/squareup/okhttp/internal/http/ag;

    iput-object v2, p0, Lcom/squareup/okhttp/internal/http/u;->r:Lcom/squareup/okhttp/internal/http/ag;

    :cond_4
    if-eqz v0, :cond_5

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/u;->m:Lcom/squareup/okhttp/ResponseSource;

    invoke-virtual {v2}, Lcom/squareup/okhttp/ResponseSource;->b()Z

    move-result v2

    if-nez v2, :cond_5

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/ag;->h()Lcom/squareup/okhttp/internal/http/ai;

    move-result-object v0

    invoke-static {v0}, Lfd;->a(Ljava/io/Closeable;)V

    :cond_5
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->m:Lcom/squareup/okhttp/ResponseSource;

    invoke-virtual {v0}, Lcom/squareup/okhttp/ResponseSource;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->d:Lcom/squareup/okhttp/c;

    if-nez v0, :cond_6

    invoke-direct {p0}, Lcom/squareup/okhttp/internal/http/u;->s()V

    :cond_6
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->d:Lcom/squareup/okhttp/c;

    invoke-virtual {v0, p0}, Lcom/squareup/okhttp/c;->a(Lcom/squareup/okhttp/internal/http/u;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/okhttp/internal/http/au;

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->g:Lcom/squareup/okhttp/internal/http/au;

    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/http/u;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->k:Lga;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->g:Lcom/squareup/okhttp/internal/http/au;

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->j:Lcom/squareup/okhttp/internal/http/aa;

    invoke-interface {v0, v1}, Lcom/squareup/okhttp/internal/http/au;->a(Lcom/squareup/okhttp/internal/http/aa;)Lga;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->k:Lga;

    goto/16 :goto_0

    :cond_7
    move-object v0, v1

    goto :goto_1

    :cond_8
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->d:Lcom/squareup/okhttp/c;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->a:Lcom/squareup/okhttp/j;

    invoke-virtual {v0}, Lcom/squareup/okhttp/j;->j()Lcom/squareup/okhttp/d;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/u;->d:Lcom/squareup/okhttp/c;

    invoke-virtual {v0, v2}, Lcom/squareup/okhttp/d;->a(Lcom/squareup/okhttp/c;)V

    iput-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->d:Lcom/squareup/okhttp/c;

    :cond_9
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->r:Lcom/squareup/okhttp/internal/http/ag;

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->n:Lcom/squareup/okhttp/internal/http/ag;

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->r:Lcom/squareup/okhttp/internal/http/ag;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/ag;->h()Lcom/squareup/okhttp/internal/http/ai;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->r:Lcom/squareup/okhttp/internal/http/ag;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/ag;->h()Lcom/squareup/okhttp/internal/http/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/ai;->b()Lgb;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/okhttp/internal/http/u;->a(Lgb;)V

    goto/16 :goto_0
.end method

.method public a(Lcom/squareup/okhttp/internal/http/f;)V
    .locals 3

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->a:Lcom/squareup/okhttp/j;

    invoke-virtual {v0}, Lcom/squareup/okhttp/j;->e()Ljava/net/CookieHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->j:Lcom/squareup/okhttp/internal/http/aa;

    invoke-virtual {v1}, Lcom/squareup/okhttp/internal/http/aa;->b()Ljava/net/URI;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p1, v2}, Lcom/squareup/okhttp/internal/http/y;->a(Lcom/squareup/okhttp/internal/http/f;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/net/CookieHandler;->put(Ljava/net/URI;Ljava/util/Map;)V

    :cond_0
    return-void
.end method

.method public b()V
    .locals 4

    iget-wide v0, p0, Lcom/squareup/okhttp/internal/http/u;->b:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/okhttp/internal/http/u;->b:J

    return-void
.end method

.method c()Z
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->j:Lcom/squareup/okhttp/internal/http/aa;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/aa;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/okhttp/internal/http/v;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final d()Lga;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->m:Lcom/squareup/okhttp/ResponseSource;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->k:Lga;

    return-object v0
.end method

.method public final e()Lfg;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->l:Lfg;

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/http/u;->d()Lga;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, Lfr;->a(Lga;)Lfg;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->l:Lfg;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->n:Lcom/squareup/okhttp/internal/http/ag;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Lcom/squareup/okhttp/internal/http/aa;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->j:Lcom/squareup/okhttp/internal/http/aa;

    return-object v0
.end method

.method public final h()Lcom/squareup/okhttp/internal/http/ag;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->n:Lcom/squareup/okhttp/internal/http/ag;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->n:Lcom/squareup/okhttp/internal/http/ag;

    return-object v0
.end method

.method public final i()Lgb;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->n:Lcom/squareup/okhttp/internal/http/ag;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->p:Lgb;

    return-object v0
.end method

.method public final j()Ljava/io/InputStream;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->q:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/http/u;->i()Lgb;

    move-result-object v0

    invoke-static {v0}, Lfr;->a(Lgb;)Lfh;

    move-result-object v0

    invoke-interface {v0}, Lfh;->k()Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->q:Ljava/io/InputStream;

    goto :goto_0
.end method

.method public final k()Lcom/squareup/okhttp/c;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->d:Lcom/squareup/okhttp/c;

    return-object v0
.end method

.method public l()Lcom/squareup/okhttp/m;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->f:Lcom/squareup/okhttp/m;

    return-object v0
.end method

.method public final m()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->g:Lcom/squareup/okhttp/internal/http/au;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->d:Lcom/squareup/okhttp/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->g:Lcom/squareup/okhttp/internal/http/au;

    invoke-interface {v0}, Lcom/squareup/okhttp/internal/http/au;->c()V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->d:Lcom/squareup/okhttp/c;

    return-void
.end method

.method public final n()Lcom/squareup/okhttp/c;
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->l:Lfg;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->l:Lfg;

    invoke-static {v1}, Lfd;->a(Ljava/io/Closeable;)V

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->p:Lgb;

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->d:Lcom/squareup/okhttp/c;

    invoke-static {v1}, Lfd;->a(Ljava/io/Closeable;)V

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->d:Lcom/squareup/okhttp/c;

    :goto_1
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->k:Lga;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->k:Lga;

    invoke-static {v1}, Lfd;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->p:Lgb;

    invoke-static {v1}, Lfd;->a(Ljava/io/Closeable;)V

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->q:Ljava/io/InputStream;

    invoke-static {v1}, Lfd;->a(Ljava/io/Closeable;)V

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->g:Lcom/squareup/okhttp/internal/http/au;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->g:Lcom/squareup/okhttp/internal/http/au;

    invoke-interface {v1}, Lcom/squareup/okhttp/internal/http/au;->d()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->d:Lcom/squareup/okhttp/c;

    invoke-static {v1}, Lfd;->a(Ljava/io/Closeable;)V

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->d:Lcom/squareup/okhttp/c;

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->d:Lcom/squareup/okhttp/c;

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->d:Lcom/squareup/okhttp/c;

    move-object v0, v1

    goto :goto_1
.end method

.method public final o()Z
    .locals 6

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/u;->j:Lcom/squareup/okhttp/internal/http/aa;

    invoke-virtual {v2}, Lcom/squareup/okhttp/internal/http/aa;->c()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "HEAD"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/u;->n:Lcom/squareup/okhttp/internal/http/ag;

    invoke-virtual {v2}, Lcom/squareup/okhttp/internal/http/ag;->c()I

    move-result v2

    const/16 v3, 0x64

    if-lt v2, v3, :cond_2

    const/16 v3, 0xc8

    if-lt v2, v3, :cond_3

    :cond_2
    const/16 v3, 0xcc

    if-eq v2, v3, :cond_3

    const/16 v3, 0x130

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/u;->n:Lcom/squareup/okhttp/internal/http/ag;

    invoke-static {v2}, Lcom/squareup/okhttp/internal/http/y;->a(Lcom/squareup/okhttp/internal/http/ag;)J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    const-string/jumbo v2, "chunked"

    iget-object v3, p0, Lcom/squareup/okhttp/internal/http/u;->n:Lcom/squareup/okhttp/internal/http/ag;

    const-string/jumbo v4, "Transfer-Encoding"

    invoke-virtual {v3, v4}, Lcom/squareup/okhttp/internal/http/ag;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final q()V
    .locals 6

    const-wide/16 v4, -0x1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->n:Lcom/squareup/okhttp/internal/http/ag;

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->m:Lcom/squareup/okhttp/ResponseSource;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "call sendRequest() first!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->m:Lcom/squareup/okhttp/ResponseSource;

    invoke-virtual {v0}, Lcom/squareup/okhttp/ResponseSource;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->l:Lfg;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->l:Lfg;

    invoke-interface {v0}, Lfg;->b()Lfo;

    move-result-object v0

    invoke-virtual {v0}, Lfo;->l()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->l:Lfg;

    invoke-interface {v0}, Lfg;->a()V

    :cond_3
    iget-wide v0, p0, Lcom/squareup/okhttp/internal/http/u;->b:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->j:Lcom/squareup/okhttp/internal/http/aa;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/http/y;->a(Lcom/squareup/okhttp/internal/http/aa;)J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->k:Lga;

    instance-of v0, v0, Lcom/squareup/okhttp/internal/http/ap;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->k:Lga;

    check-cast v0, Lcom/squareup/okhttp/internal/http/ap;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/ap;->b()J

    move-result-wide v0

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/u;->j:Lcom/squareup/okhttp/internal/http/aa;

    invoke-virtual {v2}, Lcom/squareup/okhttp/internal/http/aa;->f()Lcom/squareup/okhttp/internal/http/ad;

    move-result-object v2

    const-string/jumbo v3, "Content-Length"

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lcom/squareup/okhttp/internal/http/ad;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/ad;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/ad;->a()Lcom/squareup/okhttp/internal/http/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->j:Lcom/squareup/okhttp/internal/http/aa;

    :cond_4
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->g:Lcom/squareup/okhttp/internal/http/au;

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->j:Lcom/squareup/okhttp/internal/http/aa;

    invoke-interface {v0, v1}, Lcom/squareup/okhttp/internal/http/au;->b(Lcom/squareup/okhttp/internal/http/aa;)V

    :cond_5
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->k:Lga;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->l:Lfg;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->l:Lfg;

    invoke-interface {v0}, Lfg;->close()V

    :goto_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->k:Lga;

    instance-of v0, v0, Lcom/squareup/okhttp/internal/http/ap;

    if-eqz v0, :cond_6

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->g:Lcom/squareup/okhttp/internal/http/au;

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->k:Lga;

    check-cast v0, Lcom/squareup/okhttp/internal/http/ap;

    invoke-interface {v1, v0}, Lcom/squareup/okhttp/internal/http/au;->a(Lcom/squareup/okhttp/internal/http/ap;)V

    :cond_6
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->g:Lcom/squareup/okhttp/internal/http/au;

    invoke-interface {v0}, Lcom/squareup/okhttp/internal/http/au;->a()V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->g:Lcom/squareup/okhttp/internal/http/au;

    invoke-interface {v0}, Lcom/squareup/okhttp/internal/http/au;->b()Lcom/squareup/okhttp/internal/http/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->j:Lcom/squareup/okhttp/internal/http/aa;

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/internal/http/aj;->a(Lcom/squareup/okhttp/internal/http/aa;)Lcom/squareup/okhttp/internal/http/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->d:Lcom/squareup/okhttp/c;

    invoke-virtual {v1}, Lcom/squareup/okhttp/c;->i()Lcom/squareup/okhttp/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/internal/http/aj;->a(Lcom/squareup/okhttp/f;)Lcom/squareup/okhttp/internal/http/aj;

    move-result-object v0

    sget-object v1, Lcom/squareup/okhttp/internal/http/y;->b:Ljava/lang/String;

    iget-wide v2, p0, Lcom/squareup/okhttp/internal/http/u;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/okhttp/internal/http/aj;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/aj;

    move-result-object v0

    sget-object v1, Lcom/squareup/okhttp/internal/http/y;->c:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/okhttp/internal/http/aj;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->m:Lcom/squareup/okhttp/ResponseSource;

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/internal/http/aj;->a(Lcom/squareup/okhttp/ResponseSource;)Lcom/squareup/okhttp/internal/http/aj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/aj;->a()Lcom/squareup/okhttp/internal/http/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->n:Lcom/squareup/okhttp/internal/http/ag;

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->d:Lcom/squareup/okhttp/c;

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->n:Lcom/squareup/okhttp/internal/http/ag;

    invoke-virtual {v1}, Lcom/squareup/okhttp/internal/http/ag;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/c;->a(I)V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->n:Lcom/squareup/okhttp/internal/http/ag;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/ag;->g()Lcom/squareup/okhttp/internal/http/f;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/squareup/okhttp/internal/http/u;->a(Lcom/squareup/okhttp/internal/http/f;)V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->m:Lcom/squareup/okhttp/ResponseSource;

    sget-object v1, Lcom/squareup/okhttp/ResponseSource;->b:Lcom/squareup/okhttp/ResponseSource;

    if-ne v0, v1, :cond_9

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->r:Lcom/squareup/okhttp/internal/http/ag;

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->n:Lcom/squareup/okhttp/internal/http/ag;

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/internal/http/ag;->a(Lcom/squareup/okhttp/internal/http/ag;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->g:Lcom/squareup/okhttp/internal/http/au;

    invoke-interface {v0}, Lcom/squareup/okhttp/internal/http/au;->e()V

    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/http/u;->m()V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->r:Lcom/squareup/okhttp/internal/http/ag;

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->n:Lcom/squareup/okhttp/internal/http/ag;

    invoke-static {v0, v1}, Lcom/squareup/okhttp/internal/http/u;->a(Lcom/squareup/okhttp/internal/http/ag;Lcom/squareup/okhttp/internal/http/ag;)Lcom/squareup/okhttp/internal/http/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->n:Lcom/squareup/okhttp/internal/http/ag;

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->a:Lcom/squareup/okhttp/j;

    invoke-virtual {v0}, Lcom/squareup/okhttp/j;->f()Lcom/squareup/okhttp/l;

    move-result-object v0

    invoke-interface {v0}, Lcom/squareup/okhttp/l;->a()V

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->r:Lcom/squareup/okhttp/internal/http/ag;

    invoke-direct {p0}, Lcom/squareup/okhttp/internal/http/u;->r()Lcom/squareup/okhttp/internal/http/ag;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/squareup/okhttp/l;->a(Lcom/squareup/okhttp/internal/http/ag;Lcom/squareup/okhttp/internal/http/ag;)V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->r:Lcom/squareup/okhttp/internal/http/ag;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/ag;->h()Lcom/squareup/okhttp/internal/http/ai;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->r:Lcom/squareup/okhttp/internal/http/ag;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/ag;->h()Lcom/squareup/okhttp/internal/http/ai;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/ai;->b()Lgb;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/okhttp/internal/http/u;->a(Lgb;)V

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->k:Lga;

    invoke-interface {v0}, Lga;->close()V

    goto/16 :goto_1

    :cond_8
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->r:Lcom/squareup/okhttp/internal/http/ag;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/ag;->h()Lcom/squareup/okhttp/internal/http/ai;

    move-result-object v0

    invoke-static {v0}, Lfd;->a(Ljava/io/Closeable;)V

    :cond_9
    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/http/u;->o()Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->g:Lcom/squareup/okhttp/internal/http/au;

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->s:Ljava/net/CacheRequest;

    invoke-interface {v0, v1}, Lcom/squareup/okhttp/internal/http/au;->a(Ljava/net/CacheRequest;)Lgb;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->o:Lgb;

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->o:Lgb;

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->p:Lgb;

    goto/16 :goto_0

    :cond_a
    invoke-direct {p0}, Lcom/squareup/okhttp/internal/http/u;->t()V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/u;->g:Lcom/squareup/okhttp/internal/http/au;

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/u;->s:Ljava/net/CacheRequest;

    invoke-interface {v0, v1}, Lcom/squareup/okhttp/internal/http/au;->a(Ljava/net/CacheRequest;)Lgb;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/squareup/okhttp/internal/http/u;->a(Lgb;)V

    goto/16 :goto_0
.end method
