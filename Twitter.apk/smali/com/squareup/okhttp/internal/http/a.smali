.class public final Lcom/squareup/okhttp/internal/http/a;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final d:Lcom/squareup/okhttp/internal/http/ai;

.field private static final e:Lcom/squareup/okhttp/internal/http/at;


# instance fields
.field public final a:Lcom/squareup/okhttp/internal/http/aa;

.field public final b:Lcom/squareup/okhttp/internal/http/ag;

.field public final c:Lcom/squareup/okhttp/ResponseSource;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/squareup/okhttp/internal/http/b;

    invoke-direct {v0}, Lcom/squareup/okhttp/internal/http/b;-><init>()V

    sput-object v0, Lcom/squareup/okhttp/internal/http/a;->d:Lcom/squareup/okhttp/internal/http/ai;

    :try_start_0
    new-instance v0, Lcom/squareup/okhttp/internal/http/at;

    const-string/jumbo v1, "HTTP/1.1 504 Gateway Timeout"

    invoke-direct {v0, v1}, Lcom/squareup/okhttp/internal/http/at;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/squareup/okhttp/internal/http/a;->e:Lcom/squareup/okhttp/internal/http/at;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method

.method private constructor <init>(Lcom/squareup/okhttp/internal/http/aa;Lcom/squareup/okhttp/internal/http/ag;Lcom/squareup/okhttp/ResponseSource;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/okhttp/internal/http/a;->a:Lcom/squareup/okhttp/internal/http/aa;

    iput-object p2, p0, Lcom/squareup/okhttp/internal/http/a;->b:Lcom/squareup/okhttp/internal/http/ag;

    iput-object p3, p0, Lcom/squareup/okhttp/internal/http/a;->c:Lcom/squareup/okhttp/ResponseSource;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/okhttp/internal/http/aa;Lcom/squareup/okhttp/internal/http/ag;Lcom/squareup/okhttp/ResponseSource;Lcom/squareup/okhttp/internal/http/b;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/okhttp/internal/http/a;-><init>(Lcom/squareup/okhttp/internal/http/aa;Lcom/squareup/okhttp/internal/http/ag;Lcom/squareup/okhttp/ResponseSource;)V

    return-void
.end method

.method static synthetic a()Lcom/squareup/okhttp/internal/http/ai;
    .locals 1

    sget-object v0, Lcom/squareup/okhttp/internal/http/a;->d:Lcom/squareup/okhttp/internal/http/ai;

    return-object v0
.end method

.method public static a(Lcom/squareup/okhttp/internal/http/ag;Lcom/squareup/okhttp/internal/http/aa;)Z
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/http/ag;->c()I

    move-result v1

    const/16 v2, 0xc8

    if-eq v1, v2, :cond_1

    const/16 v2, 0xcb

    if-eq v1, v2, :cond_1

    const/16 v2, 0x12c

    if-eq v1, v2, :cond_1

    const/16 v2, 0x12d

    if-eq v1, v2, :cond_1

    const/16 v2, 0x19a

    if-eq v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/http/ag;->j()Lcom/squareup/okhttp/b;

    move-result-object v1

    const-string/jumbo v2, "Authorization"

    invoke-virtual {p1, v2}, Lcom/squareup/okhttp/internal/http/aa;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/squareup/okhttp/b;->e()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v1}, Lcom/squareup/okhttp/b;->f()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v1}, Lcom/squareup/okhttp/b;->d()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    :cond_2
    invoke-virtual {v1}, Lcom/squareup/okhttp/b;->b()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic b()Lcom/squareup/okhttp/internal/http/at;
    .locals 1

    sget-object v0, Lcom/squareup/okhttp/internal/http/a;->e:Lcom/squareup/okhttp/internal/http/at;

    return-object v0
.end method
