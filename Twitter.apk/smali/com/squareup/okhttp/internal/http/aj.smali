.class public Lcom/squareup/okhttp/internal/http/aj;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private a:Lcom/squareup/okhttp/internal/http/aa;

.field private b:Lcom/squareup/okhttp/internal/http/at;

.field private c:Lcom/squareup/okhttp/f;

.field private d:Lcom/squareup/okhttp/internal/http/h;

.field private e:Lcom/squareup/okhttp/internal/http/ai;

.field private f:Lcom/squareup/okhttp/internal/http/ag;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/squareup/okhttp/internal/http/h;

    invoke-direct {v0}, Lcom/squareup/okhttp/internal/http/h;-><init>()V

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/aj;->d:Lcom/squareup/okhttp/internal/http/h;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/okhttp/internal/http/ag;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/squareup/okhttp/internal/http/ag;->b(Lcom/squareup/okhttp/internal/http/ag;)Lcom/squareup/okhttp/internal/http/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/aj;->a:Lcom/squareup/okhttp/internal/http/aa;

    invoke-static {p1}, Lcom/squareup/okhttp/internal/http/ag;->c(Lcom/squareup/okhttp/internal/http/ag;)Lcom/squareup/okhttp/internal/http/at;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/aj;->b:Lcom/squareup/okhttp/internal/http/at;

    invoke-static {p1}, Lcom/squareup/okhttp/internal/http/ag;->d(Lcom/squareup/okhttp/internal/http/ag;)Lcom/squareup/okhttp/f;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/aj;->c:Lcom/squareup/okhttp/f;

    invoke-static {p1}, Lcom/squareup/okhttp/internal/http/ag;->e(Lcom/squareup/okhttp/internal/http/ag;)Lcom/squareup/okhttp/internal/http/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/f;->b()Lcom/squareup/okhttp/internal/http/h;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/aj;->d:Lcom/squareup/okhttp/internal/http/h;

    invoke-static {p1}, Lcom/squareup/okhttp/internal/http/ag;->f(Lcom/squareup/okhttp/internal/http/ag;)Lcom/squareup/okhttp/internal/http/ai;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/aj;->e:Lcom/squareup/okhttp/internal/http/ai;

    invoke-static {p1}, Lcom/squareup/okhttp/internal/http/ag;->g(Lcom/squareup/okhttp/internal/http/ag;)Lcom/squareup/okhttp/internal/http/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/aj;->f:Lcom/squareup/okhttp/internal/http/ag;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/okhttp/internal/http/ag;Lcom/squareup/okhttp/internal/http/ah;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/okhttp/internal/http/aj;-><init>(Lcom/squareup/okhttp/internal/http/ag;)V

    return-void
.end method

.method static synthetic a(Lcom/squareup/okhttp/internal/http/aj;)Lcom/squareup/okhttp/internal/http/aa;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/aj;->a:Lcom/squareup/okhttp/internal/http/aa;

    return-object v0
.end method

.method static synthetic b(Lcom/squareup/okhttp/internal/http/aj;)Lcom/squareup/okhttp/internal/http/at;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/aj;->b:Lcom/squareup/okhttp/internal/http/at;

    return-object v0
.end method

.method static synthetic c(Lcom/squareup/okhttp/internal/http/aj;)Lcom/squareup/okhttp/f;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/aj;->c:Lcom/squareup/okhttp/f;

    return-object v0
.end method

.method static synthetic d(Lcom/squareup/okhttp/internal/http/aj;)Lcom/squareup/okhttp/internal/http/h;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/aj;->d:Lcom/squareup/okhttp/internal/http/h;

    return-object v0
.end method

.method static synthetic e(Lcom/squareup/okhttp/internal/http/aj;)Lcom/squareup/okhttp/internal/http/ai;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/aj;->e:Lcom/squareup/okhttp/internal/http/ai;

    return-object v0
.end method

.method static synthetic f(Lcom/squareup/okhttp/internal/http/aj;)Lcom/squareup/okhttp/internal/http/ag;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/aj;->f:Lcom/squareup/okhttp/internal/http/ag;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/squareup/okhttp/internal/http/ag;
    .locals 2

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/aj;->a:Lcom/squareup/okhttp/internal/http/aa;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "request == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/aj;->b:Lcom/squareup/okhttp/internal/http/at;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "statusLine == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Lcom/squareup/okhttp/internal/http/ag;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/okhttp/internal/http/ag;-><init>(Lcom/squareup/okhttp/internal/http/aj;Lcom/squareup/okhttp/internal/http/ah;)V

    return-object v0
.end method

.method public a(Lcom/squareup/okhttp/ResponseSource;)Lcom/squareup/okhttp/internal/http/aj;
    .locals 3

    sget-object v0, Lcom/squareup/okhttp/internal/http/y;->d:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/aj;->b:Lcom/squareup/okhttp/internal/http/at;

    invoke-virtual {v2}, Lcom/squareup/okhttp/internal/http/at;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/squareup/okhttp/internal/http/aj;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/aj;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/squareup/okhttp/f;)Lcom/squareup/okhttp/internal/http/aj;
    .locals 0

    iput-object p1, p0, Lcom/squareup/okhttp/internal/http/aj;->c:Lcom/squareup/okhttp/f;

    return-object p0
.end method

.method public a(Lcom/squareup/okhttp/internal/http/aa;)Lcom/squareup/okhttp/internal/http/aj;
    .locals 0

    iput-object p1, p0, Lcom/squareup/okhttp/internal/http/aj;->a:Lcom/squareup/okhttp/internal/http/aa;

    return-object p0
.end method

.method public a(Lcom/squareup/okhttp/internal/http/ai;)Lcom/squareup/okhttp/internal/http/aj;
    .locals 0

    iput-object p1, p0, Lcom/squareup/okhttp/internal/http/aj;->e:Lcom/squareup/okhttp/internal/http/ai;

    return-object p0
.end method

.method public a(Lcom/squareup/okhttp/internal/http/at;)Lcom/squareup/okhttp/internal/http/aj;
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "statusLine == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/squareup/okhttp/internal/http/aj;->b:Lcom/squareup/okhttp/internal/http/at;

    return-object p0
.end method

.method public a(Lcom/squareup/okhttp/internal/http/f;)Lcom/squareup/okhttp/internal/http/aj;
    .locals 1

    invoke-virtual {p1}, Lcom/squareup/okhttp/internal/http/f;->b()Lcom/squareup/okhttp/internal/http/h;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/aj;->d:Lcom/squareup/okhttp/internal/http/h;

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/aj;
    .locals 2

    :try_start_0
    new-instance v0, Lcom/squareup/okhttp/internal/http/at;

    invoke-direct {v0, p1}, Lcom/squareup/okhttp/internal/http/at;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/squareup/okhttp/internal/http/aj;->a(Lcom/squareup/okhttp/internal/http/at;)Lcom/squareup/okhttp/internal/http/aj;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/aj;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/aj;->d:Lcom/squareup/okhttp/internal/http/h;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/okhttp/internal/http/h;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/h;

    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/aj;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/aj;->d:Lcom/squareup/okhttp/internal/http/h;

    invoke-virtual {v0, p1}, Lcom/squareup/okhttp/internal/http/h;->b(Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/h;

    return-object p0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/aj;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/aj;->d:Lcom/squareup/okhttp/internal/http/h;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/okhttp/internal/http/h;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/h;

    return-object p0
.end method
