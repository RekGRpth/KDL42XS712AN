.class public final Lcom/squareup/okhttp/internal/http/ar;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/squareup/okhttp/internal/http/au;


# instance fields
.field private final a:Lcom/squareup/okhttp/internal/http/u;

.field private final b:Lcom/squareup/okhttp/internal/spdy/y;

.field private c:Lcom/squareup/okhttp/internal/spdy/ak;


# direct methods
.method public constructor <init>(Lcom/squareup/okhttp/internal/http/u;Lcom/squareup/okhttp/internal/spdy/y;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/okhttp/internal/http/ar;->a:Lcom/squareup/okhttp/internal/http/u;

    iput-object p2, p0, Lcom/squareup/okhttp/internal/http/ar;->b:Lcom/squareup/okhttp/internal/spdy/y;

    return-void
.end method

.method public static a(Ljava/util/List;Lcom/squareup/okhttp/Protocol;)Lcom/squareup/okhttp/internal/http/aj;
    .locals 11

    const/4 v3, 0x0

    const/4 v2, 0x0

    const-string/jumbo v1, "HTTP/1.1"

    new-instance v6, Lcom/squareup/okhttp/internal/http/h;

    invoke-direct {v6}, Lcom/squareup/okhttp/internal/http/h;-><init>()V

    sget-object v0, Lcom/squareup/okhttp/internal/http/y;->e:Ljava/lang/String;

    iget-object v4, p1, Lcom/squareup/okhttp/Protocol;->name:Lfi;

    invoke-virtual {v4}, Lfi;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v0, v4}, Lcom/squareup/okhttp/internal/http/h;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/h;

    move v5, v3

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v5, v0, :cond_5

    invoke-interface {p0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/okhttp/internal/spdy/d;

    iget-object v7, v0, Lcom/squareup/okhttp/internal/spdy/d;->h:Lfi;

    invoke-interface {p0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/okhttp/internal/spdy/d;

    iget-object v0, v0, Lcom/squareup/okhttp/internal/spdy/d;->i:Lfi;

    invoke-virtual {v0}, Lfi;->a()Ljava/lang/String;

    move-result-object v8

    move-object v0, v1

    move v1, v3

    :goto_1
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v1, v4, :cond_4

    invoke-virtual {v8, v3, v1}, Ljava/lang/String;->indexOf(II)I

    move-result v4

    const/4 v9, -0x1

    if-ne v4, v9, :cond_0

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v4

    :cond_0
    invoke-virtual {v8, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    sget-object v9, Lcom/squareup/okhttp/internal/spdy/d;->a:Lfi;

    invoke-virtual {v7, v9}, Lfi;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    :goto_2
    add-int/lit8 v2, v4, 0x1

    move v10, v2

    move-object v2, v1

    move v1, v10

    goto :goto_1

    :cond_1
    sget-object v9, Lcom/squareup/okhttp/internal/spdy/d;->g:Lfi;

    invoke-virtual {v7, v9}, Lfi;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    move-object v0, v1

    move-object v1, v2

    goto :goto_2

    :cond_2
    invoke-static {p1, v7}, Lcom/squareup/okhttp/internal/http/ar;->a(Lcom/squareup/okhttp/Protocol;Lfi;)Z

    move-result v9

    if-nez v9, :cond_3

    invoke-virtual {v7}, Lfi;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9, v1}, Lcom/squareup/okhttp/internal/http/h;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/h;

    :cond_3
    move-object v1, v2

    goto :goto_2

    :cond_4
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move-object v1, v0

    goto :goto_0

    :cond_5
    if-nez v2, :cond_6

    new-instance v0, Ljava/net/ProtocolException;

    const-string/jumbo v1, "Expected \':status\' header not present"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    if-nez v1, :cond_7

    new-instance v0, Ljava/net/ProtocolException;

    const-string/jumbo v1, "Expected \':version\' header not present"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    new-instance v0, Lcom/squareup/okhttp/internal/http/aj;

    invoke-direct {v0}, Lcom/squareup/okhttp/internal/http/aj;-><init>()V

    new-instance v3, Lcom/squareup/okhttp/internal/http/at;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Lcom/squareup/okhttp/internal/http/at;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Lcom/squareup/okhttp/internal/http/aj;->a(Lcom/squareup/okhttp/internal/http/at;)Lcom/squareup/okhttp/internal/http/aj;

    move-result-object v0

    invoke-virtual {v6}, Lcom/squareup/okhttp/internal/http/h;->a()Lcom/squareup/okhttp/internal/http/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/internal/http/aj;->a(Lcom/squareup/okhttp/internal/http/f;)Lcom/squareup/okhttp/internal/http/aj;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/squareup/okhttp/internal/http/aa;Lcom/squareup/okhttp/Protocol;Ljava/lang/String;)Ljava/util/List;
    .locals 9

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/http/aa;->d()Lcom/squareup/okhttp/internal/http/f;

    move-result-object v4

    new-instance v5, Ljava/util/ArrayList;

    invoke-virtual {v4}, Lcom/squareup/okhttp/internal/http/f;->a()I

    move-result v0

    add-int/lit8 v0, v0, 0xa

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v0, Lcom/squareup/okhttp/internal/spdy/d;

    sget-object v1, Lcom/squareup/okhttp/internal/spdy/d;->b:Lfi;

    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/http/aa;->c()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Lcom/squareup/okhttp/internal/spdy/d;-><init>(Lfi;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/squareup/okhttp/internal/spdy/d;

    sget-object v1, Lcom/squareup/okhttp/internal/spdy/d;->c:Lfi;

    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/http/aa;->a()Ljava/net/URL;

    move-result-object v3

    invoke-static {v3}, Lcom/squareup/okhttp/internal/http/af;->a(Ljava/net/URL;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Lcom/squareup/okhttp/internal/spdy/d;-><init>(Lfi;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/http/aa;->a()Ljava/net/URL;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/okhttp/internal/http/u;->a(Ljava/net/URL;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/squareup/okhttp/Protocol;->b:Lcom/squareup/okhttp/Protocol;

    if-ne v1, p1, :cond_1

    new-instance v1, Lcom/squareup/okhttp/internal/spdy/d;

    sget-object v3, Lcom/squareup/okhttp/internal/spdy/d;->g:Lfi;

    invoke-direct {v1, v3, p2}, Lcom/squareup/okhttp/internal/spdy/d;-><init>(Lfi;Ljava/lang/String;)V

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/squareup/okhttp/internal/spdy/d;

    sget-object v3, Lcom/squareup/okhttp/internal/spdy/d;->f:Lfi;

    invoke-direct {v1, v3, v0}, Lcom/squareup/okhttp/internal/spdy/d;-><init>(Lfi;Ljava/lang/String;)V

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_0
    new-instance v0, Lcom/squareup/okhttp/internal/spdy/d;

    sget-object v1, Lcom/squareup/okhttp/internal/spdy/d;->d:Lfi;

    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/http/aa;->a()Ljava/net/URL;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Lcom/squareup/okhttp/internal/spdy/d;-><init>(Lfi;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v6, Ljava/util/LinkedHashSet;

    invoke-direct {v6}, Ljava/util/LinkedHashSet;-><init>()V

    move v1, v2

    :goto_1
    invoke-virtual {v4}, Lcom/squareup/okhttp/internal/http/f;->a()I

    move-result v0

    if-ge v1, v0, :cond_6

    invoke-virtual {v4, v1}, Lcom/squareup/okhttp/internal/http/f;->a(I)Ljava/lang/String;

    move-result-object v0

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0, v3}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lfi;->a(Ljava/lang/String;)Lfi;

    move-result-object v7

    invoke-virtual {v4, v1}, Lcom/squareup/okhttp/internal/http/f;->b(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {p1, v7}, Lcom/squareup/okhttp/internal/http/ar;->a(Lcom/squareup/okhttp/Protocol;Lfi;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    sget-object v1, Lcom/squareup/okhttp/Protocol;->a:Lcom/squareup/okhttp/Protocol;

    if-ne v1, p1, :cond_2

    new-instance v1, Lcom/squareup/okhttp/internal/spdy/d;

    sget-object v3, Lcom/squareup/okhttp/internal/spdy/d;->e:Lfi;

    invoke-direct {v1, v3, v0}, Lcom/squareup/okhttp/internal/spdy/d;-><init>(Lfi;Ljava/lang/String;)V

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_3
    sget-object v0, Lcom/squareup/okhttp/internal/spdy/d;->b:Lfi;

    invoke-virtual {v7, v0}, Lfi;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/squareup/okhttp/internal/spdy/d;->c:Lfi;

    invoke-virtual {v7, v0}, Lfi;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/squareup/okhttp/internal/spdy/d;->d:Lfi;

    invoke-virtual {v7, v0}, Lfi;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/squareup/okhttp/internal/spdy/d;->e:Lfi;

    invoke-virtual {v7, v0}, Lfi;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/squareup/okhttp/internal/spdy/d;->f:Lfi;

    invoke-virtual {v7, v0}, Lfi;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/squareup/okhttp/internal/spdy/d;->g:Lfi;

    invoke-virtual {v7, v0}, Lfi;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v6, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Lcom/squareup/okhttp/internal/spdy/d;

    invoke-direct {v0, v7, v8}, Lcom/squareup/okhttp/internal/spdy/d;-><init>(Lfi;Ljava/lang/String;)V

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_4
    move v3, v2

    :goto_3
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_0

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/okhttp/internal/spdy/d;

    iget-object v0, v0, Lcom/squareup/okhttp/internal/spdy/d;->h:Lfi;

    invoke-virtual {v0, v7}, Lfi;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/okhttp/internal/spdy/d;

    iget-object v0, v0, Lcom/squareup/okhttp/internal/spdy/d;->i:Lfi;

    invoke-virtual {v0}, Lfi;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v8}, Lcom/squareup/okhttp/internal/http/ar;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v8, Lcom/squareup/okhttp/internal/spdy/d;

    invoke-direct {v8, v7, v0}, Lcom/squareup/okhttp/internal/spdy/d;-><init>(Lfi;Ljava/lang/String;)V

    invoke-interface {v5, v3, v8}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    :cond_5
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    :cond_6
    return-object v5
.end method

.method private static a(Lcom/squareup/okhttp/Protocol;Lfi;)Z
    .locals 3

    const/4 v1, 0x1

    const/4 v0, 0x0

    sget-object v2, Lcom/squareup/okhttp/Protocol;->b:Lcom/squareup/okhttp/Protocol;

    if-ne p0, v2, :cond_2

    const-string/jumbo v2, "connection"

    invoke-virtual {p1, v2}, Lfi;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "host"

    invoke-virtual {p1, v2}, Lfi;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "keep-alive"

    invoke-virtual {p1, v2}, Lfi;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "proxy-connection"

    invoke-virtual {p1, v2}, Lfi;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "transfer-encoding"

    invoke-virtual {p1, v2}, Lfi;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    :goto_0
    return v0

    :cond_2
    sget-object v2, Lcom/squareup/okhttp/Protocol;->a:Lcom/squareup/okhttp/Protocol;

    if-ne p0, v2, :cond_4

    const-string/jumbo v2, "connection"

    invoke-virtual {p1, v2}, Lfi;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string/jumbo v2, "host"

    invoke-virtual {p1, v2}, Lfi;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string/jumbo v2, "keep-alive"

    invoke-virtual {p1, v2}, Lfi;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string/jumbo v2, "proxy-connection"

    invoke-virtual {p1, v2}, Lfi;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string/jumbo v2, "te"

    invoke-virtual {p1, v2}, Lfi;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string/jumbo v2, "transfer-encoding"

    invoke-virtual {p1, v2}, Lfi;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string/jumbo v2, "encoding"

    invoke-virtual {p1, v2}, Lfi;->b(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    const-string/jumbo v2, "upgrade"

    invoke-virtual {p1, v2}, Lfi;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0, p0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method


# virtual methods
.method public a(Lcom/squareup/okhttp/internal/http/aa;)Lga;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/squareup/okhttp/internal/http/ar;->b(Lcom/squareup/okhttp/internal/http/aa;)V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/ar;->c:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/spdy/ak;->f()Lga;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/net/CacheRequest;)Lgb;
    .locals 2

    new-instance v0, Lcom/squareup/okhttp/internal/http/as;

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/ar;->c:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-direct {v0, v1, p1}, Lcom/squareup/okhttp/internal/http/as;-><init>(Lcom/squareup/okhttp/internal/spdy/ak;Ljava/net/CacheRequest;)V

    return-object v0
.end method

.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/ar;->c:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/spdy/ak;->f()Lga;

    move-result-object v0

    invoke-interface {v0}, Lga;->close()V

    return-void
.end method

.method public a(Lcom/squareup/okhttp/internal/http/ap;)V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b()Lcom/squareup/okhttp/internal/http/aj;
    .locals 2

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/ar;->c:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/spdy/ak;->c()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/ar;->b:Lcom/squareup/okhttp/internal/spdy/y;

    invoke-virtual {v1}, Lcom/squareup/okhttp/internal/spdy/y;->a()Lcom/squareup/okhttp/Protocol;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/okhttp/internal/http/ar;->a(Ljava/util/List;Lcom/squareup/okhttp/Protocol;)Lcom/squareup/okhttp/internal/http/aj;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/squareup/okhttp/internal/http/aa;)V
    .locals 5

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/ar;->c:Lcom/squareup/okhttp/internal/spdy/ak;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/ar;->a:Lcom/squareup/okhttp/internal/http/u;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/u;->b()V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/ar;->a:Lcom/squareup/okhttp/internal/http/u;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/u;->c()Z

    move-result v0

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/ar;->a:Lcom/squareup/okhttp/internal/http/u;

    invoke-virtual {v2}, Lcom/squareup/okhttp/internal/http/u;->k()Lcom/squareup/okhttp/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/okhttp/c;->k()I

    move-result v2

    invoke-static {v2}, Lcom/squareup/okhttp/internal/http/af;->a(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/okhttp/internal/http/ar;->b:Lcom/squareup/okhttp/internal/spdy/y;

    iget-object v4, p0, Lcom/squareup/okhttp/internal/http/ar;->b:Lcom/squareup/okhttp/internal/spdy/y;

    invoke-virtual {v4}, Lcom/squareup/okhttp/internal/spdy/y;->a()Lcom/squareup/okhttp/Protocol;

    move-result-object v4

    invoke-static {p1, v4, v2}, Lcom/squareup/okhttp/internal/http/ar;->a(Lcom/squareup/okhttp/internal/http/aa;Lcom/squareup/okhttp/Protocol;Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v3, v2, v0, v1}, Lcom/squareup/okhttp/internal/spdy/y;->a(Ljava/util/List;ZZ)Lcom/squareup/okhttp/internal/spdy/ak;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/ar;->c:Lcom/squareup/okhttp/internal/spdy/ak;

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/ar;->c:Lcom/squareup/okhttp/internal/spdy/ak;

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/ar;->a:Lcom/squareup/okhttp/internal/http/u;

    iget-object v1, v1, Lcom/squareup/okhttp/internal/http/u;->a:Lcom/squareup/okhttp/j;

    invoke-virtual {v1}, Lcom/squareup/okhttp/j;->b()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/squareup/okhttp/internal/spdy/ak;->a(J)V

    goto :goto_0
.end method

.method public c()V
    .locals 0

    return-void
.end method

.method public d()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public e()V
    .locals 0

    return-void
.end method
