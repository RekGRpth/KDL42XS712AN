.class Lcom/squareup/okhttp/internal/http/o;
.super Lcom/squareup/okhttp/internal/http/m;
.source "Twttr"

# interfaces
.implements Lgb;


# instance fields
.field final synthetic d:Lcom/squareup/okhttp/internal/http/k;

.field private e:I

.field private f:Z

.field private final g:Lcom/squareup/okhttp/internal/http/u;


# direct methods
.method constructor <init>(Lcom/squareup/okhttp/internal/http/k;Ljava/net/CacheRequest;Lcom/squareup/okhttp/internal/http/u;)V
    .locals 1

    iput-object p1, p0, Lcom/squareup/okhttp/internal/http/o;->d:Lcom/squareup/okhttp/internal/http/k;

    invoke-direct {p0, p1, p2}, Lcom/squareup/okhttp/internal/http/m;-><init>(Lcom/squareup/okhttp/internal/http/k;Ljava/net/CacheRequest;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/squareup/okhttp/internal/http/o;->e:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/squareup/okhttp/internal/http/o;->f:Z

    iput-object p3, p0, Lcom/squareup/okhttp/internal/http/o;->g:Lcom/squareup/okhttp/internal/http/u;

    return-void
.end method

.method private b()V
    .locals 5

    const/4 v4, 0x0

    const/4 v2, -0x1

    const/4 v3, 0x1

    iget v0, p0, Lcom/squareup/okhttp/internal/http/o;->e:I

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/o;->d:Lcom/squareup/okhttp/internal/http/k;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/http/k;->f(Lcom/squareup/okhttp/internal/http/k;)Lfh;

    move-result-object v0

    invoke-interface {v0, v3}, Lfh;->a(Z)Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/o;->d:Lcom/squareup/okhttp/internal/http/k;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/http/k;->f(Lcom/squareup/okhttp/internal/http/k;)Lfh;

    move-result-object v0

    invoke-interface {v0, v3}, Lfh;->a(Z)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-eq v1, v2, :cond_1

    invoke-virtual {v0, v4, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_1
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x10

    invoke-static {v1, v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/squareup/okhttp/internal/http/o;->e:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    iget v0, p0, Lcom/squareup/okhttp/internal/http/o;->e:I

    if-nez v0, :cond_2

    iput-boolean v4, p0, Lcom/squareup/okhttp/internal/http/o;->f:Z

    new-instance v0, Lcom/squareup/okhttp/internal/http/h;

    invoke-direct {v0}, Lcom/squareup/okhttp/internal/http/h;-><init>()V

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/o;->d:Lcom/squareup/okhttp/internal/http/k;

    invoke-virtual {v1, v0}, Lcom/squareup/okhttp/internal/http/k;->a(Lcom/squareup/okhttp/internal/http/h;)V

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/o;->g:Lcom/squareup/okhttp/internal/http/u;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/h;->a()Lcom/squareup/okhttp/internal/http/f;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/squareup/okhttp/internal/http/u;->a(Lcom/squareup/okhttp/internal/http/f;)V

    invoke-virtual {p0, v3}, Lcom/squareup/okhttp/internal/http/o;->a(Z)V

    :cond_2
    return-void

    :catch_0
    move-exception v1

    new-instance v1, Ljava/net/ProtocolException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Expected a hex chunk size but was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public b(Lfo;J)J
    .locals 5

    const-wide/16 v0, -0x1

    const-wide/16 v2, 0x0

    cmp-long v2, p2, v2

    if-gez v2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "byteCount < 0: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-boolean v2, p0, Lcom/squareup/okhttp/internal/http/o;->b:Z

    if-eqz v2, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-boolean v2, p0, Lcom/squareup/okhttp/internal/http/o;->f:Z

    if-nez v2, :cond_3

    :cond_2
    :goto_0
    return-wide v0

    :cond_3
    iget v2, p0, Lcom/squareup/okhttp/internal/http/o;->e:I

    if-eqz v2, :cond_4

    iget v2, p0, Lcom/squareup/okhttp/internal/http/o;->e:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_5

    :cond_4
    invoke-direct {p0}, Lcom/squareup/okhttp/internal/http/o;->b()V

    iget-boolean v2, p0, Lcom/squareup/okhttp/internal/http/o;->f:Z

    if-eqz v2, :cond_2

    :cond_5
    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/o;->d:Lcom/squareup/okhttp/internal/http/k;

    invoke-static {v2}, Lcom/squareup/okhttp/internal/http/k;->f(Lcom/squareup/okhttp/internal/http/k;)Lfh;

    move-result-object v2

    iget v3, p0, Lcom/squareup/okhttp/internal/http/o;->e:I

    int-to-long v3, v3

    invoke-static {p2, p3, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v3

    invoke-interface {v2, p1, v3, v4}, Lfh;->b(Lfo;J)J

    move-result-wide v2

    cmp-long v0, v2, v0

    if-nez v0, :cond_6

    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/http/o;->a()V

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "unexpected end of stream"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    iget v0, p0, Lcom/squareup/okhttp/internal/http/o;->e:I

    int-to-long v0, v0

    sub-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, Lcom/squareup/okhttp/internal/http/o;->e:I

    invoke-virtual {p0, p1, v2, v3}, Lcom/squareup/okhttp/internal/http/o;->a(Lfo;J)V

    move-wide v0, v2

    goto :goto_0
.end method

.method public close()V
    .locals 2

    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/http/o;->b:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/http/o;->f:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/o;->d:Lcom/squareup/okhttp/internal/http/k;

    const/16 v1, 0x64

    invoke-virtual {v0, p0, v1}, Lcom/squareup/okhttp/internal/http/k;->a(Lgb;I)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/http/o;->a()V

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/squareup/okhttp/internal/http/o;->b:Z

    goto :goto_0
.end method
