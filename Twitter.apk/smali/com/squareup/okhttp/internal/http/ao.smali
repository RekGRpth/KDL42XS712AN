.class final Lcom/squareup/okhttp/internal/http/ao;
.super Lcom/squareup/okhttp/internal/http/d;
.source "Twttr"


# instance fields
.field private final a:Lcom/squareup/okhttp/internal/http/an;


# direct methods
.method public constructor <init>(Lcom/squareup/okhttp/internal/http/an;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/okhttp/internal/http/d;-><init>(Ljava/net/HttpURLConnection;)V

    iput-object p1, p0, Lcom/squareup/okhttp/internal/http/ao;->a:Lcom/squareup/okhttp/internal/http/an;

    return-void
.end method


# virtual methods
.method protected a()Lcom/squareup/okhttp/f;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/ao;->a:Lcom/squareup/okhttp/internal/http/an;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/http/an;->a(Lcom/squareup/okhttp/internal/http/an;)Lcom/squareup/okhttp/internal/http/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/ag;->f()Lcom/squareup/okhttp/f;

    move-result-object v0

    return-object v0
.end method

.method public getHostnameVerifier()Ljavax/net/ssl/HostnameVerifier;
    .locals 1

    invoke-static {}, Lcom/squareup/okhttp/internal/http/al;->e()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public getSSLSocketFactory()Ljavax/net/ssl/SSLSocketFactory;
    .locals 1

    invoke-static {}, Lcom/squareup/okhttp/internal/http/al;->e()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public setFixedLengthStreamingMode(J)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/ao;->a:Lcom/squareup/okhttp/internal/http/an;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/okhttp/internal/http/an;->setFixedLengthStreamingMode(J)V

    return-void
.end method

.method public setHostnameVerifier(Ljavax/net/ssl/HostnameVerifier;)V
    .locals 1

    invoke-static {}, Lcom/squareup/okhttp/internal/http/al;->b()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public setSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V
    .locals 1

    invoke-static {}, Lcom/squareup/okhttp/internal/http/al;->b()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method
