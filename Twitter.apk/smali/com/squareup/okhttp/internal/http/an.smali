.class final Lcom/squareup/okhttp/internal/http/an;
.super Ljava/net/HttpURLConnection;
.source "Twttr"


# instance fields
.field private final a:Lcom/squareup/okhttp/internal/http/aa;

.field private final b:Lcom/squareup/okhttp/internal/http/ag;


# direct methods
.method public constructor <init>(Lcom/squareup/okhttp/internal/http/ag;)V
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p1}, Lcom/squareup/okhttp/internal/http/ag;->a()Lcom/squareup/okhttp/internal/http/aa;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/okhttp/internal/http/aa;->a()Ljava/net/URL;

    move-result-object v1

    invoke-direct {p0, v1}, Ljava/net/HttpURLConnection;-><init>(Ljava/net/URL;)V

    invoke-virtual {p1}, Lcom/squareup/okhttp/internal/http/ag;->a()Lcom/squareup/okhttp/internal/http/aa;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/okhttp/internal/http/an;->a:Lcom/squareup/okhttp/internal/http/aa;

    iput-object p1, p0, Lcom/squareup/okhttp/internal/http/an;->b:Lcom/squareup/okhttp/internal/http/ag;

    iput-boolean v0, p0, Lcom/squareup/okhttp/internal/http/an;->connected:Z

    invoke-virtual {p1}, Lcom/squareup/okhttp/internal/http/ag;->h()Lcom/squareup/okhttp/internal/http/ai;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/squareup/okhttp/internal/http/an;->doOutput:Z

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/an;->a:Lcom/squareup/okhttp/internal/http/aa;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/aa;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/an;->method:Ljava/lang/String;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/squareup/okhttp/internal/http/an;)Lcom/squareup/okhttp/internal/http/ag;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/an;->b:Lcom/squareup/okhttp/internal/http/ag;

    return-object v0
.end method


# virtual methods
.method public addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-static {}, Lcom/squareup/okhttp/internal/http/al;->b()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public connect()V
    .locals 1

    invoke-static {}, Lcom/squareup/okhttp/internal/http/al;->b()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public disconnect()V
    .locals 1

    invoke-static {}, Lcom/squareup/okhttp/internal/http/al;->b()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public getAllowUserInteraction()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getConnectTimeout()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getContent()Ljava/lang/Object;
    .locals 1

    invoke-static {}, Lcom/squareup/okhttp/internal/http/al;->d()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public getContent([Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1

    invoke-static {}, Lcom/squareup/okhttp/internal/http/al;->d()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public getDefaultUseCaches()Z
    .locals 1

    invoke-super {p0}, Ljava/net/HttpURLConnection;->getDefaultUseCaches()Z

    move-result v0

    return v0
.end method

.method public getDoInput()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public getDoOutput()Z
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/an;->a:Lcom/squareup/okhttp/internal/http/aa;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/aa;->e()Lcom/squareup/okhttp/internal/http/ac;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getErrorStream()Ljava/io/InputStream;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getHeaderField(I)Ljava/lang/String;
    .locals 3

    if-gez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid header index: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/an;->b:Lcom/squareup/okhttp/internal/http/ag;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/ag;->b()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/an;->b:Lcom/squareup/okhttp/internal/http/ag;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/ag;->g()Lcom/squareup/okhttp/internal/http/f;

    move-result-object v0

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/internal/http/f;->b(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getHeaderField(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/an;->b:Lcom/squareup/okhttp/internal/http/ag;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/ag;->b()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/an;->b:Lcom/squareup/okhttp/internal/http/ag;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/ag;->g()Lcom/squareup/okhttp/internal/http/f;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/okhttp/internal/http/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getHeaderFieldKey(I)Ljava/lang/String;
    .locals 3

    if-gez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid header index: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/an;->b:Lcom/squareup/okhttp/internal/http/ag;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/ag;->g()Lcom/squareup/okhttp/internal/http/f;

    move-result-object v0

    add-int/lit8 v1, p1, -0x1

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/internal/http/f;->a(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getHeaderFields()Ljava/util/Map;
    .locals 2

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/an;->b:Lcom/squareup/okhttp/internal/http/ag;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/ag;->g()Lcom/squareup/okhttp/internal/http/f;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/an;->b:Lcom/squareup/okhttp/internal/http/ag;

    invoke-virtual {v1}, Lcom/squareup/okhttp/internal/http/ag;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/squareup/okhttp/internal/http/y;->a(Lcom/squareup/okhttp/internal/http/f;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getIfModifiedSince()J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getInputStream()Ljava/io/InputStream;
    .locals 1

    invoke-static {}, Lcom/squareup/okhttp/internal/http/al;->d()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public getInstanceFollowRedirects()Z
    .locals 1

    invoke-super {p0}, Ljava/net/HttpURLConnection;->getInstanceFollowRedirects()Z

    move-result v0

    return v0
.end method

.method public getOutputStream()Ljava/io/OutputStream;
    .locals 1

    invoke-static {}, Lcom/squareup/okhttp/internal/http/al;->b()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public getReadTimeout()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getRequestMethod()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/an;->a:Lcom/squareup/okhttp/internal/http/aa;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/aa;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRequestProperties()Ljava/util/Map;
    .locals 1

    invoke-static {}, Lcom/squareup/okhttp/internal/http/al;->c()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public getRequestProperty(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/an;->a:Lcom/squareup/okhttp/internal/http/aa;

    invoke-virtual {v0, p1}, Lcom/squareup/okhttp/internal/http/aa;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getResponseCode()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/an;->b:Lcom/squareup/okhttp/internal/http/ag;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/ag;->c()I

    move-result v0

    return v0
.end method

.method public getResponseMessage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/an;->b:Lcom/squareup/okhttp/internal/http/ag;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/ag;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUseCaches()Z
    .locals 1

    invoke-super {p0}, Ljava/net/HttpURLConnection;->getUseCaches()Z

    move-result v0

    return v0
.end method

.method public setAllowUserInteraction(Z)V
    .locals 1

    invoke-static {}, Lcom/squareup/okhttp/internal/http/al;->b()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public setChunkedStreamingMode(I)V
    .locals 1

    invoke-static {}, Lcom/squareup/okhttp/internal/http/al;->b()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public setConnectTimeout(I)V
    .locals 1

    invoke-static {}, Lcom/squareup/okhttp/internal/http/al;->b()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public setDefaultUseCaches(Z)V
    .locals 0

    invoke-super {p0, p1}, Ljava/net/HttpURLConnection;->setDefaultUseCaches(Z)V

    return-void
.end method

.method public setDoInput(Z)V
    .locals 1

    invoke-static {}, Lcom/squareup/okhttp/internal/http/al;->b()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public setDoOutput(Z)V
    .locals 1

    invoke-static {}, Lcom/squareup/okhttp/internal/http/al;->b()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public setFixedLengthStreamingMode(I)V
    .locals 1

    invoke-static {}, Lcom/squareup/okhttp/internal/http/al;->b()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public setFixedLengthStreamingMode(J)V
    .locals 1

    invoke-static {}, Lcom/squareup/okhttp/internal/http/al;->b()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public setIfModifiedSince(J)V
    .locals 1

    invoke-static {}, Lcom/squareup/okhttp/internal/http/al;->b()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public setInstanceFollowRedirects(Z)V
    .locals 1

    invoke-static {}, Lcom/squareup/okhttp/internal/http/al;->b()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public setReadTimeout(I)V
    .locals 1

    invoke-static {}, Lcom/squareup/okhttp/internal/http/al;->b()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public setRequestMethod(Ljava/lang/String;)V
    .locals 1

    invoke-static {}, Lcom/squareup/okhttp/internal/http/al;->b()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-static {}, Lcom/squareup/okhttp/internal/http/al;->b()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public setUseCaches(Z)V
    .locals 1

    invoke-static {}, Lcom/squareup/okhttp/internal/http/al;->b()Ljava/lang/RuntimeException;

    move-result-object v0

    throw v0
.end method

.method public usingProxy()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
