.class final Lcom/squareup/okhttp/internal/http/n;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lga;


# instance fields
.field final synthetic a:Lcom/squareup/okhttp/internal/http/k;

.field private final b:[B

.field private c:Z


# direct methods
.method private constructor <init>(Lcom/squareup/okhttp/internal/http/k;)V
    .locals 1

    iput-object p1, p0, Lcom/squareup/okhttp/internal/http/n;->a:Lcom/squareup/okhttp/internal/http/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x12

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/n;->b:[B

    return-void

    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0x0t
        0xdt
        0xat
    .end array-data
.end method

.method synthetic constructor <init>(Lcom/squareup/okhttp/internal/http/k;Lcom/squareup/okhttp/internal/http/l;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/okhttp/internal/http/n;-><init>(Lcom/squareup/okhttp/internal/http/k;)V

    return-void
.end method

.method private a(J)V
    .locals 5

    const/16 v0, 0x10

    :cond_0
    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/n;->b:[B

    add-int/lit8 v0, v0, -0x1

    invoke-static {}, Lcom/squareup/okhttp/internal/http/k;->i()[B

    move-result-object v2

    const-wide/16 v3, 0xf

    and-long/2addr v3, p1

    long-to-int v3, v3

    aget-byte v2, v2, v3

    aput-byte v2, v1, v0

    const/4 v1, 0x4

    ushr-long/2addr p1, v1

    const-wide/16 v1, 0x0

    cmp-long v1, p1, v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/n;->a:Lcom/squareup/okhttp/internal/http/k;

    invoke-static {v1}, Lcom/squareup/okhttp/internal/http/k;->a(Lcom/squareup/okhttp/internal/http/k;)Lfg;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/n;->b:[B

    iget-object v3, p0, Lcom/squareup/okhttp/internal/http/n;->b:[B

    array-length v3, v3

    sub-int/2addr v3, v0

    invoke-interface {v1, v2, v0, v3}, Lfg;->a([BII)Lfg;

    return-void
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/http/n;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/n;->a:Lcom/squareup/okhttp/internal/http/k;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/http/k;->a(Lcom/squareup/okhttp/internal/http/k;)Lfg;

    move-result-object v0

    invoke-interface {v0}, Lfg;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lfo;J)V
    .locals 2

    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/http/n;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const-wide/16 v0, 0x0

    cmp-long v0, p2, v0

    if-nez v0, :cond_1

    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p2, p3}, Lcom/squareup/okhttp/internal/http/n;->a(J)V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/n;->a:Lcom/squareup/okhttp/internal/http/k;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/http/k;->a(Lcom/squareup/okhttp/internal/http/k;)Lfg;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lfg;->a(Lfo;J)V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/n;->a:Lcom/squareup/okhttp/internal/http/k;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/http/k;->a(Lcom/squareup/okhttp/internal/http/k;)Lfg;

    move-result-object v0

    const-string/jumbo v1, "\r\n"

    invoke-interface {v0, v1}, Lfg;->a(Ljava/lang/String;)Lfg;

    goto :goto_0
.end method

.method public declared-synchronized close()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/http/n;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/squareup/okhttp/internal/http/n;->c:Z

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/n;->a:Lcom/squareup/okhttp/internal/http/k;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/http/k;->a(Lcom/squareup/okhttp/internal/http/k;)Lfg;

    move-result-object v0

    invoke-static {}, Lcom/squareup/okhttp/internal/http/k;->h()[B

    move-result-object v1

    invoke-interface {v0, v1}, Lfg;->a([B)Lfg;

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/n;->a:Lcom/squareup/okhttp/internal/http/k;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/squareup/okhttp/internal/http/k;->a(Lcom/squareup/okhttp/internal/http/k;I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
