.class public final Lcom/squareup/okhttp/j;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Cloneable;
.implements Ljava/net/URLStreamHandlerFactory;


# instance fields
.field private final a:Ljava/util/Map;

.field private final b:Lcom/squareup/okhttp/n;

.field private c:Ljava/net/Proxy;

.field private d:Ljava/util/List;

.field private e:Ljava/net/ProxySelector;

.field private f:Ljava/net/CookieHandler;

.field private g:Lcom/squareup/okhttp/l;

.field private h:Ljavax/net/ssl/SSLSocketFactory;

.field private i:Ljavax/net/ssl/HostnameVerifier;

.field private j:Lcom/squareup/okhttp/g;

.field private k:Lcom/squareup/okhttp/d;

.field private l:Z

.field private m:I

.field private n:I


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/squareup/okhttp/j;->l:Z

    new-instance v0, Ljava/util/HashMap;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/squareup/okhttp/j;->a:Ljava/util/Map;

    new-instance v0, Lcom/squareup/okhttp/n;

    invoke-direct {v0}, Lcom/squareup/okhttp/n;-><init>()V

    iput-object v0, p0, Lcom/squareup/okhttp/j;->b:Lcom/squareup/okhttp/n;

    return-void
.end method

.method private a(Ljava/net/ResponseCache;)Lcom/squareup/okhttp/l;
    .locals 1

    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/squareup/okhttp/l;

    if-eqz v0, :cond_1

    :cond_0
    check-cast p1, Lcom/squareup/okhttp/l;

    :goto_0
    return-object p1

    :cond_1
    new-instance v0, Lcom/squareup/okhttp/internal/http/al;

    invoke-direct {v0, p1}, Lcom/squareup/okhttp/internal/http/al;-><init>(Ljava/net/ResponseCache;)V

    move-object p1, v0

    goto :goto_0
.end method

.method private declared-synchronized p()Ljavax/net/ssl/SSLSocketFactory;
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/squareup/okhttp/j;->h:Ljavax/net/ssl/SSLSocketFactory;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :try_start_1
    const-string/jumbo v0, "TLS"

    invoke-static {v0}, Ljavax/net/ssl/SSLContext;->getInstance(Ljava/lang/String;)Ljavax/net/ssl/SSLContext;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Ljavax/net/ssl/SSLContext;->init([Ljavax/net/ssl/KeyManager;[Ljavax/net/ssl/TrustManager;Ljava/security/SecureRandom;)V

    invoke-virtual {v0}, Ljavax/net/ssl/SSLContext;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/j;->h:Ljavax/net/ssl/SSLSocketFactory;
    :try_end_1
    .catch Ljava/security/GeneralSecurityException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/squareup/okhttp/j;->h:Ljavax/net/ssl/SSLSocketFactory;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v0

    :catch_0
    move-exception v0

    :try_start_3
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/squareup/okhttp/j;->m:I

    return v0
.end method

.method public a(Lcom/squareup/okhttp/l;)Lcom/squareup/okhttp/j;
    .locals 0

    iput-object p1, p0, Lcom/squareup/okhttp/j;->g:Lcom/squareup/okhttp/l;

    return-object p0
.end method

.method public a(Ljava/net/CookieHandler;)Lcom/squareup/okhttp/j;
    .locals 0

    iput-object p1, p0, Lcom/squareup/okhttp/j;->f:Ljava/net/CookieHandler;

    return-object p0
.end method

.method public a(Ljava/net/Proxy;)Lcom/squareup/okhttp/j;
    .locals 0

    iput-object p1, p0, Lcom/squareup/okhttp/j;->c:Ljava/net/Proxy;

    return-object p0
.end method

.method public a(Ljava/util/List;)Lcom/squareup/okhttp/j;
    .locals 4

    invoke-static {p1}, Lfd;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sget-object v1, Lcom/squareup/okhttp/Protocol;->c:Lcom/squareup/okhttp/Protocol;

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "protocols doesn\'t contain http/1.1: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "protocols must not contain null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-static {v0}, Lfd;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/j;->d:Ljava/util/List;

    return-object p0
.end method

.method public a(Ljavax/net/ssl/HostnameVerifier;)Lcom/squareup/okhttp/j;
    .locals 0

    iput-object p1, p0, Lcom/squareup/okhttp/j;->i:Ljavax/net/ssl/HostnameVerifier;

    return-object p0
.end method

.method public a(Ljavax/net/ssl/SSLSocketFactory;)Lcom/squareup/okhttp/j;
    .locals 0

    iput-object p1, p0, Lcom/squareup/okhttp/j;->h:Ljavax/net/ssl/SSLSocketFactory;

    return-object p0
.end method

.method public declared-synchronized a(Lcom/squareup/okhttp/a;)Ljava/lang/Object;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/squareup/okhttp/j;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/okhttp/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/squareup/okhttp/j;->a:Ljava/util/Map;

    invoke-interface {v0, p1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, p1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Ljava/net/URL;)Ljava/net/HttpURLConnection;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/j;->c:Ljava/net/Proxy;

    invoke-virtual {p0, p1, v0}, Lcom/squareup/okhttp/j;->a(Ljava/net/URL;Ljava/net/Proxy;)Ljava/net/HttpURLConnection;

    move-result-object v0

    return-object v0
.end method

.method a(Ljava/net/URL;Ljava/net/Proxy;)Ljava/net/HttpURLConnection;
    .locals 4

    invoke-virtual {p1}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/okhttp/j;->n()Lcom/squareup/okhttp/j;

    move-result-object v1

    iput-object p2, v1, Lcom/squareup/okhttp/j;->c:Ljava/net/Proxy;

    const-string/jumbo v2, "http"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;

    invoke-direct {v0, p1, v1}, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;-><init>(Ljava/net/URL;Lcom/squareup/okhttp/j;)V

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v2, "https"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v0, Lcom/squareup/okhttp/internal/http/x;

    invoke-direct {v0, p1, v1}, Lcom/squareup/okhttp/internal/http/x;-><init>(Ljava/net/URL;Lcom/squareup/okhttp/j;)V

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Unexpected protocol: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public a(JLjava/util/concurrent/TimeUnit;)V
    .locals 4

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "timeout < 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "unit == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-lez v2, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Timeout too large."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    long-to-int v0, v0

    iput v0, p0, Lcom/squareup/okhttp/j;->m:I

    return-void
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lcom/squareup/okhttp/j;->n:I

    return v0
.end method

.method public b(JLjava/util/concurrent/TimeUnit;)V
    .locals 4

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "timeout < 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "unit == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-lez v2, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Timeout too large."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    long-to-int v0, v0

    iput v0, p0, Lcom/squareup/okhttp/j;->n:I

    return-void
.end method

.method public declared-synchronized b(Lcom/squareup/okhttp/a;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/squareup/okhttp/j;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()Ljava/net/Proxy;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/j;->c:Ljava/net/Proxy;

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/okhttp/j;->o()Lcom/squareup/okhttp/j;

    move-result-object v0

    return-object v0
.end method

.method public createURLStreamHandler(Ljava/lang/String;)Ljava/net/URLStreamHandler;
    .locals 1

    const-string/jumbo v0, "http"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "https"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/squareup/okhttp/k;

    invoke-direct {v0, p0, p1}, Lcom/squareup/okhttp/k;-><init>(Lcom/squareup/okhttp/j;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public d()Ljava/net/ProxySelector;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/j;->e:Ljava/net/ProxySelector;

    return-object v0
.end method

.method public e()Ljava/net/CookieHandler;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/j;->f:Ljava/net/CookieHandler;

    return-object v0
.end method

.method public f()Lcom/squareup/okhttp/l;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/j;->g:Lcom/squareup/okhttp/l;

    return-object v0
.end method

.method public g()Ljavax/net/ssl/SSLSocketFactory;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/j;->h:Ljavax/net/ssl/SSLSocketFactory;

    return-object v0
.end method

.method public h()Ljavax/net/ssl/HostnameVerifier;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/j;->i:Ljavax/net/ssl/HostnameVerifier;

    return-object v0
.end method

.method public i()Lcom/squareup/okhttp/g;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/j;->j:Lcom/squareup/okhttp/g;

    return-object v0
.end method

.method public j()Lcom/squareup/okhttp/d;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/j;->k:Lcom/squareup/okhttp/d;

    return-object v0
.end method

.method public k()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/okhttp/j;->l:Z

    return v0
.end method

.method public l()Lcom/squareup/okhttp/n;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/j;->b:Lcom/squareup/okhttp/n;

    return-object v0
.end method

.method public m()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/j;->d:Ljava/util/List;

    return-object v0
.end method

.method n()Lcom/squareup/okhttp/j;
    .locals 2

    invoke-virtual {p0}, Lcom/squareup/okhttp/j;->o()Lcom/squareup/okhttp/j;

    move-result-object v0

    iget-object v1, v0, Lcom/squareup/okhttp/j;->e:Ljava/net/ProxySelector;

    if-nez v1, :cond_0

    invoke-static {}, Ljava/net/ProxySelector;->getDefault()Ljava/net/ProxySelector;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/okhttp/j;->e:Ljava/net/ProxySelector;

    :cond_0
    iget-object v1, v0, Lcom/squareup/okhttp/j;->f:Ljava/net/CookieHandler;

    if-nez v1, :cond_1

    invoke-static {}, Ljava/net/CookieHandler;->getDefault()Ljava/net/CookieHandler;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/okhttp/j;->f:Ljava/net/CookieHandler;

    :cond_1
    iget-object v1, v0, Lcom/squareup/okhttp/j;->g:Lcom/squareup/okhttp/l;

    if-nez v1, :cond_2

    invoke-static {}, Ljava/net/ResponseCache;->getDefault()Ljava/net/ResponseCache;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/squareup/okhttp/j;->a(Ljava/net/ResponseCache;)Lcom/squareup/okhttp/l;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/okhttp/j;->g:Lcom/squareup/okhttp/l;

    :cond_2
    iget-object v1, v0, Lcom/squareup/okhttp/j;->h:Ljavax/net/ssl/SSLSocketFactory;

    if-nez v1, :cond_3

    invoke-direct {p0}, Lcom/squareup/okhttp/j;->p()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/okhttp/j;->h:Ljavax/net/ssl/SSLSocketFactory;

    :cond_3
    iget-object v1, v0, Lcom/squareup/okhttp/j;->i:Ljavax/net/ssl/HostnameVerifier;

    if-nez v1, :cond_4

    sget-object v1, Lge;->a:Lge;

    iput-object v1, v0, Lcom/squareup/okhttp/j;->i:Ljavax/net/ssl/HostnameVerifier;

    :cond_4
    iget-object v1, v0, Lcom/squareup/okhttp/j;->j:Lcom/squareup/okhttp/g;

    if-nez v1, :cond_5

    sget-object v1, Lcom/squareup/okhttp/internal/http/i;->a:Lcom/squareup/okhttp/g;

    iput-object v1, v0, Lcom/squareup/okhttp/j;->j:Lcom/squareup/okhttp/g;

    :cond_5
    iget-object v1, v0, Lcom/squareup/okhttp/j;->k:Lcom/squareup/okhttp/d;

    if-nez v1, :cond_6

    invoke-static {}, Lcom/squareup/okhttp/d;->a()Lcom/squareup/okhttp/d;

    move-result-object v1

    iput-object v1, v0, Lcom/squareup/okhttp/j;->k:Lcom/squareup/okhttp/d;

    :cond_6
    iget-object v1, v0, Lcom/squareup/okhttp/j;->d:Ljava/util/List;

    if-nez v1, :cond_7

    sget-object v1, Lfd;->f:Ljava/util/List;

    iput-object v1, v0, Lcom/squareup/okhttp/j;->d:Ljava/util/List;

    :cond_7
    return-object v0
.end method

.method public o()Lcom/squareup/okhttp/j;
    .locals 1

    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/okhttp/j;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
.end method
