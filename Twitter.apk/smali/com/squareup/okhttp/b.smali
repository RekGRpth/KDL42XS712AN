.class public final Lcom/squareup/okhttp/b;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Z

.field private final b:Z

.field private final c:I

.field private final d:I

.field private final e:Z

.field private final f:Z

.field private final g:I

.field private final h:I

.field private final i:Z


# direct methods
.method private constructor <init>(ZZIIZZIIZ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lcom/squareup/okhttp/b;->a:Z

    iput-boolean p2, p0, Lcom/squareup/okhttp/b;->b:Z

    iput p3, p0, Lcom/squareup/okhttp/b;->c:I

    iput p4, p0, Lcom/squareup/okhttp/b;->d:I

    iput-boolean p5, p0, Lcom/squareup/okhttp/b;->e:Z

    iput-boolean p6, p0, Lcom/squareup/okhttp/b;->f:Z

    iput p7, p0, Lcom/squareup/okhttp/b;->g:I

    iput p8, p0, Lcom/squareup/okhttp/b;->h:I

    iput-boolean p9, p0, Lcom/squareup/okhttp/b;->i:Z

    return-void
.end method

.method public static a(Lcom/squareup/okhttp/internal/http/f;)Lcom/squareup/okhttp/b;
    .locals 17

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, -0x1

    const/4 v5, -0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, -0x1

    const/4 v9, -0x1

    const/4 v10, 0x0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/squareup/okhttp/internal/http/f;->a()I

    move-result v11

    if-ge v1, v11, :cond_d

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/internal/http/f;->a(I)Ljava/lang/String;

    move-result-object v11

    const-string/jumbo v12, "Cache-Control"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/internal/http/f;->a(I)Ljava/lang/String;

    move-result-object v11

    const-string/jumbo v12, "Pragma"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_0

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/internal/http/f;->b(I)Ljava/lang/String;

    move-result-object v13

    const/4 v11, 0x0

    move/from16 v16, v11

    move v11, v2

    move/from16 v2, v16

    :cond_1
    :goto_2
    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v12

    if-ge v2, v12, :cond_e

    const-string/jumbo v12, "=,;"

    invoke-static {v13, v2, v12}, Lcom/squareup/okhttp/internal/http/e;->a(Ljava/lang/String;ILjava/lang/String;)I

    move-result v12

    invoke-virtual {v13, v2, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v2

    if-eq v12, v2, :cond_2

    invoke-virtual {v13, v12}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v15, 0x2c

    if-eq v2, v15, :cond_2

    invoke-virtual {v13, v12}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v15, 0x3b

    if-ne v2, v15, :cond_3

    :cond_2
    add-int/lit8 v12, v12, 0x1

    const/4 v2, 0x0

    move-object/from16 v16, v2

    move v2, v12

    move-object/from16 v12, v16

    :goto_3
    const-string/jumbo v15, "no-cache"

    invoke-virtual {v15, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_5

    const/4 v11, 0x1

    goto :goto_2

    :cond_3
    add-int/lit8 v2, v12, 0x1

    invoke-static {v13, v2}, Lcom/squareup/okhttp/internal/http/e;->a(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v12

    if-ge v2, v12, :cond_4

    invoke-virtual {v13, v2}, Ljava/lang/String;->charAt(I)C

    move-result v12

    const/16 v15, 0x22

    if-ne v12, v15, :cond_4

    add-int/lit8 v2, v2, 0x1

    const-string/jumbo v12, "\""

    invoke-static {v13, v2, v12}, Lcom/squareup/okhttp/internal/http/e;->a(Ljava/lang/String;ILjava/lang/String;)I

    move-result v12

    invoke-virtual {v13, v2, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    add-int/lit8 v12, v12, 0x1

    move-object/from16 v16, v2

    move v2, v12

    move-object/from16 v12, v16

    goto :goto_3

    :cond_4
    const-string/jumbo v12, ",;"

    invoke-static {v13, v2, v12}, Lcom/squareup/okhttp/internal/http/e;->a(Ljava/lang/String;ILjava/lang/String;)I

    move-result v12

    invoke-virtual {v13, v2, v12}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v16, v2

    move v2, v12

    move-object/from16 v12, v16

    goto :goto_3

    :cond_5
    const-string/jumbo v15, "no-store"

    invoke-virtual {v15, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_6

    const/4 v3, 0x1

    goto/16 :goto_2

    :cond_6
    const-string/jumbo v15, "max-age"

    invoke-virtual {v15, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_7

    invoke-static {v12}, Lcom/squareup/okhttp/internal/http/e;->a(Ljava/lang/String;)I

    move-result v4

    goto/16 :goto_2

    :cond_7
    const-string/jumbo v15, "s-maxage"

    invoke-virtual {v15, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_8

    invoke-static {v12}, Lcom/squareup/okhttp/internal/http/e;->a(Ljava/lang/String;)I

    move-result v5

    goto/16 :goto_2

    :cond_8
    const-string/jumbo v15, "public"

    invoke-virtual {v15, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_9

    const/4 v6, 0x1

    goto/16 :goto_2

    :cond_9
    const-string/jumbo v15, "must-revalidate"

    invoke-virtual {v15, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_a

    const/4 v7, 0x1

    goto/16 :goto_2

    :cond_a
    const-string/jumbo v15, "max-stale"

    invoke-virtual {v15, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_b

    invoke-static {v12}, Lcom/squareup/okhttp/internal/http/e;->a(Ljava/lang/String;)I

    move-result v8

    goto/16 :goto_2

    :cond_b
    const-string/jumbo v15, "min-fresh"

    invoke-virtual {v15, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_c

    invoke-static {v12}, Lcom/squareup/okhttp/internal/http/e;->a(Ljava/lang/String;)I

    move-result v9

    goto/16 :goto_2

    :cond_c
    const-string/jumbo v12, "only-if-cached"

    invoke-virtual {v12, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_1

    const/4 v10, 0x1

    goto/16 :goto_2

    :cond_d
    new-instance v1, Lcom/squareup/okhttp/b;

    invoke-direct/range {v1 .. v10}, Lcom/squareup/okhttp/b;-><init>(ZZIIZZIIZ)V

    return-object v1

    :cond_e
    move v2, v11

    goto/16 :goto_1
.end method


# virtual methods
.method public a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/okhttp/b;->a:Z

    return v0
.end method

.method public b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/okhttp/b;->b:Z

    return v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lcom/squareup/okhttp/b;->c:I

    return v0
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lcom/squareup/okhttp/b;->d:I

    return v0
.end method

.method public e()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/okhttp/b;->e:Z

    return v0
.end method

.method public f()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/okhttp/b;->f:Z

    return v0
.end method

.method public g()I
    .locals 1

    iget v0, p0, Lcom/squareup/okhttp/b;->g:I

    return v0
.end method

.method public h()I
    .locals 1

    iget v0, p0, Lcom/squareup/okhttp/b;->h:I

    return v0
.end method

.method public i()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/okhttp/b;->i:Z

    return v0
.end method
