.class public abstract Lcom/samsung/vip/engine/e;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[I


# instance fields
.field private c:Ljava/util/HashMap;

.field private d:Z

.field private e:Ljava/util/LinkedList;

.field private f:Ljava/util/LinkedList;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v3, 0x1

    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "eng"

    aput-object v1, v0, v5

    const-string/jumbo v1, "kor"

    aput-object v1, v0, v3

    const/4 v1, 0x2

    const-string/jumbo v2, "chn"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/vip/engine/e;->a:[Ljava/lang/String;

    new-array v0, v4, [I

    aput v4, v0, v5

    aput v3, v0, v3

    sput-object v0, Lcom/samsung/vip/engine/e;->b:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/samsung/vip/engine/e;->c:Ljava/util/HashMap;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/vip/engine/e;->d:Z

    iput-object v1, p0, Lcom/samsung/vip/engine/e;->e:Ljava/util/LinkedList;

    iput-object v1, p0, Lcom/samsung/vip/engine/e;->f:Ljava/util/LinkedList;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/samsung/vip/engine/e;->e:Ljava/util/LinkedList;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/samsung/vip/engine/e;->f:Ljava/util/LinkedList;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/vip/engine/e;->c:Ljava/util/HashMap;

    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/samsung/vip/engine/e;->a:[Ljava/lang/String;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    return-void

    :cond_0
    iget-object v1, p0, Lcom/samsung/vip/engine/e;->c:Ljava/util/HashMap;

    sget-object v2, Lcom/samsung/vip/engine/e;->a:[Ljava/lang/String;

    aget-object v2, v2, v0

    sget-object v3, Lcom/samsung/vip/engine/e;->b:[I

    aget v3, v3, v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private a([F[FZ)V
    .locals 1

    if-eqz p3, :cond_0

    invoke-virtual {p0, p1, p2}, Lcom/samsung/vip/engine/e;->a([F[F)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/vip/engine/e;->e:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/samsung/vip/engine/e;->f:Ljava/util/LinkedList;

    invoke-virtual {v0, p2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private a(Z)[Ljava/lang/String;
    .locals 13

    const/4 v1, 0x0

    const/4 v12, 0x3

    const v11, 0xffff

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p0, v12}, Lcom/samsung/vip/engine/e;->a(I)I

    move-result v0

    :goto_0
    if-eqz v0, :cond_4

    const-string/jumbo v2, "VITextRecognitionLib"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Error Code: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    :goto_1
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/vip/engine/e;->e:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v6

    move v2, v3

    move v4, v3

    :goto_2
    if-lt v2, v6, :cond_1

    add-int/lit8 v7, v4, 0x1

    mul-int/lit8 v0, v7, 0x2

    new-array v8, v0, [I

    move v5, v3

    move v2, v3

    :goto_3
    if-lt v5, v6, :cond_2

    add-int/lit8 v0, v2, 0x1

    aput v11, v8, v2

    add-int/lit8 v2, v0, 0x1

    aput v11, v8, v0

    iget-object v0, p0, Lcom/samsung/vip/engine/e;->e:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    iget-object v0, p0, Lcom/samsung/vip/engine/e;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    invoke-virtual {p0, v12, v8, v7}, Lcom/samsung/vip/engine/e;->a(I[II)I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/samsung/vip/engine/e;->e:Ljava/util/LinkedList;

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    array-length v0, v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v4, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/samsung/vip/engine/e;->e:Ljava/util/LinkedList;

    invoke-virtual {v0, v5}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    array-length v9, v0

    move v4, v2

    move v2, v3

    :goto_4
    if-lt v2, v9, :cond_3

    add-int/lit8 v0, v4, 0x1

    aput v11, v8, v4

    add-int/lit8 v2, v0, 0x1

    aput v3, v8, v0

    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_3

    :cond_3
    add-int/lit8 v10, v4, 0x1

    iget-object v0, p0, Lcom/samsung/vip/engine/e;->e:Ljava/util/LinkedList;

    invoke-virtual {v0, v5}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    aget v0, v0, v2

    float-to-int v0, v0

    aput v0, v8, v4

    add-int/lit8 v4, v10, 0x1

    iget-object v0, p0, Lcom/samsung/vip/engine/e;->f:Ljava/util/LinkedList;

    invoke-virtual {v0, v5}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    aget v0, v0, v2

    float-to-int v0, v0

    aput v0, v8, v10

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    :cond_4
    invoke-virtual {p0}, Lcom/samsung/vip/engine/e;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_5

    const-string/jumbo v0, "VITextRecognitionLib"

    const-string/jumbo v2, "GetResult() return null!"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    goto/16 :goto_1

    :cond_5
    invoke-direct {p0, v0}, Lcom/samsung/vip/engine/e;->c(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1
.end method

.method private c(Ljava/lang/String;)[Ljava/lang/String;
    .locals 7

    const v6, 0xffff

    const/4 v1, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    move v0, v1

    move v2, v1

    :goto_0
    if-lt v2, v4, :cond_0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_1
    return-object v0

    :cond_0
    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-ne v3, v6, :cond_1

    add-int/lit8 v0, v0, 0x1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    new-array v2, v0, [Ljava/lang/String;

    move v0, v1

    move v3, v1

    :goto_2
    if-lt v3, v4, :cond_3

    move-object v0, v2

    goto :goto_1

    :cond_3
    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-ne v5, v6, :cond_4

    invoke-virtual {p1, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v0, v3, 0x1

    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_2
.end method


# virtual methods
.method protected abstract a(I)I
.end method

.method protected abstract a(I[II)I
.end method

.method public a(Ljava/lang/String;)I
    .locals 2

    const/4 v1, -0x1

    iget-object v0, p0, Lcom/samsung/vip/engine/e;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/vip/engine/e;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-nez v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method protected abstract a(Ljava/lang/String;IIIS)I
.end method

.method public a(Ljava/lang/String;IS)I
    .locals 6

    const/16 v3, 0x640

    const/16 v4, 0x4b0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/vip/engine/e;->a(Ljava/lang/String;IIIS)I

    move-result v0

    return v0
.end method

.method protected abstract a()V
.end method

.method protected abstract a([F[F)V
.end method

.method protected abstract b()Ljava/lang/String;
.end method

.method public b([F[F)V
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/vip/engine/e;->d:Z

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/vip/engine/e;->a([F[FZ)V

    return-void
.end method

.method public b(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/vip/engine/e;->c:Ljava/util/HashMap;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/vip/engine/e;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public c()V
    .locals 0

    invoke-virtual {p0}, Lcom/samsung/vip/engine/e;->a()V

    return-void
.end method

.method public d()[Ljava/lang/String;
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/vip/engine/e;->d:Z

    invoke-direct {p0, v0}, Lcom/samsung/vip/engine/e;->a(Z)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()[Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/samsung/vip/engine/e;->a:[Ljava/lang/String;

    return-object v0
.end method
