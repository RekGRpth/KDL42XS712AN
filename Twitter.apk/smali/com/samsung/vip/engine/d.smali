.class public Lcom/samsung/vip/engine/d;
.super Lcom/samsung/vip/engine/VIRecognitionLib;
.source "Twttr"


# instance fields
.field private a:Ljava/util/LinkedList;

.field private b:Ljava/util/LinkedList;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/samsung/vip/engine/VIRecognitionLib;-><init>()V

    iput-object v0, p0, Lcom/samsung/vip/engine/d;->a:Ljava/util/LinkedList;

    iput-object v0, p0, Lcom/samsung/vip/engine/d;->b:Ljava/util/LinkedList;

    return-void
.end method


# virtual methods
.method public a(I)Ljava/lang/String;
    .locals 1

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const-string/jumbo v0, ""

    :goto_0
    return-object v0

    :pswitch_1
    const-string/jumbo v0, "line"

    goto :goto_0

    :pswitch_2
    const-string/jumbo v0, "arrow"

    goto :goto_0

    :pswitch_3
    const-string/jumbo v0, "bezier"

    goto :goto_0

    :pswitch_4
    const-string/jumbo v0, "circle"

    goto :goto_0

    :pswitch_5
    const-string/jumbo v0, "circlearc"

    goto :goto_0

    :pswitch_6
    const-string/jumbo v0, "ellipse"

    goto :goto_0

    :pswitch_7
    const-string/jumbo v0, "polygon"

    goto :goto_0

    :pswitch_8
    const-string/jumbo v0, "ellipsearc"

    goto :goto_0

    :pswitch_9
    const-string/jumbo v0, "table"

    goto :goto_0

    :pswitch_a
    const-string/jumbo v0, "group"

    goto :goto_0

    :pswitch_b
    const-string/jumbo v0, "polyline"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_8
        :pswitch_3
        :pswitch_a
        :pswitch_2
        :pswitch_9
        :pswitch_b
    .end packed-switch
.end method

.method public a()V
    .locals 1

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/samsung/vip/engine/d;->a:Ljava/util/LinkedList;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/samsung/vip/engine/d;->b:Ljava/util/LinkedList;

    const/high16 v0, 0xa00000

    invoke-virtual {p0, v0}, Lcom/samsung/vip/engine/d;->VISH_InitSmartShapeEngine(I)V

    return-void
.end method

.method public a([F[F)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/vip/engine/d;->a:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/samsung/vip/engine/d;->b:Ljava/util/LinkedList;

    invoke-virtual {v0, p2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public b()V
    .locals 0

    invoke-virtual {p0}, Lcom/samsung/vip/engine/d;->VISH_ReleaseSmartShapeEngine()V

    return-void
.end method

.method public c()V
    .locals 0

    invoke-virtual {p0}, Lcom/samsung/vip/engine/d;->VISH_ClearScene()V

    return-void
.end method

.method public d()[Lef;
    .locals 9

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/samsung/vip/engine/d;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v5

    move v1, v2

    move v3, v2

    :goto_0
    if-lt v1, v5, :cond_0

    mul-int/lit8 v0, v3, 0x2

    new-array v6, v0, [I

    move v4, v2

    move v1, v2

    :goto_1
    if-lt v4, v5, :cond_1

    iget-object v0, p0, Lcom/samsung/vip/engine/d;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    iget-object v0, p0, Lcom/samsung/vip/engine/d;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    invoke-virtual {p0, v6}, Lcom/samsung/vip/engine/d;->VISH_UpdateScene([I)V

    invoke-virtual {p0}, Lcom/samsung/vip/engine/d;->VISH_GetGraphPrimitiveArray()[Lef;

    move-result-object v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    :goto_2
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/vip/engine/d;->a:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    array-length v0, v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v3, v0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/samsung/vip/engine/d;->a:Ljava/util/LinkedList;

    invoke-virtual {v0, v4}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    array-length v7, v0

    move v3, v1

    move v1, v2

    :goto_3
    if-lt v1, v7, :cond_2

    add-int/lit8 v0, v3, 0x1

    const v1, 0xffff

    aput v1, v6, v3

    add-int/lit8 v1, v0, 0x1

    aput v2, v6, v0

    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    :cond_2
    add-int/lit8 v8, v3, 0x1

    iget-object v0, p0, Lcom/samsung/vip/engine/d;->a:Ljava/util/LinkedList;

    invoke-virtual {v0, v4}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    aget v0, v0, v1

    float-to-int v0, v0

    aput v0, v6, v3

    add-int/lit8 v3, v8, 0x1

    iget-object v0, p0, Lcom/samsung/vip/engine/d;->b:Ljava/util/LinkedList;

    invoke-virtual {v0, v4}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    aget v0, v0, v1

    float-to-int v0, v0

    aput v0, v6, v8

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_3
    const-string/jumbo v1, "VIShapeRecognitionLib"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Result group # : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method
