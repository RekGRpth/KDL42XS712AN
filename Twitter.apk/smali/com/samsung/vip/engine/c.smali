.class public Lcom/samsung/vip/engine/c;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/samsung/vip/engine/c;->a:Ljava/util/ArrayList;

    return-void
.end method

.method private a(FF)F
    .locals 4

    const v1, 0x40490fdb    # (float)Math.PI

    const/4 v0, 0x0

    cmpl-float v2, p1, v0

    if-nez v2, :cond_3

    cmpl-float v1, p2, v0

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    cmpl-float v0, p2, v0

    if-lez v0, :cond_2

    const v0, 0x3fc90fdb

    goto :goto_0

    :cond_2
    const v0, 0x4096cbe4

    goto :goto_0

    :cond_3
    cmpl-float v2, p1, v0

    if-lez v2, :cond_5

    cmpl-float v1, p2, v0

    if-eqz v1, :cond_0

    cmpl-float v0, p2, v0

    if-lez v0, :cond_4

    div-float v0, p2, p1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->atan(D)D

    move-result-wide v0

    double-to-float v0, v0

    goto :goto_0

    :cond_4
    const v0, 0x40c90fdb

    div-float v1, p2, p1

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->atan(D)D

    move-result-wide v1

    double-to-float v1, v1

    add-float/2addr v0, v1

    goto :goto_0

    :cond_5
    cmpl-float v0, p2, v0

    if-nez v0, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    div-float v0, p2, p1

    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->atan(D)D

    move-result-wide v2

    double-to-float v0, v2

    add-float/2addr v0, v1

    goto :goto_0
.end method

.method private a(FFFFF)F
    .locals 8

    const/high16 v0, 0x3f800000    # 1.0f

    sub-float/2addr v0, p1

    float-to-double v0, v0

    const-wide/high16 v2, 0x4008000000000000L    # 3.0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    float-to-double v2, p2

    mul-double/2addr v0, v2

    const/high16 v2, 0x40400000    # 3.0f

    mul-float/2addr v2, p1

    float-to-double v2, v2

    const/high16 v4, 0x3f800000    # 1.0f

    sub-float/2addr v4, p1

    float-to-double v4, v4

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    mul-double/2addr v2, v4

    float-to-double v4, p3

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    const-wide/high16 v2, 0x4008000000000000L    # 3.0

    float-to-double v4, p1

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    mul-double/2addr v2, v4

    const/high16 v4, 0x3f800000    # 1.0f

    sub-float/2addr v4, p1

    float-to-double v4, v4

    mul-double/2addr v2, v4

    float-to-double v4, p4

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    float-to-double v2, p1

    const-wide/high16 v4, 0x4008000000000000L    # 3.0

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    float-to-double v4, p5

    mul-double/2addr v2, v4

    add-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method

.method private a(Landroid/graphics/PointF;FF)F
    .locals 5

    iget v0, p1, Landroid/graphics/PointF;->x:F

    iget v1, p1, Landroid/graphics/PointF;->y:F

    div-float/2addr v0, p2

    div-float/2addr v1, p3

    float-to-double v1, v1

    float-to-double v3, v0

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    double-to-float v0, v0

    const/4 v1, 0x0

    cmpg-float v1, v0, v1

    if-gez v1, :cond_0

    float-to-double v0, v0

    const-wide v2, 0x401921fb54442d18L    # 6.283185307179586

    add-double/2addr v0, v2

    double-to-float v0, v0

    :cond_0
    return v0
.end method

.method private a([S)F
    .locals 3

    const/4 v2, 0x0

    aget-short v0, p1, v2

    const v1, 0xffff

    and-int/2addr v0, v1

    or-int/2addr v0, v2

    const/4 v1, 0x1

    aget-short v1, p1, v1

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    return v0
.end method

.method private a(FLandroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Landroid/graphics/PointF;
    .locals 7

    iget v2, p2, Landroid/graphics/PointF;->x:F

    iget v3, p3, Landroid/graphics/PointF;->x:F

    iget v4, p4, Landroid/graphics/PointF;->x:F

    iget v5, p5, Landroid/graphics/PointF;->x:F

    move-object v0, p0

    move v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/samsung/vip/engine/c;->a(FFFFF)F

    move-result v6

    iget v2, p2, Landroid/graphics/PointF;->y:F

    iget v3, p3, Landroid/graphics/PointF;->y:F

    iget v4, p4, Landroid/graphics/PointF;->y:F

    iget v5, p5, Landroid/graphics/PointF;->y:F

    move-object v0, p0

    move v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/samsung/vip/engine/c;->a(FFFFF)F

    move-result v0

    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1, v6, v0}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v1
.end method

.method private a(Landroid/graphics/Point;)Landroid/graphics/PointF;
    .locals 2

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iget v1, p1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/PointF;->x:F

    iget v1, p1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/PointF;->y:F

    return-object v0
.end method

.method private a(Landroid/graphics/PointF;Landroid/graphics/Point;FFI)Landroid/graphics/PointF;
    .locals 4

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    const/4 v1, 0x1

    if-ne p5, v1, :cond_0

    iget v1, p1, Landroid/graphics/PointF;->x:F

    mul-float/2addr v1, p3

    iget v2, p1, Landroid/graphics/PointF;->y:F

    mul-float/2addr v2, p4

    sub-float/2addr v1, v2

    iget v2, p2, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/PointF;->x:F

    iget v1, p1, Landroid/graphics/PointF;->x:F

    mul-float/2addr v1, p4

    iget v2, p1, Landroid/graphics/PointF;->y:F

    mul-float/2addr v2, p3

    add-float/2addr v1, v2

    iget v2, p2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/PointF;->y:F

    :goto_0
    return-object v0

    :cond_0
    iget v1, p1, Landroid/graphics/PointF;->x:F

    iget v2, p2, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    mul-float/2addr v1, p3

    iget v2, p1, Landroid/graphics/PointF;->y:F

    iget v3, p2, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    mul-float/2addr v2, p4

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/PointF;->x:F

    neg-float v1, p4

    iget v2, p1, Landroid/graphics/PointF;->x:F

    iget v3, p2, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    mul-float/2addr v1, v2

    iget v2, p1, Landroid/graphics/PointF;->y:F

    iget v3, p2, Landroid/graphics/Point;->y:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    mul-float/2addr v2, p3

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/PointF;->y:F

    goto :goto_0
.end method

.method private a(Leh;[SI)V
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    const/4 v1, 0x2

    new-array v1, v1, [S

    add-int/lit8 v2, p3, 0x1

    aget-short v3, p2, p3

    aput-short v3, v1, v7

    add-int/lit8 v3, v2, 0x1

    aget-short v2, p2, v2

    aput-short v2, v1, v8

    invoke-direct {p0, v1}, Lcom/samsung/vip/engine/c;->b([S)I

    move-result v2

    iput v2, v0, Landroid/graphics/Point;->x:I

    add-int/lit8 v2, v3, 0x1

    aget-short v3, p2, v3

    aput-short v3, v1, v7

    add-int/lit8 v3, v2, 0x1

    aget-short v2, p2, v2

    aput-short v2, v1, v8

    invoke-direct {p0, v1}, Lcom/samsung/vip/engine/c;->b([S)I

    move-result v2

    iput v2, v0, Landroid/graphics/Point;->y:I

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    add-int/lit8 v4, v3, 0x1

    aget-short v3, p2, v3

    aput-short v3, v1, v7

    add-int/lit8 v3, v4, 0x1

    aget-short v4, p2, v4

    aput-short v4, v1, v8

    invoke-direct {p0, v1}, Lcom/samsung/vip/engine/c;->b([S)I

    move-result v4

    iput v4, v2, Landroid/graphics/Point;->x:I

    add-int/lit8 v4, v3, 0x1

    aget-short v3, p2, v3

    aput-short v3, v1, v7

    add-int/lit8 v3, v4, 0x1

    aget-short v4, p2, v4

    aput-short v4, v1, v8

    invoke-direct {p0, v1}, Lcom/samsung/vip/engine/c;->b([S)I

    move-result v4

    iput v4, v2, Landroid/graphics/Point;->y:I

    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4}, Landroid/graphics/Point;-><init>()V

    add-int/lit8 v5, v3, 0x1

    aget-short v3, p2, v3

    aput-short v3, v1, v7

    add-int/lit8 v3, v5, 0x1

    aget-short v5, p2, v5

    aput-short v5, v1, v8

    invoke-direct {p0, v1}, Lcom/samsung/vip/engine/c;->b([S)I

    move-result v5

    iput v5, v4, Landroid/graphics/Point;->x:I

    add-int/lit8 v5, v3, 0x1

    aget-short v3, p2, v3

    aput-short v3, v1, v7

    add-int/lit8 v3, v5, 0x1

    aget-short v5, p2, v5

    aput-short v5, v1, v8

    invoke-direct {p0, v1}, Lcom/samsung/vip/engine/c;->b([S)I

    move-result v5

    iput v5, v4, Landroid/graphics/Point;->y:I

    new-instance v5, Landroid/graphics/Point;

    invoke-direct {v5}, Landroid/graphics/Point;-><init>()V

    add-int/lit8 v6, v3, 0x1

    aget-short v3, p2, v3

    aput-short v3, v1, v7

    add-int/lit8 v3, v6, 0x1

    aget-short v6, p2, v6

    aput-short v6, v1, v8

    invoke-direct {p0, v1}, Lcom/samsung/vip/engine/c;->b([S)I

    move-result v6

    iput v6, v5, Landroid/graphics/Point;->x:I

    add-int/lit8 v6, v3, 0x1

    aget-short v3, p2, v3

    aput-short v3, v1, v7

    add-int/lit8 v3, v6, 0x1

    aget-short v3, p2, v6

    aput-short v3, v1, v8

    invoke-direct {p0, v1}, Lcom/samsung/vip/engine/c;->b([S)I

    move-result v1

    iput v1, v5, Landroid/graphics/Point;->y:I

    invoke-virtual {p1, v0}, Leh;->a(Landroid/graphics/Point;)V

    invoke-virtual {p1, v2}, Leh;->b(Landroid/graphics/Point;)V

    invoke-virtual {p1, v4}, Leh;->d(Landroid/graphics/Point;)V

    invoke-virtual {p1, v5}, Leh;->c(Landroid/graphics/Point;)V

    return-void
.end method

.method private a(Lej;[SI)V
    .locals 10

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    aget-short v3, p2, p3

    add-int/lit8 v1, p3, 0x2

    const/4 v0, 0x0

    :goto_0
    if-lt v0, v3, :cond_0

    invoke-virtual {p1, v2}, Lej;->a(Ljava/util/List;)V

    return-void

    :cond_0
    new-instance v4, Lei;

    invoke-direct {v4}, Lei;-><init>()V

    new-instance v5, Landroid/graphics/Point;

    invoke-direct {v5}, Landroid/graphics/Point;-><init>()V

    new-instance v6, Landroid/graphics/Point;

    invoke-direct {v6}, Landroid/graphics/Point;-><init>()V

    new-instance v7, Landroid/graphics/Point;

    invoke-direct {v7}, Landroid/graphics/Point;-><init>()V

    new-instance v8, Landroid/graphics/Point;

    invoke-direct {v8}, Landroid/graphics/Point;-><init>()V

    add-int/lit8 v9, v1, 0x1

    aget-short v1, p2, v1

    iput v1, v5, Landroid/graphics/Point;->x:I

    add-int/lit8 v1, v9, 0x1

    aget-short v9, p2, v9

    iput v9, v5, Landroid/graphics/Point;->y:I

    add-int/lit8 v9, v1, 0x1

    aget-short v1, p2, v1

    iput v1, v6, Landroid/graphics/Point;->x:I

    add-int/lit8 v1, v9, 0x1

    aget-short v9, p2, v9

    iput v9, v6, Landroid/graphics/Point;->y:I

    add-int/lit8 v9, v1, 0x1

    aget-short v1, p2, v1

    iput v1, v7, Landroid/graphics/Point;->x:I

    add-int/lit8 v1, v9, 0x1

    aget-short v9, p2, v9

    iput v9, v7, Landroid/graphics/Point;->y:I

    add-int/lit8 v9, v1, 0x1

    aget-short v1, p2, v1

    iput v1, v8, Landroid/graphics/Point;->x:I

    add-int/lit8 v1, v9, 0x1

    aget-short v9, p2, v9

    iput v9, v8, Landroid/graphics/Point;->y:I

    invoke-virtual {v4, v5}, Lei;->a(Landroid/graphics/Point;)V

    invoke-virtual {v4, v6}, Lei;->b(Landroid/graphics/Point;)V

    invoke-virtual {v4, v7}, Lei;->c(Landroid/graphics/Point;)V

    invoke-virtual {v4, v8}, Lei;->d(Landroid/graphics/Point;)V

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private a(Lek;[SI)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    const/4 v1, 0x2

    new-array v1, v1, [S

    add-int/lit8 v2, p3, 0x1

    aget-short v3, p2, p3

    aput-short v3, v1, v4

    add-int/lit8 v3, v2, 0x1

    aget-short v2, p2, v2

    aput-short v2, v1, v5

    invoke-direct {p0, v1}, Lcom/samsung/vip/engine/c;->b([S)I

    move-result v2

    iput v2, v0, Landroid/graphics/Point;->x:I

    add-int/lit8 v2, v3, 0x1

    aget-short v3, p2, v3

    aput-short v3, v1, v4

    add-int/lit8 v3, v2, 0x1

    aget-short v2, p2, v2

    aput-short v2, v1, v5

    invoke-direct {p0, v1}, Lcom/samsung/vip/engine/c;->b([S)I

    move-result v1

    iput v1, v0, Landroid/graphics/Point;->y:I

    invoke-virtual {p1, v0}, Lek;->a(Landroid/graphics/Point;)V

    add-int/lit8 v0, v3, 0x1

    aget-short v0, p2, v3

    invoke-virtual {p1, v0}, Lek;->a(I)V

    return-void
.end method

.method private a(Lel;[SI)V
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x1

    aget-short v0, p2, v6

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    const/4 v3, 0x2

    new-array v3, v3, [S

    add-int/lit8 v4, p3, 0x1

    aget-short v5, p2, p3

    aput-short v5, v3, v7

    add-int/lit8 v5, v4, 0x1

    aget-short v4, p2, v4

    aput-short v4, v3, v6

    invoke-direct {p0, v3}, Lcom/samsung/vip/engine/c;->b([S)I

    move-result v4

    iput v4, v0, Landroid/graphics/Point;->x:I

    add-int/lit8 v4, v5, 0x1

    aget-short v5, p2, v5

    aput-short v5, v3, v7

    add-int/lit8 v5, v4, 0x1

    aget-short v4, p2, v4

    aput-short v4, v3, v6

    invoke-direct {p0, v3}, Lcom/samsung/vip/engine/c;->b([S)I

    move-result v4

    iput v4, v0, Landroid/graphics/Point;->y:I

    invoke-virtual {p1, v0}, Lel;->a(Landroid/graphics/Point;)V

    add-int/lit8 v0, v5, 0x1

    aget-short v4, p2, v5

    aput-short v4, v3, v7

    add-int/lit8 v4, v0, 0x1

    aget-short v0, p2, v0

    aput-short v0, v3, v6

    invoke-direct {p0, v3}, Lcom/samsung/vip/engine/c;->b([S)I

    move-result v0

    iput v0, v1, Landroid/graphics/Point;->x:I

    add-int/lit8 v0, v4, 0x1

    aget-short v4, p2, v4

    aput-short v4, v3, v7

    add-int/lit8 v4, v0, 0x1

    aget-short v0, p2, v0

    aput-short v0, v3, v6

    invoke-direct {p0, v3}, Lcom/samsung/vip/engine/c;->b([S)I

    move-result v0

    iput v0, v1, Landroid/graphics/Point;->y:I

    invoke-virtual {p1, v1}, Lel;->b(Landroid/graphics/Point;)V

    add-int/lit8 v0, v4, 0x1

    aget-short v1, p2, v4

    aput-short v1, v3, v7

    add-int/lit8 v1, v0, 0x1

    aget-short v0, p2, v0

    aput-short v0, v3, v6

    invoke-direct {p0, v3}, Lcom/samsung/vip/engine/c;->b([S)I

    move-result v0

    iput v0, v2, Landroid/graphics/Point;->x:I

    add-int/lit8 v0, v1, 0x1

    aget-short v1, p2, v1

    aput-short v1, v3, v7

    add-int/lit8 v1, v0, 0x1

    aget-short v0, p2, v0

    aput-short v0, v3, v6

    invoke-direct {p0, v3}, Lcom/samsung/vip/engine/c;->b([S)I

    move-result v0

    iput v0, v2, Landroid/graphics/Point;->y:I

    invoke-virtual {p1, v2}, Lel;->c(Landroid/graphics/Point;)V

    add-int/lit8 v0, v1, 0x1

    aget-short v1, p2, v1

    invoke-virtual {p1, v1}, Lel;->b(I)V

    add-int/lit8 v1, v0, 0x1

    aget-short v0, p2, v0

    invoke-virtual {p1, v0}, Lel;->a(I)V

    return-void
.end method

.method private a(Lem;[SI)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    const/4 v1, 0x2

    new-array v1, v1, [S

    add-int/lit8 v2, p3, 0x1

    aget-short v3, p2, p3

    aput-short v3, v1, v4

    add-int/lit8 v3, v2, 0x1

    aget-short v2, p2, v2

    aput-short v2, v1, v5

    invoke-direct {p0, v1}, Lcom/samsung/vip/engine/c;->b([S)I

    move-result v2

    iput v2, v0, Landroid/graphics/Point;->x:I

    add-int/lit8 v2, v3, 0x1

    aget-short v3, p2, v3

    aput-short v3, v1, v4

    add-int/lit8 v3, v2, 0x1

    aget-short v2, p2, v2

    aput-short v2, v1, v5

    invoke-direct {p0, v1}, Lcom/samsung/vip/engine/c;->b([S)I

    move-result v2

    iput v2, v0, Landroid/graphics/Point;->y:I

    invoke-virtual {p1, v0}, Lem;->a(Landroid/graphics/Point;)V

    add-int/lit8 v0, v3, 0x1

    aget-short v2, p2, v3

    invoke-virtual {p1, v2}, Lem;->a(I)V

    add-int/lit8 v2, v0, 0x1

    aget-short v0, p2, v0

    invoke-virtual {p1, v0}, Lem;->b(I)V

    add-int/lit8 v0, v2, 0x1

    aget-short v2, p2, v2

    aput-short v2, v1, v4

    add-int/lit8 v2, v0, 0x1

    aget-short v0, p2, v0

    aput-short v0, v1, v5

    invoke-direct {p0, v1}, Lcom/samsung/vip/engine/c;->a([S)F

    move-result v0

    invoke-virtual {p1, v0}, Lem;->a(F)V

    add-int/lit8 v0, v2, 0x1

    aget-short v2, p2, v2

    aput-short v2, v1, v4

    add-int/lit8 v2, v0, 0x1

    aget-short v0, p2, v0

    aput-short v0, v1, v5

    invoke-direct {p0, v1}, Lcom/samsung/vip/engine/c;->a([S)F

    move-result v0

    invoke-virtual {p1, v0}, Lem;->b(F)V

    return-void
.end method

.method private a(Len;[SI)V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    const/4 v3, 0x2

    new-array v3, v3, [S

    add-int/lit8 v4, p3, 0x1

    aget-short v5, p2, p3

    aput-short v5, v3, v6

    add-int/lit8 v5, v4, 0x1

    aget-short v4, p2, v4

    aput-short v4, v3, v7

    invoke-direct {p0, v3}, Lcom/samsung/vip/engine/c;->b([S)I

    move-result v4

    iput v4, v0, Landroid/graphics/Point;->x:I

    add-int/lit8 v4, v5, 0x1

    aget-short v5, p2, v5

    aput-short v5, v3, v6

    add-int/lit8 v5, v4, 0x1

    aget-short v4, p2, v4

    aput-short v4, v3, v7

    invoke-direct {p0, v3}, Lcom/samsung/vip/engine/c;->b([S)I

    move-result v4

    iput v4, v0, Landroid/graphics/Point;->y:I

    invoke-virtual {p1, v0}, Len;->a(Landroid/graphics/Point;)V

    add-int/lit8 v0, v5, 0x1

    aget-short v4, p2, v5

    aput-short v4, v3, v6

    add-int/lit8 v4, v0, 0x1

    aget-short v0, p2, v0

    aput-short v0, v3, v7

    invoke-direct {p0, v3}, Lcom/samsung/vip/engine/c;->b([S)I

    move-result v0

    iput v0, v1, Landroid/graphics/Point;->x:I

    add-int/lit8 v0, v4, 0x1

    aget-short v4, p2, v4

    aput-short v4, v3, v6

    add-int/lit8 v4, v0, 0x1

    aget-short v0, p2, v0

    aput-short v0, v3, v7

    invoke-direct {p0, v3}, Lcom/samsung/vip/engine/c;->b([S)I

    move-result v0

    iput v0, v1, Landroid/graphics/Point;->y:I

    invoke-virtual {p1, v1}, Len;->b(Landroid/graphics/Point;)V

    add-int/lit8 v0, v4, 0x1

    aget-short v1, p2, v4

    aput-short v1, v3, v6

    add-int/lit8 v1, v0, 0x1

    aget-short v0, p2, v0

    aput-short v0, v3, v7

    invoke-direct {p0, v3}, Lcom/samsung/vip/engine/c;->b([S)I

    move-result v0

    iput v0, v2, Landroid/graphics/Point;->x:I

    add-int/lit8 v0, v1, 0x1

    aget-short v1, p2, v1

    aput-short v1, v3, v6

    add-int/lit8 v1, v0, 0x1

    aget-short v0, p2, v0

    aput-short v0, v3, v7

    invoke-direct {p0, v3}, Lcom/samsung/vip/engine/c;->b([S)I

    move-result v0

    iput v0, v2, Landroid/graphics/Point;->y:I

    invoke-virtual {p1, v2}, Len;->c(Landroid/graphics/Point;)V

    add-int/lit8 v0, v1, 0x1

    aget-short v1, p2, v1

    invoke-virtual {p1, v1}, Len;->b(I)V

    add-int/lit8 v1, v0, 0x1

    aget-short v0, p2, v0

    invoke-virtual {p1, v0}, Len;->c(I)V

    add-int/lit8 v0, v1, 0x1

    aget-short v1, p2, v1

    aput-short v1, v3, v6

    add-int/lit8 v1, v0, 0x1

    aget-short v0, p2, v0

    aput-short v0, v3, v7

    invoke-direct {p0, v3}, Lcom/samsung/vip/engine/c;->a([S)F

    move-result v0

    invoke-virtual {p1, v0}, Len;->a(F)V

    add-int/lit8 v0, v1, 0x1

    aget-short v1, p2, v1

    aput-short v1, v3, v6

    add-int/lit8 v1, v0, 0x1

    aget-short v0, p2, v0

    aput-short v0, v3, v7

    invoke-direct {p0, v3}, Lcom/samsung/vip/engine/c;->a([S)F

    move-result v0

    invoke-virtual {p1, v0}, Len;->b(F)V

    add-int/lit8 v0, v1, 0x1

    aget-short v0, p2, v1

    invoke-virtual {p1, v0}, Len;->a(I)V

    return-void
.end method

.method private a(Leo;[SI)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    const/4 v2, 0x2

    new-array v2, v2, [S

    add-int/lit8 v3, p3, 0x1

    aget-short v4, p2, p3

    aput-short v4, v2, v5

    add-int/lit8 v4, v3, 0x1

    aget-short v3, p2, v3

    aput-short v3, v2, v6

    invoke-direct {p0, v2}, Lcom/samsung/vip/engine/c;->b([S)I

    move-result v3

    iput v3, v0, Landroid/graphics/Point;->x:I

    add-int/lit8 v3, v4, 0x1

    aget-short v4, p2, v4

    aput-short v4, v2, v5

    add-int/lit8 v4, v3, 0x1

    aget-short v3, p2, v3

    aput-short v3, v2, v6

    invoke-direct {p0, v2}, Lcom/samsung/vip/engine/c;->b([S)I

    move-result v3

    iput v3, v0, Landroid/graphics/Point;->y:I

    add-int/lit8 v3, v4, 0x1

    aget-short v4, p2, v4

    aput-short v4, v2, v5

    add-int/lit8 v4, v3, 0x1

    aget-short v3, p2, v3

    aput-short v3, v2, v6

    invoke-direct {p0, v2}, Lcom/samsung/vip/engine/c;->b([S)I

    move-result v3

    iput v3, v1, Landroid/graphics/Point;->x:I

    add-int/lit8 v3, v4, 0x1

    aget-short v4, p2, v4

    aput-short v4, v2, v5

    add-int/lit8 v4, v3, 0x1

    aget-short v3, p2, v3

    aput-short v3, v2, v6

    invoke-direct {p0, v2}, Lcom/samsung/vip/engine/c;->b([S)I

    move-result v2

    iput v2, v1, Landroid/graphics/Point;->y:I

    invoke-virtual {p1, v0}, Leo;->a(Landroid/graphics/Point;)V

    invoke-virtual {p1, v1}, Leo;->b(Landroid/graphics/Point;)V

    return-void
.end method

.method private a(Lep;[SI)V
    .locals 6

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    aget-short v2, p2, p3

    const/4 v0, 0x0

    :goto_0
    if-lt v0, v2, :cond_0

    invoke-virtual {p1, v1}, Lep;->a(Ljava/util/ArrayList;)V

    return-void

    :cond_0
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    add-int/lit8 v4, p3, 0x2

    mul-int/lit8 v5, v0, 0x2

    add-int/2addr v4, v5

    add-int/lit8 v4, v4, 0x0

    aget-short v4, p2, v4

    iput v4, v3, Landroid/graphics/Point;->x:I

    add-int/lit8 v4, p3, 0x2

    mul-int/lit8 v5, v0, 0x2

    add-int/2addr v4, v5

    add-int/lit8 v4, v4, 0x1

    aget-short v4, p2, v4

    iput v4, v3, Landroid/graphics/Point;->y:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private a(Leq;[SI)V
    .locals 6

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    aget-short v2, p2, p3

    const/4 v0, 0x0

    :goto_0
    if-lt v0, v2, :cond_0

    invoke-virtual {p1, v1}, Leq;->a(Ljava/util/ArrayList;)V

    return-void

    :cond_0
    new-instance v3, Landroid/graphics/Point;

    invoke-direct {v3}, Landroid/graphics/Point;-><init>()V

    add-int/lit8 v4, p3, 0x2

    mul-int/lit8 v5, v0, 0x2

    add-int/2addr v4, v5

    add-int/lit8 v4, v4, 0x0

    aget-short v4, p2, v4

    iput v4, v3, Landroid/graphics/Point;->x:I

    add-int/lit8 v4, p3, 0x2

    mul-int/lit8 v5, v0, 0x2

    add-int/2addr v4, v5

    add-int/lit8 v4, v4, 0x1

    aget-short v4, p2, v4

    iput v4, v3, Landroid/graphics/Point;->y:I

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private a(Ljava/util/ArrayList;Landroid/graphics/PointF;FFFZLandroid/graphics/PointF;Landroid/graphics/PointF;I)V
    .locals 6

    const v5, 0x40c90fdb

    new-instance v1, Leg;

    invoke-direct {v1}, Leg;-><init>()V

    invoke-virtual {v1, p2}, Leg;->a(Landroid/graphics/PointF;)V

    invoke-virtual {v1, p3}, Leg;->b(F)V

    invoke-virtual {v1, p4}, Leg;->a(F)V

    invoke-virtual {v1, p5}, Leg;->c(F)V

    if-eqz p6, :cond_3

    iget v0, p7, Landroid/graphics/PointF;->x:F

    iget v2, p2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v0, v2

    iget v2, p7, Landroid/graphics/PointF;->y:F

    iget v3, p2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v2, v3

    invoke-direct {p0, v0, v2}, Lcom/samsung/vip/engine/c;->a(FF)F

    move-result v0

    iget v2, p8, Landroid/graphics/PointF;->x:F

    iget v3, p2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v2, v3

    iget v3, p8, Landroid/graphics/PointF;->y:F

    iget v4, p2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v3, v4

    invoke-direct {p0, v2, v3}, Lcom/samsung/vip/engine/c;->a(FF)F

    move-result v2

    invoke-virtual {v1, v0}, Leg;->d(F)V

    if-lez p9, :cond_1

    cmpl-float v3, v0, v2

    if-lez v3, :cond_0

    sub-float/2addr v0, v2

    sub-float v0, v5, v0

    :goto_0
    invoke-virtual {v1, v0}, Leg;->e(F)V

    :goto_1
    invoke-direct {p0, p1, v1}, Lcom/samsung/vip/engine/c;->a(Ljava/util/ArrayList;Leg;)V

    return-void

    :cond_0
    sub-float v0, v2, v0

    goto :goto_0

    :cond_1
    cmpl-float v3, v0, v2

    if-lez v3, :cond_2

    sub-float v0, v2, v0

    goto :goto_0

    :cond_2
    sub-float v0, v2, v0

    sub-float/2addr v0, v5

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Leg;->d(F)V

    invoke-virtual {v1, v5}, Leg;->e(F)V

    goto :goto_1
.end method

.method private a(Ljava/util/ArrayList;Leg;)V
    .locals 20

    invoke-virtual/range {p2 .. p2}, Leg;->a()Landroid/graphics/PointF;

    move-result-object v2

    iget v4, v2, Landroid/graphics/PointF;->x:F

    iget v5, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual/range {p2 .. p2}, Leg;->c()F

    move-result v6

    invoke-virtual/range {p2 .. p2}, Leg;->b()F

    move-result v7

    invoke-virtual/range {p2 .. p2}, Leg;->d()F

    move-result v8

    invoke-virtual/range {p2 .. p2}, Leg;->e()F

    move-result v9

    invoke-virtual/range {p2 .. p2}, Leg;->f()F

    move-result v3

    add-float v2, v6, v7

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v10

    mul-float/2addr v2, v10

    const v10, 0x453b8000    # 3000.0f

    cmpl-float v2, v2, v10

    if-lez v2, :cond_0

    const/16 v2, 0x80

    :goto_0
    int-to-float v10, v2

    div-float v10, v3, v10

    add-int/lit8 v3, v2, 0x1

    new-array v11, v3, [Landroid/graphics/PointF;

    const/4 v3, 0x0

    :goto_1
    add-int/lit8 v12, v2, 0x1

    if-lt v3, v12, :cond_1

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void

    :cond_0
    const/16 v2, 0x40

    goto :goto_0

    :cond_1
    int-to-float v12, v3

    mul-float/2addr v12, v10

    add-float/2addr v12, v9

    float-to-double v13, v4

    float-to-double v15, v7

    float-to-double v0, v12

    move-wide/from16 v17, v0

    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->cos(D)D

    move-result-wide v17

    mul-double v15, v15, v17

    float-to-double v0, v8

    move-wide/from16 v17, v0

    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->cos(D)D

    move-result-wide v17

    mul-double v15, v15, v17

    add-double/2addr v13, v15

    float-to-double v15, v6

    float-to-double v0, v12

    move-wide/from16 v17, v0

    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->sin(D)D

    move-result-wide v17

    mul-double v15, v15, v17

    float-to-double v0, v8

    move-wide/from16 v17, v0

    invoke-static/range {v17 .. v18}, Ljava/lang/Math;->sin(D)D

    move-result-wide v17

    mul-double v15, v15, v17

    sub-double/2addr v13, v15

    double-to-float v13, v13

    float-to-double v14, v5

    float-to-double v0, v7

    move-wide/from16 v16, v0

    float-to-double v0, v12

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->cos(D)D

    move-result-wide v18

    mul-double v16, v16, v18

    float-to-double v0, v8

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->sin(D)D

    move-result-wide v18

    mul-double v16, v16, v18

    add-double v14, v14, v16

    float-to-double v0, v6

    move-wide/from16 v16, v0

    float-to-double v0, v12

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->sin(D)D

    move-result-wide v18

    mul-double v16, v16, v18

    float-to-double v0, v8

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->cos(D)D

    move-result-wide v18

    mul-double v16, v16, v18

    add-double v14, v14, v16

    double-to-float v12, v14

    new-instance v14, Landroid/graphics/PointF;

    invoke-direct {v14, v13, v12}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v14, v11, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1
.end method

.method private a(Ljava/util/ArrayList;Leh;)V
    .locals 2

    new-instance v0, Leo;

    invoke-direct {v0}, Leo;-><init>()V

    invoke-virtual {p2}, Leh;->a()Landroid/graphics/Point;

    move-result-object v1

    invoke-virtual {v0, v1}, Leo;->a(Landroid/graphics/Point;)V

    invoke-virtual {p2}, Leh;->b()Landroid/graphics/Point;

    move-result-object v1

    invoke-virtual {v0, v1}, Leo;->b(Landroid/graphics/Point;)V

    invoke-direct {p0, p1, v0}, Lcom/samsung/vip/engine/c;->a(Ljava/util/ArrayList;Leo;)V

    invoke-virtual {p2}, Leh;->b()Landroid/graphics/Point;

    move-result-object v1

    invoke-virtual {v0, v1}, Leo;->a(Landroid/graphics/Point;)V

    invoke-virtual {p2}, Leh;->d()Landroid/graphics/Point;

    move-result-object v1

    invoke-virtual {v0, v1}, Leo;->b(Landroid/graphics/Point;)V

    invoke-direct {p0, p1, v0}, Lcom/samsung/vip/engine/c;->a(Ljava/util/ArrayList;Leo;)V

    invoke-virtual {p2}, Leh;->b()Landroid/graphics/Point;

    move-result-object v1

    invoke-virtual {v0, v1}, Leo;->a(Landroid/graphics/Point;)V

    invoke-virtual {p2}, Leh;->c()Landroid/graphics/Point;

    move-result-object v1

    invoke-virtual {v0, v1}, Leo;->b(Landroid/graphics/Point;)V

    invoke-direct {p0, p1, v0}, Lcom/samsung/vip/engine/c;->a(Ljava/util/ArrayList;Leo;)V

    return-void
.end method

.method private a(Ljava/util/ArrayList;Lej;)V
    .locals 14

    const/16 v10, 0x20

    invoke-virtual/range {p2 .. p2}, Lej;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v11

    const/4 v1, 0x0

    mul-int/lit8 v12, v11, 0x21

    new-array v13, v12, [Landroid/graphics/PointF;

    const/4 v0, 0x0

    move v9, v0

    :goto_0
    if-lt v9, v11, :cond_0

    invoke-virtual {p1, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void

    :cond_0
    invoke-virtual/range {p2 .. p2}, Lej;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lei;

    new-instance v2, Landroid/graphics/PointF;

    invoke-virtual {v0}, Lei;->a()Landroid/graphics/Point;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/graphics/PointF;-><init>(Landroid/graphics/Point;)V

    new-instance v3, Landroid/graphics/PointF;

    invoke-virtual {v0}, Lei;->b()Landroid/graphics/Point;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/graphics/PointF;-><init>(Landroid/graphics/Point;)V

    new-instance v4, Landroid/graphics/PointF;

    invoke-virtual {v0}, Lei;->c()Landroid/graphics/Point;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/graphics/PointF;-><init>(Landroid/graphics/Point;)V

    new-instance v5, Landroid/graphics/PointF;

    invoke-virtual {v0}, Lei;->d()Landroid/graphics/Point;

    move-result-object v0

    invoke-direct {v5, v0}, Landroid/graphics/PointF;-><init>(Landroid/graphics/Point;)V

    const/4 v0, 0x0

    move v7, v0

    move v8, v1

    :goto_1
    const/16 v0, 0x21

    if-lt v7, v0, :cond_2

    :cond_1
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    move v1, v8

    goto :goto_0

    :cond_2
    int-to-float v0, v7

    int-to-float v1, v10

    div-float v1, v0, v1

    if-ge v8, v12, :cond_1

    if-ne v7, v10, :cond_3

    add-int/lit8 v6, v8, 0x1

    const/high16 v1, 0x3f800000    # 1.0f

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/vip/engine/c;->a(FLandroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v0

    aput-object v0, v13, v8

    move v0, v6

    :goto_2
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    move v8, v0

    goto :goto_1

    :cond_3
    add-int/lit8 v6, v8, 0x1

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/vip/engine/c;->a(FLandroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v0

    aput-object v0, v13, v8

    move v0, v6

    goto :goto_2
.end method

.method private a(Ljava/util/ArrayList;Lek;)V
    .locals 10

    const/4 v7, 0x0

    new-instance v2, Landroid/graphics/PointF;

    invoke-virtual {p2}, Lek;->a()Landroid/graphics/Point;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/graphics/PointF;-><init>(Landroid/graphics/Point;)V

    invoke-virtual {p2}, Lek;->b()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p2}, Lek;->b()I

    move-result v0

    int-to-float v4, v0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v9, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v8, v7

    invoke-direct/range {v0 .. v9}, Lcom/samsung/vip/engine/c;->a(Ljava/util/ArrayList;Landroid/graphics/PointF;FFFZLandroid/graphics/PointF;Landroid/graphics/PointF;I)V

    return-void
.end method

.method private a(Ljava/util/ArrayList;Lel;)V
    .locals 10

    new-instance v2, Landroid/graphics/PointF;

    invoke-virtual {p2}, Lel;->b()Landroid/graphics/Point;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/graphics/PointF;-><init>(Landroid/graphics/Point;)V

    invoke-virtual {p2}, Lel;->e()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p2}, Lel;->e()I

    move-result v0

    int-to-float v4, v0

    const/4 v5, 0x0

    const/4 v6, 0x1

    new-instance v7, Landroid/graphics/PointF;

    invoke-virtual {p2}, Lel;->c()Landroid/graphics/Point;

    move-result-object v0

    invoke-direct {v7, v0}, Landroid/graphics/PointF;-><init>(Landroid/graphics/Point;)V

    new-instance v8, Landroid/graphics/PointF;

    invoke-virtual {p2}, Lel;->d()Landroid/graphics/Point;

    move-result-object v0

    invoke-direct {v8, v0}, Landroid/graphics/PointF;-><init>(Landroid/graphics/Point;)V

    invoke-virtual {p2}, Lel;->a()I

    move-result v9

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v9}, Lcom/samsung/vip/engine/c;->a(Ljava/util/ArrayList;Landroid/graphics/PointF;FFFZLandroid/graphics/PointF;Landroid/graphics/PointF;I)V

    return-void
.end method

.method private a(Ljava/util/ArrayList;Lem;)V
    .locals 10

    const/4 v7, 0x0

    new-instance v2, Landroid/graphics/PointF;

    invoke-virtual {p2}, Lem;->a()Landroid/graphics/Point;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/graphics/PointF;-><init>(Landroid/graphics/Point;)V

    invoke-virtual {p2}, Lem;->c()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p2}, Lem;->b()I

    move-result v0

    int-to-float v4, v0

    invoke-virtual {p2}, Lem;->d()F

    move-result v0

    invoke-virtual {p2}, Lem;->e()F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/samsung/vip/engine/c;->a(FF)F

    move-result v5

    const/4 v6, 0x0

    const/4 v9, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v8, v7

    invoke-direct/range {v0 .. v9}, Lcom/samsung/vip/engine/c;->a(Ljava/util/ArrayList;Landroid/graphics/PointF;FFFZLandroid/graphics/PointF;Landroid/graphics/PointF;I)V

    return-void
.end method

.method private a(Ljava/util/ArrayList;Len;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/samsung/vip/engine/c;->b(Ljava/util/ArrayList;Len;)V

    return-void
.end method

.method private a(Ljava/util/ArrayList;Leo;)V
    .locals 7

    const/4 v0, 0x3

    new-array v0, v0, [Landroid/graphics/PointF;

    invoke-virtual {p2}, Leo;->a()Landroid/graphics/Point;

    move-result-object v1

    invoke-virtual {p2}, Leo;->b()Landroid/graphics/Point;

    move-result-object v2

    const/4 v3, 0x0

    new-instance v4, Landroid/graphics/PointF;

    iget v5, v1, Landroid/graphics/Point;->x:I

    int-to-float v5, v5

    iget v6, v1, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    invoke-direct {v4, v5, v6}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v4, v0, v3

    const/4 v3, 0x1

    new-instance v4, Landroid/graphics/PointF;

    iget v5, v1, Landroid/graphics/Point;->x:I

    iget v6, v2, Landroid/graphics/Point;->x:I

    add-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    iget v1, v1, Landroid/graphics/Point;->y:I

    iget v6, v2, Landroid/graphics/Point;->y:I

    add-int/2addr v1, v6

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-direct {v4, v5, v1}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v4, v0, v3

    const/4 v1, 0x2

    new-instance v3, Landroid/graphics/PointF;

    iget v4, v2, Landroid/graphics/Point;->x:I

    int-to-float v4, v4

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    invoke-direct {v3, v4, v2}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v3, v0, v1

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private a(Ljava/util/ArrayList;Lep;)V
    .locals 6

    const/4 v2, 0x0

    invoke-virtual {p2}, Lep;->a()Ljava/util/ArrayList;

    move-result-object v3

    if-nez v3, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_0

    new-instance v5, Leo;

    invoke-direct {v5}, Leo;-><init>()V

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Point;

    invoke-virtual {v5, v0}, Leo;->a(Landroid/graphics/Point;)V

    add-int/lit8 v0, v4, -0x1

    if-ge v1, v0, :cond_2

    add-int/lit8 v0, v1, 0x1

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Point;

    invoke-virtual {v5, v0}, Leo;->b(Landroid/graphics/Point;)V

    :goto_1
    invoke-direct {p0, p1, v5}, Lcom/samsung/vip/engine/c;->a(Ljava/util/ArrayList;Leo;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Point;

    invoke-virtual {v5, v0}, Leo;->b(Landroid/graphics/Point;)V

    goto :goto_1
.end method

.method private a(Ljava/util/ArrayList;Leq;)V
    .locals 5

    invoke-virtual {p2}, Leq;->a()Ljava/util/ArrayList;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    add-int/lit8 v0, v3, -0x1

    if-ge v1, v0, :cond_0

    new-instance v4, Leo;

    invoke-direct {v4}, Leo;-><init>()V

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Point;

    invoke-virtual {v4, v0}, Leo;->a(Landroid/graphics/Point;)V

    add-int/lit8 v0, v1, 0x1

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Point;

    invoke-virtual {v4, v0}, Leo;->b(Landroid/graphics/Point;)V

    invoke-direct {p0, p1, v4}, Lcom/samsung/vip/engine/c;->a(Ljava/util/ArrayList;Leo;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private a(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 6

    const/4 v4, 0x1

    const/4 v3, 0x0

    move v2, v3

    :goto_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v2, v0, :cond_0

    move v0, v3

    :goto_1
    if-eqz v0, :cond_3

    invoke-virtual {p3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leo;

    invoke-direct {p0, p1, v0}, Lcom/samsung/vip/engine/c;->a(Ljava/util/ArrayList;Leo;)V

    const/4 v0, 0x2

    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leo;

    invoke-direct {p0, p1, v0}, Lcom/samsung/vip/engine/c;->a(Ljava/util/ArrayList;Leo;)V

    :goto_2
    return-void

    :cond_0
    invoke-virtual {p3, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leo;

    new-instance v5, Leo;

    invoke-direct {v5}, Leo;-><init>()V

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Leh;

    invoke-virtual {v1}, Leh;->a()Landroid/graphics/Point;

    move-result-object v1

    invoke-virtual {v5, v1}, Leo;->a(Landroid/graphics/Point;)V

    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Leh;

    invoke-virtual {v1}, Leh;->b()Landroid/graphics/Point;

    move-result-object v1

    invoke-virtual {v5, v1}, Leo;->b(Landroid/graphics/Point;)V

    invoke-virtual {v0, v5, v4}, Leo;->a(Leo;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v4

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    invoke-virtual {p3, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leo;

    invoke-direct {p0, p1, v0}, Lcom/samsung/vip/engine/c;->a(Ljava/util/ArrayList;Leo;)V

    add-int/lit8 v3, v3, 0x1

    :cond_3
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v3, v0, :cond_2

    goto :goto_2
.end method

.method private a(Ljava/util/ArrayList;[S)V
    .locals 8

    const/4 v3, 0x1

    const/4 v0, 0x0

    aget-short v1, p2, v3

    add-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x1

    aget-short v2, p2, v1

    aget-short v1, p2, v3

    add-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x2

    aget-short v3, p2, v1

    move v1, v0

    :goto_0
    if-lt v1, v2, :cond_0

    :goto_1
    if-lt v0, v3, :cond_1

    return-void

    :cond_0
    new-instance v4, Leo;

    invoke-direct {v4}, Leo;-><init>()V

    new-instance v5, Landroid/graphics/Point;

    mul-int/lit8 v6, v1, 0x4

    add-int/lit8 v6, v6, 0x2

    add-int/lit8 v6, v6, 0x0

    aget-short v6, p2, v6

    mul-int/lit8 v7, v1, 0x4

    add-int/lit8 v7, v7, 0x2

    add-int/lit8 v7, v7, 0x1

    aget-short v7, p2, v7

    invoke-direct {v5, v6, v7}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {v4, v5}, Leo;->a(Landroid/graphics/Point;)V

    new-instance v5, Landroid/graphics/Point;

    mul-int/lit8 v6, v1, 0x4

    add-int/lit8 v6, v6, 0x2

    add-int/lit8 v6, v6, 0x2

    aget-short v6, p2, v6

    mul-int/lit8 v7, v1, 0x4

    add-int/lit8 v7, v7, 0x2

    add-int/lit8 v7, v7, 0x3

    aget-short v7, p2, v7

    invoke-direct {v5, v6, v7}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {v4, v5}, Leo;->b(Landroid/graphics/Point;)V

    invoke-direct {p0, p1, v4}, Lcom/samsung/vip/engine/c;->a(Ljava/util/ArrayList;Leo;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    new-instance v1, Leo;

    invoke-direct {v1}, Leo;-><init>()V

    new-instance v4, Landroid/graphics/Point;

    mul-int/lit8 v5, v2, 0x4

    add-int/lit8 v5, v5, 0x2

    mul-int/lit8 v6, v0, 0x4

    add-int/2addr v5, v6

    add-int/lit8 v5, v5, 0x0

    aget-short v5, p2, v5

    mul-int/lit8 v6, v2, 0x4

    add-int/lit8 v6, v6, 0x2

    mul-int/lit8 v7, v0, 0x4

    add-int/2addr v6, v7

    add-int/lit8 v6, v6, 0x1

    aget-short v6, p2, v6

    invoke-direct {v4, v5, v6}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {v1, v4}, Leo;->a(Landroid/graphics/Point;)V

    new-instance v4, Landroid/graphics/Point;

    mul-int/lit8 v5, v2, 0x4

    add-int/lit8 v5, v5, 0x2

    mul-int/lit8 v6, v0, 0x4

    add-int/2addr v5, v6

    add-int/lit8 v5, v5, 0x2

    aget-short v5, p2, v5

    mul-int/lit8 v6, v2, 0x4

    add-int/lit8 v6, v6, 0x2

    mul-int/lit8 v7, v0, 0x4

    add-int/2addr v6, v7

    add-int/lit8 v6, v6, 0x3

    aget-short v6, p2, v6

    invoke-direct {v4, v5, v6}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {v1, v4}, Leo;->b(Landroid/graphics/Point;)V

    invoke-direct {p0, p1, v1}, Lcom/samsung/vip/engine/c;->a(Ljava/util/ArrayList;Leo;)V

    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1
.end method

.method private a(Ljava/util/ArrayList;[SLjava/util/ArrayList;)V
    .locals 9

    const/4 v8, 0x5

    const/4 v3, 0x2

    const/4 v1, 0x0

    aget-short v4, p2, v1

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move v2, v1

    move v0, v3

    :goto_0
    if-lt v2, v4, :cond_0

    if-eqz p3, :cond_b

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_b

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v8, :cond_b

    invoke-direct {p0, p1, p3, v5}, Lcom/samsung/vip/engine/c;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    :goto_1
    return-void

    :cond_0
    aget-short v6, p2, v0

    add-int/lit8 v0, v0, 0x2

    const/4 v7, 0x1

    if-ne v6, v7, :cond_2

    new-instance v6, Leo;

    invoke-direct {v6}, Leo;-><init>()V

    invoke-direct {p0, v6, p2, v0}, Lcom/samsung/vip/engine/c;->a(Leo;[SI)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x8

    :cond_1
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    if-ne v6, v3, :cond_3

    new-instance v6, Lek;

    invoke-direct {v6}, Lek;-><init>()V

    invoke-direct {p0, v6, p2, v0}, Lcom/samsung/vip/engine/c;->a(Lek;[SI)V

    invoke-direct {p0, p1, v6}, Lcom/samsung/vip/engine/c;->a(Ljava/util/ArrayList;Lek;)V

    add-int/lit8 v0, v0, 0x6

    goto :goto_2

    :cond_3
    const/4 v7, 0x6

    if-ne v6, v7, :cond_4

    new-instance v6, Lem;

    invoke-direct {v6}, Lem;-><init>()V

    invoke-direct {p0, v6, p2, v0}, Lcom/samsung/vip/engine/c;->a(Lem;[SI)V

    invoke-direct {p0, p1, v6}, Lcom/samsung/vip/engine/c;->a(Ljava/util/ArrayList;Lem;)V

    add-int/lit8 v0, v0, 0xa

    goto :goto_2

    :cond_4
    const/4 v7, 0x7

    if-ne v6, v7, :cond_5

    new-instance v6, Lel;

    invoke-direct {v6}, Lel;-><init>()V

    invoke-direct {p0, v6, p2, v0}, Lcom/samsung/vip/engine/c;->a(Lel;[SI)V

    invoke-direct {p0, p1, v6}, Lcom/samsung/vip/engine/c;->a(Ljava/util/ArrayList;Lel;)V

    add-int/lit8 v0, v0, 0xe

    goto :goto_2

    :cond_5
    const/16 v7, 0x8

    if-ne v6, v7, :cond_6

    new-instance v6, Len;

    invoke-direct {v6}, Len;-><init>()V

    invoke-direct {p0, v6, p2, v0}, Lcom/samsung/vip/engine/c;->a(Len;[SI)V

    invoke-direct {p0, p1, v6}, Lcom/samsung/vip/engine/c;->a(Ljava/util/ArrayList;Len;)V

    add-int/lit8 v0, v0, 0x14

    goto :goto_2

    :cond_6
    if-ne v6, v8, :cond_7

    new-instance v6, Lep;

    invoke-direct {v6}, Lep;-><init>()V

    invoke-direct {p0, v6, p2, v0}, Lcom/samsung/vip/engine/c;->a(Lep;[SI)V

    invoke-direct {p0, p1, v6}, Lcom/samsung/vip/engine/c;->a(Ljava/util/ArrayList;Lep;)V

    invoke-virtual {v6}, Lep;->a()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    mul-int/lit8 v6, v6, 0x4

    add-int/lit8 v6, v6, 0x2

    add-int/2addr v0, v6

    goto :goto_2

    :cond_7
    const/16 v7, 0xd

    if-ne v6, v7, :cond_8

    new-instance v6, Leq;

    invoke-direct {v6}, Leq;-><init>()V

    invoke-direct {p0, v6, p2, v0}, Lcom/samsung/vip/engine/c;->a(Leq;[SI)V

    invoke-direct {p0, p1, v6}, Lcom/samsung/vip/engine/c;->a(Ljava/util/ArrayList;Leq;)V

    invoke-virtual {v6}, Leq;->a()Ljava/util/ArrayList;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    mul-int/lit8 v6, v6, 0x4

    add-int/lit8 v6, v6, 0x2

    add-int/2addr v0, v6

    goto :goto_2

    :cond_8
    const/16 v7, 0x9

    if-ne v6, v7, :cond_9

    new-instance v6, Lej;

    invoke-direct {v6}, Lej;-><init>()V

    invoke-direct {p0, v6, p2, v0}, Lcom/samsung/vip/engine/c;->a(Lej;[SI)V

    invoke-direct {p0, p1, v6}, Lcom/samsung/vip/engine/c;->a(Ljava/util/ArrayList;Lej;)V

    invoke-virtual {v6}, Lej;->a()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    mul-int/lit8 v6, v6, 0x2

    mul-int/lit8 v6, v6, 0x4

    add-int/lit8 v6, v6, 0x2

    add-int/2addr v0, v6

    goto/16 :goto_2

    :cond_9
    const/16 v7, 0xb

    if-ne v6, v7, :cond_1

    new-instance v6, Leh;

    invoke-direct {v6}, Leh;-><init>()V

    invoke-direct {p0, v6, p2, v0}, Lcom/samsung/vip/engine/c;->a(Leh;[SI)V

    invoke-direct {p0, p1, v6}, Lcom/samsung/vip/engine/c;->a(Ljava/util/ArrayList;Leh;)V

    add-int/lit8 v0, v0, 0x10

    goto/16 :goto_2

    :cond_a
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Leo;

    invoke-direct {p0, p1, v0}, Lcom/samsung/vip/engine/c;->a(Ljava/util/ArrayList;Leo;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    :cond_b
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_a

    goto/16 :goto_1
.end method

.method private b([S)I
    .locals 3

    const/4 v2, 0x0

    aget-short v0, p1, v2

    const v1, 0xffff

    and-int/2addr v0, v1

    or-int/2addr v0, v2

    const/4 v1, 0x1

    aget-short v1, p1, v1

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    return v0
.end method

.method private b(Ljava/util/ArrayList;Len;)V
    .locals 12

    const/4 v11, 0x1

    const/4 v5, -0x1

    const-wide v9, 0x401921fb54442d18L    # 6.283185307179586

    invoke-virtual {p2}, Len;->c()Landroid/graphics/Point;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/vip/engine/c;->a(Landroid/graphics/Point;)Landroid/graphics/PointF;

    move-result-object v1

    invoke-virtual {p2}, Len;->b()Landroid/graphics/Point;

    move-result-object v2

    invoke-virtual {p2}, Len;->g()F

    move-result v3

    invoke-virtual {p2}, Len;->h()F

    move-result v4

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/vip/engine/c;->a(Landroid/graphics/PointF;Landroid/graphics/Point;FFI)Landroid/graphics/PointF;

    move-result-object v6

    invoke-virtual {p2}, Len;->d()Landroid/graphics/Point;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/vip/engine/c;->a(Landroid/graphics/Point;)Landroid/graphics/PointF;

    move-result-object v1

    invoke-virtual {p2}, Len;->b()Landroid/graphics/Point;

    move-result-object v2

    invoke-virtual {p2}, Len;->g()F

    move-result v3

    invoke-virtual {p2}, Len;->h()F

    move-result v4

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/vip/engine/c;->a(Landroid/graphics/PointF;Landroid/graphics/Point;FFI)Landroid/graphics/PointF;

    move-result-object v0

    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1}, Landroid/graphics/PointF;-><init>()V

    invoke-virtual {p2}, Len;->e()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Len;->f()I

    move-result v3

    int-to-float v3, v3

    invoke-direct {p0, v6, v2, v3}, Lcom/samsung/vip/engine/c;->a(Landroid/graphics/PointF;FF)F

    move-result v8

    invoke-virtual {p2}, Len;->e()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Len;->f()I

    move-result v3

    int-to-float v3, v3

    invoke-direct {p0, v0, v2, v3}, Lcom/samsung/vip/engine/c;->a(Landroid/graphics/PointF;FF)F

    move-result v0

    invoke-virtual {p2}, Len;->a()I

    move-result v2

    if-ne v2, v11, :cond_1

    cmpl-float v2, v8, v0

    if-lez v2, :cond_0

    float-to-double v2, v0

    add-double/2addr v2, v9

    double-to-float v0, v2

    :cond_0
    :goto_0
    sub-float v2, v0, v8

    invoke-virtual {p2}, Len;->f()I

    move-result v0

    invoke-virtual {p2}, Len;->e()I

    move-result v3

    add-int/2addr v0, v3

    int-to-float v0, v0

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v3

    mul-float/2addr v0, v3

    const v3, 0x453b8000    # 3000.0f

    cmpl-float v0, v0, v3

    if-lez v0, :cond_2

    const/16 v0, 0x80

    move v6, v0

    :goto_1
    int-to-float v0, v6

    div-float v9, v2, v0

    add-int/lit8 v0, v6, 0x1

    new-array v10, v0, [Landroid/graphics/PointF;

    const/4 v0, 0x0

    move v7, v0

    :goto_2
    add-int/lit8 v0, v6, 0x1

    if-lt v7, v0, :cond_3

    invoke-virtual {p1, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void

    :cond_1
    cmpg-float v2, v8, v0

    if-gez v2, :cond_0

    float-to-double v2, v0

    sub-double/2addr v2, v9

    double-to-float v0, v2

    goto :goto_0

    :cond_2
    const/16 v0, 0x40

    move v6, v0

    goto :goto_1

    :cond_3
    int-to-float v0, v7

    mul-float/2addr v0, v9

    add-float/2addr v0, v8

    invoke-virtual {p2}, Len;->e()I

    move-result v2

    int-to-double v2, v2

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    double-to-int v2, v2

    int-to-float v2, v2

    iput v2, v1, Landroid/graphics/PointF;->x:F

    invoke-virtual {p2}, Len;->f()I

    move-result v2

    int-to-double v2, v2

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    mul-double/2addr v2, v4

    double-to-int v0, v2

    int-to-float v0, v0

    iput v0, v1, Landroid/graphics/PointF;->y:F

    invoke-virtual {p2}, Len;->b()Landroid/graphics/Point;

    move-result-object v2

    invoke-virtual {p2}, Len;->g()F

    move-result v3

    invoke-virtual {p2}, Len;->h()F

    move-result v4

    move-object v0, p0

    move v5, v11

    invoke-direct/range {v0 .. v5}, Lcom/samsung/vip/engine/c;->a(Landroid/graphics/PointF;Landroid/graphics/Point;FFI)Landroid/graphics/PointF;

    move-result-object v0

    new-instance v2, Landroid/graphics/PointF;

    iget v3, v0, Landroid/graphics/PointF;->x:F

    iget v0, v0, Landroid/graphics/PointF;->y:F

    invoke-direct {v2, v3, v0}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v2, v10, v7

    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_2
.end method


# virtual methods
.method public a(Lef;)V
    .locals 1

    sget-object v0, Lcom/samsung/vip/engine/c;->a:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/samsung/vip/engine/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 5

    const/4 v4, 0x0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sget-object v0, Lcom/samsung/vip/engine/c;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lef;

    iget-short v3, v0, Lef;->b:S

    iget-object v0, v0, Lef;->c:[S

    packed-switch v3, :pswitch_data_0

    :goto_1
    :pswitch_0
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/graphics/PointF;

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :pswitch_1
    new-instance v3, Leo;

    invoke-direct {v3}, Leo;-><init>()V

    invoke-direct {p0, v3, v0, v4}, Lcom/samsung/vip/engine/c;->a(Leo;[SI)V

    invoke-direct {p0, p1, v3}, Lcom/samsung/vip/engine/c;->a(Ljava/util/ArrayList;Leo;)V

    goto :goto_1

    :pswitch_2
    new-instance v3, Lek;

    invoke-direct {v3}, Lek;-><init>()V

    invoke-direct {p0, v3, v0, v4}, Lcom/samsung/vip/engine/c;->a(Lek;[SI)V

    invoke-direct {p0, p1, v3}, Lcom/samsung/vip/engine/c;->a(Ljava/util/ArrayList;Lek;)V

    goto :goto_1

    :pswitch_3
    new-instance v3, Lem;

    invoke-direct {v3}, Lem;-><init>()V

    invoke-direct {p0, v3, v0, v4}, Lcom/samsung/vip/engine/c;->a(Lem;[SI)V

    invoke-direct {p0, p1, v3}, Lcom/samsung/vip/engine/c;->a(Ljava/util/ArrayList;Lem;)V

    goto :goto_1

    :pswitch_4
    new-instance v3, Lel;

    invoke-direct {v3}, Lel;-><init>()V

    invoke-direct {p0, v3, v0, v4}, Lcom/samsung/vip/engine/c;->a(Lel;[SI)V

    invoke-direct {p0, p1, v3}, Lcom/samsung/vip/engine/c;->a(Ljava/util/ArrayList;Lel;)V

    goto :goto_1

    :pswitch_5
    new-instance v3, Len;

    invoke-direct {v3}, Len;-><init>()V

    invoke-direct {p0, v3, v0, v4}, Lcom/samsung/vip/engine/c;->a(Len;[SI)V

    invoke-direct {p0, p1, v3}, Lcom/samsung/vip/engine/c;->a(Ljava/util/ArrayList;Len;)V

    goto :goto_1

    :pswitch_6
    new-instance v3, Lep;

    invoke-direct {v3}, Lep;-><init>()V

    invoke-direct {p0, v3, v0, v4}, Lcom/samsung/vip/engine/c;->a(Lep;[SI)V

    invoke-direct {p0, p1, v3}, Lcom/samsung/vip/engine/c;->a(Ljava/util/ArrayList;Lep;)V

    goto :goto_1

    :pswitch_7
    new-instance v3, Leq;

    invoke-direct {v3}, Leq;-><init>()V

    invoke-direct {p0, v3, v0, v4}, Lcom/samsung/vip/engine/c;->a(Leq;[SI)V

    invoke-direct {p0, p1, v3}, Lcom/samsung/vip/engine/c;->a(Ljava/util/ArrayList;Leq;)V

    goto :goto_1

    :pswitch_8
    new-instance v3, Leh;

    invoke-direct {v3}, Leh;-><init>()V

    invoke-direct {p0, v3, v0, v4}, Lcom/samsung/vip/engine/c;->a(Leh;[SI)V

    invoke-direct {p0, p1, v3}, Lcom/samsung/vip/engine/c;->a(Ljava/util/ArrayList;Leh;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_9
    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/vip/engine/c;->a(Ljava/util/ArrayList;[SLjava/util/ArrayList;)V

    goto :goto_1

    :pswitch_a
    new-instance v3, Lej;

    invoke-direct {v3}, Lej;-><init>()V

    invoke-direct {p0, v3, v0, v4}, Lcom/samsung/vip/engine/c;->a(Lej;[SI)V

    invoke-direct {p0, p1, v3}, Lcom/samsung/vip/engine/c;->a(Ljava/util/ArrayList;Lej;)V

    goto :goto_1

    :pswitch_b
    invoke-direct {p0, p1, v0}, Lcom/samsung/vip/engine/c;->a(Ljava/util/ArrayList;[S)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_b
        :pswitch_7
    .end packed-switch
.end method
