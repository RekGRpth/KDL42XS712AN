.class public Lcom/samsung/vip/engine/VITextAllRecognitionLib;
.super Lcom/samsung/vip/engine/e;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/vip/engine/e;-><init>()V

    return-void
.end method

.method private native VIHW_AddStroke([F[F)V
.end method

.method private native VIHW_Close()V
.end method

.method private native VIHW_GetResult()Ljava/lang/String;
.end method

.method private native VIHW_Init(Ljava/lang/String;IIIS)I
.end method

.method private native VIHW_Recog(I)I
.end method

.method private native VIHW_Recog(I[II)I
.end method


# virtual methods
.method protected a(I)I
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/vip/engine/VITextAllRecognitionLib;->VIHW_Recog(I)I

    move-result v0

    return v0
.end method

.method protected a(I[II)I
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/vip/engine/VITextAllRecognitionLib;->VIHW_Recog(I[II)I

    move-result v0

    return v0
.end method

.method protected a(Ljava/lang/String;IIIS)I
    .locals 1

    invoke-direct/range {p0 .. p5}, Lcom/samsung/vip/engine/VITextAllRecognitionLib;->VIHW_Init(Ljava/lang/String;IIIS)I

    move-result v0

    return v0
.end method

.method protected a()V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/vip/engine/VITextAllRecognitionLib;->VIHW_Close()V

    return-void
.end method

.method protected a([F[F)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/samsung/vip/engine/VITextAllRecognitionLib;->VIHW_AddStroke([F[F)V

    return-void
.end method

.method protected b()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/samsung/vip/engine/VITextAllRecognitionLib;->VIHW_GetResult()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
