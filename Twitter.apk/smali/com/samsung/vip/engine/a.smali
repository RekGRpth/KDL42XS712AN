.class public Lcom/samsung/vip/engine/a;
.super Lcom/samsung/vip/engine/VIRecognitionLib;
.source "Twttr"


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;

.field private static final d:[Ljava/lang/String;

.field private static final e:[Ljava/lang/String;

.field private static final f:[Ljava/lang/String;

.field private static final g:[Ljava/lang/String;

.field private static final h:[Ljava/lang/String;

.field private static final i:[Ljava/lang/String;

.field private static final j:[Ljava/lang/String;

.field private static final k:[Ljava/lang/String;

.field private static final l:[Ljava/lang/String;

.field private static final m:[Ljava/lang/String;

.field private static final n:[Ljava/lang/String;

.field private static final o:[Ljava/lang/String;

.field private static final p:[Ljava/lang/String;

.field private static final q:[Ljava/lang/String;


# instance fields
.field private r:Ljava/util/LinkedList;

.field private s:Ljava/util/LinkedList;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "#"

    aput-object v1, v0, v2

    const-string/jumbo v1, "\\frac"

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/vip/engine/a;->a:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "%"

    aput-object v1, v0, v2

    const-string/jumbo v1, "\\div"

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/vip/engine/a;->b:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "*"

    aput-object v1, v0, v2

    const-string/jumbo v1, "\\times"

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/vip/engine/a;->c:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "pi"

    aput-object v1, v0, v2

    const-string/jumbo v1, "\\pi"

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/vip/engine/a;->d:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "sin"

    aput-object v1, v0, v2

    const-string/jumbo v1, "\\sin"

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/vip/engine/a;->e:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "cos"

    aput-object v1, v0, v2

    const-string/jumbo v1, "\\cos"

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/vip/engine/a;->f:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "tan"

    aput-object v1, v0, v2

    const-string/jumbo v1, "\\tan"

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/vip/engine/a;->g:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "asin"

    aput-object v1, v0, v2

    const-string/jumbo v1, "\\arcsin"

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/vip/engine/a;->h:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "acos"

    aput-object v1, v0, v2

    const-string/jumbo v1, "\\arccos"

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/vip/engine/a;->i:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "atan"

    aput-object v1, v0, v2

    const-string/jumbo v1, "\\arctan"

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/vip/engine/a;->j:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "sinh"

    aput-object v1, v0, v2

    const-string/jumbo v1, "\\sinh"

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/vip/engine/a;->k:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "cosh"

    aput-object v1, v0, v2

    const-string/jumbo v1, "\\cosh"

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/vip/engine/a;->l:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "tanh"

    aput-object v1, v0, v2

    const-string/jumbo v1, "\\tanh"

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/vip/engine/a;->m:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "log"

    aput-object v1, v0, v2

    const-string/jumbo v1, "\\log"

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/vip/engine/a;->n:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "ln"

    aput-object v1, v0, v2

    const-string/jumbo v1, "\\ln"

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/vip/engine/a;->o:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "sqrt"

    aput-object v1, v0, v2

    const-string/jumbo v1, "\\sqrt"

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/vip/engine/a;->p:[Ljava/lang/String;

    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v1, "^"

    aput-object v1, v0, v2

    const-string/jumbo v1, "^"

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/vip/engine/a;->q:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/samsung/vip/engine/VIRecognitionLib;-><init>()V

    iput-object v0, p0, Lcom/samsung/vip/engine/a;->r:Ljava/util/LinkedList;

    iput-object v0, p0, Lcom/samsung/vip/engine/a;->s:Ljava/util/LinkedList;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)I
    .locals 3

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/samsung/vip/engine/a;->r:Ljava/util/LinkedList;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/samsung/vip/engine/a;->s:Ljava/util/LinkedList;

    const/high16 v0, 0x40000

    const/16 v1, 0x640

    const/16 v2, 0x4b0

    invoke-virtual {p0, p1, v0, v1, v2}, Lcom/samsung/vip/engine/a;->VIEQ_Init(Ljava/lang/String;III)I

    move-result v0

    return v0
.end method

.method public a()V
    .locals 0

    invoke-virtual {p0}, Lcom/samsung/vip/engine/a;->VIEQ_Close()V

    return-void
.end method

.method public a([F[F)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/vip/engine/a;->r:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/samsung/vip/engine/a;->s:Ljava/util/LinkedList;

    invoke-virtual {v0, p2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 12

    const/4 v1, 0x0

    const v11, 0xffff

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/samsung/vip/engine/a;->r:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v6

    move v2, v3

    move v4, v3

    :goto_0
    if-lt v2, v6, :cond_1

    add-int/lit8 v7, v4, 0x1

    mul-int/lit8 v0, v7, 0x2

    new-array v8, v0, [I

    move v5, v3

    move v2, v3

    :goto_1
    if-lt v5, v6, :cond_3

    add-int/lit8 v0, v2, 0x1

    aput v11, v8, v2

    add-int/lit8 v2, v0, 0x1

    aput v11, v8, v0

    iget-object v0, p0, Lcom/samsung/vip/engine/a;->r:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    iget-object v0, p0, Lcom/samsung/vip/engine/a;->s:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    invoke-virtual {p0, v8, v7}, Lcom/samsung/vip/engine/a;->VIEQ_Recog([II)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "VIEquationRecognitionLib"

    const-string/jumbo v2, "Output result is null!"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    :cond_0
    :goto_2
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/vip/engine/a;->r:Ljava/util/LinkedList;

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    array-length v0, v0

    add-int v5, v4, v0

    add-int/lit8 v5, v5, 0x2

    const/16 v7, 0x7d0

    if-le v5, v7, :cond_2

    const-string/jumbo v0, "VIEquationRecognitionLib"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Too many input points! : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    goto :goto_2

    :cond_2
    add-int/lit8 v0, v0, 0x1

    add-int/2addr v4, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/samsung/vip/engine/a;->r:Ljava/util/LinkedList;

    invoke-virtual {v0, v5}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    array-length v9, v0

    move v4, v2

    move v2, v3

    :goto_3
    if-lt v2, v9, :cond_4

    add-int/lit8 v0, v4, 0x1

    aput v11, v8, v4

    add-int/lit8 v2, v0, 0x1

    aput v3, v8, v0

    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_1

    :cond_4
    add-int/lit8 v10, v4, 0x1

    iget-object v0, p0, Lcom/samsung/vip/engine/a;->r:Ljava/util/LinkedList;

    invoke-virtual {v0, v5}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    aget v0, v0, v2

    float-to-int v0, v0

    aput v0, v8, v4

    add-int/lit8 v4, v10, 0x1

    iget-object v0, p0, Lcom/samsung/vip/engine/a;->s:Ljava/util/LinkedList;

    invoke-virtual {v0, v5}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    aget v0, v0, v2

    float-to-int v0, v0

    aput v0, v8, v10

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3
.end method
