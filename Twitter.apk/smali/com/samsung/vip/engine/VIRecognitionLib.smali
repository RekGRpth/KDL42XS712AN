.class public Lcom/samsung/vip/engine/VIRecognitionLib;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected native VIEQ_Close()V
.end method

.method protected native VIEQ_Init(Ljava/lang/String;III)I
.end method

.method protected native VIEQ_Recog([II)Ljava/lang/String;
.end method

.method protected native VISH_ClearScene()V
.end method

.method protected native VISH_ClearSelected()V
.end method

.method protected native VISH_DeObject()V
.end method

.method protected native VISH_DeleteChoosenPrimitive()S
.end method

.method protected native VISH_GetGraphPrimitiveArray()[Lef;
.end method

.method protected native VISH_GetLastShapeType()I
.end method

.method protected native VISH_GetLastStrokeBreakPoints()[I
.end method

.method protected native VISH_InitSmartShapeEngine(I)V
.end method

.method protected native VISH_JoinSelectedObject()V
.end method

.method protected native VISH_PrimitiveRotate([I[I)I
.end method

.method protected native VISH_PrimitiveTranslate([F)I
.end method

.method protected native VISH_PrimitiveZoom([I[I)I
.end method

.method protected native VISH_ReleaseSmartShapeEngine()V
.end method

.method protected native VISH_SearchPrimitive(I[I)S
.end method

.method protected native VISH_UpdateMovePrimitivesData([F)V
.end method

.method protected native VISH_UpdateScene([I)V
.end method
