.class Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final CHANGE_STROKE_FRAME_BEAUTIFY_DESCRIPTION_NAME:Ljava/lang/String; = "string_transform_into_auto_shape"

.field private static final CHANGE_STROKE_FRAME_BEAUTIFY_DRAWABLE_NAME:Ljava/lang/String; = "snote_photoframe_refine"

.field private static final CHANGE_STROKE_FRAME_BG_DRAWABLE_NAME:Ljava/lang/String; = "selector_change_stroke_frame_bg"

.field private static final CHANGE_STROKE_FRAME_IMAGE_UX_TABLE:Ljava/util/HashMap;

.field private static final CHANGE_STROKE_FRAME_ORIGINAL_DESCRIPTION_NAME:Ljava/lang/String; = "string_transform_back_to_original_shape"

.field private static final CHANGE_STROKE_FRAME_ORIGINAL_DRAWABLE_NAME:Ljava/lang/String; = "snote_photoframe_undo"

.field private static HASH_KEY_IMAGE_MARGIN:I = 0x0

.field private static HASH_KEY_IMAGE_MARGIN_WHEN_BOTTOM:I = 0x0

.field private static HASH_KEY_IMAGE_MARGIN_WHEN_TOP:I = 0x0

.field private static HASH_KEY_IMAGE_SIZE:I = 0x0

.field private static HASH_KEY_PIXEL_1080_1920:I = 0x0

.field private static HASH_KEY_PIXEL_2560_1600:I = 0x0

.field private static HASH_KEY_PIXEL_720_1280:I = 0x0

.field private static HASH_KEY_PIXEL_DEFAULT:I = 0x0

.field private static final PIXEL_TO_ZOOM_STANDARD_VALUE:I = 0x5

.field private static final SIGMA:F = 1.0E-4f

.field private static final STROKE_FRAME_MIN_WIDTH_HEIGHT:I = 0xa

.field private static final TAG:Ljava/lang/String; = "SpenStrokeFrame"


# instance fields
.field private dm:Landroid/util/DisplayMetrics;

.field private mActivity:Landroid/app/Activity;

.field private mBeautifyContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

.field private mBeautifyImage:Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

.field private mBeautifyStoke:Ljava/util/ArrayList;

.field private mBgBitmap:Landroid/graphics/Bitmap;

.field private mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

.field private mCameraViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

.field private mChangeFrameBeautifyDrawable:Landroid/graphics/drawable/Drawable;

.field private mChangeFrameBgDrawable:Landroid/graphics/drawable/Drawable;

.field private mChangeFrameButton:Landroid/widget/ImageButton;

.field private mChangeFrameDescriptionBeautify:Ljava/lang/String;

.field private mChangeFrameDescriptionOriginal:Ljava/lang/String;

.field private mChangeFrameOriginalDrawable:Landroid/graphics/drawable/Drawable;

.field private mFrameShapePath:Landroid/graphics/Path;

.field private mObjectContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

.field private mOriginalContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

.field private mOriginalImage:Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

.field private mOriginalStroke:Ljava/util/ArrayList;

.field private mPageHeight:I

.field private mPageWidth:I

.field private mPan:Landroid/graphics/PointF;

.field private mPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

.field private mPenSize:F

.field private final mPrevTouchPoint:Landroid/graphics/PointF;

.field mPreviewCallback:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnPreviewCallback;

.field private mPreviewRatio:F

.field private mRatio:F

.field private mRect:Landroid/graphics/RectF;

.field private mShapeMaskBitmap:Landroid/graphics/Bitmap;

.field private mShapeMaskLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

.field private mShapeMaskView:Landroid/widget/ImageView;

.field private mShapeRecognition:Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;

.field private mSpenSurfaceViewBitmap:Landroid/graphics/Bitmap;

.field private mStartFramePosition:Landroid/graphics/PointF;

.field private mStrokeFrameAction:I

.field private mStrokeFrameType:I

.field private final mTouchCheckPath:Landroid/graphics/Path;

.field private mTouchImage:Landroid/widget/ImageView;

.field private mViewGroup:Landroid/view/ViewGroup;

.field private mWorkBitmap:Landroid/graphics/Bitmap;

.field private mZoomCur:I

.field private mZoomLast:I

.field private mZoomMode:I

.field private mZoomPrev:I

.field private mZoomY:F

.field private updateListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    const/4 v8, 0x2

    const/4 v7, 0x5

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x0

    sput v4, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_SIZE:I

    sget v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_SIZE:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN:I

    sget v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN_WHEN_TOP:I

    sget v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN_WHEN_TOP:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN_WHEN_BOTTOM:I

    const/16 v0, 0xbb8

    sput v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_PIXEL_1080_1920:I

    const/16 v0, 0x1040

    sput v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_PIXEL_2560_1600:I

    const/16 v0, 0x7d0

    sput v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_PIXEL_720_1280:I

    sget v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_PIXEL_1080_1920:I

    sput v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_PIXEL_DEFAULT:I

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->CHANGE_STROKE_FRAME_IMAGE_UX_TABLE:Ljava/util/HashMap;

    new-instance v0, Landroid/graphics/Rect;

    const/16 v1, 0x27

    const/16 v2, 0x27

    invoke-direct {v0, v4, v4, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, v4, v4, v5, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sget v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_SIZE:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Landroid/graphics/Rect;

    const/4 v1, 0x4

    invoke-direct {v0, v4, v4, v1, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    sget v1, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN_WHEN_TOP:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v4, v8, v4, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    sget v1, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN_WHEN_BOTTOM:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->CHANGE_STROKE_FRAME_IMAGE_UX_TABLE:Ljava/util/HashMap;

    sget v1, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_PIXEL_1080_1920:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Landroid/graphics/Rect;

    const/16 v1, 0x3a

    const/16 v2, 0x3b

    invoke-direct {v0, v4, v4, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, v4, v4, v5, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sget v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_SIZE:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v4, v4, v7, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    sget v1, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN_WHEN_TOP:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v4, v6, v4, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->CHANGE_STROKE_FRAME_IMAGE_UX_TABLE:Ljava/util/HashMap;

    sget v1, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_PIXEL_2560_1600:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Landroid/graphics/Rect;

    const/16 v1, 0x23

    const/16 v2, 0x23

    invoke-direct {v0, v4, v4, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, v4, v4, v5, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sget v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_SIZE:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Landroid/graphics/Rect;

    const/4 v1, 0x4

    invoke-direct {v0, v4, v4, v1, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    sget v1, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN_WHEN_TOP:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v4, v8, v4, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    sget v1, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN_WHEN_BOTTOM:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->CHANGE_STROKE_FRAME_IMAGE_UX_TABLE:Ljava/util/HashMap;

    sget v1, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_PIXEL_720_1280:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mTouchCheckPath:Landroid/graphics/Path;

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPrevTouchPoint:Landroid/graphics/PointF;

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStrokeFrameType:I

    return v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStrokeFrameType:I

    return-void
.end method

.method static synthetic access$10(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/RectF;)Landroid/graphics/RectF;
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->convertRelative(Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$11(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Path;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->createShapeMaskBitmap(Landroid/graphics/Path;)V

    return-void
.end method

.method static synthetic access$12(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$13(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;Landroid/graphics/RectF;Landroid/graphics/RectF;)Z
    .locals 1

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->drawPen(Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V

    return-void
.end method

.method static synthetic access$15(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/RectF;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->setRect(Landroid/graphics/RectF;)V

    return-void
.end method

.method static synthetic access$16(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->updateChangeFrameButtonPosition()V

    return-void
.end method

.method static synthetic access$17(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$18(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    return-object v0
.end method

.method static synthetic access$19(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    return-object v0
.end method

.method static synthetic access$20(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;F)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPreviewRatio:F

    return-void
.end method

.method static synthetic access$21(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)F
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPreviewRatio:F

    return v0
.end method

.method static synthetic access$22(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/widget/RelativeLayout$LayoutParams;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    return-void
.end method

.method static synthetic access$23(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/widget/RelativeLayout$LayoutParams;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    return-object v0
.end method

.method static synthetic access$24(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Path;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mTouchCheckPath:Landroid/graphics/Path;

    return-object v0
.end method

.method static synthetic access$25(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/PointF;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPrevTouchPoint:Landroid/graphics/PointF;

    return-object v0
.end method

.method static synthetic access$26(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomMode:I

    return-void
.end method

.method static synthetic access$27(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;F)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomY:F

    return-void
.end method

.method static synthetic access$28(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomCur:I

    return v0
.end method

.method static synthetic access$29(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomLast:I

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->makePath(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$30(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomPrev:I

    return-void
.end method

.method static synthetic access$31(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)F
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomY:F

    return v0
.end method

.method static synthetic access$32(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomLast:I

    return v0
.end method

.method static synthetic access$33(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomCur:I

    return-void
.end method

.method static synthetic access$34(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomPrev:I

    return v0
.end method

.method static synthetic access$35(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomMode:I

    return v0
.end method

.method static synthetic access$36(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mWorkBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$37(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mWorkBitmap:Landroid/graphics/Bitmap;

    return-void
.end method

.method static synthetic access$38(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBgBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$39(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBgBitmap:Landroid/graphics/Bitmap;

    return-void
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Path;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mFrameShapePath:Landroid/graphics/Path;

    return-void
.end method

.method static synthetic access$40(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskBitmap:Landroid/graphics/Bitmap;

    return-void
.end method

.method static synthetic access$41(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cleanResource()V

    return-void
.end method

.method static synthetic access$42(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->createRotateBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$43(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;Landroid/graphics/RectF;)Landroid/graphics/Bitmap;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->createSizeFitableBitmap(Landroid/graphics/Bitmap;Landroid/graphics/RectF;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$44(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;Landroid/graphics/Path;)Landroid/graphics/Bitmap;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->createStrokeFrameBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Path;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$45(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectImage;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalImage:Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    return-object v0
.end method

.method static synthetic access$46(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectImage;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyImage:Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    return-object v0
.end method

.method static synthetic access$47(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->updateListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    return-object v0
.end method

.method static synthetic access$48(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPageWidth:I

    return v0
.end method

.method static synthetic access$49(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPageHeight:I

    return v0
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mObjectContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    return-object v0
.end method

.method static synthetic access$50(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/RectF;)Landroid/graphics/RectF;
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->convertAbsolute(Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$51(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mSpenSurfaceViewBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$52(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Z
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->startCamera()Z

    move-result v0

    return v0
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    return-object v0
.end method

.method static synthetic access$7(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->setChangeFrameButtonResources()V

    return-void
.end method

.method static synthetic access$8(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Path;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mFrameShapePath:Landroid/graphics/Path;

    return-object v0
.end method

.method static synthetic access$9(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    return-object v0
.end method

.method private cancel(I)V
    .locals 3

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cleanResource()V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStrokeFrameAction:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->updateListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    or-int/lit8 v1, p1, 0x1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mObjectContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;->onCanceled(ILcom/samsung/android/sdk/pen/document/SpenObjectContainer;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->updateListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    or-int/lit8 v1, p1, 0x2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mObjectContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;->onCanceled(ILcom/samsung/android/sdk/pen/document/SpenObjectContainer;)V

    goto :goto_0
.end method

.method private cleanResource()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeRecognition:Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeRecognition:Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;->setResultListener(Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase$ResultListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v0, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognitionManager;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mActivity:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognitionManager;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeRecognition:Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognitionManager;->destroyRecognition(Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeRecognition:Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameButton:Landroid/widget/ImageButton;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameButton:Landroid/widget/ImageButton;

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mTouchImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mTouchImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mTouchImage:Landroid/widget/ImageView;

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->stop()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskView:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskView:Landroid/widget/ImageView;

    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setReferenceBitmap(Landroid/graphics/Bitmap;)V

    new-instance v0, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mActivity:Landroid/app/Activity;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;->destroyPen(Lcom/samsung/android/sdk/pen/pen/SpenPen;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mWorkBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mWorkBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mWorkBitmap:Landroid/graphics/Bitmap;

    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBgBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBgBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBgBitmap:Landroid/graphics/Bitmap;

    :cond_7
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mSpenSurfaceViewBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mSpenSurfaceViewBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mSpenSurfaceViewBitmap:Landroid/graphics/Bitmap;

    :cond_8
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskBitmap:Landroid/graphics/Bitmap;

    :cond_9
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameOriginalDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_a

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameOriginalDrawable:Landroid/graphics/drawable/Drawable;

    :cond_a
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameBeautifyDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_b

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameBeautifyDrawable:Landroid/graphics/drawable/Drawable;

    :cond_b
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameBgDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_c

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameBgDrawable:Landroid/graphics/drawable/Drawable;

    :cond_c
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mActivity:Landroid/app/Activity;

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0
.end method

.method private convertAbsolute(Landroid/graphics/RectF;)Landroid/graphics/RectF;
    .locals 3

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iget v1, p1, Landroid/graphics/RectF;->left:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStartFramePosition:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRatio:F

    div-float/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPan:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->left:F

    iget v1, p1, Landroid/graphics/RectF;->right:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStartFramePosition:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRatio:F

    div-float/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPan:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    iget v1, p1, Landroid/graphics/RectF;->top:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStartFramePosition:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRatio:F

    div-float/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPan:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->top:F

    iget v1, p1, Landroid/graphics/RectF;->bottom:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStartFramePosition:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRatio:F

    div-float/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPan:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    return-object v0
.end method

.method private convertRelative(Landroid/graphics/RectF;)Landroid/graphics/RectF;
    .locals 3

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iget v1, p1, Landroid/graphics/RectF;->left:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPan:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRatio:F

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStartFramePosition:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->left:F

    iget v1, p1, Landroid/graphics/RectF;->right:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPan:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRatio:F

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStartFramePosition:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    iget v1, p1, Landroid/graphics/RectF;->top:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPan:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRatio:F

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStartFramePosition:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->top:F

    iget v1, p1, Landroid/graphics/RectF;->bottom:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPan:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRatio:F

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStartFramePosition:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    return-object v0
.end method

.method private createPen()Lcom/samsung/android/sdk/pen/pen/SpenPen;
    .locals 5

    const/4 v4, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalStroke:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v2, 0x1

    if-ge v0, v2, :cond_0

    const-string/jumbo v0, "SpenStrokeFrame"

    const-string/jumbo v2, "OriginalStroke Size = 0"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalStroke:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    new-instance v2, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mActivity:Landroid/app/Activity;

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;-><init>(Landroid/content/Context;)V

    :try_start_0
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPenName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;->createPen(Ljava/lang/String;)Lcom/samsung/android/sdk/pen/pen/SpenPen;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->getPenAttribute(I)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPenSize()F

    move-result v2

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPenSize:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPenSize:F

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setSize(F)V

    :cond_1
    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->getPenAttribute(I)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->isCurveEnabled()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setCurveEnabled(Z)V

    :cond_2
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->getPenAttribute(I)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getColor()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setColor(I)V

    :cond_3
    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->getPenAttribute(I)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getAdvancedPenSetting()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setAdvancedSetting(Ljava/lang/String;)V

    :cond_4
    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InstantiationException;->printStackTrace()V

    move-object v0, v1

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    move-object v0, v1

    goto :goto_0

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move-object v0, v1

    goto :goto_0
.end method

.method private createRotateBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 7

    const/4 v1, 0x0

    const/high16 v4, 0x40000000    # 2.0f

    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->getCameraDegree()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v4

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v4

    invoke-virtual {v5, v0, v2, v3}, Landroid/graphics/Matrix;->setRotate(FFF)V

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move-object v0, p1

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "SpenStrokeFrame"

    const-string/jumbo v1, "rotateBitmap is null. out of memory"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :cond_0
    return-object v0
.end method

.method private createShapeMaskBitmap(Landroid/graphics/Path;)V
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    const/4 v1, 0x0

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    sget-object v1, Landroid/graphics/Region$Op;->XOR:Landroid/graphics/Region$Op;

    invoke-virtual {v0, p1, v1}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;Landroid/graphics/Region$Op;)Z

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mSpenSurfaceViewBitmap:Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v3, v3, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    return-void
.end method

.method private createSizeFitableBitmap(Landroid/graphics/Bitmap;Landroid/graphics/RectF;)Landroid/graphics/Bitmap;
    .locals 8

    const/4 v0, 0x0

    const/4 v7, 0x0

    const/4 v5, 0x0

    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v3

    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v1

    cmpl-float v2, v3, v5

    if-eqz v2, :cond_0

    cmpl-float v2, v1, v5

    if-nez v2, :cond_1

    :cond_0
    const-string/jumbo v2, "SpenStrokeFrame"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "width or height is 0. width = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " height = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v0

    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v4, 0x1

    if-ne v2, v4, :cond_5

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPreviewRatio:F

    div-float v4, v1, v3

    cmpl-float v2, v2, v4

    if-lez v2, :cond_4

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPreviewRatio:F

    div-float v4, v3, v1

    mul-float/2addr v2, v4

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, v1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v1

    int-to-float v1, v1

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    sub-float/2addr v1, v4

    cmpg-float v4, v1, v2

    if-gez v4, :cond_2

    move v2, v1

    :cond_2
    :goto_1
    cmpg-float v1, v3, v5

    if-lez v1, :cond_3

    cmpg-float v1, v2, v5

    if-gtz v1, :cond_7

    :cond_3
    const-string/jumbo v1, "SpenStrokeFrame"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "width or height is 0. width = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " height = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPreviewRatio:F

    div-float v4, v3, v1

    mul-float/2addr v2, v4

    div-float v2, v3, v2

    float-to-int v2, v2

    int-to-float v3, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getWidth()I

    move-result v2

    int-to-float v2, v2

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v4

    cmpg-float v4, v2, v3

    if-gez v4, :cond_9

    move v3, v2

    move v2, v1

    goto :goto_1

    :cond_5
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPreviewRatio:F

    div-float v4, v3, v1

    cmpl-float v2, v2, v4

    if-lez v2, :cond_6

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPreviewRatio:F

    div-float v4, v1, v3

    mul-float/2addr v2, v4

    mul-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v3, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getWidth()I

    move-result v2

    int-to-float v2, v2

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v4

    cmpg-float v4, v2, v3

    if-gez v4, :cond_9

    move v3, v2

    move v2, v1

    goto :goto_1

    :cond_6
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPreviewRatio:F

    div-float v4, v1, v3

    mul-float/2addr v2, v4

    div-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v2, v1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v1

    int-to-float v1, v1

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    sub-float/2addr v1, v4

    cmpg-float v4, v1, v2

    if-gez v4, :cond_2

    move v2, v1

    goto/16 :goto_1

    :cond_7
    float-to-int v1, v3

    float-to-int v4, v2

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-nez v1, :cond_8

    const-string/jumbo v1, "SpenStrokeFrame"

    const-string/jumbo v2, "resultBitmap is null. out of memory"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_8
    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v5, Landroid/graphics/Rect;

    float-to-int v3, v3

    float-to-int v2, v2

    invoke-direct {v5, v7, v7, v3, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v2, Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    invoke-direct {v2, v7, v7, v3, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v4, p1, v2, v5, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    move-object v0, v1

    goto/16 :goto_0

    :cond_9
    move v2, v1

    goto/16 :goto_1
.end method

.method private createStrokeFrameBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Path;)Landroid/graphics/Bitmap;
    .locals 6

    const/4 v5, 0x0

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    const-string/jumbo v1, "SpenStrokeFrame"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "createStrokeFrameBitmap src or rect  is nullsrc "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " relativePath "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v0

    :cond_1
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {p2, v2, v1}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v3

    float-to-int v3, v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-nez v1, :cond_2

    const-string/jumbo v1, "SpenStrokeFrame"

    const-string/jumbo v2, "resultBitmap createBitmap is failed. out fof memory"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    iget v3, v2, Landroid/graphics/RectF;->left:F

    neg-float v3, v3

    iget v2, v2, Landroid/graphics/RectF;->top:F

    neg-float v2, v2

    invoke-virtual {p2, v3, v2}, Landroid/graphics/Path;->offset(FF)V

    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    sget-object v3, Landroid/graphics/Region$Op;->INTERSECT:Landroid/graphics/Region$Op;

    invoke-virtual {v2, p2, v3}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;Landroid/graphics/Region$Op;)Z

    invoke-virtual {v2, p1, v5, v5, v0}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    move-object v0, v1

    goto :goto_0
.end method

.method private drawPen(Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;Landroid/graphics/RectF;Landroid/graphics/RectF;)Z
    .locals 21

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mWorkBitmap:Landroid/graphics/Bitmap;

    if-nez v1, :cond_0

    const-string/jumbo v1, "SpenStrokeFrame"

    const-string/jumbo v2, "workBitmap isn\'t created"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    new-instance v1, Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mWorkBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v1, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    const/4 v2, 0x0

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBgBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setReferenceBitmap(Landroid/graphics/Bitmap;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mWorkBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObjectList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :cond_1
    :goto_1
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_2

    new-instance v1, Landroid/graphics/RectF;

    move-object/from16 v0, p3

    invoke-direct {v1, v0}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPenSize:F

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->increaseRect(Landroid/graphics/RectF;F)V

    new-instance v2, Landroid/graphics/RectF;

    move-object/from16 v0, p4

    invoke-direct {v2, v0}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPenSize:F

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRatio:F

    mul-float/2addr v3, v4

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->increaseRect(Landroid/graphics/RectF;F)V

    new-instance v3, Landroid/graphics/Canvas;

    move-object/from16 v0, p1

    invoke-direct {v3, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mWorkBitmap:Landroid/graphics/Bitmap;

    new-instance v5, Landroid/graphics/Rect;

    iget v6, v1, Landroid/graphics/RectF;->left:F

    float-to-int v6, v6

    iget v7, v1, Landroid/graphics/RectF;->top:F

    float-to-int v7, v7

    iget v8, v1, Landroid/graphics/RectF;->right:F

    float-to-int v8, v8

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    float-to-int v1, v1

    invoke-direct {v5, v6, v7, v8, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v1, Landroid/graphics/Rect;

    iget v6, v2, Landroid/graphics/RectF;->left:F

    float-to-int v6, v6

    iget v7, v2, Landroid/graphics/RectF;->top:F

    float-to-int v7, v7

    iget v8, v2, Landroid/graphics/RectF;->right:F

    float-to-int v8, v8

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    float-to-int v2, v2

    invoke-direct {v1, v6, v7, v8, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    const/4 v2, 0x0

    invoke-virtual {v3, v4, v5, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setBitmap(Landroid/graphics/Bitmap;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setReferenceBitmap(Landroid/graphics/Bitmap;)V

    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_2
    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getType()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPoints()[Landroid/graphics/PointF;

    move-result-object v16

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPressures()[F

    move-result-object v17

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v18, v0

    const/4 v2, 0x2

    move/from16 v0, v18

    if-lt v0, v2, :cond_1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getTimeStamps()[I

    move-result-object v19

    new-instance v20, Landroid/graphics/RectF;

    invoke-direct/range {v20 .. v20}, Landroid/graphics/RectF;-><init>()V

    const/4 v1, 0x0

    aget v1, v19, v1

    int-to-long v1, v1

    const/4 v3, 0x0

    aget v3, v19, v3

    int-to-long v3, v3

    const/4 v5, 0x1

    const/4 v6, 0x0

    aget-object v6, v16, v6

    iget v6, v6, Landroid/graphics/PointF;->x:F

    const/4 v7, 0x0

    aget-object v7, v16, v7

    iget v7, v7, Landroid/graphics/PointF;->y:F

    const/4 v8, 0x0

    aget v8, v17, v8

    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static/range {v1 .. v14}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    move-result-object v1

    const/4 v2, 0x1

    move v9, v2

    :goto_2
    move/from16 v0, v18

    if-lt v9, v0, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    move-object/from16 v0, v20

    invoke-virtual {v2, v1, v0}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->redrawPen(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    goto/16 :goto_1

    :cond_3
    aget v2, v19, v9

    int-to-long v2, v2

    aget-object v4, v16, v9

    iget v4, v4, Landroid/graphics/PointF;->x:F

    aget-object v5, v16, v9

    iget v5, v5, Landroid/graphics/PointF;->y:F

    aget v6, v17, v9

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    invoke-virtual/range {v1 .. v8}, Landroid/view/MotionEvent;->addBatch(JFFFFI)V

    add-int/lit8 v2, v9, 0x1

    move v9, v2

    goto :goto_2
.end method

.method private getIntValueAppliedDensity(F)I
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->dm:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method private increaseRect(Landroid/graphics/RectF;F)V
    .locals 1

    iget v0, p1, Landroid/graphics/RectF;->left:F

    sub-float/2addr v0, p2

    iput v0, p1, Landroid/graphics/RectF;->left:F

    iget v0, p1, Landroid/graphics/RectF;->top:F

    sub-float/2addr v0, p2

    iput v0, p1, Landroid/graphics/RectF;->top:F

    iget v0, p1, Landroid/graphics/RectF;->right:F

    add-float/2addr v0, p2

    iput v0, p1, Landroid/graphics/RectF;->right:F

    iget v0, p1, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v0, p2

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    return-void
.end method

.method private loadChangeFrameButtonResources()Z
    .locals 9

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "SpenStrokeFrame"

    const-string/jumbo v2, "PackageManager is null"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_0
    :try_start_0
    const-string/jumbo v2, "com.samsung.android.sdk.spen30"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->dm:Landroid/util/DisplayMetrics;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->dm:Landroid/util/DisplayMetrics;

    if-nez v2, :cond_1

    const-string/jumbo v1, "SpenStrokeFrame"

    const-string/jumbo v2, "DisplayMetrics Get is failed"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_0
    move-exception v1

    const-string/jumbo v2, "SpenStrokeFrame"

    const-string/jumbo v3, "SpenSDK Resource not found"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    :cond_1
    const-string/jumbo v2, "string_transform_into_auto_shape"

    const-string/jumbo v3, "string"

    const-string/jumbo v4, "com.samsung.android.sdk.spen30"

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    const-string/jumbo v3, "string_transform_back_to_original_shape"

    const-string/jumbo v4, "string"

    const-string/jumbo v5, "com.samsung.android.sdk.spen30"

    invoke-virtual {v1, v3, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    const-string/jumbo v4, "selector_change_stroke_frame_bg"

    const-string/jumbo v5, "drawable"

    const-string/jumbo v6, "com.samsung.android.sdk.spen30"

    invoke-virtual {v1, v4, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    const-string/jumbo v5, "snote_photoframe_refine"

    const-string/jumbo v6, "drawable"

    const-string/jumbo v7, "com.samsung.android.sdk.spen30"

    invoke-virtual {v1, v5, v6, v7}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    const-string/jumbo v6, "snote_photoframe_undo"

    const-string/jumbo v7, "drawable"

    const-string/jumbo v8, "com.samsung.android.sdk.spen30"

    invoke-virtual {v1, v6, v7, v8}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    if-eqz v3, :cond_2

    if-eqz v2, :cond_2

    if-eqz v4, :cond_2

    if-eqz v5, :cond_2

    if-nez v6, :cond_3

    :cond_2
    const-string/jumbo v1, "SpenStrokeFrame"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "ChangeFrame Resource not found. Original = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v7, " Beautify = "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " BgDrawable = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " BeautifyDrawable = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " OriginalDrawable = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_3
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameDescriptionOriginal:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameDescriptionBeautify:Ljava/lang/String;

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameBgDrawable:Landroid/graphics/drawable/Drawable;

    invoke-static {v1, v6}, Lcom/samsung/android/sdk/pen/util/SpenScreenCodecDecoder;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameOriginalDrawable:Landroid/graphics/drawable/Drawable;

    invoke-static {v1, v5}, Lcom/samsung/android/sdk/pen/util/SpenScreenCodecDecoder;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameBeautifyDrawable:Landroid/graphics/drawable/Drawable;

    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method private makePath(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;
    .locals 18

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObjectList()Ljava/util/ArrayList;

    move-result-object v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_1

    new-instance v7, Landroid/graphics/Path;

    invoke-direct {v7}, Landroid/graphics/Path;-><init>()V

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v10

    new-array v11, v10, [I

    const/4 v0, 0x0

    const/4 v1, 0x1

    aput v1, v11, v0

    const/4 v0, 0x1

    :goto_1
    if-lt v0, v10, :cond_3

    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPoints()[Landroid/graphics/PointF;

    move-result-object v3

    const/4 v0, 0x0

    aget-object v0, v3, v0

    iget v0, v0, Landroid/graphics/PointF;->x:F

    const/4 v4, 0x0

    aget-object v4, v3, v4

    iget v4, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v7, v0, v4}, Landroid/graphics/Path;->moveTo(FF)V

    const/4 v0, 0x1

    :goto_2
    array-length v4, v3

    if-lt v0, v4, :cond_4

    const/4 v0, 0x1

    move v8, v0

    move v5, v1

    move v6, v2

    :goto_3
    if-lt v8, v10, :cond_5

    :cond_0
    move-object v0, v7

    :goto_4
    return-object v0

    :cond_1
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getType()I

    move-result v0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_2

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    aput v1, v11, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    aget-object v1, v3, v0

    iget v2, v1, Landroid/graphics/PointF;->x:F

    aget-object v1, v3, v0

    iget v1, v1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v7, v2, v1}, Landroid/graphics/Path;->lineTo(FF)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/high16 v1, 0x447a0000    # 1000.0f

    const/4 v4, 0x1

    :goto_5
    if-lt v4, v10, :cond_7

    if-eqz v3, :cond_0

    const/4 v0, 0x1

    aput v0, v11, v3

    invoke-virtual {v9, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPoints()[Landroid/graphics/PointF;

    move-result-object v3

    if-eqz v2, :cond_11

    const/4 v0, 0x0

    move v1, v5

    move v2, v6

    :goto_6
    array-length v4, v3

    if-lt v0, v4, :cond_10

    :cond_6
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    move v5, v1

    move v6, v2

    goto :goto_3

    :cond_7
    aget v0, v11, v4

    const/4 v12, 0x1

    if-ne v0, v12, :cond_8

    move v0, v1

    move v1, v2

    move v2, v3

    :goto_7
    add-int/lit8 v4, v4, 0x1

    move v3, v2

    move v2, v1

    move v1, v0

    goto :goto_5

    :cond_8
    if-nez v3, :cond_c

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPoints()[Landroid/graphics/PointF;

    move-result-object v0

    if-eqz v0, :cond_9

    array-length v1, v0

    if-nez v1, :cond_a

    :cond_9
    const-string/jumbo v0, "SpenStrokeFrame"

    const-string/jumbo v1, "nextPoint is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_4

    :cond_a
    const/4 v1, 0x0

    aget-object v1, v0, v1

    iget v1, v1, Landroid/graphics/PointF;->x:F

    const/4 v2, 0x0

    aget-object v2, v0, v2

    iget v2, v2, Landroid/graphics/PointF;->y:F

    array-length v3, v0

    add-int/lit8 v3, v3, -0x1

    aget-object v3, v0, v3

    iget v3, v3, Landroid/graphics/PointF;->x:F

    array-length v12, v0

    add-int/lit8 v12, v12, -0x1

    aget-object v0, v0, v12

    iget v0, v0, Landroid/graphics/PointF;->y:F

    sub-float v12, v6, v1

    sub-float v13, v6, v1

    mul-float/2addr v12, v13

    sub-float v13, v5, v2

    sub-float v14, v5, v2

    mul-float/2addr v13, v14

    add-float/2addr v12, v13

    sub-float v13, v6, v3

    sub-float v14, v6, v3

    mul-float/2addr v13, v14

    sub-float v14, v5, v0

    sub-float v15, v5, v0

    mul-float/2addr v14, v15

    add-float/2addr v13, v14

    cmpl-float v12, v12, v13

    if-lez v12, :cond_b

    sub-float v1, v6, v3

    sub-float v2, v6, v3

    mul-float/2addr v1, v2

    sub-float v2, v5, v0

    sub-float v0, v5, v0

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    const/4 v1, 0x0

    move v2, v4

    goto :goto_7

    :cond_b
    sub-float v0, v6, v1

    sub-float v1, v6, v1

    mul-float/2addr v0, v1

    sub-float v1, v5, v2

    sub-float v2, v5, v2

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    const/4 v1, 0x1

    move v2, v4

    goto :goto_7

    :cond_c
    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPoints()[Landroid/graphics/PointF;

    move-result-object v0

    if-eqz v0, :cond_d

    array-length v12, v0

    if-nez v12, :cond_e

    :cond_d
    const-string/jumbo v0, "SpenStrokeFrame"

    const-string/jumbo v1, "nextPoint is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto/16 :goto_4

    :cond_e
    const/4 v12, 0x0

    aget-object v12, v0, v12

    iget v12, v12, Landroid/graphics/PointF;->x:F

    const/4 v13, 0x0

    aget-object v13, v0, v13

    iget v13, v13, Landroid/graphics/PointF;->y:F

    array-length v14, v0

    add-int/lit8 v14, v14, -0x1

    aget-object v14, v0, v14

    iget v14, v14, Landroid/graphics/PointF;->x:F

    array-length v15, v0

    add-int/lit8 v15, v15, -0x1

    aget-object v0, v0, v15

    iget v0, v0, Landroid/graphics/PointF;->y:F

    sub-float v15, v6, v12

    sub-float v16, v6, v12

    mul-float v15, v15, v16

    sub-float v16, v5, v13

    sub-float v17, v5, v13

    mul-float v16, v16, v17

    add-float v15, v15, v16

    cmpl-float v15, v1, v15

    if-lez v15, :cond_f

    sub-float v0, v6, v12

    sub-float v1, v6, v12

    mul-float/2addr v0, v1

    sub-float v1, v5, v13

    sub-float v2, v5, v13

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    const/4 v1, 0x1

    move v2, v4

    goto/16 :goto_7

    :cond_f
    sub-float v12, v6, v14

    sub-float v13, v6, v14

    mul-float/2addr v12, v13

    sub-float v13, v5, v0

    sub-float v15, v5, v0

    mul-float/2addr v13, v15

    add-float/2addr v12, v13

    cmpl-float v12, v1, v12

    if-lez v12, :cond_12

    sub-float v1, v6, v14

    sub-float v2, v6, v14

    mul-float/2addr v1, v2

    sub-float v2, v5, v0

    sub-float v0, v5, v0

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    const/4 v1, 0x0

    move v2, v4

    goto/16 :goto_7

    :cond_10
    aget-object v1, v3, v0

    iget v2, v1, Landroid/graphics/PointF;->x:F

    aget-object v1, v3, v0

    iget v1, v1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v7, v2, v1}, Landroid/graphics/Path;->lineTo(FF)V

    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_6

    :cond_11
    array-length v0, v3

    add-int/lit8 v0, v0, -0x1

    move v1, v5

    move v2, v6

    :goto_8
    if-ltz v0, :cond_6

    aget-object v1, v3, v0

    iget v2, v1, Landroid/graphics/PointF;->x:F

    aget-object v1, v3, v0

    iget v1, v1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v7, v2, v1}, Landroid/graphics/Path;->lineTo(FF)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_8

    :cond_12
    move v0, v1

    move v1, v2

    move v2, v3

    goto/16 :goto_7
.end method

.method private setChangeFrameButtonResources()V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameButton:Landroid/widget/ImageButton;

    if-nez v0, :cond_0

    const-string/jumbo v0, "SpenStrokeFrame"

    const-string/jumbo v1, "ChangeFrameButton isn\'t created yet"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameBgDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStrokeFrameType:I

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameBeautifyDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameDescriptionBeautify:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameButton:Landroid/widget/ImageButton;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameBgDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameOriginalDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameButton:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameDescriptionOriginal:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method

.method private setRect(Landroid/graphics/RectF;)V
    .locals 4

    const/4 v3, 0x0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    float-to-int v1, v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    float-to-int v2, v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    float-to-int v1, v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mTouchImage:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mTouchImage:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    return-void
.end method

.method private startCamera()Z
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->loadChangeFrameButtonResources()Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v1, "SpenStrokeFrame"

    const-string/jumbo v2, "ChangeFrame Resource Loading is failed"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_0
    new-instance v2, Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mActivity:Landroid/app/Activity;

    invoke-direct {v2, v3}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameButton:Landroid/widget/ImageButton;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->setChangeFrameButtonResources()V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->updateChangeFrameButtonPosition()V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameButton:Landroid/widget/ImageButton;

    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$1;

    invoke-direct {v3, p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$1;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v2, Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mActivity:Landroid/app/Activity;

    invoke-direct {v2, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskView:Landroid/widget/ImageView;

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    invoke-direct {v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskView:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskView:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskView:Landroid/widget/ImageView;

    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$2;

    invoke-direct {v3, p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$2;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    new-instance v2, Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mActivity:Landroid/app/Activity;

    invoke-direct {v2, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mTouchImage:Landroid/widget/ImageView;

    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mActivity:Landroid/app/Activity;

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;-><init>(Landroid/app/Activity;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mTouchImage:Landroid/widget/ImageView;

    if-nez v2, :cond_2

    :cond_1
    const-string/jumbo v1, "SpenStrokeFrame"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "CameraView or TouchImage is null. CameraView = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "TouchImage = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mTouchImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->requestFocusFromTouch()Z

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    float-to-int v3, v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    float-to-int v4, v4

    invoke-virtual {v2, v3, v4, v0, v0}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->setZOrderMediaOverlay(Z)V

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$3;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPreviewCallback:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnPreviewCallback;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPreviewCallback:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnPreviewCallback;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->setOnPreviewCallback(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnPreviewCallback;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mTouchImage:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mTouchImage:Landroid/widget/ImageView;

    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;

    invoke-direct {v2, p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->setOnCompleteCameraFrameListener(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnCompleteCameraFrameListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskView:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mTouchImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    move v0, v1

    goto/16 :goto_0
.end method

.method private startRecognition()Z
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    new-instance v3, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognitionManager;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mActivity:Landroid/app/Activity;

    invoke-direct {v3, v0}, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognitionManager;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x4

    invoke-virtual {v3, v1, v0}, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognitionManager;->getInfoList(II)Ljava/util/List;

    move-result-object v0

    const/4 v4, 0x0

    :try_start_0
    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;

    invoke-virtual {v3, v0}, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognitionManager;->createRecognition(Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;)Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeRecognition:Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeRecognition:Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;

    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;

    invoke-direct {v3, p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;->setResultListener(Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase$ResultListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeRecognition:Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalStroke:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/recognition/SpenShapeRecognition;->request(Ljava/util/List;)V
    :try_end_0
    .catch Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move v0, v1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;->printStackTrace()V

    move v0, v2

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    move v0, v2

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    move v0, v2

    goto :goto_0
.end method

.method private updateChangeFrameButtonPosition()V
    .locals 9

    const/4 v8, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameButton:Landroid/widget/ImageButton;

    if-nez v0, :cond_0

    const-string/jumbo v0, "SpenStrokeFrame"

    const-string/jumbo v1, "ChangeFrameButton isn\'t created yet"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->dm:Landroid/util/DisplayMetrics;

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->dm:Landroid/util/DisplayMetrics;

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    add-int/2addr v0, v1

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->CHANGE_STROKE_FRAME_IMAGE_UX_TABLE:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    sget v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_PIXEL_DEFAULT:I

    move v3, v0

    :goto_1
    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->CHANGE_STROKE_FRAME_IMAGE_UX_TABLE:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    sget v1, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN_WHEN_TOP:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->CHANGE_STROKE_FRAME_IMAGE_UX_TABLE:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    sget v2, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN_WHEN_TOP:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Rect;

    sget-object v2, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->CHANGE_STROKE_FRAME_IMAGE_UX_TABLE:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/HashMap;

    sget v6, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_SIZE:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Rect;

    int-to-float v4, v4

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v4, v6

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v6

    iget v7, v1, Landroid/graphics/Rect;->top:I

    add-int/2addr v6, v7

    int-to-float v6, v6

    invoke-direct {p0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v6

    int-to-float v6, v6

    cmpl-float v4, v4, v6

    if-ltz v4, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->right:F

    float-to-int v0, v0

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v3

    iget v4, v1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v3

    sub-int/2addr v0, v3

    iput v0, v5, Landroid/graphics/Rect;->left:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    float-to-int v0, v0

    iget v1, v1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, v5, Landroid/graphics/Rect;->top:I

    iget v0, v5, Landroid/graphics/Rect;->left:I

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, v5, Landroid/graphics/Rect;->right:I

    iget v0, v5, Landroid/graphics/Rect;->top:I

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, v5, Landroid/graphics/Rect;->bottom:I

    :goto_2
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget v1, v5, Landroid/graphics/Rect;->left:I

    iget v2, v5, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0, v1, v2, v8, v8}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mChangeFrameButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v4

    iget v6, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v4, v6

    int-to-float v4, v4

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v4

    int-to-float v4, v4

    cmpl-float v1, v1, v4

    if-ltz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    float-to-int v1, v1

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v3

    iget v4, v0, Landroid/graphics/Rect;->right:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v3

    sub-int/2addr v1, v3

    iput v1, v5, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    float-to-int v1, v1

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v3

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v3

    int-to-float v0, v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v0

    sub-int v0, v1, v0

    iput v0, v5, Landroid/graphics/Rect;->top:I

    iget v0, v5, Landroid/graphics/Rect;->left:I

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, v5, Landroid/graphics/Rect;->right:I

    iget v0, v5, Landroid/graphics/Rect;->top:I

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, v5, Landroid/graphics/Rect;->bottom:I

    goto :goto_2

    :cond_2
    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->CHANGE_STROKE_FRAME_IMAGE_UX_TABLE:Ljava/util/HashMap;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    sget v1, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->HASH_KEY_IMAGE_MARGIN:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    float-to-int v1, v1

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v3

    iget v4, v0, Landroid/graphics/Rect;->right:I

    add-int/2addr v3, v4

    int-to-float v3, v3

    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v3

    sub-int/2addr v1, v3

    iput v1, v5, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getHeight()I

    move-result v1

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v3

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v3

    int-to-float v0, v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v0

    sub-int v0, v1, v0

    iput v0, v5, Landroid/graphics/Rect;->top:I

    iget v0, v5, Landroid/graphics/Rect;->left:I

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, v5, Landroid/graphics/Rect;->right:I

    iget v0, v5, Landroid/graphics/Rect;->top:I

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->getIntValueAppliedDensity(F)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, v5, Landroid/graphics/Rect;->bottom:I

    goto/16 :goto_2

    :cond_3
    move v3, v0

    goto/16 :goto_1
.end method


# virtual methods
.method public cancelStrokeFrame()Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cleanResource()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mObjectContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    return-object v0
.end method

.method public retakeStrokeFrame(Landroid/app/Activity;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;IIILcom/samsung/android/sdk/pen/document/SpenObjectContainer;Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;Landroid/graphics/PointF;FLandroid/graphics/PointF;Landroid/view/ViewGroup;)V
    .locals 6

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mActivity:Landroid/app/Activity;

    iput p6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStrokeFrameType:I

    iput-object p7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mObjectContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    iput-object p9, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPan:Landroid/graphics/PointF;

    move/from16 v0, p10

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRatio:F

    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStartFramePosition:Landroid/graphics/PointF;

    iput-object p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mSpenSurfaceViewBitmap:Landroid/graphics/Bitmap;

    iput p4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPageWidth:I

    iput p5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPageHeight:I

    iput-object p8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->updateListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    const/4 v1, 0x2

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStrokeFrameAction:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPageWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPageHeight:I

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mWorkBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mWorkBitmap:Landroid/graphics/Bitmap;

    if-nez v1, :cond_1

    const-string/jumbo v1, "SpenStrokeFrame"

    const-string/jumbo v2, "workBitmap create is failed"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0x20

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mSpenSurfaceViewBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mSpenSurfaceViewBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskBitmap:Landroid/graphics/Bitmap;

    if-nez v1, :cond_2

    const-string/jumbo v1, "SpenStrokeFrame"

    const-string/jumbo v2, "ShapeMaskBitmap create is failed"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0x20

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V

    goto :goto_0

    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalStroke:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mObjectContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObjectList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_5

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyStoke:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mObjectContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObjectList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_7

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->createPen()Lcom/samsung/android/sdk/pen/pen/SpenPen;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    if-nez v1, :cond_9

    const-string/jumbo v1, "SpenStrokeFrame"

    const-string/jumbo v2, "Pen Create is failed"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0x20

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V

    goto/16 :goto_0

    :cond_5
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getType()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_6

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalImage:Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    goto :goto_1

    :cond_6
    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getType()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalStroke:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_7
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getType()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_8

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyImage:Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    goto :goto_2

    :cond_8
    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getType()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_4

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyStoke:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_9
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStrokeFrameType:I

    if-nez v1, :cond_a

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->makePath(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mFrameShapePath:Landroid/graphics/Path;

    :goto_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mFrameShapePath:Landroid/graphics/Path;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->convertRelative(Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v2

    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    sget-object v5, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v3, v4, v2, v5}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mFrameShapePath:Landroid/graphics/Path;

    invoke-virtual {v4, v3, v1}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;Landroid/graphics/Path;)V

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->createShapeMaskBitmap(Landroid/graphics/Path;)V

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStrokeFrameType:I

    if-nez v1, :cond_b

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskBitmap:Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    invoke-direct {p0, v1, v3, v4, v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->drawPen(Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    move-result v1

    :goto_4
    if-nez v1, :cond_c

    const-string/jumbo v1, "SpenStrokeFrame"

    const-string/jumbo v2, "drawPen is failed"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0x20

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V

    goto/16 :goto_0

    :cond_a
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->makePath(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mFrameShapePath:Landroid/graphics/Path;

    goto :goto_3

    :cond_b
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskBitmap:Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;

    invoke-direct {p0, v1, v3, v4, v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->drawPen(Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    move-result v1

    goto :goto_4

    :cond_c
    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->setRect(Landroid/graphics/RectF;)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->startCamera()Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "SpenStrokeFrame"

    const-string/jumbo v2, "startCamera is failed"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0x20

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V

    goto/16 :goto_0
.end method

.method public takeStrokeFrame(Landroid/app/Activity;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;IIILcom/samsung/android/sdk/pen/document/SpenObjectContainer;Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;Landroid/graphics/PointF;FLandroid/graphics/PointF;Landroid/view/ViewGroup;)V
    .locals 5

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mActivity:Landroid/app/Activity;

    iput p6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStrokeFrameType:I

    iput-object p7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mObjectContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mViewGroup:Landroid/view/ViewGroup;

    iput-object p9, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPan:Landroid/graphics/PointF;

    iput p10, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRatio:F

    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStartFramePosition:Landroid/graphics/PointF;

    iput-object p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mSpenSurfaceViewBitmap:Landroid/graphics/Bitmap;

    iput-object p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBgBitmap:Landroid/graphics/Bitmap;

    iput p4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPageWidth:I

    iput p5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPageHeight:I

    iput-object p8, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->updateListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    const/4 v1, 0x1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStrokeFrameAction:I

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalStroke:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mObjectContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObjectList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mObjectContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyImage:Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->createPen()Lcom/samsung/android/sdk/pen/pen/SpenPen;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    if-nez v1, :cond_4

    const-string/jumbo v1, "SpenStrokeFrame"

    const-string/jumbo v2, "Pen Create is failed"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0x20

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getType()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_3

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalImage:Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    goto :goto_0

    :cond_3
    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getType()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalStroke:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->startRecognition()Z

    move-result v1

    if-nez v1, :cond_1

    const-string/jumbo v1, "SpenStrokeFrame"

    const-string/jumbo v2, "Recognition is failed"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0x8

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V

    goto :goto_1
.end method
