.class Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;
.super Landroid/os/Handler;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final mTextBox:Ljava/lang/ref/WeakReference;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 1

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;->mTextBox:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;->mTextBox:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;->mTextBox:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$24(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$19(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)[Landroid/widget/ImageButton;

    move-result-object v1

    const/4 v2, 0x1

    aget-object v1, v1, v2

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$25(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-result-object v1

    if-eqz v1, :cond_2

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$25(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_2

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$25(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->hide()V

    :cond_2
    invoke-virtual {p0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
