.class Lcom/samsung/android/sdk/pen/engine/SpenView$ViewUpdateCanvasListener;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateCanvasListener;


# instance fields
.field mView:Lcom/samsung/android/sdk/pen/engine/SpenView;

.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenView;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenView;Lcom/samsung/android/sdk/pen/engine/SpenView;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenView$ViewUpdateCanvasListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenView$ViewUpdateCanvasListener;->mView:Lcom/samsung/android/sdk/pen/engine/SpenView;

    return-void
.end method


# virtual methods
.method public onUpdateCanvas(Landroid/graphics/RectF;Z)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView$ViewUpdateCanvasListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenView;

    invoke-static {v0, p2}, Lcom/samsung/android/sdk/pen/engine/SpenView;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenView;Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView$ViewUpdateCanvasListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenView;->mThreadId:J
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenView;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenView;)J

    move-result-wide v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView$ViewUpdateCanvasListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenView;->invalidate()V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView$ViewUpdateCanvasListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenView;->postInvalidate()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView$ViewUpdateCanvasListener;->mView:Lcom/samsung/android/sdk/pen/engine/SpenView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView$ViewUpdateCanvasListener;->mView:Lcom/samsung/android/sdk/pen/engine/SpenView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenView;->getReplayState()I

    move-result v0

    if-ne v0, v4, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView$ViewUpdateCanvasListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenView;

    invoke-static {v0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenView;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenView;Z)V

    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView$ViewUpdateCanvasListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenView;->mUpdating:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenView;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenView;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView$ViewUpdateCanvasListener;->mView:Lcom/samsung/android/sdk/pen/engine/SpenView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenView;->getReplayState()I

    move-result v0

    if-eq v0, v4, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenView$ViewUpdateCanvasListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenView;

    invoke-static {v0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenView;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenView;Z)V

    :cond_2
    const-wide/16 v0, 0x5

    const/4 v2, 0x0

    :try_start_0
    invoke-static {v0, v1, v2}, Ljava/lang/Thread;->sleep(JI)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method
