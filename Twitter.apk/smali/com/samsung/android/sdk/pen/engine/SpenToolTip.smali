.class Lcom/samsung/android/sdk/pen/engine/SpenToolTip;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mEraserBitmap:Landroid/graphics/Bitmap;

.field private mEraserPaint:Landroid/graphics/Paint;

.field private mPenBitmap:Landroid/graphics/Bitmap;

.field private mPenList:Ljava/util/List;

.field private mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

.field private mPenPaint:Landroid/graphics/Paint;

.field private mPoints:[Landroid/graphics/PointF;

.field private mPressures:[F

.field private mSdkResources:Landroid/content/res/Resources;

.field private mSpoidBitmap:Landroid/graphics/Bitmap;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 7

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/high16 v4, 0x43160000    # 150.0f

    const/4 v0, 0x0

    const/4 v3, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSdkResources:Landroid/content/res/Resources;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSpoidBitmap:Landroid/graphics/Bitmap;

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mContext:Landroid/content/Context;

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string/jumbo v1, "com.samsung.android.sdk.spen30"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSdkResources:Landroid/content/res/Resources;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v0, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    const/4 v0, 0x3

    new-array v0, v0, [Landroid/graphics/PointF;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    const/4 v0, 0x3

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPressures:[F

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40800000    # 4.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserPaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserPaint:Landroid/graphics/Paint;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    new-instance v1, Landroid/graphics/PointF;

    const/4 v2, 0x0

    invoke-direct {v1, v2, v4}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v1, v0, v5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    new-instance v1, Landroid/graphics/PointF;

    const/high16 v2, 0x42480000    # 50.0f

    invoke-direct {v1, v2, v4}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v1, v0, v3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    new-instance v1, Landroid/graphics/PointF;

    const/high16 v2, 0x42c80000    # 100.0f

    invoke-direct {v1, v2, v4}, Landroid/graphics/PointF;-><init>(FF)V

    aput-object v1, v0, v6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPressures:[F

    const/high16 v1, 0x3f800000    # 1.0f

    aput v1, v0, v5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPressures:[F

    const v1, 0x3f4ccccd    # 0.8f

    aput v1, v0, v3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPressures:[F

    const v1, 0x3f19999a    # 0.6f

    aput v1, v0, v6

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenList:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenBitmap:Landroid/graphics/Bitmap;

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSpoidBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSpoidBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSpoidBitmap:Landroid/graphics/Bitmap;

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;->close()V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    :cond_4
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSdkResources:Landroid/content/res/Resources;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSdkResources:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->flushLayoutCache()V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSdkResources:Landroid/content/res/Resources;

    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    const/4 v1, 0x0

    aput-object v2, v0, v1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    const/4 v1, 0x1

    aput-object v2, v0, v1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    const/4 v1, 0x2

    aput-object v2, v0, v1

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    :cond_6
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPressures:[F

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenPaint:Landroid/graphics/Paint;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserPaint:Landroid/graphics/Paint;

    return-void
.end method

.method public getDrawableEraserImage(F)Landroid/graphics/drawable/Drawable;
    .locals 4

    const/16 v1, 0xcc

    const/high16 v3, 0x42cc0000    # 102.0f

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v1, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    :cond_0
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    const/4 v1, 0x0

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3, v3, p1, v1}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSdkResources:Landroid/content/res/Resources;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    return-object v0
.end method

.method public getDrawableHoverImage()Landroid/graphics/drawable/Drawable;
    .locals 8

    const/4 v7, 0x0

    const/16 v6, 0x64

    const/4 v1, 0x0

    const-string/jumbo v0, "snote_toolbar_icon_spoid_hover"

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSpoidBitmap:Landroid/graphics/Bitmap;

    if-nez v2, :cond_1

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v6, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSpoidBitmap:Landroid/graphics/Bitmap;

    :cond_1
    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_2

    move-object v0, v1

    goto :goto_0

    :cond_2
    new-instance v2, Landroid/graphics/Canvas;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSpoidBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v2, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v2, v7, v3}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    const/16 v4, 0x2b

    const/16 v5, 0x39

    invoke-virtual {v3, v4, v7, v6, v5}, Landroid/graphics/Rect;->set(IIII)V

    invoke-virtual {v2, v0, v1, v3, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSdkResources:Landroid/content/res/Resources;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSpoidBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public getDrawableImage(Ljava/lang/String;IF)Landroid/graphics/drawable/Drawable;
    .locals 21

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenList:Ljava/util/List;

    if-nez v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;->getPenInfoList()Ljava/util/List;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenList:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenList:Ljava/util/List;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    move v3, v2

    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lt v3, v2, :cond_1

    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenList:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;

    iget-object v4, v2, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-nez v4, :cond_7

    iget-object v3, v2, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->iconImageUri:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    move-object/from16 v16, v3

    check-cast v16, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenBitmap:Landroid/graphics/Bitmap;

    if-nez v3, :cond_2

    const/16 v3, 0x64

    const/16 v4, 0x12c

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenBitmap:Landroid/graphics/Bitmap;

    :cond_2
    new-instance v17, Landroid/graphics/Rect;

    invoke-direct/range {v17 .. v17}, Landroid/graphics/Rect;-><init>()V

    new-instance v18, Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, v18

    invoke-direct {v0, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    const/4 v3, 0x0

    sget-object v4, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    invoke-virtual {v3, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;->createPen(Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;)Lcom/samsung/android/sdk/pen/pen/SpenPen;

    move-result-object v19

    iget-object v3, v2, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    const-string/jumbo v4, "com.samsung.android.sdk.pen.pen.preload.MagicPen"

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_5

    const-string/jumbo v2, "snote_popup_pensetting_preview_alpha"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/BitmapDrawable;

    const/4 v3, 0x0

    const/16 v4, 0x96

    const/16 v5, 0x64

    const/high16 v6, 0x43160000    # 150.0f

    add-float v6, v6, p3

    float-to-int v6, v6

    move-object/from16 v0, v17

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/Rect;->set(IIII)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenPaint:Landroid/graphics/Paint;

    shr-int/lit8 v4, p2, 0x18

    and-int/lit16 v4, v4, 0xff

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenPaint:Landroid/graphics/Paint;

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v2, v3, v1, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenPaint:Landroid/graphics/Paint;

    const/16 v3, 0xff

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setBitmap(Landroid/graphics/Bitmap;)V

    :goto_2
    const/4 v2, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setBitmap(Landroid/graphics/Bitmap;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;->destroyPen(Lcom/samsung/android/sdk/pen/pen/SpenPen;)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    :goto_3
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/16 v4, 0x64

    const/16 v5, 0xfa

    move-object/from16 v0, v17

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    if-eqz v16, :cond_4

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenPaint:Landroid/graphics/Paint;

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v2, v3, v1, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_4
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSdkResources:Landroid/content/res/Resources;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v2, v3, v4}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    goto/16 :goto_0

    :cond_5
    :try_start_1
    move-object/from16 v0, v19

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setColor(I)V

    iget-object v3, v2, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    const-string/jumbo v4, "com.samsung.android.sdk.pen.pen.preload.InkPen"

    invoke-virtual {v3, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_6

    move/from16 v0, p3

    float-to-double v2, v0

    const-wide/high16 v4, 0x3ff8000000000000L    # 1.5

    mul-double/2addr v2, v4

    double-to-float v10, v2

    :goto_4
    const/high16 v2, 0x40400000    # 3.0f

    mul-float/2addr v2, v10

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setSize(F)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPenBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setBitmap(Landroid/graphics/Bitmap;)V

    new-instance v20, Landroid/graphics/RectF;

    invoke-direct/range {v20 .. v20}, Landroid/graphics/RectF;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    iget v7, v4, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    iget v8, v4, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPressures:[F

    const/4 v5, 0x0

    aget v9, v4, v5

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-wide v4, v2

    invoke-static/range {v2 .. v15}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    move-result-object v4

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v4, v1}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    invoke-virtual {v4}, Landroid/view/MotionEvent;->recycle()V

    const-wide/16 v4, 0x5

    add-long/2addr v4, v2

    const/4 v6, 0x2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    const/4 v8, 0x1

    aget-object v7, v7, v8

    iget v7, v7, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    const/4 v9, 0x1

    aget-object v8, v8, v9

    iget v8, v8, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPressures:[F

    const/4 v11, 0x1

    aget v9, v9, v11

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-static/range {v2 .. v15}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    move-result-object v4

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v4, v1}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    invoke-virtual {v4}, Landroid/view/MotionEvent;->recycle()V

    const-wide/16 v4, 0xa

    add-long/2addr v4, v2

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    const/4 v8, 0x2

    aget-object v7, v7, v8

    iget v7, v7, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPoints:[Landroid/graphics/PointF;

    const/4 v9, 0x2

    aget-object v8, v8, v9

    iget v8, v8, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mPressures:[F

    const/4 v11, 0x2

    aget v9, v9, v11

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-static/range {v2 .. v15}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    move-result-object v2

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v0, v2, v1}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    invoke-virtual {v2}, Landroid/view/MotionEvent;->recycle()V
    :try_end_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    goto/16 :goto_2

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto/16 :goto_3

    :cond_6
    :try_start_2
    iget-object v2, v2, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    const-string/jumbo v3, "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    move-result v2

    if-nez v2, :cond_8

    move/from16 v0, p3

    float-to-double v2, v0

    const-wide v4, 0x3ff3333333333333L    # 1.2

    mul-double/2addr v2, v4

    double-to-float v10, v2

    goto/16 :goto_4

    :catch_1
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto/16 :goto_3

    :catch_2
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto/16 :goto_3

    :catch_3
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_3

    :cond_7
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto/16 :goto_1

    :cond_8
    move/from16 v10, p3

    goto/16 :goto_4
.end method

.method public getDrawableRemoverImage(I)Landroid/graphics/drawable/Drawable;
    .locals 4

    const/16 v1, 0xcc

    const/high16 v3, 0x42cc0000    # 102.0f

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v1, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    :cond_0
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    const/4 v1, 0x0

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    int-to-float v1, p1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSdkResources:Landroid/content/res/Resources;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mEraserBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    return-object v0
.end method

.method setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSdkResources:Landroid/content/res/Resources;

    const-string/jumbo v1, "drawable"

    const-string/jumbo v2, "com.samsung.android.sdk.spen30"

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->mSdkResources:Landroid/content/res/Resources;

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method
