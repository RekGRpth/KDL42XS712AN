.class Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface$UpdateListener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCanceled(ILjava/lang/Object;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mUpdateListener:Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$UpdateListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;)Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$UpdateListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$UpdateListener;->onCanceled(ILjava/lang/Object;)V

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->access$1(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;Z)V

    return-void
.end method

.method public onCompleted(Ljava/lang/Object;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mUpdateListener:Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$UpdateListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;)Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$UpdateListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$UpdateListener;->onCompleted(Ljava/lang/Object;)V

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->access$1(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;Z)V

    return-void
.end method

.method public onObjectUpdated(Landroid/graphics/RectF;Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mUpdateListener:Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$UpdateListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;)Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$UpdateListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$UpdateListener;->onObjectUpdated(Landroid/graphics/RectF;Ljava/lang/Object;)V

    return-void
.end method
