.class Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;
.super Landroid/view/View;
.source "Twttr"


# static fields
.field private static final ZOOMPAD_BAR_BUTTON_WIDTH:I = 0x24

.field private static final ZOOMPAD_BAR_COLOR:I = -0xb8b4b9

.field private static final ZOOMPAD_BAR_SIZE:I = 0x22

.field private static final ZOOMPAD_BG_COLOR:I = -0x848485

.field private static final ZOOMPAD_MOVING_RECT_COLOR:I = 0x4779caf2

.field private static final ZOOMPAD_STATE_BTN_ENTER:I = 0x7

.field private static final ZOOMPAD_STATE_BTN_LEFT:I = 0x5

.field private static final ZOOMPAD_STATE_BTN_RIGHT:I = 0x6

.field private static final ZOOMPAD_STATE_NONE:I = 0x0

.field private static final ZOOMPAD_STATE_PAD_MOVE:I = 0x3

.field private static final ZOOMPAD_STATE_PAD_MOVING_RECT:I = 0x4

.field private static final ZOOMPAD_STATE_VIEW_MOVE:I = 0x1

.field private static final ZOOMPAD_STATE_VIEW_RESIZE:I = 0x2

.field private static final ZOOMPAD_VIEW_LINE_COLOR:I = -0xf4742a

.field private static final ZOOMVIEW_RESIZE:I = 0x11


# instance fields
.field private final TAG:Ljava/lang/String;

.field private mAntiAliasPaint:Landroid/graphics/Paint;

.field private mCanvasLayer:Ljava/util/ArrayList;

.field private mCloseButton:Landroid/widget/ImageButton;

.field private mContext:Landroid/content/Context;

.field private mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

.field private mDeltaX:F

.field private mDeltaY:F

.field private mEnterButton:Landroid/widget/ImageButton;

.field private mLeftButton:Landroid/widget/ImageButton;

.field private mOnePT:F

.field private mParentView:Landroid/view/ViewGroup;

.field private mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

.field private mPenName:Ljava/lang/String;

.field private mRatio:F

.field private mRelative:Landroid/widget/RelativeLayout;

.field private mRightButton:Landroid/widget/ImageButton;

.field private mScreenHeight:I

.field private mScreenHeightExceptSIP:I

.field private mScreenWidth:I

.field private mSdkResources:Landroid/content/res/Resources;

.field private mSrcPaint:Landroid/graphics/Paint;

.field private mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

.field private mZoomPadActionListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadActionListener;

.field private mZoomPadBarImageRect:Landroid/graphics/Rect;

.field private mZoomPadBarMovingRect:Landroid/graphics/Rect;

.field private mZoomPadBarRect:Landroid/graphics/Rect;

.field private mZoomPadBitmap:[Landroid/graphics/Bitmap;

.field private mZoomPadBitmapRect:Landroid/graphics/RectF;

.field private mZoomPadEnabled:Z

.field private mZoomPadHandleRect:Landroid/graphics/Rect;

.field private mZoomPadHandler:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadHandler;

.field private mZoomPadLinePaint:Landroid/graphics/Paint;

.field private mZoomPadMovingRect:Landroid/graphics/Rect;

.field private mZoomPadPaint:Landroid/graphics/Paint;

.field private mZoomPadPrePoint:Landroid/graphics/PointF;

.field private mZoomPadRect:Landroid/graphics/RectF;

.field private mZoomPadScrollLimit:I

.field private mZoomPadState:I

.field private mZoomViewPaint:Landroid/graphics/Paint;

.field private mZoomViewRect:Landroid/graphics/RectF;

.field private mZoomViewSaveRect:Landroid/graphics/RectF;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    const-string/jumbo v0, "ZoomPad"

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->TAG:Ljava/lang/String;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mSdkResources:Landroid/content/res/Resources;

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadEnabled:Z

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadState:I

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadPrePoint:Landroid/graphics/PointF;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadHandler:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadHandler;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mLeftButton:Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mRightButton:Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mEnterButton:Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mCloseButton:Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mParentView:Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mCanvasLayer:Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mSrcPaint:Landroid/graphics/Paint;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mAntiAliasPaint:Landroid/graphics/Paint;

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mScreenWidth:I

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mScreenHeight:I

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mScreenHeightExceptSIP:I

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mDeltaX:F

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mDeltaY:F

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mRatio:F

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    const/4 v0, 0x5

    new-array v0, v0, [Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmap:[Landroid/graphics/Bitmap;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40000000    # 2.0f

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewPaint:Landroid/graphics/Paint;

    const v1, -0xf4742a

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadPaint:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadLinePaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadLinePaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40800000    # 4.0f

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadLinePaint:Landroid/graphics/Paint;

    const v1, -0xb8b4b9

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewSaveRect:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadRect:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadMovingRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarMovingRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarImageRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadHandleRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadPrePoint:Landroid/graphics/PointF;

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadHandler;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadHandler;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadHandler:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadHandler;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mSrcPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mSrcPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mSrcPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mSrcPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mAntiAliasPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string/jumbo v1, "com.samsung.android.sdk.spen30"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mSdkResources:Landroid/content/res/Resources;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v0, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mContext:Landroid/content/Context;

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private ExtendRectFromRectF(Landroid/graphics/Rect;Landroid/graphics/RectF;)V
    .locals 2

    iget v0, p2, Landroid/graphics/RectF;->left:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->left:I

    iget v0, p2, Landroid/graphics/RectF;->top:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->top:I

    iget v0, p2, Landroid/graphics/RectF;->right:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->right:I

    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    return-void
.end method

.method private MakeNewExtendRect(Landroid/graphics/RectF;)Landroid/graphics/Rect;
    .locals 6

    new-instance v0, Landroid/graphics/Rect;

    iget v1, p1, Landroid/graphics/RectF;->left:F

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->floor(D)D

    move-result-wide v1

    double-to-int v1, v1

    iget v2, p1, Landroid/graphics/RectF;->top:F

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v2, v2

    iget v3, p1, Landroid/graphics/RectF;->right:F

    float-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v3

    double-to-int v3, v3

    iget v4, p1, Landroid/graphics/RectF;->bottom:F

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v0
.end method

.method private absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 2

    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mRatio:F

    div-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mDeltaX:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->left:F

    iget v0, p2, Landroid/graphics/RectF;->right:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mRatio:F

    div-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mDeltaX:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->right:F

    iget v0, p2, Landroid/graphics/RectF;->top:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mRatio:F

    div-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mDeltaY:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mRatio:F

    div-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mDeltaY:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->setZoomPadButton(I)V

    return-void
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;)V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->updateFrameBuffer()V

    return-void
.end method

.method private getResourceBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 6

    const/4 v5, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mSdkResources:Landroid/content/res/Resources;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mSdkResources:Landroid/content/res/Resources;

    const-string/jumbo v2, "drawable"

    const-string/jumbo v3, "com.samsung.android.sdk.spen30"

    invoke-virtual {v0, p1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-object v1

    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mSdkResources:Landroid/content/res/Resources;

    invoke-static {v2, v0}, Lcom/samsung/android/sdk/pen/util/SpenScreenCodecDecoder;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_2

    instance-of v1, v0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_1

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v2}, Landroid/graphics/Canvas;->getWidth()I

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/Canvas;->getHeight()I

    move-result v4

    invoke-virtual {v0, v5, v5, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    move-object v0, v1

    :goto_2
    move-object v1, v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_2

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method private getResourceDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mSdkResources:Landroid/content/res/Resources;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mSdkResources:Landroid/content/res/Resources;

    const-string/jumbo v2, "drawable"

    const-string/jumbo v3, "com.samsung.android.sdk.spen30"

    invoke-virtual {v1, p1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mSdkResources:Landroid/content/res/Resources;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenScreenCodecDecoder;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method private getResourceString(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mSdkResources:Landroid/content/res/Resources;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mSdkResources:Landroid/content/res/Resources;

    const-string/jumbo v2, "string"

    const-string/jumbo v3, "com.samsung.android.sdk.spen30"

    invoke-virtual {v1, p1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mSdkResources:Landroid/content/res/Resources;

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private onTouchInputPen(Landroid/view/MotionEvent;)Z
    .locals 9

    const/4 v0, 0x0

    const/4 v8, 0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    and-int/lit16 v1, v1, 0xff

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    if-eqz v2, :cond_0

    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    neg-float v3, v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    neg-float v4, v4

    invoke-virtual {p1, v3, v4}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadMovingRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    cmpg-float v0, v0, v2

    if-gez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadHandler:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadHandler;

    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadHandler;->setMovingEnabled(Z)V

    :cond_1
    if-ne v1, v8, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadHandler:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadHandler;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadHandler;->isMovingEnabled()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadHandler:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadHandler;

    const-wide/16 v1, 0x258

    invoke-virtual {v0, v8, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadHandler;->sendEmptyMessageDelayed(IJ)Z

    :cond_2
    return v8

    :pswitch_0
    new-instance v3, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    invoke-direct {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;-><init>()V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mPenName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->setPenName(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->getColor()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->setColor(I)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->getSize()F

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v6

    div-float/2addr v5, v6

    div-float/2addr v4, v5

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->setPenSize(F)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->isCurveEnabled()Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->setCurveEnabled(Z)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->getAdvancedSetting()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->setAdvancedPenSetting(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    invoke-virtual {v3, p1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v2

    if-lt v0, v2, :cond_3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    div-float/2addr v2, v3

    mul-float/2addr v0, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    add-float/2addr v0, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    div-float/2addr v3, v4

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    new-instance v4, Landroid/graphics/PointF;

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mRatio:F

    div-float/2addr v0, v5

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mDeltaX:F

    add-float/2addr v0, v5

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mRatio:F

    div-float/2addr v2, v5

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mDeltaY:F

    add-float/2addr v2, v5

    invoke-direct {v4, v0, v2}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPressure()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    long-to-int v2, v5

    invoke-virtual {v3, v4, v0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->addPoint(Landroid/graphics/PointF;FI)V

    goto/16 :goto_0

    :cond_3
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v4

    div-float/2addr v3, v4

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    add-float/2addr v2, v3

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    div-float/2addr v4, v5

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    new-instance v5, Landroid/graphics/PointF;

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mRatio:F

    div-float/2addr v2, v6

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mDeltaX:F

    add-float/2addr v2, v6

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mRatio:F

    div-float/2addr v3, v6

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mDeltaY:F

    add-float/2addr v3, v6

    invoke-direct {v5, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getHistoricalPressure(I)F

    move-result v2

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getHistoricalEventTime(I)J

    move-result-wide v6

    long-to-int v3, v6

    invoke-virtual {v4, v5, v2, v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->addPoint(Landroid/graphics/PointF;FI)V

    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    :pswitch_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    invoke-virtual {v3, p1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    :goto_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v2

    if-lt v0, v2, :cond_4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    div-float/2addr v2, v3

    mul-float/2addr v0, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    add-float/2addr v0, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    div-float/2addr v3, v4

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    new-instance v4, Landroid/graphics/PointF;

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mRatio:F

    div-float/2addr v0, v5

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mDeltaX:F

    add-float/2addr v0, v5

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mRatio:F

    div-float/2addr v2, v5

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mDeltaY:F

    add-float/2addr v2, v5

    invoke-direct {v4, v0, v2}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPressure()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    long-to-int v2, v5

    invoke-virtual {v3, v4, v0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->addPoint(Landroid/graphics/PointF;FI)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->invalidate()V

    goto/16 :goto_0

    :cond_4
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v4

    div-float/2addr v3, v4

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    add-float/2addr v2, v3

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    div-float/2addr v4, v5

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    new-instance v5, Landroid/graphics/PointF;

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mRatio:F

    div-float/2addr v2, v6

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mDeltaX:F

    add-float/2addr v2, v6

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mRatio:F

    div-float/2addr v3, v6

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mDeltaY:F

    add-float/2addr v3, v6

    invoke-direct {v5, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getHistoricalPressure(I)F

    move-result v2

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getHistoricalEventTime(I)J

    move-result-wide v6

    long-to-int v3, v6

    invoke-virtual {v4, v5, v2, v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->addPoint(Landroid/graphics/PointF;FI)V

    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_2

    :pswitch_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    invoke-virtual {v3, p1, v2}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    :goto_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v2

    if-lt v0, v2, :cond_6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    div-float/2addr v2, v3

    mul-float/2addr v0, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    add-float/2addr v0, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    div-float/2addr v3, v4

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    new-instance v4, Landroid/graphics/PointF;

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mRatio:F

    div-float/2addr v0, v5

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mDeltaX:F

    add-float/2addr v0, v5

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mRatio:F

    div-float/2addr v2, v5

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mDeltaY:F

    add-float/2addr v2, v5

    invoke-direct {v4, v0, v2}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPressure()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    long-to-int v2, v5

    invoke-virtual {v3, v4, v0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->addPoint(Landroid/graphics/PointF;FI)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadActionListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadActionListener;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadActionListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadActionListener;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    invoke-interface {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadActionListener;->onUpdate(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->updateFrameBuffer()V

    :cond_5
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    goto/16 :goto_0

    :cond_6
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v4

    div-float/2addr v3, v4

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    add-float/2addr v2, v3

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    div-float/2addr v4, v5

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    new-instance v5, Landroid/graphics/PointF;

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mRatio:F

    div-float/2addr v2, v6

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mDeltaX:F

    add-float/2addr v2, v6

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mRatio:F

    div-float/2addr v3, v6

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mDeltaY:F

    add-float/2addr v3, v6

    invoke-direct {v5, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getHistoricalPressure(I)F

    move-result v2

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getHistoricalEventTime(I)J

    move-result-wide v6

    long-to-int v3, v6

    invoke-virtual {v4, v5, v2, v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->addPoint(Landroid/graphics/PointF;FI)V

    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private onTouchZoomPad(Landroid/view/MotionEvent;)Z
    .locals 12

    const/high16 v11, 0x41500000    # 13.0f

    const/4 v0, 0x1

    const/high16 v10, 0x42080000    # 34.0f

    const/high16 v9, 0x40000000    # 2.0f

    const/high16 v8, 0x40800000    # 4.0f

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadPrePoint:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    sub-float/2addr v3, v1

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadPrePoint:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    sub-float/2addr v4, v2

    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5}, Landroid/graphics/RectF;-><init>()V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    and-int/lit16 v6, v6, 0xff

    packed-switch v6, :pswitch_data_0

    :cond_0
    :goto_0
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadState:I

    if-eqz v1, :cond_1b

    if-ne v6, v0, :cond_1

    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadState:I

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->updateFrameBuffer()V

    :cond_1
    :goto_1
    return v0

    :pswitch_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadHandler:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadHandler;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadHandler;->setMovingEnabled(Z)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadHandler:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadHandler;

    invoke-virtual {v3, v0}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadHandler;->removeMessages(I)V

    const/4 v3, 0x0

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadState:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadPrePoint:Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    iput v4, v3, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadPrePoint:Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    iput v4, v3, Landroid/graphics/PointF;->y:F

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadRect:Landroid/graphics/RectF;

    invoke-virtual {v3, v1, v2}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    const/high16 v4, 0x41880000    # 17.0f

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v4, v7

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    const/high16 v7, 0x41880000    # 17.0f

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v7, v8

    sub-float/2addr v4, v7

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->right:F

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v5, v3, v4, v7, v8}, Landroid/graphics/RectF;->set(FFFF)V

    invoke-virtual {v5, v1, v2}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string/jumbo v1, "ZoomPad"

    const-string/jumbo v2, "ZoomView resize contains"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x2

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadState:I

    goto :goto_0

    :cond_2
    const-string/jumbo v1, "ZoomPad"

    const-string/jumbo v2, "ZoomView contains"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadState:I

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarMovingRect:Landroid/graphics/Rect;

    float-to-int v4, v1

    float-to-int v5, v2

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string/jumbo v1, "ZoomPad"

    const-string/jumbo v2, "ZoomPadBar contains"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x3

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadState:I

    goto/16 :goto_0

    :cond_4
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadHandleRect:Landroid/graphics/Rect;

    float-to-int v1, v1

    float-to-int v2, v2

    invoke-virtual {v3, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "ZoomPad"

    const-string/jumbo v2, "ZoomPadMovingRect contains"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x4

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadState:I

    goto/16 :goto_0

    :pswitch_1
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadState:I

    packed-switch v5, :pswitch_data_1

    :cond_5
    :goto_2
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadState:I

    if-eq v3, v0, :cond_6

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadState:I

    const/4 v4, 0x3

    if-eq v3, v4, :cond_6

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadState:I

    const/4 v4, 0x4

    if-eq v3, v4, :cond_6

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadState:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_7

    :cond_6
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->updateFrameBuffer()V

    :cond_7
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadPrePoint:Landroid/graphics/PointF;

    iput v1, v3, Landroid/graphics/PointF;->x:F

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadPrePoint:Landroid/graphics/PointF;

    iput v2, v1, Landroid/graphics/PointF;->y:F

    goto/16 :goto_0

    :pswitch_2
    const-string/jumbo v5, "ZoomPad"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "ZoomView moving x="

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " y="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v5

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v8, v7, Landroid/graphics/RectF;->left:F

    sub-float/2addr v8, v3

    iput v8, v7, Landroid/graphics/RectF;->left:F

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v8, v7, Landroid/graphics/RectF;->right:F

    sub-float v3, v8, v3

    iput v3, v7, Landroid/graphics/RectF;->right:F

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v7, v3, Landroid/graphics/RectF;->top:F

    sub-float/2addr v7, v4

    iput v7, v3, Landroid/graphics/RectF;->top:F

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v7, v3, Landroid/graphics/RectF;->bottom:F

    sub-float v4, v7, v4

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    const/4 v4, 0x0

    cmpg-float v3, v3, v4

    if-gez v3, :cond_8

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    iput v4, v3, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    add-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    :cond_8
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mScreenWidth:I

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_9

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mScreenWidth:I

    int-to-float v4, v4

    sub-float/2addr v4, v5

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    sub-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mScreenWidth:I

    int-to-float v4, v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    sub-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    :cond_9
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewSaveRect:Landroid/graphics/RectF;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    invoke-virtual {v3, v4}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    goto/16 :goto_2

    :pswitch_3
    const-string/jumbo v3, "ZoomPad"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "ZoomBar moving x="

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v7, " y="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    float-to-int v3, v3

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    iget v7, v5, Landroid/graphics/RectF;->top:F

    float-to-int v8, v4

    int-to-float v8, v8

    sub-float/2addr v7, v8

    iput v7, v5, Landroid/graphics/RectF;->top:F

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    iget v7, v5, Landroid/graphics/RectF;->bottom:F

    float-to-int v8, v4

    int-to-float v8, v8

    sub-float/2addr v7, v8

    iput v7, v5, Landroid/graphics/RectF;->bottom:F

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v7, v10

    sub-float/2addr v5, v7

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadScrollLimit:I

    int-to-float v7, v7

    cmpg-float v5, v5, v7

    if-gez v5, :cond_a

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v7, v10

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadScrollLimit:I

    int-to-float v8, v8

    add-float/2addr v7, v8

    iput v7, v5, Landroid/graphics/RectF;->top:F

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v7, v10

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadScrollLimit:I

    int-to-float v8, v8

    add-float/2addr v7, v8

    int-to-float v8, v3

    add-float/2addr v7, v8

    iput v7, v5, Landroid/graphics/RectF;->bottom:F

    :cond_a
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mScreenHeight:I

    int-to-float v7, v7

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v8, v9

    sub-float/2addr v7, v8

    cmpl-float v5, v5, v7

    if-lez v5, :cond_b

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mScreenHeight:I

    int-to-float v7, v7

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v8, v9

    sub-float/2addr v7, v8

    int-to-float v3, v3

    sub-float v3, v7, v3

    iput v3, v5, Landroid/graphics/RectF;->top:F

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mScreenHeight:I

    int-to-float v5, v5

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v7, v9

    sub-float/2addr v5, v7

    iput v5, v3, Landroid/graphics/RectF;->bottom:F

    :cond_b
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadRect:Landroid/graphics/RectF;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v7, v10

    sub-float/2addr v5, v7

    iput v5, v3, Landroid/graphics/RectF;->top:F

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadRect:Landroid/graphics/RectF;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    iput v5, v3, Landroid/graphics/RectF;->bottom:F

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadMovingRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadMovingRect:Landroid/graphics/Rect;

    iget v7, v5, Landroid/graphics/Rect;->top:I

    float-to-int v8, v4

    sub-int/2addr v7, v8

    iput v7, v5, Landroid/graphics/Rect;->top:I

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadMovingRect:Landroid/graphics/Rect;

    iget v7, v5, Landroid/graphics/Rect;->bottom:I

    float-to-int v8, v4

    sub-int/2addr v7, v8

    iput v7, v5, Landroid/graphics/Rect;->bottom:I

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadMovingRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    int-to-float v5, v5

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v7, v10

    sub-float/2addr v5, v7

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadScrollLimit:I

    int-to-float v7, v7

    cmpg-float v5, v5, v7

    if-gez v5, :cond_c

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadMovingRect:Landroid/graphics/Rect;

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v7, v10

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadScrollLimit:I

    int-to-float v8, v8

    add-float/2addr v7, v8

    float-to-int v7, v7

    iput v7, v5, Landroid/graphics/Rect;->top:I

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadMovingRect:Landroid/graphics/Rect;

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v7, v10

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadScrollLimit:I

    int-to-float v8, v8

    add-float/2addr v7, v8

    int-to-float v8, v3

    add-float/2addr v7, v8

    float-to-int v7, v7

    iput v7, v5, Landroid/graphics/Rect;->bottom:I

    :cond_c
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadMovingRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    int-to-float v5, v5

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mScreenHeight:I

    int-to-float v7, v7

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v8, v9

    sub-float/2addr v7, v8

    cmpl-float v5, v5, v7

    if-lez v5, :cond_d

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadMovingRect:Landroid/graphics/Rect;

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mScreenHeight:I

    int-to-float v7, v7

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v8, v9

    sub-float/2addr v7, v8

    int-to-float v3, v3

    sub-float v3, v7, v3

    float-to-int v3, v3

    iput v3, v5, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadMovingRect:Landroid/graphics/Rect;

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mScreenHeight:I

    int-to-float v5, v5

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v7, v9

    sub-float/2addr v5, v7

    float-to-int v5, v5

    iput v5, v3, Landroid/graphics/Rect;->bottom:I

    :cond_d
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadHandleRect:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadMovingRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v7, v11

    float-to-int v7, v7

    sub-int/2addr v5, v7

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadMovingRect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    const/high16 v8, 0x41e80000    # 29.0f

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v8, v9

    float-to-int v8, v8

    sub-int/2addr v7, v8

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadMovingRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->left:I

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v9, v11

    float-to-int v9, v9

    add-int/2addr v8, v9

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadMovingRect:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v3, v5, v7, v8, v9}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarMovingRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarMovingRect:Landroid/graphics/Rect;

    iget v7, v5, Landroid/graphics/Rect;->top:I

    float-to-int v8, v4

    sub-int/2addr v7, v8

    iput v7, v5, Landroid/graphics/Rect;->top:I

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarMovingRect:Landroid/graphics/Rect;

    iget v7, v5, Landroid/graphics/Rect;->bottom:I

    float-to-int v8, v4

    sub-int/2addr v7, v8

    iput v7, v5, Landroid/graphics/Rect;->bottom:I

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarMovingRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadScrollLimit:I

    if-ge v5, v7, :cond_e

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarMovingRect:Landroid/graphics/Rect;

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadScrollLimit:I

    iput v7, v5, Landroid/graphics/Rect;->top:I

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarMovingRect:Landroid/graphics/Rect;

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadScrollLimit:I

    add-int/2addr v7, v3

    iput v7, v5, Landroid/graphics/Rect;->bottom:I

    :cond_e
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarMovingRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    int-to-float v5, v5

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->top:F

    cmpl-float v5, v5, v7

    if-lez v5, :cond_f

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarMovingRect:Landroid/graphics/Rect;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->top:F

    float-to-int v7, v7

    sub-int v3, v7, v3

    iput v3, v5, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarMovingRect:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    float-to-int v5, v5

    iput v5, v3, Landroid/graphics/Rect;->bottom:I

    :cond_f
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarRect:Landroid/graphics/Rect;

    iget v7, v5, Landroid/graphics/Rect;->top:I

    float-to-int v8, v4

    sub-int/2addr v7, v8

    iput v7, v5, Landroid/graphics/Rect;->top:I

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarRect:Landroid/graphics/Rect;

    iget v7, v5, Landroid/graphics/Rect;->bottom:I

    float-to-int v8, v4

    sub-int/2addr v7, v8

    iput v7, v5, Landroid/graphics/Rect;->bottom:I

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadScrollLimit:I

    if-ge v5, v7, :cond_10

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarRect:Landroid/graphics/Rect;

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadScrollLimit:I

    iput v7, v5, Landroid/graphics/Rect;->top:I

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarRect:Landroid/graphics/Rect;

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadScrollLimit:I

    add-int/2addr v7, v3

    iput v7, v5, Landroid/graphics/Rect;->bottom:I

    :cond_10
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    int-to-float v5, v5

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->top:F

    cmpl-float v5, v5, v7

    if-lez v5, :cond_11

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarRect:Landroid/graphics/Rect;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->top:F

    int-to-float v3, v3

    sub-float v3, v7, v3

    float-to-int v3, v3

    iput v3, v5, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarRect:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    float-to-int v5, v5

    iput v5, v3, Landroid/graphics/Rect;->bottom:I

    :cond_11
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarImageRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarImageRect:Landroid/graphics/Rect;

    iget v7, v5, Landroid/graphics/Rect;->top:I

    float-to-int v8, v4

    sub-int/2addr v7, v8

    iput v7, v5, Landroid/graphics/Rect;->top:I

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarImageRect:Landroid/graphics/Rect;

    iget v7, v5, Landroid/graphics/Rect;->bottom:I

    float-to-int v4, v4

    sub-int v4, v7, v4

    iput v4, v5, Landroid/graphics/Rect;->bottom:I

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarImageRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadScrollLimit:I

    if-ge v4, v5, :cond_12

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarImageRect:Landroid/graphics/Rect;

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadScrollLimit:I

    iput v5, v4, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarImageRect:Landroid/graphics/Rect;

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadScrollLimit:I

    add-int/2addr v5, v3

    iput v5, v4, Landroid/graphics/Rect;->bottom:I

    :cond_12
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarImageRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    cmpl-float v4, v4, v5

    if-lez v4, :cond_13

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarImageRect:Landroid/graphics/Rect;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    int-to-float v3, v3

    sub-float v3, v5, v3

    float-to-int v3, v3

    iput v3, v4, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarImageRect:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    float-to-int v4, v4

    iput v4, v3, Landroid/graphics/Rect;->bottom:I

    :cond_13
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mRelative:Landroid/widget/RelativeLayout;

    if-eqz v3, :cond_5

    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarImageRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarImageRect:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    invoke-direct {v3, v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarImageRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    iput v4, v3, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarImageRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    iput v4, v3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mRelative:Landroid/widget/RelativeLayout;

    invoke-virtual {v4, v3}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_2

    :pswitch_4
    const-string/jumbo v4, "ZoomPad"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "ZoomBar moving rect x="

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v7, " y="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadMovingRect:Landroid/graphics/Rect;

    iget v5, v4, Landroid/graphics/Rect;->left:I

    int-to-float v5, v5

    sub-float v3, v5, v3

    float-to-int v3, v3

    iput v3, v4, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadMovingRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v4

    div-float/2addr v4, v9

    cmpg-float v3, v3, v4

    if-gez v3, :cond_14

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadMovingRect:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v4

    float-to-int v4, v4

    div-int/lit8 v4, v4, 0x2

    iput v4, v3, Landroid/graphics/Rect;->left:I

    :cond_14
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadMovingRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v4

    const/high16 v5, 0x40400000    # 3.0f

    mul-float/2addr v4, v5

    div-float/2addr v4, v8

    cmpl-float v3, v3, v4

    if-lez v3, :cond_15

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadMovingRect:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v4

    float-to-int v4, v4

    mul-int/lit8 v4, v4, 0x3

    div-int/lit8 v4, v4, 0x4

    iput v4, v3, Landroid/graphics/Rect;->left:I

    :cond_15
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadHandleRect:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadMovingRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v5, v11

    float-to-int v5, v5

    sub-int/2addr v4, v5

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadMovingRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    const/high16 v7, 0x41e80000    # 29.0f

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v7, v8

    float-to-int v7, v7

    sub-int/2addr v5, v7

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadMovingRect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->left:I

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v8, v11

    float-to-int v8, v8

    add-int/2addr v7, v8

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadMovingRect:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v3, v4, v5, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_2

    :pswitch_5
    const-string/jumbo v4, "ZoomPad"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "Zoom view resize x="

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v7, " y="

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v5

    div-float/2addr v4, v5

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v7, v5, Landroid/graphics/RectF;->right:F

    sub-float/2addr v7, v3

    iput v7, v5, Landroid/graphics/RectF;->right:F

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v7, v5, Landroid/graphics/RectF;->bottom:F

    mul-float/2addr v3, v4

    sub-float v3, v7, v3

    iput v3, v5, Landroid/graphics/RectF;->bottom:F

    const-string/jumbo v3, "ZoomPad"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "Zoom view resize ratio="

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " right="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->right:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " bottom="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v5

    div-float/2addr v5, v8

    add-float/2addr v4, v5

    cmpg-float v3, v3, v4

    if-gez v3, :cond_16

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v5

    div-float/2addr v5, v8

    add-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    :cond_16
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    div-float/2addr v5, v8

    add-float/2addr v4, v5

    cmpg-float v3, v3, v4

    if-gez v3, :cond_5

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    div-float/2addr v5, v8

    add-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_2

    :pswitch_6
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadState:I

    if-eq v1, v0, :cond_17

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadState:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    :cond_17
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadState:I

    if-ne v1, v0, :cond_18

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1a

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadActionListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadActionListener;

    if-eqz v1, :cond_18

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadActionListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadActionListener;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mDeltaX:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mDeltaY:F

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    sub-float/2addr v3, v4

    invoke-interface {v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadActionListener;->onChangePan(FF)V

    :cond_18
    :goto_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    const/4 v3, 0x0

    cmpg-float v2, v2, v3

    if-gez v2, :cond_19

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    iput v3, v2, Landroid/graphics/RectF;->top:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    add-float/2addr v3, v1

    iput v3, v2, Landroid/graphics/RectF;->bottom:F

    :cond_19
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v4, v8

    sub-float/2addr v3, v4

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    sub-float v1, v3, v1

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v3, v8

    sub-float/2addr v1, v3

    iput v1, v2, Landroid/graphics/RectF;->top:F

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v3, v8

    sub-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    :cond_1a
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    const/4 v2, 0x0

    cmpg-float v1, v1, v2

    if-gez v1, :cond_18

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadActionListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadActionListener;

    if-eqz v1, :cond_18

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadActionListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadActionListener;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mDeltaX:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mDeltaY:F

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    add-float/2addr v3, v4

    invoke-interface {v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadActionListener;->onChangePan(FF)V

    goto :goto_3

    :cond_1b
    const/4 v0, 0x0

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_6
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
        :pswitch_5
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    if-eqz p1, :cond_0

    const/4 v1, 0x3

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->getResourceDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_0
    if-eqz p2, :cond_1

    new-array v1, v4, [I

    const v2, 0x10100a7    # android.R.attr.state_pressed

    aput v2, v1, v3

    invoke-direct {p0, p2}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->getResourceDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    new-array v1, v4, [I

    const v2, 0x10100a1    # android.R.attr.state_selected

    aput v2, v1, v3

    invoke-direct {p0, p2}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->getResourceDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_1
    if-eqz p3, :cond_2

    new-array v1, v4, [I

    const v2, 0x101009c    # android.R.attr.state_focused

    aput v2, v1, v3

    invoke-direct {p0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->getResourceDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_2
    return-object v0

    nop

    :array_0
    .array-data 4
        -0x10100a7
        -0x10100a1
        -0x101009c
    .end array-data
.end method

.method private setZoomPadButton(I)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    float-to-int v0, v0

    div-int/lit8 v0, v0, 0x2

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v2, v1, Landroid/graphics/RectF;->left:F

    int-to-float v3, v0

    sub-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->left:F

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v2, v1, Landroid/graphics/RectF;->right:F

    int-to-float v0, v0

    sub-float v0, v2, v0

    iput v0, v1, Landroid/graphics/RectF;->right:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    cmpg-float v0, v0, v4

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iput v4, v1, Landroid/graphics/RectF;->left:F

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iput v0, v1, Landroid/graphics/RectF;->right:F

    :cond_0
    :goto_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->updateFrameBuffer()V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mScreenWidth:I

    int-to-float v2, v2

    cmpl-float v1, v1, v2

    if-nez v1, :cond_1

    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->setZoomPadButton(I)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v2, v1, Landroid/graphics/RectF;->left:F

    int-to-float v3, v0

    add-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->left:F

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v2, v1, Landroid/graphics/RectF;->right:F

    int-to-float v0, v0

    add-float/2addr v0, v2

    iput v0, v1, Landroid/graphics/RectF;->right:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->right:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mScreenWidth:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mScreenWidth:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    sub-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->left:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mScreenWidth:I

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->right:F

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewSaveRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iput v1, v0, Landroid/graphics/RectF;->left:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewSaveRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    iput v1, v0, Landroid/graphics/RectF;->right:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v1, v0, Landroid/graphics/RectF;->top:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewSaveRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->top:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v1, v0, Landroid/graphics/RectF;->bottom:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewSaveRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    sub-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->top:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private setZoomPadSize(II)I
    .locals 10

    const/high16 v9, 0x42080000    # 34.0f

    const/high16 v8, 0x41500000    # 13.0f

    const/high16 v7, 0x40800000    # 4.0f

    const/high16 v5, 0x40000000    # 2.0f

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadRect:Landroid/graphics/RectF;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v1, v5

    iput v1, v0, Landroid/graphics/RectF;->left:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadRect:Landroid/graphics/RectF;

    int-to-float v1, p1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v2, v5

    sub-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadRect:Landroid/graphics/RectF;

    int-to-double v1, p2

    const-wide v3, 0x3fe70a3d70a3d70aL    # 0.72

    mul-double/2addr v1, v3

    double-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->top:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadRect:Landroid/graphics/RectF;

    int-to-float v1, p2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v2, v5

    sub-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    const/high16 v0, 0x41b80000    # 23.0f

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadScrollLimit:I

    int-to-float v0, p2

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    sub-float/2addr v0, v1

    float-to-int v0, v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    float-to-int v2, v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    float-to-int v3, v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    float-to-int v4, v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    float-to-int v5, v5

    int-to-float v5, v5

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v6, v9

    add-float/2addr v5, v6

    float-to-int v5, v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarMovingRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    add-int/lit8 v2, v2, -0x32

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, 0x32

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    iget v2, v1, Landroid/graphics/RectF;->top:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v3, v9

    add-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->top:F

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadMovingRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    float-to-int v2, v2

    mul-int/lit8 v2, v2, 0x3

    div-int/lit8 v2, v2, 0x4

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    float-to-int v4, v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    float-to-int v5, v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadHandleRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadMovingRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v3, v8

    float-to-int v3, v3

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadMovingRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    const/high16 v4, 0x41e80000    # 29.0f

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v4, v5

    float-to-int v4, v4

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadMovingRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v5, v8

    float-to-int v5, v5

    add-int/2addr v4, v5

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadMovingRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    div-float/2addr v3, v7

    add-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->right:F

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    div-float/2addr v3, v7

    add-float/2addr v2, v3

    iput v2, v1, Landroid/graphics/RectF;->bottom:F

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewSaveRect:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    invoke-virtual {v1, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarImageRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    const/high16 v3, 0x42100000    # 36.0f

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v3, v4

    mul-float/2addr v3, v7

    float-to-int v3, v3

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmap:[Landroid/graphics/Bitmap;

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    float-to-int v3, v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    float-to-int v4, v4

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v4, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    aput-object v3, v1, v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v0

    :catch_0
    move-exception v1

    const/4 v1, 0x2

    const-string/jumbo v2, "Bitmap failed to create."

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method private setZoomPadView()V
    .locals 9
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    const/4 v3, 0x0

    const/16 v8, 0x10

    const/4 v7, 0x1

    const/high16 v6, 0x42100000    # 36.0f

    const/high16 v5, 0x42080000    # 34.0f

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mRelative:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarImageRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarImageRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarImageRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarImageRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mRelative:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mRelative:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mRelative:Landroid/widget/RelativeLayout;

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarImageRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarImageRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarImageRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarImageRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mRelative:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    new-instance v1, Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mLeftButton:Landroid/widget/ImageButton;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v2, v6

    float-to-int v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v3, v5

    float-to-int v3, v3

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mLeftButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v1}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mLeftButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v7}, Landroid/widget/ImageButton;->setFocusable(Z)V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v1, v8, :cond_1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mLeftButton:Landroid/widget/ImageButton;

    const-string/jumbo v2, "zoompad_menu_left"

    const-string/jumbo v3, "zoompad_menu_press"

    const-string/jumbo v4, "zoompad_menu_focus"

    invoke-direct {p0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_1
    new-instance v1, Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mRightButton:Landroid/widget/ImageButton;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v2, v6

    float-to-int v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v3, v5

    float-to-int v3, v3

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mRightButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v1}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mRightButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v7}, Landroid/widget/ImageButton;->setFocusable(Z)V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v1, v8, :cond_2

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mRightButton:Landroid/widget/ImageButton;

    const-string/jumbo v2, "zoompad_menu_right"

    const-string/jumbo v3, "zoompad_menu_press"

    const-string/jumbo v4, "zoompad_menu_focus"

    invoke-direct {p0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_2
    new-instance v1, Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mEnterButton:Landroid/widget/ImageButton;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v2, v6

    float-to-int v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v3, v5

    float-to-int v3, v3

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mEnterButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v1}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mEnterButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v7}, Landroid/widget/ImageButton;->setFocusable(Z)V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v1, v8, :cond_3

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mEnterButton:Landroid/widget/ImageButton;

    const-string/jumbo v2, "zoompad_menu_enter"

    const-string/jumbo v3, "zoompad_menu_press"

    const-string/jumbo v4, "zoompad_menu_focus"

    invoke-direct {p0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_3
    new-instance v1, Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mCloseButton:Landroid/widget/ImageButton;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v2, v6

    float-to-int v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v3, v5

    float-to-int v3, v3

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mCloseButton:Landroid/widget/ImageButton;

    invoke-virtual {v2, v1}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mCloseButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v7}, Landroid/widget/ImageButton;->setFocusable(Z)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mCloseButton:Landroid/widget/ImageButton;

    const-string/jumbo v2, "string_close"

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->getResourceString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v1, v8, :cond_4

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mCloseButton:Landroid/widget/ImageButton;

    const-string/jumbo v2, "zoompad_menu_close"

    const-string/jumbo v3, "zoompad_menu_press"

    const-string/jumbo v4, "zoompad_menu_focus"

    invoke-direct {p0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mLeftButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mRightButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mEnterButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mCloseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mRelative:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mLeftButton:Landroid/widget/ImageButton;

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$1;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mRightButton:Landroid/widget/ImageButton;

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$2;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mEnterButton:Landroid/widget/ImageButton;

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$3;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$3;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mCloseButton:Landroid/widget/ImageButton;

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$4;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$4;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_0

    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mLeftButton:Landroid/widget/ImageButton;

    const-string/jumbo v2, "zoompad_menu_left"

    const-string/jumbo v3, "zoompad_menu_press"

    const-string/jumbo v4, "zoompad_menu_focus"

    invoke-direct {p0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mRightButton:Landroid/widget/ImageButton;

    const-string/jumbo v2, "zoompad_menu_right"

    const-string/jumbo v3, "zoompad_menu_press"

    const-string/jumbo v4, "zoompad_menu_focus"

    invoke-direct {p0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mEnterButton:Landroid/widget/ImageButton;

    const-string/jumbo v2, "zoompad_menu_enter"

    const-string/jumbo v3, "zoompad_menu_press"

    const-string/jumbo v4, "zoompad_menu_focus"

    invoke-direct {p0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_3

    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mCloseButton:Landroid/widget/ImageButton;

    const-string/jumbo v2, "zoompad_menu_close"

    const-string/jumbo v3, "zoompad_menu_press"

    const-string/jumbo v4, "zoompad_menu_focus"

    invoke-direct {p0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_4
.end method

.method private updateFrameBuffer()V
    .locals 8

    const/4 v6, 0x0

    const/4 v2, 0x0

    new-instance v3, Landroid/graphics/Canvas;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmap:[Landroid/graphics/Bitmap;

    const/4 v1, 0x4

    aget-object v0, v0, v1

    invoke-direct {v3, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    sget-object v0, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v3, v2, v0}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    invoke-direct {p0, v4, v0}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->ExtendRectFromRectF(Landroid/graphics/Rect;Landroid/graphics/RectF;)V

    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5}, Landroid/graphics/RectF;-><init>()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    invoke-virtual {v5, v6, v6, v0, v1}, Landroid/graphics/RectF;->set(FFFF)V

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->invalidate()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    invoke-virtual {v0, v2, v2}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v6

    invoke-virtual {v0, v2, v2, v6}, Landroid/graphics/Bitmap;->setPixel(III)V

    if-nez v1, :cond_2

    invoke-direct {p0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->MakeNewExtendRect(Landroid/graphics/RectF;)Landroid/graphics/Rect;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mSrcPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v0, v4, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_1
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    invoke-direct {p0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->MakeNewExtendRect(Landroid/graphics/RectF;)Landroid/graphics/Rect;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v0, v4, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_1
.end method

.method private updateZoomPad(Landroid/graphics/Canvas;)V
    .locals 9

    const/high16 v4, 0x41880000    # 17.0f

    const/high16 v8, 0x40000000    # 2.0f

    const/4 v7, 0x4

    const/4 v6, 0x0

    const/4 v5, 0x0

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadEnabled:Z

    if-eqz v0, :cond_0

    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v2, v4

    sub-float/2addr v1, v2

    float-to-int v1, v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    float-to-int v2, v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    float-to-int v3, v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    float-to-int v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadState:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmap:[Landroid/graphics/Bitmap;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmap:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v7

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmap:[Landroid/graphics/Bitmap;

    aget-object v2, v2, v7

    invoke-virtual {v2, v5, v5}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v2

    invoke-virtual {v1, v5, v5, v2}, Landroid/graphics/Bitmap;->setPixel(III)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmap:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v7

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->MakeNewExtendRect(Landroid/graphics/RectF;)Landroid/graphics/Rect;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v6, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadPaint:Landroid/graphics/Paint;

    const v2, -0xb8b4b9

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadPaint:Landroid/graphics/Paint;

    const v2, 0x4779caf2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadMovingRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmap:[Landroid/graphics/Bitmap;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadHandleRect:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v6, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadRect:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadLinePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmap:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v5

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarMovingRect:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v6, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mScreenHeight:I

    int-to-float v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v3, v8

    sub-float/2addr v2, v3

    cmpg-float v1, v1, v2

    if-gez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mOnePT:F

    mul-float/2addr v2, v8

    add-float/2addr v1, v2

    float-to-int v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mScreenWidth:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mScreenHeight:I

    invoke-virtual {v0, v5, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadPaint:Landroid/graphics/Paint;

    const v2, -0x848485

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmap:[Landroid/graphics/Bitmap;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v6, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public close()V
    .locals 7

    const/16 v6, 0x10

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmap:[Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmap:[Landroid/graphics/Bitmap;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-lt v0, v2, :cond_4

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmap:[Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mRelative:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v6, :cond_6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mLeftButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v6, :cond_7

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mRightButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v6, :cond_8

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mEnterButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v6, :cond_9

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mCloseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_4
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mLeftButton:Landroid/widget/ImageButton;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mRightButton:Landroid/widget/ImageButton;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mEnterButton:Landroid/widget/ImageButton;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mCloseButton:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mRelative:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->removeAllViews()V

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mRelative:Landroid/widget/RelativeLayout;

    :cond_0
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewPaint:Landroid/graphics/Paint;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadPaint:Landroid/graphics/Paint;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadLinePaint:Landroid/graphics/Paint;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewSaveRect:Landroid/graphics/RectF;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadRect:Landroid/graphics/RectF;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadMovingRect:Landroid/graphics/Rect;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarRect:Landroid/graphics/Rect;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarMovingRect:Landroid/graphics/Rect;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBarImageRect:Landroid/graphics/Rect;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadHandleRect:Landroid/graphics/Rect;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadHandler:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadHandler;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadPrePoint:Landroid/graphics/PointF;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mStroke:Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;->destroyPen(Lcom/samsung/android/sdk/pen/pen/SpenPen;)V

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;->close()V

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    :cond_2
    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mContext:Landroid/content/Context;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mSdkResources:Landroid/content/res/Resources;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadActionListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadActionListener;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mCanvasLayer:Ljava/util/ArrayList;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mSrcPaint:Landroid/graphics/Paint;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mAntiAliasPaint:Landroid/graphics/Paint;

    :cond_3
    return-void

    :cond_4
    aget-object v3, v1, v0

    if-eqz v3, :cond_5

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v4

    if-nez v4, :cond_5

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mLeftButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1

    :cond_7
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mRightButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    :cond_8
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mEnterButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_3

    :cond_9
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mCloseButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v5}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_4
.end method

.method public getZoomViewRect()Landroid/graphics/RectF;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    return-object v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->updateZoomPad(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadEnabled:Z

    if-eqz v1, :cond_2

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->onTouchZoomPad(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->onTouchInputPen(Landroid/view/MotionEvent;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public setActionListener(Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadActionListener;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadActionListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadActionListener;

    return-void
.end method

.method public setCanvasSize(III)I
    .locals 3

    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mScreenWidth:I

    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mScreenHeight:I

    iput p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mScreenHeightExceptSIP:I

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mScreenHeightExceptSIP:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mScreenWidth:I

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mScreenHeight:I

    if-eqz v1, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mScreenWidth:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mScreenHeight:I

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->setZoomPadSize(II)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const-string/jumbo v1, "ZoomPad"

    const-string/jumbo v2, "The width and height of screen know not yet"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setLayer(Ljava/util/ArrayList;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mCanvasLayer:Ljava/util/ArrayList;

    return-void
.end method

.method public setPanAndZoom(FFF)V
    .locals 3

    const-string/jumbo v0, "ZoomPad"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "], ratio="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mDeltaX:F

    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mDeltaY:F

    iput p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mRatio:F

    return-void
.end method

.method public setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V
    .locals 5

    const/4 v4, 0x4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    if-eqz v0, :cond_2

    :try_start_0
    iget-object v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mPenName:Ljava/lang/String;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;->destroyPen(Lcom/samsung/android/sdk/pen/pen/SpenPen;)V

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    iget-object v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;->createPen(Ljava/lang/String;)Lcom/samsung/android/sdk/pen/pen/SpenPen;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmap:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v4

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmapRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    div-float/2addr v2, v3

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setSize(F)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->getPenAttribute(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iget-boolean v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setCurveEnabled(Z)V

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->getPenAttribute(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mCurrentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iget-object v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setAdvancedSetting(Ljava/lang/String;)V

    :cond_2
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public setZoomViewPosition(FF)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iput p1, v0, Landroid/graphics/RectF;->left:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomViewRect:Landroid/graphics/RectF;

    iput p2, v0, Landroid/graphics/RectF;->top:F

    return-void
.end method

.method public startZoomPad(Landroid/view/ViewGroup;)V
    .locals 4

    const/4 v3, 0x1

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mParentView:Landroid/view/ViewGroup;

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadEnabled:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->setZoomPadView()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mParentView:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mParentView:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mParentView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mRelative:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmap:[Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    const-string/jumbo v2, "zoompad_button"

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->getResourceBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    aput-object v2, v0, v1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmap:[Landroid/graphics/Bitmap;

    const-string/jumbo v1, "zoompad_handle_bottom"

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->getResourceBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    aput-object v1, v0, v3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmap:[Landroid/graphics/Bitmap;

    const/4 v1, 0x2

    const-string/jumbo v2, "zoompad_selected_handle_normal"

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->getResourceBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    aput-object v2, v0, v1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmap:[Landroid/graphics/Bitmap;

    const/4 v1, 0x3

    const-string/jumbo v2, "zoompad_selected_handle_press"

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->getResourceBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    aput-object v2, v0, v1

    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadEnabled:Z

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->updateFrameBuffer()V

    :cond_1
    return-void
.end method

.method public stopZoomPad()V
    .locals 6

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadEnabled:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mParentView:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mParentView:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mRelative:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mParentView:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmap:[Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadBitmap:[Landroid/graphics/Bitmap;

    array-length v3, v2

    move v0, v1

    :goto_0
    if-lt v0, v3, :cond_4

    :cond_1
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadEnabled:Z

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadActionListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadActionListener;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->mZoomPadActionListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadActionListener;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadActionListener;->onStop()V

    :cond_3
    return-void

    :cond_4
    aget-object v4, v2, v0

    if-eqz v4, :cond_5

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v5

    if-nez v5, :cond_5

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
