.class interface abstract Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final PAGE_DIRECTION_LEFT:I = 0x0

.field public static final PAGE_DIRECTION_RIGHT:I = 0x1


# virtual methods
.method public abstract close()V
.end method

.method public abstract drawAnimation(Landroid/graphics/Canvas;)V
.end method

.method public abstract isWorking()Z
.end method

.method public abstract saveScreenshot()Z
.end method

.method public abstract setCanvasInformation(IIII)V
.end method

.method public abstract setPaint(Landroid/graphics/Paint;)V
.end method

.method public abstract setScreenResolution(II)V
.end method

.method public abstract startAnimation(I)Z
.end method
