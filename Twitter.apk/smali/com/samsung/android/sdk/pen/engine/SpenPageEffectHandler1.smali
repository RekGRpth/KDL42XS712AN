.class Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;
.super Landroid/os/Handler;
.source "Twttr"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;


# static fields
.field private static final FRAME_COUNT:I = 0xc

.field private static final MIN_RATIO:F = 0.98f

.field private static final PAGE_EFFECT_TIMER_INTERVAL:I = 0x0

.field private static final SIZE_FACTOR:F = 0.0016666651f


# instance fields
.field private mBackPaint:Landroid/graphics/Paint;

.field private mBackRect:[Landroid/graphics/Rect;

.field private mBmpGradient:Landroid/graphics/Bitmap;

.field private mCanvasHeight:I

.field private mCanvasWidth:I

.field private mCount:I

.field private mDirection:I

.field private mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

.field private mScreenHeight:I

.field private mScreenWidth:I

.field private mShot0:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

.field private mShot1:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

.field private mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

.field private mShotR:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

.field private mStartX:I

.field private mStartY:I

.field private mWorking:Z


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;)V
    .locals 4

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot0:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot1:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    const/4 v0, 0x7

    new-array v0, v0, [Landroid/graphics/Rect;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBackRect:[Landroid/graphics/Rect;

    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBackRect:[Landroid/graphics/Rect;

    array-length v2, v2

    if-lt v0, v2, :cond_0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mWorking:Z

    return-void

    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBackRect:[Landroid/graphics/Rect;

    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidth:I

    return v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasHeight:I

    return v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;)Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    return-object v0
.end method

.method private endAnimation()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->removeMessages(I)V

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mWorking:Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;->onFinish()V

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot0:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot0:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->clean()V

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot1:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot1:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->clean()V

    :cond_2
    return-void
.end method

.method private makeShadowBitmap()Z
    .locals 9

    const/4 v5, 0x0

    const/16 v4, 0xff

    const/4 v1, 0x0

    const/4 v8, 0x0

    :try_start_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidth:I

    div-int/lit8 v0, v0, 0xa

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasHeight:I

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBmpGradient:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    new-instance v0, Landroid/graphics/LinearGradient;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidth:I

    int-to-float v2, v2

    const/high16 v3, 0x41200000    # 10.0f

    div-float v3, v2, v3

    const/16 v2, 0x7d

    invoke-static {v2, v8, v8, v8}, Landroid/graphics/Color;->argb(IIII)I

    move-result v5

    invoke-static {v8, v4, v4, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v6

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    move v2, v1

    move v4, v1

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    new-instance v0, Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBmpGradient:Landroid/graphics/Bitmap;

    invoke-direct {v0, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v2, Landroid/graphics/Rect;

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidth:I

    div-int/lit8 v3, v3, 0xa

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasHeight:I

    invoke-direct {v2, v8, v8, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBmpGradient:Landroid/graphics/Bitmap;

    move v0, v8

    goto :goto_0

    :catch_1
    move-exception v0

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBmpGradient:Landroid/graphics/Bitmap;

    move v0, v8

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->removeMessages(I)V

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mWorking:Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot0:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot0:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->clean()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot0:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->src:Landroid/graphics/Rect;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot0:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot0:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot1:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot1:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->clean()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot1:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->src:Landroid/graphics/Rect;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot1:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot1:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBmpGradient:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBmpGradient:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBmpGradient:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBmpGradient:Landroid/graphics/Bitmap;

    :cond_2
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBackRect:[Landroid/graphics/Rect;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    return-void
.end method

.method public drawAnimation(Landroid/graphics/Canvas;)V
    .locals 6

    const/4 v5, 0x0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBackRect:[Landroid/graphics/Rect;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-lt v0, v2, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotR:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->src:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotR:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->bmp:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotR:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->src:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotR:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    invoke-virtual {p1, v0, v1, v2, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot0:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->src:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->bmp:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->src:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    invoke-virtual {p1, v0, v1, v2, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBmpGradient:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartY:I

    int-to-float v2, v2

    invoke-virtual {p1, v0, v1, v2, v5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    :cond_1
    return-void

    :cond_2
    aget-object v3, v1, v0

    invoke-virtual {v3}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBackPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 9

    const/high16 v7, 0x40000000    # 2.0f

    const v6, 0x3ada7400

    const/4 v8, 0x0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCount:I

    const/16 v1, 0xc

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->endAnimation()V

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidth:I

    int-to-float v0, v0

    const/high16 v1, 0x41400000    # 12.0f

    div-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mDirection:I

    if-nez v1, :cond_1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCount:I

    rsub-int/lit8 v1, v1, 0xc

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    :goto_1
    neg-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartX:I

    int-to-float v1, v1

    add-float/2addr v0, v1

    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-lez v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->src:Landroid/graphics/Rect;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidth:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasHeight:I

    invoke-virtual {v1, v8, v8, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    float-to-int v2, v0

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartY:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidth:I

    int-to-float v4, v4

    add-float/2addr v0, v4

    float-to-int v0, v0

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartY:I

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasHeight:I

    add-int/2addr v4, v5

    invoke-virtual {v1, v2, v3, v0, v4}, Landroid/graphics/Rect;->set(IIII)V

    :goto_2
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mDirection:I

    if-nez v0, :cond_3

    const/high16 v0, 0x3f800000    # 1.0f

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCount:I

    int-to-float v1, v1

    mul-float/2addr v1, v6

    sub-float/2addr v0, v1

    :goto_3
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidth:I

    int-to-float v1, v1

    mul-float/2addr v1, v0

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasHeight:I

    int-to-float v2, v2

    mul-float/2addr v2, v0

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartX:I

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidth:I

    int-to-float v4, v4

    sub-float/2addr v4, v1

    div-float/2addr v4, v7

    add-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartY:I

    int-to-float v4, v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasHeight:I

    int-to-float v5, v5

    sub-float/2addr v5, v2

    div-float/2addr v5, v7

    add-float/2addr v4, v5

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    int-to-float v5, v5

    invoke-static {v3, v5}, Ljava/lang/Math;->max(FF)F

    move-result v5

    add-float/2addr v1, v3

    add-float/2addr v2, v4

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotR:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->src:Landroid/graphics/Rect;

    sub-float v3, v5, v3

    div-float v0, v3, v0

    float-to-int v0, v0

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidth:I

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasHeight:I

    invoke-virtual {v6, v0, v8, v3, v7}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotR:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    float-to-int v3, v5

    float-to-int v4, v4

    float-to-int v1, v1

    float-to-int v2, v2

    invoke-virtual {v0, v3, v4, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBackRect:[Landroid/graphics/Rect;

    aget-object v0, v0, v8

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mScreenWidth:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotR:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0, v1, v8, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBackRect:[Landroid/graphics/Rect;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotR:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mScreenWidth:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mScreenHeight:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBackRect:[Landroid/graphics/Rect;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotR:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotR:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotR:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBackRect:[Landroid/graphics/Rect;

    const/4 v1, 0x3

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotR:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotR:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mScreenWidth:I

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotR:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBackRect:[Landroid/graphics/Rect;

    const/4 v1, 0x4

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0, v8, v8, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBackRect:[Landroid/graphics/Rect;

    const/4 v1, 0x5

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v8, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBackRect:[Landroid/graphics/Rect;

    const/4 v1, 0x6

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mScreenHeight:I

    invoke-virtual {v0, v8, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;->onUpdate()V

    const-wide/16 v0, 0x0

    invoke-virtual {p0, v8, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    :cond_1
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCount:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    goto/16 :goto_1

    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->src:Landroid/graphics/Rect;

    neg-float v2, v0

    float-to-int v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidth:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasHeight:I

    invoke-virtual {v1, v2, v8, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartY:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidth:I

    int-to-float v3, v3

    add-float/2addr v0, v3

    float-to-int v0, v0

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartY:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasHeight:I

    add-int/2addr v3, v4

    invoke-virtual {v1, v8, v2, v0, v3}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_2

    :cond_3
    const v0, 0x3f7ae148    # 0.98f

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCount:I

    add-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    mul-float/2addr v1, v6

    add-float/2addr v0, v1

    goto/16 :goto_3
.end method

.method public isWorking()Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mWorking:Z

    return v0
.end method

.method public saveScreenshot()Z
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->makeShadowBitmap()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot0:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->saveScreenshot()Z

    move-result v0

    goto :goto_0
.end method

.method public setCanvasInformation(IIII)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartX:I

    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartY:I

    iput p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidth:I

    iput p4, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasHeight:I

    return-void
.end method

.method public setPaint(Landroid/graphics/Paint;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mBackPaint:Landroid/graphics/Paint;

    return-void
.end method

.method public setScreenResolution(II)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mScreenWidth:I

    if-ne p1, v0, :cond_1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mScreenHeight:I

    if-ne p2, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mScreenWidth:I

    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mScreenHeight:I

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mWorking:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mWorking:Z

    goto :goto_0
.end method

.method public startAnimation(I)Z
    .locals 8

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot1:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->saveScreenshot()Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot0:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->src:Landroid/graphics/Rect;

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidth:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasHeight:I

    invoke-virtual {v2, v0, v0, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot0:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartX:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartY:I

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartX:I

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidth:I

    add-int/2addr v5, v6

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mStartY:I

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasHeight:I

    add-int/2addr v6, v7

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Rect;->set(IIII)V

    if-nez p1, :cond_1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot1:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot0:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotR:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    :goto_1
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCount:I

    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mDirection:I

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mWorking:Z

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v0, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->sendEmptyMessageDelayed(IJ)Z

    move v0, v1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot0:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotL:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShot1:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mShotR:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;

    goto :goto_1
.end method
