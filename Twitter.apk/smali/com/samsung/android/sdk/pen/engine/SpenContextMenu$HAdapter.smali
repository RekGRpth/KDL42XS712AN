.class Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;
.super Landroid/widget/BaseAdapter;
.source "Twttr"


# instance fields
.field private final mOnButtonClicked:Landroid/view/View$OnClickListener;

.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)V
    .locals 1

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter$1;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->mOnButtonClicked:Landroid/view/View$OnClickListener;

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 13
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "SpenContextMenu"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "mContext is NULL at position = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    new-instance v4, Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {v4, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v6, 0x0

    invoke-virtual {v0, v1, v2, v3, v6}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    const/4 v0, 0x0

    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$7()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    const/16 v3, 0x62

    const/16 v2, 0x48

    const/16 v1, 0x5c

    const/4 v0, 0x2

    :goto_1
    add-int/lit8 v6, p1, 0x1

    rem-int/lit8 v6, v6, 0x2

    if-nez v6, :cond_5

    new-instance v3, Landroid/widget/ImageView;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/Context;

    move-result-object v6

    invoke-direct {v3, v6}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsNormalMode:Z
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v1, 0x1

    int-to-float v2, v2

    invoke-static {v1, v2, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    :goto_2
    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v0, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, v0, v1, v2, v5}, Landroid/widget/ImageView;->setPadding(IIII)V

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$7()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/res/Resources;

    move-result-object v1

    const-string/jumbo v2, "tw_quick_bubble_divider_holo_light"

    const-string/jumbo v5, "drawable"

    const-string/jumbo v6, "com.samsung.android.sdk.spen30"

    invoke-virtual {v1, v2, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_3
    invoke-virtual {v4, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    move-object v0, v4

    goto/16 :goto_0

    :cond_1
    const/16 v3, 0x48

    const/16 v2, 0x3d

    const/16 v1, 0x4e

    const/4 v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v2, 0x1

    int-to-float v1, v1

    invoke-static {v2, v1, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/res/Resources;

    move-result-object v1

    const-string/jumbo v2, "tw_quick_bubble_divider_holo_light"

    const-string/jumbo v5, "drawable"

    const-string/jumbo v6, "com.samsung.android.sdk.spen30"

    invoke-virtual {v1, v2, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_3

    :cond_4
    const v0, -0x777778

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    goto :goto_3

    :cond_5
    div-int/lit8 v6, p1, 0x2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsNormalMode:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Z

    move-result v0

    if-eqz v0, :cond_f

    const/4 v0, 0x1

    int-to-float v1, v3

    invoke-static {v0, v1, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v1, v0

    const/4 v0, 0x1

    int-to-float v2, v2

    invoke-static {v0, v2, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    :goto_4
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v1, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    new-instance v3, Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {v3, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v2}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v7, 0x0

    invoke-virtual {v3, v0, v1, v2, v7}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$7()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_10

    const/16 v0, 0xff

    const/16 v1, 0xff

    const/16 v2, 0xff

    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    :goto_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout;->setClickable(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout;->setLongClickable(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    new-instance v1, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->drawableBackgroundPressed:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/res/Resources;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/res/Resources;

    move-result-object v2

    const-string/jumbo v7, "quick_popup_bg_press"

    const-string/jumbo v8, "drawable"

    const-string/jumbo v9, "com.samsung.android.sdk.spen30"

    invoke-virtual {v2, v7, v8, v9}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :cond_6
    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v7, 0x0

    const v8, 0x10100a7    # android.R.attr.state_pressed

    aput v8, v2, v7

    invoke-virtual {v1, v2, v0}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->drawableBackgroundNormal:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/res/Resources;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/res/Resources;

    move-result-object v2

    const-string/jumbo v7, "quick_popup_bg"

    const-string/jumbo v8, "drawable"

    const-string/jumbo v9, "com.samsung.android.sdk.spen30"

    invoke-virtual {v2, v7, v8, v9}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    :cond_7
    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x1

    const/4 v7, -0x1

    invoke-direct {v2, v0, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    mul-int/lit8 v7, v0, 0x2

    if-eqz p1, :cond_8

    if-ne p1, v7, :cond_11

    :cond_8
    if-nez p1, :cond_a

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    new-instance v8, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;
    invoke-static {v10}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/Context;

    move-result-object v10

    const/4 v11, 0x1

    invoke-direct {v8, v9, v10, v11}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;Landroid/content/Context;Z)V

    invoke-static {v0, v8}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$12(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageLeftItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageLeftItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v0

    sget-object v8, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageLeftItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageLeftItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v0

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->mOnButtonClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_9
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageLeftItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v8

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->id:I

    invoke-virtual {v8, v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setId(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageLeftItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    :cond_a
    if-ne p1, v7, :cond_c

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    new-instance v7, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;
    invoke-static {v9}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/Context;

    move-result-object v9

    const/4 v10, 0x0

    invoke-direct {v7, v8, v9, v10}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;Landroid/content/Context;Z)V

    invoke-static {v0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$13(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageRightItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageRightItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v0

    sget-object v2, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageRightItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageRightItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->mOnButtonClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_b
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageRightItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->id:I

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->setId(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageRightItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    :cond_c
    :goto_6
    new-instance v1, Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    const/16 v0, 0xa

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setId(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    if-nez v0, :cond_13

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->drawableDisableItem:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_7
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x2

    const/4 v7, -0x2

    invoke-direct {v0, v2, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$7()I

    move-result v2

    const/4 v7, 0x1

    if-ne v2, v7, :cond_15

    const/4 v2, 0x1

    const/high16 v7, 0x42040000    # 33.0f

    invoke-static {v2, v7, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    const/4 v2, 0x1

    const/high16 v7, 0x41000000    # 8.0f

    invoke-static {v2, v7, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsNormalMode:Z
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Z

    move-result v2

    if-eqz v2, :cond_14

    const/4 v2, 0x1

    const/high16 v7, 0x42000000    # 32.0f

    invoke-static {v2, v7, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    :goto_8
    const/4 v2, 0x6

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v2, 0xe

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v3, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    new-instance v2, Landroid/widget/TextView;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v0, -0x2

    const/4 v8, -0x2

    invoke-direct {v7, v0, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v0, 0xe

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v0, 0xc

    invoke-virtual {v1}, Landroid/widget/ImageView;->getId()I

    move-result v1

    invoke-virtual {v7, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$7()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1b

    const/4 v0, 0x1

    const/high16 v1, 0x40400000    # 3.0f

    invoke-static {v0, v1, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, v7, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    const/4 v0, 0x1

    const/high16 v1, 0x40400000    # 3.0f

    invoke-static {v0, v1, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, v7, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    const/4 v0, 0x1

    const/high16 v1, 0x41200000    # 10.0f

    invoke-static {v0, v1, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, v7, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    const-string/jumbo v0, "#1e1e1e"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    const/4 v0, 0x1

    const/high16 v1, 0x41700000    # 15.0f

    invoke-virtual {v2, v0, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsNormalMode:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Z

    move-result v0

    if-eqz v0, :cond_17

    const/4 v0, 0x1

    const/high16 v1, 0x40e00000    # 7.0f

    invoke-static {v0, v1, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, v7, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_d
    :goto_9
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    if-nez v0, :cond_e

    const v0, 0x3e99999a    # 0.3f

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setAlpha(F)V

    :cond_e
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    const/4 v0, 0x2

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setMaxLines(I)V

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setGravity(I)V

    invoke-virtual {v3, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v4, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    const v0, 0x3ac90

    add-int/2addr v0, v6

    invoke-virtual {v4, v0}, Landroid/widget/LinearLayout;->setId(I)V

    move-object v0, v4

    goto/16 :goto_0

    :cond_f
    const/4 v0, 0x1

    int-to-float v2, v3

    invoke-static {v0, v2, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v2, v0

    const/4 v0, 0x1

    int-to-float v1, v1

    invoke-static {v0, v1, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    move v1, v2

    goto/16 :goto_4

    :cond_10
    const/16 v0, 0xe9

    const/16 v1, 0xeb

    const/16 v2, 0xec

    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    goto/16 :goto_5

    :cond_11
    new-instance v7, Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/Context;

    move-result-object v0

    invoke-direct {v7, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v7, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    sget-object v0, Landroid/widget/ImageView$ScaleType;->FIT_CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    invoke-virtual {v7, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->mOnButtonClicked:Landroid/view/View$OnClickListener;

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_12
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->id:I

    invoke-virtual {v7, v0}, Landroid/widget/ImageView;->setId(I)V

    invoke-virtual {v3, v7}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_6

    :cond_13
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->drawableNormalItem:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_7

    :cond_14
    const/4 v2, 0x1

    const/high16 v7, 0x42500000    # 52.0f

    invoke-static {v2, v7, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    goto/16 :goto_8

    :cond_15
    const/4 v2, 0x1

    const/high16 v7, 0x41a00000    # 20.0f

    invoke-static {v2, v7, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    const/4 v2, 0x1

    const/high16 v7, 0x41000000    # 8.0f

    invoke-static {v2, v7, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsNormalMode:Z
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Z

    move-result v2

    if-eqz v2, :cond_16

    const/4 v2, 0x1

    const/high16 v7, 0x41a80000    # 21.0f

    invoke-static {v2, v7, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    goto/16 :goto_8

    :cond_16
    const/4 v2, 0x1

    const/high16 v7, 0x42180000    # 38.0f

    invoke-static {v2, v7, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    goto/16 :goto_8

    :cond_17
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->name:Ljava/lang/String;

    if-eqz v8, :cond_d

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v0

    new-array v9, v0, [F

    invoke-virtual {v2}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v10

    invoke-virtual {v0, v8, v1, v10, v9}, Landroid/graphics/Paint;->getTextWidths(Ljava/lang/String;II[F)I

    move-result v10

    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_a
    if-lt v0, v10, :cond_18

    const/4 v0, 0x1

    const/high16 v9, 0x42c40000    # 98.0f

    invoke-static {v0, v9, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    const/4 v9, 0x1

    const/high16 v10, 0x40400000    # 3.0f

    invoke-static {v9, v10, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v9

    float-to-int v9, v9

    mul-int/lit8 v9, v9, 0x2

    sub-int/2addr v0, v9

    int-to-float v0, v0

    cmpg-float v0, v1, v0

    if-gez v0, :cond_19

    const/4 v0, 0x1

    :goto_b
    const/4 v1, 0x1

    const/high16 v9, 0x40e00000    # 7.0f

    invoke-static {v1, v9, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    iput v1, v7, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setSingleLine(Z)V

    const/4 v1, 0x1

    const/high16 v9, 0x40800000    # 4.0f

    invoke-static {v1, v9, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    int-to-float v1, v1

    const/high16 v9, 0x3f800000    # 1.0f

    invoke-virtual {v2, v1, v9}, Landroid/widget/TextView;->setLineSpacing(FF)V

    const/4 v1, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x1

    const/high16 v12, -0x3ef00000    # -9.0f

    invoke-static {v11, v12, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {v2, v1, v9, v10, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    if-eqz v0, :cond_1a

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_9

    :cond_18
    aget v11, v9, v0

    add-float/2addr v1, v11

    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_19
    const/4 v0, 0x0

    goto :goto_b

    :cond_1a
    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_9

    :cond_1b
    const/4 v0, 0x1

    const/high16 v1, 0x40400000    # 3.0f

    invoke-static {v0, v1, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, v7, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    const/4 v0, 0x1

    const/high16 v1, 0x40400000    # 3.0f

    invoke-static {v0, v1, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, v7, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    const/4 v0, 0x1

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, v7, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    const/high16 v0, -0x1000000

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    const/4 v0, 0x1

    const/high16 v1, 0x41400000    # 12.0f

    invoke-virtual {v2, v0, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsNormalMode:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Z

    move-result v0

    if-eqz v0, :cond_1c

    const/4 v0, 0x1

    const/high16 v1, 0x40a00000    # 5.0f

    invoke-static {v0, v1, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, v7, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->name:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_9

    :cond_1c
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->name:Ljava/lang/String;

    if-eqz v8, :cond_d

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v0

    new-array v9, v0, [F

    invoke-virtual {v2}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v10

    invoke-virtual {v0, v8, v1, v10, v9}, Landroid/graphics/Paint;->getTextWidths(Ljava/lang/String;II[F)I

    move-result v10

    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_c
    if-lt v0, v10, :cond_1d

    const/4 v0, 0x1

    const/high16 v9, 0x42900000    # 72.0f

    invoke-static {v0, v9, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    const/4 v9, 0x1

    const/high16 v10, 0x40400000    # 3.0f

    invoke-static {v9, v10, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v9

    float-to-int v9, v9

    mul-int/lit8 v9, v9, 0x2

    sub-int/2addr v0, v9

    int-to-float v0, v0

    cmpg-float v0, v1, v0

    if-gez v0, :cond_1e

    const/4 v0, 0x1

    :goto_d
    const/4 v1, 0x1

    const/high16 v9, 0x40a00000    # 5.0f

    invoke-static {v1, v9, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    iput v1, v7, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setSingleLine(Z)V

    const/4 v1, 0x1

    const/high16 v9, 0x40400000    # 3.0f

    invoke-static {v1, v9, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    int-to-float v1, v1

    const/high16 v9, 0x3f400000    # 0.75f

    invoke-virtual {v2, v1, v9}, Landroid/widget/TextView;->setLineSpacing(FF)V

    const/4 v1, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x1

    const/4 v12, 0x0

    invoke-static {v11, v12, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {v2, v1, v9, v10, v5}, Landroid/widget/TextView;->setPadding(IIII)V

    if-eqz v0, :cond_1f

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_9

    :cond_1d
    aget v11, v9, v0

    add-float/2addr v1, v11

    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    :cond_1e
    const/4 v0, 0x0

    goto :goto_d

    :cond_1f
    invoke-virtual {v2, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_9
.end method
