.class Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;
.super Landroid/os/Handler;
.source "Twttr"


# static fields
.field private static final CURSOR_DELAY:I = 0x258

.field public static final CURSOR_MESSAGE:I = 0x1


# instance fields
.field private mIsStartBlink:Z

.field private final mTextBox:Ljava/lang/ref/WeakReference;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 1

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->mIsStartBlink:Z

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->mTextBox:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->mTextBox:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->mTextBox:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v0, :cond_0

    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    :cond_2
    :goto_1
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto :goto_0

    :pswitch_0
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->invalidate()V

    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->mIsStartBlink:Z

    if-eqz v1, :cond_2

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->removeMessages(I)V

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHighlightPathBogus:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$21(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x0

    :goto_2
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    const-wide/16 v0, 0x258

    invoke-virtual {p0, v2, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public restartBlink()V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->mTextBox:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->mTextBox:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v0, :cond_0

    invoke-static {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->removeMessages(I)V

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->mIsStartBlink:Z

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x258

    invoke-virtual {p0, v2, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method public startBlink()V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->mTextBox:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->mTextBox:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v0, :cond_0

    invoke-static {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->mIsStartBlink:Z

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->removeMessages(I)V

    const-wide/16 v0, 0x258

    invoke-virtual {p0, v2, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method public stopBlink()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->mTextBox:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->mTextBox:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v0, :cond_0

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->mIsStartBlink:Z

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->removeMessages(I)V

    goto :goto_0
.end method
