.class Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;
.super Landroid/os/Handler;
.source "Twttr"


# static fields
.field private static synthetic $SWITCH_TABLE$com$samsung$android$sdk$pen$engine$SpenSmartScaleGestureDetector$State:[I

.field private static final DBG:Z

.field private static final HANDLER_TIME:I


# instance fields
.field private final m1CMPixel:F

.field private mBottomScrollRegion:Landroid/graphics/Rect;

.field private mCenterX:F

.field private mCenterY:F

.field private final mDPI:F

.field private mDeltaX:F

.field private mDeltaY:F

.field private mDiffX:F

.field private mDiffY:F

.field private mDistanceX:F

.field private mDistanceY:F

.field private mDownTime:J

.field private mDownX:F

.field private mDownY:F

.field private mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

.field private mEdgePaint:Landroid/graphics/Paint;

.field private mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

.field private mEffectFrame:I

.field private mFactor:F

.field private mFlickDirection:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

.field private mFrameStartX:I

.field private mFrameStartY:I

.field private mHorizontalEnterTime:J

.field private mHorizontalResponseTime:I

.field private mHorizontalVelocity:I

.field private mLeftScrollRegion:Landroid/graphics/Rect;

.field private mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

.field private mMaxDeltaX:F

.field private mMaxDeltaY:F

.field private mOrigRatio:F

.field private mRatio:F

.field private mRightScrollRegion:Landroid/graphics/Rect;

.field private mRtoCvsItstFrmHeight:I

.field private mRtoCvsItstFrmWidth:I

.field private mScrollX:F

.field private mScrollY:F

.field private mSmartScaleRegion:Landroid/graphics/Rect;

.field private mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

.field private mToolAndActionMap:Landroid/util/SparseIntArray;

.field private mTopScrollRegion:Landroid/graphics/Rect;

.field private mUseHorizontalScroll:Z

.field private mUseSmartScale:Z

.field private mUseVerticalScroll:Z

.field private mVerticalEnterTime:J

.field private mVerticalResponseTime:I

.field private mVerticalVelocity:I

.field private mZoomOutResponseTime:I

.field private mZoomRatio:F


# direct methods
.method static synthetic $SWITCH_TABLE$com$samsung$android$sdk$pen$engine$SpenSmartScaleGestureDetector$State()[I
    .locals 3

    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->$SWITCH_TABLE$com$samsung$android$sdk$pen$engine$SpenSmartScaleGestureDetector$State:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->values()[Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->EDGE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_7

    :goto_1
    :try_start_1
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->FLING_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_6

    :goto_2
    :try_start_2
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->IDLE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_5

    :goto_3
    :try_start_3
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->READY_FOR_ZOOMOUT_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_4

    :goto_4
    :try_start_4
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->SCROLL_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_3

    :goto_5
    :try_start_5
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ZOOMED_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_2

    :goto_6
    :try_start_6
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ZOOMIN_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_1

    :goto_7
    :try_start_7
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ZOOMOUT_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_0

    :goto_8
    sput-object v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->$SWITCH_TABLE$com$samsung$android$sdk$pen$engine$SpenSmartScaleGestureDetector$State:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_8

    :catch_1
    move-exception v1

    goto :goto_7

    :catch_2
    move-exception v1

    goto :goto_6

    :catch_3
    move-exception v1

    goto :goto_5

    :catch_4
    move-exception v1

    goto :goto_4

    :catch_5
    move-exception v1

    goto :goto_3

    :catch_6
    move-exception v1

    goto :goto_2

    :catch_7
    move-exception v1

    goto :goto_1
.end method

.method public constructor <init>(Landroid/content/Context;F)V
    .locals 7

    const/high16 v6, 0x3f800000    # 1.0f

    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->IDLE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaY:F

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mOrigRatio:F

    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseSmartScale:Z

    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseHorizontalScroll:Z

    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseVerticalScroll:Z

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mSmartScaleRegion:Landroid/graphics/Rect;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mLeftScrollRegion:Landroid/graphics/Rect;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRightScrollRegion:Landroid/graphics/Rect;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mTopScrollRegion:Landroid/graphics/Rect;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mBottomScrollRegion:Landroid/graphics/Rect;

    const/high16 v0, 0x3e000000    # 0.125f

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFactor:F

    const/high16 v0, 0x3fc00000    # 1.5f

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mZoomRatio:F

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHorizontalResponseTime:I

    iput-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHorizontalEnterTime:J

    const/16 v0, 0x14

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHorizontalVelocity:I

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mVerticalResponseTime:I

    iput-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mVerticalEnterTime:J

    const/16 v0, 0x14

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mVerticalVelocity:I

    iput-wide v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDownTime:J

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDownX:F

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDownY:F

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFlickDirection:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    new-instance v0, Landroid/util/SparseIntArray;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Landroid/util/SparseIntArray;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mToolAndActionMap:Landroid/util/SparseIntArray;

    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDPI:F

    const v0, 0x3ec9932d

    mul-float/2addr v0, p2

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->m1CMPixel:F

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    invoke-direct {v0, p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgePaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgePaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgePaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    return-void
.end method

.method private Fling()V
    .locals 6

    const/high16 v5, 0x3f800000    # 1.0f

    const v4, 0x3e19999a    # 0.15f

    const/high16 v1, 0x42c80000    # 100.0f

    const/high16 v0, -0x3d380000    # -100.0f

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceX:F

    mul-float v3, v2, v4

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceY:F

    mul-float/2addr v2, v4

    cmpg-float v4, v3, v0

    if-gez v4, :cond_1

    move v3, v0

    :cond_0
    :goto_0
    cmpg-float v4, v2, v0

    if-gez v4, :cond_2

    :goto_1
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceX:F

    sub-float/2addr v1, v3

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceX:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceY:F

    sub-float/2addr v1, v0

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceY:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    sub-float/2addr v1, v3

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    sub-float/2addr v2, v0

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpg-float v3, v3, v5

    if-gez v3, :cond_3

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpg-float v0, v0, v5

    if-gez v0, :cond_3

    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->IDLE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    float-to-int v1, v1

    int-to-float v1, v1

    float-to-int v2, v2

    int-to-float v2, v2

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onChangePan(FF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onUpdateScreenFrameBuffer()V

    :goto_2
    return-void

    :cond_1
    cmpl-float v4, v3, v1

    if-lez v4, :cond_0

    move v3, v1

    goto :goto_0

    :cond_2
    cmpl-float v0, v2, v1

    if-lez v0, :cond_4

    move v0, v1

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onChangePan(FF)V

    const/4 v0, 0x0

    const-wide/16 v1, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method private ReadyForZoomout()V
    .locals 2

    const-string/jumbo v0, "SPen_Library"

    const-string/jumbo v1, "[SMART SCALE] READY FOR ZOOM OUT()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ZOOMOUT_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->ZoomOut()V

    return-void
.end method

.method private Scroll()V
    .locals 8

    const/4 v3, 0x1

    const/4 v7, 0x0

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseVerticalScroll:Z

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mTopScrollRegion:Landroid/graphics/Rect;

    if-eqz v0, :cond_b

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    cmpl-float v0, v0, v7

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mTopScrollRegion:Landroid/graphics/Rect;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollX:F

    float-to-int v2, v2

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollY:F

    float-to-int v4, v4

    invoke-virtual {v0, v2, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_b

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mVerticalVelocity:I

    sub-int v0, v1, v0

    :goto_0
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseVerticalScroll:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mBottomScrollRegion:Landroid/graphics/Rect;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mBottomScrollRegion:Landroid/graphics/Rect;

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollX:F

    float-to-int v4, v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollY:F

    float-to-int v5, v5

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mVerticalVelocity:I

    add-int/2addr v0, v2

    :cond_0
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseHorizontalScroll:Z

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mLeftScrollRegion:Landroid/graphics/Rect;

    if-eqz v2, :cond_a

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    cmpl-float v2, v2, v7

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mLeftScrollRegion:Landroid/graphics/Rect;

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollX:F

    float-to-int v4, v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollY:F

    float-to-int v5, v5

    invoke-virtual {v2, v4, v5}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_a

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHorizontalVelocity:I

    sub-int v2, v1, v2

    :goto_1
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseHorizontalScroll:Z

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRightScrollRegion:Landroid/graphics/Rect;

    if-eqz v4, :cond_1

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    cmpg-float v4, v4, v5

    if-gez v4, :cond_1

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRightScrollRegion:Landroid/graphics/Rect;

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollX:F

    float-to-int v5, v5

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollY:F

    float-to-int v6, v6

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_1

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHorizontalVelocity:I

    add-int/2addr v2, v4

    :cond_1
    if-eqz v2, :cond_9

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    int-to-float v2, v2

    add-float/2addr v2, v4

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    cmpg-float v2, v2, v7

    if-gez v2, :cond_3

    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    move v2, v1

    :goto_2
    move v4, v2

    move v2, v3

    :goto_3
    if-eqz v0, :cond_7

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    int-to-float v0, v0

    add-float/2addr v0, v2

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    cmpg-float v0, v0, v7

    if-gez v0, :cond_4

    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    move v0, v1

    :goto_4
    if-eqz v3, :cond_2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    invoke-interface {v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onChangePan(FF)V

    :cond_2
    if-eqz v0, :cond_5

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->sendEmptyMessageDelayed(IJ)Z

    :goto_5
    return-void

    :cond_3
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    cmpl-float v2, v2, v4

    if-lez v2, :cond_8

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    move v2, v1

    goto :goto_2

    :cond_4
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaY:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_6

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaY:F

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    move v0, v1

    goto :goto_4

    :cond_5
    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ZOOMED_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    goto :goto_5

    :cond_6
    move v0, v3

    goto :goto_4

    :cond_7
    move v3, v2

    move v0, v4

    goto :goto_4

    :cond_8
    move v2, v3

    goto :goto_2

    :cond_9
    move v2, v1

    move v4, v3

    goto :goto_3

    :cond_a
    move v2, v1

    goto/16 :goto_1

    :cond_b
    move v0, v1

    goto/16 :goto_0
.end method

.method private ZoomIn()V
    .locals 4

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mZoomRatio:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_1

    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ZOOMED_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCenterX:F

    float-to-int v1, v1

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCenterY:F

    float-to-int v2, v2

    int-to-float v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mZoomRatio:F

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onChangeScale(FFF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onUpdateScreenFrameBuffer()V

    const-string/jumbo v0, "SPen_Library"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "[SMART SCALE] ZOOM IN(), RATIO : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mZoomRatio:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", STATE : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ZOOMIN_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFactor:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCenterX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCenterY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onChangeScale(FFF)V

    :cond_2
    const/4 v0, 0x0

    const-wide/16 v1, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->sendEmptyMessageDelayed(IJ)Z

    const-string/jumbo v0, "SPen_Library"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "[SMART SCALE] ZOOM IN(), RATIO : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", STATE : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private ZoomOut()V
    .locals 4

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFactor:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mOrigRatio:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mOrigRatio:F

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->IDLE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->IDLE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCenterX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCenterY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onChangeScale(FFF)V

    const/4 v0, 0x0

    const-wide/16 v1, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->sendEmptyMessageDelayed(IJ)Z

    :goto_0
    const-string/jumbo v0, "SPen_Library"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "[SMART SCALE] ZOOM OUT(), RATIO : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", STATE : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCenterX:F

    float-to-int v1, v1

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCenterY:F

    float-to-int v2, v2

    int-to-float v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onChangeScale(FFF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onUpdateScreenFrameBuffer()V

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mToolAndActionMap:Landroid/util/SparseIntArray;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mToolAndActionMap:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mToolAndActionMap:Landroid/util/SparseIntArray;

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->close()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->close()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    :cond_2
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgePaint:Landroid/graphics/Paint;

    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->LEFT:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    if-ne v0, v1, :cond_4

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFrameStartX:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFrameStartY:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRtoCvsItstFrmHeight:I

    add-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    const/high16 v1, 0x43870000    # 270.0f

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->rotate(F)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v1, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    :cond_0
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->TOP:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    if-ne v0, v1, :cond_6

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFrameStartX:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFrameStartY:I

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->rotate(F)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v1, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    :cond_2
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    :cond_3
    :goto_1
    return-void

    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->RIGHT:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    if-ne v0, v1, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFrameStartX:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRtoCvsItstFrmWidth:I

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFrameStartY:I

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    const/high16 v1, 0x42b40000    # 90.0f

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->rotate(F)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v1, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    :cond_5
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->BOTTOM:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    if-ne v0, v1, :cond_3

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFrameStartX:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRtoCvsItstFrmWidth:I

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFrameStartY:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRtoCvsItstFrmHeight:I

    add-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    const/high16 v1, 0x43340000    # 180.0f

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->rotate(F)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v1

    if-nez v1, :cond_7

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v1, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    :cond_7
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto :goto_1
.end method

.method public enableHorizontalSmartScroll(ZLandroid/graphics/Rect;Landroid/graphics/Rect;II)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseHorizontalScroll:Z

    iput-object p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mLeftScrollRegion:Landroid/graphics/Rect;

    iput-object p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRightScrollRegion:Landroid/graphics/Rect;

    iput p4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHorizontalResponseTime:I

    iput p5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHorizontalVelocity:I

    return-void
.end method

.method public enableSmartScale(ZLandroid/graphics/Rect;IIF)V
    .locals 4

    if-nez p1, :cond_1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseSmartScale:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string/jumbo v0, "SPen_Library"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "[SMART SCALE] enableSmartScale use : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseSmartScale:Z

    iput-object p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mSmartScaleRegion:Landroid/graphics/Rect;

    iput p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEffectFrame:I

    iput p4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mZoomOutResponseTime:I

    iput p5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mZoomRatio:F

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ZOOMIN_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ZOOMED_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ZOOMOUT_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-ne v0, v1, :cond_0

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCenterX:F

    float-to-int v1, v1

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCenterY:F

    float-to-int v2, v2

    int-to-float v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mOrigRatio:F

    invoke-interface {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onChangeScale(FFF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onUpdateScreenFrameBuffer()V

    goto :goto_0
.end method

.method public enableVerticalSmartScroll(ZLandroid/graphics/Rect;Landroid/graphics/Rect;II)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseVerticalScroll:Z

    iput-object p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mTopScrollRegion:Landroid/graphics/Rect;

    iput-object p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mBottomScrollRegion:Landroid/graphics/Rect;

    iput p4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mVerticalResponseTime:I

    iput p5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mVerticalVelocity:I

    return-void
.end method

.method public getCenterX()F
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCenterX:F

    return v0
.end method

.method public getCenterY()F
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCenterY:F

    return v0
.end method

.method public getState()Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    return-object v0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->$SWITCH_TABLE$com$samsung$android$sdk$pen$engine$SpenSmartScaleGestureDetector$State()[I

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->ZoomIn()V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->Scroll()V

    goto :goto_0

    :pswitch_3
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->ReadyForZoomout()V

    goto :goto_0

    :pswitch_4
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->ZoomOut()V

    goto :goto_0

    :pswitch_5
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->Fling()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_5
    .end packed-switch
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)V
    .locals 6

    const/high16 v5, 0x40800000    # 4.0f

    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFlickDirection:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    if-ne v0, v1, :cond_6

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    mul-float/2addr v0, v5

    div-float v0, p3, v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceX:F

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    mul-float/2addr v0, v5

    div-float v0, p4, v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceY:F

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceX:F

    sub-float/2addr v0, v1

    cmpg-float v0, v0, v4

    if-gez v0, :cond_4

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceX:F

    :cond_0
    :goto_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceY:F

    sub-float/2addr v0, v1

    cmpg-float v0, v0, v4

    if-gez v0, :cond_5

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceY:F

    :cond_1
    :goto_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceX:F

    float-to-double v0, v0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceY:F

    float-to-double v0, v0

    cmpl-double v0, v0, v2

    if-eqz v0, :cond_3

    :cond_2
    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->FLING_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->Fling()V

    :cond_3
    :goto_2
    return-void

    :cond_4
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceX:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceX:F

    goto :goto_0

    :cond_5
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceY:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaY:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaY:F

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDistanceY:F

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFlickDirection:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;->LEFT:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    if-ne v0, v1, :cond_7

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onFlick(I)Z

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFlickDirection:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;->RIGHT:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onFlick(I)Z

    goto :goto_2
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)V
    .locals 6

    const/16 v5, 0xa

    const/4 v3, 0x2

    const/4 v4, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    if-ne v1, v3, :cond_0

    const/16 v1, 0x9

    if-ne v0, v1, :cond_9

    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseSmartScale:Z

    if-eqz v1, :cond_9

    const-string/jumbo v0, "SPen_Library"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "[SMART SCALE] ON HOVER ENTER. STATE : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->READY_FOR_ZOOMOUT_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-ne v0, v1, :cond_2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mZoomOutResponseTime:I

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->removeMessages(I)V

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->IDLE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ZOOMOUT_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->FLING_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-eq v0, v1, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->EDGE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-ne v0, v1, :cond_0

    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getButtonState()I

    move-result v0

    if-ne v0, v3, :cond_4

    const-string/jumbo v0, "SPen_Library"

    const-string/jumbo v1, "[SMART SCALE] ON HOVER ENTER. BUTTON_SECONDARY"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCenterX:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mCenterY:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->IDLE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-eq v0, v1, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->FLING_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-eq v0, v1, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->EDGE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-ne v0, v1, :cond_6

    :cond_5
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mOrigRatio:F

    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mSmartScaleRegion:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string/jumbo v0, "SPen_Library"

    const-string/jumbo v1, "[SMART SCALE] ON HOVER ENTER. SMART REGION CONTAINS"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mZoomRatio:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_7

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mZoomRatio:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEffectFrame:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFactor:F

    :goto_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->ZoomIn()V

    goto/16 :goto_0

    :cond_7
    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFactor:F

    goto :goto_1

    :cond_8
    const-string/jumbo v0, "SPen_Library"

    const-string/jumbo v1, "[SMART SCALE] ON HOVER ENTER. SMART REGION NOT CONTAINS!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_9
    if-eq v0, v5, :cond_16

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->SCROLL_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-eq v1, v2, :cond_a

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ZOOMED_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-eq v1, v2, :cond_a

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->IDLE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-ne v1, v2, :cond_16

    :cond_a
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollX:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollY:F

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseVerticalScroll:Z

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mTopScrollRegion:Landroid/graphics/Rect;

    if-eqz v0, :cond_b

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    cmpl-float v0, v0, v4

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mTopScrollRegion:Landroid/graphics/Rect;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollX:F

    float-to-int v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollY:F

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-nez v0, :cond_d

    :cond_b
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mBottomScrollRegion:Landroid/graphics/Rect;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mBottomScrollRegion:Landroid/graphics/Rect;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollX:F

    float-to-int v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollY:F

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-nez v0, :cond_d

    :cond_c
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mVerticalEnterTime:J

    :cond_d
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseHorizontalScroll:Z

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mLeftScrollRegion:Landroid/graphics/Rect;

    if-eqz v0, :cond_e

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    cmpl-float v0, v0, v4

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mLeftScrollRegion:Landroid/graphics/Rect;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollX:F

    float-to-int v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollY:F

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-nez v0, :cond_10

    :cond_e
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRightScrollRegion:Landroid/graphics/Rect;

    if-eqz v0, :cond_f

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_f

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRightScrollRegion:Landroid/graphics/Rect;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollX:F

    float-to-int v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollY:F

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-nez v0, :cond_10

    :cond_f
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHorizontalEnterTime:J

    :cond_10
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ZOOMED_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-eq v0, v1, :cond_11

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->IDLE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-ne v0, v1, :cond_0

    :cond_11
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseVerticalScroll:Z

    if-eqz v0, :cond_12

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mVerticalEnterTime:J

    sub-long/2addr v0, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mVerticalResponseTime:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_12

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mTopScrollRegion:Landroid/graphics/Rect;

    if-eqz v0, :cond_12

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    cmpl-float v0, v0, v4

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mTopScrollRegion:Landroid/graphics/Rect;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollX:F

    float-to-int v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollY:F

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-nez v0, :cond_15

    :cond_12
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseVerticalScroll:Z

    if-eqz v0, :cond_13

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mVerticalEnterTime:J

    sub-long/2addr v0, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mVerticalResponseTime:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_13

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mBottomScrollRegion:Landroid/graphics/Rect;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mBottomScrollRegion:Landroid/graphics/Rect;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollX:F

    float-to-int v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollY:F

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-nez v0, :cond_15

    :cond_13
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseHorizontalScroll:Z

    if-eqz v0, :cond_14

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHorizontalEnterTime:J

    sub-long/2addr v0, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHorizontalResponseTime:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_14

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mLeftScrollRegion:Landroid/graphics/Rect;

    if-eqz v0, :cond_14

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    cmpl-float v0, v0, v4

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mLeftScrollRegion:Landroid/graphics/Rect;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollX:F

    float-to-int v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollY:F

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-nez v0, :cond_15

    :cond_14
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseHorizontalScroll:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHorizontalEnterTime:J

    sub-long/2addr v0, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mHorizontalResponseTime:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRightScrollRegion:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRightScrollRegion:Landroid/graphics/Rect;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollX:F

    float-to-int v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mScrollY:F

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_15
    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->SCROLL_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->Scroll()V

    goto/16 :goto_0

    :cond_16
    if-ne v0, v5, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getButtonState()I

    move-result v0

    if-eq v0, v3, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseSmartScale:Z

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->IDLE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-eq v0, v1, :cond_18

    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->READY_FOR_ZOOMOUT_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mZoomOutResponseTime:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mZoomOutResponseTime:I

    int-to-long v1, v1

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->sendEmptyMessageDelayed(IJ)Z

    :cond_17
    :goto_2
    const-string/jumbo v0, "SPen_Library"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "[SMART SCALE] ON HOVER EXIT. STATE : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_18
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseVerticalScroll:Z

    if-nez v0, :cond_19

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mUseHorizontalScroll:Z

    if-eqz v0, :cond_17

    :cond_19
    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->IDLE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    float-to-int v1, v1

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    float-to-int v2, v2

    int-to-float v2, v2

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onChangePan(FF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onUpdateScreenFrameBuffer()V

    goto :goto_2
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)V
    .locals 14

    const/4 v13, 0x1

    const/high16 v12, 0x43480000    # 200.0f

    const/high16 v11, 0x40000000    # 2.0f

    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    if-nez v0, :cond_2

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->FLING_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-eq v5, v6, :cond_0

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    sget-object v6, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->READY_FOR_ZOOMOUT_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-ne v5, v6, :cond_1

    :cond_0
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    float-to-int v6, v6

    int-to-float v6, v6

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    float-to-int v7, v7

    int-to-float v7, v7

    invoke-interface {v5, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onChangePan(FF)V

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    invoke-interface {v5}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;->onUpdateScreenFrameBuffer()V

    :cond_1
    const/4 v5, 0x2

    if-ne v2, v5, :cond_6

    sget-object v5, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->ZOOMED_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    :cond_2
    :goto_0
    if-nez v0, :cond_7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    iput-wide v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDownTime:J

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDownX:F

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDownY:F

    sget-object v5, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFlickDirection:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    :cond_3
    :goto_1
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mToolAndActionMap:Landroid/util/SparseIntArray;

    invoke-virtual {v5, v2}, Landroid/util/SparseIntArray;->get(I)I

    move-result v2

    if-ne v2, v13, :cond_5

    if-eqz v0, :cond_5

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1b

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->enable:Z

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    sget-object v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    if-ne v0, v2, :cond_e

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_d

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    sget-object v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->LEFT:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    iput-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iput v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->startX:F

    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->enable:Z

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    sget-object v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    if-ne v0, v2, :cond_16

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_15

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->TOP:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iput v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->startY:F

    :cond_5
    :goto_3
    return-void

    :cond_6
    sget-object v5, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->IDLE_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mState:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    goto :goto_0

    :cond_7
    if-ne v0, v13, :cond_3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    iget-wide v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDownTime:J

    sub-long/2addr v5, v7

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDownX:F

    sub-float v7, v3, v7

    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDiffX:F

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDownY:F

    sub-float v7, v4, v7

    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDiffY:F

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDiffX:F

    mul-float/2addr v7, v12

    long-to-float v8, v5

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDPI:F

    mul-float/2addr v8, v9

    div-float/2addr v7, v8

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDiffY:F

    mul-float/2addr v8, v12

    long-to-float v5, v5

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDPI:F

    mul-float/2addr v5, v6

    div-float v5, v8, v5

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDiffX:F

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->m1CMPixel:F

    div-float/2addr v8, v11

    cmpg-float v6, v6, v8

    if-gez v6, :cond_8

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDiffY:F

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->m1CMPixel:F

    div-float/2addr v8, v11

    cmpg-float v6, v6, v8

    if-gez v6, :cond_8

    sget-object v5, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFlickDirection:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    goto/16 :goto_1

    :cond_8
    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v6

    cmpg-float v6, v6, v10

    if-gez v6, :cond_9

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v6

    cmpg-float v6, v6, v10

    if-gez v6, :cond_9

    sget-object v5, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFlickDirection:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    goto/16 :goto_1

    :cond_9
    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v6

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v8

    cmpl-float v6, v6, v8

    if-lez v6, :cond_b

    cmpl-float v5, v7, v1

    if-lez v5, :cond_a

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    float-to-double v5, v5

    const-wide/16 v8, 0x0

    cmpg-double v5, v5, v8

    if-gtz v5, :cond_a

    sget-object v5, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;->LEFT:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFlickDirection:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    goto/16 :goto_1

    :cond_a
    cmpg-float v5, v7, v1

    if-gez v5, :cond_3

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    cmpl-float v5, v5, v6

    if-ltz v5, :cond_3

    sget-object v5, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;->RIGHT:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFlickDirection:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    goto/16 :goto_1

    :cond_b
    cmpl-float v6, v5, v1

    if-lez v6, :cond_c

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    cmpg-float v6, v6, v1

    if-gtz v6, :cond_c

    sget-object v5, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;->TOP:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFlickDirection:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    goto/16 :goto_1

    :cond_c
    cmpg-float v5, v5, v1

    if-gez v5, :cond_3

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaY:F

    cmpl-float v5, v5, v6

    if-ltz v5, :cond_3

    sget-object v5, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;->BOTTOM:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFlickDirection:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Direction;

    goto/16 :goto_1

    :cond_d
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    cmpl-float v0, v0, v2

    if-ltz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    sget-object v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->RIGHT:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    iput-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iput v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->startX:F

    goto/16 :goto_2

    :cond_e
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    sget-object v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->LEFT:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    if-ne v0, v2, :cond_11

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    cmpl-float v0, v0, v10

    if-lez v0, :cond_f

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    sget-object v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    iput-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->onRelease()V

    goto/16 :goto_2

    :cond_f
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->startX:F

    sub-float v0, v3, v0

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRtoCvsItstFrmWidth:I

    int-to-float v2, v2

    div-float/2addr v0, v2

    cmpg-float v2, v0, v1

    if-gez v2, :cond_10

    move v0, v1

    :cond_10
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v2, v0}, Landroid/widget/EdgeEffect;->onPull(F)V

    goto/16 :goto_2

    :cond_11
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    sub-float/2addr v2, v10

    cmpg-float v0, v0, v2

    if-gez v0, :cond_12

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    sget-object v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    iput-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->onRelease()V

    goto/16 :goto_2

    :cond_12
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->startX:F

    sub-float/2addr v0, v3

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRtoCvsItstFrmWidth:I

    int-to-float v2, v2

    div-float/2addr v0, v2

    cmpg-float v2, v0, v1

    if-gez v2, :cond_13

    move v0, v1

    :cond_13
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v2, v0}, Landroid/widget/EdgeEffect;->onPull(F)V

    goto/16 :goto_2

    :cond_14
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    sget-object v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    iput-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    goto/16 :goto_2

    :cond_15
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaY:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->BOTTOM:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iput v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->startY:F

    goto/16 :goto_3

    :cond_16
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    sget-object v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->TOP:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    if-ne v0, v2, :cond_18

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    cmpl-float v0, v0, v10

    if-lez v0, :cond_17

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->onRelease()V

    goto/16 :goto_3

    :cond_17
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->startY:F

    sub-float v0, v4, v0

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRtoCvsItstFrmHeight:I

    int-to-float v2, v2

    div-float/2addr v0, v2

    cmpg-float v2, v0, v1

    if-gez v2, :cond_1d

    :goto_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v0, v1}, Landroid/widget/EdgeEffect;->onPull(F)V

    goto/16 :goto_3

    :cond_18
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaY:F

    sub-float/2addr v2, v10

    cmpg-float v0, v0, v2

    if-gez v0, :cond_19

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->onRelease()V

    goto/16 :goto_3

    :cond_19
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->startY:F

    sub-float/2addr v0, v4

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRtoCvsItstFrmHeight:I

    int-to-float v2, v2

    div-float/2addr v0, v2

    cmpg-float v2, v0, v1

    if-gez v2, :cond_1c

    :goto_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v0, v1}, Landroid/widget/EdgeEffect;->onPull(F)V

    goto/16 :goto_3

    :cond_1a
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    goto/16 :goto_3

    :cond_1b
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionLR;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;->NONE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->direction:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$DirectionTB;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->onRelease()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->onRelease()V

    goto/16 :goto_3

    :cond_1c
    move v1, v0

    goto :goto_5

    :cond_1d
    move v1, v0

    goto :goto_4
.end method

.method public onZoom(FFF)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaX:F

    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mDeltaY:F

    iput p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRatio:F

    return-void
.end method

.method public setDrawInformation(IIII)V
    .locals 3

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRtoCvsItstFrmWidth:I

    if-ne v0, p1, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRtoCvsItstFrmHeight:I

    if-ne v0, p2, :cond_0

    :goto_0
    return-void

    :cond_0
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRtoCvsItstFrmWidth:I

    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRtoCvsItstFrmHeight:I

    iput p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFrameStartX:I

    iput p4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mFrameStartY:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->effect:Landroid/widget/EdgeEffect;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRtoCvsItstFrmHeight:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRtoCvsItstFrmWidth:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/EdgeEffect;->setSize(II)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->effect:Landroid/widget/EdgeEffect;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRtoCvsItstFrmWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mRtoCvsItstFrmHeight:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/EdgeEffect;->setSize(II)V

    goto :goto_0
.end method

.method public setLimitHeight(FF)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaY:F

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaX:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->enable:Z

    :goto_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mMaxDeltaY:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->enable:Z

    :goto_1
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeLR:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;

    iput-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectLR;->enable:Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mEdgeTB:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;

    iput-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$EdgeEffectTB;->enable:Z

    goto :goto_1
.end method

.method public setListener(Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;

    return-void
.end method

.method public setToolTypeAction(II)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->mToolAndActionMap:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseIntArray;->put(II)V

    return-void
.end method
