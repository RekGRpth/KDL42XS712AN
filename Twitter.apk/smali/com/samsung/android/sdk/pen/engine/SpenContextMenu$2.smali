.class Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return v2

    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    invoke-static {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mParent:Landroid/view/View;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mParent:Landroid/view/View;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mParent:Landroid/view/View;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resetIndex()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->setFocusMenuItem(I)Landroid/view/View;

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mDelay:I
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$15(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)I

    move-result v0

    const/16 v1, 0x3e8

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mDelay:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$15(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)I

    move-result v1

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->updateTimer(I)V
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$16(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
