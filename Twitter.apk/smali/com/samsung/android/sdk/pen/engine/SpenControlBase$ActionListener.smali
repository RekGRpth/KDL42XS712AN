.class public interface abstract Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;
.super Ljava/lang/Object;
.source "Twttr"


# virtual methods
.method public abstract onClosed(Ljava/util/ArrayList;)V
.end method

.method public abstract onMenuSelected(Ljava/util/ArrayList;I)V
.end method

.method public abstract onObjectChanged(Ljava/util/ArrayList;)V
.end method

.method public abstract onRectChanged(Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
.end method

.method public abstract onRequestBackground()Landroid/graphics/Bitmap;
.end method

.method public abstract onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V
.end method

.method public abstract onRequestScroll(FF)V
.end method

.method public abstract onRotationChanged(FLcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
.end method

.method public abstract onVisibleUpdated(Ljava/util/ArrayList;Z)V
.end method
