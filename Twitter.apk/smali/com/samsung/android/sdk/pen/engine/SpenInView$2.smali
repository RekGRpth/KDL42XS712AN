.class Lcom/samsung/android/sdk/pen/engine/SpenInView$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCanceled(ILcom/samsung/android/sdk/pen/document/SpenObjectContainer;)V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$50(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V

    and-int/lit8 v0, p1, 0x1

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->undoToTag()[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateUndo([Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->clearHistoryTag()V

    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateStrokeFrameListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$52(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateStrokeFrameListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$52(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;->onCanceled(ILcom/samsung/android/sdk/pen/document/SpenObjectContainer;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->setVisibility(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->update()V

    goto :goto_0
.end method

.method public onCompleted(ILcom/samsung/android/sdk/pen/document/SpenObjectContainer;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$50(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V

    invoke-virtual {p2, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->setVisibility(Z)V

    const-string/jumbo v0, "STROKE_FRAME"

    const/4 v1, 0x2

    invoke-virtual {p2, v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->setExtraDataInt(Ljava/lang/String;I)V

    if-nez p1, :cond_2

    invoke-virtual {p2, v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setVisibility(Z)V

    invoke-virtual {p2, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setVisibility(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-static {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$51(Lcom/samsung/android/sdk/pen/engine/SpenInView;I)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->clearHistoryTag()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->update()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateStrokeFrameListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$52(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateStrokeFrameListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$52(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;->onCompleted(ILcom/samsung/android/sdk/pen/document/SpenObjectContainer;)V

    :cond_1
    return-void

    :cond_2
    if-ne p1, v2, :cond_0

    invoke-virtual {p2, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setVisibility(Z)V

    invoke-virtual {p2, v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setVisibility(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-static {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$51(Lcom/samsung/android/sdk/pen/engine/SpenInView;I)V

    goto :goto_0
.end method
