.class Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
.super Landroid/view/View;
.source "Twttr"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation


# static fields
.field private static final BITMAP_CUE_BOTTOM:I = 0x1

.field private static final BITMAP_CUE_MAX:I = 0x2

.field private static final BITMAP_CUE_TOP:I = 0x0

.field private static final BITMAP_HANDLE_CENTER_BOTTOM:I = 0x5

.field private static final BITMAP_HANDLE_CENTER_TOP:I = 0x4

.field private static final BITMAP_HANDLE_LEFT_BOTTOM:I = 0x1

.field private static final BITMAP_HANDLE_LEFT_TOP:I = 0x0

.field private static final BITMAP_HANDLE_MAX:I = 0x6

.field private static final BITMAP_HANDLE_RIGHT_BOTTOM:I = 0x3

.field private static final BITMAP_HANDLE_RIGHT_TOP:I = 0x2

.field private static final BORDER_STATIC_LINE_WIDTH:I = 0x4

.field private static final CR_CHAR:C = '\r'

.field private static final CUE_BOTTOM:I = 0x1

.field private static final CUE_MAX:I = 0x2

.field private static final CUE_TOP:I = 0x0

.field private static final CURSOR_WIDTH:I = 0x2

.field private static final DBG:Z = false

.field private static final DEFAULT_CONTEXTMENU_DELAY:I = 0x1388

.field private static final DEFAULT_CUE_FILE_NAME:[Ljava/lang/String;

.field private static final DEFAULT_HANDLE_FILE_NAME:[Ljava/lang/String;

.field private static final DEFAULT_PADDING:I = 0x5

.field private static final DEFAULT_SPAN:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

.field private static final HANDLE_END:I = 0x2

.field private static final HANDLE_MAX:I = 0x3

.field private static final HANDLE_MIDDLE:I = 0x1

.field private static final HANDLE_START:I = 0x0

.field private static final ITALIC_ANGLE_IN_DEGREE:F = 15.0f

.field private static final LF_CHAR:C = '\n'

.field private static final PRESS_AREA_COLOR_DARK:I = -0xe69570

.field private static final PRESS_AREA_COLOR_LIGHT:I = -0x7f86350e

.field private static final SCROLL_BAR_COLOR:I = -0x7fa0a0a1

.field private static final SCROLL_BAR_WIDTH:I = 0x4

.field private static final SIN_15_DEGREE:F

.field private static final SPACE_CHAR:C = ' '

.field private static final TAB_CHAR:C = '\t'

.field private static final TEXT_INPUT_LIMITED:I = 0x1388

.field private static mContextMenuItemList:Ljava/util/ArrayList;


# instance fields
.field private CONTROL_BOTTOM_MARGIN:I

.field private CONTROL_LEFT_MARGIN:I

.field private CONTROL_RIGHT_MARGIN:I

.field private CONTROL_TOP_MARGIN:I

.field private final MAX_OBJECT_HEIGHT:F

.field private final MAX_OBJECT_WIDTH:F

.field private mAltKeyPressed:Z

.field private mBitmap:Landroid/graphics/Bitmap;

.field private final mBlink:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

.field private mChangeWatcher:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;

.field private mClipboardMessage:Landroid/widget/Toast;

.field private final mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

.field private mContextMenuVisible:Z

.field private mCueBitmap:[Landroid/graphics/Bitmap;

.field private mCueButton:[Landroid/widget/ImageButton;

.field private mCurrentOrientation:I

.field private mCursorHandleDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;

.field private mCursorHandleVisible:Z

.field private mCursorVisible:Z

.field private mDeltaY:F

.field private mEditable:Landroid/text/Editable;

.field private mEndIndex:[I

.field protected mFirstDraw:Z

.field protected mGestureDetector:Landroid/view/GestureDetector;

.field private mHandleBitmap:[Landroid/graphics/Bitmap;

.field private mHandleButton:[Landroid/widget/ImageButton;

.field mHandleListener:[Landroid/view/View$OnTouchListener;

.field private mHandlePressed:Z

.field private mHasWindowFocus:Z

.field private mHighLightPaint:Landroid/graphics/Paint;

.field private mHighlightPathBogus:Z

.field private mInputConnection:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;

.field private final mInputMethodChangedReceiver:Landroid/content/BroadcastReceiver;

.field private mIsCommitText:Z

.field private mIsComposingText:Z

.field private mIsDeletedText:Z

.field private mIsEditableClear:Z

.field private mIsFirstCharLF:Z

.field private mIsTyping:Z

.field private mIsViewMode:Z

.field private mKeyListener:Landroid/text/method/KeyListener;

.field private mKeypadFilter:Landroid/content/IntentFilter;

.field private mLineCount:I

.field private mLinePosition:[Landroid/graphics/PointF;

.field private mMetricsRect:Landroid/graphics/Rect;

.field private mNativeTextView:I

.field private mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

.field private mPrevLineIndex:I

.field private mPrevRelativeWidth:I

.field private mRequestObjectChange:Ljava/lang/Runnable;

.field private mScaleMatrix:Landroid/graphics/Matrix;

.field private mScrollBarDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;

.field private mScrollBarPaint:Landroid/graphics/Paint;

.field private mScrollBarVisible:Z

.field private mSelectPaint:Landroid/graphics/Paint;

.field private mShiftKeyPressed:Z

.field private mShowCursor:J

.field private mStartIndex:[I

.field private mSurroundingTextLength:I

.field private mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

.field private mTempMatrix:Landroid/graphics/Matrix;

.field private mTempRectF:Landroid/graphics/RectF;

.field private mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

.field private mTextDirection:[I

.field protected mTextEraserEnable:Z

.field private mTextItalic:[Z

.field private mTextLimit:I

.field private mTextRect:[Landroid/graphics/RectF;

.field private mTextSize:[F

.field private mTextView:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

.field private mTouchEnable:Z

.field private final mTyping:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;

.field private mUpdateHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-wide/high16 v0, 0x402e000000000000L    # 15.0

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    double-to-float v0, v0

    sput v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->SIN_15_DEGREE:F

    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->DEFAULT_HANDLE_FILE_NAME:[Ljava/lang/String;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->DEFAULT_CUE_FILE_NAME:[Ljava/lang/String;

    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->DEFAULT_SPAN:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenuItemList:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/ViewGroup;II)V
    .locals 9

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_LEFT_MARGIN:I

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_RIGHT_MARGIN:I

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_TOP_MARGIN:I

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_BOTTOM_MARGIN:I

    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->DEFAULT_HANDLE_FILE_NAME:[Ljava/lang/String;

    new-instance v1, Ljava/lang/String;

    const-string/jumbo v2, "text_select_handle_left_2_browser"

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v4

    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->DEFAULT_HANDLE_FILE_NAME:[Ljava/lang/String;

    new-instance v1, Ljava/lang/String;

    const-string/jumbo v2, "text_select_handle_left_browser"

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v6

    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->DEFAULT_HANDLE_FILE_NAME:[Ljava/lang/String;

    new-instance v1, Ljava/lang/String;

    const-string/jumbo v2, "text_select_handle_right_2_browser"

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v7

    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->DEFAULT_HANDLE_FILE_NAME:[Ljava/lang/String;

    new-instance v1, Ljava/lang/String;

    const-string/jumbo v2, "text_select_handle_right_browser"

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v8

    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->DEFAULT_HANDLE_FILE_NAME:[Ljava/lang/String;

    const/4 v1, 0x4

    new-instance v2, Ljava/lang/String;

    const-string/jumbo v3, "text_select_handle_reverse"

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->DEFAULT_HANDLE_FILE_NAME:[Ljava/lang/String;

    const/4 v1, 0x5

    new-instance v2, Ljava/lang/String;

    const-string/jumbo v3, "text_select_handle_middle"

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v2, v0, v1

    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->DEFAULT_CUE_FILE_NAME:[Ljava/lang/String;

    new-instance v1, Ljava/lang/String;

    const-string/jumbo v2, "text_cue_top_press"

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v4

    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->DEFAULT_CUE_FILE_NAME:[Ljava/lang/String;

    new-instance v1, Ljava/lang/String;

    const-string/jumbo v2, "text_cue_bottom_press"

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v1, v0, v6

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevRelativeWidth:I

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCurrentOrientation:I

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHighLightPaint:Landroid/graphics/Paint;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSelectPaint:Landroid/graphics/Paint;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarPaint:Landroid/graphics/Paint;

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHighlightPathBogus:Z

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeypadFilter:Landroid/content/IntentFilter;

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorVisible:Z

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleVisible:Z

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarVisible:Z

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTouchEnable:Z

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandlePressed:Z

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsViewMode:Z

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHasWindowFocus:Z

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextEraserEnable:Z

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mFirstDraw:Z

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenuVisible:Z

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mChangeWatcher:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeyListener:Landroid/text/method/KeyListener;

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsTyping:Z

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsCommitText:Z

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsDeletedText:Z

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsComposingText:Z

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsEditableClear:Z

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mAltKeyPressed:Z

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mShiftKeyPressed:Z

    const/16 v0, 0x1388

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextLimit:I

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSurroundingTextLength:I

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevLineIndex:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDeltaY:F

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLinePosition:[Landroid/graphics/PointF;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextSize:[F

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextDirection:[I

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextItalic:[Z

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsFirstCharLF:Z

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMetricsRect:Landroid/graphics/Rect;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mUpdateHandler:Landroid/os/Handler;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mRequestObjectChange:Ljava/lang/Runnable;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextView:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$1;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mInputMethodChangedReceiver:Landroid/content/BroadcastReceiver;

    new-array v0, v8, [Landroid/view/View$OnTouchListener;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleListener:[Landroid/view/View$OnTouchListener;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleListener:[Landroid/view/View$OnTouchListener;

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$2;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    aput-object v1, v0, v4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleListener:[Landroid/view/View$OnTouchListener;

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$3;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    aput-object v1, v0, v6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleListener:[Landroid/view/View$OnTouchListener;

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$4;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$4;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    aput-object v1, v0, v7

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_init()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSelectPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSelectPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSelectPaint:Landroid/graphics/Paint;

    const v1, -0x7f86350e

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSelectPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHighLightPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHighLightPaint:Landroid/graphics/Paint;

    const v1, -0xff6901

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarPaint:Landroid/graphics/Paint;

    const v1, -0x7fa0a0a1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBlink:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-direct {v0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;

    invoke-direct {v0, p0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mChangeWatcher:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    if-nez v0, :cond_0

    invoke-static {}, Landroid/text/Editable$Factory;->getInstance()Landroid/text/Editable$Factory;

    move-result-object v0

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/text/Editable$Factory;->newEditable(Ljava/lang/CharSequence;)Landroid/text/Editable;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v0, v4}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    new-array v0, v6, [Landroid/text/InputFilter;

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v2

    const/16 v3, 0x1388

    invoke-direct {v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;-><init>(Landroid/content/Context;I)V

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v1, v0}, Landroid/text/Editable;->setFilters([Landroid/text/InputFilter;)V

    :cond_0
    sget-object v0, Landroid/text/method/TextKeyListener$Capitalize;->SENTENCES:Landroid/text/method/TextKeyListener$Capitalize;

    invoke-static {v4, v0}, Landroid/text/method/TextKeyListener;->getInstance(ZLandroid/text/method/TextKeyListener$Capitalize;)Landroid/text/method/TextKeyListener;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeyListener:Landroid/text/method/KeyListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTyping:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mUpdateHandler:Landroid/os/Handler;

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$5;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mRequestObjectChange:Ljava/lang/Runnable;

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_construct(ILandroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    :cond_1
    int-to-float v0, p3

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_WIDTH:F

    int-to-float v0, p4

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_HEIGHT:F

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    if-le v1, v2, :cond_2

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    :goto_0
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, v4, v4, v0, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mMetricsRect:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCurrentOrientation:I

    iput-object p0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextView:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeypadFilter:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeypadFilter:Landroid/content/IntentFilter;

    const-string/jumbo v1, "ResponseAxT9Info"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mInputMethodChangedReceiver:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeypadFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-direct {p0, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->initTextBox(Landroid/view/ViewGroup;)V

    return-void

    :cond_2
    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    goto :goto_0
.end method

.method private absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V
    .locals 2

    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v0, v1

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->left:F

    iget v0, p2, Landroid/graphics/RectF;->right:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v0, v1

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->right:F

    iget v0, p2, Landroid/graphics/RectF;->top:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v0, v1

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v0, v1

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextView:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;FF)[F
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getAbsolutePoint(FF)[F

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$10(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateSelection()V

    return-void
.end method

.method static synthetic access$11(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;I)F
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->checkForVerticalScroll(I)F

    move-result v0

    return v0
.end method

.method static synthetic access$12(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onDrawHandle()V

    return-void
.end method

.method static synthetic access$13(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showCursorHandle()V

    return-void
.end method

.method static synthetic access$14(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    return-object v0
.end method

.method static synthetic access$15(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/text/method/KeyListener;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeyListener:Landroid/text/method/KeyListener;

    return-object v0
.end method

.method static synthetic access$16(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsDeletedText:Z

    return-void
.end method

.method static synthetic access$17(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsCommitText:Z

    return-void
.end method

.method static synthetic access$18(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsComposingText:Z

    return-void
.end method

.method static synthetic access$19(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)[Landroid/widget/ImageButton;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/text/Editable;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    return-object v0
.end method

.method static synthetic access$20(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorVisible:Z

    return-void
.end method

.method static synthetic access$21(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHighlightPathBogus:Z

    return v0
.end method

.method static synthetic access$22(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateSettingInfo()V

    return-void
.end method

.method static synthetic access$23(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsTyping:Z

    return-void
.end method

.method static synthetic access$24(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleVisible:Z

    return-void
.end method

.method static synthetic access$25(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    return-object v0
.end method

.method static synthetic access$26(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarVisible:Z

    return-void
.end method

.method static synthetic access$27(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsViewMode:Z

    return v0
.end method

.method static synthetic access$28(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsTyping:Z

    return v0
.end method

.method static synthetic access$29(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    return v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;I)Landroid/graphics/Rect;
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorRect(I)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$30(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/graphics/Matrix;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method static synthetic access$31(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/graphics/Matrix;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method static synthetic access$32(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    return v0
.end method

.method static synthetic access$33(I)F
    .locals 1

    invoke-static {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getPan(I)F

    move-result v0

    return v0
.end method

.method static synthetic access$34(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;IF)I
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getOffsetForHorizontal(IF)I

    move-result v0

    return v0
.end method

.method static synthetic access$35(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)[Landroid/graphics/RectF;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    return-object v0
.end method

.method static synthetic access$36(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)[I
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    return-object v0
.end method

.method static synthetic access$37(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;ILcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;)Z
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->findWord(ILcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$38(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getTextLength()I

    move-result v0

    return v0
.end method

.method static synthetic access$39(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBlink:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

    return-object v0
.end method

.method static synthetic access$40(I)I
    .locals 1

    invoke-static {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getHeight(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$41(IF)V
    .locals 0

    invoke-static {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_setPan(IF)V

    return-void
.end method

.method static synthetic access$42(I)Z
    .locals 1

    invoke-static {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_update(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$43(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$44(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showScrollBar()V

    return-void
.end method

.method static synthetic access$45(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onCueTopButtonDown(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$46(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onCueBottomButtonDown(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$47(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideCursorHandle()V

    return-void
.end method

.method static synthetic access$48(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTyping:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;

    return-object v0
.end method

.method static synthetic access$49(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->initMeasureInfo()V

    return-void
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHighlightPathBogus:Z

    return-void
.end method

.method static synthetic access$50(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsEditableClear:Z

    return v0
.end method

.method static synthetic access$51(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsCommitText:Z

    return v0
.end method

.method static synthetic access$52(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsComposingText:Z

    return v0
.end method

.method static synthetic access$53(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsDeletedText:Z

    return v0
.end method

.method static synthetic access$54(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorVisible:Z

    return v0
.end method

.method static synthetic access$55(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSurroundingTextLength:I

    return v0
.end method

.method static synthetic access$56(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;I)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSurroundingTextLength:I

    return-void
.end method

.method static synthetic access$57(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;III)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextDirection(III)V

    return-void
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandlePressed:Z

    return-void
.end method

.method static synthetic access$7(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;F)I
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineForVertical(F)I

    move-result v0

    return v0
.end method

.method static synthetic access$8(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;IF)I
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v0

    return v0
.end method

.method static synthetic access$9(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    return-object v0
.end method

.method private adjustCursorSize(Landroid/graphics/Rect;II)V
    .locals 7

    const/4 v4, 0x0

    const/4 v2, 0x0

    const-string/jumbo v0, "adjustCursorSize() is called."

    invoke-static {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getLineCount(I)I

    move-result v5

    move v3, v2

    move v0, v2

    move v1, v2

    :goto_1
    if-lt v3, v5, :cond_3

    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3, v1, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->findSpans(II)Ljava/util/ArrayList;

    move-result-object v5

    if-eqz v5, :cond_b

    move v1, v2

    move v3, v4

    :goto_2
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_5

    :goto_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0, p2, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->findSpans(II)Ljava/util/ArrayList;

    move-result-object v6

    if-eqz v6, :cond_a

    move v1, v2

    move v5, v4

    :goto_4
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_7

    :goto_5
    cmpl-float v0, v3, v4

    if-eqz v0, :cond_2

    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v0

    sub-float v1, v3, v5

    const/high16 v4, 0x40800000    # 4.0f

    div-float/2addr v1, v4

    sub-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    :cond_2
    iget v0, p1, Landroid/graphics/Rect;->bottom:I

    const v1, 0x3fa66666    # 1.3f

    mul-float/2addr v1, v5

    float-to-int v1, v1

    sub-int/2addr v0, v1

    iput v0, p1, Landroid/graphics/Rect;->top:I

    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "adjustCursorSize maxFontSize = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getLineStartIndex(II)I

    move-result v1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getLineEndIndex(II)I

    move-result v0

    if-gt v1, p2, :cond_4

    add-int/lit8 v6, v0, 0x1

    if-ge v6, p2, :cond_1

    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_5
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    if-eqz v0, :cond_6

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    cmpg-float v6, v3, v0

    if-gtz v6, :cond_6

    move v3, v0

    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_7
    if-eqz p2, :cond_8

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    if-eq v0, p2, :cond_9

    :cond_8
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    if-eqz v0, :cond_9

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    iget v5, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    :cond_9
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_4

    :cond_a
    move v5, v4

    goto :goto_5

    :cond_b
    move v3, v4

    goto/16 :goto_3
.end method

.method private adjustTextBox()V
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v1

    double-to-int v1, v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_measure(II)Z

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v1

    if-nez v1, :cond_1

    const-string/jumbo v0, "coordinateInfo is null."

    invoke-static {v6, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v0

    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    invoke-direct {p0, v2, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getHeight(I)I

    move-result v3

    if-nez v3, :cond_4

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getDefaultHeight()I

    move-result v0

    :goto_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getAutoFitOption()I

    move-result v4

    const/4 v5, 0x2

    if-eq v4, v5, :cond_2

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getAutoFitOption()I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_b

    :cond_2
    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v4

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v4, v4

    if-ge v0, v4, :cond_7

    if-lez v0, :cond_3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getMinHeight()F

    move-result v3

    cmpg-float v4, v3, v7

    if-gtz v4, :cond_5

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v1

    int-to-float v0, v0

    invoke-direct {p0, v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->resize(FF)V

    :cond_3
    :goto_2
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->checkCursorPosition()V

    goto :goto_0

    :cond_4
    int-to-float v0, v3

    iget v4, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v0, v4

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v0, v4

    goto :goto_1

    :cond_5
    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v1, v3

    int-to-float v3, v0

    cmpg-float v3, v1, v3

    if-gez v3, :cond_6

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v1

    int-to-float v0, v0

    invoke-direct {p0, v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->resize(FF)V

    goto :goto_2

    :cond_6
    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v0

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->resize(FF)V

    goto :goto_2

    :cond_7
    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v4

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v4, v4

    if-le v0, v4, :cond_3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v4

    iget v4, v4, Landroid/graphics/RectF;->top:F

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    add-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_HEIGHT:F

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_BOTTOM_MARGIN:I

    int-to-float v5, v5

    sub-float/2addr v4, v5

    cmpg-float v3, v3, v4

    if-gez v3, :cond_a

    const-string/jumbo v3, "adjustTextBox() Set smaller than the original size."

    invoke-static {v6, v3}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getMaxHeight()F

    move-result v3

    cmpg-float v4, v3, v7

    if-gtz v4, :cond_8

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v1

    int-to-float v0, v0

    invoke-direct {p0, v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->resize(FF)V

    goto :goto_2

    :cond_8
    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v1, v3

    int-to-float v3, v0

    cmpl-float v3, v1, v3

    if-lez v3, :cond_9

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v1

    int-to-float v0, v0

    invoke-direct {p0, v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->resize(FF)V

    goto :goto_2

    :cond_9
    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v0

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->resize(FF)V

    goto :goto_2

    :cond_a
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_HEIGHT:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_BOTTOM_MARGIN:I

    int-to-float v3, v3

    sub-float/2addr v0, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v3

    iget v3, v3, Landroid/graphics/RectF;->top:F

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    sub-float/2addr v0, v3

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v1

    int-to-float v0, v0

    invoke-direct {p0, v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->resize(FF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getAutoFitOption()I

    move-result v0

    and-int/lit8 v0, v0, -0x3

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setAutoFitOption(I)V

    goto/16 :goto_2

    :cond_b
    const-string/jumbo v0, "adjustTextBox() height == localLayoutParams.height or Gravity is GRAVITY_CLIP_VERTICAL."

    invoke-static {v6, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    goto/16 :goto_2
.end method

.method private applyScaledRect(Landroid/graphics/Rect;)V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v0, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v0, p1}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    return-void
.end method

.method private checkCursorPosition()V
    .locals 10

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    const-string/jumbo v1, "mObjectText is null."

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsViewMode:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v3

    if-nez v3, :cond_2

    const/4 v0, 0x0

    const-string/jumbo v1, "coordinateInfo is null."

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v4

    if-nez v4, :cond_3

    const/4 v0, 0x0

    const-string/jumbo v1, "objectRect is null"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v2

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorRect(I)Landroid/graphics/Rect;

    move-result-object v0

    if-nez v0, :cond_4

    const/4 v0, 0x0

    const-string/jumbo v1, "cursorRect is null"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    goto :goto_0

    :cond_4
    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5, v0}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getHeight(I)I

    move-result v0

    int-to-float v6, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getPan(I)F

    move-result v1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getTopMargin()F

    move-result v0

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getBottomMargin()F

    move-result v7

    add-float/2addr v0, v7

    const/4 v7, 0x0

    cmpl-float v7, v0, v7

    if-lez v7, :cond_5

    const/4 v0, 0x0

    :cond_5
    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v7

    cmpl-float v7, v6, v7

    if-lez v7, :cond_a

    iget v7, v5, Landroid/graphics/RectF;->top:F

    cmpg-float v7, v7, v1

    if-gez v7, :cond_9

    iget v1, v5, Landroid/graphics/RectF;->top:F

    :cond_6
    :goto_1
    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v7

    add-float/2addr v7, v0

    add-float/2addr v7, v1

    cmpg-float v7, v6, v7

    if-gez v7, :cond_14

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v7

    add-float/2addr v0, v7

    add-float/2addr v0, v1

    sub-float/2addr v0, v6

    sub-float v0, v1, v0

    :goto_2
    const/4 v1, 0x0

    cmpg-float v1, v0, v1

    if-gez v1, :cond_7

    const/4 v0, 0x0

    :cond_7
    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCurrnetLineIndex(I)I

    move-result v7

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevLineIndex:I

    if-eq v1, v7, :cond_8

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_setPan(IF)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setVereticalPan(F)V

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    float-to-double v8, v6

    cmpg-double v0, v0, v8

    if-gez v0, :cond_8

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showScrollBar()V

    :cond_8
    iget v0, v4, Landroid/graphics/RectF;->left:F

    iget v1, v4, Landroid/graphics/RectF;->top:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getPan(I)F

    move-result v2

    sub-float/2addr v1, v2

    invoke-virtual {v5, v0, v1}, Landroid/graphics/RectF;->offset(FF)V

    iget v0, v5, Landroid/graphics/RectF;->left:F

    iget v1, v5, Landroid/graphics/RectF;->top:F

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getRelativePoint(FF)[F

    move-result-object v0

    if-nez v0, :cond_b

    const/4 v0, 0x0

    const-string/jumbo v1, "pts is null"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    goto/16 :goto_0

    :cond_9
    iget v7, v5, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v8

    add-float/2addr v8, v1

    cmpl-float v7, v7, v8

    if-lez v7, :cond_6

    iget v1, v5, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v7

    sub-float/2addr v1, v7

    goto :goto_1

    :cond_a
    const/4 v0, 0x0

    goto :goto_2

    :cond_b
    const/4 v1, 0x1

    aget v0, v0, v1

    iput v0, v5, Landroid/graphics/RectF;->top:F

    iget v0, v5, Landroid/graphics/RectF;->left:F

    iget v1, v5, Landroid/graphics/RectF;->bottom:F

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getRelativePoint(FF)[F

    move-result-object v0

    if-nez v0, :cond_c

    const/4 v0, 0x0

    const-string/jumbo v1, "pts is null"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    goto/16 :goto_0

    :cond_c
    const/4 v1, 0x1

    aget v0, v0, v1

    iput v0, v5, Landroid/graphics/RectF;->bottom:F

    new-instance v6, Landroid/graphics/RectF;

    invoke-direct {v6}, Landroid/graphics/RectF;-><init>()V

    invoke-direct {p0, v6, v5, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "checkCursorPosition() cursorRect left = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, v5, Landroid/graphics/RectF;->left:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", top = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v5, Landroid/graphics/RectF;->top:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", right = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v5, Landroid/graphics/RectF;->right:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", bottom = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v5, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", height = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "checkCursorPosition() relativeCursorRect left = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, v6, Landroid/graphics/RectF;->left:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", top = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v6, Landroid/graphics/RectF;->top:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", right = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v6, Landroid/graphics/RectF;->right:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", bottom = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v6, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", height = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v8

    if-eqz v8, :cond_0

    iget v8, v6, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v9

    int-to-float v9, v9

    cmpl-float v8, v8, v9

    if-lez v8, :cond_11

    iget v1, v6, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v8

    int-to-float v8, v8

    sub-float/2addr v1, v8

    iget v8, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v1, v8

    const/high16 v8, 0x41100000    # 9.0f

    add-float/2addr v1, v8

    :cond_d
    :goto_3
    iget v8, v6, Landroid/graphics/RectF;->right:F

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v9

    int-to-float v9, v9

    cmpl-float v8, v8, v9

    if-lez v8, :cond_12

    iget v2, v6, Landroid/graphics/RectF;->right:F

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v0

    int-to-float v0, v0

    sub-float v0, v2, v0

    iget v2, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v0, v2

    iget v2, v5, Landroid/graphics/RectF;->right:F

    iget v3, v4, Landroid/graphics/RectF;->right:F

    const/high16 v4, 0x40a00000    # 5.0f

    sub-float/2addr v3, v4

    cmpg-float v2, v2, v3

    if-gez v2, :cond_e

    const/high16 v2, 0x40a00000    # 5.0f

    add-float/2addr v0, v2

    :cond_e
    :goto_4
    const/4 v2, 0x0

    cmpl-float v2, v0, v2

    if-nez v2, :cond_f

    const/4 v2, 0x0

    cmpl-float v2, v1, v2

    if-eqz v2, :cond_10

    :cond_f
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevLineIndex:I

    if-eq v2, v7, :cond_10

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onRequestScroll(FF)V

    :cond_10
    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevLineIndex:I

    goto/16 :goto_0

    :cond_11
    iget v8, v6, Landroid/graphics/RectF;->top:F

    const/4 v9, 0x0

    cmpg-float v8, v8, v9

    if-gez v8, :cond_d

    iget v1, v6, Landroid/graphics/RectF;->top:F

    iget v8, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v1, v8

    iget v8, v5, Landroid/graphics/RectF;->top:F

    const/high16 v9, 0x40a00000    # 5.0f

    cmpl-float v8, v8, v9

    if-lez v8, :cond_d

    const/high16 v8, 0x41100000    # 9.0f

    sub-float/2addr v1, v8

    goto :goto_3

    :cond_12
    iget v0, v6, Landroid/graphics/RectF;->left:F

    const/4 v4, 0x0

    cmpg-float v0, v0, v4

    if-gez v0, :cond_13

    iget v0, v6, Landroid/graphics/RectF;->left:F

    iget v2, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v0, v2

    iget v2, v5, Landroid/graphics/RectF;->left:F

    const/high16 v3, 0x420c0000    # 35.0f

    cmpl-float v2, v2, v3

    if-lez v2, :cond_e

    const/high16 v2, 0x420c0000    # 35.0f

    sub-float/2addr v0, v2

    goto :goto_4

    :cond_13
    move v0, v2

    goto :goto_4

    :cond_14
    move v0, v1

    goto/16 :goto_2
.end method

.method private checkForHorizontalScroll()Z
    .locals 5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v1, 0x1

    const/4 v0, 0x0

    const-string/jumbo v2, "checkForHorizontalScroll() is called."

    invoke-static {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    if-nez v2, :cond_1

    const-string/jumbo v1, "checkForHorizontalScroll() mNativeTextView is invalid."

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandlePressed:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getAutoFitOption()I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_3

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getAutoFitOption()I

    move-result v2

    if-eq v2, v1, :cond_3

    :cond_2
    const-string/jumbo v1, "checkForHorizontalScroll() Don\'t need to check scroll."

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v2

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v3

    if-nez v3, :cond_4

    const-string/jumbo v1, "coordinateInfo is null."

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    goto :goto_0

    :cond_4
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    invoke-direct {p0, v4, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMaximumWidth()F

    move-result v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevRelativeWidth:I

    int-to-float v3, v3

    cmpl-float v3, v3, v2

    if-eqz v3, :cond_5

    float-to-int v3, v2

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mPrevRelativeWidth:I

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v3

    invoke-direct {p0, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->resize(FF)V

    iget v3, v4, Landroid/graphics/RectF;->left:F

    add-float/2addr v2, v3

    iput v2, v4, Landroid/graphics/RectF;->right:F

    :cond_5
    const-string/jumbo v2, "checkForHorizontalScroll() end"

    invoke-static {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    move v0, v1

    goto :goto_0
.end method

.method private checkForVerticalScroll(I)F
    .locals 12

    const/high16 v11, 0x41100000    # 9.0f

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    if-nez v0, :cond_1

    const-string/jumbo v0, "localLayoutParams is null"

    invoke-static {v9, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v3

    if-nez v3, :cond_2

    const-string/jumbo v0, "coordinateInfo is null."

    invoke-static {v9, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v4

    if-nez v4, :cond_3

    const-string/jumbo v0, "objectRect is null"

    invoke-static {v9, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorRect(I)Landroid/graphics/Rect;

    move-result-object v0

    if-nez v0, :cond_4

    const-string/jumbo v0, "cursorRect is null"

    invoke-static {v9, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    goto :goto_0

    :cond_4
    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5, v0}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getPan(I)F

    move-result v2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getHeight(I)I

    move-result v0

    int-to-float v6, v0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getTopMargin()F

    move-result v0

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getBottomMargin()F

    move-result v7

    add-float/2addr v0, v7

    cmpl-float v7, v0, v1

    if-lez v7, :cond_5

    move v0, v1

    :cond_5
    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v7

    cmpl-float v7, v6, v7

    if-lez v7, :cond_9

    iget v7, v5, Landroid/graphics/RectF;->top:F

    cmpg-float v7, v7, v2

    if-gez v7, :cond_8

    iget v2, v5, Landroid/graphics/RectF;->top:F

    :cond_6
    :goto_1
    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v7

    add-float/2addr v7, v0

    add-float/2addr v7, v2

    cmpg-float v7, v6, v7

    if-gez v7, :cond_e

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v7

    add-float/2addr v0, v7

    add-float/2addr v0, v2

    sub-float/2addr v0, v6

    sub-float v0, v2, v0

    :goto_2
    cmpg-float v2, v0, v1

    if-gez v2, :cond_7

    move v0, v1

    :cond_7
    iget v2, v4, Landroid/graphics/RectF;->left:F

    iget v4, v4, Landroid/graphics/RectF;->top:F

    sub-float v0, v4, v0

    invoke-virtual {v5, v2, v0}, Landroid/graphics/RectF;->offset(FF)V

    iget v0, v5, Landroid/graphics/RectF;->left:F

    iget v2, v5, Landroid/graphics/RectF;->top:F

    invoke-direct {p0, v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getRelativePoint(FF)[F

    move-result-object v0

    if-nez v0, :cond_a

    const-string/jumbo v0, "pts is null"

    invoke-static {v9, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    goto/16 :goto_0

    :cond_8
    iget v7, v5, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v8

    add-float/2addr v8, v2

    cmpl-float v7, v7, v8

    if-lez v7, :cond_6

    iget v2, v5, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v7

    sub-float/2addr v2, v7

    goto :goto_1

    :cond_9
    move v0, v1

    goto :goto_2

    :cond_a
    aget v0, v0, v10

    iput v0, v5, Landroid/graphics/RectF;->top:F

    iget v0, v5, Landroid/graphics/RectF;->left:F

    iget v2, v5, Landroid/graphics/RectF;->bottom:F

    invoke-direct {p0, v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getRelativePoint(FF)[F

    move-result-object v0

    if-nez v0, :cond_b

    const-string/jumbo v0, "pts is null"

    invoke-static {v9, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    goto/16 :goto_0

    :cond_b
    aget v0, v0, v10

    iput v0, v5, Landroid/graphics/RectF;->bottom:F

    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    invoke-direct {p0, v2, v5, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v4

    if-eqz v4, :cond_0

    iget v4, v2, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v6

    int-to-float v6, v6

    cmpl-float v4, v4, v6

    if-lez v4, :cond_d

    iget v1, v2, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v0

    int-to-float v0, v0

    sub-float v0, v1, v0

    iget v1, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v0, v1

    add-float v1, v0, v11

    :cond_c
    :goto_3
    invoke-virtual {p0, v10}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "checkForVerticalScroll index = "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, ", delta = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v9, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    goto/16 :goto_0

    :cond_d
    iget v0, v2, Landroid/graphics/RectF;->top:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_c

    iget v0, v2, Landroid/graphics/RectF;->top:F

    iget v1, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float v1, v0, v1

    iget v0, v5, Landroid/graphics/RectF;->top:F

    const/high16 v2, 0x40a00000    # 5.0f

    cmpl-float v0, v0, v2

    if-lez v0, :cond_c

    sub-float/2addr v1, v11

    goto :goto_3

    :cond_e
    move v0, v2

    goto/16 :goto_2
.end method

.method private checkObjectBounds()V
    .locals 5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v0

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v1

    if-nez v1, :cond_2

    const/4 v0, 0x0

    const-string/jumbo v1, "coordinateInfo is null."

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    goto :goto_0

    :cond_2
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    invoke-direct {p0, v2, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_LEFT_MARGIN:I

    add-int/lit8 v2, v2, 0x0

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_3

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_LEFT_MARGIN:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_LEFT_MARGIN:I

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->left:F

    :cond_3
    iget v1, v0, Landroid/graphics/RectF;->top:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_TOP_MARGIN:I

    add-int/lit8 v2, v2, 0x0

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_4

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_TOP_MARGIN:I

    int-to-float v2, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_TOP_MARGIN:I

    int-to-float v1, v1

    iput v1, v0, Landroid/graphics/RectF;->top:F

    :cond_4
    iget v1, v0, Landroid/graphics/RectF;->right:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_WIDTH:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_RIGHT_MARGIN:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    cmpl-float v1, v1, v2

    if-lez v1, :cond_7

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getAutoFitOption()I

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getAutoFitOption()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_6

    :cond_5
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_WIDTH:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_RIGHT_MARGIN:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v2

    sub-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->left:F

    :cond_6
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_WIDTH:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_RIGHT_MARGIN:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getDefaultWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v2

    int-to-float v3, v1

    cmpg-float v2, v2, v3

    if-gez v2, :cond_7

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_WIDTH:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_RIGHT_MARGIN:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    int-to-float v1, v1

    sub-float v1, v2, v1

    iput v1, v0, Landroid/graphics/RectF;->left:F

    :cond_7
    iget v1, v0, Landroid/graphics/RectF;->bottom:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_HEIGHT:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_BOTTOM_MARGIN:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    cmpl-float v1, v1, v2

    if-lez v1, :cond_a

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getAutoFitOption()I

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getAutoFitOption()I

    move-result v1

    if-ne v1, v4, :cond_9

    :cond_8
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_HEIGHT:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_BOTTOM_MARGIN:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v2

    sub-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->top:F

    :cond_9
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_HEIGHT:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_BOTTOM_MARGIN:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getDefaultHeight()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v2

    int-to-float v3, v1

    cmpg-float v2, v2, v3

    if-gez v2, :cond_a

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_HEIGHT:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_BOTTOM_MARGIN:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    int-to-float v1, v1

    sub-float v1, v2, v1

    iput v1, v0, Landroid/graphics/RectF;->top:F

    :cond_a
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v1, v0, v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setRect(Landroid/graphics/RectF;Z)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onObjectChanged()V

    goto/16 :goto_0
.end method

.method private drawSelectRect(Landroid/graphics/Canvas;IILandroid/graphics/RectF;Landroid/graphics/Rect;)V
    .locals 9

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getPan(I)F

    move-result v3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v4

    if-nez v4, :cond_4

    const-string/jumbo v0, "drawSelectRect() objectRect is invalid."

    invoke-static {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v0, v0, p2

    iget v0, v0, Landroid/graphics/RectF;->top:F

    sub-float/2addr v0, v3

    float-to-int v0, v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v2, v2, p2

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v2, v3

    float-to-int v2, v2

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v5, v5, p2

    iget v5, v5, Landroid/graphics/RectF;->left:F

    float-to-int v5, v5

    if-gez v0, :cond_3

    move v0, v1

    :cond_3
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v6, v6, p2

    iget v6, v6, Landroid/graphics/RectF;->right:F

    float-to-int v6, v6

    int-to-float v7, v2

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v8

    cmpl-float v7, v7, v8

    if-lez v7, :cond_5

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v2

    :goto_1
    float-to-int v2, v2

    invoke-virtual {p5, v5, v0, v6, v2}, Landroid/graphics/Rect;->set(IIII)V

    invoke-direct {p0, p5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->applyScaledRect(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSelectPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p5, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    add-int/lit8 p2, p2, 0x1

    :cond_4
    if-lt p2, p3, :cond_2

    goto :goto_0

    :cond_5
    int-to-float v2, v2

    goto :goto_1
.end method

.method private drawSelectedLine(Landroid/graphics/Canvas;IILandroid/graphics/RectF;Landroid/graphics/Rect;)V
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v0, v0, p2

    invoke-virtual {p4, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    add-int/lit8 v0, p2, 0x1

    move v2, v0

    :goto_1
    if-le v2, p3, :cond_1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getPan(I)F

    move-result v2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v3

    if-nez v3, :cond_6

    const-string/jumbo v0, "drawSelectedLine() objectRect is invalid."

    invoke-static {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    goto :goto_0

    :cond_1
    iget v0, p4, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v3, v3, v2

    iget v3, v3, Landroid/graphics/RectF;->left:F

    cmpg-float v0, v0, v3

    if-gez v0, :cond_2

    iget v0, p4, Landroid/graphics/RectF;->left:F

    :goto_2
    iput v0, p4, Landroid/graphics/RectF;->left:F

    iget v0, p4, Landroid/graphics/RectF;->top:F

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v3, v3, v2

    iget v3, v3, Landroid/graphics/RectF;->top:F

    cmpg-float v0, v0, v3

    if-gez v0, :cond_3

    iget v0, p4, Landroid/graphics/RectF;->top:F

    :goto_3
    iput v0, p4, Landroid/graphics/RectF;->top:F

    iget v0, p4, Landroid/graphics/RectF;->right:F

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v3, v3, v2

    iget v3, v3, Landroid/graphics/RectF;->right:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_4

    iget v0, p4, Landroid/graphics/RectF;->right:F

    :goto_4
    iput v0, p4, Landroid/graphics/RectF;->right:F

    iget v0, p4, Landroid/graphics/RectF;->bottom:F

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v3, v3, v2

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    cmpl-float v0, v0, v3

    if-lez v0, :cond_5

    iget v0, p4, Landroid/graphics/RectF;->bottom:F

    :goto_5
    iput v0, p4, Landroid/graphics/RectF;->bottom:F

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v0, v0, v2

    iget v0, v0, Landroid/graphics/RectF;->left:F

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v0, v0, v2

    iget v0, v0, Landroid/graphics/RectF;->top:F

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v0, v0, v2

    iget v0, v0, Landroid/graphics/RectF;->right:F

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v0, v0, v2

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    goto :goto_5

    :cond_6
    iget v0, p4, Landroid/graphics/RectF;->top:F

    sub-float/2addr v0, v2

    float-to-int v0, v0

    iget v4, p4, Landroid/graphics/RectF;->bottom:F

    sub-float v2, v4, v2

    float-to-int v2, v2

    iget v4, p4, Landroid/graphics/RectF;->left:F

    float-to-int v4, v4

    if-gez v0, :cond_7

    move v0, v1

    :cond_7
    iget v1, p4, Landroid/graphics/RectF;->right:F

    float-to-int v5, v1

    int-to-float v1, v2

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v6

    cmpl-float v1, v1, v6

    if-lez v1, :cond_8

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v1

    :goto_6
    float-to-int v1, v1

    invoke-virtual {p5, v4, v0, v5, v1}, Landroid/graphics/Rect;->set(IIII)V

    invoke-direct {p0, p5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->applyScaledRect(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSelectPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, p5, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_0

    :cond_8
    int-to-float v1, v2

    goto :goto_6
.end method

.method private findWord(ILcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;)Z
    .locals 8

    const/16 v7, 0xa

    const/4 v1, 0x1

    const/16 v6, 0x20

    const/16 v5, 0x9

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsTyping:Z

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    :cond_2
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getTextLength()I

    move-result v2

    if-ge p1, v2, :cond_0

    if-ltz p1, :cond_0

    iput v0, p2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->endIndex:I

    iput v0, p2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->startIndex:I

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineFromIndex(I)I

    move-result v2

    if-ltz v2, :cond_0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    if-eqz v3, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v0, v0, v2

    iput v0, p2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->startIndex:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v0, v0, v2

    add-int/lit8 v0, v0, 0x1

    iput v0, p2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->endIndex:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v2

    add-int/lit8 v0, p1, -0x1

    :goto_1
    iget v3, p2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->startIndex:I

    if-ge v0, v3, :cond_4

    :goto_2
    invoke-virtual {v2, p1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-eq v0, v6, :cond_3

    if-eq v0, v5, :cond_3

    if-eq v0, v7, :cond_3

    const/16 v3, 0xd

    if-ne v0, v3, :cond_7

    :cond_3
    iput p1, p2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->endIndex:I

    move v0, v1

    goto :goto_0

    :cond_4
    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-eq v3, v6, :cond_5

    if-eq v3, v5, :cond_5

    if-eq v3, v7, :cond_5

    const/16 v4, 0xd

    if-ne v3, v4, :cond_6

    :cond_5
    add-int/lit8 v0, v0, 0x1

    iput v0, p2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->startIndex:I

    goto :goto_2

    :cond_6
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_7
    add-int/lit8 v0, p1, 0x1

    :goto_3
    iget v3, p2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->endIndex:I

    if-lt v0, v3, :cond_8

    :goto_4
    move v0, v1

    goto :goto_0

    :cond_8
    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-eq v3, v6, :cond_9

    if-ne v3, v5, :cond_a

    :cond_9
    iput v0, p2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->endIndex:I

    goto :goto_4

    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_3
.end method

.method private getAbsolutePoint(FF)[F
    .locals 5

    const/4 v0, 0x0

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v1, 0x2

    new-array v1, v1, [F

    aput p1, v1, v4

    const/4 v2, 0x1

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getPan(I)F

    move-result v3

    add-float/2addr v3, p2

    aput v3, v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2, v3}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v2, v1}, Landroid/graphics/Matrix;->mapPoints([F)V

    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v3

    if-nez v3, :cond_1

    const-string/jumbo v1, "coordinateInfo is null."

    invoke-static {v4, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v0

    invoke-direct {p0, v2, v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRotation()F

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    invoke-virtual {v0, v3, v4, v2}, Landroid/graphics/Matrix;->setRotate(FFF)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v2}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapPoints([F)V

    move-object v0, v1

    goto :goto_0
.end method

.method private getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    return-object v0
.end method

.method private getCurrnetLineIndex(I)I
    .locals 2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getLineCount(I)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-gez v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    return v0

    :cond_1
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getLineStartIndex(II)I

    move-result v1

    if-ge p1, v1, :cond_0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method private getCursorIndex(IF)I
    .locals 9

    const/4 v8, 0x0

    const/4 v5, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "getCursorIndex line = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", h ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v0, :cond_1

    move v2, v5

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsTyping:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextDirection:[I

    if-nez v0, :cond_4

    :cond_3
    const-string/jumbo v0, "getCursorIndex fail to measureText()"

    invoke-static {v5, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    move v2, v5

    goto :goto_0

    :cond_4
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    if-lt p1, v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    add-int/lit8 v1, v1, -0x1

    aget v2, v0, v1

    goto :goto_0

    :cond_5
    if-gez p1, :cond_6

    move v2, v5

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_7

    move v2, v5

    goto :goto_0

    :cond_7
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v1, v1, p1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v2, v2, p1

    if-ne v1, v2, :cond_8

    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsFirstCharLF:Z

    if-eqz v1, :cond_8

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v0, v0, p1

    add-int/lit8 v2, v0, 0x1

    goto :goto_0

    :cond_8
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v1, v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0xa

    if-eq v0, v1, :cond_9

    const/16 v1, 0xd

    if-ne v0, v1, :cond_a

    :cond_9
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v0, v0, p1

    add-int/lit8 v0, v0, 0x1

    :goto_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v6, v1, p1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v4, v1, p1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v2, v2, p1

    aget-object v1, v1, v2

    iget v3, v1, Landroid/graphics/RectF;->left:F

    move v2, v0

    move v1, v4

    move v0, v3

    :goto_2
    if-le v2, v6, :cond_b

    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "getCursorIndex h = "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " startIndex = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, " lastIndex = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    cmpg-float v0, p2, v3

    if-gez v0, :cond_13

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextDirection:[I

    aget v0, v0, v4

    if-nez v0, :cond_12

    move v2, v4

    goto/16 :goto_0

    :cond_a
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v0, v0, p1

    goto :goto_1

    :cond_b
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v7, v7, v2

    iget v7, v7, Landroid/graphics/RectF;->left:F

    cmpl-float v7, p2, v7

    if-ltz v7, :cond_f

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v7, v7, v2

    iget v7, v7, Landroid/graphics/RectF;->right:F

    cmpg-float v7, p2, v7

    if-gtz v7, :cond_f

    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "getCursorIndex index = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v0

    cmpl-float v0, p2, v0

    if-ltz v0, :cond_d

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextDirection:[I

    aget v0, v0, v2

    if-nez v0, :cond_0

    if-ge v2, v6, :cond_c

    add-int/lit8 v2, v2, 0x1

    :cond_c
    :goto_3
    if-gt v2, v6, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    cmpl-float v0, v0, v8

    if-nez v0, :cond_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_d
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextDirection:[I

    aget v0, v0, v2

    if-eqz v0, :cond_0

    if-ge v2, v6, :cond_e

    add-int/lit8 v2, v2, 0x1

    :cond_e
    :goto_4
    if-gt v2, v6, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    cmpl-float v0, v0, v8

    if-nez v0, :cond_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_f
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v7, v7, v2

    iget v7, v7, Landroid/graphics/RectF;->left:F

    cmpg-float v7, v0, v7

    if-gez v7, :cond_10

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v0, v0, v2

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move v1, v2

    :cond_10
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v7, v7, v2

    iget v7, v7, Landroid/graphics/RectF;->left:F

    cmpl-float v7, v3, v7

    if-lez v7, :cond_11

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v3, v3, v2

    iget v3, v3, Landroid/graphics/RectF;->left:F

    move v4, v2

    :cond_11
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_2

    :cond_12
    add-int/lit8 v2, v4, 0x1

    goto/16 :goto_0

    :cond_13
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextDirection:[I

    aget v0, v0, v1

    if-nez v0, :cond_14

    add-int/lit8 v2, v1, 0x1

    goto/16 :goto_0

    :cond_14
    move v2, v1

    goto/16 :goto_0
.end method

.method private getCursorRect(I)Landroid/graphics/Rect;
    .locals 11

    const/high16 v10, 0x40800000    # 4.0f

    const/high16 v9, 0x40000000    # 2.0f

    const/4 v1, 0x0

    const/4 v4, 0x0

    const-string/jumbo v0, "getCursorRect() is called."

    invoke-static {v4, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    if-nez v0, :cond_0

    const-string/jumbo v0, "getCursorRect() mNativeTextView is invalid."

    invoke-static {v4, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v0, :cond_1

    move-object v0, v1

    goto :goto_0

    :cond_1
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    if-lez p1, :cond_8

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsTyping:Z

    if-eqz v0, :cond_4

    const-string/jumbo v0, "getCursorRect() calculate by native_getTextRect."

    invoke-static {v4, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    add-int/lit8 v3, p1, -0x1

    invoke-static {v1, v3, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getTextRect(IILandroid/graphics/RectF;)Z

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getGravity()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getDeltaY(I)F

    move-result v3

    invoke-virtual {v0, v1, v3}, Landroid/graphics/RectF;->offset(FF)V

    add-int/lit8 v1, p1, -0x1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isLTRAttribute(I)Z

    move-result v1

    if-eqz v1, :cond_3

    iget v1, v0, Landroid/graphics/RectF;->right:F

    float-to-int v1, v1

    add-int/lit8 v1, v1, -0x2

    iget v3, v0, Landroid/graphics/RectF;->top:F

    float-to-int v3, v3

    iget v4, v0, Landroid/graphics/RectF;->right:F

    float-to-int v4, v4

    add-int/lit8 v4, v4, 0x2

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    float-to-int v0, v0

    invoke-virtual {v2, v1, v3, v4, v0}, Landroid/graphics/Rect;->set(IIII)V

    :cond_2
    :goto_1
    move-object v0, v2

    goto :goto_0

    :cond_3
    iget v1, v0, Landroid/graphics/RectF;->left:F

    float-to-int v1, v1

    add-int/lit8 v1, v1, -0x2

    iget v3, v0, Landroid/graphics/RectF;->top:F

    float-to-int v3, v3

    iget v4, v0, Landroid/graphics/RectF;->left:F

    float-to-int v4, v4

    add-int/lit8 v4, v4, 0x2

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    float-to-int v0, v0

    invoke-virtual {v2, v1, v3, v4, v0}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_1

    :cond_4
    const-string/jumbo v0, "getCursorRect() calculate by mTextRect."

    invoke-static {v4, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextDirection:[I

    if-nez v0, :cond_6

    :cond_5
    move-object v0, v1

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    add-int/lit8 v1, p1, -0x1

    aget-object v0, v0, v1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextDirection:[I

    add-int/lit8 v3, p1, -0x1

    aget v1, v1, v3

    if-nez v1, :cond_7

    iget v1, v0, Landroid/graphics/RectF;->right:F

    float-to-int v1, v1

    add-int/lit8 v1, v1, -0x2

    iget v3, v0, Landroid/graphics/RectF;->top:F

    float-to-int v3, v3

    iget v4, v0, Landroid/graphics/RectF;->right:F

    float-to-int v4, v4

    add-int/lit8 v4, v4, 0x2

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    float-to-int v0, v0

    invoke-virtual {v2, v1, v3, v4, v0}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_1

    :cond_7
    iget v1, v0, Landroid/graphics/RectF;->left:F

    float-to-int v1, v1

    add-int/lit8 v1, v1, -0x2

    iget v3, v0, Landroid/graphics/RectF;->top:F

    float-to-int v3, v3

    iget v4, v0, Landroid/graphics/RectF;->left:F

    float-to-int v4, v4

    add-int/lit8 v4, v4, 0x2

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    float-to-int v0, v0

    invoke-virtual {v2, v1, v3, v4, v0}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_1

    :cond_8
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_a

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getLineStartIndex(II)I

    move-result v0

    const/4 v3, -0x1

    if-eq v0, v3, :cond_a

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    if-nez v0, :cond_9

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v1, v4, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getTextRect(IILandroid/graphics/RectF;)Z

    :goto_2
    iget v1, v0, Landroid/graphics/RectF;->left:F

    float-to-int v1, v1

    add-int/lit8 v1, v1, -0x2

    iget v3, v0, Landroid/graphics/RectF;->top:F

    float-to-int v3, v3

    iget v4, v0, Landroid/graphics/RectF;->left:F

    float-to-int v4, v4

    add-int/lit8 v4, v4, 0x2

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    float-to-int v0, v0

    invoke-virtual {v2, v1, v3, v4, v0}, Landroid/graphics/Rect;->set(IIII)V

    goto/16 :goto_1

    :cond_9
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v0, v0, v4

    goto :goto_2

    :cond_a
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0, v4, v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->findSpans(II)Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_b

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_3
    if-gez v0, :cond_d

    :cond_b
    :goto_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getParagraph()Ljava/util/ArrayList;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v3, v4

    :goto_5
    if-ge v3, v6, :cond_2

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    if-eqz v0, :cond_c

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->type:I

    if-nez v0, :cond_f

    iget v7, v2, Landroid/graphics/Rect;->top:I

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->lineSpacing:F

    float-to-int v0, v0

    add-int/2addr v0, v7

    iput v0, v2, Landroid/graphics/Rect;->bottom:I

    :cond_c
    :goto_6
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v7

    if-nez v7, :cond_10

    const-string/jumbo v0, "getCursorRect() objectRect is invalid."

    invoke-static {v4, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    move-object v0, v1

    goto/16 :goto_0

    :cond_d
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    instance-of v5, v5, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    if-eqz v5, :cond_e

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getTopMargin()F

    move-result v5

    float-to-int v5, v5

    iput v5, v2, Landroid/graphics/Rect;->top:I

    iget v5, v2, Landroid/graphics/Rect;->top:I

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    float-to-int v0, v0

    add-int/2addr v0, v5

    iput v0, v2, Landroid/graphics/Rect;->bottom:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getLeftMargin()F

    move-result v0

    float-to-int v0, v0

    iput v0, v2, Landroid/graphics/Rect;->left:I

    iget v0, v2, Landroid/graphics/Rect;->left:I

    add-int/lit8 v0, v0, 0x4

    iput v0, v2, Landroid/graphics/Rect;->right:I

    goto :goto_4

    :cond_e
    add-int/lit8 v0, v0, -0x1

    goto :goto_3

    :cond_f
    iget v7, v2, Landroid/graphics/Rect;->top:I

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v8, v0

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->lineSpacing:F

    mul-float/2addr v0, v8

    float-to-int v0, v0

    add-int/2addr v0, v7

    iput v0, v2, Landroid/graphics/Rect;->bottom:I

    goto :goto_6

    :cond_10
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;->align:I

    const/4 v8, 0x2

    if-ne v0, v8, :cond_12

    iget v0, v2, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    invoke-virtual {v7}, Landroid/graphics/RectF;->width()F

    move-result v8

    div-float/2addr v8, v9

    add-float/2addr v0, v8

    float-to-int v0, v0

    iput v0, v2, Landroid/graphics/Rect;->left:I

    iget v0, v2, Landroid/graphics/Rect;->right:I

    int-to-float v0, v0

    invoke-virtual {v7}, Landroid/graphics/RectF;->width()F

    move-result v7

    div-float/2addr v7, v9

    add-float/2addr v0, v7

    float-to-int v0, v0

    iput v0, v2, Landroid/graphics/Rect;->right:I

    :cond_11
    :goto_7
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_5

    :cond_12
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;->align:I

    const/4 v8, 0x1

    if-ne v0, v8, :cond_11

    invoke-virtual {v7}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRightMargin()F

    move-result v8

    sub-float/2addr v0, v8

    sub-float/2addr v0, v10

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v8

    int-to-float v8, v8

    sub-float/2addr v0, v8

    float-to-int v0, v0

    iput v0, v2, Landroid/graphics/Rect;->left:I

    invoke-virtual {v7}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRightMargin()F

    move-result v7

    sub-float/2addr v0, v7

    sub-float/2addr v0, v10

    float-to-int v0, v0

    iput v0, v2, Landroid/graphics/Rect;->right:I

    goto :goto_7
.end method

.method private getDeltaY(I)F
    .locals 6

    const/4 v5, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "measureText() objectRect is invalid."

    invoke-static {v3, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v2

    if-nez v2, :cond_2

    const-string/jumbo v0, "coordinateInfo is null."

    invoke-static {v3, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    goto :goto_0

    :cond_2
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    invoke-direct {p0, v3, v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getHeight(I)I

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getDefaultHeight()I

    move-result v0

    move v2, v0

    :goto_1
    if-eq p1, v5, :cond_3

    const/4 v0, 0x2

    if-ne p1, v0, :cond_6

    :cond_3
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v0

    int-to-float v4, v2

    sub-float/2addr v0, v4

    if-ne p1, v5, :cond_4

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v0, v4

    :cond_4
    :goto_2
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    int-to-float v2, v2

    cmpg-float v2, v3, v2

    if-ltz v2, :cond_0

    move v1, v0

    goto :goto_0

    :cond_5
    int-to-float v0, v0

    iget v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v0, v2

    float-to-int v0, v0

    move v2, v0

    goto :goto_1

    :cond_6
    move v0, v1

    goto :goto_2
.end method

.method private getDrawableImage(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string/jumbo v2, "com.samsung.android.sdk.spen30"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v1

    const-string/jumbo v2, "drawable"

    const-string/jumbo v3, "com.samsung.android.sdk.spen30"

    invoke-virtual {v1, p1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    :try_start_1
    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    :try_start_2
    invoke-virtual {v1}, Ljava/lang/OutOfMemoryError;->printStackTrace()V
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private getInvMatrix()Landroid/graphics/Matrix;
    .locals 2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method private getLineForVertical(F)I
    .locals 4

    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "getLineForVertical v = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", mLineCount ="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsTyping:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLinePosition:[Landroid/graphics/PointF;

    if-nez v1, :cond_2

    :cond_1
    :goto_0
    return v0

    :cond_2
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    add-int/lit8 v1, v1, -0x1

    :goto_1
    if-ltz v1, :cond_1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLinePosition:[Landroid/graphics/PointF;

    aget-object v2, v2, v1

    iget v2, v2, Landroid/graphics/PointF;->y:F

    cmpl-float v2, p1, v2

    if-ltz v2, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "getLineForVertical line = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    move v0, v1

    goto :goto_0

    :cond_3
    add-int/lit8 v1, v1, -0x1

    goto :goto_1
.end method

.method private getLineFromIndex(I)I
    .locals 3

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsTyping:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    if-nez v0, :cond_2

    :cond_1
    :goto_0
    return v1

    :cond_2
    if-ltz p1, :cond_1

    move v0, v1

    :goto_1
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v2, v2, v0

    if-gt p1, v2, :cond_3

    move v1, v0

    goto :goto_0

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private getMultiLanguage(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string/jumbo v2, "com.samsung.android.sdk.spen30"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v1

    const-string/jumbo v2, "string"

    const-string/jumbo v3, "com.samsung.android.sdk.spen30"

    invoke-virtual {v1, p1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private getOffsetForHorizontal(IF)I
    .locals 7

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsTyping:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    if-ge p1, v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v0, v0, p1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v1, v1, p1

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v0, v0, p1

    add-int/lit8 v2, v0, 0x1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v1, v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0xa

    if-eq v0, v1, :cond_4

    const/16 v1, 0xd

    if-ne v0, v1, :cond_5

    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v0, v0, p1

    add-int/lit8 v0, v0, 0x1

    :goto_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v5, v1, p1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v4, v1, p1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v2, v2, p1

    aget-object v1, v1, v2

    iget v3, v1, Landroid/graphics/RectF;->left:F

    move v2, v0

    move v1, v4

    move v0, v3

    :goto_2
    if-le v2, v5, :cond_6

    cmpg-float v0, p2, v3

    if-gez v0, :cond_a

    :goto_3
    move v2, v4

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v0, v0, p1

    goto :goto_1

    :cond_6
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v6, v6, v2

    iget v6, v6, Landroid/graphics/RectF;->left:F

    cmpl-float v6, p2, v6

    if-ltz v6, :cond_7

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v6, v6, v2

    iget v6, v6, Landroid/graphics/RectF;->right:F

    cmpg-float v6, p2, v6

    if-lez v6, :cond_0

    :cond_7
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v6, v6, v2

    iget v6, v6, Landroid/graphics/RectF;->left:F

    cmpg-float v6, v0, v6

    if-gez v6, :cond_8

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v0, v0, v2

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move v1, v2

    :cond_8
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v6, v6, v2

    iget v6, v6, Landroid/graphics/RectF;->left:F

    cmpl-float v6, v3, v6

    if-lez v6, :cond_9

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v3, v3, v2

    iget v3, v3, Landroid/graphics/RectF;->left:F

    move v4, v2

    :cond_9
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_a
    move v4, v1

    goto :goto_3
.end method

.method private getRelativePoint(FF)[F
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x2

    new-array v1, v1, [F

    const/4 v2, 0x0

    aput p1, v1, v2

    const/4 v2, 0x1

    aput p2, v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v2

    if-eqz v2, :cond_0

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRotation()F

    move-result v3

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    invoke-virtual {v0, v3, v4, v2}, Landroid/graphics/Matrix;->setRotate(FFF)V

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapPoints([F)V

    move-object v0, v1

    goto :goto_0
.end method

.method private getScrollBarRect()Landroid/graphics/Rect;
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v1, 0x0

    const-string/jumbo v2, "invalid objectRect"

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    goto :goto_0

    :cond_1
    iget v0, v1, Landroid/graphics/RectF;->left:F

    neg-float v0, v0

    iget v2, v1, Landroid/graphics/RectF;->top:F

    neg-float v2, v2

    invoke-virtual {v1, v0, v2}, Landroid/graphics/RectF;->offset(FF)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getPan(I)F

    move-result v2

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iget v3, v1, Landroid/graphics/RectF;->right:F

    float-to-int v3, v3

    add-int/lit8 v3, v3, -0xd

    iput v3, v0, Landroid/graphics/Rect;->left:I

    iget v3, v1, Landroid/graphics/RectF;->right:F

    float-to-int v3, v3

    add-int/lit8 v3, v3, -0x5

    add-int/lit8 v3, v3, 0x4

    iput v3, v0, Landroid/graphics/Rect;->right:I

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getHeight(I)I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v2, v4

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, v0, Landroid/graphics/Rect;->top:I

    iget v2, v0, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v3

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getHeight(I)I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v1, v4

    mul-float/2addr v1, v3

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    goto :goto_0
.end method

.method private getSettingInfo(II)Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    .locals 17

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_0
    new-instance v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    invoke-direct {v12}, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;-><init>()V

    iget v2, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_WIDTH:F

    float-to-double v3, v3

    const-wide/high16 v5, 0x4069000000000000L    # 200.0

    div-double/2addr v3, v5

    double-to-float v3, v3

    mul-float/2addr v2, v3

    iput v2, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    const/4 v11, 0x0

    const/4 v10, 0x0

    const/4 v9, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v2, v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->findSpans(II)Ljava/util/ArrayList;

    move-result-object v14

    if-eqz v14, :cond_1

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v13, v2

    :goto_1
    if-gez v13, :cond_4

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getParagraph()Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_20

    :cond_3
    move-object v2, v12

    goto :goto_0

    :cond_4
    invoke-virtual {v14, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    instance-of v3, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    if-eqz v3, :cond_9

    if-nez v7, :cond_9

    move/from16 v0, p1

    move/from16 v1, p2

    if-ne v0, v1, :cond_6

    iget v3, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    iget v15, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    if-ne v3, v15, :cond_5

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    iput v2, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    const/4 v2, 0x1

    move v3, v5

    move v7, v9

    move v5, v2

    move v9, v11

    move v2, v4

    move v4, v6

    move v6, v8

    move v8, v10

    :goto_3
    add-int/lit8 v10, v13, -0x1

    move v13, v10

    move v11, v9

    move v9, v7

    move v10, v8

    move v7, v5

    move v8, v6

    move v5, v3

    move v6, v4

    move v4, v2

    goto :goto_1

    :cond_5
    iget v3, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p2

    if-eq v3, v0, :cond_1f

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    iput v2, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    add-int/lit8 v2, v11, 0x1

    move v3, v5

    move v5, v7

    move v7, v9

    move v9, v2

    move v2, v4

    move v4, v6

    move v6, v8

    move v8, v10

    goto :goto_3

    :cond_6
    iget v3, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p1

    if-gt v3, v0, :cond_7

    iget v3, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    move/from16 v0, p2

    if-lt v3, v0, :cond_7

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    iput v2, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    const/4 v2, 0x1

    move v3, v5

    move v7, v9

    move v5, v2

    move v9, v11

    move v2, v4

    move v4, v6

    move v6, v8

    move v8, v10

    goto :goto_3

    :cond_7
    if-nez v11, :cond_8

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    iput v2, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    add-int/lit8 v2, v11, 0x1

    move v3, v5

    move v5, v7

    move v7, v9

    move v9, v2

    move v2, v4

    move v4, v6

    move v6, v8

    move v8, v10

    goto :goto_3

    :cond_8
    iget v3, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    cmpl-float v2, v3, v2

    if-eqz v2, :cond_1f

    const/4 v2, 0x0

    iput v2, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    move v2, v4

    move v3, v5

    move v4, v6

    move v5, v7

    move v6, v8

    move v7, v9

    move v8, v10

    move v9, v11

    goto :goto_3

    :cond_9
    instance-of v3, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;

    if-eqz v3, :cond_e

    if-nez v6, :cond_e

    move/from16 v0, p1

    move/from16 v1, p2

    if-ne v0, v1, :cond_b

    iget v3, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    iget v15, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    if-ne v3, v15, :cond_a

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;->foregroundColor:I

    iput v2, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    const/4 v2, 0x1

    move v3, v5

    move v6, v8

    move v5, v7

    move v8, v10

    move v7, v9

    move v9, v11

    move/from16 v16, v2

    move v2, v4

    move/from16 v4, v16

    goto/16 :goto_3

    :cond_a
    iget v3, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p2

    if-eq v3, v0, :cond_1f

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;->foregroundColor:I

    iput v2, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    add-int/lit8 v2, v10, 0x1

    move v3, v5

    move v5, v7

    move v7, v9

    move v9, v11

    move/from16 v16, v8

    move v8, v2

    move v2, v4

    move v4, v6

    move/from16 v6, v16

    goto/16 :goto_3

    :cond_b
    iget v3, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p1

    if-gt v3, v0, :cond_c

    iget v3, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    move/from16 v0, p2

    if-lt v3, v0, :cond_c

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;->foregroundColor:I

    iput v2, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    const/4 v2, 0x1

    move v3, v5

    move v6, v8

    move v5, v7

    move v8, v10

    move v7, v9

    move v9, v11

    move/from16 v16, v2

    move v2, v4

    move/from16 v4, v16

    goto/16 :goto_3

    :cond_c
    if-nez v10, :cond_d

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;->foregroundColor:I

    iput v2, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    add-int/lit8 v2, v10, 0x1

    move v3, v5

    move v5, v7

    move v7, v9

    move v9, v11

    move/from16 v16, v8

    move v8, v2

    move v2, v4

    move v4, v6

    move/from16 v6, v16

    goto/16 :goto_3

    :cond_d
    iget v3, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;->foregroundColor:I

    if-eq v3, v2, :cond_1f

    const/4 v2, 0x0

    iput v2, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    move v2, v4

    move v3, v5

    move v4, v6

    move v5, v7

    move v6, v8

    move v7, v9

    move v8, v10

    move v9, v11

    goto/16 :goto_3

    :cond_e
    instance-of v3, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;

    if-eqz v3, :cond_13

    if-nez v5, :cond_13

    move/from16 v0, p1

    move/from16 v1, p2

    if-ne v0, v1, :cond_10

    iget v3, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    iget v15, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    if-ne v3, v15, :cond_f

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;->backgroundColor:I

    iput v2, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->bgColor:I

    const/4 v2, 0x1

    move v3, v2

    move v5, v7

    move v2, v4

    move v7, v9

    move v4, v6

    move v9, v11

    move v6, v8

    move v8, v10

    goto/16 :goto_3

    :cond_f
    iget v3, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p2

    if-eq v3, v0, :cond_1f

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;->backgroundColor:I

    iput v2, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->bgColor:I

    add-int/lit8 v2, v9, 0x1

    move v3, v5

    move v9, v11

    move v5, v7

    move v7, v2

    move v2, v4

    move v4, v6

    move v6, v8

    move v8, v10

    goto/16 :goto_3

    :cond_10
    iget v3, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p1

    if-gt v3, v0, :cond_11

    iget v3, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    move/from16 v0, p2

    if-lt v3, v0, :cond_11

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;->backgroundColor:I

    iput v2, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->bgColor:I

    const/4 v2, 0x1

    move v3, v2

    move v5, v7

    move v2, v4

    move v7, v9

    move v4, v6

    move v9, v11

    move v6, v8

    move v8, v10

    goto/16 :goto_3

    :cond_11
    if-nez v9, :cond_12

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;->backgroundColor:I

    iput v2, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->bgColor:I

    add-int/lit8 v2, v9, 0x1

    move v3, v5

    move v9, v11

    move v5, v7

    move v7, v2

    move v2, v4

    move v4, v6

    move v6, v8

    move v8, v10

    goto/16 :goto_3

    :cond_12
    iget v3, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->bgColor:I

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;->backgroundColor:I

    if-eq v3, v2, :cond_1f

    const/4 v2, 0x0

    iput v2, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->bgColor:I

    move v2, v4

    move v3, v5

    move v4, v6

    move v5, v7

    move v6, v8

    move v7, v9

    move v8, v10

    move v9, v11

    goto/16 :goto_3

    :cond_13
    instance-of v3, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;

    if-eqz v3, :cond_18

    if-nez v4, :cond_18

    move/from16 v0, p1

    move/from16 v1, p2

    if-ne v0, v1, :cond_15

    iget v3, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    iget v15, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    if-ne v3, v15, :cond_14

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;->fontName:Ljava/lang/String;

    iput-object v2, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    const/4 v2, 0x1

    move v3, v5

    move v4, v6

    move v5, v7

    move v6, v8

    move v7, v9

    move v8, v10

    move v9, v11

    goto/16 :goto_3

    :cond_14
    iget v3, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p2

    if-eq v3, v0, :cond_1f

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;->fontName:Ljava/lang/String;

    iput-object v2, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    add-int/lit8 v2, v8, 0x1

    move v3, v5

    move v8, v10

    move v5, v7

    move v7, v9

    move v9, v11

    move/from16 v16, v6

    move v6, v2

    move v2, v4

    move/from16 v4, v16

    goto/16 :goto_3

    :cond_15
    iget v3, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p1

    if-gt v3, v0, :cond_16

    iget v3, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    move/from16 v0, p2

    if-lt v3, v0, :cond_16

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;->fontName:Ljava/lang/String;

    iput-object v2, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    const/4 v2, 0x1

    move v3, v5

    move v4, v6

    move v5, v7

    move v6, v8

    move v7, v9

    move v8, v10

    move v9, v11

    goto/16 :goto_3

    :cond_16
    if-nez v8, :cond_17

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;->fontName:Ljava/lang/String;

    iput-object v2, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    add-int/lit8 v2, v8, 0x1

    move v3, v5

    move v8, v10

    move v5, v7

    move v7, v9

    move v9, v11

    move/from16 v16, v6

    move v6, v2

    move v2, v4

    move/from16 v4, v16

    goto/16 :goto_3

    :cond_17
    iget-object v3, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;->fontName:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_1f

    const-string/jumbo v2, ""

    iput-object v2, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    move v2, v4

    move v3, v5

    move v4, v6

    move v5, v7

    move v6, v8

    move v7, v9

    move v8, v10

    move v9, v11

    goto/16 :goto_3

    :cond_18
    instance-of v3, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BoldStyleSpanInfo;

    if-eqz v3, :cond_1a

    iget v3, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p2

    if-ne v3, v0, :cond_19

    if-nez p2, :cond_1a

    :cond_19
    move-object v3, v2

    check-cast v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BoldStyleSpanInfo;

    iget-boolean v3, v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BoldStyleSpanInfo;->isBold:Z

    if-eqz v3, :cond_1f

    iget v3, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p1

    if-lt v0, v3, :cond_1f

    iget v2, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    move/from16 v0, p2

    if-gt v0, v2, :cond_1f

    iget v2, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    move v2, v4

    move v3, v5

    move v4, v6

    move v5, v7

    move v6, v8

    move v7, v9

    move v8, v10

    move v9, v11

    goto/16 :goto_3

    :cond_1a
    instance-of v3, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;

    if-eqz v3, :cond_1c

    iget v3, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p2

    if-ne v3, v0, :cond_1b

    if-nez p2, :cond_1c

    :cond_1b
    move-object v3, v2

    check-cast v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;

    iget-boolean v3, v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;->isItalic:Z

    if-eqz v3, :cond_1f

    iget v3, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p1

    if-lt v0, v3, :cond_1f

    iget v2, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    move/from16 v0, p2

    if-gt v0, v2, :cond_1f

    iget v2, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    or-int/lit8 v2, v2, 0x2

    iput v2, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    move v2, v4

    move v3, v5

    move v4, v6

    move v5, v7

    move v6, v8

    move v7, v9

    move v8, v10

    move v9, v11

    goto/16 :goto_3

    :cond_1c
    instance-of v3, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$UnderlineStyleSpanInfo;

    if-eqz v3, :cond_1e

    iget v3, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p2

    if-ne v3, v0, :cond_1d

    if-nez p2, :cond_1e

    :cond_1d
    move-object v3, v2

    check-cast v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$UnderlineStyleSpanInfo;

    iget-boolean v3, v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$UnderlineStyleSpanInfo;->isUnderline:Z

    if-eqz v3, :cond_1f

    iget v3, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p1

    if-lt v0, v3, :cond_1f

    iget v2, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    move/from16 v0, p2

    if-gt v0, v2, :cond_1f

    iget v2, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    or-int/lit8 v2, v2, 0x4

    iput v2, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    move v2, v4

    move v3, v5

    move v4, v6

    move v5, v7

    move v6, v8

    move v7, v9

    move v8, v10

    move v9, v11

    goto/16 :goto_3

    :cond_1e
    instance-of v3, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;

    if-eqz v3, :cond_1f

    iget v3, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    move/from16 v0, p2

    if-eq v3, v0, :cond_1f

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;->textDirection:I

    iput v2, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    :cond_1f
    move v2, v4

    move v3, v5

    move v4, v6

    move v5, v7

    move v6, v8

    move v7, v9

    move v8, v10

    move v9, v11

    goto/16 :goto_3

    :cond_20
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;

    instance-of v3, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;

    if-eqz v3, :cond_21

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;->align:I

    iput v2, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    goto/16 :goto_2

    :cond_21
    instance-of v3, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    if-eqz v3, :cond_2

    move-object v3, v2

    check-cast v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    iget v3, v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->type:I

    iput v3, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacingType:I

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->lineSpacing:F

    iput v2, v12, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    goto/16 :goto_2
.end method

.method private getTextLength()I
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_0
.end method

.method private hideCursorHandle()V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    aget-object v0, v0, v2

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;->removeCallbacks(Ljava/lang/Runnable;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleVisible:Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    aget-object v0, v0, v2

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0
.end method

.method private initCue()V
    .locals 9
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v8, -0x2

    const/4 v7, 0x2

    const/4 v2, 0x0

    new-array v0, v7, [Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueBitmap:[Landroid/graphics/Bitmap;

    move v0, v2

    :goto_0
    if-lt v0, v7, :cond_1

    new-array v0, v7, [Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueButton:[Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    move v3, v2

    move v1, v2

    :goto_1
    if-lt v3, v7, :cond_3

    :cond_0
    return-void

    :cond_1
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->DEFAULT_CUE_FILE_NAME:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getDrawableImage(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueBitmap:[Landroid/graphics/Bitmap;

    invoke-static {v1}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v4

    aput-object v4, v3, v0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueBitmap:[Landroid/graphics/Bitmap;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueBitmap:[Landroid/graphics/Bitmap;

    aget-object v4, v4, v0

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v4, v5, v2}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueBitmap:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string/jumbo v1, "Bitmap is already recycled."

    invoke-static {v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueButton:[Landroid/widget/ImageButton;

    new-instance v5, Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    aput-object v5, v4, v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueButton:[Landroid/widget/ImageButton;

    aget-object v4, v4, v3

    invoke-virtual {v4, v2}, Landroid/widget/ImageButton;->setBackgroundColor(I)V

    packed-switch v3, :pswitch_data_0

    :goto_2
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueBitmap:[Landroid/graphics/Bitmap;

    aget-object v4, v4, v1

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueButton:[Landroid/widget/ImageButton;

    aget-object v4, v4, v3

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueBitmap:[Landroid/graphics/Bitmap;

    aget-object v5, v5, v1

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v4, v8, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueButton:[Landroid/widget/ImageButton;

    aget-object v5, v5, v3

    invoke-virtual {v0, v5, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_4
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueButton:[Landroid/widget/ImageButton;

    aget-object v4, v4, v3

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setVisibility(I)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :pswitch_0
    move v1, v2

    goto :goto_2

    :pswitch_1
    const/4 v1, 0x1

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private initEditable()V
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "initEditable() strBefore = "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsEditableClear:Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v0}, Landroid/text/method/TextKeyListener;->clear(Landroid/text/Editable;)V

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_7

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v2, 0xa

    if-eq v0, v2, :cond_6

    sget-object v0, Landroid/text/method/TextKeyListener$Capitalize;->NONE:Landroid/text/method/TextKeyListener$Capitalize;

    invoke-static {v5, v0}, Landroid/text/method/TextKeyListener;->getInstance(ZLandroid/text/method/TextKeyListener$Capitalize;)Landroid/text/method/TextKeyListener;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeyListener:Landroid/text/method/KeyListener;

    :goto_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setChangeWatcher()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x0

    if-eqz v2, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    if-le v0, v1, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setCursorPos(I)V

    :cond_1
    invoke-virtual {v2, v5, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {v2, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_2
    const-string/jumbo v2, ""

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_3

    move-object v2, v1

    :cond_3
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_4

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_4
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "initEditable() strContent = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", strBefore = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", strAfter = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_8

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v3

    invoke-interface {v0, v5, v3, v2}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    if-eqz v1, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v0, v2, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-static {v0, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    :cond_5
    :goto_2
    iput v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSurroundingTextLength:I

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsEditableClear:Z

    goto/16 :goto_0

    :cond_6
    sget-object v0, Landroid/text/method/TextKeyListener$Capitalize;->SENTENCES:Landroid/text/method/TextKeyListener$Capitalize;

    invoke-static {v5, v0}, Landroid/text/method/TextKeyListener;->getInstance(ZLandroid/text/method/TextKeyListener$Capitalize;)Landroid/text/method/TextKeyListener;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeyListener:Landroid/text/method/KeyListener;

    goto/16 :goto_1

    :cond_7
    sget-object v0, Landroid/text/method/TextKeyListener$Capitalize;->SENTENCES:Landroid/text/method/TextKeyListener$Capitalize;

    invoke-static {v5, v0}, Landroid/text/method/TextKeyListener;->getInstance(ZLandroid/text/method/TextKeyListener$Capitalize;)Landroid/text/method/TextKeyListener;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeyListener:Landroid/text/method/KeyListener;

    goto/16 :goto_1

    :cond_8
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v0}, Landroid/text/Editable;->clear()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v0, v5}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    goto :goto_2
.end method

.method private initHandle()V
    .locals 9

    const/4 v7, 0x6

    const/4 v8, -0x2

    const/4 v3, 0x3

    const/4 v2, 0x0

    new-array v0, v7, [Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    move v0, v2

    :goto_0
    if-lt v0, v7, :cond_1

    new-array v0, v3, [Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    move v4, v2

    move v1, v2

    :goto_1
    if-lt v4, v3, :cond_3

    :cond_0
    return-void

    :cond_1
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->DEFAULT_HANDLE_FILE_NAME:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getDrawableImage(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    invoke-static {v1}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v5

    aput-object v5, v4, v0

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    aget-object v5, v5, v0

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v5, v6, v2}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string/jumbo v1, "Bitmap is already recycled."

    invoke-static {v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    new-instance v6, Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-direct {v6, v7}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    aput-object v6, v5, v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    aget-object v5, v5, v4

    invoke-virtual {v5, v2}, Landroid/widget/ImageButton;->setBackgroundColor(I)V

    packed-switch v4, :pswitch_data_0

    :goto_2
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    aget-object v5, v5, v1

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    aget-object v5, v5, v4

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    aget-object v6, v6, v1

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v5, v8, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    aget-object v6, v6, v4

    invoke-virtual {v0, v6, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_4
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    aget-object v5, v5, v4

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleListener:[Landroid/view/View$OnTouchListener;

    aget-object v6, v6, v4

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    aget-object v5, v5, v4

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1

    :pswitch_0
    move v1, v2

    goto :goto_2

    :pswitch_1
    const/4 v1, 0x5

    goto :goto_2

    :pswitch_2
    move v1, v3

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private initMeasureInfo()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLinePosition:[Landroid/graphics/PointF;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextSize:[F

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextDirection:[I

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextItalic:[Z

    return-void
.end method

.method private initTextBox(Landroid/view/ViewGroup;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v1, -0x2

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    if-eqz p1, :cond_0

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p1, p0, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    new-instance v0, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;

    invoke-direct {v2, p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mGestureDetector:Landroid/view/GestureDetector;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mGestureDetector:Landroid/view/GestureDetector;

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;

    invoke-direct {v1, p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;)V

    invoke-virtual {v0, v1}, Landroid/view/GestureDetector;->setOnDoubleTapListener(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mGestureDetector:Landroid/view/GestureDetector;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/GestureDetector;->setIsLongpressEnabled(Z)V

    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setFocusableInTouchMode(Z)V

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setVisibility(I)V

    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setFocusable(Z)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->initHandle()V

    return-void
.end method

.method private isCursorVisible()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method private isItalicAttribute(I)Z
    .locals 6

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v0, :cond_0

    move v0, v2

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0, p1, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->findSpans(II)Ljava/util/ArrayList;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_1
    if-gez v3, :cond_2

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    instance-of v1, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;

    if-eqz v1, :cond_5

    iget v1, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    if-ne v1, p1, :cond_3

    if-eqz p1, :cond_3

    iget v1, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    iget v5, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    if-ne v1, v5, :cond_5

    :cond_3
    move-object v1, v0

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;

    iget-boolean v1, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;->isItalic:Z

    if-eqz v1, :cond_4

    iget v1, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    if-lt p1, v1, :cond_5

    iget v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    if-gt p1, v0, :cond_5

    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_0

    :cond_5
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_1
.end method

.method private isLTRAttribute(I)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v0, :cond_0

    :goto_0
    return v2

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0, p1, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->findSpans(II)Ljava/util/ArrayList;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_1
    if-gez v3, :cond_2

    :cond_1
    :goto_2
    move v2, v1

    goto :goto_0

    :cond_2
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;

    if-eqz v0, :cond_6

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    iget v5, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    iget v6, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v0}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    :goto_3
    if-gt v5, v0, :cond_3

    if-le v6, v0, :cond_4

    :cond_3
    const-string/jumbo v0, "Invalid Span Info"

    invoke-static {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    goto :goto_2

    :cond_4
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;->textDirection:I

    if-nez v0, :cond_5

    move v0, v1

    :goto_4
    move v2, v0

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_4

    :cond_6
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_1

    :cond_7
    move v0, v2

    goto :goto_3
.end method

.method private static native native_command(IILjava/util/ArrayList;I)Ljava/util/ArrayList;
.end method

.method private static native native_construct(ILandroid/content/Context;)Z
.end method

.method private static native native_finalize(I)V
.end method

.method private static native native_getHeight(I)I
.end method

.method private static native native_getHintTextWidth(I)F
.end method

.method private static native native_getLineCount(I)I
.end method

.method private static native native_getLineEndIndex(II)I
.end method

.method private static native native_getLinePosition(IILandroid/graphics/PointF;)Z
.end method

.method private static native native_getLineStartIndex(II)I
.end method

.method private static native native_getPan(I)F
.end method

.method private static native native_getTextRect(IILandroid/graphics/RectF;)Z
.end method

.method private static native native_init()I
.end method

.method private static native native_measure(II)Z
.end method

.method private static native native_setBitmap(ILandroid/graphics/Bitmap;)Z
.end method

.method private static native native_setObjectText(ILcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)Z
.end method

.method private static native native_setPan(IF)V
.end method

.method private static native native_update(I)Z
.end method

.method private onCueBottomButtonDown(Landroid/view/MotionEvent;)Z
    .locals 9

    const/high16 v8, 0x42480000    # 50.0f

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getPan(I)F

    move-result v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getHeight(I)I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v5

    cmpl-float v3, v3, v5

    if-lez v3, :cond_0

    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getLineCount(I)I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-static {v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getLineEndIndex(II)I

    move-result v6

    invoke-static {v5, v6, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getTextRect(IILandroid/graphics/RectF;)Z

    const/4 v5, 0x2

    new-array v5, v5, [F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    aput v6, v5, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    aput v6, v5, v1

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v6, v7}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v6, v5}, Landroid/graphics/Matrix;->mapPoints([F)V

    iget v3, v3, Landroid/graphics/RectF;->top:F

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v6

    add-float/2addr v2, v6

    cmpl-float v2, v3, v2

    if-lez v2, :cond_0

    aget v2, v5, v0

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v3

    sub-float/2addr v3, v8

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    aget v2, v5, v1

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v3

    sub-float/2addr v3, v8

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private onCueBottonDown(Landroid/view/MotionEvent;)Z
    .locals 8
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/16 v3, 0x8

    const/4 v0, 0x0

    const/4 v7, 0x1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v1

    new-instance v6, Landroid/graphics/RectF;

    invoke-direct {v6}, Landroid/graphics/RectF;-><init>()V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v2

    if-nez v2, :cond_1

    const-string/jumbo v1, "coordinateInfo is null."

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0, v6, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onCueTopButtonDown(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget v1, v6, Landroid/graphics/RectF;->right:F

    float-to-int v1, v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueBitmap:[Landroid/graphics/Bitmap;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sub-int v2, v1, v2

    iget v1, v6, Landroid/graphics/RectF;->top:F

    float-to-int v3, v1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueBitmap:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueBitmap:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueButton:[Landroid/widget/ImageButton;

    aget-object v1, v1, v0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setHandlePos(Landroid/view/View;IIIILandroid/graphics/RectF;)V

    move v0, v7

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onCueBottomButtonDown(Landroid/view/MotionEvent;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget v0, v6, Landroid/graphics/RectF;->right:F

    float-to-int v0, v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueBitmap:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v7

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sub-int/2addr v0, v1

    add-int/lit8 v2, v0, 0x2

    iget v0, v6, Landroid/graphics/RectF;->bottom:F

    float-to-int v0, v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueBitmap:[Landroid/graphics/Bitmap;

    aget-object v1, v1, v7

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sub-int/2addr v0, v1

    add-int/lit8 v3, v0, 0x4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueBitmap:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v7

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueBitmap:[Landroid/graphics/Bitmap;

    aget-object v0, v0, v7

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueButton:[Landroid/widget/ImageButton;

    aget-object v1, v0, v7

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setHandlePos(Landroid/view/View;IIIILandroid/graphics/RectF;)V

    move v0, v7

    goto/16 :goto_0

    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v7, :cond_4

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueButton:[Landroid/widget/ImageButton;

    aget-object v0, v1, v0

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCueButton:[Landroid/widget/ImageButton;

    aget-object v0, v0, v7

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    :cond_4
    move v0, v7

    goto/16 :goto_0
.end method

.method private onCueTopButtonDown(Landroid/view/MotionEvent;)Z
    .locals 9

    const/high16 v8, 0x42480000    # 50.0f

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getPan(I)F

    move-result v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getHeight(I)I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v4, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v5

    cmpl-float v3, v3, v5

    if-lez v3, :cond_0

    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v5, v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getTextRect(IILandroid/graphics/RectF;)Z

    const/4 v5, 0x2

    new-array v5, v5, [F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    aput v6, v5, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    aput v6, v5, v1

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v6, v7}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v6, v5}, Landroid/graphics/Matrix;->mapPoints([F)V

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    cmpg-float v2, v3, v2

    if-gez v2, :cond_0

    aget v2, v5, v0

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v3

    sub-float/2addr v3, v8

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    aget v2, v5, v1

    cmpg-float v2, v2, v8

    if-gez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method private onDrawCursor(Landroid/graphics/Canvas;)V
    .locals 8

    const/4 v0, 0x0

    const-string/jumbo v1, "onDrawCursor()"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorVisible:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsViewMode:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHighlightPathBogus:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v1}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v2

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v1}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v1

    if-ne v2, v1, :cond_0

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorRect(I)Landroid/graphics/Rect;

    move-result-object v3

    if-nez v3, :cond_2

    const-string/jumbo v1, "onDrawCursor() cursorRect is null."

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v4

    if-nez v4, :cond_3

    const-string/jumbo v1, "onDrawCursor() objectRect is null."

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    goto :goto_0

    :cond_3
    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getPan(I)F

    move-result v5

    iget v6, v3, Landroid/graphics/Rect;->bottom:I

    int-to-float v6, v6

    cmpg-float v6, v6, v5

    if-ltz v6, :cond_4

    iget v6, v3, Landroid/graphics/Rect;->top:I

    int-to-float v6, v6

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v7

    add-float/2addr v7, v5

    cmpl-float v6, v6, v7

    if-lez v6, :cond_5

    :cond_4
    const-string/jumbo v1, "onDrawCursor() cursor is out of range."

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    goto :goto_0

    :cond_5
    invoke-direct {p0, v3, v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->adjustCursorSize(Landroid/graphics/Rect;II)V

    iget v1, v3, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    sub-float/2addr v1, v5

    float-to-int v1, v1

    iget v6, v3, Landroid/graphics/Rect;->bottom:I

    int-to-float v6, v6

    sub-float v5, v6, v5

    float-to-int v5, v5

    if-gez v1, :cond_6

    :goto_1
    iput v0, v3, Landroid/graphics/Rect;->top:I

    int-to-float v0, v5

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_7

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v0

    :goto_2
    float-to-int v0, v0

    iput v0, v3, Landroid/graphics/Rect;->bottom:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v0, v3}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v0, v3}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isItalicAttribute(I)Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHighLightPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_0

    :cond_6
    move v0, v1

    goto :goto_1

    :cond_7
    int-to-float v0, v5

    goto :goto_2

    :cond_8
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    sget v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->SIN_15_DEGREE:F

    float-to-double v0, v0

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-double v4, v2

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    div-double/2addr v4, v6

    mul-double/2addr v0, v4

    double-to-float v0, v0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    const/high16 v0, 0x41700000    # 15.0f

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerX()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHighLightPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto/16 :goto_0
.end method

.method private onDrawHandle()V
    .locals 13

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsViewMode:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v1}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v7

    if-ne v0, v7, :cond_3

    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleVisible:Z

    if-nez v1, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isShown()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/widget/ImageButton;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v8

    new-instance v6, Landroid/graphics/RectF;

    invoke-direct {v6}, Landroid/graphics/RectF;-><init>()V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v2

    if-nez v2, :cond_4

    const/4 v0, 0x0

    const-string/jumbo v1, "coordinateInfo is null."

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    goto :goto_0

    :cond_4
    invoke-direct {p0, v6, v8, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorRect(I)Landroid/graphics/Rect;

    move-result-object v3

    if-nez v3, :cond_5

    const/4 v0, 0x0

    const-string/jumbo v1, "cursorRect is null."

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    goto :goto_0

    :cond_5
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getPan(I)F

    move-result v9

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    const/4 v4, 0x1

    aget-object v1, v1, v4

    if-ne v0, v7, :cond_b

    iget v0, v3, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v0

    cmpg-float v0, v0, v9

    if-ltz v0, :cond_6

    iget v0, v3, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    invoke-virtual {v8}, Landroid/graphics/RectF;->height()F

    move-result v4

    add-float/2addr v4, v9

    cmpl-float v0, v0, v4

    if-lez v0, :cond_7

    :cond_6
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_0

    :cond_7
    const/4 v0, 0x0

    neg-float v4, v9

    float-to-int v4, v4

    invoke-virtual {v3, v0, v4}, Landroid/graphics/Rect;->offset(II)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v0, v3}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v0, v4}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v0, v3}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    iget v0, v6, Landroid/graphics/RectF;->left:F

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerX()I

    move-result v4

    int-to-float v4, v4

    add-float v7, v0, v4

    iget v0, v6, Landroid/graphics/RectF;->top:F

    iget v4, v3, Landroid/graphics/Rect;->bottom:I

    int-to-float v4, v4

    add-float/2addr v0, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v5, 0x5

    aget-object v4, v4, v5

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v8, 0x5

    aget-object v5, v5, v8

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    iget-boolean v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleVisible:Z

    if-eqz v8, :cond_9

    int-to-float v8, v5

    add-float/2addr v8, v0

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    cmpl-float v2, v8, v2

    if-lez v2, :cond_a

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    const/4 v8, 0x1

    aget-object v3, v3, v8

    invoke-virtual {v3}, Landroid/widget/ImageButton;->getHeight()I

    move-result v3

    add-int/2addr v2, v3

    int-to-float v2, v2

    sub-float/2addr v0, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v3, 0x4

    aget-object v2, v2, v3

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v3, 0x4

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_8
    :goto_1
    float-to-double v2, v7

    int-to-double v7, v4

    const-wide/high16 v9, 0x4000000000000000L    # 2.0

    div-double/2addr v7, v9

    sub-double/2addr v2, v7

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v2, v2

    float-to-int v3, v0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setHandlePos(Landroid/view/View;IIIILandroid/graphics/RectF;)V

    :cond_9
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_0

    :cond_a
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v3, 0x5

    aget-object v2, v2, v3

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v3, 0x5

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1

    :cond_b
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorRect(I)Landroid/graphics/Rect;

    move-result-object v0

    if-nez v0, :cond_c

    const/4 v0, 0x0

    const-string/jumbo v1, "onDrawHandle().. cursorRect is null."

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    goto/16 :goto_0

    :cond_c
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    cmpg-float v2, v2, v9

    if-ltz v2, :cond_d

    iget v2, v0, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    invoke-virtual {v8}, Landroid/graphics/RectF;->height()F

    move-result v3

    add-float/2addr v3, v9

    cmpl-float v2, v2, v3

    if-lez v2, :cond_e

    :cond_d
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    :goto_2
    invoke-direct {p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorRect(I)Landroid/graphics/Rect;

    move-result-object v7

    if-nez v7, :cond_13

    const/4 v0, 0x0

    const-string/jumbo v1, "onDrawHandle().. cursorRect is null."

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    goto/16 :goto_0

    :cond_e
    const/4 v2, 0x0

    neg-float v3, v9

    float-to-int v3, v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Rect;->offset(II)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v2, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v2, v3}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v2, v0}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    iget v2, v6, Landroid/graphics/RectF;->left:F

    iget v3, v0, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    iget v3, v6, Landroid/graphics/RectF;->top:F

    iget v4, v0, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v10, 0x0

    aget-object v5, v5, v10

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v10, v4

    sub-float v10, v2, v10

    const/4 v11, 0x0

    cmpg-float v10, v10, v11

    if-gez v10, :cond_10

    int-to-float v10, v5

    sub-float v10, v3, v10

    const/4 v11, 0x0

    cmpg-float v10, v10, v11

    if-gez v10, :cond_10

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    add-float/2addr v0, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v10, 0x3

    aget-object v3, v3, v10

    if-eqz v3, :cond_f

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v10, 0x3

    aget-object v3, v3, v10

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_f
    :goto_3
    float-to-int v2, v2

    float-to-int v3, v0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setHandlePos(Landroid/view/View;IIIILandroid/graphics/RectF;)V

    goto :goto_2

    :cond_10
    int-to-float v10, v5

    sub-float v10, v3, v10

    const/4 v11, 0x0

    cmpg-float v10, v10, v11

    if-gez v10, :cond_11

    int-to-float v10, v4

    sub-float/2addr v2, v10

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    add-float/2addr v0, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v10, 0x1

    aget-object v3, v3, v10

    if-eqz v3, :cond_f

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v10, 0x1

    aget-object v3, v3, v10

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_3

    :cond_11
    int-to-float v0, v4

    sub-float v0, v2, v0

    const/4 v10, 0x0

    cmpg-float v0, v0, v10

    if-gez v0, :cond_12

    int-to-float v0, v5

    sub-float v0, v3, v0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v10, 0x2

    aget-object v3, v3, v10

    if-eqz v3, :cond_f

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v10, 0x2

    aget-object v3, v3, v10

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_3

    :cond_12
    int-to-float v0, v4

    sub-float/2addr v2, v0

    int-to-float v0, v5

    sub-float v0, v3, v0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v10, 0x0

    aget-object v3, v3, v10

    if-eqz v3, :cond_f

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v10, 0x0

    aget-object v3, v3, v10

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_3

    :cond_13
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    const/4 v1, 0x2

    aget-object v1, v0, v1

    iget v0, v7, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v0

    cmpg-float v0, v0, v9

    if-ltz v0, :cond_14

    iget v0, v7, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    invoke-virtual {v8}, Landroid/graphics/RectF;->height()F

    move-result v2

    add-float/2addr v2, v9

    cmpl-float v0, v0, v2

    if-lez v0, :cond_15

    :cond_14
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_0

    :cond_15
    const/4 v0, 0x0

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getPan(I)F

    move-result v2

    neg-float v2, v2

    float-to-int v2, v2

    invoke-virtual {v7, v0, v2}, Landroid/graphics/Rect;->offset(II)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v0, v7}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v0, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v0, v7}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    iget v0, v6, Landroid/graphics/RectF;->left:F

    iget v2, v7, Landroid/graphics/Rect;->right:I

    int-to-float v2, v2

    add-float v3, v0, v2

    iget v0, v6, Landroid/graphics/RectF;->top:F

    iget v2, v7, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    add-float/2addr v2, v0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v4, 0x3

    aget-object v0, v0, v4

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v5, 0x3

    aget-object v0, v0, v5

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v8

    if-eqz v8, :cond_0

    int-to-float v8, v4

    add-float/2addr v8, v3

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v9

    int-to-float v9, v9

    cmpl-float v8, v8, v9

    if-lez v8, :cond_16

    int-to-float v8, v5

    add-float/2addr v8, v2

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v9

    int-to-float v9, v9

    cmpl-float v8, v8, v9

    if-lez v8, :cond_16

    int-to-float v0, v4

    sub-float/2addr v3, v0

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v0

    add-int/2addr v0, v5

    int-to-float v0, v0

    sub-float v0, v2, v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v7, 0x0

    aget-object v2, v2, v7

    if-eqz v2, :cond_1b

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v7, 0x0

    aget-object v2, v2, v7

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    move v2, v3

    :goto_4
    float-to-int v2, v2

    float-to-int v3, v0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setHandlePos(Landroid/view/View;IIIILandroid/graphics/RectF;)V

    goto/16 :goto_0

    :cond_16
    int-to-float v8, v5

    add-float/2addr v8, v2

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v9

    int-to-float v9, v9

    cmpl-float v8, v8, v9

    if-lez v8, :cond_17

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v0

    add-int/2addr v0, v5

    int-to-float v0, v0

    sub-float v0, v2, v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v7, 0x2

    aget-object v2, v2, v7

    if-eqz v2, :cond_1b

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v7, 0x2

    aget-object v2, v2, v7

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    move v2, v3

    goto :goto_4

    :cond_17
    int-to-float v7, v4

    add-float/2addr v7, v3

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v0

    int-to-float v0, v0

    cmpl-float v0, v7, v0

    if-lez v0, :cond_18

    int-to-float v0, v4

    sub-float v0, v3, v0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v7, 0x1

    aget-object v3, v3, v7

    if-eqz v3, :cond_1a

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v7, 0x1

    aget-object v3, v3, v7

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    move v12, v2

    move v2, v0

    move v0, v12

    goto :goto_4

    :cond_18
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v7, 0x3

    aget-object v0, v0, v7

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    const/4 v7, 0x3

    aget-object v0, v0, v7

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_19
    move v0, v2

    move v2, v3

    goto :goto_4

    :cond_1a
    move v12, v2

    move v2, v0

    move v0, v12

    goto :goto_4

    :cond_1b
    move v2, v3

    goto :goto_4
.end method

.method private onDrawScrollBar(Landroid/graphics/Canvas;)V
    .locals 3

    const/4 v0, 0x0

    const-string/jumbo v1, "onDrawCursor()"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarVisible:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getScrollBarRect()Landroid/graphics/Rect;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v1, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    invoke-virtual {v1, v0}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempRectF:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method private onDrawSelect(Landroid/graphics/Canvas;)V
    .locals 13

    const/4 v5, 0x0

    const/4 v1, -0x1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsViewMode:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v0

    if-eq v0, v3, :cond_0

    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsTyping:Z

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    :cond_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    if-eqz v2, :cond_0

    if-le v3, v0, :cond_8

    move v2, v0

    :goto_1
    move v0, v1

    move v4, v5

    :goto_2
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    if-lt v4, v6, :cond_3

    move v12, v0

    move v0, v1

    :goto_3
    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "stringLength = "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getTextLength()I

    move-result v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v6, " startIndex = "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v6, " endIndex = "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "lineCount = "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v6, " startLine = "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v6, " endLine = "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    if-eq v12, v1, :cond_0

    if-eq v0, v1, :cond_0

    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    if-ne v12, v0, :cond_6

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->drawSelectRect(Landroid/graphics/Canvas;IILandroid/graphics/RectF;Landroid/graphics/Rect;)V

    goto/16 :goto_0

    :cond_3
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v6, v6, v4

    if-gt v6, v2, :cond_4

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v6, v6, v4

    if-lt v6, v2, :cond_4

    move v0, v4

    :cond_4
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v6, v6, v4

    add-int/lit8 v7, v3, -0x1

    if-gt v6, v7, :cond_5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v6, v6, v4

    add-int/lit8 v7, v3, -0x1

    if-lt v6, v7, :cond_5

    move v12, v0

    move v0, v4

    goto/16 :goto_3

    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_2

    :cond_6
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v1, v1, v12

    add-int/lit8 v9, v1, 0x1

    move-object v6, p0

    move-object v7, p1

    move v8, v2

    move-object v10, v4

    move-object v11, v5

    invoke-direct/range {v6 .. v11}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->drawSelectRect(Landroid/graphics/Canvas;IILandroid/graphics/RectF;Landroid/graphics/Rect;)V

    add-int/lit8 v1, v12, 0x1

    :goto_4
    if-lt v1, v0, :cond_7

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v2, v1, v0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->drawSelectRect(Landroid/graphics/Canvas;IILandroid/graphics/RectF;Landroid/graphics/Rect;)V

    goto/16 :goto_0

    :cond_7
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v8, v2, v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v9, v2, v1

    move-object v6, p0

    move-object v7, p1

    move-object v10, v4

    move-object v11, v5

    invoke-direct/range {v6 .. v11}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->drawSelectedLine(Landroid/graphics/Canvas;IILandroid/graphics/RectF;Landroid/graphics/Rect;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_8
    move v2, v3

    move v3, v0

    goto/16 :goto_1
.end method

.method private onMoveByKey(ILandroid/view/KeyEvent;)V
    .locals 7

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v1}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorPos()I

    move-result v2

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorRect(I)Landroid/graphics/Rect;

    move-result-object v3

    if-nez v3, :cond_1

    const-string/jumbo v0, "onMoveByKey().. cursorRect is null."

    invoke-static {v6, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v3}, Landroid/graphics/Rect;->centerY()I

    move-result v4

    int-to-float v4, v4

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineForVertical(F)I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    if-nez v5, :cond_3

    :cond_2
    const-string/jumbo v0, "onMoveByKey() Fail to measureText()."

    invoke-static {v6, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    goto :goto_0

    :cond_3
    sparse-switch p1, :sswitch_data_0

    :cond_4
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorPos()I

    move-result v0

    if-eq v2, v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    goto :goto_0

    :sswitch_0
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mShiftKeyPressed:Z

    if-eqz v0, :cond_5

    if-lez v4, :cond_4

    add-int/lit8 v0, v4, -0x1

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerX()I

    move-result v3

    int-to-float v3, v3

    invoke-direct {p0, v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v0

    invoke-virtual {p0, v0, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto :goto_1

    :cond_5
    if-lez v4, :cond_4

    add-int/lit8 v0, v4, -0x1

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerX()I

    move-result v1

    int-to-float v1, v1

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v0

    invoke-virtual {p0, v0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto :goto_1

    :sswitch_1
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mShiftKeyPressed:Z

    if-eqz v0, :cond_6

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    add-int/lit8 v0, v0, -0x1

    if-ge v4, v0, :cond_4

    add-int/lit8 v0, v4, 0x1

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerX()I

    move-result v3

    int-to-float v3, v3

    invoke-direct {p0, v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v0

    invoke-virtual {p0, v0, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto :goto_1

    :cond_6
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    add-int/lit8 v0, v0, -0x1

    if-ge v4, v0, :cond_4

    add-int/lit8 v0, v4, 0x1

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerX()I

    move-result v1

    int-to-float v1, v1

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v0

    invoke-virtual {p0, v0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto :goto_1

    :sswitch_2
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mAltKeyPressed:Z

    if-eqz v3, :cond_7

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v1, v1, v4

    aget-object v0, v0, v1

    iget v0, v0, Landroid/graphics/RectF;->left:F

    invoke-direct {p0, v4, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v0

    invoke-virtual {p0, v0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto :goto_1

    :cond_7
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mShiftKeyPressed:Z

    if-eqz v3, :cond_8

    if-lez v0, :cond_4

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto/16 :goto_1

    :cond_8
    if-lez v2, :cond_4

    if-eq v0, v1, :cond_9

    invoke-virtual {p0, v0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto/16 :goto_1

    :cond_9
    add-int/lit8 v0, v2, -0x1

    invoke-virtual {p0, v0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto/16 :goto_1

    :sswitch_3
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mAltKeyPressed:Z

    if-eqz v3, :cond_a

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v1, v1, v4

    aget-object v0, v0, v1

    iget v0, v0, Landroid/graphics/RectF;->right:F

    invoke-direct {p0, v4, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v0

    invoke-virtual {p0, v0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto/16 :goto_1

    :cond_a
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mShiftKeyPressed:Z

    if-eqz v3, :cond_b

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    if-lez v3, :cond_4

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    add-int/lit8 v4, v4, -0x1

    aget v3, v3, v4

    add-int/lit8 v3, v3, 0x1

    if-ge v0, v3, :cond_4

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto/16 :goto_1

    :cond_b
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    if-lez v3, :cond_4

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    add-int/lit8 v4, v4, -0x1

    aget v3, v3, v4

    add-int/lit8 v3, v3, 0x1

    if-ge v2, v3, :cond_4

    if-eq v0, v1, :cond_c

    invoke-virtual {p0, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto/16 :goto_1

    :cond_c
    add-int/lit8 v0, v2, 0x1

    invoke-virtual {p0, v0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto/16 :goto_1

    :sswitch_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    aget v1, v1, v4

    aget-object v0, v0, v1

    iget v0, v0, Landroid/graphics/RectF;->left:F

    invoke-direct {p0, v4, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v0

    invoke-virtual {p0, v0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto/16 :goto_1

    :sswitch_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    aget v1, v1, v4

    aget-object v0, v0, v1

    iget v0, v0, Landroid/graphics/RectF;->right:F

    invoke-direct {p0, v4, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v0

    invoke-virtual {p0, v0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto/16 :goto_1

    :sswitch_6
    invoke-virtual {p0, v6, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto/16 :goto_1

    :sswitch_7
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    if-lez v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_1
        0x15 -> :sswitch_2
        0x16 -> :sswitch_3
        0x5c -> :sswitch_6
        0x5d -> :sswitch_7
        0x7a -> :sswitch_4
        0x7b -> :sswitch_5
        0x91 -> :sswitch_5
        0x92 -> :sswitch_1
        0x93 -> :sswitch_7
        0x94 -> :sswitch_2
        0x96 -> :sswitch_3
        0x97 -> :sswitch_4
        0x98 -> :sswitch_0
        0x99 -> :sswitch_6
    .end sparse-switch
.end method

.method private relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V
    .locals 2

    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v0, v1

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->left:F

    iget v0, p2, Landroid/graphics/RectF;->right:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v0, v1

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->right:F

    iget v0, p2, Landroid/graphics/RectF;->top:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v0, v1

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v0, v1

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    return-void
.end method

.method private resize(FF)V
    .locals 8

    const/4 v7, 0x0

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v2

    if-nez v2, :cond_1

    const/4 v0, 0x0

    const-string/jumbo v1, "coordinateInfo is null."

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-eqz v3, :cond_0

    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-direct {p0, v3, v4, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    invoke-virtual {v0, v3}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    invoke-virtual {v1, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    iget v4, v1, Landroid/graphics/RectF;->top:F

    add-float/2addr v4, p2

    iput v4, v1, Landroid/graphics/RectF;->bottom:F

    iget v4, v1, Landroid/graphics/RectF;->left:F

    add-float/2addr v4, p1

    iput v4, v1, Landroid/graphics/RectF;->right:F

    invoke-virtual {v3, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRotation()F

    move-result v4

    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setRotation(F)V

    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5}, Landroid/graphics/RectF;-><init>()V

    cmpl-float v4, v4, v7

    if-nez v4, :cond_3

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setPivotX(F)V

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setPivotY(F)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setPivotX(F)V

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setPivotY(F)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMatrix()Landroid/graphics/Matrix;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    iget v4, v1, Landroid/graphics/RectF;->left:F

    iget v6, v0, Landroid/graphics/RectF;->left:F

    sub-float/2addr v4, v6

    iget v0, v0, Landroid/graphics/RectF;->top:F

    iget v1, v1, Landroid/graphics/RectF;->top:F

    sub-float/2addr v0, v1

    invoke-virtual {v3, v4, v0}, Landroid/graphics/RectF;->offset(FF)V

    :cond_2
    :goto_1
    invoke-direct {p0, v5, v3, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    const/4 v1, 0x1

    invoke-virtual {v0, v5, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setRect(Landroid/graphics/RectF;Z)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onObjectChanged()V

    goto :goto_0

    :cond_3
    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v4

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v6

    cmpl-float v4, v4, v6

    if-nez v4, :cond_4

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    move-result v0

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerY()F

    move-result v1

    sub-float/2addr v0, v1

    invoke-virtual {v3, v7, v0}, Landroid/graphics/RectF;->offset(FF)V

    goto :goto_1

    :cond_4
    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v4

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v6

    cmpl-float v4, v4, v6

    if-nez v4, :cond_2

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v0

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    sub-float/2addr v0, v1

    invoke-virtual {v3, v0, v7}, Landroid/graphics/RectF;->offset(FF)V

    goto :goto_1
.end method

.method private setChangeWatcher()V
    .locals 7

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v4}, Landroid/text/Spannable;->length()I

    move-result v0

    const-class v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;

    invoke-interface {v4, v2, v0, v1}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;

    array-length v5, v0

    move v1, v2

    :goto_1
    if-lt v1, v5, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mChangeWatcher:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;

    if-nez v0, :cond_2

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mChangeWatcher:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "setChangeWatcher() textLength = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mChangeWatcher:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;

    const v1, 0x640012

    invoke-interface {v4, v0, v2, v3, v1}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeyListener:Landroid/text/method/KeyListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeyListener:Landroid/text/method/KeyListener;

    const/16 v1, 0x12

    invoke-interface {v4, v0, v2, v3, v1}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    :cond_3
    aget-object v6, v0, v1

    invoke-interface {v4, v6}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private setHandlePos(Landroid/view/View;IIIILandroid/graphics/RectF;)V
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "setHandlePos() x = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", y = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", relativeObjectRect.top = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p6, Landroid/graphics/RectF;->top:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", relativeObjectRect.bottom = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p6, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {v2}, Landroid/graphics/Matrix;->reset()V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRotation()F

    move-result v3

    invoke-virtual {p6}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    invoke-virtual {p6}, Landroid/graphics/RectF;->centerY()F

    move-result v5

    invoke-virtual {v2, v3, v4, v5}, Landroid/graphics/Matrix;->setRotate(FFF)V

    const/4 v3, 0x2

    new-array v3, v3, [F

    int-to-float v4, p2

    aput v4, v3, v6

    int-to-float v4, p3

    aput v4, v3, v8

    invoke-virtual {v2, v3}, Landroid/graphics/Matrix;->mapPoints([F)V

    aget v2, v3, v6

    float-to-int v2, v2

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v2

    iget v4, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    add-int/2addr v4, p4

    sub-int/2addr v2, v4

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    aget v2, v3, v8

    float-to-int v2, v2

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v0

    iget v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    add-int/2addr v2, p5

    sub-int/2addr v0, v2

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    iput p4, v1, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    iput p5, v1, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    invoke-virtual {p1, v7}, Landroid/view/View;->setPivotX(F)V

    invoke-virtual {p1, v7}, Landroid/view/View;->setPivotY(F)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRotation()F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setRotation(F)V

    invoke-virtual {p1, v6, v6, v6, v6}, Landroid/view/View;->setPadding(IIII)V

    invoke-virtual {p1, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p1, v6}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1}, Landroid/view/View;->bringToFront()V

    goto :goto_0
.end method

.method private setParagraphAlign(I)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getParagraph()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;

    invoke-direct {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;-><init>()V

    iput v2, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;->startPos:I

    iput v2, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;->endPos:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    iput v2, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;->endPos:I

    :cond_2
    iput p1, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;->align:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setParagraph(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private setParagraphIndent(I)V
    .locals 0

    return-void
.end method

.method private setParagraphSpacing(IF)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getParagraph()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    invoke-direct {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;-><init>()V

    iput v2, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->startPos:I

    iput v2, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->endPos:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    iput v2, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->endPos:I

    :cond_2
    iput p1, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->type:I

    iput p2, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->lineSpacing:F

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setParagraph(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private setTextBackgroundColor(III)V
    .locals 4

    const/4 v3, 0x3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;

    invoke-direct {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;-><init>()V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v2

    iput v0, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    if-nez v2, :cond_1

    :goto_1
    iput v0, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    iput v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    :goto_2
    move-object v0, v1

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;

    iput p1, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;->backgroundColor:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->appendSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_1

    :cond_2
    iput p2, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    iput p3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    iput v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    goto :goto_2
.end method

.method private setTextBold(ZII)V
    .locals 4

    const/4 v3, 0x3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BoldStyleSpanInfo;

    invoke-direct {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BoldStyleSpanInfo;-><init>()V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v2

    iput v0, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    if-nez v2, :cond_1

    :goto_1
    iput v0, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    iput v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    :goto_2
    move-object v0, v1

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BoldStyleSpanInfo;

    iput-boolean p1, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BoldStyleSpanInfo;->isBold:Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->appendSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_1

    :cond_2
    iput p2, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    iput p3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    iput v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    goto :goto_2
.end method

.method private setTextDirection(III)V
    .locals 4

    const/4 v3, 0x3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;

    invoke-direct {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;-><init>()V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v2

    iput v0, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    if-nez v2, :cond_1

    :goto_1
    iput v0, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    iput v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    :goto_2
    move-object v0, v1

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;

    iput p1, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;->textDirection:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->appendSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_1

    :cond_2
    iput p2, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    iput p3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    iput v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    goto :goto_2
.end method

.method private setTextFontColor(III)V
    .locals 5

    const/4 v4, 0x3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getSpan()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    :goto_1
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;

    invoke-direct {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;-><init>()V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v0

    iput v2, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    if-nez v0, :cond_4

    move v0, v2

    :goto_2
    iput v0, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    iput v4, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    :goto_3
    move-object v0, v1

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;

    iput p1, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;->foregroundColor:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->appendSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V

    goto :goto_0

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    instance-of v1, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;

    if-eqz v1, :cond_1

    iget v1, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    if-ne v1, p2, :cond_1

    iget v1, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    if-ne v1, p3, :cond_1

    move-object v1, v0

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;->foregroundColor:I

    if-eq v1, p1, :cond_1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->removeSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V

    goto :goto_1

    :cond_4
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_2

    :cond_5
    iput p2, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    iput p3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    iput v4, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    goto :goto_3
.end method

.method private setTextFontName(Ljava/lang/String;II)V
    .locals 4

    const/4 v3, 0x3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;

    invoke-direct {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;-><init>()V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v2

    iput v0, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    if-nez v2, :cond_1

    :goto_1
    iput v0, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    iput v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    :goto_2
    move-object v0, v1

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;

    iput-object p1, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;->fontName:Ljava/lang/String;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->appendSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_1

    :cond_2
    iput p2, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    iput p3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    iput v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    goto :goto_2
.end method

.method private setTextFontSize(FII)V
    .locals 4

    const/4 v3, 0x3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    invoke-direct {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;-><init>()V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v2

    iput v0, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    if-nez v2, :cond_1

    :goto_1
    iput v0, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    iput v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    :goto_2
    move-object v0, v1

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    iput p1, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->appendSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_1

    :cond_2
    iput p2, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    iput p3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    iput v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    goto :goto_2
.end method

.method private setTextItalic(ZII)V
    .locals 4

    const/4 v3, 0x3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;

    invoke-direct {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;-><init>()V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v2

    iput v0, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    if-nez v2, :cond_1

    :goto_1
    iput v0, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    iput v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    :goto_2
    move-object v0, v1

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;

    iput-boolean p1, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;->isItalic:Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->appendSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_1

    :cond_2
    iput p2, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    iput p3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    iput v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    goto :goto_2
.end method

.method private setTextUnderline(ZII)V
    .locals 4

    const/4 v3, 0x3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$UnderlineStyleSpanInfo;

    invoke-direct {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$UnderlineStyleSpanInfo;-><init>()V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v2

    iput v0, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    if-nez v2, :cond_1

    :goto_1
    iput v0, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    iput v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    :goto_2
    move-object v0, v1

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$UnderlineStyleSpanInfo;

    iput-boolean p1, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$UnderlineStyleSpanInfo;->isUnderline:Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->appendSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_1

    :cond_2
    iput p2, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    iput p3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    iput v3, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->intervalType:I

    goto :goto_2
.end method

.method private shouldBlink()Z
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isCursorVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private showCursorHandle()V
    .locals 6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;

    if-nez v0, :cond_0

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x1388

    add-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$HandleDuration;->postAtTime(Ljava/lang/Runnable;J)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorHandleVisible:Z

    return-void
.end method

.method private showScrollBar()V
    .locals 6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;

    if-nez v0, :cond_0

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarDuration:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x1f4

    add-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;->postAtTime(Ljava/lang/Runnable;J)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScrollBarVisible:Z

    return-void
.end method

.method private updateContextmenu()V
    .locals 8

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->mDirtyFlag:Z

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenuVisible:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->reset()V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "onSelectionChange : parentRect"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    iget v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget v4, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v4

    iput v1, v3, Landroid/graphics/Rect;->left:I

    iget v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget v4, v2, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v4

    iput v1, v3, Landroid/graphics/Rect;->top:I

    iget v1, v3, Landroid/graphics/Rect;->left:I

    iget v4, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    add-int/2addr v1, v4

    iput v1, v3, Landroid/graphics/Rect;->right:I

    iget v1, v3, Landroid/graphics/Rect;->top:I

    iget v0, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    add-int/2addr v0, v1

    iput v0, v3, Landroid/graphics/Rect;->bottom:I

    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "onSelectionChange : textRect "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v1}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v1

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorRect(I)Landroid/graphics/Rect;

    move-result-object v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "startCursorRect is null."

    invoke-static {v7, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorRect(I)Landroid/graphics/Rect;

    move-result-object v1

    if-nez v1, :cond_2

    const-string/jumbo v0, "endCursorRect is null."

    invoke-static {v7, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    goto :goto_0

    :cond_2
    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getPan(I)F

    move-result v4

    iget v5, v3, Landroid/graphics/Rect;->left:I

    iget v6, v3, Landroid/graphics/Rect;->top:I

    int-to-float v6, v6

    sub-float/2addr v6, v4

    float-to-int v6, v6

    invoke-virtual {v0, v5, v6}, Landroid/graphics/Rect;->offset(II)V

    iget v5, v3, Landroid/graphics/Rect;->left:I

    iget v3, v3, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    sub-float/2addr v3, v4

    float-to-int v3, v3

    invoke-virtual {v1, v5, v3}, Landroid/graphics/Rect;->offset(II)V

    iget v3, v0, Landroid/graphics/Rect;->top:I

    add-int/lit16 v3, v3, -0x190

    iget v4, v2, Landroid/graphics/Rect;->top:I

    if-le v3, v4, :cond_3

    const/16 v1, -0x190

    invoke-virtual {v0, v7, v1}, Landroid/graphics/Rect;->offset(II)V

    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "updateContextmenu() : rect "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v7, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->setRect(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    const/16 v1, 0x1388

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->show(I)V

    goto :goto_0

    :cond_3
    iget v3, v1, Landroid/graphics/Rect;->bottom:I

    add-int/lit16 v3, v3, 0x140

    iget v4, v2, Landroid/graphics/Rect;->bottom:I

    if-ge v3, v4, :cond_4

    const/16 v0, 0xc8

    invoke-virtual {v1, v7, v0}, Landroid/graphics/Rect;->offset(II)V

    move-object v0, v1

    goto :goto_1

    :cond_4
    iget v1, v2, Landroid/graphics/Rect;->top:I

    iput v1, v0, Landroid/graphics/Rect;->top:I

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenuVisible:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isContextMenuShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->hide()V

    goto :goto_0
.end method

.method private updateSelection()V
    .locals 7

    const/4 v5, -0x1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Landroid/view/inputmethod/InputMethodManager;->isActive(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v1}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v2

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v1}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v3

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->getComposingSpanStart(Landroid/text/Spannable;)I

    move-result v4

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->getComposingSpanEnd(Landroid/text/Spannable;)I

    move-result v5

    :goto_0
    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Landroid/view/inputmethod/InputMethodManager;->updateSelection(Landroid/view/View;IIII)V

    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "updateSelection selStart = "

    invoke-direct {v1, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", selEnd = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", candStart = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", candEnd = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    move v4, v5

    goto :goto_0
.end method

.method private updateSettingInfo()V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v0

    invoke-direct {p0, v0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getSettingInfo(II)Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;->onSettingTextInfoChanged(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    goto :goto_0
.end method


# virtual methods
.method public appendText(Ljava/lang/String;)V
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v1}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextLimit:I

    const/16 v4, 0x1388

    if-eq v3, v4, :cond_1

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextLimit:I

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v4

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    if-le v3, v4, :cond_0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v3, v1, v2, p1}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    :goto_0
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextEraserEnable:Z

    if-eqz v1, :cond_2

    :goto_1
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorVisible:Z

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    return-void

    :cond_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextLimit:I

    sub-int/2addr v4, v1

    invoke-virtual {p1, v0, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v1, v2, v4}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    goto :goto_0

    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v3, v1, v2, p1}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public close()V
    .locals 5

    const/4 v1, 0x0

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideSoftInput()V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mInputMethodChangedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    move v2, v1

    :goto_0
    const/4 v3, 0x3

    if-lt v2, v3, :cond_4

    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_finalize(I)V

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    :cond_1
    move v0, v1

    :goto_1
    const/4 v2, 0x6

    if-lt v0, v2, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->hide()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->close()V

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v0}, Landroid/text/Editable;->getFilters()[Landroid/text/InputFilter;

    move-result-object v0

    if-eqz v0, :cond_3

    aget-object v0, v0, v1

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;->close()V

    :cond_3
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextView:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    return-void

    :cond_4
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    aget-object v3, v3, v2

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    aget-object v2, v2, v0

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleBitmap:[Landroid/graphics/Bitmap;

    aput-object v4, v2, v0

    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z
    .locals 1

    invoke-super {p0, p1}, Landroid/view/View;->dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public enableTextInput(Z)V
    .locals 2

    const/4 v1, 0x1

    if-eqz p1, :cond_0

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setFocusableInTouchMode(Z)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setVisibility(I)V

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setFocusable(Z)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->requestFocus()Z

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextEraserEnable:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showSoftInput()V

    :cond_0
    return-void
.end method

.method public enableTouch(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTouchEnable:Z

    return-void
.end method

.method public fit(Z)V
    .locals 10

    const/high16 v9, 0x40000000    # 2.0f

    const/4 v8, 0x0

    const/4 v7, 0x0

    const-string/jumbo v0, "fit() is called."

    invoke-static {v7, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    if-nez v0, :cond_1

    const-string/jumbo v0, "fit() mNativeTextView is invalid."

    invoke-static {v7, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_2

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->adjustTextBox()V

    :cond_2
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v2

    if-nez v2, :cond_3

    const-string/jumbo v0, "coordinateInfo is null."

    invoke-static {v7, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v3

    if-nez v3, :cond_4

    const-string/jumbo v0, "objectRect is null."

    invoke-static {v7, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    goto :goto_0

    :cond_4
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    invoke-direct {p0, v4, v3, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    iget v5, v4, Landroid/graphics/RectF;->left:F

    float-to-int v5, v5

    iput v5, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget v5, v4, Landroid/graphics/RectF;->top:F

    float-to-int v5, v5

    iput v5, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget v5, v4, Landroid/graphics/RectF;->left:F

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v6

    add-float/2addr v5, v6

    sub-float/2addr v0, v5

    float-to-int v0, v0

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v0

    float-to-int v0, v0

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v0

    float-to-int v0, v0

    iput v0, v1, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v5

    float-to-int v5, v5

    if-ne v0, v5, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v5

    float-to-int v5, v5

    if-eq v0, v5, :cond_9

    :cond_5
    const-string/jumbo v0, "fit() Bitmap is recycled."

    invoke-static {v7, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v0

    float-to-double v5, v0

    invoke-static {v5, v6}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v5

    double-to-int v0, v5

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    float-to-double v5, v3

    invoke-static {v5, v6}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v5

    double-to-int v3, v5

    if-lez v0, :cond_6

    if-lez v3, :cond_6

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v3, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    invoke-static {v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_setBitmap(ILandroid/graphics/Bitmap;)Z

    :cond_6
    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "fit() bitmap width = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v5, ", object height = "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    :cond_7
    :goto_1
    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_8
    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v0

    div-float/2addr v0, v9

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setPivotX(F)V

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v0

    div-float/2addr v0, v9

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setPivotY(F)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRotation()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setRotation(F)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    iget v1, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    iget v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    invoke-virtual {v0, v1, v2, v8, v8}, Landroid/graphics/Matrix;->setScale(FFFF)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_update(I)Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBlink:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->restartBlink()V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->invalidate()V

    goto/16 :goto_0

    :cond_9
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v7, v7}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v3

    invoke-virtual {v0, v7, v7, v3}, Landroid/graphics/Bitmap;->setPixel(III)V

    goto :goto_1

    :cond_a
    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v0

    float-to-double v5, v0

    invoke-static {v5, v6}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v5

    double-to-int v0, v5

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    float-to-double v5, v3

    invoke-static {v5, v6}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v5

    double-to-int v3, v5

    if-lez v0, :cond_7

    if-lez v3, :cond_7

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v3, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_setBitmap(ILandroid/graphics/Bitmap;)Z

    goto :goto_1
.end method

.method public getCursorPos()I
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v0

    goto :goto_0
.end method

.method public getDefaultHeight()I
    .locals 7

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v0, :cond_0

    move v0, v4

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getTopMargin()F

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getBottomMargin()F

    move-result v1

    add-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v2

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v5

    invoke-virtual {v0, v2, v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->findSpans(II)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_5

    :cond_2
    move v2, v1

    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getParagraph()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_6

    :cond_4
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v0

    if-nez v0, :cond_8

    const-string/jumbo v0, "coordinateInfo is null."

    invoke-static {v4, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    move v0, v4

    goto :goto_0

    :cond_5
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    instance-of v5, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    if-eqz v5, :cond_1

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    move v2, v0

    goto :goto_1

    :cond_6
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;

    instance-of v1, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    if-eqz v1, :cond_3

    move-object v1, v0

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->type:I

    const/4 v6, 0x1

    if-ne v1, v6, :cond_7

    int-to-float v1, v3

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->lineSpacing:F

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    move v3, v0

    goto :goto_2

    :cond_7
    int-to-float v1, v3

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->lineSpacing:F

    add-float/2addr v0, v1

    float-to-int v0, v0

    move v3, v0

    goto :goto_2

    :cond_8
    int-to-float v1, v3

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    goto/16 :goto_0
.end method

.method public getDefaultWidth()I
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getLeftMargin()F

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRightMargin()F

    move-result v2

    add-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v3

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getTextLength()I

    move-result v4

    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5}, Landroid/graphics/RectF;-><init>()V

    move v2, v1

    :goto_1
    if-lt v2, v4, :cond_1

    const/high16 v2, 0x40800000    # 4.0f

    add-float/2addr v0, v2

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v2

    if-nez v2, :cond_3

    const-string/jumbo v0, "coordinateInfo is null."

    invoke-static {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    move v0, v1

    goto :goto_0

    :cond_1
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v6, v2, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getTextRect(IILandroid/graphics/RectF;)Z

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v6

    cmpg-float v6, v0, v6

    if-gez v6, :cond_2

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v0

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    int-to-float v1, v3

    add-float/2addr v0, v1

    iget v1, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    goto :goto_0
.end method

.method public getMaximumHeight()F
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getHeight(I)I

    move-result v0

    int-to-float v0, v0

    return v0
.end method

.method public getMaximumWidth()F
    .locals 17

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v8

    if-nez v8, :cond_1

    const/4 v1, 0x0

    const-string/jumbo v2, "coordinateInfo is null."

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v1, v8}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    const/4 v1, 0x0

    const-string/jumbo v2, "checkForHorizontalScroll() start"

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    new-instance v9, Landroid/graphics/RectF;

    invoke-direct {v9}, Landroid/graphics/RectF;-><init>()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getLeftMargin()F

    move-result v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRightMargin()F

    move-result v2

    add-float/2addr v2, v1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->isHintTextEnabled()Z

    move-result v1

    if-eqz v1, :cond_3

    if-eqz v11, :cond_2

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_3

    :cond_2
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getHintTextWidth(I)F

    move-result v1

    add-float/2addr v1, v2

    :goto_1
    iget v2, v8, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float v4, v2, v1

    if-eqz v11, :cond_c

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v12

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "checkForHorizontalScroll() length = "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    const/4 v2, 0x0

    move v7, v2

    move v3, v4

    move v6, v1

    move v5, v1

    move v2, v4

    :goto_2
    if-lt v7, v12, :cond_7

    :goto_3
    float-to-double v1, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v1

    double-to-int v1, v1

    int-to-float v1, v1

    goto/16 :goto_0

    :cond_3
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getLineCount(I)I

    move-result v1

    if-nez v1, :cond_6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->findSpans(II)Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_d

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_5

    move v1, v2

    goto :goto_1

    :cond_5
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    instance-of v4, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    if-eqz v4, :cond_4

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    add-float/2addr v1, v2

    goto :goto_1

    :cond_6
    const/high16 v1, 0x40a00000    # 5.0f

    add-float/2addr v1, v2

    goto :goto_1

    :cond_7
    invoke-virtual {v11, v7}, Ljava/lang/String;->charAt(I)C

    move-result v13

    const/16 v14, 0xa

    if-ne v13, v14, :cond_a

    move v3, v4

    move v6, v1

    :goto_4
    cmpl-float v13, v6, v5

    if-lez v13, :cond_8

    move v5, v6

    :cond_8
    cmpl-float v13, v3, v2

    if-lez v13, :cond_9

    move v2, v3

    :cond_9
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    :cond_a
    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v13, v7, v9}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getTextRect(IILandroid/graphics/RectF;)Z

    iget v13, v8, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    invoke-virtual {v9}, Landroid/graphics/RectF;->width()F

    move-result v14

    mul-float/2addr v13, v14

    iget v14, v10, Landroid/graphics/RectF;->left:F

    add-float/2addr v14, v6

    invoke-virtual {v9}, Landroid/graphics/RectF;->width()F

    move-result v15

    add-float/2addr v14, v15

    move-object/from16 v0, p0

    iget v15, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_WIDTH:F

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_RIGHT_MARGIN:I

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    sub-float v15, v15, v16

    const/high16 v16, 0x40800000    # 4.0f

    sub-float v15, v15, v16

    cmpg-float v14, v14, v15

    if-gez v14, :cond_b

    invoke-virtual {v9}, Landroid/graphics/RectF;->width()F

    move-result v14

    add-float/2addr v6, v14

    add-float/2addr v3, v13

    goto :goto_4

    :cond_b
    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->MAX_OBJECT_WIDTH:F

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_RIGHT_MARGIN:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    const/high16 v2, 0x40800000    # 4.0f

    sub-float/2addr v1, v2

    iget v2, v10, Landroid/graphics/RectF;->left:F

    sub-float/2addr v1, v2

    iget v2, v8, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v2, v1

    goto/16 :goto_3

    :cond_c
    move v2, v4

    goto/16 :goto_3

    :cond_d
    move v1, v2

    goto/16 :goto_1
.end method

.method public getObjectText()Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    return-object v0
.end method

.method public getPixel(II)I
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    int-to-float v1, p1

    int-to-float v2, p2

    invoke-direct {p0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getAbsolutePoint(FF)[F

    move-result-object v1

    if-nez v1, :cond_1

    const-string/jumbo v1, "pts is null."

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getPan(I)F

    move-result v2

    aget v3, v1, v0

    float-to-int v3, v3

    const/4 v4, 0x1

    aget v1, v1, v4

    sub-float/2addr v1, v2

    float-to-int v1, v1

    if-ltz v3, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    if-ge v3, v2, :cond_0

    if-ltz v1, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v3, v1}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v0

    goto :goto_0
.end method

.method public getText(Z)Ljava/lang/String;
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v1}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v2

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v1}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v3

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    if-eqz p1, :cond_1

    if-eqz v1, :cond_0

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public getTextLimit()I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextLimit:I

    return v0
.end method

.method public hideSoftInput()V
    .locals 3

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    return-void
.end method

.method public hideTextBox()V
    .locals 1

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setVisibility(I)V

    return-void
.end method

.method public isAccessoryKeyboardState()I
    .locals 5

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v2, "input_method"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    :try_start_0
    const-string/jumbo v3, "isAccessoryKeyboardState"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v2

    const/4 v3, 0x0

    :try_start_1
    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v2, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_2

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    :goto_1
    move v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V
    :try_end_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_1

    :catch_3
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V
    :try_end_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_1
.end method

.method public isContextMenuShowing()Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->isShowing()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isContextMenuVisible()Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenuVisible:Z

    return v0
.end method

.method public isTextInputable()Z
    .locals 1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v0

    return v0
.end method

.method public isTouchEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTouchEnable:Z

    return v0
.end method

.method public isViewModeEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsViewMode:Z

    return v0
.end method

.method public measureText()V
    .locals 9

    const/4 v5, 0x1

    const/4 v7, 0x0

    const/4 v1, 0x0

    const-string/jumbo v0, "measureText() start.."

    invoke-static {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    if-nez v0, :cond_1

    const-string/jumbo v0, "measureText() mNativeTextView is invalid."

    invoke-static {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v2

    float-to-int v2, v2

    invoke-static {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_measure(II)Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getGravity()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getDeltaY(I)F

    move-result v2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_14

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    :goto_1
    if-lez v4, :cond_10

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getLineCount(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    new-array v0, v0, [Landroid/graphics/PointF;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLinePosition:[Landroid/graphics/PointF;

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    new-array v0, v4, [Landroid/graphics/RectF;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    new-array v0, v4, [F

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextSize:[F

    new-array v0, v4, [I

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextDirection:[I

    new-array v0, v4, [Z

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextItalic:[Z

    move v0, v1

    :goto_2
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I

    if-lt v0, v3, :cond_3

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getLineStartIndex(II)I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_4

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsFirstCharLF:Z

    :goto_3
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getPan(I)F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mDeltaY:F

    move v0, v1

    :goto_4
    if-lt v0, v4, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getSpan()Ljava/util/ArrayList;

    move-result-object v7

    if-eqz v7, :cond_2

    move v2, v1

    :goto_5
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v2, v0, :cond_6

    :cond_2
    :goto_6
    const-string/jumbo v0, "measureText() end.."

    invoke-static {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    goto/16 :goto_0

    :cond_3
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLinePosition:[Landroid/graphics/PointF;

    new-instance v6, Landroid/graphics/PointF;

    invoke-direct {v6}, Landroid/graphics/PointF;-><init>()V

    aput-object v6, v3, v0

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLinePosition:[Landroid/graphics/PointF;

    aget-object v6, v6, v0

    invoke-static {v3, v0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getLinePosition(IILandroid/graphics/PointF;)Z

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLinePosition:[Landroid/graphics/PointF;

    aget-object v3, v3, v0

    invoke-virtual {v3, v7, v2}, Landroid/graphics/PointF;->offset(FF)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mStartIndex:[I

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v6, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getLineStartIndex(II)I

    move-result v6

    aput v6, v3, v0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    invoke-static {v6, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getLineEndIndex(II)I

    move-result v6

    aput v6, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsFirstCharLF:Z

    goto :goto_3

    :cond_5
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    new-instance v6, Landroid/graphics/RectF;

    invoke-direct {v6}, Landroid/graphics/RectF;-><init>()V

    aput-object v6, v3, v0

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v6, v6, v0

    invoke-static {v3, v0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getTextRect(IILandroid/graphics/RectF;)Z

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;

    aget-object v3, v3, v0

    invoke-virtual {v3, v7, v2}, Landroid/graphics/RectF;->offset(FF)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextDirection:[I

    aput v1, v3, v0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextItalic:[Z

    aput-boolean v1, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    if-eqz v0, :cond_7

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    iget v6, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    if-le v0, v4, :cond_13

    move v3, v4

    :goto_7
    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    iget v8, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    move v0, v6

    :goto_8
    if-lt v0, v3, :cond_9

    :cond_7
    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;

    if-eqz v0, :cond_c

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    iget v3, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    if-le v0, v4, :cond_12

    move v6, v4

    :goto_9
    if-gt v3, v4, :cond_8

    if-le v6, v4, :cond_b

    :cond_8
    const-string/jumbo v0, "Invalid Span Info"

    invoke-static {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    goto/16 :goto_6

    :cond_9
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextSize:[F

    aput v8, v6, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_a
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextDirection:[I

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;->textDirection:I

    aput v0, v8, v3

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    :cond_b
    if-lt v3, v6, :cond_a

    :cond_c
    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;

    if-eqz v0, :cond_d

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    iget v6, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    if-le v0, v4, :cond_11

    move v3, v4

    :goto_a
    if-lt v6, v3, :cond_e

    :cond_d
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_5

    :cond_e
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextItalic:[Z

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;->isItalic:Z

    if-eqz v0, :cond_f

    move v0, v5

    :goto_b
    aput-boolean v0, v8, v6

    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_a

    :cond_f
    move v0, v1

    goto :goto_b

    :cond_10
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->initMeasureInfo()V

    goto/16 :goto_6

    :cond_11
    move v3, v0

    goto :goto_a

    :cond_12
    move v6, v0

    goto :goto_9

    :cond_13
    move v3, v0

    goto/16 :goto_7

    :cond_14
    move v4, v1

    goto/16 :goto_1
.end method

.method public onCheckIsTextEditor()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCurrentOrientation:I

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->setDirty()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->isShowing()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->reset()V

    :cond_0
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCurrentOrientation:I

    :cond_1
    invoke-super {p0, p1}, Landroid/view/View;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 7

    const/4 v0, 0x4

    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v1, 0x0

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getIMEActionType()I

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getTextInputType()I

    move-result v6

    packed-switch v5, :pswitch_data_0

    move v5, v4

    :goto_0
    packed-switch v6, :pswitch_data_1

    move v0, v4

    :goto_1
    :pswitch_0
    const/4 v1, 0x0

    iput-object v1, p1, Landroid/view/inputmethod/EditorInfo;->actionLabel:Ljava/lang/CharSequence;

    const-string/jumbo v1, "SPenSDK"

    iput-object v1, p1, Landroid/view/inputmethod/EditorInfo;->label:Ljava/lang/CharSequence;

    iget v1, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    const/high16 v2, 0x10000000

    or-int/2addr v2, v5

    or-int/2addr v1, v2

    iput v1, p1, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    const-string/jumbo v1, "inputType=DisableEmoji"

    iput-object v1, p1, Landroid/view/inputmethod/EditorInfo;->privateImeOptions:Ljava/lang/String;

    or-int/lit16 v0, v0, 0x4000

    iput v0, p1, Landroid/view/inputmethod/EditorInfo;->inputType:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mInputConnection:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;

    if-nez v0, :cond_0

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;

    invoke-direct {v0, p0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Landroid/view/View;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mInputConnection:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v0

    iput v0, p1, Landroid/view/inputmethod/EditorInfo;->initialSelStart:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v0

    iput v0, p1, Landroid/view/inputmethod/EditorInfo;->initialSelEnd:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mInputConnection:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;

    return-object v0

    :pswitch_1
    const/4 v5, 0x6

    goto :goto_0

    :pswitch_2
    move v5, v2

    goto :goto_0

    :pswitch_3
    const/4 v5, 0x5

    goto :goto_0

    :pswitch_4
    move v5, v4

    goto :goto_0

    :pswitch_5
    move v5, v3

    goto :goto_0

    :pswitch_6
    move v5, v0

    goto :goto_0

    :pswitch_7
    move v5, v1

    goto :goto_0

    :pswitch_8
    move v0, v1

    goto :goto_1

    :pswitch_9
    move v0, v2

    goto :goto_1

    :pswitch_a
    move v0, v3

    goto :goto_1

    :pswitch_b
    move v0, v4

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_7
        :pswitch_4
        :pswitch_2
        :pswitch_5
        :pswitch_1
        :pswitch_6
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_8
        :pswitch_b
        :pswitch_9
        :pswitch_a
        :pswitch_0
    .end packed-switch
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBlink:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->stopBlink()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTyping:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;->stopInput()V

    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    const/4 v1, 0x0

    const-string/jumbo v0, "onDraw()"

    invoke-static {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mFirstDraw:Z

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mFirstDraw:Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mUpdateHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mRequestObjectChange:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    if-nez v0, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onDrawSelect(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onDrawHandle()V

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onDrawCursor(Landroid/graphics/Canvas;)V

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onDrawScrollBar(Landroid/graphics/Canvas;)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateContextmenu()V

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;->onFocusChanged(Z)V

    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mShowCursor:J

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBlink:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->startBlink()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v2, v2, v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_command(IILjava/util/ArrayList;I)Ljava/util/ArrayList;

    :cond_1
    :goto_0
    invoke-super {p0, p1, p2, p3}, Landroid/view/View;->onFocusChanged(ZILandroid/graphics/Rect;)V

    return-void

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBlink:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->stopBlink()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTyping:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;->stopInput()V

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 5

    const/4 v2, 0x1

    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "onKeyDown() keyCode = "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v0

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    :sswitch_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBlink:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->restartBlink()V

    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onMoveByKey(ILandroid/view/KeyEvent;)V

    move v0, v2

    goto :goto_0

    :sswitch_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    const-string/jumbo v4, "\n"

    invoke-interface {v3, v1, v0, v4}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    move v0, v2

    goto :goto_0

    :sswitch_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    const-string/jumbo v4, " "

    invoke-interface {v3, v1, v0, v4}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    move v0, v2

    goto :goto_0

    :sswitch_3
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    const/16 v4, 0x9

    invoke-static {v4}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v1, v0, v4}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    move v0, v2

    goto :goto_0

    :sswitch_4
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsDeletedText:Z

    if-ne v1, v0, :cond_1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getTextLength()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    add-int/lit8 v3, v1, 0x1

    invoke-interface {v0, v1, v3}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    :cond_0
    :goto_1
    move v0, v2

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    if-ge v1, v0, :cond_2

    move v3, v1

    :goto_2
    if-ge v1, v0, :cond_3

    :goto_3
    invoke-interface {v4, v3, v0}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    goto :goto_1

    :cond_2
    move v3, v0

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_3

    :sswitch_5
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mAltKeyPressed:Z

    move v0, v2

    goto :goto_0

    :sswitch_6
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mShiftKeyPressed:Z

    move v0, v2

    goto :goto_0

    :sswitch_7
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isNumLockOn()Z

    move-result v0

    if-eqz v0, :cond_4

    add-int/lit16 v0, p1, -0x90

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->appendText(Ljava/lang/String;)V

    :goto_4
    move v0, v2

    goto :goto_0

    :cond_4
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onMoveByKey(ILandroid/view/KeyEvent;)V

    goto :goto_4

    nop

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_0
        0x15 -> :sswitch_0
        0x16 -> :sswitch_0
        0x39 -> :sswitch_5
        0x3a -> :sswitch_5
        0x3b -> :sswitch_6
        0x3c -> :sswitch_6
        0x3d -> :sswitch_3
        0x3e -> :sswitch_2
        0x42 -> :sswitch_1
        0x5c -> :sswitch_0
        0x5d -> :sswitch_0
        0x70 -> :sswitch_4
        0x7a -> :sswitch_0
        0x7b -> :sswitch_0
        0x90 -> :sswitch_7
        0x91 -> :sswitch_7
        0x92 -> :sswitch_7
        0x93 -> :sswitch_7
        0x94 -> :sswitch_7
        0x95 -> :sswitch_7
        0x96 -> :sswitch_7
        0x97 -> :sswitch_7
        0x98 -> :sswitch_7
        0x99 -> :sswitch_7
    .end sparse-switch
.end method

.method public onKeyShortcut(ILandroid/view/KeyEvent;)Z
    .locals 11

    const/16 v9, 0x96

    const/16 v8, 0x50

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v3, "clipboard"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v3}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v4}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v4

    if-le v3, v4, :cond_10

    :goto_0
    sparse-switch p1, :sswitch_data_0

    :cond_0
    move v0, v2

    :goto_1
    return v0

    :sswitch_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelectionAll()V

    move v0, v1

    goto :goto_1

    :sswitch_1
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v5}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v0}, Landroid/content/ClipboardManager;->getPrimaryClip()Landroid/content/ClipData;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-virtual {v5}, Landroid/content/ClipData;->getItemCount()I

    move-result v6

    move v3, v2

    :goto_2
    if-lt v3, v6, :cond_3

    :cond_1
    const-string/jumbo v3, "clipData"

    invoke-static {v3, v4}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    if-nez v0, :cond_6

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v3, "string_copied_to_clipboard"

    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMultiLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    :goto_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v0, v8, v2, v9}, Landroid/widget/Toast;->setGravity(III)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    invoke-virtual {v5, v2}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v7

    if-nez v7, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v3, "string_already_exists"

    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMultiLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    :goto_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v0, v8, v2, v9}, Landroid/widget/Toast;->setGravity(III)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    move v0, v1

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    const-string/jumbo v3, "string_already_exists"

    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMultiLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v0, v2}, Landroid/widget/Toast;->setDuration(I)V

    goto :goto_4

    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    const-string/jumbo v3, "string_copied_to_clipboard"

    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMultiLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v0, v2}, Landroid/widget/Toast;->setDuration(I)V

    goto :goto_3

    :sswitch_2
    if-eq v4, v3, :cond_9

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v5}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v4, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_8

    invoke-virtual {v0}, Landroid/content/ClipboardManager;->getPrimaryClip()Landroid/content/ClipData;

    move-result-object v5

    if-eqz v5, :cond_7

    invoke-virtual {v5}, Landroid/content/ClipData;->getItemCount()I

    move-result v6

    move v3, v2

    :goto_5
    if-lt v3, v6, :cond_a

    :cond_7
    const-string/jumbo v3, "clipData"

    invoke-static {v3, v4}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    if-nez v0, :cond_d

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v3, "string_copied_to_clipboard"

    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMultiLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    :goto_6
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v0, v8, v2, v9}, Landroid/widget/Toast;->setGravity(III)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_8
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->removeText()V

    :cond_9
    move v0, v1

    goto/16 :goto_1

    :cond_a
    invoke-virtual {v5, v2}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v7

    if-nez v7, :cond_c

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    if-nez v0, :cond_b

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v3, "string_already_exists"

    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMultiLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    :goto_7
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v0, v8, v2, v9}, Landroid/widget/Toast;->setGravity(III)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->removeText()V

    move v0, v1

    goto/16 :goto_1

    :cond_b
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    const-string/jumbo v3, "string_already_exists"

    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMultiLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v0, v2}, Landroid/widget/Toast;->setDuration(I)V

    goto :goto_7

    :cond_c
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :cond_d
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    const-string/jumbo v3, "string_copied_to_clipboard"

    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMultiLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v0, v2}, Landroid/widget/Toast;->setDuration(I)V

    goto :goto_6

    :sswitch_3
    invoke-virtual {v0}, Landroid/content/ClipboardManager;->getPrimaryClip()Landroid/content/ClipData;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ClipData$Item;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_e

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->appendText(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    if-nez v0, :cond_f

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v3, "string_pasted_to_clipboard"

    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMultiLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    :goto_8
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v0, v8, v2, v9}, Landroid/widget/Toast;->setGravity(III)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_e
    move v0, v1

    goto/16 :goto_1

    :cond_f
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    const-string/jumbo v3, "string_pasted_to_clipboard"

    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getMultiLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mClipboardMessage:Landroid/widget/Toast;

    invoke-virtual {v0, v2}, Landroid/widget/Toast;->setDuration(I)V

    goto :goto_8

    :cond_10
    move v10, v4

    move v4, v3

    move v3, v10

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x1d -> :sswitch_0
        0x1f -> :sswitch_1
        0x32 -> :sswitch_3
        0x34 -> :sswitch_2
    .end sparse-switch
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "onKeyUp() keyCode = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    :sswitch_0
    return v0

    :sswitch_1
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mAltKeyPressed:Z

    move v0, v1

    goto :goto_0

    :sswitch_2
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mShiftKeyPressed:Z

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v2}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v3}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v3

    if-le v2, v3, :cond_0

    invoke-virtual {p0, v3, v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    :cond_0
    move v0, v1

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_0
        0x15 -> :sswitch_0
        0x16 -> :sswitch_0
        0x39 -> :sswitch_1
        0x3a -> :sswitch_1
        0x3b -> :sswitch_2
        0x3c -> :sswitch_2
        0x3d -> :sswitch_0
        0x42 -> :sswitch_0
        0x43 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onObjectChanged()V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;->onObjectChanged(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V

    :cond_0
    return-void
.end method

.method protected onRequestScroll(FF)V
    .locals 3

    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onRequestScroll dx = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", dy = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;->onRequestScroll(FF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;->onObjectChanged(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V

    :cond_0
    return-void
.end method

.method protected onSelectionChanged(II)V
    .locals 2

    const/4 v0, 0x0

    const-string/jumbo v1, "onSelectionChange is called"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;->onSelectionChanged(II)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->setDirty()V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTouchEnable:Z

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->hide()V

    :cond_2
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsViewMode:Z

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->bringToFront()V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->requestFocus()Z

    :cond_3
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextEraserEnable:Z

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v2

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v3

    if-nez v3, :cond_4

    const-string/jumbo v1, "coordinateInfo is null."

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    goto :goto_0

    :cond_4
    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    invoke-direct {p0, v4, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    const/4 v2, 0x2

    new-array v2, v2, [F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    iget v5, v4, Landroid/graphics/RectF;->left:F

    sub-float/2addr v3, v5

    aput v3, v2, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    iget v4, v4, Landroid/graphics/RectF;->top:F

    sub-float/2addr v3, v4

    aput v3, v2, v1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getInvMatrix()Landroid/graphics/Matrix;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/graphics/Matrix;->mapPoints([F)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :goto_1
    move v0, v1

    goto :goto_0

    :pswitch_0
    aget v3, v2, v1

    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineForVertical(F)I

    move-result v3

    aget v2, v2, v0

    invoke-direct {p0, v3, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v2

    invoke-virtual {p0, v2, v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto :goto_1

    :pswitch_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v3}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v3

    aget v4, v2, v1

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineForVertical(F)I

    move-result v4

    aget v2, v2, v0

    invoke-direct {p0, v4, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v2

    invoke-virtual {p0, v3, v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto :goto_1

    :pswitch_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v3}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v3

    aget v4, v2, v1

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineForVertical(F)I

    move-result v4

    aget v2, v2, v0

    invoke-direct {p0, v4, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I

    move-result v2

    invoke-virtual {p0, v3, v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->removeText()V

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method protected onVisibleUpdated(Z)V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-interface {v0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;->onVisibleUpdated(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;Z)V

    :cond_0
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHasWindowFocus:Z

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBlink:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->startBlink()V

    :goto_0
    invoke-super {p0, p1}, Landroid/view/View;->onWindowFocusChanged(Z)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBlink:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Blink;->stopBlink()V

    goto :goto_0
.end method

.method public removeText()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v1}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v1

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsDeletedText:Z

    if-eq v0, v1, :cond_2

    if-ge v0, v1, :cond_1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v2, v0, v1}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    invoke-virtual {p0, v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    return-void

    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v2, v1, v0}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    invoke-virtual {p0, v1, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto :goto_0

    :cond_2
    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    add-int/lit8 v2, v0, -0x1

    invoke-interface {v1, v2, v0}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    goto :goto_0
.end method

.method public setContextMenu(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->hide()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->setInstance(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)V

    return-void
.end method

.method public setContextMenuVisible(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenuVisible:Z

    return-void
.end method

.method public setCursorPos(I)V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    return-void
.end method

.method public setEraserMode(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextEraserEnable:Z

    return-void
.end method

.method public setMargin(IIII)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_LEFT_MARGIN:I

    iput p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_RIGHT_MARGIN:I

    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_TOP_MARGIN:I

    iput p4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->CONTROL_BOTTOM_MARGIN:I

    return-void
.end method

.method public setObjectText(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V
    .locals 3

    const/4 v2, 0x0

    const-string/jumbo v0, "setObjectText() is called."

    invoke-static {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    if-nez v0, :cond_2

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "setObjectText() "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "textObj"

    :goto_0
    invoke-static {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    :goto_1
    return-void

    :cond_1
    const-string/jumbo v0, "mNativeTextView is invalid."

    goto :goto_0

    :cond_2
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_setObjectText(ILcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)Z

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->initEditable()V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    goto :goto_1
.end method

.method public setParagraph(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;)V
    .locals 2

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    instance-of v0, p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;

    if-eqz v0, :cond_2

    check-cast p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;

    iget v0, p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;->align:I

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setParagraphAlign(I)V

    :cond_1
    :goto_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    goto :goto_0

    :cond_2
    instance-of v0, p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->type:I

    check-cast p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    iget v1, p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->lineSpacing:F

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setParagraphSpacing(IF)V

    goto :goto_1
.end method

.method public setSelection(IIZ)V
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "setSelection start = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " ,end = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    if-lt v0, p2, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v0, p1, p2}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setCursorPos(I)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateSelection()V

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextEraserEnable:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    if-eqz v0, :cond_2

    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getSettingInfo(II)Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;->onSettingTextInfoChanged(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onSelectionChanged(II)V

    :cond_2
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorVisible:Z

    if-eqz p3, :cond_3

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V

    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    goto :goto_0
.end method

.method public setSelection(IZ)V
    .locals 5

    const/16 v4, 0x8

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-gez p1, :cond_0

    const-string/jumbo v2, "setSelection pos < 0"

    invoke-static {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    if-ge v2, p1, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v2, p1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setCursorPos(I)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateSelection()V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    aget-object v2, v2, v0

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    invoke-virtual {v2, v4}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    if-eqz v2, :cond_3

    invoke-direct {p0, p1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getSettingInfo(II)Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    invoke-interface {v3, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;->onSettingTextInfoChanged(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    :cond_3
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextEraserEnable:Z

    if-eqz v2, :cond_5

    :goto_1
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorVisible:Z

    if-eqz p2, :cond_4

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    if-lez v2, :cond_4

    iget v0, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    if-lez v0, :cond_4

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V

    :cond_4
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method public setSelectionAll()V
    .locals 3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto :goto_0
.end method

.method public setText(Ljava/lang/String;)V
    .locals 4

    const/4 v3, 0x0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextLimit:I

    const/16 v1, 0x1388

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-interface {v0, v3, v1, p1}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    return-void

    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextLimit:I

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-le v0, v1, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-interface {v0, v3, v1, p1}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextLimit:I

    invoke-virtual {p1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v3, v1, v2}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    goto :goto_0
.end method

.method public setTextBoxListener(Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;)V
    .locals 2

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v1}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getSettingInfo(II)Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;->onSettingTextInfoChanged(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    :cond_0
    return-void
.end method

.method public setTextFont(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V
    .locals 3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v1}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v1

    instance-of v2, p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;

    if-eqz v2, :cond_1

    check-cast p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;

    iget v2, p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;->foregroundColor:I

    invoke-direct {p0, v2, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextFontColor(III)V

    :cond_0
    :goto_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    return-void

    :cond_1
    instance-of v2, p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    if-eqz v2, :cond_2

    check-cast p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    iget v2, p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    invoke-direct {p0, v2, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextFontSize(FII)V

    goto :goto_0

    :cond_2
    instance-of v2, p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;

    if-eqz v2, :cond_3

    check-cast p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;

    iget v2, p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BackgroundColorSpanInfo;->backgroundColor:I

    invoke-direct {p0, v2, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextBackgroundColor(III)V

    goto :goto_0

    :cond_3
    instance-of v2, p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;

    if-eqz v2, :cond_4

    check-cast p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;

    iget-object v2, p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;->fontName:Ljava/lang/String;

    invoke-direct {p0, v2, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextFontName(Ljava/lang/String;II)V

    goto :goto_0

    :cond_4
    instance-of v2, p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;

    if-eqz v2, :cond_0

    check-cast p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;

    iget v2, p1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;->textDirection:I

    invoke-direct {p0, v2, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextDirection(III)V

    goto :goto_0
.end method

.method public setTextLimit(I)V
    .locals 4

    const/16 v0, 0x1388

    const/4 v3, 0x1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    if-ge v0, p1, :cond_1

    move p1, v0

    :cond_1
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextLimit:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextLimit:I

    if-le v0, v1, :cond_2

    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsDeletedText:Z

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextLimit:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextLimit:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    :cond_2
    new-array v0, v3, [Landroid/text/InputFilter;

    const/4 v1, 0x0

    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;-><init>(Landroid/content/Context;I)V

    aput-object v2, v0, v1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-interface {v1, v0}, Landroid/text/Editable;->setFilters([Landroid/text/InputFilter;)V

    goto :goto_0
.end method

.method public setTextStyle(IZ)V
    .locals 4

    const/4 v3, 0x1

    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Bold="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    and-int/lit8 v2, p1, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " ITALIC="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    and-int/lit8 v2, p1, 0x2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " UNDER="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    and-int/lit8 v2, p1, 0x4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;

    invoke-static {v1}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v1

    if-ne p1, v3, :cond_1

    invoke-direct {p0, p2, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextBold(ZII)V

    :cond_0
    :goto_0
    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    return-void

    :cond_1
    const/4 v2, 0x2

    if-ne p1, v2, :cond_2

    invoke-direct {p0, p2, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextItalic(ZII)V

    goto :goto_0

    :cond_2
    const/4 v2, 0x4

    if-ne p1, v2, :cond_0

    invoke-direct {p0, p2, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextUnderline(ZII)V

    goto :goto_0
.end method

.method public setViewModeEnabled(Z)V
    .locals 2

    const/4 v1, 0x1

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsViewMode:Z

    if-eqz p1, :cond_0

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setFocusableInTouchMode(Z)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setVisibility(I)V

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setFocusable(Z)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->requestFocus()Z

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V

    :cond_0
    return-void
.end method

.method public showSoftInput()V
    .locals 4

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHasWindowFocus:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextView:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$6;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$6;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public showTextBox()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setVisibility(I)V

    return-void
.end method

.method protected updateContextMenuLocation()V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->setDirty()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->hide()V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateContextmenu()V

    :cond_0
    return-void
.end method
