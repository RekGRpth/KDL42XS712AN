.class Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;
.super Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
.source "Twttr"


# instance fields
.field private mObjectList:Ljava/util/ArrayList;

.field private mRotation:F

.field private final mUnionRect:Landroid/graphics/RectF;

.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)V
    .locals 1

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mRotation:F

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mUnionRect:Landroid/graphics/RectF;

    return-void
.end method


# virtual methods
.method public appendObject(Ljava/util/ArrayList;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;

    return-void
.end method

.method public clearChangedFlag()V
    .locals 0

    return-void
.end method

.method public copy(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 0

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getDrawnRect()Landroid/graphics/RectF;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getExtraDataByteArray(Ljava/lang/String;)[B
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getExtraDataInt(Ljava/lang/String;)I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public getExtraDataString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getExtraDataStringArray(Ljava/lang/String;)[Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getMinHeight()F
    .locals 5

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    return v1

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isVisible()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getMinHeight()F

    move-result v0

    cmpl-float v4, v1, v2

    if-eqz v4, :cond_2

    cmpg-float v4, v1, v0

    if-gez v4, :cond_0

    :cond_2
    move v1, v0

    goto :goto_0
.end method

.method public getMinWidth()F
    .locals 5

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    return v1

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isVisible()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getMinWidth()F

    move-result v0

    cmpl-float v4, v1, v2

    if-eqz v4, :cond_2

    cmpg-float v4, v1, v0

    if-gez v4, :cond_0

    :cond_2
    move v1, v0

    goto :goto_0
.end method

.method public getObjectList()Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getRect()Landroid/graphics/RectF;
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mUnionRect:Landroid/graphics/RectF;

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/graphics/RectF;->set(FFFF)V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mUnionRect:Landroid/graphics/RectF;

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRect()Landroid/graphics/RectF;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mUnionRect:Landroid/graphics/RectF;

    invoke-virtual {v2, v0}, Landroid/graphics/RectF;->union(Landroid/graphics/RectF;)V

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public getResizeOption()I
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    move v1, v2

    move v3, v2

    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_0

    if-eqz v3, :cond_3

    :goto_1
    return v4

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getResizeOption()I

    move-result v0

    if-ne v0, v5, :cond_1

    move v4, v5

    goto :goto_1

    :cond_1
    if-ne v0, v4, :cond_2

    move v3, v4

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_3
    move v4, v2

    goto :goto_1
.end method

.method public getRotation()F
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mRotation:F

    return v0
.end method

.method public getRuntimeHandle()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public getSorDataInt(Ljava/lang/String;)I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public getSorDataString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getSorInfo()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getSorPackageLink()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getType()I
    .locals 1

    const/4 v0, 0x4

    return v0
.end method

.method public hasExtraDataByteArray(Ljava/lang/String;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public hasExtraDataInt(Ljava/lang/String;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public hasExtraDataString(Ljava/lang/String;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public hasExtraDataStringArray(Ljava/lang/String;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public hasSorDataInt(Ljava/lang/String;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public hasSorDataString(Ljava/lang/String;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public hashCode()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public isChanged()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isMovable()Z
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isMovable()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isOutOfViewEnabled()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isRecorded()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isRotatable()Z
    .locals 3

    const/4 v2, 0x0

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_1

    const/4 v2, 0x1

    :cond_0
    return v2

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isRotatable()Z

    move-result v0

    if-eqz v0, :cond_0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public isSelectable()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isVisible()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public removeExtraDataByteArray(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public removeExtraDataInt(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public removeExtraDataString(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public removeExtraDataStringArray(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public removeSorDataInt(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public removeSorDataString(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public setExtraDataByteArray(Ljava/lang/String;[B)V
    .locals 0

    return-void
.end method

.method public setExtraDataInt(Ljava/lang/String;I)V
    .locals 0

    return-void
.end method

.method public setExtraDataString(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public setExtraDataStringArray(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public setMovable(Z)V
    .locals 0

    return-void
.end method

.method public setOutOfViewEnabled(Z)V
    .locals 0

    return-void
.end method

.method public setRecorded(Z)V
    .locals 0

    return-void
.end method

.method public setRect(Landroid/graphics/RectF;Z)V
    .locals 14

    iget v0, p1, Landroid/graphics/RectF;->right:F

    iget v1, p1, Landroid/graphics/RectF;->left:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget v0, p1, Landroid/graphics/RectF;->bottom:F

    iget v1, p1, Landroid/graphics/RectF;->top:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->getRect()Landroid/graphics/RectF;

    move-result-object v10

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v10, v0, v1, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_4

    iget v0, p1, Landroid/graphics/RectF;->left:F

    iget v1, p1, Landroid/graphics/RectF;->right:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_e

    iget v0, p1, Landroid/graphics/RectF;->left:F

    iget v1, p1, Landroid/graphics/RectF;->right:F

    iput v1, p1, Landroid/graphics/RectF;->left:F

    iput v0, p1, Landroid/graphics/RectF;->right:F

    const/4 v0, 0x1

    move v1, v0

    :goto_2
    iget v0, p1, Landroid/graphics/RectF;->top:F

    iget v2, p1, Landroid/graphics/RectF;->bottom:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_d

    iget v0, p1, Landroid/graphics/RectF;->top:F

    iget v2, p1, Landroid/graphics/RectF;->bottom:F

    iput v2, p1, Landroid/graphics/RectF;->top:F

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    const/4 v0, 0x1

    move v2, v0

    :goto_3
    iget v0, v10, Landroid/graphics/RectF;->left:F

    iget v3, p1, Landroid/graphics/RectF;->left:F

    cmpl-float v0, v0, v3

    if-nez v0, :cond_2

    iget v0, v10, Landroid/graphics/RectF;->top:F

    iget v3, p1, Landroid/graphics/RectF;->top:F

    cmpl-float v0, v0, v3

    if-nez v0, :cond_2

    iget v0, v10, Landroid/graphics/RectF;->right:F

    iget v3, p1, Landroid/graphics/RectF;->right:F

    cmpl-float v0, v0, v3

    if-nez v0, :cond_2

    iget v0, v10, Landroid/graphics/RectF;->bottom:F

    iget v3, p1, Landroid/graphics/RectF;->bottom:F

    cmpl-float v0, v0, v3

    if-nez v0, :cond_2

    if-nez v1, :cond_2

    if-eqz v2, :cond_0

    :cond_2
    const/4 v0, 0x0

    const/4 v4, 0x0

    iget v3, v10, Landroid/graphics/RectF;->right:F

    iget v5, v10, Landroid/graphics/RectF;->left:F

    cmpl-float v3, v3, v5

    if-eqz v3, :cond_c

    iget v0, p1, Landroid/graphics/RectF;->right:F

    iget v3, p1, Landroid/graphics/RectF;->left:F

    sub-float/2addr v0, v3

    iget v3, v10, Landroid/graphics/RectF;->right:F

    iget v5, v10, Landroid/graphics/RectF;->left:F

    sub-float/2addr v3, v5

    div-float/2addr v0, v3

    move v3, v0

    :goto_4
    iget v0, v10, Landroid/graphics/RectF;->bottom:F

    iget v5, v10, Landroid/graphics/RectF;->top:F

    cmpl-float v0, v0, v5

    if-eqz v0, :cond_3

    iget v0, p1, Landroid/graphics/RectF;->bottom:F

    iget v4, p1, Landroid/graphics/RectF;->top:F

    sub-float/2addr v0, v4

    iget v4, v10, Landroid/graphics/RectF;->bottom:F

    iget v5, v10, Landroid/graphics/RectF;->top:F

    sub-float/2addr v4, v5

    div-float/2addr v0, v4

    move v4, v0

    :cond_3
    const/4 v0, 0x0

    move v5, v0

    :goto_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v5, v0, :cond_6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mUnionRect:Landroid/graphics/RectF;

    invoke-virtual {v0, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRect()Landroid/graphics/RectF;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v10, v0}, Landroid/graphics/RectF;->union(Landroid/graphics/RectF;)V

    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_1

    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRect()Landroid/graphics/RectF;

    move-result-object v11

    iget v6, v11, Landroid/graphics/RectF;->left:F

    iget v7, v10, Landroid/graphics/RectF;->left:F

    sub-float/2addr v6, v7

    iget v7, v11, Landroid/graphics/RectF;->top:F

    iget v8, v10, Landroid/graphics/RectF;->top:F

    sub-float v12, v7, v8

    iget v7, v11, Landroid/graphics/RectF;->right:F

    iget v8, v11, Landroid/graphics/RectF;->left:F

    sub-float v9, v7, v8

    iget v7, v11, Landroid/graphics/RectF;->bottom:F

    iget v8, v11, Landroid/graphics/RectF;->top:F

    sub-float v8, v7, v8

    mul-float v7, v6, v3

    mul-float v6, v12, v4

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getResizeOption()I

    move-result v12

    const/4 v13, 0x2

    if-eq v12, v13, :cond_7

    mul-float/2addr v9, v3

    mul-float/2addr v8, v4

    :cond_7
    if-eqz v1, :cond_8

    iget v12, p1, Landroid/graphics/RectF;->right:F

    iget v13, p1, Landroid/graphics/RectF;->left:F

    sub-float/2addr v12, v13

    sub-float v7, v12, v7

    sub-float/2addr v7, v9

    :cond_8
    if-eqz v2, :cond_9

    iget v12, p1, Landroid/graphics/RectF;->bottom:F

    iget v13, p1, Landroid/graphics/RectF;->top:F

    sub-float/2addr v12, v13

    sub-float v6, v12, v6

    sub-float/2addr v6, v8

    :cond_9
    iget v12, p1, Landroid/graphics/RectF;->left:F

    add-float/2addr v7, v12

    iput v7, v11, Landroid/graphics/RectF;->left:F

    iget v7, p1, Landroid/graphics/RectF;->top:F

    add-float/2addr v6, v7

    iput v6, v11, Landroid/graphics/RectF;->top:F

    iget v6, v11, Landroid/graphics/RectF;->left:F

    add-float/2addr v6, v9

    iput v6, v11, Landroid/graphics/RectF;->right:F

    iget v6, v11, Landroid/graphics/RectF;->top:F

    add-float/2addr v6, v8

    iput v6, v11, Landroid/graphics/RectF;->bottom:F

    if-eqz v2, :cond_a

    iget v6, v11, Landroid/graphics/RectF;->bottom:F

    iget v7, v11, Landroid/graphics/RectF;->top:F

    iput v7, v11, Landroid/graphics/RectF;->bottom:F

    iput v6, v11, Landroid/graphics/RectF;->top:F

    :cond_a
    if-eqz v1, :cond_b

    iget v6, v11, Landroid/graphics/RectF;->right:F

    iget v7, v11, Landroid/graphics/RectF;->left:F

    iput v7, v11, Landroid/graphics/RectF;->right:F

    iput v6, v11, Landroid/graphics/RectF;->left:F

    :cond_b
    const/4 v6, 0x0

    invoke-virtual {v0, v11, v6}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setRect(Landroid/graphics/RectF;Z)V

    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto/16 :goto_5

    :cond_c
    move v3, v0

    goto/16 :goto_4

    :cond_d
    move v2, v3

    goto/16 :goto_3

    :cond_e
    move v1, v2

    goto/16 :goto_2
.end method

.method public setResizeOption(I)V
    .locals 0

    return-void
.end method

.method public setRotatable(Z)V
    .locals 0

    return-void
.end method

.method public setRotation(F)V
    .locals 13

    const/4 v12, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mRotation:F

    sub-float v8, p1, v0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->getRect()Landroid/graphics/RectF;

    move-result-object v9

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mObjectList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->mRotation:F

    :cond_0
    return-void

    :cond_1
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRect()Landroid/graphics/RectF;

    move-result-object v11

    iget v0, v11, Landroid/graphics/RectF;->left:F

    cmpl-float v0, v0, v12

    if-nez v0, :cond_2

    iget v0, v11, Landroid/graphics/RectF;->right:F

    cmpl-float v0, v0, v12

    if-nez v0, :cond_2

    iget v0, v11, Landroid/graphics/RectF;->top:F

    cmpl-float v0, v0, v12

    if-nez v0, :cond_2

    iget v0, v11, Landroid/graphics/RectF;->bottom:F

    cmpl-float v0, v0, v12

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    invoke-virtual {v11}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {v11}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v9}, Landroid/graphics/RectF;->centerX()F

    move-result v3

    float-to-int v3, v3

    int-to-float v3, v3

    invoke-virtual {v9}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    float-to-int v4, v4

    int-to-float v4, v4

    float-to-double v5, v8

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->getRotatePoint(IIFFD)Landroid/graphics/PointF;

    move-result-object v0

    iget v1, v0, Landroid/graphics/PointF;->x:F

    invoke-virtual {v11}, Landroid/graphics/RectF;->centerX()F

    move-result v2

    sub-float/2addr v1, v2

    iget v0, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v11}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    sub-float/2addr v0, v2

    invoke-virtual {v11, v1, v0}, Landroid/graphics/RectF;->offset(FF)V

    const/4 v0, 0x0

    invoke-virtual {v7, v11, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setRect(Landroid/graphics/RectF;Z)V

    :cond_3
    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRotation()F

    move-result v0

    add-float/2addr v0, v8

    invoke-virtual {v7, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setRotation(F)V

    goto :goto_0
.end method

.method public setSelectable(Z)V
    .locals 0

    return-void
.end method

.method public setSorDataInt(Ljava/lang/String;I)V
    .locals 0

    return-void
.end method

.method public setSorDataString(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public setSorInfo(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public setSorPackageLink(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public setVisibility(Z)V
    .locals 0

    return-void
.end method
