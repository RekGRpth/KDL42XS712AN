.class public interface abstract Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final CANCEL_CAUSE_ERROR:I = 0x20

.field public static final CANCEL_CAUSE_FAILED_TO_RECOGNIZE:I = 0x8

.field public static final CANCEL_CAUSE_OUT_OF_VIEW:I = 0x10

.field public static final CANCEL_CAUSE_TOO_SMALL:I = 0x4

.field public static final CANCEL_STATE_RETAKE:I = 0x2

.field public static final CANCEL_STATE_TAKE:I = 0x1

.field public static final FRAMETYPE_BEAUTIFY:I = 0x1

.field public static final FRAMETYPE_ORIGINAL:I


# virtual methods
.method public abstract onCanceled(ILcom/samsung/android/sdk/pen/document/SpenObjectContainer;)V
.end method

.method public abstract onCompleted(ILcom/samsung/android/sdk/pen/document/SpenObjectContainer;)V
.end method
