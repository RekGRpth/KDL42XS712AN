.class Lcom/samsung/android/sdk/pen/engine/SpenInView$OnZoomPadActionListener;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadActionListener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnZoomPadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenInView$OnZoomPadActionListener;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnZoomPadActionListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V

    return-void
.end method


# virtual methods
.method public onChangePan(FF)V
    .locals 4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnZoomPadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnZoomPadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I

    move-result v0

    const/4 v1, 0x1

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setPan(IFFZ)V
    invoke-static {v0, p1, p2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$14(IFFZ)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnZoomPadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSpenZoomPad:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$44(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnZoomPadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSpenZoomPad:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$44(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnZoomPadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$30(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnZoomPadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$31(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnZoomPadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$29(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->setPanAndZoom(FFF)V

    goto :goto_0
.end method

.method public onStop()V
    .locals 4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnZoomPadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnZoomPadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$34(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnZoomPadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$45(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnZoomPadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeightExceptSIP:I
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$46(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I

    move-result v3

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setScreenSize(IIII)V
    invoke-static {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$47(IIII)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnZoomPadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnZoomPadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreZoomable:Z
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$48(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setZoomable(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnZoomPadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mZoomPadListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomPadListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$49(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenZoomPadListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnZoomPadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mZoomPadListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomPadListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$49(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenZoomPadListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPadListener;->onStop()V

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnZoomPadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    const/4 v1, 0x0

    const/4 v2, 0x1

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenInView;->onUpdateCanvas(Landroid/graphics/RectF;Z)V
    invoke-static {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$36(Lcom/samsung/android/sdk/pen/engine/SpenInView;Landroid/graphics/RectF;Z)V

    return-void
.end method

.method public onUpdate(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnZoomPadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnZoomPadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->appendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnZoomPadActionListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->update()V

    :cond_0
    return-void
.end method
