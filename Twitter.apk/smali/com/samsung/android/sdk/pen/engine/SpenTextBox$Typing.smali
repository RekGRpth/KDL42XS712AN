.class Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;
.super Landroid/os/Handler;
.source "Twttr"


# static fields
.field private static final INPUT_DELAY:I = 0xc8

.field public static final INPUT_END_MESSAGE:I = 0x1


# instance fields
.field private final mTextBox:Ljava/lang/ref/WeakReference;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 1

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;->mTextBox:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;->mTextBox:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;->mTextBox:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v0, :cond_0

    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    :goto_1
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    goto :goto_0

    :pswitch_0
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateSettingInfo()V
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$22(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$23(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public startInput()V
    .locals 3

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;->removeMessages(I)V

    const-wide/16 v0, 0xc8

    invoke-virtual {p0, v2, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method

.method public stopInput()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;->removeMessages(I)V

    return-void
.end method
