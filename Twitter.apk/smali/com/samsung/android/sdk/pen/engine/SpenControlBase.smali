.class public abstract Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
.super Landroid/view/View;
.source "Twttr"


# static fields
.field private static synthetic $SWITCH_TABLE$com$samsung$android$sdk$pen$engine$SpenControlBase$QUADRANT:[I = null

.field protected static final ALL:I = -0x1

.field private static final DEBUG:Z = false

.field protected static final DEFAULT_BORDER_POINT:Ljava/lang/String; = "handler_icon"

.field protected static final DEFAULT_DEGREE_STRING:Ljava/lang/String; = "\u00b0"

.field private static final DEFAULT_DENSITY_DPI:I = 0xa0

.field protected static final DEFAULT_HANDLE_RESIZE_ICON_SIZE:I = 0x16

.field protected static final DEFAULT_HANDLE_RESIZE_ICON_SIZE_N1:I = 0x26

.field protected static final DEFAULT_HANDLE_ROTATE_ICON_SIZE:I = 0x1e

.field protected static final DEFAULT_HANDLE_ROTATE_ICON_SIZE_N1:I = 0x2e

.field protected static final DEFAULT_RESIZE_ZONE_SIZE:I = 0x14

.field protected static final DEFAULT_RESIZE_ZONE_SIZE_N1:I = 0x22

.field protected static final DEFAULT_ROTATE_POINT_BORDER:Ljava/lang/String; = "handler_icon_rotate"

.field protected static final DEFAULT_ROTATION_ZONE_SIZE:I = 0x19

.field protected static final DEFAULT_ROTATION_ZONE_SIZE_N1:I = 0x26

.field protected static final DIMMING_BG_COLOR:I = 0x40000000

.field protected static final FLIP_DIRECTION_HORIZONTAL:I = 0x2

.field protected static final FLIP_DIRECTION_NONE:I = 0x0

.field protected static final FLIP_DIRECTION_VERTICAL:I = 0x1

.field protected static final MIN_RESIZE_ZONE_SIZE:I = 0x5

.field protected static final NO_OBJECT:I = -0x1

.field private static final SIGMA:F = 1.0E-4f

.field public static final STYLE_BORDER_NONE:I = 0x1

.field public static final STYLE_BORDER_NONE_ACTION_NONE:I = 0x3

.field public static final STYLE_BORDER_OBJECT:I = 0x0

.field public static final STYLE_BORDER_STATIC:I = 0x2

.field protected static final TOUCH_ZONE_BOTTOM:I = 0x7

.field protected static final TOUCH_ZONE_BOTTOM_LEFT:I = 0x6

.field protected static final TOUCH_ZONE_BOTTOM_RIGHT:I = 0x8

.field protected static final TOUCH_ZONE_CENTER:I = 0x9

.field protected static final TOUCH_ZONE_LEFT:I = 0x4

.field protected static final TOUCH_ZONE_MAX:I = 0xa

.field protected static final TOUCH_ZONE_NONE:I = -0x1

.field protected static final TOUCH_ZONE_RIGHT:I = 0x5

.field protected static final TOUCH_ZONE_ROTATE:I = 0x0

.field protected static final TOUCH_ZONE_TOP:I = 0x2

.field protected static final TOUCH_ZONE_TOP_LEFT:I = 0x1

.field protected static final TOUCH_ZONE_TOP_RIGHT:I = 0x3

.field private static final TRIVIAL_MOVING_CRITERIA:I = 0x14

.field private static mObjectOutlineEnable:Z


# instance fields
.field protected mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

.field private final mDeltaEdge:I

.field private mDensityDpi:I

.field private final mHandler:Landroid/os/Handler;

.field private mIndex:I

.field protected mIsClosed:Z

.field private mIsDim:Z

.field private mIsFirstTouch:Z

.field private mIsFlipDirectionHorizontal:Z

.field private mIsFlipDirectionVertical:Z

.field protected mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

.field protected mMaximumResizeRect:Landroid/graphics/RectF;

.field protected mMinimumResizeRect:Landroid/graphics/RectF;

.field private mObjectBaseList:Ljava/util/ArrayList;

.field protected mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

.field private mPanBackup:Ljava/util/HashMap;

.field private mPointDown:Landroid/graphics/PointF;

.field private mPrev:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

.field private mRectList:Ljava/util/ArrayList;

.field private mResourceMap:Ljava/util/Map;

.field protected mRotateAngle:F

.field private final mSelectContextMenuListener:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$ContextMenuListener;

.field private mStyle:I

.field private mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

.field protected mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

.field private mTempRect:Landroid/graphics/Rect;

.field private mTempRectF:Landroid/graphics/RectF;

.field private mTmpMatrix:Landroid/graphics/Matrix;

.field private mTouchEnable:Z

.field protected mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

.field private mTouchZone:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;

.field protected mTransactionTouchEvent:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTransactionTouchEvent;

.field private mTrivialMovingEn:Z


# direct methods
.method static synthetic $SWITCH_TABLE$com$samsung$android$sdk$pen$engine$SpenControlBase$QUADRANT()[I
    .locals 3

    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->$SWITCH_TABLE$com$samsung$android$sdk$pen$engine$SpenControlBase$QUADRANT:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->values()[Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->QUADRANT_1:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_4

    :goto_1
    :try_start_1
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->QUADRANT_2:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_3

    :goto_2
    :try_start_2
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->QUADRANT_3:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_2

    :goto_3
    :try_start_3
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->QUADRANT_4:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_1

    :goto_4
    :try_start_4
    sget-object v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->QUADRANT_MAX:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_0

    :goto_5
    sput-object v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->$SWITCH_TABLE$com$samsung$android$sdk$pen$engine$SpenControlBase$QUADRANT:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_5

    :catch_1
    move-exception v1

    goto :goto_4

    :catch_2
    move-exception v1

    goto :goto_3

    :catch_3
    move-exception v1

    goto :goto_2

    :catch_4
    move-exception v1

    goto :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectOutlineEnable:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPanBackup:Ljava/util/HashMap;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempRect:Landroid/graphics/Rect;

    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsDim:Z

    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsClosed:Z

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTmpMatrix:Landroid/graphics/Matrix;

    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFirstTouch:Z

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mDeltaEdge:I

    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFlipDirectionHorizontal:Z

    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFlipDirectionVertical:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIndex:I

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$1;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mSelectContextMenuListener:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$ContextMenuListener;

    invoke-direct {p0, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->initialize(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    return-void
.end method

.method private absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V
    .locals 2

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v0, v1

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->left:F

    iget v0, p2, Landroid/graphics/RectF;->right:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v0, v1

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->right:F

    iget v0, p2, Landroid/graphics/RectF;->top:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v0, v1

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v0, v1

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mStyle:I

    return v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIndex:I

    return v0
.end method

.method private calculateContextMenuPosition(Landroid/graphics/Rect;)Landroid/graphics/Rect;
    .locals 13

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v5

    if-nez v5, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchZone:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;

    const/4 v6, 0x0

    invoke-virtual {v3, v6, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->getRect(ILandroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchZone:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;

    const/4 v7, 0x2

    invoke-virtual {v6, v7, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->getRect(ILandroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    add-float/2addr v3, v6

    add-float/2addr v2, v3

    float-to-double v2, v2

    mul-double/2addr v0, v2

    double-to-int v0, v0

    invoke-virtual {v5}, Landroid/graphics/RectF;->centerY()F

    move-result v1

    int-to-float v0, v0

    sub-float v0, v1, v0

    float-to-int v2, v0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-nez v1, :cond_2

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->getRect()Landroid/graphics/Rect;

    move-result-object v6

    if-nez v6, :cond_3

    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    const/4 v1, 0x1

    const/high16 v3, 0x40e00000    # 7.0f

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    invoke-static {v1, v3, v7}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v7, v1

    const/4 v1, 0x1

    const/high16 v3, 0x40a00000    # 5.0f

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v8

    invoke-static {v1, v3, v8}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v8, v1

    iget v1, p1, Landroid/graphics/Rect;->left:I

    iget v3, p1, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v3

    int-to-double v9, v1

    const-wide/high16 v11, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v9, v11

    double-to-int v3, v9

    iget v1, p1, Landroid/graphics/Rect;->top:I

    if-ge v1, v2, :cond_6

    iget v1, p1, Landroid/graphics/Rect;->top:I

    :goto_1
    iget v9, p1, Landroid/graphics/Rect;->bottom:I

    if-le v9, v2, :cond_4

    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    :cond_4
    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v9

    int-to-double v9, v9

    const-wide/high16 v11, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v9, v11

    double-to-int v9, v9

    sub-int/2addr v3, v9

    if-gez v3, :cond_7

    const/4 v3, 0x0

    :cond_5
    :goto_2
    sub-int/2addr v1, v7

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v9

    sub-int/2addr v1, v9

    add-int/2addr v2, v7

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v7

    add-int/2addr v2, v7

    sub-int/2addr v2, v8

    if-gez v1, :cond_9

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v7

    if-le v2, v7, :cond_9

    invoke-virtual {v5}, Landroid/graphics/RectF;->centerY()F

    move-result v1

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v2

    int-to-float v2, v2

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v2, v5

    cmpl-float v1, v1, v2

    if-lez v1, :cond_8

    const/4 v0, 0x0

    :goto_3
    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v1

    add-int/2addr v1, v3

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v2

    add-int/2addr v2, v0

    invoke-virtual {v4, v3, v0, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    move-object v0, v4

    goto/16 :goto_0

    :cond_6
    move v1, v2

    goto :goto_1

    :cond_7
    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v9

    add-int/2addr v9, v3

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v10

    if-le v9, v10, :cond_5

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v3

    invoke-virtual {v6}, Landroid/graphics/Rect;->width()I

    move-result v9

    sub-int/2addr v3, v9

    goto :goto_2

    :cond_8
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v0

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v1

    sub-int/2addr v0, v1

    goto :goto_3

    :cond_9
    if-gez v1, :cond_a

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v0

    sub-int v0, v2, v0

    goto :goto_3

    :cond_a
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v0

    if-le v2, v0, :cond_b

    move v0, v1

    goto :goto_3

    :cond_b
    move v0, v1

    goto :goto_3
.end method

.method private checkAllRectOutOfCanvas(Landroid/graphics/RectF;IF)Z
    .locals 11

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/high16 v10, 0x3f800000    # 1.0f

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-interface {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    :cond_0
    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotX(I)F

    move-result v2

    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotY(I)F

    move-result v3

    new-instance v4, Landroid/graphics/PointF;

    iget v5, p1, Landroid/graphics/RectF;->left:F

    iget v6, p1, Landroid/graphics/RectF;->top:F

    invoke-direct {v4, v5, v6}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-direct {p0, v2, v3, p3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v4

    new-instance v5, Landroid/graphics/PointF;

    iget v6, p1, Landroid/graphics/RectF;->right:F

    iget v7, p1, Landroid/graphics/RectF;->top:F

    invoke-direct {v5, v6, v7}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-direct {p0, v2, v3, p3, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    new-instance v6, Landroid/graphics/PointF;

    iget v7, p1, Landroid/graphics/RectF;->left:F

    iget v8, p1, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v6, v7, v8}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-direct {p0, v2, v3, p3, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v6

    new-instance v7, Landroid/graphics/PointF;

    iget v8, p1, Landroid/graphics/RectF;->right:F

    iget v9, p1, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v7, v8, v9}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-direct {p0, v2, v3, p3, v7}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v2

    invoke-direct {p0, v4, v5, v6, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v2

    aget v3, v2, v1

    aget v4, v2, v0

    const/4 v5, 0x2

    aget v5, v2, v5

    const/4 v6, 0x3

    aget v2, v2, v6

    new-instance v6, Landroid/graphics/RectF;

    invoke-direct {v6, v3, v4, v5, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    new-instance v2, Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    add-float/2addr v3, v10

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    add-float/2addr v4, v10

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->right:F

    sub-float/2addr v5, v10

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v7, v7, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v7, v10

    invoke-direct {v2, v3, v4, v5, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v2, v6}, Landroid/graphics/RectF;->intersect(Landroid/graphics/RectF;)Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v6, v2}, Landroid/graphics/RectF;->contains(Landroid/graphics/RectF;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    move v0, v1

    :cond_2
    return v0
.end method

.method private drawDimmingWindow(Landroid/graphics/Canvas;Landroid/graphics/RectF;)V
    .locals 2

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsDim:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->getPaint(I)Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    :cond_0
    return-void
.end method

.method private drawHighlightRect(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 5

    const/4 v0, 0x0

    const-string/jumbo v1, "Base::drawHighlightObject"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectOutlineEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->getPaint(I)Landroid/graphics/Paint;

    move-result-object v0

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    new-instance v2, Landroid/graphics/RectF;

    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRect()Landroid/graphics/RectF;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-interface {v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {p0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRotation()F

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    move-result v3

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    invoke-virtual {p1, v2, v3, v4}, Landroid/graphics/Canvas;->rotate(FFF)V

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0
.end method

.method private drawHighlightStroke(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;)V
    .locals 12

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectOutlineEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getType()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPenName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPenName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "com.samsung.android.sdk.pen.pen.preload.Beautify"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    :cond_2
    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPoints()[Landroid/graphics/PointF;

    move-result-object v3

    if-eqz v3, :cond_0

    array-length v2, v3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->getPaint(I)Landroid/graphics/Paint;

    move-result-object v7

    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPenSize()F

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    int-to-float v0, v0

    invoke-virtual {v7, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    const/16 v1, 0x64

    if-gt v2, v1, :cond_19

    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->isCurveEnabled()Z

    move-result v1

    if-eqz v1, :cond_19

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    :goto_1
    if-lt v1, v2, :cond_6

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPenName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "com.samsung.android.sdk.pen.pen.preload.Pencil"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    const/4 v1, 0x0

    move v2, v1

    :goto_2
    add-int/lit8 v1, v6, -0x1

    if-lt v2, v1, :cond_8

    :cond_3
    :goto_3
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v9

    const/4 v1, 0x0

    aget-object v1, v3, v1

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    add-float/2addr v1, v2

    const/4 v2, 0x0

    aget-object v2, v3, v2

    iget v2, v2, Landroid/graphics/PointF;->y:F

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    sub-float/2addr v2, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v2, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    add-float/2addr v2, v4

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    const/4 v1, 0x3

    if-lt v6, v1, :cond_17

    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPenName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "com.samsung.android.sdk.pen.pen.preload.Marker"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPenName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "com.samsung.android.sdk.pen.pen.preload.Brush"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_e

    :cond_4
    new-instance v3, Landroid/graphics/PointF;

    invoke-direct {v3}, Landroid/graphics/PointF;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    const/4 v2, 0x0

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/PointF;

    const/4 v4, 0x0

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getStartEndBitmapCalcPoint(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;F)V

    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2}, Landroid/graphics/PointF;-><init>()V

    iget v4, v3, Landroid/graphics/PointF;->x:F

    const/4 v1, 0x1

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    add-float/2addr v1, v4

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v1, v4

    iput v1, v2, Landroid/graphics/PointF;->x:F

    iget v4, v3, Landroid/graphics/PointF;->y:F

    const/4 v1, 0x1

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v4

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v1, v4

    iput v1, v2, Landroid/graphics/PointF;->y:F

    iget v1, v3, Landroid/graphics/PointF;->x:F

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v1, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    add-float/2addr v1, v4

    iget v4, v3, Landroid/graphics/PointF;->y:F

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v8, v8, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->y:F

    sub-float/2addr v4, v8

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v4, v8

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v8, v8, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->top:F

    add-float/2addr v4, v8

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Path;->moveTo(FF)V

    iget v1, v3, Landroid/graphics/PointF;->x:F

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v1, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    add-float/2addr v1, v4

    iget v3, v3, Landroid/graphics/PointF;->y:F

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    add-float/2addr v3, v4

    iget v4, v2, Landroid/graphics/PointF;->x:F

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v8, v8, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->x:F

    sub-float/2addr v4, v8

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v4, v8

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v8, v8, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->left:F

    add-float/2addr v4, v8

    iget v2, v2, Landroid/graphics/PointF;->y:F

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v8, v8, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->y:F

    sub-float/2addr v2, v8

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v2, v8

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v8, v8, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->top:F

    add-float/2addr v2, v8

    invoke-virtual {v0, v1, v3, v4, v2}, Landroid/graphics/Path;->quadTo(FFFF)V

    const/4 v2, 0x0

    const/4 v1, 0x1

    move v3, v1

    move-object v1, v2

    :goto_4
    add-int/lit8 v2, v6, -0x1

    if-lt v3, v2, :cond_d

    iget v2, v1, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    add-float/2addr v2, v3

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v1, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    add-float/2addr v3, v1

    add-int/lit8 v1, v6, -0x1

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v1, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    add-float/2addr v4, v1

    add-int/lit8 v1, v6, -0x1

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, v5

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v1, v5

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    add-float/2addr v1, v5

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/graphics/Path;->quadTo(FFFF)V

    :cond_5
    :goto_5
    invoke-virtual {p1, v0, v7}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPenSize()F

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x40800000    # 4.0f

    div-float/2addr v1, v2

    invoke-virtual {v7, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    const/4 v1, -0x1

    invoke-virtual {v7, v1}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->DST_OUT:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v7, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    invoke-virtual {p1, v0, v7}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto/16 :goto_0

    :cond_6
    aget-object v4, v3, v1

    invoke-direct {p0, v5, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isContained(Ljava/util/ArrayList;Landroid/graphics/PointF;)Z

    move-result v4

    if-nez v4, :cond_7

    aget-object v4, v3, v1

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1

    :cond_8
    add-int/lit8 v1, v2, 0x1

    if-ge v1, v6, :cond_9

    new-instance v4, Landroid/graphics/PointF;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v9, v1, Landroid/graphics/PointF;->x:F

    add-int/lit8 v1, v2, 0x1

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    add-float/2addr v1, v9

    const/high16 v9, 0x40000000    # 2.0f

    div-float v9, v1, v9

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v10, v1, Landroid/graphics/PointF;->y:F

    add-int/lit8 v1, v2, 0x1

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v10

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v1, v10

    invoke-direct {v4, v9, v1}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_9
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto/16 :goto_2

    :cond_a
    const/4 v1, 0x0

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v1, 0x1

    move v2, v1

    :goto_6
    add-int/lit8 v1, v6, -0x2

    if-lt v2, v1, :cond_b

    const/4 v1, 0x1

    if-le v6, v1, :cond_3

    add-int/lit8 v1, v6, -0x1

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_3

    :cond_b
    add-int/lit8 v1, v2, 0x2

    if-ge v1, v6, :cond_c

    new-instance v4, Landroid/graphics/PointF;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v9, v1, Landroid/graphics/PointF;->x:F

    add-int/lit8 v1, v2, 0x2

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    add-float/2addr v1, v9

    const/high16 v9, 0x40000000    # 2.0f

    div-float v9, v1, v9

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v10, v1, Landroid/graphics/PointF;->y:F

    add-int/lit8 v1, v2, 0x2

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v10

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v1, v10

    invoke-direct {v4, v9, v1}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_c
    add-int/lit8 v1, v2, 0x2

    move v2, v1

    goto :goto_6

    :cond_d
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Landroid/graphics/PointF;

    new-instance v4, Landroid/graphics/PointF;

    invoke-direct {v4}, Landroid/graphics/PointF;-><init>()V

    iget v8, v2, Landroid/graphics/PointF;->x:F

    add-int/lit8 v1, v3, 0x1

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    add-float/2addr v1, v8

    float-to-double v8, v1

    const-wide/high16 v10, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v8, v10

    double-to-float v1, v8

    iput v1, v4, Landroid/graphics/PointF;->x:F

    iget v8, v2, Landroid/graphics/PointF;->y:F

    add-int/lit8 v1, v3, 0x1

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v8

    float-to-double v8, v1

    const-wide/high16 v10, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v8, v10

    double-to-float v1, v8

    iput v1, v4, Landroid/graphics/PointF;->y:F

    iget v1, v2, Landroid/graphics/PointF;->x:F

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v8, v8, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v8

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v1, v8

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v8, v8, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->left:F

    add-float/2addr v1, v8

    iget v2, v2, Landroid/graphics/PointF;->y:F

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v8, v8, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->y:F

    sub-float/2addr v2, v8

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v2, v8

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v8, v8, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->top:F

    add-float/2addr v2, v8

    iget v8, v4, Landroid/graphics/PointF;->x:F

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v9, v9, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->x:F

    sub-float/2addr v8, v9

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v9, v9, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v8, v9

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v9, v9, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->left:F

    add-float/2addr v8, v9

    iget v9, v4, Landroid/graphics/PointF;->y:F

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->y:F

    sub-float/2addr v9, v10

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v10, v10, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v9, v10

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->top:F

    add-float/2addr v9, v10

    invoke-virtual {v0, v1, v2, v8, v9}, Landroid/graphics/Path;->quadTo(FFFF)V

    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move-object v1, v4

    goto/16 :goto_4

    :cond_e
    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPenName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "com.samsung.android.sdk.pen.pen.preload.Pencil"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_10

    const/4 v1, 0x0

    move v2, v1

    :goto_7
    if-lt v2, v9, :cond_f

    add-int/lit8 v1, v6, -0x1

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    add-float/2addr v2, v1

    add-int/lit8 v1, v6, -0x1

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v1, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    add-float/2addr v1, v3

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Path;->lineTo(FF)V

    goto/16 :goto_5

    :cond_f
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v1, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    add-float/2addr v3, v1

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v1, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    add-float/2addr v4, v1

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v10

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v10, v10, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v1, v10

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->left:F

    add-float/2addr v10, v1

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v11, v11, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, v11

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v1, v11

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->top:F

    add-float/2addr v1, v11

    invoke-virtual {v0, v3, v4, v10, v1}, Landroid/graphics/Path;->quadTo(FFFF)V

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto/16 :goto_7

    :cond_10
    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPenName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_15

    rem-int/lit8 v1, v6, 0x2

    const/4 v2, 0x1

    if-ne v1, v2, :cond_12

    const/4 v1, 0x0

    move v2, v1

    :goto_8
    add-int/lit8 v1, v9, -0x1

    if-ge v2, v1, :cond_5

    mul-int/lit8 v1, v2, 0x2

    add-int/lit8 v1, v1, 0x1

    if-ge v1, v6, :cond_11

    mul-int/lit8 v1, v2, 0x2

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v1, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    add-float/2addr v3, v1

    mul-int/lit8 v1, v2, 0x2

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v1, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    add-float/2addr v4, v1

    add-int/lit8 v1, v2, 0x1

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v10

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v10, v10, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v1, v10

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->left:F

    add-float/2addr v10, v1

    add-int/lit8 v1, v2, 0x1

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v11, v11, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, v11

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v1, v11

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->top:F

    add-float/2addr v1, v11

    invoke-virtual {v0, v3, v4, v10, v1}, Landroid/graphics/Path;->quadTo(FFFF)V

    :cond_11
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto/16 :goto_8

    :cond_12
    const/4 v1, 0x0

    move v2, v1

    :goto_9
    add-int/lit8 v1, v9, -0x2

    if-lt v2, v1, :cond_13

    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1}, Landroid/graphics/PointF;-><init>()V

    add-int/lit8 v1, v9, -0x2

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Landroid/graphics/PointF;

    add-int/lit8 v1, v6, -0x3

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Landroid/graphics/PointF;

    iget v1, v2, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v1, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    add-float/2addr v1, v3

    iget v2, v2, Landroid/graphics/PointF;->y:F

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    add-float/2addr v2, v3

    iget v3, v4, Landroid/graphics/PointF;->x:F

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    sub-float/2addr v3, v5

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v3, v5

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    add-float/2addr v3, v5

    iget v4, v4, Landroid/graphics/PointF;->y:F

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    sub-float/2addr v4, v5

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v4, v5

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    add-float/2addr v4, v5

    add-int/lit8 v5, v9, -0x1

    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->x:F

    sub-float/2addr v5, v6

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v5, v6

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->left:F

    add-float/2addr v5, v6

    add-int/lit8 v6, v9, -0x1

    invoke-virtual {v8, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->y:F

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v8, v8, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v8, v8, Landroid/graphics/PointF;->y:F

    sub-float/2addr v6, v8

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v8, v8, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v6, v8

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v8, v8, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->top:F

    add-float/2addr v6, v8

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    goto/16 :goto_5

    :cond_13
    mul-int/lit8 v1, v2, 0x2

    add-int/lit8 v1, v1, 0x1

    if-ge v1, v6, :cond_14

    mul-int/lit8 v1, v2, 0x2

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v1, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    add-float/2addr v3, v1

    mul-int/lit8 v1, v2, 0x2

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v1, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    add-float/2addr v4, v1

    add-int/lit8 v1, v2, 0x1

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v10

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v10, v10, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v1, v10

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->left:F

    add-float/2addr v10, v1

    add-int/lit8 v1, v2, 0x1

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v11, v11, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, v11

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v1, v11

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->top:F

    add-float/2addr v1, v11

    invoke-virtual {v0, v3, v4, v10, v1}, Landroid/graphics/Path;->quadTo(FFFF)V

    :cond_14
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto/16 :goto_9

    :cond_15
    const/4 v1, 0x0

    move v2, v1

    :goto_a
    add-int/lit8 v1, v9, -0x1

    if-ge v2, v1, :cond_5

    mul-int/lit8 v1, v2, 0x2

    add-int/lit8 v1, v1, 0x1

    if-ge v1, v6, :cond_16

    mul-int/lit8 v1, v2, 0x2

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v1, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    add-float/2addr v3, v1

    mul-int/lit8 v1, v2, 0x2

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v1, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    add-float/2addr v4, v1

    add-int/lit8 v1, v2, 0x1

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v10

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v10, v10, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v1, v10

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->left:F

    add-float/2addr v10, v1

    add-int/lit8 v1, v2, 0x1

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v11, v11, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, v11

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v1, v11

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->top:F

    add-float/2addr v1, v11

    invoke-virtual {v0, v3, v4, v10, v1}, Landroid/graphics/Path;->quadTo(FFFF)V

    :cond_16
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto/16 :goto_a

    :cond_17
    const/4 v1, 0x2

    if-ne v6, v1, :cond_18

    const/4 v1, 0x1

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    add-float/2addr v2, v1

    const/4 v1, 0x1

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v1, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    add-float/2addr v1, v3

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Path;->lineTo(FF)V

    invoke-virtual {p1, v0, v7}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPenSize()F

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x40800000    # 4.0f

    div-float/2addr v1, v2

    invoke-virtual {v7, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    const/4 v1, -0x1

    invoke-virtual {v7, v1}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->DST_OUT:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v7, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    invoke-virtual {p1, v0, v7}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto/16 :goto_0

    :cond_18
    const/4 v0, 0x1

    if-ne v6, v0, :cond_0

    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPenSize()F

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v0, v1

    const/high16 v1, 0x40800000    # 4.0f

    div-float/2addr v0, v1

    invoke-virtual {v7, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    const/4 v0, 0x0

    aget-object v0, v3, v0

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    sub-float/2addr v0, v1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v0, v1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    add-float/2addr v0, v1

    const/4 v1, 0x0

    aget-object v1, v3, v1

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    add-float/2addr v1, v2

    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPenSize()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    float-to-int v2, v2

    int-to-float v2, v2

    invoke-virtual {p1, v0, v1, v2, v7}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    :cond_19
    const/4 v1, 0x0

    aget-object v1, v3, v1

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v1, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    add-float/2addr v1, v4

    const/4 v4, 0x0

    aget-object v4, v3, v4

    iget v4, v4, Landroid/graphics/PointF;->y:F

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    sub-float/2addr v4, v5

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v4, v5

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    add-float/2addr v4, v5

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Path;->setLastPoint(FF)V

    const/4 v1, 0x0

    :goto_b
    if-lt v1, v2, :cond_1a

    invoke-virtual {p1, v0, v7}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPenSize()F

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v1, v2

    const/high16 v2, 0x40800000    # 4.0f

    div-float/2addr v1, v2

    invoke-virtual {v7, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    const/4 v1, -0x1

    invoke-virtual {v7, v1}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->DST_OUT:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v7, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    invoke-virtual {p1, v0, v7}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto/16 :goto_0

    :cond_1a
    aget-object v4, v3, v1

    iget v4, v4, Landroid/graphics/PointF;->x:F

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    sub-float/2addr v4, v5

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v4, v5

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    add-float/2addr v4, v5

    aget-object v5, v3, v1

    iget v5, v5, Landroid/graphics/PointF;->y:F

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->y:F

    sub-float/2addr v5, v6

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v5, v6

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    add-float/2addr v5, v6

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_b
.end method

.method private findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F
    .locals 6

    iget v0, p1, Landroid/graphics/PointF;->x:F

    iget v1, p2, Landroid/graphics/PointF;->x:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    iget v0, p1, Landroid/graphics/PointF;->x:F

    :goto_0
    iget v1, p1, Landroid/graphics/PointF;->x:F

    iget v2, p2, Landroid/graphics/PointF;->x:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_1

    iget v1, p1, Landroid/graphics/PointF;->x:F

    :goto_1
    iget v2, p1, Landroid/graphics/PointF;->y:F

    iget v3, p2, Landroid/graphics/PointF;->y:F

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_2

    iget v2, p1, Landroid/graphics/PointF;->y:F

    :goto_2
    iget v3, p1, Landroid/graphics/PointF;->y:F

    iget v4, p2, Landroid/graphics/PointF;->y:F

    cmpl-float v3, v3, v4

    if-lez v3, :cond_3

    iget v3, p1, Landroid/graphics/PointF;->y:F

    :goto_3
    const/4 v4, 0x4

    new-array v4, v4, [F

    const/4 v5, 0x0

    aput v0, v4, v5

    const/4 v0, 0x1

    aput v2, v4, v0

    const/4 v0, 0x2

    aput v1, v4, v0

    const/4 v0, 0x3

    aput v3, v4, v0

    return-object v4

    :cond_0
    iget v0, p2, Landroid/graphics/PointF;->x:F

    goto :goto_0

    :cond_1
    iget v1, p2, Landroid/graphics/PointF;->x:F

    goto :goto_1

    :cond_2
    iget v2, p2, Landroid/graphics/PointF;->y:F

    goto :goto_2

    :cond_3
    iget v3, p2, Landroid/graphics/PointF;->y:F

    goto :goto_3
.end method

.method private findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)[F
    .locals 6

    iget v0, p1, Landroid/graphics/PointF;->x:F

    iget v1, p2, Landroid/graphics/PointF;->x:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_8

    iget v0, p1, Landroid/graphics/PointF;->x:F

    :goto_0
    iget v1, p3, Landroid/graphics/PointF;->x:F

    cmpg-float v1, v1, v0

    if-gtz v1, :cond_0

    iget v0, p3, Landroid/graphics/PointF;->x:F

    :cond_0
    iget v1, p4, Landroid/graphics/PointF;->x:F

    cmpg-float v1, v1, v0

    if-gtz v1, :cond_1

    iget v0, p4, Landroid/graphics/PointF;->x:F

    :cond_1
    iget v1, p1, Landroid/graphics/PointF;->x:F

    iget v2, p2, Landroid/graphics/PointF;->x:F

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_9

    iget v1, p1, Landroid/graphics/PointF;->x:F

    :goto_1
    iget v2, p3, Landroid/graphics/PointF;->x:F

    cmpl-float v2, v2, v1

    if-ltz v2, :cond_2

    iget v1, p3, Landroid/graphics/PointF;->x:F

    :cond_2
    iget v2, p4, Landroid/graphics/PointF;->x:F

    cmpl-float v2, v2, v1

    if-ltz v2, :cond_3

    iget v1, p4, Landroid/graphics/PointF;->x:F

    :cond_3
    iget v2, p1, Landroid/graphics/PointF;->y:F

    iget v3, p2, Landroid/graphics/PointF;->y:F

    cmpg-float v2, v2, v3

    if-gtz v2, :cond_a

    iget v2, p1, Landroid/graphics/PointF;->y:F

    :goto_2
    iget v3, p3, Landroid/graphics/PointF;->y:F

    cmpg-float v3, v3, v2

    if-gtz v3, :cond_4

    iget v2, p3, Landroid/graphics/PointF;->y:F

    :cond_4
    iget v3, p4, Landroid/graphics/PointF;->y:F

    cmpg-float v3, v3, v2

    if-gtz v3, :cond_5

    iget v2, p4, Landroid/graphics/PointF;->y:F

    :cond_5
    iget v3, p1, Landroid/graphics/PointF;->y:F

    iget v4, p2, Landroid/graphics/PointF;->y:F

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_b

    iget v3, p1, Landroid/graphics/PointF;->y:F

    :goto_3
    iget v4, p3, Landroid/graphics/PointF;->y:F

    cmpl-float v4, v4, v3

    if-ltz v4, :cond_6

    iget v3, p3, Landroid/graphics/PointF;->y:F

    :cond_6
    iget v4, p4, Landroid/graphics/PointF;->y:F

    cmpl-float v4, v4, v3

    if-ltz v4, :cond_7

    iget v3, p4, Landroid/graphics/PointF;->y:F

    :cond_7
    const/4 v4, 0x4

    new-array v4, v4, [F

    const/4 v5, 0x0

    aput v0, v4, v5

    const/4 v0, 0x1

    aput v2, v4, v0

    const/4 v0, 0x2

    aput v1, v4, v0

    const/4 v0, 0x3

    aput v3, v4, v0

    return-object v4

    :cond_8
    iget v0, p2, Landroid/graphics/PointF;->x:F

    goto :goto_0

    :cond_9
    iget v1, p2, Landroid/graphics/PointF;->x:F

    goto :goto_1

    :cond_a
    iget v2, p2, Landroid/graphics/PointF;->y:F

    goto :goto_2

    :cond_b
    iget v3, p2, Landroid/graphics/PointF;->y:F

    goto :goto_3
.end method

.method private findResizeRate(Landroid/graphics/PointF;Landroid/graphics/RectF;)Landroid/graphics/PointF;
    .locals 6

    const/high16 v1, 0x3f800000    # 1.0f

    const v5, 0x38d1b717    # 1.0E-4f

    new-instance v3, Landroid/graphics/PointF;

    invoke-direct {v3}, Landroid/graphics/PointF;-><init>()V

    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpg-float v2, v2, v5

    if-gez v2, :cond_0

    move v0, v1

    :cond_0
    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpg-float v4, v4, v5

    if-gez v4, :cond_1

    :goto_0
    iget v2, p1, Landroid/graphics/PointF;->x:F

    div-float v0, v2, v0

    iput v0, v3, Landroid/graphics/PointF;->x:F

    iget v0, p1, Landroid/graphics/PointF;->y:F

    div-float/2addr v0, v1

    iput v0, v3, Landroid/graphics/PointF;->y:F

    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, " resizeRate.x.y = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, v3, Landroid/graphics/PointF;->x:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " , "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    return-object v3

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method private fit(ILcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V
    .locals 3

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRect()Landroid/graphics/RectF;

    move-result-object v1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    goto :goto_0
.end method

.method private getBorderAngle(I)F
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRotation()F

    move-result v0

    goto :goto_0
.end method

.method private getBorderRect(I)Landroid/graphics/RectF;
    .locals 3

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempRectF:Landroid/graphics/RectF;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRect()Landroid/graphics/RectF;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {p0, v1, v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempRectF:Landroid/graphics/RectF;

    goto :goto_0
.end method

.method private getFixedPoint(ILandroid/graphics/RectF;)Landroid/graphics/PointF;
    .locals 2

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-object v0

    :pswitch_1
    invoke-virtual {p2}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    iput v1, v0, Landroid/graphics/PointF;->x:F

    iget v1, p2, Landroid/graphics/RectF;->bottom:F

    iput v1, v0, Landroid/graphics/PointF;->y:F

    goto :goto_0

    :pswitch_2
    invoke-virtual {p2}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    iput v1, v0, Landroid/graphics/PointF;->x:F

    iget v1, p2, Landroid/graphics/RectF;->top:F

    iput v1, v0, Landroid/graphics/PointF;->y:F

    goto :goto_0

    :pswitch_3
    iget v1, p2, Landroid/graphics/RectF;->left:F

    iput v1, v0, Landroid/graphics/PointF;->x:F

    invoke-virtual {p2}, Landroid/graphics/RectF;->centerY()F

    move-result v1

    iput v1, v0, Landroid/graphics/PointF;->y:F

    goto :goto_0

    :pswitch_4
    iget v1, p2, Landroid/graphics/RectF;->right:F

    iput v1, v0, Landroid/graphics/PointF;->x:F

    invoke-virtual {p2}, Landroid/graphics/RectF;->centerY()F

    move-result v1

    iput v1, v0, Landroid/graphics/PointF;->y:F

    goto :goto_0

    :pswitch_5
    iget v1, p2, Landroid/graphics/RectF;->left:F

    iput v1, v0, Landroid/graphics/PointF;->x:F

    iget v1, p2, Landroid/graphics/RectF;->bottom:F

    iput v1, v0, Landroid/graphics/PointF;->y:F

    goto :goto_0

    :pswitch_6
    iget v1, p2, Landroid/graphics/RectF;->right:F

    iput v1, v0, Landroid/graphics/PointF;->x:F

    iget v1, p2, Landroid/graphics/RectF;->bottom:F

    iput v1, v0, Landroid/graphics/PointF;->y:F

    goto :goto_0

    :pswitch_7
    iget v1, p2, Landroid/graphics/RectF;->left:F

    iput v1, v0, Landroid/graphics/PointF;->x:F

    iget v1, p2, Landroid/graphics/RectF;->top:F

    iput v1, v0, Landroid/graphics/PointF;->y:F

    goto :goto_0

    :pswitch_8
    iget v1, p2, Landroid/graphics/RectF;->right:F

    iput v1, v0, Landroid/graphics/PointF;->x:F

    iget v1, p2, Landroid/graphics/RectF;->top:F

    iput v1, v0, Landroid/graphics/PointF;->y:F

    goto :goto_0

    :pswitch_9
    invoke-virtual {p2}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    iput v1, v0, Landroid/graphics/PointF;->x:F

    invoke-virtual {p2}, Landroid/graphics/RectF;->centerY()F

    move-result v1

    iput v1, v0, Landroid/graphics/PointF;->y:F

    goto :goto_0

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_9
        :pswitch_0
        :pswitch_6
        :pswitch_1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_8
        :pswitch_2
        :pswitch_7
    .end packed-switch
.end method

.method private getOffsetWhenRotation(ILandroid/graphics/RectF;Landroid/graphics/RectF;F)Landroid/graphics/PointF;
    .locals 9

    new-instance v7, Landroid/graphics/PointF;

    invoke-direct {v7}, Landroid/graphics/PointF;-><init>()V

    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getFixedPoint(ILandroid/graphics/RectF;)Landroid/graphics/PointF;

    move-result-object v0

    iget v1, v0, Landroid/graphics/PointF;->x:F

    float-to-int v1, v1

    iget v0, v0, Landroid/graphics/PointF;->y:F

    float-to-int v2, v0

    invoke-virtual {p2}, Landroid/graphics/RectF;->centerX()F

    move-result v3

    invoke-virtual {p2}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    float-to-double v5, p4

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getRotatePoint(IIFFD)Landroid/graphics/PointF;

    move-result-object v8

    invoke-direct {p0, p1, p3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getFixedPoint(ILandroid/graphics/RectF;)Landroid/graphics/PointF;

    move-result-object v0

    iget v1, v0, Landroid/graphics/PointF;->x:F

    float-to-int v1, v1

    iget v0, v0, Landroid/graphics/PointF;->y:F

    float-to-int v2, v0

    invoke-virtual {p3}, Landroid/graphics/RectF;->centerX()F

    move-result v3

    invoke-virtual {p3}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    float-to-double v5, p4

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getRotatePoint(IIFFD)Landroid/graphics/PointF;

    move-result-object v0

    iget v1, v8, Landroid/graphics/PointF;->x:F

    iget v2, v0, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v2

    iput v1, v7, Landroid/graphics/PointF;->x:F

    iget v1, v8, Landroid/graphics/PointF;->y:F

    iget v0, v0, Landroid/graphics/PointF;->y:F

    sub-float v0, v1, v0

    iput v0, v7, Landroid/graphics/PointF;->y:F

    return-object v7
.end method

.method private getPanKey(II)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private getRotateable(I)Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    const/4 v1, -0x1

    if-ne p1, v1, :cond_1

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, " index out of range. [ index = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " ]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isRotatable()Z

    move-result v0

    goto :goto_0
.end method

.method private initialize(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V
    .locals 9

    const/4 v8, 0x0

    const/16 v7, 0xa0

    const/high16 v4, 0x40000000    # 2.0f

    const/4 v6, 0x1

    const/4 v5, 0x0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    iput v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    iput-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPrev:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchZone:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempRectF:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempRect:Landroid/graphics/Rect;

    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mResourceMap:Ljava/util/Map;

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTransactionTouchEvent;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTransactionTouchEvent;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTransactionTouchEvent:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTransactionTouchEvent;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTrivialMovingEn:Z

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchEnable:Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPanBackup:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    const/high16 v1, 0x41200000    # 10.0f

    invoke-static {v6, v1, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    iget v2, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v2, v2

    invoke-static {v6, v2, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    mul-float/2addr v2, v4

    iget v3, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v3, v3

    invoke-static {v6, v3, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    mul-float/2addr v3, v4

    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4, v5, v5, v1, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMinimumResizeRect:Landroid/graphics/RectF;

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1, v5, v5, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMaximumResizeRect:Landroid/graphics/RectF;

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    if-ne v0, v7, :cond_0

    iput v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mDensityDpi:I

    :goto_0
    invoke-virtual {p0, v6, v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setLayerType(ILandroid/graphics/Paint;)V

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    return-void

    :cond_0
    const/16 v0, 0x140

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mDensityDpi:I

    goto :goto_0
.end method

.method private isContained(Ljava/util/ArrayList;Landroid/graphics/PointF;)Z
    .locals 5

    const v4, 0x38d1b717    # 1.0E-4f

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/PointF;

    iget v2, v0, Landroid/graphics/PointF;->x:F

    iget v3, p2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpg-float v2, v2, v4

    if-gez v2, :cond_0

    iget v0, v0, Landroid/graphics/PointF;->y:F

    iget v2, p2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpg-float v0, v0, v4

    if-gez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isMovable()Z
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isMovable()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isObjectAvailable()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isOutOfViewEnabled()Z
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isOutOfViewEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isRotatable()Z
    .locals 2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mStyle:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isRotatable()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private normalizeDegree(F)F
    .locals 4

    const/high16 v3, -0x3c4c0000    # -360.0f

    const/high16 v2, 0x43b40000    # 360.0f

    cmpl-float v0, v3, p1

    if-ltz v0, :cond_3

    add-float v0, p1, v2

    :goto_0
    cmpg-float v1, v2, p1

    if-gtz v1, :cond_0

    sub-float v0, p1, v2

    :cond_0
    cmpl-float v1, v3, v0

    if-gez v1, :cond_1

    cmpg-float v1, v2, v0

    if-gtz v1, :cond_2

    :cond_1
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->normalizeDegree(F)F

    move-result v0

    :cond_2
    return v0

    :cond_3
    move v0, p1

    goto :goto_0
.end method

.method private resize(ILandroid/graphics/PointF;)V
    .locals 10

    const/4 v9, 0x1

    const/4 v7, 0x0

    const/high16 v8, 0x40000000    # 2.0f

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget v2, v0, Landroid/graphics/RectF;->top:F

    iget v4, v0, Landroid/graphics/RectF;->right:F

    iget v5, v0, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v3, v1, v2, v4, v5}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isOutOfViewEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRotation()F

    move-result v1

    invoke-direct {p0, v0, p1, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->checkAllRectOutOfCanvas(Landroid/graphics/RectF;IF)Z

    move-result v1

    if-eqz v1, :cond_1

    iget v1, v3, Landroid/graphics/RectF;->left:F

    iget v2, v3, Landroid/graphics/RectF;->top:F

    iget v4, v3, Landroid/graphics/RectF;->right:F

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v0, v1, v2, v4, v3}, Landroid/graphics/RectF;->set(FFFF)V

    :cond_1
    return-void

    :pswitch_0
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resizeL2R(ILandroid/graphics/PointF;)V

    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resizeT2B(ILandroid/graphics/PointF;)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v5

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v6

    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resizeT2B(ILandroid/graphics/PointF;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getResizeOption()I

    move-result v2

    if-ne v2, v9, :cond_0

    cmpl-float v2, v5, v7

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v2

    mul-float/2addr v2, v6

    mul-float v7, v8, v5

    div-float/2addr v2, v7

    sub-float v2, v4, v2

    iput v2, v1, Landroid/graphics/RectF;->left:F

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v2

    mul-float/2addr v2, v6

    mul-float/2addr v5, v8

    div-float/2addr v2, v5

    add-float/2addr v2, v4

    iput v2, v1, Landroid/graphics/RectF;->right:F

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resizeR2L(ILandroid/graphics/PointF;)V

    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resizeT2B(ILandroid/graphics/PointF;)V

    goto :goto_0

    :pswitch_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v5

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v6

    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resizeL2R(ILandroid/graphics/PointF;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getResizeOption()I

    move-result v2

    if-ne v2, v9, :cond_0

    cmpl-float v2, v6, v7

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v2

    mul-float/2addr v2, v5

    mul-float v7, v8, v6

    div-float/2addr v2, v7

    sub-float v2, v4, v2

    iput v2, v1, Landroid/graphics/RectF;->top:F

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v2

    mul-float/2addr v2, v5

    mul-float v5, v8, v6

    div-float/2addr v2, v5

    add-float/2addr v2, v4

    iput v2, v1, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    :pswitch_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v5

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v6

    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resizeR2L(ILandroid/graphics/PointF;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getResizeOption()I

    move-result v2

    if-ne v2, v9, :cond_0

    cmpl-float v2, v6, v7

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v2

    mul-float/2addr v2, v5

    mul-float v7, v8, v6

    div-float/2addr v2, v7

    sub-float v2, v4, v2

    iput v2, v1, Landroid/graphics/RectF;->top:F

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v2

    mul-float/2addr v2, v5

    mul-float v5, v8, v6

    div-float/2addr v2, v5

    add-float/2addr v2, v4

    iput v2, v1, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_0

    :pswitch_5
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resizeL2R(ILandroid/graphics/PointF;)V

    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resizeB2T(ILandroid/graphics/PointF;)V

    goto/16 :goto_0

    :pswitch_6
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v5

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v6

    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resizeB2T(ILandroid/graphics/PointF;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getResizeOption()I

    move-result v2

    if-ne v2, v9, :cond_0

    cmpl-float v2, v5, v7

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v2

    mul-float/2addr v2, v6

    mul-float v7, v8, v5

    div-float/2addr v2, v7

    sub-float v2, v4, v2

    iput v2, v1, Landroid/graphics/RectF;->left:F

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v2

    mul-float/2addr v2, v6

    mul-float/2addr v5, v8

    div-float/2addr v2, v5

    add-float/2addr v2, v4

    iput v2, v1, Landroid/graphics/RectF;->right:F

    goto/16 :goto_0

    :pswitch_7
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resizeR2L(ILandroid/graphics/PointF;)V

    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resizeB2T(ILandroid/graphics/PointF;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private resize2Threshold(I)V
    .locals 12

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v9

    invoke-virtual {v9}, Landroid/graphics/RectF;->width()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const v2, 0x38d1b717    # 1.0E-4f

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_0

    invoke-virtual {v9}, Landroid/graphics/RectF;->height()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const v2, 0x38d1b717    # 1.0E-4f

    cmpg-float v1, v1, v2

    if-gez v1, :cond_2

    :cond_0
    iget v1, v9, Landroid/graphics/RectF;->left:F

    iput v1, v0, Landroid/graphics/RectF;->left:F

    iget v1, v9, Landroid/graphics/RectF;->right:F

    iput v1, v0, Landroid/graphics/RectF;->right:F

    iget v1, v9, Landroid/graphics/RectF;->bottom:F

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    iget v1, v9, Landroid/graphics/RectF;->top:F

    iput v1, v0, Landroid/graphics/RectF;->top:F

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {v9}, Landroid/graphics/RectF;->width()F

    move-result v1

    invoke-virtual {v9}, Landroid/graphics/RectF;->height()F

    move-result v2

    div-float v6, v1, v2

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMaximumResizeRect:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float v5, v1, v2

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMaximumResizeRect:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float v4, v1, v2

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMinimumResizeRect:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float v3, v1, v2

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMinimumResizeRect:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v2, v1

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpg-float v1, v1, v5

    if-gez v1, :cond_3

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, v3

    if-lez v1, :cond_3

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpl-float v1, v1, v2

    if-lez v1, :cond_3

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpg-float v1, v1, v4

    if-ltz v1, :cond_1

    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getResizeOption()I

    move-result v1

    const/4 v7, 0x1

    if-ne v1, v7, :cond_15

    mul-float v1, v6, v4

    cmpl-float v1, v5, v1

    if-lez v1, :cond_8

    mul-float v1, v6, v4

    :goto_1
    cmpg-float v7, v1, v5

    if-gez v7, :cond_9

    move v5, v1

    :goto_2
    mul-float v1, v6, v2

    cmpg-float v1, v3, v1

    if-gez v1, :cond_a

    mul-float v1, v6, v2

    :goto_3
    cmpl-float v7, v1, v3

    if-lez v7, :cond_b

    move v3, v4

    move v4, v5

    move v11, v2

    move v2, v1

    move v1, v11

    :goto_4
    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v5

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    cmpl-float v5, v5, v4

    if-lez v5, :cond_c

    move v5, v4

    :goto_5
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v6

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    cmpl-float v6, v6, v3

    if-lez v6, :cond_d

    move v6, v3

    :goto_6
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v10

    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v10

    cmpl-float v4, v10, v4

    if-gtz v4, :cond_4

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpg-float v2, v4, v2

    if-gez v2, :cond_14

    :cond_4
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    packed-switch v2, :pswitch_data_0

    :goto_7
    :pswitch_0
    const/4 v2, 0x1

    :goto_8
    const/4 v4, 0x1

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v7

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v7

    cmpl-float v3, v7, v3

    if-gtz v3, :cond_5

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpg-float v1, v3, v1

    if-gez v1, :cond_6

    :cond_5
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    packed-switch v1, :pswitch_data_1

    :goto_9
    :pswitch_1
    const/4 v2, 0x1

    :cond_6
    if-eqz v2, :cond_7

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getResizeOption()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_7

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    packed-switch v1, :pswitch_data_2

    :cond_7
    :goto_a
    :pswitch_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v2, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRotation()F

    move-result v1

    invoke-direct {p0, v2, v9, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getOffsetWhenRotation(ILandroid/graphics/RectF;Landroid/graphics/RectF;F)Landroid/graphics/PointF;

    move-result-object v1

    iget v2, v1, Landroid/graphics/PointF;->x:F

    iget v1, v1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v2, v1}, Landroid/graphics/RectF;->offset(FF)V

    goto/16 :goto_0

    :cond_8
    move v1, v5

    goto/16 :goto_1

    :cond_9
    div-float v4, v5, v6

    goto/16 :goto_2

    :cond_a
    move v1, v3

    goto/16 :goto_3

    :cond_b
    div-float v1, v3, v6

    move v2, v3

    move v3, v4

    move v4, v5

    goto/16 :goto_4

    :cond_c
    move v5, v2

    goto/16 :goto_5

    :cond_d
    move v6, v1

    goto/16 :goto_6

    :pswitch_3
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v2

    const/4 v4, 0x0

    cmpg-float v2, v2, v4

    if-gez v2, :cond_13

    const/4 v2, -0x1

    :goto_b
    iget v4, v9, Landroid/graphics/RectF;->right:F

    int-to-float v2, v2

    mul-float/2addr v2, v5

    sub-float v2, v4, v2

    iput v2, v0, Landroid/graphics/RectF;->left:F

    iget v2, v9, Landroid/graphics/RectF;->right:F

    iput v2, v0, Landroid/graphics/RectF;->right:F

    goto/16 :goto_7

    :pswitch_4
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v2

    const/4 v4, 0x0

    cmpg-float v2, v2, v4

    if-gez v2, :cond_e

    const/4 v7, -0x1

    :cond_e
    iget v2, v9, Landroid/graphics/RectF;->left:F

    int-to-float v4, v7

    mul-float/2addr v4, v5

    add-float/2addr v2, v4

    iput v2, v0, Landroid/graphics/RectF;->right:F

    iget v2, v9, Landroid/graphics/RectF;->left:F

    iput v2, v0, Landroid/graphics/RectF;->left:F

    goto/16 :goto_7

    :pswitch_5
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v2

    const/4 v4, 0x0

    cmpg-float v2, v2, v4

    if-gez v2, :cond_f

    const/4 v7, -0x1

    :cond_f
    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v5, v4

    add-float/2addr v2, v4

    iput v2, v0, Landroid/graphics/RectF;->right:F

    iget v2, v0, Landroid/graphics/RectF;->right:F

    int-to-float v4, v7

    mul-float/2addr v4, v5

    sub-float/2addr v2, v4

    iput v2, v0, Landroid/graphics/RectF;->left:F

    goto/16 :goto_7

    :pswitch_6
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v1

    const/4 v2, 0x0

    cmpg-float v1, v1, v2

    if-gez v1, :cond_12

    const/4 v1, -0x1

    :goto_c
    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float v3, v6, v3

    add-float/2addr v2, v3

    iput v2, v0, Landroid/graphics/RectF;->bottom:F

    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    int-to-float v1, v1

    mul-float/2addr v1, v6

    sub-float v1, v2, v1

    iput v1, v0, Landroid/graphics/RectF;->top:F

    goto/16 :goto_9

    :pswitch_7
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v1

    const/4 v2, 0x0

    cmpg-float v1, v1, v2

    if-gez v1, :cond_10

    const/4 v4, -0x1

    :cond_10
    iget v1, v9, Landroid/graphics/RectF;->bottom:F

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    iget v1, v0, Landroid/graphics/RectF;->bottom:F

    int-to-float v2, v4

    mul-float/2addr v2, v6

    sub-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->top:F

    goto/16 :goto_9

    :pswitch_8
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v1

    const/4 v2, 0x0

    cmpg-float v1, v1, v2

    if-gez v1, :cond_11

    const/4 v4, -0x1

    :cond_11
    iget v1, v9, Landroid/graphics/RectF;->top:F

    int-to-float v2, v4

    mul-float/2addr v2, v6

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    iget v1, v9, Landroid/graphics/RectF;->top:F

    iput v1, v0, Landroid/graphics/RectF;->top:F

    goto/16 :goto_9

    :pswitch_9
    iget v1, v9, Landroid/graphics/RectF;->left:F

    iget v2, v9, Landroid/graphics/RectF;->right:F

    iget v3, v9, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float v2, v5, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    iget v1, v0, Landroid/graphics/RectF;->right:F

    sub-float/2addr v1, v5

    iput v1, v0, Landroid/graphics/RectF;->left:F

    goto/16 :goto_a

    :pswitch_a
    iget v1, v9, Landroid/graphics/RectF;->top:F

    iget v2, v9, Landroid/graphics/RectF;->bottom:F

    iget v3, v9, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float v2, v6, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    iget v1, v0, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v1, v6

    iput v1, v0, Landroid/graphics/RectF;->top:F

    goto/16 :goto_a

    :pswitch_b
    iget v1, v9, Landroid/graphics/RectF;->top:F

    iget v2, v9, Landroid/graphics/RectF;->bottom:F

    iget v3, v9, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float v2, v6, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    iget v1, v0, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v1, v6

    iput v1, v0, Landroid/graphics/RectF;->top:F

    goto/16 :goto_a

    :pswitch_c
    iget v1, v9, Landroid/graphics/RectF;->left:F

    iget v2, v9, Landroid/graphics/RectF;->right:F

    iget v3, v9, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    add-float/2addr v1, v2

    const/high16 v2, 0x40000000    # 2.0f

    div-float v2, v5, v2

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    iget v1, v0, Landroid/graphics/RectF;->right:F

    sub-float/2addr v1, v5

    iput v1, v0, Landroid/graphics/RectF;->left:F

    goto/16 :goto_a

    :cond_12
    move v1, v4

    goto/16 :goto_c

    :cond_13
    move v2, v7

    goto/16 :goto_b

    :cond_14
    move v2, v8

    goto/16 :goto_8

    :cond_15
    move v1, v2

    move v2, v3

    move v3, v4

    move v4, v5

    goto/16 :goto_4

    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_5
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch -0x1
        :pswitch_6
        :pswitch_1
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_1
        :pswitch_1
        :pswitch_8
        :pswitch_8
        :pswitch_8
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x2
        :pswitch_9
        :pswitch_2
        :pswitch_a
        :pswitch_b
        :pswitch_2
        :pswitch_c
    .end packed-switch
.end method

.method private resizeB2T(ILandroid/graphics/PointF;)V
    .locals 3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    iget v1, v0, Landroid/graphics/RectF;->bottom:F

    iget v2, p2, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    return-void
.end method

.method private resizeImage(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;
    .locals 9

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    :try_start_0
    const-string/jumbo v1, "com.samsung.android.sdk.spen30"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v8

    const-string/jumbo v0, "drawable"

    const-string/jumbo v1, "com.samsung.android.sdk.spen30"

    invoke-virtual {v8, p1, v0, v1}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {v8, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v7

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    const/4 v2, 0x1

    int-to-float v5, p2

    invoke-static {v2, v5, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    const/4 v5, 0x1

    int-to-float v6, p3

    invoke-static {v5, v6, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    int-to-float v2, v2

    int-to-float v5, v3

    div-float/2addr v2, v5

    int-to-float v1, v1

    int-to-float v5, v4

    div-float/2addr v1, v5

    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {v5, v2, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v6, 0x1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v0, v8, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    :cond_1
    move-object v0, v7

    goto :goto_0
.end method

.method private resizeL2R(ILandroid/graphics/PointF;)V
    .locals 3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget v2, p2, Landroid/graphics/PointF;->x:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->left:F

    return-void
.end method

.method private resizeR2L(ILandroid/graphics/PointF;)V
    .locals 3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    iget v1, v0, Landroid/graphics/RectF;->right:F

    iget v2, p2, Landroid/graphics/PointF;->x:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    return-void
.end method

.method private resizeT2B(ILandroid/graphics/PointF;)V
    .locals 3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    iget v1, v0, Landroid/graphics/RectF;->top:F

    iget v2, p2, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->top:F

    return-void
.end method

.method private rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTmpMatrix:Landroid/graphics/Matrix;

    if-nez v0, :cond_0

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTmpMatrix:Landroid/graphics/Matrix;

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTmpMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v0, p3, p1, p2}, Landroid/graphics/Matrix;->setRotate(FFF)V

    const/4 v0, 0x2

    new-array v0, v0, [F

    iget v1, p4, Landroid/graphics/PointF;->x:F

    aput v1, v0, v2

    iget v1, p4, Landroid/graphics/PointF;->y:F

    aput v1, v0, v3

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTmpMatrix:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->mapPoints([F)V

    new-instance v1, Landroid/graphics/PointF;

    aget v2, v0, v2

    aget v0, v0, v3

    invoke-direct {v1, v2, v0}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v1
.end method


# virtual methods
.method protected applyChange()V
    .locals 8

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5}, Landroid/graphics/RectF;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "onObjectChanged applyChange "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v6, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mTouchedObjectIndex:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mTouchedObjectIndex:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_4

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mTouchedObjectIndex:I

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderAngle(I)F

    move-result v1

    sub-float/2addr v0, v1

    move v1, v0

    :goto_0
    move v2, v3

    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-direct {p0, v5, v0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "onObjectChanged applyChange "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v0, v5, v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setRect(Landroid/graphics/RectF;Z)V

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    if-eq v2, v6, :cond_2

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRotation()F

    move-result v4

    add-float/2addr v4, v1

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->normalizeDegree(F)F

    move-result v4

    :cond_2
    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getRotateable(I)Z

    move-result v7

    if-eqz v7, :cond_3

    float-to-int v4, v4

    int-to-float v4, v4

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setRotation(F)V

    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_4
    move v1, v0

    goto :goto_0
.end method

.method public close()V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->reset()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->close()Z

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getObjectList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onClosed(Ljava/util/ArrayList;)V

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsClosed:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    return-void
.end method

.method protected draw8Points(Landroid/graphics/drawable/Drawable;Landroid/graphics/Canvas;Landroid/graphics/RectF;Z)V
    .locals 11

    const/high16 v10, 0x42a00000    # 80.0f

    const/high16 v2, 0x40000000    # 2.0f

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    int-to-float v0, v0

    div-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v1, v1

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {p3}, Landroid/graphics/RectF;->centerX()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-virtual {p3}, Landroid/graphics/RectF;->centerY()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p3, v4}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    iget v6, v4, Landroid/graphics/Rect;->left:I

    sub-int/2addr v6, v0

    iget v7, v4, Landroid/graphics/Rect;->top:I

    sub-int/2addr v7, v1

    iget v8, v4, Landroid/graphics/Rect;->left:I

    add-int/2addr v8, v0

    iget v9, v4, Landroid/graphics/Rect;->top:I

    add-int/2addr v9, v1

    invoke-virtual {v5, v6, v7, v8, v9}, Landroid/graphics/Rect;->set(IIII)V

    invoke-virtual {p1, v5}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    invoke-virtual {p1, p2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    iget v6, v4, Landroid/graphics/Rect;->right:I

    sub-int/2addr v6, v0

    iget v7, v4, Landroid/graphics/Rect;->top:I

    sub-int/2addr v7, v1

    iget v8, v4, Landroid/graphics/Rect;->right:I

    add-int/2addr v8, v0

    iget v9, v4, Landroid/graphics/Rect;->top:I

    add-int/2addr v9, v1

    invoke-virtual {v5, v6, v7, v8, v9}, Landroid/graphics/Rect;->set(IIII)V

    invoke-virtual {p1, v5}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    invoke-virtual {p1, p2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    iget v6, v4, Landroid/graphics/Rect;->left:I

    sub-int/2addr v6, v0

    iget v7, v4, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v7, v1

    iget v8, v4, Landroid/graphics/Rect;->left:I

    add-int/2addr v8, v0

    iget v9, v4, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v9, v1

    invoke-virtual {v5, v6, v7, v8, v9}, Landroid/graphics/Rect;->set(IIII)V

    invoke-virtual {p1, v5}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    invoke-virtual {p1, p2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    iget v6, v4, Landroid/graphics/Rect;->right:I

    sub-int/2addr v6, v0

    iget v7, v4, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v7, v1

    iget v8, v4, Landroid/graphics/Rect;->right:I

    add-int/2addr v8, v0

    iget v9, v4, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v9, v1

    invoke-virtual {v5, v6, v7, v8, v9}, Landroid/graphics/Rect;->set(IIII)V

    invoke-virtual {p1, v5}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    invoke-virtual {p1, p2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p3}, Landroid/graphics/RectF;->width()F

    move-result v6

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    cmpl-float v6, v6, v10

    if-lez v6, :cond_0

    sub-int v6, v2, v0

    iget v7, v4, Landroid/graphics/Rect;->top:I

    sub-int/2addr v7, v1

    add-int v8, v2, v0

    iget v9, v4, Landroid/graphics/Rect;->top:I

    add-int/2addr v9, v1

    invoke-virtual {v5, v6, v7, v8, v9}, Landroid/graphics/Rect;->set(IIII)V

    invoke-virtual {p1, v5}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    invoke-virtual {p1, p2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    sub-int v6, v2, v0

    iget v7, v4, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v7, v1

    add-int/2addr v2, v0

    iget v8, v4, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v8, v1

    invoke-virtual {v5, v6, v7, v2, v8}, Landroid/graphics/Rect;->set(IIII)V

    invoke-virtual {p1, v5}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    invoke-virtual {p1, p2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    invoke-virtual {p3}, Landroid/graphics/RectF;->height()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v2, v2, v10

    if-lez v2, :cond_1

    iget v2, v4, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v0

    sub-int v6, v3, v1

    iget v7, v4, Landroid/graphics/Rect;->left:I

    add-int/2addr v7, v0

    add-int v8, v3, v1

    invoke-virtual {v5, v2, v6, v7, v8}, Landroid/graphics/Rect;->set(IIII)V

    invoke-virtual {p1, v5}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    invoke-virtual {p1, p2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    iget v2, v4, Landroid/graphics/Rect;->right:I

    sub-int/2addr v2, v0

    sub-int v6, v3, v1

    iget v4, v4, Landroid/graphics/Rect;->right:I

    add-int/2addr v0, v4

    add-int/2addr v1, v3

    invoke-virtual {v5, v2, v6, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    invoke-virtual {p1, v5}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    invoke-virtual {p1, p2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_1
    return-void
.end method

.method protected drawHighlightObject(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectOutlineEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    instance-of v0, p2, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    if-eqz v0, :cond_1

    check-cast p2, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->drawHighlightStroke(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;)V

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->drawHighlightRect(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    goto :goto_0
.end method

.method public fit()V
    .locals 2

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    :cond_2
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->updateContextMenu()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->show()V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->fit(ILcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method protected fitRotateAngle2BorderAngle()V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderAngle(I)F

    move-result v1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected getBoundBox(I)Landroid/graphics/RectF;
    .locals 12

    const/4 v8, 0x1

    const/4 v10, 0x0

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, -0x1

    if-ne p1, v0, :cond_2

    new-instance v1, Landroid/graphics/RectF;

    invoke-virtual {p0, v10}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBoundBox(I)Landroid/graphics/RectF;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    move v0, v8

    :goto_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lt v0, v2, :cond_1

    move-object v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBoundBox(I)Landroid/graphics/RectF;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/RectF;->union(Landroid/graphics/RectF;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotX(I)F

    move-result v3

    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotY(I)F

    move-result v4

    const/4 v0, 0x4

    new-array v11, v0, [Landroid/graphics/PointF;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/graphics/RectF;

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderAngle(I)F

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mTouchedObjectIndex:I

    if-ne p1, v1, :cond_8

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    move v9, v0

    :goto_2
    iget v0, v7, Landroid/graphics/RectF;->left:F

    float-to-int v1, v0

    iget v0, v7, Landroid/graphics/RectF;->top:F

    float-to-int v2, v0

    float-to-double v5, v9

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getRotatePoint(IIFFD)Landroid/graphics/PointF;

    move-result-object v0

    aput-object v0, v11, v10

    iget v0, v7, Landroid/graphics/RectF;->right:F

    float-to-int v1, v0

    iget v0, v7, Landroid/graphics/RectF;->top:F

    float-to-int v2, v0

    float-to-double v5, v9

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getRotatePoint(IIFFD)Landroid/graphics/PointF;

    move-result-object v0

    aput-object v0, v11, v8

    const/4 v8, 0x2

    iget v0, v7, Landroid/graphics/RectF;->left:F

    float-to-int v1, v0

    iget v0, v7, Landroid/graphics/RectF;->bottom:F

    float-to-int v2, v0

    float-to-double v5, v9

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getRotatePoint(IIFFD)Landroid/graphics/PointF;

    move-result-object v0

    aput-object v0, v11, v8

    const/4 v8, 0x3

    iget v0, v7, Landroid/graphics/RectF;->right:F

    float-to-int v1, v0

    iget v0, v7, Landroid/graphics/RectF;->bottom:F

    float-to-int v2, v0

    float-to-double v5, v9

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getRotatePoint(IIFFD)Landroid/graphics/PointF;

    move-result-object v0

    aput-object v0, v11, v8

    aget-object v0, v11, v10

    iget v3, v0, Landroid/graphics/PointF;->x:F

    aget-object v0, v11, v10

    iget v2, v0, Landroid/graphics/PointF;->y:F

    aget-object v0, v11, v10

    iget v1, v0, Landroid/graphics/PointF;->x:F

    aget-object v0, v11, v10

    iget v0, v0, Landroid/graphics/PointF;->y:F

    move v4, v3

    move v3, v2

    move v2, v1

    move v1, v0

    move v0, v10

    :goto_3
    array-length v5, v11

    if-lt v0, v5, :cond_3

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v4, v3, v2, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    goto/16 :goto_0

    :cond_3
    aget-object v5, v11, v0

    iget v5, v5, Landroid/graphics/PointF;->x:F

    cmpl-float v5, v4, v5

    if-ltz v5, :cond_4

    aget-object v4, v11, v0

    iget v4, v4, Landroid/graphics/PointF;->x:F

    :cond_4
    aget-object v5, v11, v0

    iget v5, v5, Landroid/graphics/PointF;->x:F

    cmpg-float v5, v2, v5

    if-gtz v5, :cond_5

    aget-object v2, v11, v0

    iget v2, v2, Landroid/graphics/PointF;->x:F

    :cond_5
    aget-object v5, v11, v0

    iget v5, v5, Landroid/graphics/PointF;->y:F

    cmpl-float v5, v3, v5

    if-ltz v5, :cond_6

    aget-object v3, v11, v0

    iget v3, v3, Landroid/graphics/PointF;->y:F

    :cond_6
    aget-object v5, v11, v0

    iget v5, v5, Landroid/graphics/PointF;->y:F

    cmpg-float v5, v1, v5

    if-gtz v5, :cond_7

    aget-object v1, v11, v0

    iget v1, v1, Landroid/graphics/PointF;->y:F

    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_8
    move v9, v0

    goto/16 :goto_2
.end method

.method public getContextMenu()Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mItemList:Ljava/util/ArrayList;

    return-object v0
.end method

.method protected getControlPivotX(I)F
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isResizeZonePressed()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v0

    goto :goto_0
.end method

.method protected getControlPivotY(I)F
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isResizeZonePressed()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    move-result v0

    goto :goto_0
.end method

.method protected getDrawableImage(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 8

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mResourceMap:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Landroid/graphics/drawable/Drawable;

    if-eqz v7, :cond_0

    :goto_0
    return-object v7

    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string/jumbo v3, "com.samsung.android.sdk.spen30"

    invoke-virtual {v1, v3}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v1

    const-string/jumbo v3, "drawable"

    const-string/jumbo v4, "com.samsung.android.sdk.spen30"

    invoke-virtual {v1, p1, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v7

    :goto_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mResourceMap:Ljava/util/Map;

    invoke-interface {v1, p1, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    :try_start_1
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/Class;->getResource(Ljava/lang/String;)Ljava/net/URL;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v1

    if-eqz v1, :cond_5

    :try_start_2
    invoke-virtual {v1}, Ljava/net/URL;->openStream()Ljava/io/InputStream;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v1

    :goto_2
    :try_start_3
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    if-eqz v1, :cond_3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mDensityDpi:I

    iput v4, v3, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    invoke-static {v1, v5, v3}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_3
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_3 .. :try_end_3} :catch_2

    move-result-object v3

    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_4} :catch_2

    if-eqz v3, :cond_2

    :try_start_5
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getNinePatchChunk()[B

    move-result-object v4

    invoke-static {v4}, Landroid/graphics/NinePatch;->isNinePatchChunk([B)Z

    move-result v1

    if-eqz v1, :cond_4

    new-instance v1, Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/4 v6, 0x0

    invoke-direct/range {v1 .. v6}, Landroid/graphics/drawable/NinePatchDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;[BLandroid/graphics/Rect;Ljava/lang/String;)V

    move-object v2, v1

    :cond_2
    :goto_3
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mResourceMap:Ljava/util/Map;

    move-object v0, v2

    check-cast v0, Landroid/graphics/drawable/Drawable;

    move-object v1, v0

    invoke-interface {v3, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    check-cast v2, Landroid/graphics/drawable/Drawable;

    move-object v7, v2

    goto :goto_0

    :catch_0
    move-exception v1

    move-object v7, v2

    goto :goto_0

    :catch_1
    move-exception v1

    move-object v7, v2

    goto :goto_0

    :cond_3
    move-object v7, v2

    goto :goto_0

    :cond_4
    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v2, v1, v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V
    :try_end_5
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_3

    :catch_2
    move-exception v1

    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1

    :cond_5
    move-object v1, v2

    goto :goto_2
.end method

.method public getMinResizeRect()Landroid/graphics/RectF;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMinimumResizeRect:Landroid/graphics/RectF;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMinimumResizeRect:Landroid/graphics/RectF;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getObjectList()Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getPixel(II)I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getRect()Landroid/graphics/RectF;
    .locals 4

    const/4 v0, 0x0

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    :goto_0
    return-object v0

    :cond_0
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2, v3, v3, v3, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_1

    move-object v0, v2

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    invoke-virtual {v2, v0}, Landroid/graphics/RectF;->union(Landroid/graphics/RectF;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method protected getRectList()Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    return-object v0
.end method

.method protected getRotatePoint(IIFFD)Landroid/graphics/PointF;
    .locals 13

    invoke-static/range {p5 .. p6}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Math;->cos(D)D

    move-result-wide v3

    invoke-static {v1, v2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v1

    int-to-float v5, p1

    sub-float v5, v5, p3

    float-to-double v5, v5

    int-to-float v7, p2

    sub-float v7, v7, p4

    float-to-double v7, v7

    mul-double v9, v5, v3

    mul-double v11, v7, v1

    sub-double/2addr v9, v11

    move/from16 v0, p3

    float-to-double v11, v0

    add-double/2addr v9, v11

    mul-double/2addr v1, v5

    mul-double/2addr v3, v7

    add-double/2addr v1, v3

    move/from16 v0, p4

    float-to-double v3, v0

    add-double/2addr v1, v3

    new-instance v3, Landroid/graphics/PointF;

    double-to-float v4, v9

    double-to-float v1, v1

    invoke-direct {v3, v4, v1}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v3
.end method

.method getStartEndBitmapCalcPoint(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;F)V
    .locals 8

    const/high16 v7, 0x42b40000    # 90.0f

    const v6, 0x3c8efa35

    const/4 v3, 0x0

    iget v0, p1, Landroid/graphics/PointF;->x:F

    iget v1, p2, Landroid/graphics/PointF;->x:F

    sub-float v1, v0, v1

    iget v0, p1, Landroid/graphics/PointF;->y:F

    iget v2, p2, Landroid/graphics/PointF;->y:F

    sub-float v2, v0, v2

    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->QUADRANT_MAX:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    cmpl-float v0, v1, v3

    if-ltz v0, :cond_0

    cmpl-float v0, v2, v3

    if-ltz v0, :cond_0

    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->QUADRANT_1:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    :goto_0
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    float-to-double v2, v2

    float-to-double v4, v1

    div-double v1, v2, v4

    invoke-static {v1, v2}, Ljava/lang/Math;->atan(D)D

    move-result-wide v1

    const-wide v3, 0x4066800000000000L    # 180.0

    mul-double/2addr v1, v3

    const-wide v3, 0x400921fb60000000L    # 3.1415927410125732

    div-double/2addr v1, v3

    double-to-float v1, v1

    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->$SWITCH_TABLE$com$samsung$android$sdk$pen$engine$SpenControlBase$QUADRANT()[I

    move-result-object v2

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    :goto_1
    return-void

    :cond_0
    cmpg-float v0, v1, v3

    if-gez v0, :cond_1

    cmpl-float v0, v2, v3

    if-ltz v0, :cond_1

    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->QUADRANT_2:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    goto :goto_0

    :cond_1
    cmpl-float v0, v1, v3

    if-ltz v0, :cond_2

    cmpg-float v0, v2, v3

    if-gez v0, :cond_2

    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->QUADRANT_3:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;->QUADRANT_4:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$QUADRANT;

    goto :goto_0

    :pswitch_0
    sub-float v0, v7, v1

    mul-float/2addr v0, v6

    iget v1, p2, Landroid/graphics/PointF;->x:F

    float-to-double v1, v1

    float-to-double v3, p4

    float-to-double v5, v0

    invoke-static {v5, v6}, Ljava/lang/Math;->sin(D)D

    move-result-wide v5

    mul-double/2addr v3, v5

    add-double/2addr v1, v3

    double-to-float v1, v1

    iput v1, p3, Landroid/graphics/PointF;->x:F

    iget v1, p2, Landroid/graphics/PointF;->y:F

    float-to-double v1, v1

    float-to-double v3, p4

    float-to-double v5, v0

    invoke-static {v5, v6}, Ljava/lang/Math;->cos(D)D

    move-result-wide v5

    mul-double/2addr v3, v5

    add-double v0, v1, v3

    double-to-float v0, v0

    iput v0, p3, Landroid/graphics/PointF;->y:F

    goto :goto_1

    :pswitch_1
    mul-float v0, v1, v6

    iget v1, p2, Landroid/graphics/PointF;->x:F

    float-to-double v1, v1

    float-to-double v3, p4

    float-to-double v5, v0

    invoke-static {v5, v6}, Ljava/lang/Math;->cos(D)D

    move-result-wide v5

    mul-double/2addr v3, v5

    sub-double/2addr v1, v3

    double-to-float v1, v1

    iput v1, p3, Landroid/graphics/PointF;->x:F

    iget v1, p2, Landroid/graphics/PointF;->y:F

    float-to-double v1, v1

    float-to-double v3, p4

    float-to-double v5, v0

    invoke-static {v5, v6}, Ljava/lang/Math;->sin(D)D

    move-result-wide v5

    mul-double/2addr v3, v5

    add-double v0, v1, v3

    double-to-float v0, v0

    iput v0, p3, Landroid/graphics/PointF;->y:F

    goto :goto_1

    :pswitch_2
    mul-float v0, v1, v6

    iget v1, p2, Landroid/graphics/PointF;->x:F

    float-to-double v1, v1

    float-to-double v3, p4

    float-to-double v5, v0

    invoke-static {v5, v6}, Ljava/lang/Math;->cos(D)D

    move-result-wide v5

    mul-double/2addr v3, v5

    add-double/2addr v1, v3

    double-to-float v1, v1

    iput v1, p3, Landroid/graphics/PointF;->x:F

    iget v1, p2, Landroid/graphics/PointF;->y:F

    float-to-double v1, v1

    float-to-double v3, p4

    float-to-double v5, v0

    invoke-static {v5, v6}, Ljava/lang/Math;->sin(D)D

    move-result-wide v5

    mul-double/2addr v3, v5

    sub-double v0, v1, v3

    double-to-float v0, v0

    iput v0, p3, Landroid/graphics/PointF;->y:F

    goto :goto_1

    :pswitch_3
    sub-float v0, v7, v1

    mul-float/2addr v0, v6

    iget v1, p2, Landroid/graphics/PointF;->x:F

    float-to-double v1, v1

    float-to-double v3, p4

    float-to-double v5, v0

    invoke-static {v5, v6}, Ljava/lang/Math;->sin(D)D

    move-result-wide v5

    mul-double/2addr v3, v5

    sub-double/2addr v1, v3

    double-to-float v1, v1

    iput v1, p3, Landroid/graphics/PointF;->x:F

    iget v1, p2, Landroid/graphics/PointF;->y:F

    float-to-double v1, v1

    float-to-double v3, p4

    float-to-double v5, v0

    invoke-static {v5, v6}, Ljava/lang/Math;->cos(D)D

    move-result-wide v5

    mul-double/2addr v3, v5

    sub-double v0, v1, v3

    double-to-float v0, v0

    iput v0, p3, Landroid/graphics/PointF;->y:F

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getStyle()I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mStyle:I

    return v0
.end method

.method protected handleMoveControl(Landroid/graphics/Point;Landroid/graphics/Point;)V
    .locals 9

    const/4 v0, 0x0

    const/high16 v5, 0x41a00000    # 20.0f

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isMovable()Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPrev:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    iget v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->x:F

    sub-float v3, v1, v2

    iget v1, p1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPrev:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    iget v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->y:F

    sub-float v4, v1, v2

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpg-float v1, v1, v5

    if-gez v1, :cond_2

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpg-float v1, v1, v5

    if-gez v1, :cond_2

    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTrivialMovingEn:Z

    if-nez v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "trivalMoving blocked: chg_X, chg_Y="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    goto :goto_0

    :cond_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTrivialMovingEn:Z

    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5}, Landroid/graphics/RectF;-><init>()V

    move v2, v0

    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v2, v0, :cond_3

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->invalidate()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPrev:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    iget v1, p1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iput v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->x:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPrev:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    iget v1, p1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    iput v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->y:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPrev:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    iget v1, p2, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iput v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->rotated_x:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPrev:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    iget v1, p2, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    iput v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->rotated_y:F

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    iget v1, v0, Landroid/graphics/RectF;->left:F

    iget v6, v0, Landroid/graphics/RectF;->top:F

    iget v7, v0, Landroid/graphics/RectF;->right:F

    iget v8, v0, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v5, v1, v6, v7, v8}, Landroid/graphics/RectF;->set(FFFF)V

    iget v1, v0, Landroid/graphics/RectF;->left:F

    add-float/2addr v1, v3

    iget v6, v0, Landroid/graphics/RectF;->top:F

    add-float/2addr v6, v4

    iget v7, v0, Landroid/graphics/RectF;->right:F

    add-float/2addr v7, v3

    iget v8, v0, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v8, v4

    invoke-virtual {v0, v1, v6, v7, v8}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isOutOfViewEnabled()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRotation()F

    move-result v1

    invoke-direct {p0, v0, v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->checkAllRectOutOfCanvas(Landroid/graphics/RectF;IF)Z

    move-result v1

    if-eqz v1, :cond_4

    iget v1, v5, Landroid/graphics/RectF;->left:F

    iget v6, v5, Landroid/graphics/RectF;->top:F

    iget v7, v5, Landroid/graphics/RectF;->right:F

    iget v8, v5, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v0, v1, v6, v7, v8}, Landroid/graphics/RectF;->set(FFFF)V

    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onRectChanged(Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_1
.end method

.method protected handleResizeControl(Landroid/graphics/PointF;Landroid/graphics/PointF;)Z
    .locals 16

    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    new-instance v3, Landroid/graphics/PointF;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v3, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    move-object/from16 v0, p2

    iget v1, v0, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPrev:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    iget v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->rotated_x:F

    sub-float/2addr v1, v2

    iput v1, v3, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p2

    iget v1, v0, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPrev:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    iget v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->rotated_y:F

    sub-float/2addr v1, v2

    iput v1, v3, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v2, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mTouchedObjectIndex:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getResizeOption()I

    move-result v1

    const/4 v4, 0x2

    if-ne v1, v4, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findResizeRate(Landroid/graphics/PointF;Landroid/graphics/RectF;)Landroid/graphics/PointF;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v3, 0x6

    if-eq v1, v3, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v3, 0x3

    if-ne v1, v3, :cond_5

    :cond_2
    const/4 v1, -0x1

    iget v3, v12, Landroid/graphics/PointF;->x:F

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget v4, v12, Landroid/graphics/PointF;->y:F

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_4

    iget v3, v12, Landroid/graphics/PointF;->x:F

    int-to-float v1, v1

    mul-float/2addr v1, v3

    iput v1, v12, Landroid/graphics/PointF;->y:F

    :cond_3
    :goto_1
    new-instance v13, Landroid/graphics/RectF;

    invoke-direct {v13}, Landroid/graphics/RectF;-><init>()V

    new-instance v14, Landroid/graphics/RectF;

    invoke-direct {v14}, Landroid/graphics/RectF;-><init>()V

    new-instance v15, Landroid/graphics/PointF;

    const/4 v1, 0x0

    const/4 v3, 0x0

    invoke-direct {v15, v1, v3}, Landroid/graphics/PointF;-><init>(FF)V

    const/4 v1, 0x0

    move v11, v1

    :goto_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v11, v1, :cond_8

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->invalidate()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPrev:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    move-object/from16 v0, p1

    iget v2, v0, Landroid/graphics/PointF;->x:F

    iput v2, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->x:F

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPrev:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    move-object/from16 v0, p1

    iget v2, v0, Landroid/graphics/PointF;->y:F

    iput v2, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->y:F

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPrev:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    move-object/from16 v0, p2

    iget v2, v0, Landroid/graphics/PointF;->x:F

    iput v2, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->rotated_x:F

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPrev:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    move-object/from16 v0, p2

    iget v2, v0, Landroid/graphics/PointF;->y:F

    iput v2, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->rotated_y:F

    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_4
    iget v3, v12, Landroid/graphics/PointF;->y:F

    int-to-float v1, v1

    mul-float/2addr v1, v3

    iput v1, v12, Landroid/graphics/PointF;->x:F

    goto :goto_1

    :cond_5
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/16 v3, 0x8

    if-eq v1, v3, :cond_6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v3, 0x1

    if-ne v1, v3, :cond_3

    :cond_6
    const/4 v1, 0x1

    iget v3, v12, Landroid/graphics/PointF;->x:F

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    iget v4, v12, Landroid/graphics/PointF;->y:F

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpl-float v3, v3, v4

    if-lez v3, :cond_7

    iget v3, v12, Landroid/graphics/PointF;->x:F

    int-to-float v1, v1

    mul-float/2addr v1, v3

    iput v1, v12, Landroid/graphics/PointF;->y:F

    goto :goto_1

    :cond_7
    iget v3, v12, Landroid/graphics/PointF;->y:F

    int-to-float v1, v1

    mul-float/2addr v1, v3

    iput v1, v12, Landroid/graphics/PointF;->x:F

    goto/16 :goto_1

    :cond_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v1, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/RectF;

    invoke-virtual {v13, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v3

    iget v4, v12, Landroid/graphics/PointF;->x:F

    mul-float/2addr v3, v4

    iput v3, v15, Landroid/graphics/PointF;->x:F

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    iget v3, v12, Landroid/graphics/PointF;->y:F

    mul-float/2addr v1, v3

    iput v1, v15, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v15}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resize(ILandroid/graphics/PointF;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isOutOfViewEnabled()Z

    move-result v1

    if-nez v1, :cond_9

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRotation()F

    move-result v8

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getResizeOption()I

    move-result v1

    const/4 v9, 0x1

    if-ne v1, v9, :cond_a

    const/4 v9, 0x1

    :goto_3
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v10

    invoke-virtual {v10}, Landroid/graphics/RectF;->height()F

    move-result v10

    div-float v10, v1, v10

    move-object/from16 v1, p0

    invoke-virtual/range {v1 .. v10}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isClippedObject(IZZZFFFZF)Z

    move-result v1

    if-eqz v1, :cond_9

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/RectF;

    invoke-virtual {v1, v13}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    :cond_9
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v1, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v3, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onRectChanged(Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v1

    if-nez v1, :cond_b

    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_a
    const/4 v9, 0x0

    goto :goto_3

    :cond_b
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v1, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/RectF;

    invoke-virtual {v14, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    iget v1, v13, Landroid/graphics/RectF;->right:F

    iget v3, v13, Landroid/graphics/RectF;->left:F

    sub-float/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Math;->signum(F)F

    move-result v1

    iget v3, v14, Landroid/graphics/RectF;->right:F

    iget v4, v14, Landroid/graphics/RectF;->left:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->signum(F)F

    move-result v3

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_c

    const/4 v3, 0x2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onFlip(ILcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    :cond_c
    iget v1, v13, Landroid/graphics/RectF;->bottom:F

    iget v3, v13, Landroid/graphics/RectF;->top:F

    sub-float/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Math;->signum(F)F

    move-result v1

    iget v3, v14, Landroid/graphics/RectF;->bottom:F

    iget v4, v14, Landroid/graphics/RectF;->top:F

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->signum(F)F

    move-result v3

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_d

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-object/from16 v0, p0

    invoke-virtual {v0, v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onFlip(ILcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    :cond_d
    add-int/lit8 v1, v11, 0x1

    move v11, v1

    goto/16 :goto_2
.end method

.method protected handleRotationControl(Landroid/graphics/Point;)V
    .locals 14

    const/4 v2, 0x1

    const/4 v5, 0x0

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v13, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mTouchedObjectIndex:I

    invoke-virtual {p0, v13}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotY(I)F

    move-result v0

    iget v1, p1, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget v1, p1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    invoke-virtual {p0, v13}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotX(I)F

    move-result v4

    sub-float/2addr v1, v4

    float-to-double v6, v1

    float-to-double v0, v0

    invoke-static {v6, v7, v0, v1}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    const-wide v6, 0x4066800000000000L    # 180.0

    mul-double/2addr v0, v6

    const-wide v6, 0x400921fb54442d18L    # Math.PI

    div-double/2addr v0, v6

    double-to-float v0, v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->normalizeDegree(F)F

    move-result v10

    move v1, v3

    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_7

    move v11, v2

    :goto_2
    if-nez v11, :cond_2

    move v1, v3

    :goto_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_9

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchZone:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;

    invoke-direct {p0, v13}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->getRect(ILandroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    move-result v0

    invoke-virtual {p0, v13}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotX(I)F

    move-result v2

    invoke-virtual {p0, v13}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotY(I)F

    move-result v4

    sub-float v5, v2, v1

    sub-float v1, v2, v1

    mul-float/2addr v1, v5

    sub-float v5, v4, v0

    sub-float v0, v4, v0

    mul-float/2addr v0, v5

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    iget v5, p1, Landroid/graphics/Point;->x:I

    int-to-float v5, v5

    sub-float v5, v2, v5

    iget v6, p1, Landroid/graphics/Point;->x:I

    int-to-float v6, v6

    sub-float/2addr v2, v6

    mul-float/2addr v2, v5

    iget v5, p1, Landroid/graphics/Point;->y:I

    int-to-float v5, v5

    sub-float v5, v4, v5

    iget v6, p1, Landroid/graphics/Point;->y:I

    int-to-float v6, v6

    sub-float/2addr v4, v6

    mul-float/2addr v4, v5

    add-float/2addr v2, v4

    float-to-double v4, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v2, v10

    float-to-int v2, v2

    int-to-float v2, v2

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    const-wide v6, 0x4051800000000000L    # 70.0

    add-double/2addr v0, v6

    cmpg-double v0, v4, v0

    if-gez v0, :cond_4

    const/high16 v0, 0x43320000    # 178.0f

    cmpl-float v0, v10, v0

    if-lez v0, :cond_d

    const/high16 v0, 0x43380000    # 184.0f

    cmpg-float v0, v10, v0

    if-gez v0, :cond_d

    const/high16 v0, 0x43340000    # 180.0f

    :goto_4
    const/high16 v1, -0x3cc80000    # -184.0f

    cmpl-float v1, v0, v1

    if-lez v1, :cond_3

    const/high16 v1, -0x3cce0000    # -178.0f

    cmpg-float v1, v0, v1

    if-gez v1, :cond_3

    const/high16 v0, -0x3ccc0000    # -180.0f

    :cond_3
    float-to-int v0, v0

    div-int/lit8 v0, v0, 0x5

    mul-int/lit8 v0, v0, 0x5

    int-to-float v0, v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    :cond_4
    if-eqz v11, :cond_5

    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    move v1, v3

    :goto_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_a

    :cond_5
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->invalidate()V

    :goto_6
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_0

    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getRotateable(I)Z

    move-result v0

    if-eqz v0, :cond_6

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    if-eq v3, v13, :cond_c

    const/4 v1, -0x1

    if-eq v13, v1, :cond_c

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    invoke-direct {p0, v13}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderAngle(I)F

    move-result v1

    sub-float/2addr v0, v1

    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderAngle(I)F

    move-result v1

    add-float/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->normalizeDegree(F)F

    move-result v0

    move v1, v0

    :goto_7
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {p0, v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onRotationChanged(FLcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    :cond_7
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isOutOfViewEnabled()Z

    move-result v0

    if-nez v0, :cond_8

    move v11, v3

    goto/16 :goto_2

    :cond_8
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_1

    :cond_9
    float-to-int v0, v10

    int-to-float v7, v0

    move-object v0, p0

    move v4, v3

    move v6, v5

    move v8, v3

    move v9, v5

    invoke-virtual/range {v0 .. v9}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isClippedObject(IZZZFFFZF)Z

    move-result v0

    if-nez v0, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_3

    :cond_a
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    iget v4, v0, Landroid/graphics/RectF;->left:F

    iget v5, v0, Landroid/graphics/RectF;->top:F

    iget v6, v0, Landroid/graphics/RectF;->right:F

    iget v7, v0, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v2, v4, v5, v6, v7}, Landroid/graphics/RectF;->set(FFFF)V

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    invoke-direct {p0, v0, v1, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->checkAllRectOutOfCanvas(Landroid/graphics/RectF;IF)Z

    move-result v0

    if-eqz v0, :cond_b

    iput v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    goto/16 :goto_0

    :cond_b
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_5

    :cond_c
    move v1, v0

    goto :goto_7

    :cond_d
    move v0, v10

    goto/16 :goto_4
.end method

.method protected handleTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 12

    const/4 v10, 0x1

    const/4 v9, 0x0

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v9

    :cond_1
    new-instance v11, Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    invoke-direct {v11, v0, v1}, Landroid/graphics/Point;-><init>(II)V

    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    and-int/lit16 v1, v1, 0xff

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    iput-boolean v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFlipDirectionHorizontal:Z

    iput-boolean v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFlipDirectionVertical:Z

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPrev:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    iget v2, v11, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    iput v2, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->x:F

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPrev:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    iget v2, v11, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    iput v2, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->y:F

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->reset()V

    move v8, v9

    :goto_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v8, v1, :cond_2

    move-object v6, v0

    :goto_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPrev:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    iget v1, v6, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iput v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->rotated_x:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPrev:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;

    iget v1, v6, Landroid/graphics/Point;->y:I

    int-to-float v1, v1

    iput v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPositionBackup;->rotated_y:F

    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "Obj ["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mTouchedObjectIndex:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "] mRotateAngle = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "] pos_rotated ="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v9, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isPressed()Z

    move-result v0

    if-eqz v0, :cond_0

    move v9, v10

    goto/16 :goto_0

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    iget v1, v11, Landroid/graphics/Point;->x:I

    iget v2, v11, Landroid/graphics/Point;->y:I

    invoke-virtual {p0, v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotX(I)F

    move-result v3

    invoke-virtual {p0, v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotY(I)F

    move-result v4

    invoke-direct {p0, v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderAngle(I)F

    move-result v0

    neg-float v0, v0

    float-to-double v5, v0

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getRotatePoint(IIFFD)Landroid/graphics/PointF;

    move-result-object v0

    new-instance v6, Landroid/graphics/Point;

    iget v1, v0, Landroid/graphics/PointF;->x:F

    float-to-int v1, v1

    iget v0, v0, Landroid/graphics/PointF;->y:F

    float-to-int v0, v0

    invoke-direct {v6, v1, v0}, Landroid/graphics/Point;-><init>(II)V

    invoke-direct {p0, v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v1

    iget v2, v6, Landroid/graphics/Point;->x:I

    iget v3, v6, Landroid/graphics/Point;->y:I

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    move-object v0, p0

    move-object v4, v7

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onCheckTouchPosition(Landroid/graphics/RectF;IILcom/samsung/android/sdk/pen/document/SpenObjectBase;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isPressed()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iput v8, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mTouchedObjectIndex:I

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRotation()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    goto/16 :goto_2

    :cond_3
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    move-object v0, v6

    goto/16 :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->close()V

    goto/16 :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mTouchedObjectIndex:I

    if-ltz v0, :cond_e

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mTouchedObjectIndex:I

    :goto_3
    iget v1, v11, Landroid/graphics/Point;->x:I

    iget v2, v11, Landroid/graphics/Point;->y:I

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotX(I)F

    move-result v3

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotY(I)F

    move-result v4

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderAngle(I)F

    move-result v0

    neg-float v0, v0

    float-to-double v5, v0

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getRotatePoint(IIFFD)Landroid/graphics/PointF;

    move-result-object v1

    new-instance v2, Landroid/graphics/Point;

    iget v0, v1, Landroid/graphics/PointF;->x:F

    float-to-int v0, v0

    iget v3, v1, Landroid/graphics/PointF;->y:F

    float-to-int v3, v3

    invoke-direct {v2, v0, v3}, Landroid/graphics/Point;-><init>(II)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isRotateZonePressed()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isRotatable()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0, v11}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->handleRotationControl(Landroid/graphics/Point;)V

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isResizeZonePressed()Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-direct {v0, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    new-instance v2, Landroid/graphics/PointF;

    iget v3, v1, Landroid/graphics/PointF;->x:F

    iget v1, v1, Landroid/graphics/PointF;->y:F

    invoke-direct {v2, v3, v1}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {p0, v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->handleResizeControl(Landroid/graphics/PointF;Landroid/graphics/PointF;)Z

    move-result v9

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isMoveZonePressed()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p0, v11, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->handleMoveControl(Landroid/graphics/Point;Landroid/graphics/Point;)V

    move v9, v10

    goto/16 :goto_0

    :cond_6
    move v9, v10

    goto/16 :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isResizeZonePressed()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isRotateZonePressed()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isMoveZonePressed()Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_7
    move v1, v9

    :goto_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_a

    :cond_8
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isResizeZonePressed()Z

    move-result v0

    if-eqz v0, :cond_9

    :goto_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v9, v0, :cond_b

    :cond_9
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onObjectChanged()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->reset()V

    move v9, v10

    goto/16 :goto_0

    :cond_a
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBoundBox(I)Landroid/graphics/RectF;

    move-result-object v2

    if-eqz v2, :cond_8

    if-eqz v0, :cond_8

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerX()F

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    sub-float/2addr v3, v4

    float-to-int v3, v3

    invoke-virtual {v2}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    sub-float/2addr v2, v4

    float-to-int v2, v2

    int-to-float v3, v3

    int-to-float v2, v2

    invoke-virtual {v0, v3, v2}, Landroid/graphics/RectF;->offset(FF)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    :cond_b
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFlipDirectionHorizontal:Z

    if-eqz v1, :cond_c

    iget v1, v0, Landroid/graphics/RectF;->right:F

    iget v2, v0, Landroid/graphics/RectF;->left:F

    iput v2, v0, Landroid/graphics/RectF;->right:F

    iput v1, v0, Landroid/graphics/RectF;->left:F

    :cond_c
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFlipDirectionVertical:Z

    if-eqz v1, :cond_d

    iget v1, v0, Landroid/graphics/RectF;->top:F

    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    iput v2, v0, Landroid/graphics/RectF;->top:F

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    :cond_d
    add-int/lit8 v9, v9, 0x1

    goto :goto_5

    :cond_e
    move v0, v9

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected isClippedObject(IZZZFFFZF)Z
    .locals 14

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-interface {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    :cond_0
    if-nez p3, :cond_1

    if-eqz p4, :cond_2

    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/RectF;

    move-object v6, v1

    :goto_0
    if-nez v6, :cond_3

    const/4 v1, 0x0

    :goto_1
    return v1

    :cond_2
    if-eqz p2, :cond_cd

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v1

    move-object v6, v1

    goto :goto_0

    :cond_3
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotX(I)F

    move-result v7

    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotY(I)F

    move-result v8

    if-eqz p3, :cond_a

    const/4 v2, 0x0

    const/4 v1, 0x0

    new-instance v3, Landroid/graphics/PointF;

    iget v4, v6, Landroid/graphics/RectF;->left:F

    iget v5, v6, Landroid/graphics/RectF;->top:F

    invoke-direct {v3, v4, v5}, Landroid/graphics/PointF;-><init>(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v3

    new-instance v4, Landroid/graphics/PointF;

    iget v5, v6, Landroid/graphics/RectF;->right:F

    iget v9, v6, Landroid/graphics/RectF;->top:F

    invoke-direct {v4, v5, v9}, Landroid/graphics/PointF;-><init>(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v4

    new-instance v5, Landroid/graphics/PointF;

    iget v9, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v5, v9, v10}, Landroid/graphics/PointF;-><init>(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    new-instance v9, Landroid/graphics/PointF;

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v9, v10, v11}, Landroid/graphics/PointF;-><init>(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v9}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v7

    invoke-direct {p0, v3, v4, v5, v7}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v8

    const/4 v9, 0x0

    aget v9, v8, v9

    const/4 v10, 0x1

    aget v10, v8, v10

    const/4 v11, 0x2

    aget v11, v8, v11

    const/4 v12, 0x3

    aget v8, v8, v12

    iget-object v12, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v12, v12, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v12, v12, Landroid/graphics/RectF;->left:F

    cmpg-float v12, v9, v12

    if-gez v12, :cond_4

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    sub-float/2addr v1, v9

    :cond_4
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v9, v9, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->right:F

    cmpl-float v9, v11, v9

    if-lez v9, :cond_5

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    sub-float/2addr v1, v11

    :cond_5
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v9, v9, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->top:F

    cmpg-float v9, v10, v9

    if-gez v9, :cond_6

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, v10

    :cond_6
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v9, v9, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->bottom:F

    cmpl-float v9, v8, v9

    if-lez v9, :cond_7

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v2, v8

    :cond_7
    const/4 v8, 0x0

    cmpl-float v8, v1, v8

    if-nez v8, :cond_8

    const/4 v8, 0x0

    cmpl-float v8, v2, v8

    if-eqz v8, :cond_9

    :cond_8
    iget v8, v3, Landroid/graphics/PointF;->x:F

    add-float/2addr v8, v1

    iput v8, v3, Landroid/graphics/PointF;->x:F

    iget v8, v4, Landroid/graphics/PointF;->x:F

    add-float/2addr v8, v1

    iput v8, v4, Landroid/graphics/PointF;->x:F

    iget v8, v5, Landroid/graphics/PointF;->x:F

    add-float/2addr v8, v1

    iput v8, v5, Landroid/graphics/PointF;->x:F

    iget v8, v7, Landroid/graphics/PointF;->x:F

    add-float/2addr v1, v8

    iput v1, v7, Landroid/graphics/PointF;->x:F

    iget v1, v3, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v2

    iput v1, v3, Landroid/graphics/PointF;->y:F

    iget v1, v4, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v2

    iput v1, v4, Landroid/graphics/PointF;->y:F

    iget v1, v5, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v2

    iput v1, v5, Landroid/graphics/PointF;->y:F

    iget v1, v7, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v2

    iput v1, v7, Landroid/graphics/PointF;->y:F

    invoke-direct {p0, v3, v4, v5, v7}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v1

    const/4 v2, 0x0

    aget v2, v1, v2

    const/4 v8, 0x1

    aget v8, v1, v8

    const/4 v9, 0x2

    aget v9, v1, v9

    const/4 v10, 0x3

    aget v1, v1, v10

    const-wide/high16 v10, 0x3fe0000000000000L    # 0.5

    add-float/2addr v2, v9

    float-to-double v12, v2

    mul-double v9, v10, v12

    double-to-float v2, v9

    const-wide/high16 v9, 0x3fe0000000000000L    # 0.5

    add-float/2addr v1, v8

    float-to-double v11, v1

    mul-double v8, v9, v11

    double-to-float v1, v8

    move/from16 v0, p7

    neg-float v8, v0

    invoke-direct {p0, v2, v1, v8, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v3

    move/from16 v0, p7

    neg-float v8, v0

    invoke-direct {p0, v2, v1, v8, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v4

    move/from16 v0, p7

    neg-float v8, v0

    invoke-direct {p0, v2, v1, v8, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    move/from16 v0, p7

    neg-float v8, v0

    invoke-direct {p0, v2, v1, v8, v7}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v1

    invoke-direct {p0, v3, v4, v5, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v1

    const/4 v2, 0x0

    aget v2, v1, v2

    const/4 v3, 0x1

    aget v3, v1, v3

    const/4 v4, 0x2

    aget v4, v1, v4

    const/4 v5, 0x3

    aget v1, v1, v5

    invoke-virtual {v6, v2, v3, v4, v1}, Landroid/graphics/RectF;->set(FFFF)V

    :cond_9
    const/4 v1, 0x1

    goto/16 :goto_1

    :cond_a
    if-eqz p4, :cond_c2

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v9

    new-instance v1, Landroid/graphics/PointF;

    iget v2, v6, Landroid/graphics/RectF;->left:F

    iget v3, v6, Landroid/graphics/RectF;->top:F

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    new-instance v1, Landroid/graphics/PointF;

    iget v2, v6, Landroid/graphics/RectF;->right:F

    iget v3, v6, Landroid/graphics/RectF;->top:F

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v4

    new-instance v1, Landroid/graphics/PointF;

    iget v2, v6, Landroid/graphics/RectF;->left:F

    iget v3, v6, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v3

    new-instance v1, Landroid/graphics/PointF;

    iget v2, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v1, v2, v10}, Landroid/graphics/PointF;-><init>(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v2

    invoke-direct {p0, v5, v4, v3, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v1

    const/4 v10, 0x0

    aget v10, v1, v10

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->left:F

    cmpg-float v11, v10, v11

    if-gez v11, :cond_10

    iget v11, v9, Landroid/graphics/RectF;->right:F

    iget v12, v9, Landroid/graphics/RectF;->left:F

    sub-float/2addr v11, v12

    invoke-static {v11}, Ljava/lang/Math;->signum(F)F

    move-result v11

    iget v12, v6, Landroid/graphics/RectF;->right:F

    iget v13, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v12, v13

    invoke-static {v12}, Ljava/lang/Math;->signum(F)F

    move-result v12

    cmpl-float v11, v11, v12

    if-eqz v11, :cond_b

    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFlipDirectionHorizontal:Z

    :cond_b
    iget v11, v9, Landroid/graphics/RectF;->bottom:F

    iget v12, v9, Landroid/graphics/RectF;->top:F

    sub-float/2addr v11, v12

    invoke-static {v11}, Ljava/lang/Math;->signum(F)F

    move-result v11

    iget v12, v6, Landroid/graphics/RectF;->bottom:F

    iget v13, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v12, v13

    invoke-static {v12}, Ljava/lang/Math;->signum(F)F

    move-result v12

    cmpl-float v11, v11, v12

    if-eqz v11, :cond_c

    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFlipDirectionVertical:Z

    :cond_c
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->left:F

    sub-float/2addr v11, v10

    iget v12, v5, Landroid/graphics/PointF;->x:F

    sub-float v12, v10, v12

    invoke-static {v12}, Ljava/lang/Math;->abs(F)F

    move-result v12

    const v13, 0x38d1b717    # 1.0E-4f

    cmpg-float v12, v12, v13

    if-gez v12, :cond_22

    iget v1, v5, Landroid/graphics/PointF;->x:F

    add-float/2addr v1, v11

    iput v1, v5, Landroid/graphics/PointF;->x:F

    move/from16 v0, p7

    neg-float v1, v0

    invoke-direct {p0, v7, v8, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    move/from16 v0, p7

    neg-float v1, v0

    invoke-direct {p0, v7, v8, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v2

    invoke-direct {p0, v5, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v1

    :cond_d
    :goto_2
    const/4 v10, 0x0

    aget v10, v1, v10

    const/4 v11, 0x1

    aget v11, v1, v11

    const/4 v12, 0x2

    aget v12, v1, v12

    const/4 v13, 0x3

    aget v1, v1, v13

    invoke-virtual {v6, v10, v11, v12, v1}, Landroid/graphics/RectF;->set(FFFF)V

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->top:F

    invoke-virtual {v5, v1, v10}, Landroid/graphics/PointF;->set(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->top:F

    invoke-virtual {v4, v1, v10}, Landroid/graphics/PointF;->set(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v4

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v3, v1, v10}, Landroid/graphics/PointF;->set(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v3

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v2, v1, v10}, Landroid/graphics/PointF;->set(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v2

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isCornerZonePressed()Z

    move-result v1

    if-eqz v1, :cond_34

    iget v1, v5, Landroid/graphics/PointF;->x:F

    iget v10, v4, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v10

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const v10, 0x38d1b717    # 1.0E-4f

    cmpg-float v1, v1, v10

    if-ltz v1, :cond_e

    iget v1, v5, Landroid/graphics/PointF;->x:F

    iget v10, v3, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v10

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const v10, 0x38d1b717    # 1.0E-4f

    cmpg-float v1, v1, v10

    if-gez v1, :cond_2c

    :cond_e
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v10, 0x1

    if-ne v1, v10, :cond_26

    iget v1, v5, Landroid/graphics/PointF;->y:F

    iget v10, v3, Landroid/graphics/PointF;->y:F

    cmpg-float v1, v1, v10

    if-gez v1, :cond_25

    iget v1, v6, Landroid/graphics/RectF;->top:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->bottom:F

    :cond_f
    :goto_3
    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->top:F

    invoke-virtual {v5, v1, v10}, Landroid/graphics/PointF;->set(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->top:F

    invoke-virtual {v4, v1, v10}, Landroid/graphics/PointF;->set(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v4

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v3, v1, v10}, Landroid/graphics/PointF;->set(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v3

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v2, v1, v10}, Landroid/graphics/PointF;->set(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v2

    :cond_10
    :goto_4
    invoke-direct {p0, v5, v4, v3, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v1

    const/4 v10, 0x1

    aget v10, v1, v10

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->top:F

    cmpg-float v11, v10, v11

    if-gez v11, :cond_16

    iget v11, v9, Landroid/graphics/RectF;->right:F

    iget v12, v9, Landroid/graphics/RectF;->left:F

    sub-float/2addr v11, v12

    invoke-static {v11}, Ljava/lang/Math;->signum(F)F

    move-result v11

    iget v12, v6, Landroid/graphics/RectF;->right:F

    iget v13, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v12, v13

    invoke-static {v12}, Ljava/lang/Math;->signum(F)F

    move-result v12

    cmpl-float v11, v11, v12

    if-eqz v11, :cond_11

    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFlipDirectionHorizontal:Z

    :cond_11
    iget v11, v9, Landroid/graphics/RectF;->bottom:F

    iget v12, v9, Landroid/graphics/RectF;->top:F

    sub-float/2addr v11, v12

    invoke-static {v11}, Ljava/lang/Math;->signum(F)F

    move-result v11

    iget v12, v6, Landroid/graphics/RectF;->bottom:F

    iget v13, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v12, v13

    invoke-static {v12}, Ljava/lang/Math;->signum(F)F

    move-result v12

    cmpl-float v11, v11, v12

    if-eqz v11, :cond_12

    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFlipDirectionVertical:Z

    :cond_12
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->top:F

    sub-float/2addr v11, v10

    iget v12, v5, Landroid/graphics/PointF;->y:F

    sub-float v12, v10, v12

    invoke-static {v12}, Ljava/lang/Math;->abs(F)F

    move-result v12

    const v13, 0x38d1b717    # 1.0E-4f

    cmpg-float v12, v12, v13

    if-gez v12, :cond_4a

    iget v1, v5, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v11

    iput v1, v5, Landroid/graphics/PointF;->y:F

    move/from16 v0, p7

    neg-float v1, v0

    invoke-direct {p0, v7, v8, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    move/from16 v0, p7

    neg-float v1, v0

    invoke-direct {p0, v7, v8, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v2

    invoke-direct {p0, v5, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v1

    :cond_13
    :goto_5
    const/4 v10, 0x0

    aget v10, v1, v10

    const/4 v11, 0x1

    aget v11, v1, v11

    const/4 v12, 0x2

    aget v12, v1, v12

    const/4 v13, 0x3

    aget v1, v1, v13

    invoke-virtual {v6, v10, v11, v12, v1}, Landroid/graphics/RectF;->set(FFFF)V

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->top:F

    invoke-virtual {v5, v1, v10}, Landroid/graphics/PointF;->set(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->top:F

    invoke-virtual {v4, v1, v10}, Landroid/graphics/PointF;->set(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v4

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v3, v1, v10}, Landroid/graphics/PointF;->set(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v3

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v2, v1, v10}, Landroid/graphics/PointF;->set(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v2

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isCornerZonePressed()Z

    move-result v1

    if-eqz v1, :cond_5c

    iget v1, v5, Landroid/graphics/PointF;->x:F

    iget v10, v4, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v10

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const v10, 0x38d1b717    # 1.0E-4f

    cmpg-float v1, v1, v10

    if-ltz v1, :cond_14

    iget v1, v5, Landroid/graphics/PointF;->x:F

    iget v10, v3, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v10

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const v10, 0x38d1b717    # 1.0E-4f

    cmpg-float v1, v1, v10

    if-gez v1, :cond_54

    :cond_14
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v10, 0x1

    if-ne v1, v10, :cond_4e

    iget v1, v5, Landroid/graphics/PointF;->x:F

    iget v10, v3, Landroid/graphics/PointF;->x:F

    cmpl-float v1, v1, v10

    if-lez v1, :cond_4d

    iget v1, v6, Landroid/graphics/RectF;->top:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->bottom:F

    :cond_15
    :goto_6
    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->top:F

    invoke-virtual {v5, v1, v10}, Landroid/graphics/PointF;->set(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->top:F

    invoke-virtual {v4, v1, v10}, Landroid/graphics/PointF;->set(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v4

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v3, v1, v10}, Landroid/graphics/PointF;->set(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v3

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v2, v1, v10}, Landroid/graphics/PointF;->set(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v2

    :cond_16
    :goto_7
    invoke-direct {p0, v5, v4, v3, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v1

    const/4 v10, 0x2

    aget v10, v1, v10

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->right:F

    cmpl-float v11, v10, v11

    if-lez v11, :cond_1c

    iget v11, v9, Landroid/graphics/RectF;->right:F

    iget v12, v9, Landroid/graphics/RectF;->left:F

    sub-float/2addr v11, v12

    invoke-static {v11}, Ljava/lang/Math;->signum(F)F

    move-result v11

    iget v12, v6, Landroid/graphics/RectF;->right:F

    iget v13, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v12, v13

    invoke-static {v12}, Ljava/lang/Math;->signum(F)F

    move-result v12

    cmpl-float v11, v11, v12

    if-eqz v11, :cond_17

    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFlipDirectionHorizontal:Z

    :cond_17
    iget v11, v9, Landroid/graphics/RectF;->bottom:F

    iget v12, v9, Landroid/graphics/RectF;->top:F

    sub-float/2addr v11, v12

    invoke-static {v11}, Ljava/lang/Math;->signum(F)F

    move-result v11

    iget v12, v6, Landroid/graphics/RectF;->bottom:F

    iget v13, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v12, v13

    invoke-static {v12}, Ljava/lang/Math;->signum(F)F

    move-result v12

    cmpl-float v11, v11, v12

    if-eqz v11, :cond_18

    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFlipDirectionVertical:Z

    :cond_18
    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->right:F

    sub-float/2addr v11, v10

    iget v12, v5, Landroid/graphics/PointF;->x:F

    sub-float v12, v10, v12

    invoke-static {v12}, Ljava/lang/Math;->abs(F)F

    move-result v12

    const v13, 0x38d1b717    # 1.0E-4f

    cmpg-float v12, v12, v13

    if-gez v12, :cond_72

    iget v1, v5, Landroid/graphics/PointF;->x:F

    add-float/2addr v1, v11

    iput v1, v5, Landroid/graphics/PointF;->x:F

    move/from16 v0, p7

    neg-float v1, v0

    invoke-direct {p0, v7, v8, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    move/from16 v0, p7

    neg-float v1, v0

    invoke-direct {p0, v7, v8, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v2

    invoke-direct {p0, v5, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v1

    :cond_19
    :goto_8
    const/4 v10, 0x0

    aget v10, v1, v10

    const/4 v11, 0x1

    aget v11, v1, v11

    const/4 v12, 0x2

    aget v12, v1, v12

    const/4 v13, 0x3

    aget v1, v1, v13

    invoke-virtual {v6, v10, v11, v12, v1}, Landroid/graphics/RectF;->set(FFFF)V

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->top:F

    invoke-virtual {v5, v1, v10}, Landroid/graphics/PointF;->set(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->top:F

    invoke-virtual {v4, v1, v10}, Landroid/graphics/PointF;->set(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v4

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v3, v1, v10}, Landroid/graphics/PointF;->set(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v3

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v2, v1, v10}, Landroid/graphics/PointF;->set(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v2

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isCornerZonePressed()Z

    move-result v1

    if-eqz v1, :cond_84

    iget v1, v5, Landroid/graphics/PointF;->x:F

    iget v10, v4, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v10

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const v10, 0x38d1b717    # 1.0E-4f

    cmpg-float v1, v1, v10

    if-ltz v1, :cond_1a

    iget v1, v5, Landroid/graphics/PointF;->x:F

    iget v10, v3, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v10

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const v10, 0x38d1b717    # 1.0E-4f

    cmpg-float v1, v1, v10

    if-gez v1, :cond_7c

    :cond_1a
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v10, 0x1

    if-ne v1, v10, :cond_76

    iget v1, v5, Landroid/graphics/PointF;->y:F

    iget v10, v3, Landroid/graphics/PointF;->y:F

    cmpl-float v1, v1, v10

    if-lez v1, :cond_75

    iget v1, v6, Landroid/graphics/RectF;->top:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->bottom:F

    :cond_1b
    :goto_9
    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->top:F

    invoke-virtual {v5, v1, v10}, Landroid/graphics/PointF;->set(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->top:F

    invoke-virtual {v4, v1, v10}, Landroid/graphics/PointF;->set(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v4

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v3, v1, v10}, Landroid/graphics/PointF;->set(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v3

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v2, v1, v10}, Landroid/graphics/PointF;->set(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v2

    :cond_1c
    :goto_a
    invoke-direct {p0, v5, v4, v3, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v1

    const/4 v10, 0x3

    aget v10, v1, v10

    iget-object v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v11, v11, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->bottom:F

    cmpl-float v11, v10, v11

    if-lez v11, :cond_21

    iget v11, v9, Landroid/graphics/RectF;->right:F

    iget v12, v9, Landroid/graphics/RectF;->left:F

    sub-float/2addr v11, v12

    invoke-static {v11}, Ljava/lang/Math;->signum(F)F

    move-result v11

    iget v12, v6, Landroid/graphics/RectF;->right:F

    iget v13, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v12, v13

    invoke-static {v12}, Ljava/lang/Math;->signum(F)F

    move-result v12

    cmpl-float v11, v11, v12

    if-eqz v11, :cond_1d

    const/4 v11, 0x1

    iput-boolean v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFlipDirectionHorizontal:Z

    :cond_1d
    iget v11, v9, Landroid/graphics/RectF;->bottom:F

    iget v9, v9, Landroid/graphics/RectF;->top:F

    sub-float v9, v11, v9

    invoke-static {v9}, Ljava/lang/Math;->signum(F)F

    move-result v9

    iget v11, v6, Landroid/graphics/RectF;->bottom:F

    iget v12, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v11, v12

    invoke-static {v11}, Ljava/lang/Math;->signum(F)F

    move-result v11

    cmpl-float v9, v9, v11

    if-eqz v9, :cond_1e

    const/4 v9, 0x1

    iput-boolean v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFlipDirectionVertical:Z

    :cond_1e
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v9, v9, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v9, v10

    iget v11, v5, Landroid/graphics/PointF;->y:F

    sub-float v11, v10, v11

    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    move-result v11

    const v12, 0x38d1b717    # 1.0E-4f

    cmpg-float v11, v11, v12

    if-gez v11, :cond_9a

    iget v1, v5, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v9

    iput v1, v5, Landroid/graphics/PointF;->y:F

    move/from16 v0, p7

    neg-float v1, v0

    invoke-direct {p0, v7, v8, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    move/from16 v0, p7

    neg-float v1, v0

    invoke-direct {p0, v7, v8, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v2

    invoke-direct {p0, v5, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v1

    :cond_1f
    :goto_b
    const/4 v9, 0x0

    aget v9, v1, v9

    const/4 v10, 0x1

    aget v10, v1, v10

    const/4 v11, 0x2

    aget v11, v1, v11

    const/4 v12, 0x3

    aget v1, v1, v12

    invoke-virtual {v6, v9, v10, v11, v1}, Landroid/graphics/RectF;->set(FFFF)V

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v9, v6, Landroid/graphics/RectF;->top:F

    invoke-virtual {v5, v1, v9}, Landroid/graphics/PointF;->set(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v1

    iget v5, v6, Landroid/graphics/RectF;->right:F

    iget v9, v6, Landroid/graphics/RectF;->top:F

    invoke-virtual {v4, v5, v9}, Landroid/graphics/PointF;->set(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v4

    iget v5, v6, Landroid/graphics/RectF;->left:F

    iget v9, v6, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v3, v5, v9}, Landroid/graphics/PointF;->set(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v3

    iget v5, v6, Landroid/graphics/RectF;->right:F

    iget v9, v6, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v2, v5, v9}, Landroid/graphics/PointF;->set(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v2

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isCornerZonePressed()Z

    move-result v5

    if-eqz v5, :cond_ac

    iget v5, v1, Landroid/graphics/PointF;->x:F

    iget v7, v4, Landroid/graphics/PointF;->x:F

    sub-float/2addr v5, v7

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const v7, 0x38d1b717    # 1.0E-4f

    cmpg-float v5, v5, v7

    if-ltz v5, :cond_20

    iget v5, v1, Landroid/graphics/PointF;->x:F

    iget v7, v3, Landroid/graphics/PointF;->x:F

    sub-float/2addr v5, v7

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    const v7, 0x38d1b717    # 1.0E-4f

    cmpg-float v5, v5, v7

    if-gez v5, :cond_a4

    :cond_20
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v7, 0x1

    if-ne v5, v7, :cond_9e

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget v2, v3, Landroid/graphics/PointF;->x:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_9d

    iget v1, v6, Landroid/graphics/RectF;->top:F

    iget v2, v6, Landroid/graphics/RectF;->right:F

    iget v3, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    div-float v2, v2, p9

    add-float/2addr v1, v2

    iput v1, v6, Landroid/graphics/RectF;->bottom:F

    :cond_21
    :goto_c
    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_22
    iget v12, v4, Landroid/graphics/PointF;->x:F

    sub-float v12, v10, v12

    invoke-static {v12}, Ljava/lang/Math;->abs(F)F

    move-result v12

    const v13, 0x38d1b717    # 1.0E-4f

    cmpg-float v12, v12, v13

    if-gez v12, :cond_23

    iget v1, v4, Landroid/graphics/PointF;->x:F

    add-float/2addr v1, v11

    iput v1, v4, Landroid/graphics/PointF;->x:F

    move/from16 v0, p7

    neg-float v1, v0

    invoke-direct {p0, v7, v8, v1, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v4

    move/from16 v0, p7

    neg-float v1, v0

    invoke-direct {p0, v7, v8, v1, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v3

    invoke-direct {p0, v4, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v1

    goto/16 :goto_2

    :cond_23
    iget v12, v3, Landroid/graphics/PointF;->x:F

    sub-float v12, v10, v12

    invoke-static {v12}, Ljava/lang/Math;->abs(F)F

    move-result v12

    const v13, 0x38d1b717    # 1.0E-4f

    cmpg-float v12, v12, v13

    if-gez v12, :cond_24

    iget v1, v3, Landroid/graphics/PointF;->x:F

    add-float/2addr v1, v11

    iput v1, v3, Landroid/graphics/PointF;->x:F

    move/from16 v0, p7

    neg-float v1, v0

    invoke-direct {p0, v7, v8, v1, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v4

    move/from16 v0, p7

    neg-float v1, v0

    invoke-direct {p0, v7, v8, v1, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v3

    invoke-direct {p0, v4, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v1

    goto/16 :goto_2

    :cond_24
    iget v12, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v10, v12

    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v10

    const v12, 0x38d1b717    # 1.0E-4f

    cmpg-float v10, v10, v12

    if-gez v10, :cond_d

    iget v1, v2, Landroid/graphics/PointF;->x:F

    add-float/2addr v1, v11

    iput v1, v2, Landroid/graphics/PointF;->x:F

    move/from16 v0, p7

    neg-float v1, v0

    invoke-direct {p0, v7, v8, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    move/from16 v0, p7

    neg-float v1, v0

    invoke-direct {p0, v7, v8, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v2

    invoke-direct {p0, v5, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v1

    goto/16 :goto_2

    :cond_25
    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->right:F

    goto/16 :goto_3

    :cond_26
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v10, 0x6

    if-ne v1, v10, :cond_28

    iget v1, v3, Landroid/graphics/PointF;->y:F

    iget v10, v2, Landroid/graphics/PointF;->y:F

    cmpg-float v1, v1, v10

    if-gez v1, :cond_27

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->right:F

    goto/16 :goto_3

    :cond_27
    iget v1, v6, Landroid/graphics/RectF;->bottom:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->top:F

    goto/16 :goto_3

    :cond_28
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/16 v10, 0x8

    if-ne v1, v10, :cond_2a

    iget v1, v2, Landroid/graphics/PointF;->y:F

    iget v10, v4, Landroid/graphics/PointF;->y:F

    cmpg-float v1, v1, v10

    if-gez v1, :cond_29

    iget v1, v6, Landroid/graphics/RectF;->bottom:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->top:F

    goto/16 :goto_3

    :cond_29
    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->left:F

    goto/16 :goto_3

    :cond_2a
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v10, 0x3

    if-ne v1, v10, :cond_f

    iget v1, v4, Landroid/graphics/PointF;->y:F

    iget v10, v5, Landroid/graphics/PointF;->y:F

    cmpg-float v1, v1, v10

    if-gez v1, :cond_2b

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->left:F

    goto/16 :goto_3

    :cond_2b
    iget v1, v6, Landroid/graphics/RectF;->top:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_3

    :cond_2c
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v10, 0x1

    if-ne v1, v10, :cond_2e

    iget v1, v5, Landroid/graphics/PointF;->y:F

    iget v10, v3, Landroid/graphics/PointF;->y:F

    cmpg-float v1, v1, v10

    if-gez v1, :cond_2d

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->right:F

    goto/16 :goto_3

    :cond_2d
    iget v1, v6, Landroid/graphics/RectF;->top:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_3

    :cond_2e
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v10, 0x6

    if-ne v1, v10, :cond_30

    iget v1, v3, Landroid/graphics/PointF;->y:F

    iget v10, v2, Landroid/graphics/PointF;->y:F

    cmpg-float v1, v1, v10

    if-gez v1, :cond_2f

    iget v1, v6, Landroid/graphics/RectF;->bottom:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->top:F

    goto/16 :goto_3

    :cond_2f
    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->right:F

    goto/16 :goto_3

    :cond_30
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/16 v10, 0x8

    if-ne v1, v10, :cond_32

    iget v1, v2, Landroid/graphics/PointF;->y:F

    iget v10, v4, Landroid/graphics/PointF;->y:F

    cmpg-float v1, v1, v10

    if-gez v1, :cond_31

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->left:F

    goto/16 :goto_3

    :cond_31
    iget v1, v6, Landroid/graphics/RectF;->bottom:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->top:F

    goto/16 :goto_3

    :cond_32
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v10, 0x3

    if-ne v1, v10, :cond_f

    iget v1, v4, Landroid/graphics/PointF;->y:F

    iget v10, v5, Landroid/graphics/PointF;->y:F

    cmpg-float v1, v1, v10

    if-gez v1, :cond_33

    iget v1, v6, Landroid/graphics/RectF;->top:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_3

    :cond_33
    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->left:F

    goto/16 :goto_3

    :cond_34
    if-eqz p8, :cond_10

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isCornerZonePressed()Z

    move-result v1

    if-nez v1, :cond_10

    iget v1, v5, Landroid/graphics/PointF;->x:F

    iget v10, v4, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v10

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const v10, 0x38d1b717    # 1.0E-4f

    cmpg-float v1, v1, v10

    if-ltz v1, :cond_35

    iget v1, v5, Landroid/graphics/PointF;->x:F

    iget v10, v3, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v10

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const v10, 0x38d1b717    # 1.0E-4f

    cmpg-float v1, v1, v10

    if-gez v1, :cond_3a

    :cond_35
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v10, 0x2

    if-ne v1, v10, :cond_37

    iget v1, v6, Landroid/graphics/RectF;->top:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->bottom:F

    :cond_36
    :goto_d
    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->top:F

    invoke-virtual {v5, v1, v10}, Landroid/graphics/PointF;->set(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->top:F

    invoke-virtual {v4, v1, v10}, Landroid/graphics/PointF;->set(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v4

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v3, v1, v10}, Landroid/graphics/PointF;->set(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v3

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v2, v1, v10}, Landroid/graphics/PointF;->set(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v2

    goto/16 :goto_4

    :cond_37
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v10, 0x7

    if-ne v1, v10, :cond_38

    iget v1, v6, Landroid/graphics/RectF;->bottom:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->top:F

    goto :goto_d

    :cond_38
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v10, 0x4

    if-ne v1, v10, :cond_39

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->right:F

    goto :goto_d

    :cond_39
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v10, 0x5

    if-ne v1, v10, :cond_36

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->left:F

    goto :goto_d

    :cond_3a
    const/4 v1, 0x0

    cmpg-float v1, p7, v1

    if-gez v1, :cond_cc

    const/high16 v1, 0x43b40000    # 360.0f

    add-float v1, v1, p7

    :goto_e
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v10, v10, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v11, 0x2

    if-ne v10, v11, :cond_3e

    const/4 v10, 0x0

    cmpl-float v10, v1, v10

    if-lez v10, :cond_3b

    const/high16 v10, 0x42b40000    # 90.0f

    cmpg-float v10, v1, v10

    if-gez v10, :cond_3b

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->right:F

    goto/16 :goto_d

    :cond_3b
    const/high16 v10, 0x42b40000    # 90.0f

    cmpl-float v10, v1, v10

    if-lez v10, :cond_3c

    const/high16 v10, 0x43340000    # 180.0f

    cmpg-float v10, v1, v10

    if-gez v10, :cond_3c

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->left:F

    goto/16 :goto_d

    :cond_3c
    const/high16 v10, 0x43340000    # 180.0f

    cmpl-float v10, v1, v10

    if-lez v10, :cond_3d

    const/high16 v10, 0x43870000    # 270.0f

    cmpg-float v10, v1, v10

    if-gez v10, :cond_3d

    iget v1, v6, Landroid/graphics/RectF;->top:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_d

    :cond_3d
    const/high16 v10, 0x43870000    # 270.0f

    cmpl-float v10, v1, v10

    if-lez v10, :cond_36

    const/high16 v10, 0x43b40000    # 360.0f

    cmpg-float v1, v1, v10

    if-gez v1, :cond_36

    iget v1, v6, Landroid/graphics/RectF;->top:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_d

    :cond_3e
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v10, v10, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v11, 0x5

    if-ne v10, v11, :cond_42

    const/4 v10, 0x0

    cmpl-float v10, v1, v10

    if-lez v10, :cond_3f

    const/high16 v10, 0x42b40000    # 90.0f

    cmpg-float v10, v1, v10

    if-gez v10, :cond_3f

    iget v1, v6, Landroid/graphics/RectF;->bottom:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->top:F

    goto/16 :goto_d

    :cond_3f
    const/high16 v10, 0x42b40000    # 90.0f

    cmpl-float v10, v1, v10

    if-lez v10, :cond_40

    const/high16 v10, 0x43340000    # 180.0f

    cmpg-float v10, v1, v10

    if-gez v10, :cond_40

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->left:F

    goto/16 :goto_d

    :cond_40
    const/high16 v10, 0x43340000    # 180.0f

    cmpl-float v10, v1, v10

    if-lez v10, :cond_41

    const/high16 v10, 0x43870000    # 270.0f

    cmpg-float v10, v1, v10

    if-gez v10, :cond_41

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->left:F

    goto/16 :goto_d

    :cond_41
    const/high16 v10, 0x43870000    # 270.0f

    cmpl-float v10, v1, v10

    if-lez v10, :cond_36

    const/high16 v10, 0x43b40000    # 360.0f

    cmpg-float v1, v1, v10

    if-gez v1, :cond_36

    iget v1, v6, Landroid/graphics/RectF;->top:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_d

    :cond_42
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v10, v10, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v11, 0x7

    if-ne v10, v11, :cond_46

    const/4 v10, 0x0

    cmpl-float v10, v1, v10

    if-lez v10, :cond_43

    const/high16 v10, 0x42b40000    # 90.0f

    cmpg-float v10, v1, v10

    if-gez v10, :cond_43

    iget v1, v6, Landroid/graphics/RectF;->bottom:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->top:F

    goto/16 :goto_d

    :cond_43
    const/high16 v10, 0x42b40000    # 90.0f

    cmpl-float v10, v1, v10

    if-lez v10, :cond_44

    const/high16 v10, 0x43340000    # 180.0f

    cmpg-float v10, v1, v10

    if-gez v10, :cond_44

    iget v1, v6, Landroid/graphics/RectF;->bottom:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->top:F

    goto/16 :goto_d

    :cond_44
    const/high16 v10, 0x43340000    # 180.0f

    cmpl-float v10, v1, v10

    if-lez v10, :cond_45

    const/high16 v10, 0x43870000    # 270.0f

    cmpg-float v10, v1, v10

    if-gez v10, :cond_45

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->left:F

    goto/16 :goto_d

    :cond_45
    const/high16 v10, 0x43870000    # 270.0f

    cmpl-float v10, v1, v10

    if-lez v10, :cond_36

    const/high16 v10, 0x43b40000    # 360.0f

    cmpg-float v1, v1, v10

    if-gez v1, :cond_36

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->right:F

    goto/16 :goto_d

    :cond_46
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v10, v10, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v11, 0x4

    if-ne v10, v11, :cond_36

    const/4 v10, 0x0

    cmpl-float v10, v1, v10

    if-lez v10, :cond_47

    const/high16 v10, 0x42b40000    # 90.0f

    cmpg-float v10, v1, v10

    if-gez v10, :cond_47

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->right:F

    goto/16 :goto_d

    :cond_47
    const/high16 v10, 0x42b40000    # 90.0f

    cmpl-float v10, v1, v10

    if-lez v10, :cond_48

    const/high16 v10, 0x43340000    # 180.0f

    cmpg-float v10, v1, v10

    if-gez v10, :cond_48

    iget v1, v6, Landroid/graphics/RectF;->bottom:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->top:F

    goto/16 :goto_d

    :cond_48
    const/high16 v10, 0x43340000    # 180.0f

    cmpl-float v10, v1, v10

    if-lez v10, :cond_49

    const/high16 v10, 0x43870000    # 270.0f

    cmpg-float v10, v1, v10

    if-gez v10, :cond_49

    iget v1, v6, Landroid/graphics/RectF;->top:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_d

    :cond_49
    const/high16 v10, 0x43870000    # 270.0f

    cmpl-float v10, v1, v10

    if-lez v10, :cond_36

    const/high16 v10, 0x43b40000    # 360.0f

    cmpg-float v1, v1, v10

    if-gez v1, :cond_36

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->right:F

    goto/16 :goto_d

    :cond_4a
    iget v12, v4, Landroid/graphics/PointF;->y:F

    sub-float v12, v10, v12

    invoke-static {v12}, Ljava/lang/Math;->abs(F)F

    move-result v12

    const v13, 0x38d1b717    # 1.0E-4f

    cmpg-float v12, v12, v13

    if-gez v12, :cond_4b

    iget v1, v4, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v11

    iput v1, v4, Landroid/graphics/PointF;->y:F

    move/from16 v0, p7

    neg-float v1, v0

    invoke-direct {p0, v7, v8, v1, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v4

    move/from16 v0, p7

    neg-float v1, v0

    invoke-direct {p0, v7, v8, v1, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v3

    invoke-direct {p0, v4, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v1

    goto/16 :goto_5

    :cond_4b
    iget v12, v3, Landroid/graphics/PointF;->y:F

    sub-float v12, v10, v12

    invoke-static {v12}, Ljava/lang/Math;->abs(F)F

    move-result v12

    const v13, 0x38d1b717    # 1.0E-4f

    cmpg-float v12, v12, v13

    if-gez v12, :cond_4c

    iget v1, v3, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v11

    iput v1, v3, Landroid/graphics/PointF;->y:F

    move/from16 v0, p7

    neg-float v1, v0

    invoke-direct {p0, v7, v8, v1, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v4

    move/from16 v0, p7

    neg-float v1, v0

    invoke-direct {p0, v7, v8, v1, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v3

    invoke-direct {p0, v4, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v1

    goto/16 :goto_5

    :cond_4c
    iget v12, v2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v10, v12

    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v10

    const v12, 0x38d1b717    # 1.0E-4f

    cmpg-float v10, v10, v12

    if-gez v10, :cond_13

    iget v1, v2, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v11

    iput v1, v2, Landroid/graphics/PointF;->y:F

    move/from16 v0, p7

    neg-float v1, v0

    invoke-direct {p0, v7, v8, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    move/from16 v0, p7

    neg-float v1, v0

    invoke-direct {p0, v7, v8, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v2

    invoke-direct {p0, v5, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v1

    goto/16 :goto_5

    :cond_4d
    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->right:F

    goto/16 :goto_6

    :cond_4e
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v10, 0x6

    if-ne v1, v10, :cond_50

    iget v1, v3, Landroid/graphics/PointF;->x:F

    iget v10, v2, Landroid/graphics/PointF;->x:F

    cmpl-float v1, v1, v10

    if-lez v1, :cond_4f

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->right:F

    goto/16 :goto_6

    :cond_4f
    iget v1, v6, Landroid/graphics/RectF;->bottom:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->top:F

    goto/16 :goto_6

    :cond_50
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/16 v10, 0x8

    if-ne v1, v10, :cond_52

    iget v1, v2, Landroid/graphics/PointF;->x:F

    iget v10, v4, Landroid/graphics/PointF;->x:F

    cmpl-float v1, v1, v10

    if-lez v1, :cond_51

    iget v1, v6, Landroid/graphics/RectF;->bottom:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->top:F

    goto/16 :goto_6

    :cond_51
    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->left:F

    goto/16 :goto_6

    :cond_52
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v10, 0x3

    if-ne v1, v10, :cond_15

    iget v1, v4, Landroid/graphics/PointF;->x:F

    iget v10, v5, Landroid/graphics/PointF;->x:F

    cmpl-float v1, v1, v10

    if-lez v1, :cond_53

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->left:F

    goto/16 :goto_6

    :cond_53
    iget v1, v6, Landroid/graphics/RectF;->top:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_6

    :cond_54
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v10, 0x1

    if-ne v1, v10, :cond_56

    iget v1, v5, Landroid/graphics/PointF;->x:F

    iget v10, v3, Landroid/graphics/PointF;->x:F

    cmpl-float v1, v1, v10

    if-lez v1, :cond_55

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->right:F

    goto/16 :goto_6

    :cond_55
    iget v1, v6, Landroid/graphics/RectF;->top:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_6

    :cond_56
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v10, 0x6

    if-ne v1, v10, :cond_58

    iget v1, v3, Landroid/graphics/PointF;->x:F

    iget v10, v2, Landroid/graphics/PointF;->x:F

    cmpl-float v1, v1, v10

    if-lez v1, :cond_57

    iget v1, v6, Landroid/graphics/RectF;->bottom:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->top:F

    goto/16 :goto_6

    :cond_57
    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->right:F

    goto/16 :goto_6

    :cond_58
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/16 v10, 0x8

    if-ne v1, v10, :cond_5a

    iget v1, v2, Landroid/graphics/PointF;->x:F

    iget v10, v4, Landroid/graphics/PointF;->x:F

    cmpl-float v1, v1, v10

    if-lez v1, :cond_59

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->left:F

    goto/16 :goto_6

    :cond_59
    iget v1, v6, Landroid/graphics/RectF;->bottom:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->top:F

    goto/16 :goto_6

    :cond_5a
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v10, 0x3

    if-ne v1, v10, :cond_15

    iget v1, v4, Landroid/graphics/PointF;->x:F

    iget v10, v5, Landroid/graphics/PointF;->x:F

    cmpl-float v1, v1, v10

    if-lez v1, :cond_5b

    iget v1, v6, Landroid/graphics/RectF;->top:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_6

    :cond_5b
    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->left:F

    goto/16 :goto_6

    :cond_5c
    if-eqz p8, :cond_16

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isCornerZonePressed()Z

    move-result v1

    if-nez v1, :cond_16

    iget v1, v5, Landroid/graphics/PointF;->x:F

    iget v10, v4, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v10

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const v10, 0x38d1b717    # 1.0E-4f

    cmpg-float v1, v1, v10

    if-ltz v1, :cond_5d

    iget v1, v5, Landroid/graphics/PointF;->x:F

    iget v10, v3, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v10

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const v10, 0x38d1b717    # 1.0E-4f

    cmpg-float v1, v1, v10

    if-gez v1, :cond_62

    :cond_5d
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v10, 0x2

    if-ne v1, v10, :cond_5f

    iget v1, v6, Landroid/graphics/RectF;->top:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->bottom:F

    :cond_5e
    :goto_f
    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->top:F

    invoke-virtual {v5, v1, v10}, Landroid/graphics/PointF;->set(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->top:F

    invoke-virtual {v4, v1, v10}, Landroid/graphics/PointF;->set(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v4

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v3, v1, v10}, Landroid/graphics/PointF;->set(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v3

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v2, v1, v10}, Landroid/graphics/PointF;->set(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v2

    goto/16 :goto_7

    :cond_5f
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v10, 0x7

    if-ne v1, v10, :cond_60

    iget v1, v6, Landroid/graphics/RectF;->bottom:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->top:F

    goto :goto_f

    :cond_60
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v10, 0x4

    if-ne v1, v10, :cond_61

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->right:F

    goto :goto_f

    :cond_61
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v10, 0x5

    if-ne v1, v10, :cond_5e

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->left:F

    goto :goto_f

    :cond_62
    const/4 v1, 0x0

    cmpg-float v1, p7, v1

    if-gez v1, :cond_cb

    const/high16 v1, 0x43b40000    # 360.0f

    add-float v1, v1, p7

    :goto_10
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v10, v10, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v11, 0x2

    if-ne v10, v11, :cond_66

    const/4 v10, 0x0

    cmpl-float v10, v1, v10

    if-lez v10, :cond_63

    const/high16 v10, 0x42b40000    # 90.0f

    cmpg-float v10, v1, v10

    if-gez v10, :cond_63

    iget v1, v6, Landroid/graphics/RectF;->top:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_f

    :cond_63
    const/high16 v10, 0x42b40000    # 90.0f

    cmpl-float v10, v1, v10

    if-lez v10, :cond_64

    const/high16 v10, 0x43340000    # 180.0f

    cmpg-float v10, v1, v10

    if-gez v10, :cond_64

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->right:F

    goto/16 :goto_f

    :cond_64
    const/high16 v10, 0x43340000    # 180.0f

    cmpl-float v10, v1, v10

    if-lez v10, :cond_65

    const/high16 v10, 0x43870000    # 270.0f

    cmpg-float v10, v1, v10

    if-gez v10, :cond_65

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->left:F

    goto/16 :goto_f

    :cond_65
    const/high16 v10, 0x43870000    # 270.0f

    cmpl-float v10, v1, v10

    if-lez v10, :cond_5e

    const/high16 v10, 0x43b40000    # 360.0f

    cmpg-float v1, v1, v10

    if-gez v1, :cond_5e

    iget v1, v6, Landroid/graphics/RectF;->top:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_f

    :cond_66
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v10, v10, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v11, 0x5

    if-ne v10, v11, :cond_6a

    const/4 v10, 0x0

    cmpl-float v10, v1, v10

    if-lez v10, :cond_67

    const/high16 v10, 0x42b40000    # 90.0f

    cmpg-float v10, v1, v10

    if-gez v10, :cond_67

    iget v1, v6, Landroid/graphics/RectF;->top:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_f

    :cond_67
    const/high16 v10, 0x42b40000    # 90.0f

    cmpl-float v10, v1, v10

    if-lez v10, :cond_68

    const/high16 v10, 0x43340000    # 180.0f

    cmpg-float v10, v1, v10

    if-gez v10, :cond_68

    iget v1, v6, Landroid/graphics/RectF;->bottom:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->top:F

    goto/16 :goto_f

    :cond_68
    const/high16 v10, 0x43340000    # 180.0f

    cmpl-float v10, v1, v10

    if-lez v10, :cond_69

    const/high16 v10, 0x43870000    # 270.0f

    cmpg-float v10, v1, v10

    if-gez v10, :cond_69

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->left:F

    goto/16 :goto_f

    :cond_69
    const/high16 v10, 0x43870000    # 270.0f

    cmpl-float v10, v1, v10

    if-lez v10, :cond_5e

    const/high16 v10, 0x43b40000    # 360.0f

    cmpg-float v1, v1, v10

    if-gez v1, :cond_5e

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->left:F

    goto/16 :goto_f

    :cond_6a
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v10, v10, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v11, 0x7

    if-ne v10, v11, :cond_6e

    const/4 v10, 0x0

    cmpl-float v10, v1, v10

    if-lez v10, :cond_6b

    const/high16 v10, 0x42b40000    # 90.0f

    cmpg-float v10, v1, v10

    if-gez v10, :cond_6b

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->right:F

    goto/16 :goto_f

    :cond_6b
    const/high16 v10, 0x42b40000    # 90.0f

    cmpl-float v10, v1, v10

    if-lez v10, :cond_6c

    const/high16 v10, 0x43340000    # 180.0f

    cmpg-float v10, v1, v10

    if-gez v10, :cond_6c

    iget v1, v6, Landroid/graphics/RectF;->bottom:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->top:F

    goto/16 :goto_f

    :cond_6c
    const/high16 v10, 0x43340000    # 180.0f

    cmpl-float v10, v1, v10

    if-lez v10, :cond_6d

    const/high16 v10, 0x43870000    # 270.0f

    cmpg-float v10, v1, v10

    if-gez v10, :cond_6d

    iget v1, v6, Landroid/graphics/RectF;->bottom:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->top:F

    goto/16 :goto_f

    :cond_6d
    const/high16 v10, 0x43870000    # 270.0f

    cmpl-float v10, v1, v10

    if-lez v10, :cond_5e

    const/high16 v10, 0x43b40000    # 360.0f

    cmpg-float v1, v1, v10

    if-gez v1, :cond_5e

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->left:F

    goto/16 :goto_f

    :cond_6e
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v10, v10, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v11, 0x4

    if-ne v10, v11, :cond_5e

    const/4 v10, 0x0

    cmpl-float v10, v1, v10

    if-lez v10, :cond_6f

    const/high16 v10, 0x42b40000    # 90.0f

    cmpg-float v10, v1, v10

    if-gez v10, :cond_6f

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->right:F

    goto/16 :goto_f

    :cond_6f
    const/high16 v10, 0x42b40000    # 90.0f

    cmpl-float v10, v1, v10

    if-lez v10, :cond_70

    const/high16 v10, 0x43340000    # 180.0f

    cmpg-float v10, v1, v10

    if-gez v10, :cond_70

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->right:F

    goto/16 :goto_f

    :cond_70
    const/high16 v10, 0x43340000    # 180.0f

    cmpl-float v10, v1, v10

    if-lez v10, :cond_71

    const/high16 v10, 0x43870000    # 270.0f

    cmpg-float v10, v1, v10

    if-gez v10, :cond_71

    iget v1, v6, Landroid/graphics/RectF;->bottom:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->top:F

    goto/16 :goto_f

    :cond_71
    const/high16 v10, 0x43870000    # 270.0f

    cmpl-float v10, v1, v10

    if-lez v10, :cond_5e

    const/high16 v10, 0x43b40000    # 360.0f

    cmpg-float v1, v1, v10

    if-gez v1, :cond_5e

    iget v1, v6, Landroid/graphics/RectF;->top:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_f

    :cond_72
    iget v12, v4, Landroid/graphics/PointF;->x:F

    sub-float v12, v10, v12

    invoke-static {v12}, Ljava/lang/Math;->abs(F)F

    move-result v12

    const v13, 0x38d1b717    # 1.0E-4f

    cmpg-float v12, v12, v13

    if-gez v12, :cond_73

    iget v1, v4, Landroid/graphics/PointF;->x:F

    add-float/2addr v1, v11

    iput v1, v4, Landroid/graphics/PointF;->x:F

    move/from16 v0, p7

    neg-float v1, v0

    invoke-direct {p0, v7, v8, v1, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v4

    move/from16 v0, p7

    neg-float v1, v0

    invoke-direct {p0, v7, v8, v1, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v3

    invoke-direct {p0, v4, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v1

    goto/16 :goto_8

    :cond_73
    iget v12, v3, Landroid/graphics/PointF;->x:F

    sub-float v12, v10, v12

    invoke-static {v12}, Ljava/lang/Math;->abs(F)F

    move-result v12

    const v13, 0x38d1b717    # 1.0E-4f

    cmpg-float v12, v12, v13

    if-gez v12, :cond_74

    iget v1, v3, Landroid/graphics/PointF;->x:F

    add-float/2addr v1, v11

    iput v1, v3, Landroid/graphics/PointF;->x:F

    move/from16 v0, p7

    neg-float v1, v0

    invoke-direct {p0, v7, v8, v1, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v4

    move/from16 v0, p7

    neg-float v1, v0

    invoke-direct {p0, v7, v8, v1, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v3

    invoke-direct {p0, v4, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v1

    goto/16 :goto_8

    :cond_74
    iget v12, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v10, v12

    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v10

    const v12, 0x38d1b717    # 1.0E-4f

    cmpg-float v10, v10, v12

    if-gez v10, :cond_19

    iget v1, v2, Landroid/graphics/PointF;->x:F

    add-float/2addr v1, v11

    iput v1, v2, Landroid/graphics/PointF;->x:F

    move/from16 v0, p7

    neg-float v1, v0

    invoke-direct {p0, v7, v8, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    move/from16 v0, p7

    neg-float v1, v0

    invoke-direct {p0, v7, v8, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v2

    invoke-direct {p0, v5, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v1

    goto/16 :goto_8

    :cond_75
    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->right:F

    goto/16 :goto_9

    :cond_76
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v10, 0x6

    if-ne v1, v10, :cond_78

    iget v1, v3, Landroid/graphics/PointF;->y:F

    iget v10, v2, Landroid/graphics/PointF;->y:F

    cmpl-float v1, v1, v10

    if-lez v1, :cond_77

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->right:F

    goto/16 :goto_9

    :cond_77
    iget v1, v6, Landroid/graphics/RectF;->bottom:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->top:F

    goto/16 :goto_9

    :cond_78
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/16 v10, 0x8

    if-ne v1, v10, :cond_7a

    iget v1, v2, Landroid/graphics/PointF;->y:F

    iget v10, v4, Landroid/graphics/PointF;->y:F

    cmpl-float v1, v1, v10

    if-lez v1, :cond_79

    iget v1, v6, Landroid/graphics/RectF;->bottom:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->top:F

    goto/16 :goto_9

    :cond_79
    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->left:F

    goto/16 :goto_9

    :cond_7a
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v10, 0x3

    if-ne v1, v10, :cond_1b

    iget v1, v4, Landroid/graphics/PointF;->y:F

    iget v10, v5, Landroid/graphics/PointF;->y:F

    cmpl-float v1, v1, v10

    if-lez v1, :cond_7b

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->left:F

    goto/16 :goto_9

    :cond_7b
    iget v1, v6, Landroid/graphics/RectF;->top:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_9

    :cond_7c
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v10, 0x1

    if-ne v1, v10, :cond_7e

    iget v1, v5, Landroid/graphics/PointF;->y:F

    iget v10, v3, Landroid/graphics/PointF;->y:F

    cmpl-float v1, v1, v10

    if-lez v1, :cond_7d

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->right:F

    goto/16 :goto_9

    :cond_7d
    iget v1, v6, Landroid/graphics/RectF;->top:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_9

    :cond_7e
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v10, 0x6

    if-ne v1, v10, :cond_80

    iget v1, v3, Landroid/graphics/PointF;->y:F

    iget v10, v2, Landroid/graphics/PointF;->y:F

    cmpl-float v1, v1, v10

    if-lez v1, :cond_7f

    iget v1, v6, Landroid/graphics/RectF;->bottom:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->top:F

    goto/16 :goto_9

    :cond_7f
    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->right:F

    goto/16 :goto_9

    :cond_80
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/16 v10, 0x8

    if-ne v1, v10, :cond_82

    iget v1, v2, Landroid/graphics/PointF;->y:F

    iget v10, v4, Landroid/graphics/PointF;->y:F

    cmpl-float v1, v1, v10

    if-lez v1, :cond_81

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->left:F

    goto/16 :goto_9

    :cond_81
    iget v1, v6, Landroid/graphics/RectF;->bottom:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->top:F

    goto/16 :goto_9

    :cond_82
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v10, 0x3

    if-ne v1, v10, :cond_1b

    iget v1, v4, Landroid/graphics/PointF;->y:F

    iget v10, v5, Landroid/graphics/PointF;->y:F

    cmpl-float v1, v1, v10

    if-lez v1, :cond_83

    iget v1, v6, Landroid/graphics/RectF;->top:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_9

    :cond_83
    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->left:F

    goto/16 :goto_9

    :cond_84
    if-eqz p8, :cond_1c

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isCornerZonePressed()Z

    move-result v1

    if-nez v1, :cond_1c

    iget v1, v5, Landroid/graphics/PointF;->x:F

    iget v10, v4, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v10

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const v10, 0x38d1b717    # 1.0E-4f

    cmpg-float v1, v1, v10

    if-ltz v1, :cond_85

    iget v1, v5, Landroid/graphics/PointF;->x:F

    iget v10, v3, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v10

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const v10, 0x38d1b717    # 1.0E-4f

    cmpg-float v1, v1, v10

    if-gez v1, :cond_8a

    :cond_85
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v10, 0x2

    if-ne v1, v10, :cond_87

    iget v1, v6, Landroid/graphics/RectF;->top:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->bottom:F

    :cond_86
    :goto_11
    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->top:F

    invoke-virtual {v5, v1, v10}, Landroid/graphics/PointF;->set(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->top:F

    invoke-virtual {v4, v1, v10}, Landroid/graphics/PointF;->set(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v4

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v3, v1, v10}, Landroid/graphics/PointF;->set(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v3

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v2, v1, v10}, Landroid/graphics/PointF;->set(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v2

    goto/16 :goto_a

    :cond_87
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v10, 0x7

    if-ne v1, v10, :cond_88

    iget v1, v6, Landroid/graphics/RectF;->bottom:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->top:F

    goto :goto_11

    :cond_88
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v10, 0x4

    if-ne v1, v10, :cond_89

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->right:F

    goto :goto_11

    :cond_89
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v10, 0x5

    if-ne v1, v10, :cond_86

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->left:F

    goto :goto_11

    :cond_8a
    const/4 v1, 0x0

    cmpg-float v1, p7, v1

    if-gez v1, :cond_ca

    const/high16 v1, 0x43b40000    # 360.0f

    add-float v1, v1, p7

    :goto_12
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v10, v10, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v11, 0x2

    if-ne v10, v11, :cond_8e

    const/4 v10, 0x0

    cmpl-float v10, v1, v10

    if-lez v10, :cond_8b

    const/high16 v10, 0x42b40000    # 90.0f

    cmpg-float v10, v1, v10

    if-gez v10, :cond_8b

    iget v1, v6, Landroid/graphics/RectF;->top:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_11

    :cond_8b
    const/high16 v10, 0x42b40000    # 90.0f

    cmpl-float v10, v1, v10

    if-lez v10, :cond_8c

    const/high16 v10, 0x43340000    # 180.0f

    cmpg-float v10, v1, v10

    if-gez v10, :cond_8c

    iget v1, v6, Landroid/graphics/RectF;->top:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_11

    :cond_8c
    const/high16 v10, 0x43340000    # 180.0f

    cmpl-float v10, v1, v10

    if-lez v10, :cond_8d

    const/high16 v10, 0x43870000    # 270.0f

    cmpg-float v10, v1, v10

    if-gez v10, :cond_8d

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->right:F

    goto/16 :goto_11

    :cond_8d
    const/high16 v10, 0x43870000    # 270.0f

    cmpl-float v10, v1, v10

    if-lez v10, :cond_86

    const/high16 v10, 0x43b40000    # 360.0f

    cmpg-float v1, v1, v10

    if-gez v1, :cond_86

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->left:F

    goto/16 :goto_11

    :cond_8e
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v10, v10, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v11, 0x5

    if-ne v10, v11, :cond_92

    const/4 v10, 0x0

    cmpl-float v10, v1, v10

    if-lez v10, :cond_8f

    const/high16 v10, 0x42b40000    # 90.0f

    cmpg-float v10, v1, v10

    if-gez v10, :cond_8f

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->left:F

    goto/16 :goto_11

    :cond_8f
    const/high16 v10, 0x42b40000    # 90.0f

    cmpl-float v10, v1, v10

    if-lez v10, :cond_90

    const/high16 v10, 0x43340000    # 180.0f

    cmpg-float v10, v1, v10

    if-gez v10, :cond_90

    iget v1, v6, Landroid/graphics/RectF;->top:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_11

    :cond_90
    const/high16 v10, 0x43340000    # 180.0f

    cmpl-float v10, v1, v10

    if-lez v10, :cond_91

    const/high16 v10, 0x43870000    # 270.0f

    cmpg-float v10, v1, v10

    if-gez v10, :cond_91

    iget v1, v6, Landroid/graphics/RectF;->bottom:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->top:F

    goto/16 :goto_11

    :cond_91
    const/high16 v10, 0x43870000    # 270.0f

    cmpl-float v10, v1, v10

    if-lez v10, :cond_86

    const/high16 v10, 0x43b40000    # 360.0f

    cmpg-float v1, v1, v10

    if-gez v1, :cond_86

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->left:F

    goto/16 :goto_11

    :cond_92
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v10, v10, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v11, 0x7

    if-ne v10, v11, :cond_96

    const/4 v10, 0x0

    cmpl-float v10, v1, v10

    if-lez v10, :cond_93

    const/high16 v10, 0x42b40000    # 90.0f

    cmpg-float v10, v1, v10

    if-gez v10, :cond_93

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->left:F

    goto/16 :goto_11

    :cond_93
    const/high16 v10, 0x42b40000    # 90.0f

    cmpl-float v10, v1, v10

    if-lez v10, :cond_94

    const/high16 v10, 0x43340000    # 180.0f

    cmpg-float v10, v1, v10

    if-gez v10, :cond_94

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->right:F

    goto/16 :goto_11

    :cond_94
    const/high16 v10, 0x43340000    # 180.0f

    cmpl-float v10, v1, v10

    if-lez v10, :cond_95

    const/high16 v10, 0x43870000    # 270.0f

    cmpg-float v10, v1, v10

    if-gez v10, :cond_95

    iget v1, v6, Landroid/graphics/RectF;->bottom:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->top:F

    goto/16 :goto_11

    :cond_95
    const/high16 v10, 0x43870000    # 270.0f

    cmpl-float v10, v1, v10

    if-lez v10, :cond_86

    const/high16 v10, 0x43b40000    # 360.0f

    cmpg-float v1, v1, v10

    if-gez v1, :cond_86

    iget v1, v6, Landroid/graphics/RectF;->bottom:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->top:F

    goto/16 :goto_11

    :cond_96
    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v10, v10, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v11, 0x4

    if-ne v10, v11, :cond_86

    const/4 v10, 0x0

    cmpl-float v10, v1, v10

    if-lez v10, :cond_97

    const/high16 v10, 0x42b40000    # 90.0f

    cmpg-float v10, v1, v10

    if-gez v10, :cond_97

    iget v1, v6, Landroid/graphics/RectF;->top:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_11

    :cond_97
    const/high16 v10, 0x42b40000    # 90.0f

    cmpl-float v10, v1, v10

    if-lez v10, :cond_98

    const/high16 v10, 0x43340000    # 180.0f

    cmpg-float v10, v1, v10

    if-gez v10, :cond_98

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->right:F

    goto/16 :goto_11

    :cond_98
    const/high16 v10, 0x43340000    # 180.0f

    cmpl-float v10, v1, v10

    if-lez v10, :cond_99

    const/high16 v10, 0x43870000    # 270.0f

    cmpg-float v10, v1, v10

    if-gez v10, :cond_99

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    iget v11, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v10, v11

    mul-float v10, v10, p9

    add-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->right:F

    goto/16 :goto_11

    :cond_99
    const/high16 v10, 0x43870000    # 270.0f

    cmpl-float v10, v1, v10

    if-lez v10, :cond_86

    const/high16 v10, 0x43b40000    # 360.0f

    cmpg-float v1, v1, v10

    if-gez v1, :cond_86

    iget v1, v6, Landroid/graphics/RectF;->bottom:F

    iget v10, v6, Landroid/graphics/RectF;->right:F

    iget v11, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    div-float v10, v10, p9

    sub-float/2addr v1, v10

    iput v1, v6, Landroid/graphics/RectF;->top:F

    goto/16 :goto_11

    :cond_9a
    iget v11, v4, Landroid/graphics/PointF;->y:F

    sub-float v11, v10, v11

    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    move-result v11

    const v12, 0x38d1b717    # 1.0E-4f

    cmpg-float v11, v11, v12

    if-gez v11, :cond_9b

    iget v1, v4, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v9

    iput v1, v4, Landroid/graphics/PointF;->y:F

    move/from16 v0, p7

    neg-float v1, v0

    invoke-direct {p0, v7, v8, v1, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v4

    move/from16 v0, p7

    neg-float v1, v0

    invoke-direct {p0, v7, v8, v1, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v3

    invoke-direct {p0, v4, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v1

    goto/16 :goto_b

    :cond_9b
    iget v11, v3, Landroid/graphics/PointF;->y:F

    sub-float v11, v10, v11

    invoke-static {v11}, Ljava/lang/Math;->abs(F)F

    move-result v11

    const v12, 0x38d1b717    # 1.0E-4f

    cmpg-float v11, v11, v12

    if-gez v11, :cond_9c

    iget v1, v3, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v9

    iput v1, v3, Landroid/graphics/PointF;->y:F

    move/from16 v0, p7

    neg-float v1, v0

    invoke-direct {p0, v7, v8, v1, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v4

    move/from16 v0, p7

    neg-float v1, v0

    invoke-direct {p0, v7, v8, v1, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v3

    invoke-direct {p0, v4, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v1

    goto/16 :goto_b

    :cond_9c
    iget v11, v2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v10, v11

    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v10

    const v11, 0x38d1b717    # 1.0E-4f

    cmpg-float v10, v10, v11

    if-gez v10, :cond_1f

    iget v1, v2, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v9

    iput v1, v2, Landroid/graphics/PointF;->y:F

    move/from16 v0, p7

    neg-float v1, v0

    invoke-direct {p0, v7, v8, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v5

    move/from16 v0, p7

    neg-float v1, v0

    invoke-direct {p0, v7, v8, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v2

    invoke-direct {p0, v5, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v1

    goto/16 :goto_b

    :cond_9d
    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v2, v6, Landroid/graphics/RectF;->bottom:F

    iget v3, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, v3

    mul-float v2, v2, p9

    add-float/2addr v1, v2

    iput v1, v6, Landroid/graphics/RectF;->right:F

    goto/16 :goto_c

    :cond_9e
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v7, 0x6

    if-ne v5, v7, :cond_a0

    iget v1, v3, Landroid/graphics/PointF;->x:F

    iget v2, v2, Landroid/graphics/PointF;->x:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_9f

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v2, v6, Landroid/graphics/RectF;->bottom:F

    iget v3, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, v3

    mul-float v2, v2, p9

    add-float/2addr v1, v2

    iput v1, v6, Landroid/graphics/RectF;->right:F

    goto/16 :goto_c

    :cond_9f
    iget v1, v6, Landroid/graphics/RectF;->bottom:F

    iget v2, v6, Landroid/graphics/RectF;->right:F

    iget v3, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    div-float v2, v2, p9

    sub-float/2addr v1, v2

    iput v1, v6, Landroid/graphics/RectF;->top:F

    goto/16 :goto_c

    :cond_a0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/16 v5, 0x8

    if-ne v3, v5, :cond_a2

    iget v1, v2, Landroid/graphics/PointF;->x:F

    iget v2, v4, Landroid/graphics/PointF;->x:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_a1

    iget v1, v6, Landroid/graphics/RectF;->bottom:F

    iget v2, v6, Landroid/graphics/RectF;->right:F

    iget v3, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    div-float v2, v2, p9

    sub-float/2addr v1, v2

    iput v1, v6, Landroid/graphics/RectF;->top:F

    goto/16 :goto_c

    :cond_a1
    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v2, v6, Landroid/graphics/RectF;->bottom:F

    iget v3, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, v3

    mul-float v2, v2, p9

    sub-float/2addr v1, v2

    iput v1, v6, Landroid/graphics/RectF;->left:F

    goto/16 :goto_c

    :cond_a2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_21

    iget v2, v4, Landroid/graphics/PointF;->x:F

    iget v1, v1, Landroid/graphics/PointF;->x:F

    cmpg-float v1, v2, v1

    if-gez v1, :cond_a3

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v2, v6, Landroid/graphics/RectF;->bottom:F

    iget v3, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, v3

    mul-float v2, v2, p9

    sub-float/2addr v1, v2

    iput v1, v6, Landroid/graphics/RectF;->left:F

    goto/16 :goto_c

    :cond_a3
    iget v1, v6, Landroid/graphics/RectF;->top:F

    iget v2, v6, Landroid/graphics/RectF;->right:F

    iget v3, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    div-float v2, v2, p9

    add-float/2addr v1, v2

    iput v1, v6, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_c

    :cond_a4
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v7, 0x1

    if-ne v5, v7, :cond_a6

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget v2, v3, Landroid/graphics/PointF;->x:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_a5

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v2, v6, Landroid/graphics/RectF;->bottom:F

    iget v3, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, v3

    mul-float v2, v2, p9

    add-float/2addr v1, v2

    iput v1, v6, Landroid/graphics/RectF;->right:F

    goto/16 :goto_c

    :cond_a5
    iget v1, v6, Landroid/graphics/RectF;->top:F

    iget v2, v6, Landroid/graphics/RectF;->right:F

    iget v3, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    div-float v2, v2, p9

    add-float/2addr v1, v2

    iput v1, v6, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_c

    :cond_a6
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v7, 0x6

    if-ne v5, v7, :cond_a8

    iget v1, v3, Landroid/graphics/PointF;->x:F

    iget v2, v2, Landroid/graphics/PointF;->x:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_a7

    iget v1, v6, Landroid/graphics/RectF;->bottom:F

    iget v2, v6, Landroid/graphics/RectF;->right:F

    iget v3, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    div-float v2, v2, p9

    sub-float/2addr v1, v2

    iput v1, v6, Landroid/graphics/RectF;->top:F

    goto/16 :goto_c

    :cond_a7
    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v2, v6, Landroid/graphics/RectF;->bottom:F

    iget v3, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, v3

    mul-float v2, v2, p9

    add-float/2addr v1, v2

    iput v1, v6, Landroid/graphics/RectF;->right:F

    goto/16 :goto_c

    :cond_a8
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/16 v5, 0x8

    if-ne v3, v5, :cond_aa

    iget v1, v2, Landroid/graphics/PointF;->x:F

    iget v2, v4, Landroid/graphics/PointF;->x:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_a9

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v2, v6, Landroid/graphics/RectF;->bottom:F

    iget v3, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, v3

    mul-float v2, v2, p9

    sub-float/2addr v1, v2

    iput v1, v6, Landroid/graphics/RectF;->left:F

    goto/16 :goto_c

    :cond_a9
    iget v1, v6, Landroid/graphics/RectF;->bottom:F

    iget v2, v6, Landroid/graphics/RectF;->right:F

    iget v3, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    div-float v2, v2, p9

    sub-float/2addr v1, v2

    iput v1, v6, Landroid/graphics/RectF;->top:F

    goto/16 :goto_c

    :cond_aa
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_21

    iget v2, v4, Landroid/graphics/PointF;->x:F

    iget v1, v1, Landroid/graphics/PointF;->x:F

    cmpg-float v1, v2, v1

    if-gez v1, :cond_ab

    iget v1, v6, Landroid/graphics/RectF;->top:F

    iget v2, v6, Landroid/graphics/RectF;->right:F

    iget v3, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    div-float v2, v2, p9

    add-float/2addr v1, v2

    iput v1, v6, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_c

    :cond_ab
    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v2, v6, Landroid/graphics/RectF;->bottom:F

    iget v3, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, v3

    mul-float v2, v2, p9

    sub-float/2addr v1, v2

    iput v1, v6, Landroid/graphics/RectF;->left:F

    goto/16 :goto_c

    :cond_ac
    if-eqz p8, :cond_21

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isCornerZonePressed()Z

    move-result v2

    if-nez v2, :cond_21

    iget v2, v1, Landroid/graphics/PointF;->x:F

    iget v4, v4, Landroid/graphics/PointF;->x:F

    sub-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const v4, 0x38d1b717    # 1.0E-4f

    cmpg-float v2, v2, v4

    if-ltz v2, :cond_ad

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget v2, v3, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const v2, 0x38d1b717    # 1.0E-4f

    cmpg-float v1, v1, v2

    if-gez v1, :cond_b1

    :cond_ad
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_ae

    iget v1, v6, Landroid/graphics/RectF;->top:F

    iget v2, v6, Landroid/graphics/RectF;->right:F

    iget v3, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    div-float v2, v2, p9

    add-float/2addr v1, v2

    iput v1, v6, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_c

    :cond_ae
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v2, 0x7

    if-ne v1, v2, :cond_af

    iget v1, v6, Landroid/graphics/RectF;->bottom:F

    iget v2, v6, Landroid/graphics/RectF;->right:F

    iget v3, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    div-float v2, v2, p9

    sub-float/2addr v1, v2

    iput v1, v6, Landroid/graphics/RectF;->top:F

    goto/16 :goto_c

    :cond_af
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_b0

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v2, v6, Landroid/graphics/RectF;->bottom:F

    iget v3, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, v3

    mul-float v2, v2, p9

    add-float/2addr v1, v2

    iput v1, v6, Landroid/graphics/RectF;->right:F

    goto/16 :goto_c

    :cond_b0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v2, 0x5

    if-ne v1, v2, :cond_21

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v2, v6, Landroid/graphics/RectF;->bottom:F

    iget v3, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, v3

    mul-float v2, v2, p9

    sub-float/2addr v1, v2

    iput v1, v6, Landroid/graphics/RectF;->left:F

    goto/16 :goto_c

    :cond_b1
    const/4 v1, 0x0

    cmpg-float v1, p7, v1

    if-gez v1, :cond_b2

    const/high16 v1, 0x43b40000    # 360.0f

    add-float p7, p7, v1

    :cond_b2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_b6

    const/4 v1, 0x0

    cmpl-float v1, p7, v1

    if-lez v1, :cond_b3

    const/high16 v1, 0x42b40000    # 90.0f

    cmpg-float v1, p7, v1

    if-gez v1, :cond_b3

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v2, v6, Landroid/graphics/RectF;->bottom:F

    iget v3, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, v3

    mul-float v2, v2, p9

    sub-float/2addr v1, v2

    iput v1, v6, Landroid/graphics/RectF;->left:F

    goto/16 :goto_c

    :cond_b3
    const/high16 v1, 0x42b40000    # 90.0f

    cmpl-float v1, p7, v1

    if-lez v1, :cond_b4

    const/high16 v1, 0x43340000    # 180.0f

    cmpg-float v1, p7, v1

    if-gez v1, :cond_b4

    iget v1, v6, Landroid/graphics/RectF;->top:F

    iget v2, v6, Landroid/graphics/RectF;->right:F

    iget v3, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    div-float v2, v2, p9

    add-float/2addr v1, v2

    iput v1, v6, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_c

    :cond_b4
    const/high16 v1, 0x43340000    # 180.0f

    cmpl-float v1, p7, v1

    if-lez v1, :cond_b5

    const/high16 v1, 0x43870000    # 270.0f

    cmpg-float v1, p7, v1

    if-gez v1, :cond_b5

    iget v1, v6, Landroid/graphics/RectF;->top:F

    iget v2, v6, Landroid/graphics/RectF;->right:F

    iget v3, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    div-float v2, v2, p9

    add-float/2addr v1, v2

    iput v1, v6, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_c

    :cond_b5
    const/high16 v1, 0x43870000    # 270.0f

    cmpl-float v1, p7, v1

    if-lez v1, :cond_21

    const/high16 v1, 0x43b40000    # 360.0f

    cmpg-float v1, p7, v1

    if-gez v1, :cond_21

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v2, v6, Landroid/graphics/RectF;->bottom:F

    iget v3, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, v3

    mul-float v2, v2, p9

    add-float/2addr v1, v2

    iput v1, v6, Landroid/graphics/RectF;->right:F

    goto/16 :goto_c

    :cond_b6
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v2, 0x5

    if-ne v1, v2, :cond_ba

    const/4 v1, 0x0

    cmpl-float v1, p7, v1

    if-lez v1, :cond_b7

    const/high16 v1, 0x42b40000    # 90.0f

    cmpg-float v1, p7, v1

    if-gez v1, :cond_b7

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v2, v6, Landroid/graphics/RectF;->bottom:F

    iget v3, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, v3

    mul-float v2, v2, p9

    sub-float/2addr v1, v2

    iput v1, v6, Landroid/graphics/RectF;->left:F

    goto/16 :goto_c

    :cond_b7
    const/high16 v1, 0x42b40000    # 90.0f

    cmpl-float v1, p7, v1

    if-lez v1, :cond_b8

    const/high16 v1, 0x43340000    # 180.0f

    cmpg-float v1, p7, v1

    if-gez v1, :cond_b8

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v2, v6, Landroid/graphics/RectF;->bottom:F

    iget v3, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, v3

    mul-float v2, v2, p9

    sub-float/2addr v1, v2

    iput v1, v6, Landroid/graphics/RectF;->left:F

    goto/16 :goto_c

    :cond_b8
    const/high16 v1, 0x43340000    # 180.0f

    cmpl-float v1, p7, v1

    if-lez v1, :cond_b9

    const/high16 v1, 0x43870000    # 270.0f

    cmpg-float v1, p7, v1

    if-gez v1, :cond_b9

    iget v1, v6, Landroid/graphics/RectF;->top:F

    iget v2, v6, Landroid/graphics/RectF;->right:F

    iget v3, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    div-float v2, v2, p9

    add-float/2addr v1, v2

    iput v1, v6, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_c

    :cond_b9
    const/high16 v1, 0x43870000    # 270.0f

    cmpl-float v1, p7, v1

    if-lez v1, :cond_21

    const/high16 v1, 0x43b40000    # 360.0f

    cmpg-float v1, p7, v1

    if-gez v1, :cond_21

    iget v1, v6, Landroid/graphics/RectF;->bottom:F

    iget v2, v6, Landroid/graphics/RectF;->right:F

    iget v3, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    div-float v2, v2, p9

    sub-float/2addr v1, v2

    iput v1, v6, Landroid/graphics/RectF;->top:F

    goto/16 :goto_c

    :cond_ba
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v2, 0x7

    if-ne v1, v2, :cond_be

    const/4 v1, 0x0

    cmpl-float v1, p7, v1

    if-lez v1, :cond_bb

    const/high16 v1, 0x42b40000    # 90.0f

    cmpg-float v1, p7, v1

    if-gez v1, :cond_bb

    iget v1, v6, Landroid/graphics/RectF;->bottom:F

    iget v2, v6, Landroid/graphics/RectF;->right:F

    iget v3, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    div-float v2, v2, p9

    sub-float/2addr v1, v2

    iput v1, v6, Landroid/graphics/RectF;->top:F

    goto/16 :goto_c

    :cond_bb
    const/high16 v1, 0x42b40000    # 90.0f

    cmpl-float v1, p7, v1

    if-lez v1, :cond_bc

    const/high16 v1, 0x43340000    # 180.0f

    cmpg-float v1, p7, v1

    if-gez v1, :cond_bc

    iget v1, v6, Landroid/graphics/RectF;->right:F

    iget v2, v6, Landroid/graphics/RectF;->bottom:F

    iget v3, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, v3

    mul-float v2, v2, p9

    sub-float/2addr v1, v2

    iput v1, v6, Landroid/graphics/RectF;->left:F

    goto/16 :goto_c

    :cond_bc
    const/high16 v1, 0x43340000    # 180.0f

    cmpl-float v1, p7, v1

    if-lez v1, :cond_bd

    const/high16 v1, 0x43870000    # 270.0f

    cmpg-float v1, p7, v1

    if-gez v1, :cond_bd

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v2, v6, Landroid/graphics/RectF;->bottom:F

    iget v3, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, v3

    mul-float v2, v2, p9

    add-float/2addr v1, v2

    iput v1, v6, Landroid/graphics/RectF;->right:F

    goto/16 :goto_c

    :cond_bd
    const/high16 v1, 0x43870000    # 270.0f

    cmpl-float v1, p7, v1

    if-lez v1, :cond_21

    const/high16 v1, 0x43b40000    # 360.0f

    cmpg-float v1, p7, v1

    if-gez v1, :cond_21

    iget v1, v6, Landroid/graphics/RectF;->bottom:F

    iget v2, v6, Landroid/graphics/RectF;->right:F

    iget v3, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    div-float v2, v2, p9

    sub-float/2addr v1, v2

    iput v1, v6, Landroid/graphics/RectF;->top:F

    goto/16 :goto_c

    :cond_be
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_21

    const/4 v1, 0x0

    cmpl-float v1, p7, v1

    if-lez v1, :cond_bf

    const/high16 v1, 0x42b40000    # 90.0f

    cmpg-float v1, p7, v1

    if-gez v1, :cond_bf

    iget v1, v6, Landroid/graphics/RectF;->bottom:F

    iget v2, v6, Landroid/graphics/RectF;->right:F

    iget v3, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    div-float v2, v2, p9

    sub-float/2addr v1, v2

    iput v1, v6, Landroid/graphics/RectF;->top:F

    goto/16 :goto_c

    :cond_bf
    const/high16 v1, 0x42b40000    # 90.0f

    cmpl-float v1, p7, v1

    if-lez v1, :cond_c0

    const/high16 v1, 0x43340000    # 180.0f

    cmpg-float v1, p7, v1

    if-gez v1, :cond_c0

    iget v1, v6, Landroid/graphics/RectF;->top:F

    iget v2, v6, Landroid/graphics/RectF;->right:F

    iget v3, v6, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    div-float v2, v2, p9

    add-float/2addr v1, v2

    iput v1, v6, Landroid/graphics/RectF;->bottom:F

    goto/16 :goto_c

    :cond_c0
    const/high16 v1, 0x43340000    # 180.0f

    cmpl-float v1, p7, v1

    if-lez v1, :cond_c1

    const/high16 v1, 0x43870000    # 270.0f

    cmpg-float v1, p7, v1

    if-gez v1, :cond_c1

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v2, v6, Landroid/graphics/RectF;->bottom:F

    iget v3, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, v3

    mul-float v2, v2, p9

    add-float/2addr v1, v2

    iput v1, v6, Landroid/graphics/RectF;->right:F

    goto/16 :goto_c

    :cond_c1
    const/high16 v1, 0x43870000    # 270.0f

    cmpl-float v1, p7, v1

    if-lez v1, :cond_21

    const/high16 v1, 0x43b40000    # 360.0f

    cmpg-float v1, p7, v1

    if-gez v1, :cond_21

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v2, v6, Landroid/graphics/RectF;->bottom:F

    iget v3, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, v3

    mul-float v2, v2, p9

    add-float/2addr v1, v2

    iput v1, v6, Landroid/graphics/RectF;->right:F

    goto/16 :goto_c

    :cond_c2
    if-eqz p2, :cond_21

    new-instance v1, Landroid/graphics/PointF;

    iget v2, v6, Landroid/graphics/RectF;->left:F

    iget v3, v6, Landroid/graphics/RectF;->top:F

    invoke-direct {v1, v2, v3}, Landroid/graphics/PointF;-><init>(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v1

    new-instance v2, Landroid/graphics/PointF;

    iget v3, v6, Landroid/graphics/RectF;->right:F

    iget v4, v6, Landroid/graphics/RectF;->top:F

    invoke-direct {v2, v3, v4}, Landroid/graphics/PointF;-><init>(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v2

    new-instance v3, Landroid/graphics/PointF;

    iget v4, v6, Landroid/graphics/RectF;->left:F

    iget v5, v6, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v3, v4, v5}, Landroid/graphics/PointF;-><init>(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v3

    new-instance v4, Landroid/graphics/PointF;

    iget v5, v6, Landroid/graphics/RectF;->right:F

    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v4, v5, v6}, Landroid/graphics/PointF;-><init>(FF)V

    move/from16 v0, p7

    invoke-direct {p0, v7, v8, v0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->rotatePoint(FFFLandroid/graphics/PointF;)Landroid/graphics/PointF;

    move-result-object v4

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->findPoints(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)[F

    move-result-object v1

    const/4 v2, 0x0

    aget v2, v1, v2

    const/4 v3, 0x1

    aget v3, v1, v3

    const/4 v4, 0x2

    aget v4, v1, v4

    const/4 v5, 0x3

    aget v1, v1, v5

    if-eqz p4, :cond_c3

    add-float v5, v2, p5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->left:F

    cmpg-float v5, v5, v6

    if-gez v5, :cond_c3

    const/4 v1, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, " Clipped X Left : "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p5

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    const/4 v1, 0x1

    goto/16 :goto_1

    :cond_c3
    if-eqz p2, :cond_c4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    cmpg-float v2, v2, v5

    if-gez v2, :cond_c4

    const/4 v1, 0x1

    goto/16 :goto_1

    :cond_c4
    if-eqz p4, :cond_c5

    add-float v2, v4, p5

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->right:F

    cmpl-float v2, v2, v5

    if-lez v2, :cond_c5

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, " Clipped X Right "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p5

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    const/4 v1, 0x1

    goto/16 :goto_1

    :cond_c5
    if-eqz p2, :cond_c6

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    cmpl-float v2, v4, v2

    if-lez v2, :cond_c6

    const/4 v1, 0x1

    goto/16 :goto_1

    :cond_c6
    if-eqz p4, :cond_c7

    add-float v2, v3, p6

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    cmpg-float v2, v2, v4

    if-gez v2, :cond_c7

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v4, " Clipped Top "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p6

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    const/4 v1, 0x1

    goto/16 :goto_1

    :cond_c7
    if-eqz p2, :cond_c8

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    cmpg-float v2, v3, v2

    if-gez v2, :cond_c8

    const/4 v1, 0x1

    goto/16 :goto_1

    :cond_c8
    if-eqz p4, :cond_c9

    add-float v2, v1, p6

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    cmpl-float v2, v2, v3

    if-lez v2, :cond_c9

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, " Clipped Bottom "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    move/from16 v0, p6

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    const/4 v1, 0x1

    goto/16 :goto_1

    :cond_c9
    if-eqz p2, :cond_21

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    cmpl-float v1, v1, v2

    if-lez v1, :cond_21

    const/4 v1, 0x1

    goto/16 :goto_1

    :cond_ca
    move/from16 v1, p7

    goto/16 :goto_12

    :cond_cb
    move/from16 v1, p7

    goto/16 :goto_10

    :cond_cc
    move/from16 v1, p7

    goto/16 :goto_e

    :cond_cd
    move-object v6, v1

    goto/16 :goto_0
.end method

.method public isClosed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsClosed:Z

    return v0
.end method

.method public isContextMenuItemEnabled(I)Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->getItemEnabled(I)Z

    move-result v0

    goto :goto_0
.end method

.method public isContextMenuVisible()Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mVisible:Z

    return v0
.end method

.method public isDimEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsDim:Z

    return v0
.end method

.method public isObjectOutlineEnabled()Z
    .locals 1

    sget-boolean v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectOutlineEnable:Z

    return v0
.end method

.method public isTouchEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchEnable:Z

    return v0
.end method

.method protected onCanvasHoverEnter()V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isMoveZonePressed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isResizeZonePressed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isRotateZonePressed()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onObjectChanged()V

    :cond_1
    return-void
.end method

.method protected onCheckTouchPosition(Landroid/graphics/RectF;IILcom/samsung/android/sdk/pen/document/SpenObjectBase;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;)V
    .locals 6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchZone:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->checkTouchPosition(Landroid/graphics/RectF;IILcom/samsung/android/sdk/pen/document/SpenObjectBase;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;)V

    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 12

    const/high16 v11, 0x40a00000    # 5.0f

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mStyle:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mFirstDraw:Z

    if-eqz v0, :cond_2

    const-string/jumbo v0, "ControlBase.onDraw is called, for the first time."

    invoke-static {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iput-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mFirstDraw:Z

    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onPrepareDraw(Landroid/graphics/Canvas;)V

    :cond_2
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->normalizeDegree(F)F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsDim:Z

    if-eqz v0, :cond_3

    const/high16 v0, 0x40000000    # 2.0f

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    :cond_3
    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_8

    move v1, v2

    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotX(I)F

    move-result v0

    float-to-int v4, v0

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getControlPivotY(I)F

    move-result v0

    float-to-int v5, v0

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    float-to-int v0, v3

    if-gez v0, :cond_4

    add-int/lit16 v0, v0, 0x168

    :cond_4
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isRotateZonePressed()Z

    move-result v6

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempRectF:Landroid/graphics/RectF;

    add-int/lit8 v7, v4, -0x2f

    int-to-float v7, v7

    add-int/lit8 v8, v5, -0x28

    int-to-float v8, v8

    add-int/lit8 v9, v4, 0x2f

    int-to-float v9, v9

    add-int/lit8 v10, v5, 0xa

    int-to-float v10, v10

    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempRectF:Landroid/graphics/RectF;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    const/4 v8, 0x4

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->getPaint(I)Landroid/graphics/Paint;

    move-result-object v7

    invoke-virtual {p1, v6, v11, v11, v7}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v6, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v0, "\u00b0"

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    int-to-float v6, v4

    int-to-float v7, v5

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    const/4 v9, 0x5

    invoke-virtual {v8, v9}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->getPaint(I)Landroid/graphics/Paint;

    move-result-object v8

    invoke-virtual {p1, v0, v6, v7, v8}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mTouchedObjectIndex:I

    if-eq v1, v0, :cond_b

    const/4 v6, -0x1

    if-eq v0, v6, :cond_b

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getRotateable(I)Z

    move-result v3

    if-eqz v3, :cond_a

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderAngle(I)F

    move-result v0

    sub-float v0, v3, v0

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderAngle(I)F

    move-result v3

    add-float/2addr v0, v3

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->normalizeDegree(F)F

    move-result v0

    :goto_2
    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "center["

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v6, "]:"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v6, ","

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v6, ", angle:"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    int-to-float v3, v4

    int-to-float v4, v5

    invoke-virtual {p1, v0, v3, v4}, Landroid/graphics/Canvas;->rotate(FFF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isResizeZonePressed()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isMoveZonePressed()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isRotateZonePressed()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v3}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempRect:Landroid/graphics/Rect;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {p0, p1, v3, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onDrawObject(Landroid/graphics/Canvas;Landroid/graphics/Rect;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    :cond_7
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_1

    :cond_8
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    invoke-direct {p0, p1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->drawHighlightStroke(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;)V

    :cond_9
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderAngle(I)F

    move-result v0

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/RectF;->centerX()F

    move-result v3

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    invoke-virtual {p1, v0, v3, v4}, Landroid/graphics/Canvas;->rotate(FFF)V

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->drawDimmingWindow(Landroid/graphics/Canvas;Landroid/graphics/RectF;)V

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {p0, p1, v3, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onDrawBorder(Landroid/graphics/Canvas;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    :cond_a
    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_b
    move v0, v3

    goto/16 :goto_2
.end method

.method protected onDrawBorder(Landroid/graphics/Canvas;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 9

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mStyle:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p3}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isRotatable()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p3}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getResizeOption()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_4

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p3}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isRotatable()Z

    move-result v2

    invoke-virtual {p3}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getResizeOption()I

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_5

    const/4 v1, 0x1

    move v6, v1

    :goto_2
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mStyle:I

    const/4 v3, 0x2

    if-ne v1, v3, :cond_8

    const/4 v1, 0x0

    const/4 v0, 0x0

    move v7, v1

    :goto_3
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchZone:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->getRect(ILandroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    invoke-virtual {v8}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    invoke-virtual {p2}, Landroid/graphics/RectF;->centerX()F

    move-result v3

    iget v4, p2, Landroid/graphics/RectF;->top:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->getPaint(I)Landroid/graphics/Paint;

    move-result-object v5

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    const-string/jumbo v0, "handler_icon_rotate"

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getDrawableImage(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v0, v1

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    int-to-float v0, v0

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v0, v3

    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->getType()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    const/high16 v3, 0x42380000    # 46.0f

    mul-float/2addr v1, v3

    const/high16 v3, 0x41f00000    # 30.0f

    div-float/2addr v1, v3

    const/high16 v3, 0x42380000    # 46.0f

    mul-float/2addr v0, v3

    const/high16 v3, 0x41f00000    # 30.0f

    div-float/2addr v0, v3

    :cond_2
    invoke-virtual {v8}, Landroid/graphics/RectF;->centerX()F

    move-result v3

    invoke-virtual {v8}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    sub-float v5, v3, v1

    float-to-int v5, v5

    sub-float v8, v4, v0

    float-to-int v8, v8

    add-float/2addr v1, v3

    float-to-int v1, v1

    add-float/2addr v0, v4

    float-to-int v0, v0

    invoke-virtual {v2, v5, v8, v1, v0}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_3
    if-eqz v7, :cond_7

    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->getType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_6

    const-string/jumbo v0, "handler_icon"

    const/16 v1, 0x26

    const/16 v2, 0x26

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resizeImage(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_4
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->getPaint(I)Landroid/graphics/Paint;

    move-result-object v1

    invoke-virtual {p1, p2, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    invoke-virtual {p0, v0, p1, p2, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->draw8Points(Landroid/graphics/drawable/Drawable;Landroid/graphics/Canvas;Landroid/graphics/RectF;Z)V

    goto/16 :goto_0

    :cond_4
    const/4 v0, 0x1

    goto/16 :goto_1

    :cond_5
    const/4 v1, 0x0

    move v6, v1

    goto/16 :goto_2

    :cond_6
    const-string/jumbo v0, "handler_icon"

    const/16 v1, 0x16

    const/16 v2, 0x16

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resizeImage(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_4

    :cond_7
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->getPaint(I)Landroid/graphics/Paint;

    move-result-object v0

    const/high16 v1, 0x40800000    # 4.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget v1, p2, Landroid/graphics/RectF;->left:F

    const/high16 v2, 0x40800000    # 4.0f

    sub-float/2addr v1, v2

    const/high16 v2, 0x3f800000    # 1.0f

    add-float/2addr v1, v2

    iget v2, p2, Landroid/graphics/RectF;->top:F

    const/high16 v3, 0x40800000    # 4.0f

    sub-float/2addr v2, v3

    const/high16 v3, 0x3f800000    # 1.0f

    add-float/2addr v2, v3

    iget v3, p2, Landroid/graphics/RectF;->right:F

    const/high16 v4, 0x40800000    # 4.0f

    add-float/2addr v3, v4

    const/high16 v4, 0x3f800000    # 1.0f

    sub-float/2addr v3, v4

    iget v4, p2, Landroid/graphics/RectF;->bottom:F

    const/high16 v5, 0x40800000    # 4.0f

    add-float/2addr v4, v5

    const/high16 v5, 0x3f800000    # 1.0f

    sub-float/2addr v4, v5

    invoke-virtual {p2, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    invoke-virtual {p1, p2, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto/16 :goto_0

    :cond_8
    move v7, v0

    move v0, v2

    goto/16 :goto_3
.end method

.method protected onDrawObject(Landroid/graphics/Canvas;Landroid/graphics/Rect;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 3

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, p2}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iget v1, v0, Landroid/graphics/Rect;->left:I

    iget v2, v0, Landroid/graphics/Rect;->right:I

    if-le v1, v2, :cond_0

    iget v1, p2, Landroid/graphics/Rect;->right:I

    iput v1, v0, Landroid/graphics/Rect;->left:I

    iget v1, p2, Landroid/graphics/Rect;->left:I

    iput v1, v0, Landroid/graphics/Rect;->right:I

    :cond_0
    iget v1, v0, Landroid/graphics/Rect;->top:I

    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    if-le v1, v2, :cond_1

    iget v1, p2, Landroid/graphics/Rect;->bottom:I

    iput v1, v0, Landroid/graphics/Rect;->top:I

    iget v1, p2, Landroid/graphics/Rect;->top:I

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->getPaint(I)Landroid/graphics/Paint;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    return-void
.end method

.method protected onFlip(ILcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 3

    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, " Flip is occured, direction : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->i(ZLjava/lang/String;)V

    return-void
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3

    const/4 v0, 0x1

    const/16 v1, 0x16

    if-ne p1, v1, :cond_1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIndex:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIndex:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIndex:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->getCountMenuItem()I

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->close()V

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIndex:I

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->scrollToMenuItem(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mHandler:Landroid/os/Handler;

    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$2;

    invoke-direct {v2, p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$2;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_1
    const/16 v1, 0x15

    if-ne p1, v1, :cond_3

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIndex:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIndex:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIndex:I

    if-gez v1, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->close()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIndex:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->scrollToMenuItem(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$3;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$3;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_3
    const/16 v0, 0x42

    if-ne p1, v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIndex:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->handleSelectMenuItem(I)V

    :cond_4
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onMenuSelected(I)V
    .locals 3

    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, " onMenuSelected is called : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->i(ZLjava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getObjectList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onMenuSelected(Ljava/util/ArrayList;I)V

    :cond_0
    return-void
.end method

.method protected onObjectChanged()V
    .locals 12

    const/4 v0, 0x0

    const-string/jumbo v1, "onObjectChanged is called.."

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mFirstDraw:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    const/4 v1, 0x0

    move v11, v0

    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_2

    if-nez v11, :cond_5

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->invalidate()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->updateContextMenu()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->show()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isOutOfViewEnabled()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Landroid/graphics/RectF;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isRotateZonePressed()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v9}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isClippedObject(IZZZFFFZF)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v11, 0x0

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderRect(I)Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {v10, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    move v0, v11

    :goto_2
    add-int/lit8 v1, v1, 0x1

    move v11, v0

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isResizeZonePressed()Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRotation()F

    move-result v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v9}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isClippedObject(IZZZFFFZF)Z

    :cond_4
    move v0, v11

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v5, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mTouchedObjectIndex:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mTouchedObjectIndex:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_e

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mTouchedObjectIndex:I

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBorderAngle(I)F

    move-result v1

    sub-float/2addr v0, v1

    move v1, v0

    :goto_3
    const/4 v0, 0x0

    move v2, v0

    :goto_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v2, v0, :cond_7

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onObjectChanged(Ljava/util/ArrayList;)V

    :cond_6
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->fit()V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->invalidate()V

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isResizeZonePressed()Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v3, -0x1

    if-ne v0, v3, :cond_9

    :cond_8
    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->resize2Threshold(I)V

    :cond_9
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-direct {p0, v4, v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isFlipEnabled()Z

    move-result v3

    if-nez v3, :cond_b

    iget v3, v4, Landroid/graphics/RectF;->left:F

    iget v6, v4, Landroid/graphics/RectF;->right:F

    cmpl-float v3, v3, v6

    if-lez v3, :cond_a

    iget v3, v4, Landroid/graphics/RectF;->right:F

    iget v6, v4, Landroid/graphics/RectF;->left:F

    iput v6, v4, Landroid/graphics/RectF;->right:F

    iput v3, v4, Landroid/graphics/RectF;->left:F

    :cond_a
    iget v3, v4, Landroid/graphics/RectF;->top:F

    iget v6, v4, Landroid/graphics/RectF;->bottom:F

    cmpl-float v3, v3, v6

    if-lez v3, :cond_b

    iget v3, v4, Landroid/graphics/RectF;->top:F

    iget v6, v4, Landroid/graphics/RectF;->bottom:F

    iput v6, v4, Landroid/graphics/RectF;->top:F

    iput v3, v4, Landroid/graphics/RectF;->bottom:F

    :cond_b
    const/4 v3, 0x0

    invoke-virtual {v0, v4, v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setRect(Landroid/graphics/RectF;Z)V

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRotateAngle:F

    if-eq v2, v5, :cond_c

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRotation()F

    move-result v3

    add-float/2addr v3, v1

    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->normalizeDegree(F)F

    move-result v3

    :cond_c
    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getRotateable(I)Z

    move-result v6

    if-eqz v6, :cond_d

    float-to-int v3, v3

    int-to-float v3, v3

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setRotation(F)V

    :cond_d
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_4

    :cond_e
    move v1, v0

    goto/16 :goto_3
.end method

.method protected onPrepareDraw(Landroid/graphics/Canvas;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mVisible:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->show()V

    :cond_0
    return-void
.end method

.method protected onRectChanged(Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 3

    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onRectChanged Rect: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->i(ZLjava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRectChanged(Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    :cond_0
    return-void
.end method

.method protected onRequestScroll(FF)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestScroll(FF)V

    :cond_0
    return-void
.end method

.method protected onRotationChanged(FLcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 2

    const/4 v0, 0x0

    const-string/jumbo v1, " onRotationChanged is called"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->i(ZLjava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRotationChanged(FLcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    :cond_0
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 11

    const/4 v1, 0x1

    const/4 v3, 0x0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isObjectAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-interface {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->setDirty()V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->fit()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v4, v0, Landroid/graphics/PointF;->x:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v5, v0, Landroid/graphics/PointF;->y:F

    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getPanKey(II)Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPanBackup:Ljava/util/HashMap;

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Point;

    if-eqz v0, :cond_8

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "onSizeChanged get : "

    invoke-direct {v2, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v7, " , "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v7, ", pan : "

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v7, v0, Landroid/graphics/Point;->y:I

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(Ljava/lang/String;)V

    iget v2, v0, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    sub-float/2addr v2, v4

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    sub-float/2addr v0, v5

    invoke-virtual {p0, v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onRequestScroll(FF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPanBackup:Ljava/util/HashMap;

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v1

    :goto_1
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-direct {p0, v2, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    const/4 v4, -0x1

    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBoundBox(I)Landroid/graphics/RectF;

    move-result-object v4

    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5}, Landroid/graphics/RectF;-><init>()V

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-direct {p0, v5, v4, v6}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "onSizeChanged absoluteObjectBoundBox : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(Ljava/lang/String;)V

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "onSizeChanged absoluteScreenRect : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPanBackup:Ljava/util/HashMap;

    invoke-direct {p0, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getPanKey(II)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Landroid/graphics/Point;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v9, v9, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v9, v9, Landroid/graphics/PointF;->x:F

    float-to-int v9, v9

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v10, v10, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v10, v10, Landroid/graphics/PointF;->y:F

    float-to-int v10, v10

    invoke-direct {v8, v9, v10}, Landroid/graphics/Point;-><init>(II)V

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "onSizeChanged put : "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, " , "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ", pan : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v7, v7, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v7, v7, Landroid/graphics/PointF;->y:F

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(Ljava/lang/String;)V

    invoke-virtual {v2, v5}, Landroid/graphics/RectF;->contains(Landroid/graphics/RectF;)Z

    move-result v6

    invoke-virtual {v5, v2}, Landroid/graphics/RectF;->intersect(Landroid/graphics/RectF;)Z

    move-result v2

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "onSizeChanged intersect : "

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v7, " , contain : "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(Ljava/lang/String;)V

    if-eqz v2, :cond_2

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isOutOfViewEnabled()Z

    move-result v2

    if-nez v2, :cond_4

    if-nez v6, :cond_4

    :cond_2
    iget v0, v4, Landroid/graphics/RectF;->bottom:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_5

    iget v0, v4, Landroid/graphics/RectF;->bottom:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v0, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v0, v2

    :goto_2
    iget v2, v4, Landroid/graphics/RectF;->left:F

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    cmpg-float v2, v2, v5

    if-gez v2, :cond_6

    iget v2, v4, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float v3, v2, v3

    :cond_3
    :goto_3
    invoke-virtual {p0, v3, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onRequestScroll(FF)V

    move v0, v1

    :cond_4
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->fit()V

    goto/16 :goto_0

    :cond_5
    iget v0, v4, Landroid/graphics/RectF;->top:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    cmpg-float v0, v0, v2

    if-gez v0, :cond_7

    iget v0, v4, Landroid/graphics/RectF;->top:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    sub-float/2addr v0, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float/2addr v0, v2

    goto :goto_2

    :cond_6
    iget v2, v4, Landroid/graphics/RectF;->right:F

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->right:F

    cmpl-float v2, v2, v5

    if-lez v2, :cond_3

    iget v2, v4, Landroid/graphics/RectF;->right:F

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    iget v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float v3, v2, v3

    goto :goto_3

    :cond_7
    move v0, v3

    goto :goto_2

    :cond_8
    move v0, v2

    goto/16 :goto_1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    const/4 v4, -0x1

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mStyle:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchEnable:Z

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    if-le v2, v1, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->close()V

    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTransactionTouchEvent:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTransactionTouchEvent;

    invoke-virtual {v2, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTransactionTouchEvent;->check(Landroid/view/MotionEvent;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    and-int/lit16 v2, v2, 0xff

    packed-switch v2, :pswitch_data_0

    :cond_3
    :goto_1
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->handleTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_6

    move v0, v1

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPointDown:Landroid/graphics/PointF;

    if-nez v0, :cond_4

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPointDown:Landroid/graphics/PointF;

    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPointDown:Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/PointF;->set(FF)V

    goto :goto_1

    :pswitch_1
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFirstTouch:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPointDown:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    sub-float/2addr v0, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPointDown:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    sub-float/2addr v2, v3

    mul-float/2addr v0, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPointDown:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    sub-float/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mPointDown:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    sub-float/2addr v3, v4

    mul-float/2addr v2, v3

    add-float/2addr v0, v2

    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    const-wide/high16 v4, 0x4034000000000000L    # 20.0

    cmpl-double v0, v2, v4

    if-ltz v0, :cond_3

    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->hide()V

    goto :goto_1

    :pswitch_2
    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIndex:I

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsFirstTouch:Z

    goto :goto_1

    :pswitch_3
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsClosed:Z

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onCanvasHoverEnter()V

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIndex:I

    goto :goto_1

    :cond_6
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->fitRotateAngle2BorderAngle()V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->fit()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->reset()V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->invalidate()V

    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->onWindowFocusChanged(Z)V

    return-void
.end method

.method protected relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V
    .locals 2

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v0, v1

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->left:F

    iget v0, p2, Landroid/graphics/RectF;->right:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v0, v1

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->right:F

    iget v0, p2, Landroid/graphics/RectF;->top:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v0, v1

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->pan:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    sub-float/2addr v0, v1

    iget v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    mul-float/2addr v0, v1

    iget-object v1, p3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->frameRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    goto :goto_0
.end method

.method protected resetIndex()V
    .locals 1

    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIndex:I

    return-void
.end method

.method public setContextMenu(Ljava/util/ArrayList;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iput-object p1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mItemList:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->setDirty()V

    return-void
.end method

.method public setContextMenuItemEnabled(IZ)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->setItemEnabled(IZ)V

    goto :goto_0
.end method

.method public setContextMenuVisible(Z)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mVisible:Z

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iput-boolean p1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mVisible:Z

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->show()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->hide()V

    goto :goto_0
.end method

.method public setDimEnabled(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mIsDim:Z

    return-void
.end method

.method protected setListener(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    return-void
.end method

.method public setMinResizeRect(Landroid/graphics/RectF;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMinimumResizeRect:Landroid/graphics/RectF;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMinimumResizeRect:Landroid/graphics/RectF;

    invoke-virtual {v0, p1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    :cond_0
    return-void
.end method

.method protected setObjectList(Ljava/util/ArrayList;)V
    .locals 12

    const/high16 v11, 0x40000000    # 2.0f

    const v10, 0x38d1b717    # 1.0E-4f

    const/4 v9, 0x0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v3, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v4, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    new-instance v5, Landroid/graphics/RectF;

    invoke-direct {v5, v9, v9, v9, v9}, Landroid/graphics/RectF;-><init>(FFFF)V

    new-instance v6, Landroid/graphics/RectF;

    invoke-direct {v6, v9, v9, v9, v9}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMinimumResizeRect:Landroid/graphics/RectF;

    invoke-virtual {v0, v5}, Landroid/graphics/RectF;->union(Landroid/graphics/RectF;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mMaximumResizeRect:Landroid/graphics/RectF;

    invoke-virtual {v0, v6}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    :cond_0
    return-void

    :cond_1
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    new-instance v1, Landroid/graphics/RectF;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getMinWidth()F

    move-result v2

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getMinHeight()F

    move-result v8

    invoke-direct {v1, v9, v9, v2, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v5, v1}, Landroid/graphics/RectF;->union(Landroid/graphics/RectF;)V

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getMaxWidth()F

    move-result v1

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getMaxHeight()F

    move-result v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v8

    cmpg-float v8, v8, v10

    if-gez v8, :cond_2

    int-to-float v1, v4

    mul-float/2addr v1, v11

    :cond_2
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v8

    cmpg-float v8, v8, v10

    if-gez v8, :cond_3

    int-to-float v2, v3

    mul-float/2addr v2, v11

    :cond_3
    new-instance v8, Landroid/graphics/RectF;

    invoke-direct {v8, v9, v9, v1, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v6, v8}, Landroid/graphics/RectF;->union(Landroid/graphics/RectF;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    new-instance v2, Landroid/graphics/RectF;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRect()Landroid/graphics/RectF;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public setObjectOutlineEnabled(Z)V
    .locals 0

    sput-boolean p1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectOutlineEnable:Z

    return-void
.end method

.method public setStyle(I)V
    .locals 1

    if-ltz p1, :cond_0

    const/4 v0, 0x3

    if-gt p1, v0, :cond_0

    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mStyle:I

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x7

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0
.end method

.method public setTouchEnabled(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTouchEnable:Z

    return-void
.end method

.method protected updateContextMenu()V
    .locals 5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mItemList:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mItemList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mDirtyFlag:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->close()Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mItemList:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mSelectContextMenuListener:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$ContextMenuListener;

    invoke-direct {v1, v2, p0, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;-><init>(Landroid/content/Context;Landroid/view/View;Ljava/util/ArrayList;Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$ContextMenuListener;)V

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    :cond_3
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getBoundBox(I)Landroid/graphics/RectF;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    const/16 v0, 0x19

    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->getType()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_4

    const/16 v0, 0x26

    :cond_4
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isRotatable()Z

    move-result v3

    if-eqz v3, :cond_5

    mul-int/lit8 v0, v0, 0x3

    :cond_5
    iget v3, v1, Landroid/graphics/RectF;->left:F

    float-to-int v3, v3

    iget v4, v1, Landroid/graphics/RectF;->top:F

    int-to-float v0, v0

    sub-float v0, v4, v0

    float-to-int v0, v0

    iget v4, v1, Landroid/graphics/RectF;->right:F

    float-to-int v4, v4

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    float-to-int v1, v1

    add-int/lit8 v1, v1, 0x14

    invoke-virtual {v2, v3, v0, v4, v1}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v0, :cond_0

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->calculateContextMenuPosition(Landroid/graphics/Rect;)Landroid/graphics/Rect;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v2, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    :cond_6
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    iget v1, v0, Landroid/graphics/Rect;->left:I

    iget v0, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual {v2, v1, v0}, Landroid/graphics/Rect;->offset(II)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->setRect(Landroid/graphics/Rect;)V

    goto/16 :goto_0
.end method

.method protected updateRectList()V
    .locals 5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mObjectBaseList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRect()Landroid/graphics/RectF;

    move-result-object v0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-interface {v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {p0, v2, v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mRectList:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
