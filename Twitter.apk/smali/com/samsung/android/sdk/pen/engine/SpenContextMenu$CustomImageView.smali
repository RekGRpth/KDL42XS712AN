.class Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
.super Landroid/widget/ImageView;
.source "Twttr"


# instance fields
.field private final mIsLeftEdge:Z

.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;Landroid/content/Context;Z)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    invoke-direct {p0, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-boolean p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mIsLeftEdge:Z

    return-void
.end method

.method private drawEdgeEffectsUnclipped(Landroid/graphics/Canvas;)V
    .locals 8

    const/4 v2, 0x0

    const/high16 v7, 0x3f000000    # 0.5f

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRightEdgeEffect:Landroid/widget/EdgeEffect;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/widget/EdgeEffect;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRightEdgeEffect:Landroid/widget/EdgeEffect;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/widget/EdgeEffect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mLeftEdgeEffect:Landroid/widget/EdgeEffect;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/widget/EdgeEffect;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mLeftEdgeEffect:Landroid/widget/EdgeEffect;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/widget/EdgeEffect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v3

    if-nez v3, :cond_0

    move v0, v1

    :cond_0
    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v3

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->mIsLeftEdge:Z

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1, v5, v0}, Landroid/graphics/Canvas;->translate(FF)V

    const/high16 v0, -0x3d4c0000    # -90.0f

    invoke-virtual {p1, v0, v5, v5}, Landroid/graphics/Canvas;->rotate(FFF)V

    invoke-virtual {p1, v6, v7}, Landroid/graphics/Canvas;->scale(FF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mLeftEdgeEffect:Landroid/widget/EdgeEffect;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/widget/EdgeEffect;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_1
    :goto_1
    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->restoreToCount(I)V

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->invalidate()V

    :cond_2
    return-void

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I
    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$7()I

    move-result v4

    if-ne v4, v1, :cond_5

    const/high16 v4, 0x42c40000    # 98.0f

    invoke-static {v1, v4, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    const/high16 v4, 0x40000000    # 2.0f

    add-float/2addr v0, v4

    :goto_2
    invoke-virtual {p1, v0, v5}, Landroid/graphics/Canvas;->translate(FF)V

    const/high16 v0, 0x42b40000    # 90.0f

    invoke-virtual {p1, v0, v5, v5}, Landroid/graphics/Canvas;->rotate(FFF)V

    invoke-virtual {p1, v6, v7}, Landroid/graphics/Canvas;->scale(FF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRightEdgeEffect:Landroid/widget/EdgeEffect;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/widget/EdgeEffect;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_4
    move v1, v2

    goto :goto_1

    :cond_5
    const/high16 v4, 0x42900000    # 72.0f

    invoke-static {v1, v4, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    add-float/2addr v0, v6

    goto :goto_2

    :cond_6
    move v0, v2

    goto/16 :goto_0
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->drawEdgeEffectsUnclipped(Landroid/graphics/Canvas;)V

    return-void
.end method
