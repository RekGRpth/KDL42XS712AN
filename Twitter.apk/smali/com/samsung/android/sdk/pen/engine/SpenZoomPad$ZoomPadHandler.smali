.class Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadHandler;
.super Landroid/os/Handler;
.source "Twttr"


# static fields
.field private static final MOVING_DELAY:I = 0x258

.field private static final MOVING_MESSAGE:I = 0x1


# instance fields
.field private mIsMoving:Z

.field private final mSpenZoomPad:Ljava/lang/ref/WeakReference;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;)V
    .locals 1

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadHandler;->mIsMoving:Z

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadHandler;->mSpenZoomPad:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadHandler;->mSpenZoomPad:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x6

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->setZoomPadButton(I)V
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;I)V

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->updateFrameBuffer()V
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadHandler;->mIsMoving:Z

    goto :goto_0
.end method

.method public isMovingEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadHandler;->mIsMoving:Z

    return v0
.end method

.method public setMovingEnabled(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadHandler;->mIsMoving:Z

    return-void
.end method
