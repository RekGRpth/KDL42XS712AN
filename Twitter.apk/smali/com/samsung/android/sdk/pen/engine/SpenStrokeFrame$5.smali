.class Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnCompleteCameraFrameListener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onComplete([B)V
    .locals 10

    const/high16 v9, 0x41200000    # 10.0f

    const/4 v1, 0x0

    const/4 v8, 0x0

    const/16 v7, 0x20

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mWorkBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$36(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mWorkBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$36(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$37(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;)V

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBgBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$38(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBgBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$38(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$39(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;)V

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$12(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$12(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$40(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;)V

    :cond_2
    if-nez p1, :cond_3

    const-string/jumbo v0, "SpenStrokeFrame"

    const-string/jumbo v1, "Camera onComplete arg0 is null"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V
    invoke-static {v0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    :goto_0
    return-void

    :cond_3
    array-length v0, p1

    invoke-static {p1, v8, v0, v1}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cleanResource()V
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$41(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V

    const-string/jumbo v0, "SpenStrokeFrame"

    const-string/jumbo v1, "Camera onComplete Bitmap is null"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V
    invoke-static {v0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$20(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;F)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->createRotateBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    invoke-static {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$42(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-result-object v5

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->makePath(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;
    invoke-static {v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Path;)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mFrameShapePath:Landroid/graphics/Path;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Path;

    move-result-object v3

    invoke-virtual {v3, v2, v8}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->convertRelative(Landroid/graphics/RectF;)Landroid/graphics/RectF;
    invoke-static {v4, v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v4

    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    sget-object v6, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v5, v2, v4, v6}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mFrameShapePath:Landroid/graphics/Path;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Path;

    move-result-object v6

    invoke-virtual {v6, v5, v3}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;Landroid/graphics/Path;)V

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->createSizeFitableBitmap(Landroid/graphics/Bitmap;Landroid/graphics/RectF;)Landroid/graphics/Bitmap;
    invoke-static {v5, v1, v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$43(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;Landroid/graphics/RectF;)Landroid/graphics/Bitmap;

    move-result-object v4

    if-nez v4, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cleanResource()V
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$41(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V

    const-string/jumbo v0, "SpenStrokeFrame"

    const-string/jumbo v1, "Camera onComplete Bitmap is null"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V
    invoke-static {v0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    goto/16 :goto_0

    :cond_5
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->createStrokeFrameBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Path;)Landroid/graphics/Bitmap;
    invoke-static {v5, v4, v3}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$44(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;Landroid/graphics/Path;)Landroid/graphics/Bitmap;

    move-result-object v3

    if-nez v3, :cond_6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cleanResource()V
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$41(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V
    invoke-static {v0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    goto/16 :goto_0

    :cond_6
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalImage:Lcom/samsung/android/sdk/pen/document/SpenObjectImage;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$45(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    move-result-object v4

    invoke-virtual {v4, v2, v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->setRect(Landroid/graphics/RectF;Z)V

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalImage:Lcom/samsung/android/sdk/pen/document/SpenObjectImage;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$45(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->setImage(Landroid/graphics/Bitmap;)V

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->recycle()V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-result-object v5

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->makePath(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;
    invoke-static {v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Path;)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mFrameShapePath:Landroid/graphics/Path;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Path;

    move-result-object v3

    invoke-virtual {v3, v2, v8}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->convertRelative(Landroid/graphics/RectF;)Landroid/graphics/RectF;
    invoke-static {v4, v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v4

    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    sget-object v6, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v5, v2, v4, v6}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mFrameShapePath:Landroid/graphics/Path;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Path;

    move-result-object v6

    invoke-virtual {v6, v5, v3}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;Landroid/graphics/Path;)V

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->createSizeFitableBitmap(Landroid/graphics/Bitmap;Landroid/graphics/RectF;)Landroid/graphics/Bitmap;
    invoke-static {v5, v1, v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$43(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;Landroid/graphics/RectF;)Landroid/graphics/Bitmap;

    move-result-object v4

    if-nez v4, :cond_7

    const-string/jumbo v0, "SpenStrokeFrame"

    const-string/jumbo v1, "Camera onComplete Bitmap is null"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V
    invoke-static {v0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    goto/16 :goto_0

    :cond_7
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->createStrokeFrameBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Path;)Landroid/graphics/Bitmap;
    invoke-static {v0, v4, v3}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$44(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;Landroid/graphics/Path;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_8

    const-string/jumbo v0, "SpenStrokeFrame"

    const-string/jumbo v1, "Beautify Stroke Frame ResultBitmap is null"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V
    invoke-static {v0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    goto/16 :goto_0

    :cond_8
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyImage:Lcom/samsung/android/sdk/pen/document/SpenObjectImage;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$46(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    move-result-object v1

    invoke-virtual {v1, v2, v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->setRect(Landroid/graphics/RectF;Z)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyImage:Lcom/samsung/android/sdk/pen/document/SpenObjectImage;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$46(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->setImage(Landroid/graphics/Bitmap;)V

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mObjectContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-result-object v0

    invoke-virtual {v0, v9, v9}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->setMinSize(FF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->updateListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$47(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStrokeFrameType:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mObjectContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;->onCompleted(ILcom/samsung/android/sdk/pen/document/SpenObjectContainer;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$5;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cleanResource()V
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$41(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V

    goto/16 :goto_0
.end method
