.class Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase$ResultListener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResult(Ljava/util/List;Ljava/util/List;)V
    .locals 9

    const/4 v8, 0x0

    const/16 v7, 0x8

    const/4 v6, 0x0

    const/16 v5, 0x20

    const/high16 v4, 0x41200000    # 10.0f

    if-nez p2, :cond_1

    const-string/jumbo v0, "SpenStrokeFrame"

    const-string/jumbo v1, "Beautify result is null. so this action is canceled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V
    invoke-static {v0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {p2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObjectList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStrokeFrameType:I
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-result-object v3

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->makePath(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;
    invoke-static {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Path;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-result-object v2

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->makePath(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;
    invoke-static {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;

    move-result-object v0

    :goto_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mFrameShapePath:Landroid/graphics/Path;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Path;

    move-result-object v2

    if-eqz v2, :cond_2

    if-nez v0, :cond_5

    :cond_2
    const-string/jumbo v0, "SpenStrokeFrame"

    const-string/jumbo v1, "Cancel cause failed to recognize"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V
    invoke-static {v0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    goto :goto_0

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->appendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-result-object v3

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->makePath(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;
    invoke-static {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Path;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-result-object v2

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->makePath(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;
    invoke-static {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;

    move-result-object v0

    goto :goto_2

    :cond_5
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mFrameShapePath:Landroid/graphics/Path;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Path;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v3

    invoke-virtual {v2, v3, v6}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    invoke-virtual {v0, v1, v6}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    new-instance v0, Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPageWidth:I
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$48(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPageHeight:I
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$49(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v3

    int-to-float v3, v3

    invoke-direct {v0, v8, v8, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->convertRelative(Landroid/graphics/RectF;)Landroid/graphics/RectF;
    invoke-static {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->convertAbsolute(Landroid/graphics/RectF;)Landroid/graphics/RectF;
    invoke-static {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$50(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->contains(Landroid/graphics/RectF;)Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/graphics/RectF;->contains(Landroid/graphics/RectF;)Z

    move-result v0

    if-nez v0, :cond_7

    :cond_6
    const-string/jumbo v0, "SpenStrokeFrame"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "StrokeFrame Rect is outside of Page. StrokeFrameType = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStrokeFrameType:I
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " Rect 2 = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    const/16 v1, 0x10

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    goto/16 :goto_0

    :cond_7
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    cmpg-float v0, v0, v4

    if-ltz v0, :cond_8

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    cmpg-float v0, v0, v4

    if-gez v0, :cond_9

    :cond_8
    const-string/jumbo v0, "SpenStrokeFrame"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "StrokeFrame Rect is too small. StrokeFrameType = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStrokeFrameType:I
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " Rect 1 = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    const/4 v1, 0x4

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    goto/16 :goto_0

    :cond_9
    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v0

    cmpg-float v0, v0, v4

    if-ltz v0, :cond_a

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v0

    cmpg-float v0, v0, v4

    if-gez v0, :cond_b

    :cond_a
    const-string/jumbo v0, "SpenStrokeFrame"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "StrokeFrame Rect is too small. StrokeFrameType = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStrokeFrameType:I
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " Rect 2 = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V
    invoke-static {v0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    goto/16 :goto_0

    :cond_b
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPageWidth:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$48(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPageHeight:I
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$49(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$37(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mWorkBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$36(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_c

    const-string/jumbo v0, "SpenStrokeFrame"

    const-string/jumbo v1, "workBitmap create is failed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V
    invoke-static {v0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    goto/16 :goto_0

    :cond_c
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mSpenSurfaceViewBitmap:Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$51(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mSpenSurfaceViewBitmap:Landroid/graphics/Bitmap;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$51(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$40(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$12(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_d

    const-string/jumbo v0, "SpenStrokeFrame"

    const-string/jumbo v1, "ShapeMaskBitmap create is failed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V
    invoke-static {v0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    goto/16 :goto_0

    :cond_d
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v2

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->convertRelative(Landroid/graphics/RectF;)Landroid/graphics/RectF;
    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v1

    new-instance v2, Landroid/graphics/Matrix;

    invoke-direct {v2}, Landroid/graphics/Matrix;-><init>()V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v3

    sget-object v4, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v2, v3, v1, v4}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mFrameShapePath:Landroid/graphics/Path;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Path;

    move-result-object v3

    invoke-virtual {v3, v2, v0}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;Landroid/graphics/Path;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->createShapeMaskBitmap(Landroid/graphics/Path;)V
    invoke-static {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Path;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStrokeFrameType:I
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v0

    if-nez v0, :cond_e

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskBitmap:Landroid/graphics/Bitmap;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$12(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Bitmap;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v4

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->drawPen(Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;Landroid/graphics/RectF;Landroid/graphics/RectF;)Z
    invoke-static {v0, v2, v3, v4, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$13(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    move-result v0

    :goto_3
    if-nez v0, :cond_f

    const-string/jumbo v0, "SpenStrokeFrame"

    const-string/jumbo v1, "drawPen is failed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V
    invoke-static {v0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    goto/16 :goto_0

    :cond_e
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskBitmap:Landroid/graphics/Bitmap;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$12(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Bitmap;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v4

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->drawPen(Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;Landroid/graphics/RectF;Landroid/graphics/RectF;)Z
    invoke-static {v0, v2, v3, v4, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$13(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    move-result v0

    goto :goto_3

    :cond_f
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->setRect(Landroid/graphics/RectF;)V
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$15(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/RectF;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->startCamera()Z
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$52(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "SpenStrokeFrame"

    const-string/jumbo v1, "startCamera is failed"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$6;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V
    invoke-static {v0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    goto/16 :goto_0
.end method
