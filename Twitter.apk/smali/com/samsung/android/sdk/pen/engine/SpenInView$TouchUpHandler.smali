.class Lcom/samsung/android/sdk/pen/engine/SpenInView$TouchUpHandler;
.super Landroid/os/Handler;
.source "Twttr"


# instance fields
.field private final mSpenView:Ljava/lang/ref/WeakReference;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V
    .locals 1

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$TouchUpHandler;->mSpenView:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$TouchUpHandler;->mSpenView:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$25(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    move-result-object v1

    if-eqz v1, :cond_0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$25(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->getState()Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    move-result-object v1

    sget-object v2, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;->FLING_STATE:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$State;

    if-eq v1, v2, :cond_0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$41(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    move-result-object v1

    if-eqz v1, :cond_0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$41(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->isWorking()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateScreenFrameBuffer()V

    goto :goto_0
.end method
