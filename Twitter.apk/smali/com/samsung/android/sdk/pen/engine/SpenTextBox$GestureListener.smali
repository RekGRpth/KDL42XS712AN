.class Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/GestureDetector$OnGestureListener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    return-void
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "onFling velocityX = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", velocityY"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    return v2
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 6

    const/4 v4, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsViewMode:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$27(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsTyping:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$28(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$29(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I

    move-result v0

    if-lez v0, :cond_8

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    new-array v2, v0, [F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    aput v0, v2, v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    aput v0, v2, v4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$30(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/graphics/Matrix;

    move-result-object v0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$31(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/graphics/Matrix;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$31(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/graphics/Matrix;->mapPoints([F)V

    aget v0, v2, v4

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$32(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I

    move-result v3

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getPan(I)F
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$33(I)F

    move-result v3

    add-float/2addr v0, v3

    aput v0, v2, v4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    aget v3, v2, v4

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineForVertical(F)I
    invoke-static {v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;F)I

    move-result v3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    aget v4, v2, v5

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getOffsetForHorizontal(IF)I
    invoke-static {v0, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$34(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;IF)I

    move-result v0

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$35(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)[Landroid/graphics/RectF;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$36(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)[I

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$36(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)[I

    move-result-object v4

    aget v3, v4, v3

    if-ne v0, v3, :cond_3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$35(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)[Landroid/graphics/RectF;

    move-result-object v3

    aget-object v3, v3, v0

    iget v3, v3, Landroid/graphics/RectF;->right:F

    aget v2, v2, v5

    cmpg-float v2, v3, v2

    if-gez v2, :cond_3

    add-int/lit8 v0, v0, 0x1

    :cond_3
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v0, v2, :cond_5

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    :goto_1
    const/16 v2, 0x20

    if-eq v1, v2, :cond_4

    const/16 v2, 0x9

    if-eq v1, v2, :cond_4

    const/16 v2, 0xa

    if-eq v1, v2, :cond_4

    const/16 v2, 0xd

    if-ne v1, v2, :cond_6

    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v1, v0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v1, v0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onSelectionChanged(II)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showCursorHandle()V
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$13(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    goto/16 :goto_0

    :cond_5
    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    goto :goto_1

    :cond_6
    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->findWord(ILcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;)Z
    invoke-static {v2, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$37(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;ILcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;)Z

    move-result v2

    if-eqz v2, :cond_7

    iget v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->startIndex:I

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->endIndex:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v2, v0, v1, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto/16 :goto_0

    :cond_7
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v1, v0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showCursorHandle()V
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$13(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getTextLength()I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$38(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v1, v0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onSelectionChanged(II)V

    goto/16 :goto_0

    :cond_8
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showCursorHandle()V
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$13(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    move-result-object v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, v5, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onSelectionChanged(II)V

    :cond_9
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, v5, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto/16 :goto_0
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 9

    const/4 v0, 0x0

    const/4 v8, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onScroll distanceX = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", distanceY"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v8

    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCoordinateInfo()Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$39(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    move-result-object v1

    iget v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->zoomRatio:F

    div-float v1, p4, v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$32(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I

    move-result v2

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getHeight(I)I
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$40(I)I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v3

    if-eqz v3, :cond_0

    float-to-double v4, v2

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v6

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    cmpl-double v4, v4, v6

    if-lez v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$32(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I

    move-result v4

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getPan(I)F
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$33(I)F

    move-result v4

    cmpl-float v5, v1, v0

    if-lez v5, :cond_5

    add-float/2addr v1, v4

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v4

    sub-float v4, v2, v4

    cmpg-float v4, v1, v4

    if-gez v4, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$32(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I

    move-result v0

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_setPan(IF)V
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$41(IF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setVereticalPan(F)V

    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$32(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I

    move-result v0

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_update(I)Z
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$42(I)Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$43(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$43(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$43(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1, v8, v8}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v1

    invoke-virtual {v0, v8, v8, v1}, Landroid/graphics/Bitmap;->setPixel(III)V

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showScrollBar()V
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$44(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->invalidate()V

    goto/16 :goto_0

    :cond_3
    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v1

    cmpg-float v1, v2, v1

    if-gez v1, :cond_4

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$32(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I

    move-result v1

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_setPan(IF)V
    invoke-static {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$41(IF)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setVereticalPan(F)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$32(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I

    move-result v0

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v1

    sub-float v1, v2, v1

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_setPan(IF)V
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$41(IF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v0

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v1

    sub-float v1, v2, v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setVereticalPan(F)V

    goto :goto_1

    :cond_5
    add-float/2addr v1, v4

    cmpg-float v2, v1, v0

    if-gez v2, :cond_6

    :goto_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$32(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I

    move-result v1

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_setPan(IF)V
    invoke-static {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$41(IF)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setVereticalPan(F)V

    goto/16 :goto_1

    :cond_6
    move v0, v1

    goto :goto_2
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0

    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mBitmap:Landroid/graphics/Bitmap;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$43(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsViewMode:Z
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$27(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$29(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I

    move-result v2

    if-lez v2, :cond_4

    const/4 v2, 0x2

    new-array v2, v2, [F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    aput v3, v2, v1

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$30(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/graphics/Matrix;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$31(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/graphics/Matrix;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$31(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/graphics/Matrix;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/graphics/Matrix;->mapPoints([F)V

    aget v3, v2, v1

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$32(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I

    move-result v4

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getPan(I)F
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$33(I)F

    move-result v4

    add-float/2addr v3, v4

    aput v3, v2, v1

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onCueTopButtonDown(Landroid/view/MotionEvent;)Z
    invoke-static {v3, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$45(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Landroid/view/MotionEvent;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onCueBottomButtonDown(Landroid/view/MotionEvent;)Z
    invoke-static {v3, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$46(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Landroid/view/MotionEvent;)Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    move-result-object v3

    if-eqz v3, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;->onMoreButtonDown(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    aget v4, v2, v1

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineForVertical(F)I
    invoke-static {v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;F)I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    aget v2, v2, v0

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorIndex(IF)I
    invoke-static {v4, v3, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;IF)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v3, v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showCursorHandle()V
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$13(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->invalidate()V

    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showSoftInput()V

    move v0, v1

    goto/16 :goto_0
.end method
