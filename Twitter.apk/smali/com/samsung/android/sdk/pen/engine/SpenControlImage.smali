.class public Lcom/samsung/android/sdk/pen/engine/SpenControlImage;
.super Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
.source "Twttr"


# static fields
.field private static CONFIG_DRAW_MOVING_OBJECT:Z = false

.field private static final DEFAULT_MOVE_COLOR:I = 0x55ffffff


# instance fields
.field mMovingImage:Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->CONFIG_DRAW_MOVING_OBJECT:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->initialize()V

    return-void
.end method

.method private initialize()V
    .locals 2

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->mMovingImage:Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;

    return-void
.end method


# virtual methods
.method public getObject()Lcom/samsung/android/sdk/pen/document/SpenObjectImage;
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->getObjectList()Ljava/util/ArrayList;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->getObjectList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->getObjectList()Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    goto :goto_0
.end method

.method protected onDrawObject(Landroid/graphics/Canvas;Landroid/graphics/Rect;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 2

    sget-boolean v0, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->CONFIG_DRAW_MOVING_OBJECT:Z

    if-eqz v0, :cond_2

    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->mMovingImage:Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->mMovingImage:Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, p2}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->mMovingImage:Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->mMovingImage:Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    const/16 v1, 0x7f

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;->setAlpha(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v0}, Landroid/graphics/Paint;->reset()V

    const v1, 0x55ffffff    # 3.518437E13f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    invoke-virtual {p1, p2, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0

    :cond_2
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onDrawObject(Landroid/graphics/Canvas;Landroid/graphics/Rect;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    goto :goto_0
.end method

.method protected onFlip(ILcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 9

    const/4 v8, 0x1

    const/high16 v7, 0x3f800000    # 1.0f

    const/high16 v6, -0x40800000    # -1.0f

    const/4 v1, 0x0

    sget-boolean v0, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->CONFIG_DRAW_MOVING_OBJECT:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->mMovingImage:Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->mMovingImage:Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v0, v2, v8}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    and-int/lit8 v2, p1, 0x2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    invoke-virtual {v5, v6, v7}, Landroid/graphics/Matrix;->preScale(FF)Z

    :cond_1
    and-int/lit8 v2, p1, 0x1

    if-ne v2, v8, :cond_2

    invoke-virtual {v5, v7, v6}, Landroid/graphics/Matrix;->preScale(FF)Z

    :cond_2
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    move v2, v1

    move v6, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    const/16 v1, 0xa0

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->setDensity(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->mMovingImage:Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;

    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v2, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    :cond_3
    invoke-super {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onFlip(ILcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    goto :goto_0
.end method

.method protected onObjectChanged()V
    .locals 0

    invoke-super {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onObjectChanged()V

    return-void
.end method

.method public setObject(Lcom/samsung/android/sdk/pen/document/SpenObjectImage;)V
    .locals 4

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->getRotation()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->mRotateAngle:F

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->setObjectList(Ljava/util/ArrayList;)V

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->getSorInfo()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->mMovingImage:Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->getImage()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->mMovingImage:Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlImage$CMovingImage;->mDrawable:Landroid/graphics/drawable/BitmapDrawable;

    goto :goto_0
.end method
