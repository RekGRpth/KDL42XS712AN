.class public Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field mDiffY:F

.field mHeight:F

.field mMinimumHeight:F

.field private final mNativeTextView:I

.field private mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->native_init()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mNativeTextView:I

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mNativeTextView:I

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->native_construct(ILandroid/content/Context;)Z

    return-void
.end method

.method private static native native_command(IILjava/util/ArrayList;I)Ljava/util/ArrayList;
.end method

.method private static native native_construct(ILandroid/content/Context;)Z
.end method

.method private static native native_finalize(I)V
.end method

.method private static native native_getHeight(I)I
.end method

.method private static native native_getLineCount(I)I
.end method

.method private static native native_getLineEndIndex(II)I
.end method

.method private static native native_getLinePosition(IILandroid/graphics/PointF;)Z
.end method

.method private static native native_getLineStartIndex(II)I
.end method

.method private static native native_getTextRect(IILandroid/graphics/RectF;)Z
.end method

.method private static native native_init()I
.end method

.method private static native native_measure(II)Z
.end method

.method private static native native_setObjectText(ILcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)Z
.end method


# virtual methods
.method protected finalize()V
    .locals 1

    :try_start_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mNativeTextView:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->native_finalize(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    return-void

    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method public getHeight()I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mHeight:F

    float-to-int v0, v0

    return v0
.end method

.method public getLineCount()I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mNativeTextView:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->native_getLineCount(I)I

    move-result v0

    return v0
.end method

.method public getLineEndIndex(I)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mNativeTextView:I

    invoke-static {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->native_getLineEndIndex(II)I

    move-result v0

    return v0
.end method

.method public getLinePosition(I)Landroid/graphics/PointF;
    .locals 3

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mNativeTextView:I

    invoke-static {v1, p1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->native_getLinePosition(IILandroid/graphics/PointF;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const/4 v1, 0x0

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mDiffY:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->offset(FF)V

    goto :goto_0
.end method

.method public getLineStartIndex(I)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mNativeTextView:I

    invoke-static {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->native_getLineStartIndex(II)I

    move-result v0

    return v0
.end method

.method public getMinHeight()I
    .locals 2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mMinimumHeight:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method

.method public getTextRect(I)Landroid/graphics/RectF;
    .locals 3

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mNativeTextView:I

    invoke-static {v1, p1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->native_getTextRect(IILandroid/graphics/RectF;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const/4 v1, 0x0

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mDiffY:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/RectF;->offset(FF)V

    goto :goto_0
.end method

.method public setObjectText(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)Z
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x1

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mNativeTextView:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-static {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->native_setObjectText(ILcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mNativeTextView:I

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v4

    float-to-int v4, v4

    invoke-static {v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->native_measure(II)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getGravity()I

    move-result v0

    const/4 v3, 0x0

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mDiffY:F

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mHeight:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mNativeTextView:I

    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->native_getHeight(I)I

    move-result v2

    int-to-float v2, v2

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mMinimumHeight:F

    if-eq v0, v1, :cond_2

    const/4 v2, 0x2

    if-ne v0, v2, :cond_3

    :cond_2
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mHeight:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mMinimumHeight:F

    sub-float/2addr v2, v3

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mDiffY:F

    if-ne v0, v1, :cond_3

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mDiffY:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v0, v2

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextMeasure;->mDiffY:F

    :cond_3
    move v0, v1

    goto :goto_0
.end method
