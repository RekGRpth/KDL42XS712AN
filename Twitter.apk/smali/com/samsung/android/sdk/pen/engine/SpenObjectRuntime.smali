.class public Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static mGlobalObjectRuntimeStart:Z


# instance fields
.field private final mObjectRuntimeObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface;

.field private mThisObjectRuntimeStart:Z

.field private mUpdateListener:Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$UpdateListener;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "E_INVALID_ARG : parameter \'objectRuntimeObject\' is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mObjectRuntimeObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface;

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;)Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$UpdateListener;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mUpdateListener:Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$UpdateListener;

    return-object v0
.end method

.method static synthetic access$1(Z)V
    .locals 0

    sput-boolean p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mGlobalObjectRuntimeStart:Z

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mThisObjectRuntimeStart:Z

    return-void
.end method


# virtual methods
.method getObjectRuntimeObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mObjectRuntimeObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface;

    return-object v0
.end method

.method public getType()I
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mObjectRuntimeObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface;->getType()I

    move-result v0

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mObjectRuntimeObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface;->onTouchEvent(Landroid/view/MotionEvent;)V

    return-void
.end method

.method public setListener(Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$UpdateListener;)Z
    .locals 2

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mUpdateListener:Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$UpdateListener;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mUpdateListener:Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$UpdateListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mObjectRuntimeObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface;

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime$1;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;)V

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface;->setListener(Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface$UpdateListener;)Z

    :goto_0
    const/4 v0, 0x1

    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mObjectRuntimeObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface;->setListener(Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface$UpdateListener;)Z

    goto :goto_0
.end method

.method public setRect(Landroid/graphics/RectF;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mObjectRuntimeObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface;->setRect(Landroid/graphics/RectF;)V

    return-void
.end method

.method public start(Ljava/lang/Object;Landroid/graphics/RectF;Landroid/graphics/PointF;FLandroid/graphics/PointF;Landroid/view/ViewGroup;)V
    .locals 7

    const/4 v1, 0x1

    sget-boolean v0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mGlobalObjectRuntimeStart:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "SpenObjectRuntime"

    const-string/jumbo v1, "SpenObjectRuntime was already started"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    sput-boolean v1, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mGlobalObjectRuntimeStart:Z

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mThisObjectRuntimeStart:Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mObjectRuntimeObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface;->start(Ljava/lang/Object;Landroid/graphics/RectF;Landroid/graphics/PointF;FLandroid/graphics/PointF;Landroid/view/ViewGroup;)V

    goto :goto_0
.end method

.method public stop(Z)V
    .locals 2

    const/4 v1, 0x0

    sget-boolean v0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mGlobalObjectRuntimeStart:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mThisObjectRuntimeStart:Z

    if-eqz v0, :cond_0

    sput-boolean v1, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mGlobalObjectRuntimeStart:Z

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mThisObjectRuntimeStart:Z

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenObjectRuntime;->mObjectRuntimeObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface;->stop(Z)V

    return-void
.end method
