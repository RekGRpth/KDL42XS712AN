.class Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureDoubleTapListener;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/GestureDetector$OnDoubleTapListener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureDoubleTapListener;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureDoubleTapListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)V

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaX:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)F

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaY:F
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->access$12(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->setZoom(FFF)V

    const/4 v0, 0x0

    return v0
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
