.class public Lcom/samsung/android/sdk/pen/engine/SpenMultiView;
.super Landroid/view/View;
.source "Twttr"

# interfaces
.implements Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;


# static fields
.field public static final PAGE_TRANSITION_EFFECT_LEFT:I = 0x0

.field public static final PAGE_TRANSITION_EFFECT_RIGHT:I = 0x1

.field public static final PAGE_TRANSITION_EFFECT_TYPE_SHADOW:I = 0x0

.field public static final PAGE_TRANSITION_EFFECT_TYPE_SLIDE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "SpenMultiView"


# instance fields
.field private isSkipTouch:Z

.field private localUserId:I

.field private mAntiAliasPaint:Landroid/graphics/Paint;

.field private mBlackPaint:Landroid/graphics/Paint;

.field private mCanvasHeight:I

.field private mCanvasWidth:I

.field private mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

.field private mContext:Landroid/content/Context;

.field private mDebugPaint:Landroid/graphics/Paint;

.field private mDeltaX:F

.field private mDeltaY:F

.field private mEraserChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

.field private mFBCanvas:Landroid/graphics/Canvas;

.field private mFlickListener:Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

.field private mFrameBuffer:Landroid/graphics/Bitmap;

.field private mFrameHeight:I

.field private mFrameStartX:I

.field private mFrameStartY:I

.field private mFrameWidth:I

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

.field private mMaxDeltaX:F

.field private mMaxDeltaY:F

.field private mNativeMulti:I

.field private mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

.field private mPageEffectListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

.field private mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

.field private mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

.field private mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

.field private mRatio:F

.field private mRtoCvsItstFrmHeight:I

.field private mRtoCvsItstFrmWidth:I

.field private mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

.field private mThreadId:J

.field private mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

.field private mTouchProcessingTime:J

.field private mZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    const-wide/16 v3, 0x0

    const/4 v2, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameHeight:I

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFBCanvas:Landroid/graphics/Canvas;

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->isSkipTouch:Z

    iput-wide v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mTouchProcessingTime:J

    iput-wide v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mThreadId:J

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaX:F

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaY:F

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaX:F

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaY:F

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDebugPaint:Landroid/graphics/Paint;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mAntiAliasPaint:Landroid/graphics/Paint;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mGestureDetector:Landroid/view/GestureDetector;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFlickListener:Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mEraserChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_init()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->construct()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    const-wide/16 v3, 0x0

    const/4 v2, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameHeight:I

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFBCanvas:Landroid/graphics/Canvas;

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->isSkipTouch:Z

    iput-wide v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mTouchProcessingTime:J

    iput-wide v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mThreadId:J

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaX:F

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaY:F

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaX:F

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaY:F

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDebugPaint:Landroid/graphics/Paint;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mAntiAliasPaint:Landroid/graphics/Paint;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mGestureDetector:Landroid/view/GestureDetector;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFlickListener:Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mEraserChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_init()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->construct()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    const-wide/16 v3, 0x0

    const/4 v2, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameHeight:I

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFBCanvas:Landroid/graphics/Canvas;

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->isSkipTouch:Z

    iput-wide v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mTouchProcessingTime:J

    iput-wide v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mThreadId:J

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaX:F

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaY:F

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaX:F

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaY:F

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDebugPaint:Landroid/graphics/Paint;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mAntiAliasPaint:Landroid/graphics/Paint;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mGestureDetector:Landroid/view/GestureDetector;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFlickListener:Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mEraserChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_init()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->construct()V

    return-void
.end method

.method private absoluteCoordinate(Landroid/graphics/Rect;FFFF)V
    .locals 2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    div-float v0, p2, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaX:F

    add-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->left:I

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    div-float v0, p4, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaX:F

    add-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->right:I

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    div-float v0, p3, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaY:F

    add-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->top:I

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    div-float v0, p5, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaY:F

    add-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    return-void
.end method

.method private absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/Rect;)V
    .locals 2

    iget v0, p2, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    div-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaX:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->left:F

    iget v0, p2, Landroid/graphics/Rect;->right:I

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    div-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaX:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->right:F

    iget v0, p2, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    div-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaY:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    iget v0, p2, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    div-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaY:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Landroid/graphics/Canvas;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->updateCanvas(Landroid/graphics/Canvas;Z)V

    return-void
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Landroid/graphics/Canvas;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->updateCanvas2(Landroid/graphics/Canvas;)V

    return-void
.end method

.method static synthetic access$10(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)F
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaY:F

    return v0
.end method

.method static synthetic access$11(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I

    return v0
.end method

.method static synthetic access$12(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    return v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    return v0
.end method

.method static synthetic access$4(IFFZ)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setPan(IFFZ)V

    return-void
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    return-object v0
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFlickListener:Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

    return-object v0
.end method

.method static synthetic access$7(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Landroid/graphics/RectF;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->onUpdateCanvas(Landroid/graphics/RectF;Z)V

    return-void
.end method

.method static synthetic access$8(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    return-object v0
.end method

.method static synthetic access$9(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)F
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaX:F

    return v0
.end method

.method private construct()V
    .locals 5

    const/16 v4, 0x8

    const/4 v3, 0x0

    const-string/jumbo v0, "SpenMultiView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "nativeMulti = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_0

    const-string/jumbo v0, " : nativeMulti must not be null"

    invoke-static {v4, v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    const-string/jumbo v0, " : context must not be null"

    invoke-static {v4, v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0

    :cond_1
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    invoke-static {v1, v2, p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_construct(ILandroid/content/Context;Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Landroid/graphics/RectF;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    :cond_2
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    const v1, -0xf2c5b1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDebugPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDebugPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDebugPaint:Landroid/graphics/Paint;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mAntiAliasPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mAntiAliasPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    new-instance v0, Landroid/view/GestureDetector;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureListener;

    invoke-direct {v2, p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureListener;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mGestureDetector:Landroid/view/GestureDetector;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mGestureDetector:Landroid/view/GestureDetector;

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureDoubleTapListener;

    invoke-direct {v1, p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureDoubleTapListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureDoubleTapListener;)V

    invoke-virtual {v0, v1}, Landroid/view/GestureDetector;->setOnDoubleTapListener(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v0, v0

    invoke-direct {v1, v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;-><init>(Landroid/content/Context;F)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnSmartScaleGestureDetectorListener;

    invoke-direct {v1, p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnSmartScaleGestureDetectorListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnSmartScaleGestureDetectorListener;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->setListener(Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;)V

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnPageEffectListener;

    invoke-direct {v1, p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnPageEffectListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnPageEffectListener;)V

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->setPaint(Landroid/graphics/Paint;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getId()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mThreadId:J

    goto/16 :goto_0
.end method

.method private createBitmap(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V
    .locals 4

    const/4 v3, 0x6

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getWidth()I

    move-result v2

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getHeight()I

    move-result v2

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    if-nez v2, :cond_2

    const-string/jumbo v0, "The width of pageDoc is 0"

    invoke-static {v3, v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0

    :cond_2
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    if-nez v2, :cond_3

    const-string/jumbo v0, "The height of pageDoc is 0"

    invoke-static {v3, v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0

    :cond_3
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    if-ne v2, v0, :cond_4

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    if-eq v0, v1, :cond_0

    :cond_4
    const-string/jumbo v0, "SpenMultiView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "createBitmap Width="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " Height="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_5
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFBCanvas:Landroid/graphics/Canvas;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFBCanvas:Landroid/graphics/Canvas;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setBitmap(ILandroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v0, 0x2

    const-string/jumbo v1, "Failed to create bitmap of frame buffer"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto/16 :goto_0
.end method

.method private getVariableForOnUpdateCanvas()V
    .locals 5

    const/high16 v4, 0x40000000    # 2.0f

    const/4 v3, 0x0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaX:F

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaX:F

    cmpg-float v0, v0, v3

    if-gez v0, :cond_0

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaX:F

    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameHeight:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaY:F

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaY:F

    cmpg-float v0, v0, v3

    if-gez v0, :cond_1

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaY:F

    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I

    if-ge v0, v2, :cond_2

    :goto_0
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmWidth:I

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameHeight:I

    if-ge v1, v0, :cond_3

    move v0, v1

    :goto_1
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmHeight:I

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmWidth:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    div-float/2addr v0, v4

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartX:I

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameHeight:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmHeight:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    div-float/2addr v0, v4

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartY:I

    return-void

    :cond_2
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameHeight:I

    goto :goto_1
.end method

.method private static native native_addUser(II)Z
.end method

.method private static native native_command(IILjava/util/ArrayList;I)Ljava/util/ArrayList;
.end method

.method private static native native_construct(ILandroid/content/Context;Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Landroid/graphics/RectF;)Z
.end method

.method private static native native_enablePenCurve(IIZ)V
.end method

.method private static native native_enableZoom(IZ)V
.end method

.method private static native native_finalize(I)V
.end method

.method private static native native_getAdvancedSetting(II)Ljava/lang/String;
.end method

.method private static native native_getEraserSize(II)F
.end method

.method private static native native_getLocalUserId(I)I
.end method

.method private static native native_getMaxZoomRatio(I)F
.end method

.method private static native native_getMinZoomRatio(I)F
.end method

.method private static native native_getPan(ILandroid/graphics/PointF;)V
.end method

.method private static native native_getPenColor(II)I
.end method

.method private static native native_getPenSize(II)F
.end method

.method private static native native_getPenStyle(II)Ljava/lang/String;
.end method

.method private static native native_getToolTypeAction(III)I
.end method

.method private static native native_getZoomRatio(I)F
.end method

.method private static native native_init()I
.end method

.method private static native native_isPenCurve(II)Z
.end method

.method private static native native_isZoomable(I)Z
.end method

.method private static native native_onHover(IILandroid/view/MotionEvent;I)Z
.end method

.method private static native native_onTouch(IILandroid/view/MotionEvent;I)Z
.end method

.method private static native native_removeUser(II)Z
.end method

.method private static native native_setAdvancedSetting(IILjava/lang/String;)V
.end method

.method private static native native_setBitmap(ILandroid/graphics/Bitmap;)V
.end method

.method private static native native_setEraserSize(IIF)Z
.end method

.method private static native native_setLocalUserId(II)Z
.end method

.method private static native native_setMaxZoomRatio(IF)Z
.end method

.method private static native native_setMinZoomRatio(IF)Z
.end method

.method private static native native_setPageDoc(ILcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z
.end method

.method private static native native_setPan(IFFZ)V
.end method

.method private static native native_setPenColor(III)V
.end method

.method private static native native_setPenSize(IIF)V
.end method

.method private static native native_setPenStyle(IILjava/lang/String;)Z
.end method

.method private static native native_setScreenSize(III)V
.end method

.method private static native native_setToolTypeAction(IIII)Z
.end method

.method private static native native_setZoom(IFFF)V
.end method

.method private static native native_update(I)Z
.end method

.method private static native native_updateHistory(I)Z
.end method

.method private onColorPickerChanged(III)V
    .locals 3

    const-string/jumbo v0, "SpenMultiView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onColorPickerChanged color"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    invoke-interface {v0, p3, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;->onChanged(III)V

    :cond_0
    return-void
.end method

.method private onUpdateCanvas(Landroid/graphics/RectF;Z)V
    .locals 4

    iget-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mThreadId:J

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->invalidate()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->postInvalidate()V

    goto :goto_0
.end method

.method private onZoom(FFF)V
    .locals 5

    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaX:F

    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaY:F

    iput p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getVariableForOnUpdateCanvas()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->onZoom(FFF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mMaxDeltaY:F

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->setLimitHeight(FF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmHeight:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartX:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartY:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->setDrawInformation(IIII)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;->onZoom(FFF)V

    :cond_0
    return-void
.end method

.method private printRect(Ljava/lang/String;Landroid/graphics/Rect;)V
    .locals 3

    const-string/jumbo v0, "SpenMultiView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/Rect;->left:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/Rect;->top:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ") ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/Rect;->right:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ") w = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " h = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private printRect(Ljava/lang/String;Landroid/graphics/RectF;)V
    .locals 3

    const-string/jumbo v0, "SpenMultiView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/RectF;->left:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/RectF;->top:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ") ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/RectF;->right:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ") w = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " h = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/Rect;)V
    .locals 2

    iget v0, p2, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaX:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->left:F

    iget v0, p2, Landroid/graphics/Rect;->right:I

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaX:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->right:F

    iget v0, p2, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaY:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    iget v0, p2, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaY:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    return-void
.end method

.method private relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 2

    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaX:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->left:F

    iget v0, p2, Landroid/graphics/RectF;->right:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaX:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->right:F

    iget v0, p2, Landroid/graphics/RectF;->top:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaY:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDeltaY:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    return-void
.end method

.method private updateCanvas(Landroid/graphics/Canvas;Z)V
    .locals 11

    const/4 v10, 0x0

    const/4 v1, 0x0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartX:I

    int-to-float v0, v0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartX:I

    int-to-float v3, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameHeight:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartX:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmWidth:I

    add-int/2addr v0, v2

    int-to-float v3, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I

    int-to-float v5, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameHeight:I

    int-to-float v6, v0

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    move-object v2, p1

    move v4, v1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartY:I

    int-to-float v0, v0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I

    int-to-float v3, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartY:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartY:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmHeight:I

    add-int/2addr v0, v2

    int-to-float v2, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I

    int-to-float v3, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameHeight:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_1
    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v10, v10}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v2

    invoke-virtual {v0, v10, v10, v2}, Landroid/graphics/Bitmap;->setPixel(III)V

    :cond_2
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmWidth:I

    int-to-float v6, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmHeight:I

    int-to-float v7, v0

    move-object v2, p0

    move v4, v1

    move v5, v1

    invoke-direct/range {v2 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->absoluteCoordinate(Landroid/graphics/Rect;FFFF)V

    new-instance v0, Landroid/graphics/Rect;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmHeight:I

    invoke-direct {v0, v10, v10, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartX:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartY:I

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->offset(II)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v3, v0, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :goto_0
    const-string/jumbo v0, "SpenMultiView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Performance updateCanvas end "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v8

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    goto :goto_0
.end method

.method private updateCanvas2(Landroid/graphics/Canvas;)V
    .locals 7

    const/4 v6, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v0, :cond_0

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmWidth:I

    int-to-float v4, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmHeight:I

    int-to-float v5, v0

    move-object v0, p0

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->absoluteCoordinate(Landroid/graphics/Rect;FFFF)V

    new-instance v0, Landroid/graphics/Rect;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmWidth:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmHeight:I

    invoke-direct {v0, v6, v6, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v1, v0, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    goto :goto_0
.end method


# virtual methods
.method public addUser(I)Z
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    invoke-static {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_addUser(II)Z

    move-result v0

    goto :goto_0
.end method

.method public captureCurrentView(Z)Landroid/graphics/Bitmap;
    .locals 4

    const/4 v0, 0x0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    if-nez p1, :cond_2

    :try_start_0
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mRtoCvsItstFrmHeight:I

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    if-nez p1, :cond_1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartX:I

    neg-int v2, v2

    int-to-float v2, v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartY:I

    neg-int v3, v3

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    :cond_1
    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->updateCanvas(Landroid/graphics/Canvas;Z)V

    goto :goto_0

    :cond_2
    :try_start_1
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameHeight:I

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_1

    :catch_0
    move-exception v1

    const-string/jumbo v1, "SpenMultiView"

    const-string/jumbo v2, "Failed to create bitmap"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x2

    const-string/jumbo v2, " : fail createBitmap."

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_1
.end method

.method public capturePage(F)Landroid/graphics/Bitmap;
    .locals 4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasWidth:I

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mCanvasHeight:I

    int-to-float v2, v2

    mul-float/2addr v2, p1

    float-to-int v2, v2

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public close()V
    .locals 2

    const/4 v1, 0x0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_finalize(I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    :cond_0
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mContext:Landroid/content/Context;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameBuffer:Landroid/graphics/Bitmap;

    :cond_1
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFBCanvas:Landroid/graphics/Canvas;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mDebugPaint:Landroid/graphics/Paint;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mAntiAliasPaint:Landroid/graphics/Paint;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mGestureDetector:Landroid/view/GestureDetector;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->close()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->close()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    :cond_3
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFlickListener:Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mEraserChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

    return-void
.end method

.method public closeControl()V
    .locals 0

    return-void
.end method

.method public getBlankColor()I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    goto :goto_0
.end method

.method public getCanvasHeight()I
    .locals 2

    const/4 v0, 0x0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getHeight()I

    move-result v0

    goto :goto_0
.end method

.method public getCanvasWidth()I
    .locals 2

    const/4 v0, 0x0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getWidth()I

    move-result v0

    goto :goto_0
.end method

.method public getEraserSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;
    .locals 3

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;-><init>()V

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getEraserSize(II)F

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    iget v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-nez v1, :cond_0

    const/4 v1, 0x1

    const-string/jumbo v2, "not set LocalUserId"

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public getEraserSettingInfo(I)Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;
    .locals 2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;-><init>()V

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    invoke-static {v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getEraserSize(II)F

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    goto :goto_0
.end method

.method public getLocalUserId()I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getLocalUserId(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    goto :goto_0
.end method

.method public getMaxZoomRatio()F
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getMaxZoomRatio(I)F

    move-result v0

    goto :goto_0
.end method

.method public getMinZoomRatio()F
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getMinZoomRatio(I)F

    move-result v0

    goto :goto_0
.end method

.method public getPan()Landroid/graphics/PointF;
    .locals 2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    invoke-static {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getPan(ILandroid/graphics/PointF;)V

    goto :goto_0
.end method

.method public getPenSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    .locals 3

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;-><init>()V

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getPenStyle(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    if-nez v1, :cond_2

    const/4 v1, 0x1

    const-string/jumbo v2, "not set LocalUserId"

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    :cond_2
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getPenSize(II)F

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getPenColor(II)I

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_isPenCurve(II)Z

    move-result v1

    iput-boolean v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getAdvancedSetting(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    goto :goto_0
.end method

.method public getPenSettingInfo(I)Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    .locals 2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;-><init>()V

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    invoke-static {v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getPenStyle(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    invoke-static {v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getPenSize(II)F

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    invoke-static {v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getPenColor(II)I

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    invoke-static {v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_isPenCurve(II)Z

    move-result v1

    iput-boolean v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    invoke-static {v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getAdvancedSetting(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    goto :goto_0
.end method

.method public getRemoverSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getSelectionSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getTextSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getToolTypeAction(I)I
    .locals 3

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    invoke-static {v0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getToolTypeAction(III)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v1, 0x1

    const-string/jumbo v2, "not set LocalUserId"

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public getToolTypeAction(II)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    invoke-static {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getToolTypeAction(III)I

    move-result v0

    goto :goto_0
.end method

.method public getZoomRatio()F
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_getZoomRatio(I)F

    move-result v0

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->isWorking()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->drawAnimation(Landroid/graphics/Canvas;)V

    :cond_1
    :goto_1
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->updateCanvas(Landroid/graphics/Canvas;Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->draw(Landroid/graphics/Canvas;)V

    goto :goto_1
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 13

    const/4 v1, 0x1

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    const/16 v0, 0x9

    if-ne v2, v0, :cond_2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    sub-long/2addr v3, v5

    const-wide/16 v5, 0x64

    iget-wide v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mTouchProcessingTime:J

    add-long/2addr v5, v7

    cmp-long v0, v3, v5

    if-lez v0, :cond_3

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->isSkipTouch:Z

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->isSkipTouch:Z

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v7

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v9

    const-string/jumbo v0, "SpenMultiView"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string/jumbo v12, "skiptouch hover action = "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v11, " eventTime = "

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v5, " downTime = "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v5, " systemTime = "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v5, " diffTime = "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->isSkipTouch:Z

    if-eqz v0, :cond_4

    const-string/jumbo v0, "SpenMultiView"

    const-string/jumbo v2, "skiptouch hover"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    invoke-interface {v0, p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;->onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameWidth:I

    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameHeight:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->setScreenResolution(II)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    invoke-static {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setScreenSize(III)V

    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    goto :goto_0
.end method

.method public onTouchEvent(ILandroid/view/MotionEvent;)Z
    .locals 3

    const/4 v2, 0x1

    if-eqz p2, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    const/4 v1, 0x0

    invoke-virtual {p2, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    invoke-static {v0, p1, p2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_onTouch(IILandroid/view/MotionEvent;I)Z

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 14

    const/4 v2, 0x0

    const/4 v1, 0x1

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    const/high16 v3, 0x3f000000    # 0.5f

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v4

    move v0, v2

    :goto_1
    if-lt v0, v4, :cond_5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v3, v0, 0xff

    if-nez v3, :cond_2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v6

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x64

    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mTouchProcessingTime:J

    add-long/2addr v6, v8

    cmp-long v0, v4, v6

    if-lez v0, :cond_6

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->isSkipTouch:Z

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->isSkipTouch:Z

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v8

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    const-string/jumbo v0, "SpenMultiView"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string/jumbo v13, "skiptouch action = "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v12, " eventTime = "

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v6, " downTime = "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v6, " systemTime = "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v6, " diffTime = "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartX:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFrameStartY:I

    neg-int v3, v3

    int-to-float v3, v3

    invoke-virtual {p1, v0, v3}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->isSkipTouch:Z

    if-nez v0, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v3

    const-string/jumbo v0, "SpenMultiView"

    const-string/jumbo v5, "Performance mPreTouchListener start"

    invoke-static {v0, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    invoke-interface {v0, p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_3
    const-string/jumbo v0, "SpenMultiView"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "Performance mPreTouchListener.onTouch end "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v3

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " ms"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v2

    invoke-static {v0, v5, p1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_onTouch(IILandroid/view/MotionEvent;I)Z

    const-string/jumbo v0, "SpenMultiView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Performance native_onTouch end "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v5

    sub-long/2addr v5, v3

    invoke-virtual {v2, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v5, " ms"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    invoke-interface {v0, p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_4
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v5

    sub-long/2addr v5, v3

    iput-wide v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mTouchProcessingTime:J

    const-string/jumbo v0, "SpenMultiView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "Performance mTouchListener.onTouch end "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v5

    sub-long v3, v5, v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_5
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getSize(I)F

    move-result v5

    cmpl-float v5, v5, v3

    if-gtz v5, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    :cond_6
    move v0, v2

    goto/16 :goto_2
.end method

.method public removeUser(I)Z
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    invoke-static {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_removeUser(II)Z

    move-result v0

    goto :goto_0
.end method

.method public setBackgroundColorChangeListener(Ljava/lang/Object;Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;)V
    .locals 2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "SpenMultiView"

    const-string/jumbo v1, "setBackgroundColorListener"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    invoke-interface {p2, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;->onChanged(Z)V

    goto :goto_0
.end method

.method public setBlankColor(I)V
    .locals 2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->setPaint(Landroid/graphics/Paint;)V

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->onUpdateCanvas(Landroid/graphics/RectF;Z)V

    goto :goto_0
.end method

.method public setColorPickerListener(Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    goto :goto_0
.end method

.method public setEraserChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mEraserChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

    goto :goto_0
.end method

.method public setEraserSettingInfo(ILcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V
    .locals 2

    const/high16 v1, 0x40000000    # 2.0f

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p2, :cond_0

    iget v0, p2, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    iput v1, p2, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    :cond_2
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    iget v1, p2, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    invoke-static {v0, p1, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setEraserSize(IIF)Z

    goto :goto_0
.end method

.method public setEraserSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V
    .locals 3

    const/high16 v1, 0x40000000    # 2.0f

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_3

    iget v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    iput v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    :cond_2
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    iget v2, p1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setEraserSize(IIF)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    const-string/jumbo v1, "not set LocalUserId"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mEraserChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mEraserChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;->onChanged(Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V

    goto :goto_0
.end method

.method public setFlickListener(Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFlickListener:Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

    goto :goto_0
.end method

.method public setHoverListener(Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;)V
    .locals 2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string/jumbo v1, "4."

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    goto :goto_0

    :cond_1
    const/16 v0, 0xc

    const-string/jumbo v1, "S Pen Hover Listener cannot be supported under android ICS"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public setLocalUserId(I)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    invoke-static {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setLocalUserId(II)Z

    move-result v0

    if-eqz v0, :cond_0

    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    goto :goto_0
.end method

.method public setMaxZoomRatio(F)Z
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    invoke-static {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setMaxZoomRatio(IF)Z

    move-result v0

    goto :goto_0
.end method

.method public setMinZoomRatio(F)Z
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    invoke-static {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setMinZoomRatio(IF)Z

    move-result v0

    goto :goto_0
.end method

.method public setPageDoc(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;IIF)Z
    .locals 4

    const/4 v0, 0x0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v1

    if-nez v1, :cond_2

    const-string/jumbo v1, "SpenMultiView"

    const-string/jumbo v2, "setPageDoc is closed"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->isWorking()Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "SpenMultiView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "setPageDoc, direction="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v1, p3}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->setType(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->saveScreenshot()Z

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->createBitmap(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    const/4 v2, 0x0

    invoke-static {v1, v2, p4, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setPan(IFFZ)V

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-static {v1, v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setPageDoc(ILcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v0, p2}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->startAnimation(I)Z

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setPageDoc(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z
    .locals 3

    const/4 v0, 0x0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v1

    if-nez v1, :cond_1

    const-string/jumbo v1, "SpenMultiView"

    const-string/jumbo v2, "setPageDoc is closed"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->createBitmap(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    invoke-static {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setPageDoc(ILcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setPageEffectListener(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

    goto :goto_0
.end method

.method public setPan(Landroid/graphics/PointF;)V
    .locals 4

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    iget v1, p1, Landroid/graphics/PointF;->x:F

    iget v2, p1, Landroid/graphics/PointF;->y:F

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setPan(IFFZ)V

    goto :goto_0
.end method

.method public setPenChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    goto :goto_0
.end method

.method public setPenSettingInfo(ILcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V
    .locals 2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p2, :cond_0

    iget v0, p2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    :cond_2
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    iget-object v1, p2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-static {v0, p1, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setPenStyle(IILjava/lang/String;)Z

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    iget v1, p2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-static {v0, p1, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setPenColor(III)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    iget v1, p2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    invoke-static {v0, p1, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setPenSize(IIF)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    iget-boolean v1, p2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    invoke-static {v0, p1, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_enablePenCurve(IIZ)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    iget-object v1, p2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-static {v0, p1, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setAdvancedSetting(IILjava/lang/String;)V

    goto :goto_0
.end method

.method public setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V
    .locals 3

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_0

    iget v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    :cond_2
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    iget-object v2, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setPenStyle(IILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    const-string/jumbo v1, "not set LocalUserId"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    :cond_3
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    iget v2, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setPenColor(III)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    iget v2, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setPenSize(IIF)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    iget-boolean v2, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_enablePenCurve(IIZ)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    iget-object v2, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setAdvancedSetting(IILjava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;->onChanged(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    goto :goto_0
.end method

.method public setPreTouchListener(Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    goto :goto_0
.end method

.method public setRemoverSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V
    .locals 0

    return-void
.end method

.method public setSelectionSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;)V
    .locals 0

    return-void
.end method

.method public setTextSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V
    .locals 0

    return-void
.end method

.method public setToolTypeAction(II)V
    .locals 2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->setToolTypeAction(II)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->localUserId:I

    invoke-static {v0, v1, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setToolTypeAction(IIII)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    const-string/jumbo v1, "not set LocalUserId"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public setToolTypeAction(III)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    invoke-static {v0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setToolTypeAction(IIII)Z

    goto :goto_0
.end method

.method public setTouchListener(Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    goto :goto_0
.end method

.method public setZoom(FFF)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    invoke-static {v0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setZoom(IFFF)V

    goto :goto_0
.end method

.method public setZoomListener(Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    goto :goto_0
.end method

.method public update()V
    .locals 2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_update(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    const-string/jumbo v1, "We can\'t update this state, we can update just for append object"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public updateRedo([Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;I)V
    .locals 2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_updateHistory(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    const-string/jumbo v1, "We can\'t redo"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public updateUndo([Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;I)V
    .locals 2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_updateHistory(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    const-string/jumbo v1, "We can\'t undo"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method
