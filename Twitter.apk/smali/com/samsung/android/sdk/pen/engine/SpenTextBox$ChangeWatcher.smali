.class Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/text/SpanWatcher;
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 2

    const/4 v0, 0x0

    const-string/jumbo v1, "beforeTextChanged"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideCursorHandle()V
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$47(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTyping:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$48(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Typing;->startInput()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$23(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->initMeasureInfo()V
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$49(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    return-void
.end method

.method public onSpanAdded(Landroid/text/Spannable;Ljava/lang/Object;II)V
    .locals 0

    return-void
.end method

.method public onSpanChanged(Landroid/text/Spannable;Ljava/lang/Object;IIII)V
    .locals 0

    return-void
.end method

.method public onSpanRemoved(Landroid/text/Spannable;Ljava/lang/Object;II)V
    .locals 0

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 5

    const/4 v4, 0x1

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsEditableClear:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$50(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$25(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$25(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$25(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$CContextMenu;->hide()V

    :cond_2
    add-int v0, p2, p4

    invoke-interface {p1, p2, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "onTextChanged() text = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", str = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", start = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", before = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", after = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    if-eqz p3, :cond_9

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v1, v4, :cond_7

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsCommitText:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$51(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsComposingText:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$52(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsDeletedText:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$53(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_3
    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onTextChanged() removeText : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " ~ "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->removeText(II)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mCursorVisible:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$54(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$20(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    :cond_4
    :goto_1
    if-eqz p4, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateSelection()V
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V
    :try_end_0
    .catch Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;->printStackTrace()V

    goto/16 :goto_0

    :cond_6
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSurroundingTextLength:I
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$55(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I

    move-result v1

    add-int/2addr v1, p3

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$56(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;I)V

    goto :goto_1

    :cond_7
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSurroundingTextLength:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$55(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I

    move-result v1

    if-nez v1, :cond_8

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v1

    invoke-virtual {v1, v0, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->replaceText(Ljava/lang/String;II)V

    goto :goto_1

    :cond_8
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSurroundingTextLength:I
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$55(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I

    move-result v2

    invoke-virtual {v1, v0, p2, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->replaceText(Ljava/lang/String;II)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$56(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;I)V

    goto :goto_1

    :cond_9
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSurroundingTextLength:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$55(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I

    move-result v1

    if-nez v1, :cond_b

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_a

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->getDirectionality(C)B

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_a
    :goto_2
    :pswitch_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->insertTextAtCursor(Ljava/lang/String;)V

    goto :goto_1

    :pswitch_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v4

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextDirection(III)V
    invoke-static {v1, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$57(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;III)V

    goto :goto_2

    :pswitch_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v4

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextDirection(III)V
    invoke-static {v1, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$57(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;III)V

    goto :goto_2

    :cond_b
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mSurroundingTextLength:I
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$55(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I

    move-result v2

    invoke-virtual {v1, v0, p2, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->replaceText(Ljava/lang/String;II)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ChangeWatcher;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$56(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;I)V
    :try_end_1
    .catch Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
