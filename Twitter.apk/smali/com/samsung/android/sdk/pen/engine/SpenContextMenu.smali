.class public Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation


# static fields
.field private static final DEFAULT_HEIGHT:I = 0x3d

.field private static final DEFAULT_HEIGHT_2:I = 0x4e

.field private static final DEFAULT_HEIGHT_2_N1:I = 0x5c

.field private static final DEFAULT_HEIGHT_N1:I = 0x48

.field protected static final DEFAULT_ITEM_WIDTH:I = 0x48

.field protected static final DEFAULT_ITEM_WIDTH_N1:I = 0x62

.field private static final DEFAULT_MINIMUM_DELAY:I = 0x3e8

.field protected static final DEFAULT_SEPERATOR_WIDTH:I = 0x1

.field protected static final DEFAULT_SEPERATOR_WIDTH_N1:I = 0x2

.field private static final DEFAULT_TEXT_SIZE:I = 0xc

.field private static final DEFAULT_TEXT_SIZE_N1:I = 0xf

.field private static final DEFAULT_WIDTH:I = 0x136

.field private static final MAX_RATE_PER_WIDTH:F = 0.9f

.field protected static final QUICK_POPUP_BG:Ljava/lang/String; = "quick_popup_bg"

.field protected static final QUICK_POPUP_BG_PRESS:Ljava/lang/String; = "quick_popup_bg_press"

.field protected static final QUICK_POPUP_DV:Ljava/lang/String; = "quick_popup_dv"

.field protected static final SHADOW_BORDER:Ljava/lang/String; = "shadow_border"

.field protected static final SHADOW_BORDER_N1:Ljava/lang/String; = "shadow_border_n1"

.field protected static final TW_QUICK_BUBBLE_DIVIDER_HOLO_LIGHT:Ljava/lang/String; = "tw_quick_bubble_divider_holo_light"

.field public static final TYPE_DEFAULT:I = 0x0

.field public static final TYPE_WIDE:I = 0x1

.field private static mType:I


# instance fields
.field private mBgImageLeftItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

.field private mBgImageRightItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

.field private final mContext:Landroid/content/Context;

.field private mCount:F

.field private mDelay:I

.field private final mHandler:Landroid/os/Handler;

.field private mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

.field private mIsNormalMode:Z

.field private mLeftEdgeEffect:Landroid/widget/EdgeEffect;

.field mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

.field private mNumItem:I

.field private mParent:Landroid/view/View;

.field private mPopupView:Landroid/view/View;

.field private mPopupWindow:Landroid/widget/PopupWindow;

.field private mRect:Landroid/graphics/Rect;

.field private mResourceSDK:Landroid/content/res/Resources;

.field private mRightEdgeEffect:Landroid/widget/EdgeEffect;

.field private mScrollFlag:Z

.field private mScrollTimer:Ljava/util/Timer;

.field private mSelectListener:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$ContextMenuListener;

.field private final mSpenHorizontalListViewListener:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$Listener;

.field private mTimer:Ljava/util/Timer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/View;Ljava/util/ArrayList;Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$ContextMenuListener;)V
    .locals 17

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mSelectListener:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$ContextMenuListener;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mTimer:Ljava/util/Timer;

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mDelay:I

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollTimer:Ljava/util/Timer;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollFlag:Z

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsNormalMode:Z

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mLeftEdgeEffect:Landroid/widget/EdgeEffect;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRightEdgeEffect:Landroid/widget/EdgeEffect;

    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$1;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$1;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mSpenHorizontalListViewListener:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$Listener;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mCount:F

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    if-eqz p1, :cond_4

    new-instance v2, Landroid/graphics/Rect;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/16 v5, 0x136

    const/16 v6, 0x3d

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    :try_start_0
    const-string/jumbo v3, "com.samsung.android.sdk.spen30"

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    move-object/from16 v0, p3

    invoke-static {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;Ljava/util/ArrayList;)V

    invoke-direct/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->isNormalMode()Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsNormalMode:Z

    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mSelectListener:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$ContextMenuListener;

    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mParent:Landroid/view/View;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupView:Landroid/view/View;

    new-instance v9, Landroid/widget/RelativeLayout;

    move-object/from16 v0, p1

    invoke-direct {v9, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v3, -0x1

    const/4 v4, -0x1

    invoke-direct {v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;

    if-eqz v3, :cond_0

    sget v2, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;

    const-string/jumbo v3, "shadow_border_n1"

    const-string/jumbo v4, "drawable"

    const-string/jumbo v5, "com.samsung.android.sdk.spen30"

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;

    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    :cond_0
    :goto_1
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x10

    if-ge v3, v4, :cond_6

    if-eqz v2, :cond_6

    invoke-virtual {v9, v2}, Landroid/widget/RelativeLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    :goto_2
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    sget v3, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_7

    const/16 v7, 0x62

    const/4 v6, 0x2

    const/16 v5, 0x5c

    const/16 v4, 0x5c

    const/16 v3, 0x8

    move v8, v7

    move v7, v6

    move v6, v5

    move v5, v4

    move v4, v3

    :goto_3
    const/4 v3, 0x4

    if-ge v2, v3, :cond_8

    mul-int v3, v8, v2

    :goto_4
    const/4 v11, 0x1

    int-to-float v3, v3

    invoke-static {v11, v3, v10}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    float-to-int v3, v3

    add-int/lit8 v11, v2, -0x1

    mul-int/2addr v11, v7

    add-int/2addr v3, v11

    new-instance v11, Landroid/graphics/Rect;

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/16 v14, 0x438

    const/16 v15, 0x438

    invoke-direct {v11, v12, v13, v14, v15}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mParent:Landroid/view/View;

    if-eqz v12, :cond_2

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mParent:Landroid/view/View;

    invoke-virtual {v12, v11}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    :cond_2
    invoke-virtual {v11}, Landroid/graphics/Rect;->width()I

    move-result v11

    if-eqz v11, :cond_f

    int-to-float v12, v3

    int-to-float v13, v11

    div-float/2addr v12, v13

    const v13, 0x3f666666    # 0.9f

    cmpl-float v12, v12, v13

    if-lez v12, :cond_f

    move/from16 v16, v3

    move v3, v2

    move/from16 v2, v16

    :cond_3
    if-gtz v3, :cond_c

    :goto_5
    if-lez v3, :cond_d

    :goto_6
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsNormalMode:Z

    if-eqz v3, :cond_e

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iput v2, v3, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    const/4 v3, 0x1

    int-to-float v5, v6

    invoke-static {v3, v5, v10}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    float-to-int v3, v3

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    :goto_7
    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-direct {v2, v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mSpenHorizontalListViewListener:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$Listener;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->setListener(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$Listener;)V

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    const/4 v3, -0x1

    const/4 v5, -0x1

    invoke-direct {v2, v3, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    invoke-virtual {v3, v2}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v2, v3, v5, v6, v7}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->setPadding(IIII)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->setFadingEdgeLength(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->setVerticalScrollBarEnabled(Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->setOverScrollMode(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$HAdapter;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)V

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->setAdapter(Landroid/widget/ListAdapter;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->setScrollBarStyle(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    invoke-virtual {v9, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    const/4 v2, 0x1

    int-to-float v3, v4

    invoke-static {v2, v3, v10}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v9, v2, v2, v2, v2}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupView:Landroid/view/View;

    new-instance v3, Landroid/widget/PopupWindow;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupView:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    mul-int/lit8 v6, v2, 0x2

    add-int/2addr v5, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->height()I

    move-result v6

    mul-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v6

    invoke-direct {v3, v4, v5, v2}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;II)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setAnimationStyle(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;)V

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setFocusable(Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$2;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$2;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)V

    invoke-virtual {v2, v3}, Landroid/widget/PopupWindow;->setTouchInterceptor(Landroid/view/View$OnTouchListener;)V

    :cond_4
    return-void

    :catch_0
    move-exception v2

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;

    goto/16 :goto_0

    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;

    const-string/jumbo v4, "shadow_border"

    const-string/jumbo v5, "drawable"

    const-string/jumbo v6, "com.samsung.android.sdk.spen30"

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    goto/16 :goto_1

    :cond_6
    if-eqz v2, :cond_1

    invoke-virtual {v9, v2}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    :cond_7
    const/16 v7, 0x48

    const/4 v6, 0x1

    const/16 v5, 0x3d

    const/16 v4, 0x4e

    const/4 v3, 0x5

    move v8, v7

    move v7, v6

    move v6, v5

    move v5, v4

    move v4, v3

    goto/16 :goto_3

    :cond_8
    const/4 v3, 0x6

    if-ge v2, v3, :cond_a

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    const/4 v11, 0x1

    if-ne v3, v11, :cond_9

    const/4 v2, 0x4

    mul-int v3, v8, v2

    goto/16 :goto_4

    :cond_9
    mul-int v3, v8, v2

    goto/16 :goto_4

    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_b

    const/4 v2, 0x4

    mul-int v3, v8, v2

    goto/16 :goto_4

    :cond_b
    const/4 v2, 0x6

    mul-int v3, v8, v2

    goto/16 :goto_4

    :cond_c
    add-int/lit8 v3, v3, -0x1

    mul-int v2, v8, v3

    const/4 v12, 0x1

    int-to-float v2, v2

    invoke-static {v12, v2, v10}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    add-int/lit8 v12, v3, -0x1

    mul-int/2addr v12, v7

    add-int/2addr v2, v12

    int-to-float v12, v2

    int-to-float v13, v11

    div-float/2addr v12, v13

    const v13, 0x3f666666    # 0.9f

    cmpg-float v12, v12, v13

    if-gtz v12, :cond_3

    goto/16 :goto_5

    :cond_d
    const/4 v2, 0x1

    int-to-float v3, v8

    invoke-static {v2, v3, v10}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    goto/16 :goto_6

    :cond_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iput v2, v3, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    const/4 v3, 0x1

    int-to-float v5, v5

    invoke-static {v3, v5, v10}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    float-to-int v3, v3

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    goto/16 :goto_7

    :cond_f
    move v2, v3

    goto/16 :goto_6
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollFlag:Z

    return-void
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageLeftItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    return-object v0
.end method

.method static synthetic access$10(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsNormalMode:Z

    return v0
.end method

.method static synthetic access$11(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/res/Resources;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mResourceSDK:Landroid/content/res/Resources;

    return-object v0
.end method

.method static synthetic access$12(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageLeftItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    return-void
.end method

.method static synthetic access$13(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageRightItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    return-void
.end method

.method static synthetic access$14(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mParent:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$15(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mDelay:I

    return v0
.end method

.method static synthetic access$16(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->updateTimer(I)V

    return-void
.end method

.method static synthetic access$17(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$18(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)F
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mCount:F

    return v0
.end method

.method static synthetic access$19(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;F)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mCount:F

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    return-object v0
.end method

.method static synthetic access$20(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Ljava/util/Timer;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollTimer:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic access$21(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;Ljava/util/Timer;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollTimer:Ljava/util/Timer;

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageRightItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/widget/EdgeEffect;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRightEdgeEffect:Landroid/widget/EdgeEffect;

    return-object v0
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/widget/EdgeEffect;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mLeftEdgeEffect:Landroid/widget/EdgeEffect;

    return-object v0
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$7()I
    .locals 1

    sget v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I

    return v0
.end method

.method static synthetic access$8(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$ContextMenuListener;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mSelectListener:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$ContextMenuListener;

    return-object v0
.end method

.method static synthetic access$9(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollFlag:Z

    return v0
.end method

.method public static getType()I
    .locals 1

    sget v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I

    return v0
.end method

.method private isNormalMode()Z
    .locals 10

    const/4 v4, -0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    if-nez v0, :cond_1

    move v2, v3

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_2

    move v2, v3

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    if-nez v0, :cond_3

    move v2, v3

    goto :goto_0

    :cond_3
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v4, 0xe

    invoke-virtual {v1, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    sget v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I

    if-ne v1, v3, :cond_4

    const-string/jumbo v1, "#1e1e1e"

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    const/high16 v1, 0x41700000    # 15.0f

    invoke-virtual {v0, v3, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    :goto_1
    invoke-virtual {v0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    sget v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I

    if-ne v0, v3, :cond_5

    const/high16 v0, 0x42c40000    # 98.0f

    invoke-static {v3, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    :goto_2
    const/high16 v4, 0x40400000    # 3.0f

    invoke-static {v3, v4, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    mul-int/lit8 v1, v1, 0x2

    sub-int v6, v0, v1

    move v1, v2

    :goto_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_6

    move v2, v3

    goto :goto_0

    :cond_4
    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    const/high16 v1, 0x41400000    # 12.0f

    invoke-virtual {v0, v3, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_1

    :cond_5
    const/high16 v0, 0x42900000    # 72.0f

    invoke-static {v3, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->name:Ljava/lang/String;

    if-nez v0, :cond_7

    move v2, v3

    goto/16 :goto_0

    :cond_7
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    new-array v7, v4, [F

    invoke-virtual {v5, v0, v2, v4, v7}, Landroid/graphics/Paint;->getTextWidths(Ljava/lang/String;II[F)I

    move-result v8

    const/4 v0, 0x0

    move v4, v0

    move v0, v2

    :goto_4
    if-lt v0, v8, :cond_8

    int-to-float v0, v6

    cmpg-float v0, v0, v4

    if-ltz v0, :cond_0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_8
    aget v9, v7, v0

    add-float/2addr v4, v9

    add-int/lit8 v0, v0, 0x1

    goto :goto_4
.end method

.method private playScrollAnimation(I)V
    .locals 6

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    sget v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I

    if-ne v0, v3, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x62

    int-to-float v0, v0

    invoke-static {v3, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    mul-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    add-float/2addr v0, v2

    :goto_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    sub-float v2, v0, v2

    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {v3, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollTimer:Ljava/util/Timer;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollTimer:Ljava/util/Timer;

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;

    invoke-direct {v1, p0, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;FF)V

    const-wide/16 v2, 0x64

    int-to-long v4, p1

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x48

    int-to-float v0, v0

    invoke-static {v3, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    mul-int/lit8 v2, v2, 0x1

    int-to-float v2, v2

    add-float/2addr v0, v2

    goto :goto_1
.end method

.method public static setType(I)V
    .locals 0

    sput p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I

    return-void
.end method

.method private updateContextMenuLocation()V
    .locals 13

    const/4 v8, 0x6

    const v12, 0x3f666666    # 0.9f

    const/4 v9, 0x4

    const/4 v11, 0x0

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    sget v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I

    if-ne v0, v4, :cond_5

    const/16 v5, 0x62

    const/4 v3, 0x2

    const/16 v2, 0x48

    const/16 v1, 0x5c

    const/16 v0, 0x8

    :goto_0
    iput v11, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    if-ge v6, v9, :cond_6

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    mul-int/2addr v6, v5

    :goto_1
    int-to-float v6, v6

    invoke-static {v4, v6, v7}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v6

    float-to-int v6, v6

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    add-int/lit8 v8, v8, -0x1

    mul-int/2addr v8, v3

    add-int/2addr v6, v8

    new-instance v8, Landroid/graphics/Rect;

    const/16 v9, 0x438

    const/16 v10, 0x438

    invoke-direct {v8, v11, v11, v9, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mParent:Landroid/view/View;

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mParent:Landroid/view/View;

    invoke-virtual {v9, v8}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    :cond_0
    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v8

    if-eqz v8, :cond_2

    int-to-float v9, v6

    int-to-float v10, v8

    div-float/2addr v9, v10

    cmpl-float v9, v9, v12

    if-lez v9, :cond_2

    :cond_1
    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    if-gtz v9, :cond_a

    :goto_2
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    if-lez v3, :cond_b

    :cond_2
    :goto_3
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mIsNormalMode:Z

    if-eqz v3, :cond_c

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v6

    iput v3, v1, Landroid/graphics/Rect;->right:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    invoke-static {v4, v2, v7}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    float-to-int v2, v2

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    :goto_4
    int-to-float v0, v0

    invoke-static {v4, v0, v7}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    mul-int/lit8 v3, v0, 0x2

    add-int/2addr v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->setWidth(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    mul-int/lit8 v3, v0, 0x2

    add-int/2addr v2, v3

    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->setHeight(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupView:Landroid/view/View;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupView:Landroid/view/View;

    invoke-virtual {v1, v0, v0, v0, v0}, Landroid/view/View;->setPadding(IIII)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    if-le v0, v1, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getLeftEdgeEffect()Landroid/widget/EdgeEffect;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mLeftEdgeEffect:Landroid/widget/EdgeEffect;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getRightEdgeEffect()Landroid/widget/EdgeEffect;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRightEdgeEffect:Landroid/widget/EdgeEffect;

    :cond_4
    return-void

    :cond_5
    const/16 v3, 0x48

    const/16 v2, 0x3d

    const/16 v1, 0x4e

    const/4 v0, 0x5

    move v5, v3

    move v3, v4

    goto/16 :goto_0

    :cond_6
    if-ge v6, v8, :cond_8

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v8

    iget v8, v8, Landroid/content/res/Configuration;->orientation:I

    if-ne v8, v4, :cond_7

    iput v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    mul-int/2addr v6, v5

    goto/16 :goto_1

    :cond_7
    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    mul-int/2addr v6, v5

    goto/16 :goto_1

    :cond_8
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v6

    iget v6, v6, Landroid/content/res/Configuration;->orientation:I

    if-ne v6, v4, :cond_9

    iput v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    mul-int/2addr v6, v5

    goto/16 :goto_1

    :cond_9
    iput v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    mul-int/2addr v6, v5

    goto/16 :goto_1

    :cond_a
    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    add-int/lit8 v6, v6, -0x1

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    mul-int/2addr v6, v5

    int-to-float v6, v6

    invoke-static {v4, v6, v7}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v6

    float-to-int v6, v6

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    add-int/lit8 v9, v9, -0x1

    mul-int/2addr v9, v3

    add-int/2addr v6, v9

    int-to-float v9, v6

    int-to-float v10, v8

    div-float/2addr v9, v10

    cmpg-float v9, v9, v12

    if-gtz v9, :cond_1

    goto/16 :goto_2

    :cond_b
    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    int-to-float v3, v5

    invoke-static {v4, v3, v7}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    float-to-int v6, v3

    goto/16 :goto_3

    :cond_c
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    add-int/2addr v3, v6

    iput v3, v2, Landroid/graphics/Rect;->right:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    invoke-static {v4, v1, v7}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    add-int/2addr v1, v3

    iput v1, v2, Landroid/graphics/Rect;->bottom:I

    goto/16 :goto_4
.end method

.method private updateTimer(I)V
    .locals 4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    :cond_0
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mTimer:Ljava/util/Timer;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mTimer:Ljava/util/Timer;

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$4;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$4;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)V

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    return-void
.end method


# virtual methods
.method public close()Z
    .locals 1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->hide()V

    const/4 v0, 0x1

    return v0
.end method

.method protected getCountMenuItem()I
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItemEnabled(I)Z
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    iget v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->id:I

    if-ne v3, p1, :cond_1

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    goto :goto_0
.end method

.method public getRect()Landroid/graphics/Rect;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method protected handleSelectMenuItem(I)V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mSelectListener:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$ContextMenuListener;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mSelectListener:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$ContextMenuListener;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->id:I

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$ContextMenuListener;->onSelected(I)V

    :cond_0
    return-void
.end method

.method public hide()V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mCount:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollTimer:Ljava/util/Timer;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollTimer:Ljava/util/Timer;

    :cond_1
    return-void
.end method

.method public isShowing()Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected scrollToMenuItem(I)V
    .locals 4

    const/4 v3, 0x1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->scrollTo(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    sget v1, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mType:I

    if-ne v1, v3, :cond_1

    add-int/lit8 v1, p1, 0x1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    sub-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x62

    int-to-float v1, v1

    invoke-static {v3, v1, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    add-int/lit8 v1, p1, 0x1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    sub-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    add-float/2addr v0, v1

    :goto_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    float-to-int v0, v0

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->scrollTo(I)V

    goto :goto_0

    :cond_1
    add-int/lit8 v1, p1, 0x1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    sub-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x48

    int-to-float v1, v1

    invoke-static {v3, v1, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    add-int/lit8 v1, p1, 0x1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    sub-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    add-float/2addr v0, v1

    goto :goto_1
.end method

.method protected setFocusMenuItem(I)Landroid/view/View;
    .locals 10

    const/16 v9, 0xec

    const/16 v8, 0xeb

    const/16 v7, 0xe9

    const/16 v6, 0xc8

    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, -0x1

    if-ne p1, v0, :cond_4

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getChildCount()I

    move-result v0

    if-lt v1, v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mParent:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mParent:Landroid/view/View;

    instance-of v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mParent:Landroid/view/View;

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->requestFocus()Z

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_1
    return-object v1

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "2407"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v7, v8, v9}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/view/View;->setBackgroundColor(I)V

    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_4
    move v4, v2

    move v3, v2

    :goto_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getChildCount()I

    move-result v0

    if-lt v4, v0, :cond_5

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mNumItem:I

    if-lt p1, v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v6, v6, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Landroid/view/View;->getId()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v5, "2407"

    invoke-virtual {v0, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    if-ne v3, p1, :cond_6

    move-object v0, v1

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v6, v6, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v5

    invoke-virtual {v0, v5}, Landroid/view/View;->setBackgroundColor(I)V

    :goto_3
    add-int/lit8 v0, v3, 0x1

    :goto_4
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v0

    goto :goto_2

    :cond_6
    move-object v0, v1

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v7, v8, v9}, Landroid/graphics/Color;->rgb(III)I

    move-result v5

    invoke-virtual {v0, v5}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_3

    :cond_7
    move v0, v3

    goto :goto_4
.end method

.method public setItemEnabled(IZ)V
    .locals 3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mMenu:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->mItemList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CMenu;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/BaseAdapter;

    invoke-virtual {v0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    goto :goto_0

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->id:I

    if-ne v2, p1, :cond_2

    iput-boolean p2, v0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenuItemInfo;->enable:Z

    goto :goto_1
.end method

.method public setRect(Landroid/graphics/Rect;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    invoke-virtual {v0, p1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    return-void
.end method

.method public show()V
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mParent:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->updateContextMenuLocation()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mParent:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v3}, Landroid/widget/PopupWindow;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    if-le v0, v2, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v4}, Landroid/widget/PopupWindow;->getWidth()I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v5}, Landroid/widget/PopupWindow;->getHeight()I

    move-result v5

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/widget/PopupWindow;->update(IIII)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mParent:Landroid/view/View;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    :goto_1
    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->playScrollAnimation(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mParent:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v2}, Landroid/widget/PopupWindow;->getWidth()I

    move-result v2

    sub-int/2addr v0, v2

    if-gez v0, :cond_3

    move v0, v1

    :cond_3
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v5}, Landroid/widget/PopupWindow;->getWidth()I

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v6}, Landroid/widget/PopupWindow;->getHeight()I

    move-result v6

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/widget/PopupWindow;->update(IIII)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mPopupWindow:Landroid/widget/PopupWindow;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mParent:Landroid/view/View;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    invoke-virtual {v2, v3, v1, v0, v4}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    goto :goto_1
.end method

.method public show(I)V
    .locals 0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->show()V

    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mDelay:I

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->updateTimer(I)V

    return-void
.end method
