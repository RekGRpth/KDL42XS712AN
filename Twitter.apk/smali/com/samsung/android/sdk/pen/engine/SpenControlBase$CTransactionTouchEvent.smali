.class public Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTransactionTouchEvent;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field protected mIsTouchDownPressed:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTransactionTouchEvent;->mIsTouchDownPressed:Z

    return-void
.end method


# virtual methods
.method public check(Landroid/view/MotionEvent;)V
    .locals 4

    const/4 v3, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    if-ne v0, v3, :cond_1

    :cond_0
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTransactionTouchEvent;->mIsTouchDownPressed:Z

    if-nez v2, :cond_1

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->setAction(I)V

    move v0, v1

    :cond_1
    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTransactionTouchEvent;->mIsTouchDownPressed:Z

    goto :goto_0

    :pswitch_1
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTransactionTouchEvent;->mIsTouchDownPressed:Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
