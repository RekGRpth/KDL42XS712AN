.class Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$HolderCallback;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$HolderCallback;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$HolderCallback;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$HolderCallback;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)V

    return-void
.end method


# virtual methods
.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$HolderCallback;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$HolderCallback;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;->onUpdateCanvas(Landroid/graphics/RectF;Z)V

    :cond_0
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 0

    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 0

    return-void
.end method
