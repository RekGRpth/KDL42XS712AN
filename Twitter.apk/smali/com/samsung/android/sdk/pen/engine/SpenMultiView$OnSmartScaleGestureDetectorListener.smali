.class Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnSmartScaleGestureDetectorListener;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnSmartScaleGestureDetectorListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnSmartScaleGestureDetectorListener;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnSmartScaleGestureDetectorListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)V

    return-void
.end method


# virtual methods
.method public onChangePan(FF)V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnSmartScaleGestureDetectorListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mNativeMulti:I
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)I

    move-result v0

    const/4 v1, 0x1

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->native_setPan(IFFZ)V
    invoke-static {v0, p1, p2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->access$4(IFFZ)V

    return-void
.end method

.method public onChangeScale(FFF)V
    .locals 0

    return-void
.end method

.method public onFlick(I)Z
    .locals 3

    const-string/jumbo v0, "SpenMultiView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onFlick direction = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnSmartScaleGestureDetectorListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->isWorking()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnSmartScaleGestureDetectorListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFlickListener:Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnSmartScaleGestureDetectorListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mFlickListener:Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;->onFlick(I)Z

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onUpdate()V
    .locals 3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnSmartScaleGestureDetectorListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    const/4 v1, 0x0

    const/4 v2, 0x1

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->onUpdateCanvas(Landroid/graphics/RectF;Z)V
    invoke-static {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Landroid/graphics/RectF;Z)V

    return-void
.end method

.method public onUpdateScreenFrameBuffer()V
    .locals 0

    return-void
.end method
