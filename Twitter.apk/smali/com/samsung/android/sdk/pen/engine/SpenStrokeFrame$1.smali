.class Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mStrokeFrameType:I
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-result-object v2

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->makePath(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;
    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Path;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mBeautifyContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->setChangeFrameButtonResources()V
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mFrameShapePath:Landroid/graphics/Path;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Path;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v2

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v3

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->convertRelative(Landroid/graphics/RectF;)Landroid/graphics/RectF;
    invoke-static {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v2

    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v4

    sget-object v5, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v3, v4, v2, v5}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mFrameShapePath:Landroid/graphics/Path;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Path;

    move-result-object v4

    invoke-virtual {v4, v3, v1}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;Landroid/graphics/Path;)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->createShapeMaskBitmap(Landroid/graphics/Path;)V
    invoke-static {v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Path;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskBitmap:Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$12(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Bitmap;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v4

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->drawPen(Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;Landroid/graphics/RectF;Landroid/graphics/RectF;)Z
    invoke-static {v1, v3, v0, v4, v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$13(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Bitmap;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "SpenStrokeFrame"

    const-string/jumbo v1, "drawPen is failed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    const/16 v1, 0x20

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancel(I)V
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    :goto_1
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    invoke-static {v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mObjectContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->makePath(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;
    invoke-static {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)Landroid/graphics/Path;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/Path;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mOriginalContainer:Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-result-object v0

    goto/16 :goto_0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->setRect(Landroid/graphics/RectF;)V
    invoke-static {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$15(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/graphics/RectF;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->updateChangeFrameButtonPosition()V
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$16(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskView:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$17(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mShapeMaskBitmap:Landroid/graphics/Bitmap;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$12(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1
.end method
