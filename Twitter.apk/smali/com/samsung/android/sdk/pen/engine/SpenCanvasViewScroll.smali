.class Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;
.super Landroid/os/Handler;
.source "Twttr"


# static fields
.field private static final SCROLL_BAR_MARGIN:I = 0xa

.field private static final SCROLL_BAR_THICK:I = 0xa


# instance fields
.field private mDeltaX:F

.field private mDeltaY:F

.field private mEnable:Z

.field private final mListener:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll$Listener;

.field private mMaxDeltaX:F

.field private mMaxDeltaY:F

.field private mPaint:Landroid/graphics/Paint;

.field private mRatioBitmapH:I

.field private mRatioBitmapW:I

.field private mRectLR:Landroid/graphics/Rect;

.field private mRectTB:Landroid/graphics/Rect;

.field private mScreenH:I

.field private mScreenW:I

.field private mShow:Z


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll$Listener;)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mEnable:Z

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mShow:Z

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mRectLR:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mRectTB:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mPaint:Landroid/graphics/Paint;

    const v1, -0x7fa0a0a1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll$Listener;

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mRectLR:Landroid/graphics/Rect;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mRectTB:Landroid/graphics/Rect;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mPaint:Landroid/graphics/Paint;

    return-void
.end method

.method public drawScroll(Landroid/graphics/Canvas;)V
    .locals 3

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mEnable:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mShow:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mMaxDeltaX:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mRectLR:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_2
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mMaxDeltaY:F

    cmpl-float v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mRectTB:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public enableScroll(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mEnable:Z

    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mShow:Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll$Listener;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll$Listener;->onUpdate()V

    :cond_0
    return-void
.end method

.method public isScroll()Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mEnable:Z

    return v0
.end method

.method public setDeltaValue(FFFF)V
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x0

    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mDeltaX:F

    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mDeltaY:F

    iput p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mMaxDeltaX:F

    iput p4, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mMaxDeltaY:F

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mMaxDeltaX:F

    cmpl-float v0, v0, v5

    if-lez v0, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mScreenW:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mScreenW:I

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mRatioBitmapW:I

    div-int/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mScreenW:I

    sub-int/2addr v1, v0

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mMaxDeltaX:F

    div-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mDeltaX:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mRectLR:Landroid/graphics/Rect;

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mScreenH:I

    add-int/lit8 v3, v3, -0xa

    add-int/lit8 v3, v3, -0xa

    add-int/2addr v0, v1

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mScreenH:I

    add-int/lit8 v4, v4, -0xa

    invoke-virtual {v2, v1, v3, v0, v4}, Landroid/graphics/Rect;->set(IIII)V

    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mMaxDeltaY:F

    cmpl-float v0, v0, v5

    if-lez v0, :cond_1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mScreenH:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mScreenH:I

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mRatioBitmapH:I

    div-int/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mScreenH:I

    sub-int/2addr v1, v0

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mMaxDeltaY:F

    div-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mDeltaY:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mRectTB:Landroid/graphics/Rect;

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mScreenW:I

    add-int/lit8 v3, v3, -0xa

    add-int/lit8 v3, v3, -0xa

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mScreenW:I

    add-int/lit8 v4, v4, -0xa

    add-int/2addr v0, v1

    invoke-virtual {v2, v3, v1, v4, v0}, Landroid/graphics/Rect;->set(IIII)V

    :cond_1
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mEnable:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mShow:Z

    invoke-virtual {p0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->removeMessages(I)V

    const-wide/16 v0, 0x12c

    invoke-virtual {p0, v6, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->sendEmptyMessageDelayed(IJ)Z

    :cond_2
    return-void
.end method

.method public setRatioBitmapSize(II)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mRatioBitmapW:I

    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mRatioBitmapH:I

    return-void
.end method

.method public setScreenSize(II)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mScreenW:I

    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->mScreenH:I

    return-void
.end method
