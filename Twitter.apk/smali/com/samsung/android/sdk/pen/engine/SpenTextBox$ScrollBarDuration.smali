.class Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;
.super Landroid/os/Handler;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final mTextBox:Ljava/lang/ref/WeakReference;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 1

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;->mTextBox:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;->mTextBox:Ljava/lang/ref/WeakReference;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;->mTextBox:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$26(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->invalidate()V

    invoke-virtual {p0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$ScrollBarDuration;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0
.end method
