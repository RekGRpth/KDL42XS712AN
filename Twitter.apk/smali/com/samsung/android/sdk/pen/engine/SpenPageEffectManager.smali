.class Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;


# instance fields
.field private mHandler:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

.field private mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;)V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;)V

    aput-object v1, v0, v3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    const/4 v1, 0x1

    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;

    invoke-direct {v2, p1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;)V

    aput-object v2, v0, v1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    aget-object v0, v0, v3

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mHandler:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    return-void
.end method


# virtual methods
.method public close()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    aget-object v0, v0, v2

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;->close()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    aput-object v1, v0, v2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    aget-object v0, v0, v3

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;->close()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    aput-object v1, v0, v3

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    :cond_0
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mHandler:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    return-void
.end method

.method public drawAnimation(Landroid/graphics/Canvas;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mHandler:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;->drawAnimation(Landroid/graphics/Canvas;)V

    return-void
.end method

.method public isWorking()Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mHandler:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;->isWorking()Z

    move-result v0

    return v0
.end method

.method public saveScreenshot()Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mHandler:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;->saveScreenshot()Z

    move-result v0

    return v0
.end method

.method public setCanvasInformation(IIII)V
    .locals 4

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-lt v0, v2, :cond_0

    return-void

    :cond_0
    aget-object v3, v1, v0

    invoke-interface {v3, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;->setCanvasInformation(IIII)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setPaint(Landroid/graphics/Paint;)V
    .locals 4

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-lt v0, v2, :cond_0

    return-void

    :cond_0
    aget-object v3, v1, v0

    invoke-interface {v3, p1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;->setPaint(Landroid/graphics/Paint;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setScreenResolution(II)V
    .locals 4

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-lt v0, v2, :cond_0

    return-void

    :cond_0
    aget-object v3, v1, v0

    invoke-interface {v3, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;->setScreenResolution(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public setType(I)V
    .locals 1

    if-gez p1, :cond_1

    const/4 p1, 0x0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    aget-object v0, v0, p1

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mHandler:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    array-length v0, v0

    if-lt p1, v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mPageEffectHandler:[Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    array-length v0, v0

    add-int/lit8 p1, v0, -0x1

    goto :goto_0
.end method

.method public startAnimation(I)Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->mHandler:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;->startAnimation(I)Z

    move-result v0

    return v0
.end method
