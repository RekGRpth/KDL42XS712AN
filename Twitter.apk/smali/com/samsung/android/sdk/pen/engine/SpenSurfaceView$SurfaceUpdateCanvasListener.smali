.class Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateCanvasListener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onUpdateCanvas(Landroid/graphics/RectF;Z)V
    .locals 13

    const/4 v3, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->GetPageEffectWorking()Z

    move-result v0

    if-nez v0, :cond_c

    if-eqz p1, :cond_b

    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->GetScreenWidth()I

    move-result v2

    int-to-float v2, v2

    cmpl-float v0, v0, v2

    if-nez v0, :cond_2

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->GetScreenHeight()I

    move-result v2

    int-to-float v2, v2

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_b

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->GetRtoBmpItstScrWidth()I

    move-result v0

    int-to-float v0, v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->GetRtoBmpItstScrHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1, v3, v3, v0, v2}, Landroid/graphics/RectF;->intersect(FFFF)Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p2, :cond_b

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getZoomRatio()F

    move-result v0

    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_b

    move-object v2, v1

    :goto_1
    if-eqz v2, :cond_a

    new-instance v0, Landroid/graphics/Rect;

    iget v3, v2, Landroid/graphics/RectF;->left:F

    float-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->floor(D)D

    move-result-wide v3

    double-to-int v3, v3

    iget v4, v2, Landroid/graphics/RectF;->top:F

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v4, v4

    iget v5, v2, Landroid/graphics/RectF;->right:F

    float-to-double v5, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v5

    double-to-int v5, v5

    iget v6, v2, Landroid/graphics/RectF;->bottom:F

    float-to-double v6, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v6, v6

    invoke-direct {v0, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->GetScreenWidth()I

    move-result v4

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-nez v3, :cond_3

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->GetScreenHeight()I

    move-result v4

    int-to-float v4, v4

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_4

    :cond_3
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->GetScreenStartX()I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->GetScreenStartY()I

    move-result v4

    invoke-virtual {v0, v3, v4}, Landroid/graphics/Rect;->offset(II)V

    :cond_4
    move-object v3, v0

    :goto_2
    if-eqz v3, :cond_9

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v3}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    :goto_3
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v6

    monitor-enter v6

    :try_start_0
    invoke-interface {v6, v3}, Landroid/view/SurfaceHolder;->lockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;

    move-result-object v7

    if-nez v7, :cond_5

    const-string/jumbo v0, "SpenSurfaceView"

    const-string/jumbo v1, "Performance onUpdateCanvas lockCanvas return null"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v6

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_5
    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    if-eqz v3, :cond_8

    iget v10, v3, Landroid/graphics/Rect;->left:I

    iget v11, v0, Landroid/graphics/Rect;->left:I

    if-ne v10, v11, :cond_6

    iget v10, v3, Landroid/graphics/Rect;->top:I

    iget v11, v0, Landroid/graphics/Rect;->top:I

    if-ne v10, v11, :cond_6

    iget v10, v3, Landroid/graphics/Rect;->right:I

    iget v11, v0, Landroid/graphics/Rect;->right:I

    if-ne v10, v11, :cond_6

    iget v10, v3, Landroid/graphics/Rect;->bottom:I

    iget v11, v0, Landroid/graphics/Rect;->bottom:I

    if-eq v10, v11, :cond_8

    :cond_6
    const-string/jumbo v2, "SpenSurfaceView"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "Performance lockCanvas warning: originDirtyRect ("

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v11, v0, Landroid/graphics/Rect;->left:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, ", "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, ") ("

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, v0, Landroid/graphics/Rect;->right:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, ", "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, ") w = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " h = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v0, "SpenSurfaceView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "Performance lockCanvas warning: outDirtyRect ("

    invoke-direct {v2, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v10, v3, Landroid/graphics/Rect;->left:I

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v10, ", "

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v10, v3, Landroid/graphics/Rect;->top:I

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v10, ") ("

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v10, v3, Landroid/graphics/Rect;->right:I

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v10, ", "

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v10, v3, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v10, ") w = "

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v10

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v10, " h = "

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move-result-object v0

    invoke-virtual {v0, v7, v1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->UpdateCanvas(Landroid/graphics/Canvas;Landroid/graphics/RectF;Z)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    if-eqz v7, :cond_7

    invoke-interface {v6, v7}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    :cond_7
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const-string/jumbo v7, "SpenSurfaceView"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "Performance total = "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v11

    sub-long/2addr v11, v4

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " ms lockCanvasTime = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sub-long v4, v8, v4

    invoke-virtual {v10, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " ms drawTime = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sub-long v8, v0, v8

    invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " ms unlcokCanvasTime = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sub-long v0, v2, v0

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " ms"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :cond_8
    move-object v1, v2

    goto :goto_4

    :cond_9
    move-object v0, v1

    goto/16 :goto_3

    :cond_a
    move-object v3, v1

    goto/16 :goto_2

    :cond_b
    move-object v2, p1

    goto/16 :goto_1

    :cond_c
    move-object v3, v1

    move-object v2, p1

    goto/16 :goto_2
.end method
