.class Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnPreviewCallback;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public OnPreview()V
    .locals 7

    const/4 v5, 0x0

    const v4, 0x38d1b717    # 1.0E-4f

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$18(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->getCamera()Landroid/hardware/Camera;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getPreviewSize()Landroid/hardware/Camera$Size;

    move-result-object v2

    if-nez v2, :cond_1

    const-string/jumbo v0, "SpenStrokeFrame"

    const-string/jumbo v1, "Camera PreviewSize is null"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpg-float v3, v3, v4

    if-ltz v3, :cond_0

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v3

    cmpg-float v3, v3, v4

    if-ltz v3, :cond_0

    iget v3, v2, Landroid/hardware/Camera$Size;->width:I

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    int-to-float v3, v3

    cmpg-float v3, v3, v4

    if-ltz v3, :cond_0

    iget v3, v2, Landroid/hardware/Camera$Size;->height:I

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    int-to-float v3, v3

    cmpg-float v3, v3, v4

    if-ltz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mActivity:Landroid/app/Activity;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$19(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_4

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget v4, v2, Landroid/hardware/Camera$Size;->width:I

    int-to-float v4, v4

    iget v2, v2, Landroid/hardware/Camera$Size;->height:I

    int-to-float v2, v2

    div-float v2, v4, v2

    invoke-static {v3, v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$20(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;F)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPreviewRatio:F
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$21(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)F

    move-result v2

    div-float v3, v1, v0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_3

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPreviewRatio:F
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$21(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)F

    move-result v2

    div-float v3, v0, v1

    mul-float/2addr v2, v3

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v1, v1

    :goto_1
    cmpl-float v2, v0, v1

    if-lez v2, :cond_5

    :cond_2
    :goto_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    float-to-int v1, v1

    float-to-int v0, v0

    invoke-direct {v3, v1, v0}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-static {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$22(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;Landroid/widget/RelativeLayout$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$23(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/widget/RelativeLayout$LayoutParams;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v1

    iget v1, v1, Landroid/graphics/RectF;->left:F

    float-to-int v1, v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/RectF;->top:F

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$18(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$23(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/widget/RelativeLayout$LayoutParams;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    :cond_3
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPreviewRatio:F
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$21(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)F

    move-result v2

    div-float v3, v0, v1

    mul-float/2addr v2, v3

    div-float/2addr v0, v2

    float-to-int v0, v0

    int-to-float v0, v0

    goto :goto_1

    :cond_4
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget v4, v2, Landroid/hardware/Camera$Size;->height:I

    int-to-float v4, v4

    iget v2, v2, Landroid/hardware/Camera$Size;->width:I

    int-to-float v2, v2

    div-float v2, v4, v2

    invoke-static {v3, v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$20(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;F)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPreviewRatio:F
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$21(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)F

    move-result v2

    div-float v3, v1, v0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_6

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPreviewRatio:F
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$21(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)F

    move-result v2

    div-float v3, v0, v1

    mul-float/2addr v2, v3

    mul-float/2addr v1, v2

    float-to-int v1, v1

    int-to-float v1, v1

    :goto_3
    cmpg-float v2, v0, v1

    if-ltz v2, :cond_2

    :cond_5
    move v6, v1

    move v1, v0

    move v0, v6

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPreviewRatio:F
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$21(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)F

    move-result v2

    div-float v3, v0, v1

    mul-float/2addr v2, v3

    div-float/2addr v0, v2

    float-to-int v0, v0

    int-to-float v0, v0

    goto :goto_3
.end method
