.class Lcom/samsung/android/sdk/pen/engine/SpenInView;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;


# static fields
.field private static final DBG:Z = false

.field public static final PAGE_TRANSITION_EFFECT_LEFT:I = 0x0

.field public static final PAGE_TRANSITION_EFFECT_RIGHT:I = 0x1

.field public static final PAGE_TRANSITION_EFFECT_TYPE_SHADOW:I = 0x0

.field public static final PAGE_TRANSITION_EFFECT_TYPE_SLIDE:I = 0x1

.field public static final REPLAY_STATE_PAUSED:I = 0x2

.field public static final REPLAY_STATE_PLAYING:I = 0x1

.field public static final REPLAY_STATE_STOPPED:I = 0x0

.field private static final STROKE_FRAME_KEY:Ljava/lang/String; = "STROKE_FRAME"

.field private static final STROKE_FRAME_RETAKE:I = 0x2

.field private static final STROKE_FRAME_TAKE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "SpenInView"


# instance fields
.field private isEraserCursor:Z

.field private isSkipTouch:Z

.field private mAntiAliasPaint:Landroid/graphics/Paint;

.field private mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

.field private mBitmapHeight:I

.field private mBitmapWidth:I

.field private mBlackPaint:Landroid/graphics/Paint;

.field private mCanvasLayer:Ljava/util/ArrayList;

.field private mCirclePaint:Landroid/graphics/Paint;

.field private mCirclePoint:Landroid/graphics/PointF;

.field private mCircleRadius:F

.field private mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

.field private mContext:Landroid/content/Context;

.field private mContextMenu:Landroid/view/ContextMenu;

.field private mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

.field private mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

.field private mDebugPaint:Landroid/graphics/Paint;

.field private mDeltaX:F

.field private mDeltaY:F

.field private mDetachReceiver:Lcom/samsung/android/sdk/pen/engine/SpenInView$DetachReceiver;

.field private mDottedLineEnable:Z

.field private mDottedLineHalfWidth:F

.field private mDottedLineIntervalHeight:I

.field private mDottedLinePaint:Landroid/graphics/Paint;

.field private mDstDrawRect:Landroid/graphics/Rect;

.field private mEraserChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

.field private mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

.field private mFlickListener:Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

.field private mFloatingLayer:Landroid/graphics/Bitmap;

.field private mFramebufferHeight:I

.field private mFramebufferWidth:I

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mHighlightInfoList:Ljava/util/ArrayList;

.field private mHighlightPaint:Landroid/graphics/Paint;

.field private mHoverDrawable:Landroid/graphics/drawable/Drawable;

.field private mHoverEnable:Z

.field private mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

.field private mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

.field public mHoverPointerlistener:Landroid/view/View$OnHoverListener;

.field private mHyperTextListener:Lcom/samsung/android/sdk/pen/engine/SpenHyperTextListener;

.field private mIsControlSelect:Z

.field private mIsHyperText:Z

.field private mIsSmartHorizontal:Z

.field private mIsSmartScale:Z

.field private mIsSmartVertical:Z

.field private mIsToolTip:Z

.field private mLongPressListener:Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;

.field private mMagicPenEnabled:Z

.field private mMaxDeltaX:F

.field private mMaxDeltaY:F

.field private mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

.field private mPageEffectListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

.field private mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

.field private mParentView:Landroid/view/ViewGroup;

.field private mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

.field private mPenDetachmentListener:Lcom/samsung/android/sdk/pen/engine/SpenPenDetachmentListener;

.field private mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

.field private mPostDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

.field private mPreDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

.field private mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

.field private mPreZoomable:Z

.field private mRatio:F

.field private mRatioBitmapHeight:I

.field private mRatioBitmapWidth:I

.field private mRemoverChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenRemoverChangeListener;

.field private mRemoverRadius:F

.field private mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

.field private mRemoverToastMessage:Landroid/widget/Toast;

.field private mReplayListener:Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;

.field private mRtoBmpItstScrHeight:I

.field private mRtoBmpItstScrWidth:I

.field private mScreenFB:Landroid/graphics/Bitmap;

.field private mScreenHeight:I

.field private mScreenHeightExceptSIP:I

.field private mScreenStartX:I

.field private mScreenStartY:I

.field private mScreenWidth:I

.field private mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

.field private mSdkResources:Landroid/content/res/Resources;

.field private mSelectionChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenSelectionChangeListener;

.field private mSelectionSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

.field private mSetPageDocHandler:Landroid/os/Handler;

.field private mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

.field private mSpenZoomPad:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;

.field private mSrcDrawRect:Landroid/graphics/Rect;

.field private mSrcPaint:Landroid/graphics/Paint;

.field private mStrokeFrame:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

.field private mStrokeFrameType:I

.field private mTextChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;

.field private mTextPaint:Landroid/graphics/Paint;

.field private mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

.field private mThisStrokeFrameListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

.field private mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

.field private mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

.field private mTouchProcessingTime:J

.field private mTouchUpHandler:Landroid/os/Handler;

.field private mTransactionClosingControl:Z

.field private mUpdateCanvasListener:Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateCanvasListener;

.field private mUpdateStrokeFrameListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

.field private mUseC2D:Z

.field private mView:Landroid/view/View;

.field private mZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

.field private mZoomPadListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomPadListener;

.field private mZoomable:Z

.field private nativeCanvas:I

.field private removerTouchX:F

.field private removerTouchY:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateCanvasListener;Z)V
    .locals 7

    const/high16 v6, -0x3d380000    # -100.0f

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContextMenu:Landroid/view/ContextMenu;

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeightExceptSIP:I

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatioBitmapWidth:I

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatioBitmapHeight:I

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFramebufferWidth:I

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFramebufferHeight:I

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcDrawRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDstDrawRect:Landroid/graphics/Rect;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSdkResources:Landroid/content/res/Resources;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFloatingLayer:Landroid/graphics/Bitmap;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenFB:Landroid/graphics/Bitmap;

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMagicPenEnabled:Z

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenRemoverChangeListener;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPostDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFlickListener:Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHyperTextListener:Lcom/samsung/android/sdk/pen/engine/SpenHyperTextListener;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLongPressListener:Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mReplayListener:Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenDetachmentListener:Lcom/samsung/android/sdk/pen/engine/SpenPenDetachmentListener;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSelectionChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenSelectionChangeListener;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mThisStrokeFrameListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateStrokeFrameListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mZoomPadListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomPadListener;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateCanvasListener:Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateCanvasListener;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSelectionSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightInfoList:Ljava/util/ArrayList;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDetachReceiver:Lcom/samsung/android/sdk/pen/engine/SpenInView$DetachReceiver;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mGestureDetector:Landroid/view/GestureDetector;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverDrawable:Landroid/graphics/drawable/Drawable;

    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverEnable:Z

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsToolTip:Z

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSkipTouch:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchProcessingTime:J

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaX:F

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaY:F

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBlackPaint:Landroid/graphics/Paint;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightPaint:Landroid/graphics/Paint;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDebugPaint:Landroid/graphics/Paint;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcPaint:Landroid/graphics/Paint;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mAntiAliasPaint:Landroid/graphics/Paint;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextPaint:Landroid/graphics/Paint;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePaint:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v6, v6}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCircleRadius:F

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineEnable:Z

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineIntervalHeight:I

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineHalfWidth:F

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mZoomable:Z

    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsSmartScale:Z

    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsSmartHorizontal:Z

    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsSmartVertical:Z

    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsHyperText:Z

    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsControlSelect:Z

    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTransactionClosingControl:Z

    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreZoomable:Z

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mView:Landroid/view/View;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mParentView:Landroid/view/ViewGroup;

    iput v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStrokeFrameType:I

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverToastMessage:Landroid/widget/Toast;

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->removerTouchX:F

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->removerTouchY:F

    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isEraserCursor:Z

    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUseC2D:Z

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView$1;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPointerlistener:Landroid/view/View$OnHoverListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$TouchUpHandler;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView$TouchUpHandler;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchUpHandler:Landroid/os/Handler;

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$SetPageDocHandler;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView$SetPageDocHandler;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSetPageDocHandler:Landroid/os/Handler;

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_init()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    iput-object p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateCanvasListener:Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateCanvasListener;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->construct()V

    const-string/jumbo v0, "/system/vendor/lib/libC2D2.so"

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUseC2D:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p3, :cond_0

    :try_start_0
    invoke-static {}, Lcom/C2Ddrawbitmap/c2ddrawbitmapJNI;->native_init_c2dJNI()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUseC2D:Z

    const-string/jumbo v0, "c2d"

    const-string/jumbo v1, "c2d blit init"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    :catch_0
    move-exception v0

    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUseC2D:Z

    const-string/jumbo v0, "c2d"

    const-string/jumbo v1, "c2d blit init fail, lib loading error"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUseC2D:Z

    const-string/jumbo v0, "c2d"

    const-string/jumbo v1, "c2d blit init fail, lib loading error"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private ExtendRectFromRectF(Landroid/graphics/Rect;Landroid/graphics/RectF;)V
    .locals 2

    iget v0, p2, Landroid/graphics/RectF;->left:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->left:I

    iget v0, p2, Landroid/graphics/RectF;->top:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->top:I

    iget v0, p2, Landroid/graphics/RectF;->right:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->right:I

    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    return-void
.end method

.method private MakeNewExtendRect(Landroid/graphics/RectF;)Landroid/graphics/Rect;
    .locals 6

    new-instance v0, Landroid/graphics/Rect;

    iget v1, p1, Landroid/graphics/RectF;->left:F

    float-to-double v1, v1

    invoke-static {v1, v2}, Ljava/lang/Math;->floor(D)D

    move-result-wide v1

    double-to-int v1, v1

    iget v2, p1, Landroid/graphics/RectF;->top:F

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v2, v2

    iget v3, p1, Landroid/graphics/RectF;->right:F

    float-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v3

    double-to-int v3, v3

    iget v4, p1, Landroid/graphics/RectF;->bottom:F

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v0
.end method

.method private absoluteCoordinate(Landroid/graphics/Rect;FFFF)V
    .locals 2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    div-float v0, p2, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->left:I

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    div-float v0, p4, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->right:I

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    div-float v0, p3, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->top:I

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    div-float v0, p5, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    return-void
.end method

.method private absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 2

    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    div-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->left:F

    iget v0, p2, Landroid/graphics/RectF;->right:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    div-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->right:F

    iget v0, p2, Landroid/graphics/RectF;->top:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    div-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    div-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    add-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/engine/SpenInView;Landroid/graphics/drawable/Drawable;)I
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setCustomHoveringIcon(Landroid/graphics/drawable/Drawable;)I

    move-result v0

    return v0
.end method

.method static synthetic access$10(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    return v0
.end method

.method static synthetic access$11(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    return v0
.end method

.method static synthetic access$12(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    return v0
.end method

.method static synthetic access$13(ILandroid/graphics/PointF;)V
    .locals 0

    invoke-static {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getPan(ILandroid/graphics/PointF;)V

    return-void
.end method

.method static synthetic access$14(IFFZ)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setPan(IFFZ)V

    return-void
.end method

.method static synthetic access$15(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$16(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenPenDetachmentListener;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenDetachmentListener:Lcom/samsung/android/sdk/pen/engine/SpenPenDetachmentListener;

    return-object v0
.end method

.method static synthetic access$17(ILandroid/view/MotionEvent;I)Z
    .locals 1

    invoke-static {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_onSingleTapUp(ILandroid/view/MotionEvent;I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$18(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSkipTouch:Z

    return v0
.end method

.method static synthetic access$19(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLongPressListener:Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    return-object v0
.end method

.method static synthetic access$20(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isEraserCursor:Z

    return v0
.end method

.method static synthetic access$21(Lcom/samsung/android/sdk/pen/engine/SpenInView;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isEraserCursor:Z

    return-void
.end method

.method static synthetic access$22(Lcom/samsung/android/sdk/pen/engine/SpenInView;F)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCircleRadius:F

    return-void
.end method

.method static synthetic access$23(Lcom/samsung/android/sdk/pen/engine/SpenInView;F)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    return-void
.end method

.method static synthetic access$24(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/graphics/PointF;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    return-object v0
.end method

.method static synthetic access$25(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    return-object v0
.end method

.method static synthetic access$26(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsSmartScale:Z

    return v0
.end method

.method static synthetic access$27(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsSmartHorizontal:Z

    return v0
.end method

.method static synthetic access$28(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsSmartVertical:Z

    return v0
.end method

.method static synthetic access$29(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    return v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    return-object v0
.end method

.method static synthetic access$30(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    return v0
.end method

.method static synthetic access$31(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    return v0
.end method

.method static synthetic access$32(ILandroid/view/MotionEvent;I)Z
    .locals 1

    invoke-static {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_onLongPress(ILandroid/view/MotionEvent;I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$33(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatioBitmapWidth:I

    return v0
.end method

.method static synthetic access$34(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    return v0
.end method

.method static synthetic access$35(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    return v0
.end method

.method static synthetic access$36(Lcom/samsung/android/sdk/pen/engine/SpenInView;Landroid/graphics/RectF;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->onUpdateCanvas(Landroid/graphics/RectF;Z)V

    return-void
.end method

.method static synthetic access$37(Lcom/samsung/android/sdk/pen/engine/SpenInView;Landroid/graphics/Canvas;Landroid/graphics/RectF;Z)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateCanvasLayer(Landroid/graphics/Canvas;Landroid/graphics/RectF;Z)V

    return-void
.end method

.method static synthetic access$38(Lcom/samsung/android/sdk/pen/engine/SpenInView;Landroid/graphics/Canvas;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateCanvasLayer2(Landroid/graphics/Canvas;)V

    return-void
.end method

.method static synthetic access$39(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSetPageDocHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/engine/SpenInView;I)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->removeHoveringIcon(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$40(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

    return-object v0
.end method

.method static synthetic access$41(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    return-object v0
.end method

.method static synthetic access$42(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFlickListener:Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

    return-object v0
.end method

.method static synthetic access$43(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;

    return-object v0
.end method

.method static synthetic access$44(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSpenZoomPad:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;

    return-object v0
.end method

.method static synthetic access$45(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    return v0
.end method

.method static synthetic access$46(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeightExceptSIP:I

    return v0
.end method

.method static synthetic access$47(IIII)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setScreenSize(IIII)V

    return-void
.end method

.method static synthetic access$48(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreZoomable:Z

    return v0
.end method

.method static synthetic access$49(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenZoomPadListener;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mZoomPadListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomPadListener;

    return-object v0
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    return-void
.end method

.method static synthetic access$50(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStrokeFrame:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    return-void
.end method

.method static synthetic access$51(Lcom/samsung/android/sdk/pen/engine/SpenInView;I)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStrokeFrameType:I

    return-void
.end method

.method static synthetic access$52(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateStrokeFrameListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    return-object v0
.end method

.method static synthetic access$53(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->raiseOnKeyDown()V

    return-void
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenControlListener;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    return-object v0
.end method

.method static synthetic access$7(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    return v0
.end method

.method static synthetic access$8(IIZZ)V
    .locals 0

    invoke-static {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_inVisibleUpdate(IIZZ)V

    return-void
.end method

.method static synthetic access$9(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    return v0
.end method

.method private applyTextSetting(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V
    .locals 10

    const/4 v3, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    :goto_0
    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getSpan()Ljava/util/ArrayList;

    move-result-object v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    if-nez v2, :cond_0

    new-instance v2, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    invoke-direct {v2}, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v4, v2, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    float-to-double v4, v4

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getCanvasWidth()I

    move-result v6

    int-to-double v6, v6

    const-wide/high16 v8, 0x4069000000000000L    # 200.0

    div-double/2addr v6, v8

    mul-double/2addr v4, v6

    double-to-float v4, v4

    iput v4, v2, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    invoke-direct {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;-><init>()V

    iput v1, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->startPos:I

    iput v0, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->endPos:I

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v5, v5, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iput v5, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;

    invoke-direct {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;-><init>()V

    iput v1, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;->startPos:I

    iput v0, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;->endPos:I

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v5, v5, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    iput v5, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;->foregroundColor:I

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v5, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BoldStyleSpanInfo;

    invoke-direct {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BoldStyleSpanInfo;-><init>()V

    iput v1, v5, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BoldStyleSpanInfo;->startPos:I

    iput v0, v5, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BoldStyleSpanInfo;->endPos:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    and-int/lit8 v2, v2, 0x1

    if-ne v2, v3, :cond_4

    move v2, v3

    :goto_1
    iput-boolean v2, v5, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$BoldStyleSpanInfo;->isBold:Z

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v5, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;

    invoke-direct {v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;-><init>()V

    iput v1, v5, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;->startPos:I

    iput v0, v5, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;->endPos:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    and-int/lit8 v2, v2, 0x2

    const/4 v6, 0x2

    if-ne v2, v6, :cond_5

    move v2, v3

    :goto_2
    iput-boolean v2, v5, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ItalicStyleSpanInfo;->isItalic:Z

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$UnderlineStyleSpanInfo;

    invoke-direct {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$UnderlineStyleSpanInfo;-><init>()V

    iput v1, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$UnderlineStyleSpanInfo;->startPos:I

    iput v0, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$UnderlineStyleSpanInfo;->endPos:I

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v5, v5, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    and-int/lit8 v5, v5, 0x4

    const/4 v6, 0x4

    if-ne v5, v6, :cond_6

    :goto_3
    iput-boolean v3, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$UnderlineStyleSpanInfo;->isUnderline:Z

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;

    invoke-direct {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;-><init>()V

    iput v1, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;->startPos:I

    iput v0, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;->endPos:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    iput-object v3, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;->fontName:Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;

    invoke-direct {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;-><init>()V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v3, v3, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    int-to-char v3, v3

    iput v3, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;->textDirection:I

    iput v1, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;->startPos:I

    iput v0, v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;->endPos:I

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1, v4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setSpan(Ljava/util/ArrayList;)V

    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getParagraph()Ljava/util/ArrayList;

    move-result-object v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    if-nez v2, :cond_2

    new-instance v2, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    invoke-direct {v2}, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    :cond_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;

    invoke-direct {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;-><init>()V

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    int-to-char v4, v4

    iput v4, v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;->align:I

    iput v1, v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;->startPos:I

    iput v0, v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;->endPos:I

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    invoke-direct {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;-><init>()V

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacingType:I

    iput v4, v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->type:I

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    iput v4, v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->lineSpacing:F

    iput v1, v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->startPos:I

    iput v0, v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->endPos:I

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setParagraph(Ljava/util/ArrayList;)V

    :cond_3
    return-void

    :cond_4
    move v2, v1

    goto/16 :goto_1

    :cond_5
    move v2, v1

    goto/16 :goto_2

    :cond_6
    move v3, v1

    goto :goto_3

    :cond_7
    move v0, v1

    goto/16 :goto_0
.end method

.method private checkMDMCameraLock()Z
    .locals 9

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    :try_start_0
    const-string/jumbo v0, "android.os.SystemProperties"

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v5

    invoke-static {v0, v4, v5}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v0

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    new-instance v7, Ljava/lang/String;

    const-string/jumbo v8, "persist.sys.camera_lock"

    invoke-direct {v7, v8}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    aput-object v7, v5, v6

    const-string/jumbo v6, "get"

    invoke-virtual {v0, v6, v4}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v4, 0x0

    invoke-virtual {v0, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_4

    :goto_0
    if-nez v0, :cond_0

    move v0, v1

    :goto_1
    return v0

    :catch_0
    move-exception v0

    move-object v0, v3

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v0, v3

    goto :goto_0

    :catch_2
    move-exception v0

    move-object v0, v3

    goto :goto_0

    :catch_3
    move-exception v0

    move-object v0, v3

    goto :goto_0

    :catch_4
    move-exception v0

    move-object v0, v3

    goto :goto_0

    :cond_0
    const-string/jumbo v3, "camera_lock.enabled"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private construct()V
    .locals 7

    const/16 v6, 0x8

    const v5, -0xf2c5b1

    const/4 v4, 0x0

    const/4 v3, 0x1

    const-string/jumbo v0, "SpenInView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "nativeCanvas = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    const-string/jumbo v0, " : nativeCanvas must not be null"

    invoke-static {v6, v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    if-nez v0, :cond_1

    const-string/jumbo v0, " : context must not be null"

    invoke-static {v6, v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0

    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string/jumbo v1, "com.samsung.android.sdk.spen30"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSdkResources:Landroid/content/res/Resources;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    invoke-static {v1, v2, p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_construct(ILandroid/content/Context;Lcom/samsung/android/sdk/pen/engine/SpenInView;Landroid/graphics/RectF;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    :cond_2
    new-instance v0, Landroid/view/GestureDetector;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;

    invoke-direct {v2, p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;)V

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mGestureDetector:Landroid/view/GestureDetector;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mGestureDetector:Landroid/view/GestureDetector;

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;

    invoke-direct {v1, p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;)V

    invoke-virtual {v0, v1}, Landroid/view/GestureDetector;->setOnDoubleTapListener(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v0, v0

    invoke-direct {v1, v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;-><init>(Landroid/content/Context;F)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnSmartScaleGestureDetectorListener;

    invoke-direct {v1, p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnSmartScaleGestureDetectorListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenInView$OnSmartScaleGestureDetectorListener;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->setListener(Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector$Listener;)V

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnScrollListener;

    invoke-direct {v1, p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnScrollListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenInView$OnScrollListener;)V

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll$Listener;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBlackPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBlackPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDebugPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDebugPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDebugPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDebugPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x3fc00000    # 1.5f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDebugPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mAntiAliasPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnPageEffectListener;

    invoke-direct {v1, p0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnPageEffectListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenInView$OnPageEffectListener;)V

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->setPaint(Landroid/graphics/Paint;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setEraserSize(IF)V

    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setRemoverSize(IF)V

    new-instance v0, Landroid/widget/Toast;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/Toast;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverToastMessage:Landroid/widget/Toast;

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView$2;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mThisStrokeFrameListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    goto/16 :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto/16 :goto_1
.end method

.method private createBitmap(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V
    .locals 4

    const/4 v3, 0x6

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getWidth()I

    move-result v2

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getHeight()I

    move-result v2

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    if-nez v2, :cond_2

    const-string/jumbo v0, "The width of pageDoc is 0"

    invoke-static {v3, v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0

    :cond_2
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    if-nez v2, :cond_3

    const-string/jumbo v0, "The height of pageDoc is 0"

    invoke-static {v3, v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0

    :cond_3
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    if-ne v0, v2, :cond_4

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    if-ne v1, v0, :cond_4

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isLayerChanged()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_4
    const-string/jumbo v0, "SpenInView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "createBitmap Width="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " Height="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " IsLayerChanged="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isLayerChanged()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFloatingLayer:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFloatingLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFloatingLayer:Landroid/graphics/Bitmap;

    :cond_5
    :try_start_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFloatingLayer:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFloatingLayer:Landroid/graphics/Bitmap;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setBitmap(ILandroid/graphics/Bitmap;)V

    const-string/jumbo v0, "SpenInView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Added Layer Count="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getLayerCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", Layer Size="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_removeCanvasBitmap(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_6
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_7
    const/4 v0, 0x0

    :goto_3
    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getLayerCount()I

    move-result v1

    if-lt v0, v1, :cond_9

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->deltaZoomSizeChanged()V

    goto/16 :goto_0

    :catch_0
    move-exception v0

    const-string/jumbo v0, "SpenInView"

    const-string/jumbo v1, "Failed to create bitmap"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_1

    :cond_8
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_6

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_2

    :cond_9
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUseC2D:Z

    if-eqz v2, :cond_a

    if-nez v0, :cond_a

    invoke-static {v1}, Lcom/C2Ddrawbitmap/c2ddrawbitmapJNI;->native_reallocBitmapJNI(Landroid/graphics/Bitmap;)V

    :cond_a
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getLayerIdByIndex(I)I

    move-result v3

    invoke-static {v2, v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setCanvasBitmap(IILandroid/graphics/Bitmap;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_3
.end method

.method private deltaZoomSizeChanged()V
    .locals 5

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getVariableForOnUpdateCanvas()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->onZoom(FFF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaY:F

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->setLimitHeight(FF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->setDrawInformation(IIII)V

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatioBitmapWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatioBitmapHeight:I

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->setRatioBitmapSize(II)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaX:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaY:F

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->setDeltaValue(FFFF)V

    :cond_1
    const-string/jumbo v0, "SpenInView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onZoom. dx : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", dy : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", r : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", MaxDx : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaX:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", MaxDy : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaY:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private drawHintText(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)V
    .locals 6

    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObjectList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-lt v1, v3, :cond_0

    return-void

    :cond_0
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    if-nez v0, :cond_2

    :cond_1
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getType()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_3

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-direct {p0, p1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->drawHintText(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V

    goto :goto_1

    :cond_3
    const/4 v5, 0x3

    if-ne v4, v5, :cond_4

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    invoke-direct {p0, p1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->drawHintText(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/pen/document/SpenObjectImage;)V

    goto :goto_1

    :cond_4
    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    invoke-direct {p0, p1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->drawHintText(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)V

    goto :goto_1
.end method

.method private drawHintText(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/pen/document/SpenObjectImage;)V
    .locals 10

    const/4 v7, 0x0

    new-instance v8, Landroid/graphics/RectF;

    invoke-direct {v8}, Landroid/graphics/RectF;-><init>()V

    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->getHintText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->isHintTextEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->getRect()Landroid/graphics/RectF;

    move-result-object v0

    invoke-direct {p0, v8, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v0, v0

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v2, v2

    invoke-virtual {v8, v0, v2}, Landroid/graphics/RectF;->offset(FF)V

    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->getHintTextFontSize()F

    move-result v0

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float v6, v0, v2

    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->getHintTextVerticalOffset()F

    move-result v0

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float v9, v0, v2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    invoke-static {v2, v7}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->getHintTextColor()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setTextSize(F)V

    new-instance v0, Landroid/text/StaticLayout;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v7, v2}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    new-instance v2, Landroid/text/TextPaint;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextPaint:Landroid/graphics/Paint;

    invoke-direct {v2, v3}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    invoke-virtual {v8}, Landroid/graphics/RectF;->width()F

    move-result v3

    float-to-int v3, v3

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget v1, v8, Landroid/graphics/RectF;->left:F

    iget v2, v8, Landroid/graphics/RectF;->top:F

    add-float/2addr v2, v9

    invoke-virtual {v8}, Landroid/graphics/RectF;->height()F

    move-result v3

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getLineCount()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v6

    sub-float/2addr v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    add-float/2addr v2, v3

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    :cond_0
    return-void
.end method

.method private drawHintText(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V
    .locals 11

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v7, 0x0

    new-instance v8, Landroid/graphics/RectF;

    invoke-direct {v8}, Landroid/graphics/RectF;-><init>()V

    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getHintText()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->isHintTextEnabled()Z

    move-result v0

    if-nez v0, :cond_4

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v0

    invoke-direct {p0, v8, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v0, v0

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v2, v2

    invoke-virtual {v8, v0, v2}, Landroid/graphics/RectF;->offset(FF)V

    iget v0, v8, Landroid/graphics/RectF;->left:F

    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getLeftMargin()F

    move-result v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v2, v3

    add-float/2addr v0, v2

    iput v0, v8, Landroid/graphics/RectF;->left:F

    iget v0, v8, Landroid/graphics/RectF;->top:F

    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getTopMargin()F

    move-result v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v2, v3

    add-float/2addr v0, v2

    iput v0, v8, Landroid/graphics/RectF;->top:F

    iget v0, v8, Landroid/graphics/RectF;->right:F

    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRightMargin()F

    move-result v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v2, v3

    sub-float/2addr v0, v2

    iput v0, v8, Landroid/graphics/RectF;->right:F

    iget v0, v8, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getBottomMargin()F

    move-result v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v2, v3

    sub-float/2addr v0, v2

    iput v0, v8, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getHintTextFontSize()F

    move-result v0

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float v6, v0, v2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getFont()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getTypeFace(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getHintTextColor()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setTextSize(F)V

    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getTextAlignment()I

    move-result v0

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    if-eq v0, v10, :cond_1

    const/4 v2, 0x3

    if-ne v0, v2, :cond_5

    :cond_1
    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_CENTER:Landroid/text/Layout$Alignment;

    :cond_2
    :goto_0
    new-instance v0, Landroid/text/StaticLayout;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v7, v2}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    new-instance v2, Landroid/text/TextPaint;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextPaint:Landroid/graphics/Paint;

    invoke-direct {v2, v3}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    invoke-virtual {v8}, Landroid/graphics/RectF;->width()F

    move-result v3

    float-to-int v3, v3

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v7}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;Landroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZ)V

    const v1, 0x3e99999a    # 0.3f

    mul-float/2addr v1, v6

    invoke-virtual {p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getGravity()I

    move-result v2

    if-ne v2, v9, :cond_6

    invoke-virtual {v8}, Landroid/graphics/RectF;->height()F

    move-result v1

    sub-float/2addr v1, v6

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    :cond_3
    :goto_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget v2, v8, Landroid/graphics/RectF;->left:F

    iget v3, v8, Landroid/graphics/RectF;->top:F

    add-float/2addr v1, v3

    invoke-virtual {p1, v2, v1}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    :cond_4
    return-void

    :cond_5
    if-ne v0, v9, :cond_2

    sget-object v4, Landroid/text/Layout$Alignment;->ALIGN_OPPOSITE:Landroid/text/Layout$Alignment;

    goto :goto_0

    :cond_6
    if-ne v2, v10, :cond_3

    invoke-virtual {v8}, Landroid/graphics/RectF;->height()F

    move-result v1

    sub-float/2addr v1, v6

    goto :goto_1
.end method

.method private fitControlToObject()V
    .locals 3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getObjectList()Ljava/util/ArrayList;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isObjectContained(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->fit()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->invalidate()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->closeControl()V

    goto :goto_0
.end method

.method private getResourceString(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSdkResources:Landroid/content/res/Resources;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSdkResources:Landroid/content/res/Resources;

    const-string/jumbo v2, "string"

    const-string/jumbo v3, "com.samsung.android.sdk.spen30"

    invoke-virtual {v1, p1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSdkResources:Landroid/content/res/Resources;

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getVariableForOnUpdateCanvas()V
    .locals 5

    const/high16 v4, 0x40000000    # 2.0f

    const/4 v3, 0x0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    div-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaX:F

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaX:F

    cmpg-float v0, v0, v3

    if-gez v0, :cond_0

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaX:F

    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeightExceptSIP:I

    if-ge v0, v1, :cond_2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    :goto_0
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    int-to-float v1, v1

    int-to-float v0, v0

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    div-float/2addr v0, v2

    sub-float v0, v1, v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaY:F

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaY:F

    cmpg-float v0, v0, v3

    if-gez v0, :cond_1

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMaxDeltaY:F

    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatioBitmapWidth:I

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatioBitmapHeight:I

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatioBitmapWidth:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    if-ge v0, v1, :cond_3

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatioBitmapWidth:I

    :goto_1
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatioBitmapHeight:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    if-ge v0, v1, :cond_4

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatioBitmapHeight:I

    :goto_2
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    div-float/2addr v0, v4

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    div-float/2addr v0, v4

    float-to-int v0, v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    return-void

    :cond_2
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeightExceptSIP:I

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    goto :goto_1

    :cond_4
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    goto :goto_2
.end method

.method private isIntersect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z
    .locals 2

    iget v0, p1, Landroid/graphics/RectF;->left:F

    iget v1, p2, Landroid/graphics/RectF;->right:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget v1, p1, Landroid/graphics/RectF;->right:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    iget v0, p1, Landroid/graphics/RectF;->top:F

    iget v1, p2, Landroid/graphics/RectF;->bottom:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    iget v0, p2, Landroid/graphics/RectF;->top:F

    iget v1, p1, Landroid/graphics/RectF;->bottom:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static native native_cancelStroke(I)V
.end method

.method private static native native_command(IILjava/util/ArrayList;I)Ljava/util/ArrayList;
.end method

.method private static native native_construct(ILandroid/content/Context;Lcom/samsung/android/sdk/pen/engine/SpenInView;Landroid/graphics/RectF;)Z
.end method

.method private static native native_drawObjectList(ILandroid/graphics/Bitmap;Ljava/util/ArrayList;I)Z
.end method

.method private static native native_enablePenCurve(IZ)V
.end method

.method private static native native_enableZoom(IZ)V
.end method

.method private static native native_finalize(I)V
.end method

.method private static native native_getAdvancedSetting(I)Ljava/lang/String;
.end method

.method private static native native_getEraserSize(I)F
.end method

.method private static native native_getMaxZoomRatio(I)F
.end method

.method private static native native_getMinZoomRatio(I)F
.end method

.method private static native native_getPan(ILandroid/graphics/PointF;)V
.end method

.method private static native native_getPenColor(I)I
.end method

.method private static native native_getPenSize(I)F
.end method

.method private static native native_getPenStyle(I)Ljava/lang/String;
.end method

.method private static native native_getReplayState(I)I
.end method

.method private static native native_getTemporaryStroke(ILjava/util/ArrayList;)V
.end method

.method private static native native_getToolTypeAction(II)I
.end method

.method private static native native_getZoomRatio(I)F
.end method

.method private static native native_inVisibleUpdate(IIZZ)V
.end method

.method private static native native_init()I
.end method

.method private static native native_isPenCurve(I)Z
.end method

.method private static native native_isZoomable(I)Z
.end method

.method private static native native_onHover(ILandroid/view/MotionEvent;I)Z
.end method

.method private static native native_onLongPress(ILandroid/view/MotionEvent;I)Z
.end method

.method private static native native_onSingleTapUp(ILandroid/view/MotionEvent;I)Z
.end method

.method private static native native_onTouch(ILandroid/view/MotionEvent;I)Z
.end method

.method private static native native_pauseReplay(I)Z
.end method

.method private static native native_removeCanvasBitmap(I)V
.end method

.method private static native native_resumeReplay(I)Z
.end method

.method private static native native_setAdvancedSetting(ILjava/lang/String;)V
.end method

.method private static native native_setBitmap(ILandroid/graphics/Bitmap;)V
.end method

.method private static native native_setCanvasBitmap(IILandroid/graphics/Bitmap;)V
.end method

.method private static native native_setEraserSize(IF)V
.end method

.method private static native native_setEraserType(II)V
.end method

.method private static native native_setHyperTextViewEnabled(IZ)V
.end method

.method private static native native_setMaxZoomRatio(IF)Z
.end method

.method private static native native_setMinZoomRatio(IF)Z
.end method

.method private static native native_setPageDoc(ILcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z
.end method

.method private static native native_setPan(IFFZ)V
.end method

.method private static native native_setPenColor(II)V
.end method

.method private static native native_setPenSize(IF)V
.end method

.method private static native native_setPenStyle(ILjava/lang/String;)Z
.end method

.method private static native native_setRemoverSize(IF)V
.end method

.method private static native native_setRemoverType(II)V
.end method

.method private static native native_setReplayPosition(II)Z
.end method

.method private static native native_setReplaySpeed(II)Z
.end method

.method private static native native_setScreenFrameBuffer(ILandroid/graphics/Bitmap;)V
.end method

.method private static native native_setScreenSize(IIII)V
.end method

.method private static native native_setSelectionType(II)V
.end method

.method private static native native_setToolTypeAction(III)V
.end method

.method private static native native_setZoom(IFFF)V
.end method

.method private static native native_startReplay(I)Z
.end method

.method private static native native_startTemporaryStroke(I)V
.end method

.method private static native native_stopReplay(I)Z
.end method

.method private static native native_stopTemporaryStroke(I)V
.end method

.method private static native native_update(I)V
.end method

.method private static native native_updateAllScreenFrameBuffer(I)V
.end method

.method private static native native_updateRedo(I[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;I)Z
.end method

.method private static native native_updateUndo(I[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;I)Z
.end method

.method private onColorPickerChanged(III)V
    .locals 7

    const-string/jumbo v0, "SpenInView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onColorPickerChanged color"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    instance-of v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getPixel(II)I

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/graphics/Color;->alpha(I)I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x437f0000    # 255.0f

    div-float/2addr v1, v2

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr v2, v1

    const/16 v3, 0xff

    invoke-static {v0}, Landroid/graphics/Color;->red(I)I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v1

    invoke-static {p3}, Landroid/graphics/Color;->red(I)I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v2

    add-float/2addr v4, v5

    float-to-int v4, v4

    invoke-static {v0}, Landroid/graphics/Color;->green(I)I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v1

    invoke-static {p3}, Landroid/graphics/Color;->green(I)I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v2

    add-float/2addr v5, v6

    float-to-int v5, v5

    invoke-static {v0}, Landroid/graphics/Color;->blue(I)I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v1

    invoke-static {p3}, Landroid/graphics/Color;->blue(I)I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-int v0, v0

    invoke-static {v3, v4, v5, v0}, Landroid/graphics/Color;->argb(IIII)I

    move-result p3

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    invoke-interface {v0, p3, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;->onChanged(III)V

    :cond_1
    return-void
.end method

.method private onCompleted()V
    .locals 2

    const-string/jumbo v0, "SpenInView"

    const-string/jumbo v1, "onCompleted"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mReplayListener:Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mReplayListener:Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;->onCompleted()V

    :cond_0
    return-void
.end method

.method private onHyperText(Ljava/lang/String;II)V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHyperTextListener:Lcom/samsung/android/sdk/pen/engine/SpenHyperTextListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v0, p3}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getObjectByRuntimeHandle(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHyperTextListener:Lcom/samsung/android/sdk/pen/engine/SpenHyperTextListener;

    invoke-interface {v1, p1, p2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenHyperTextListener;->onSelected(Ljava/lang/String;ILcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V

    :cond_0
    return-void
.end method

.method private onProgressChanged(II)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mReplayListener:Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mReplayListener:Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;->onProgressChanged(II)V

    :cond_0
    return-void
.end method

.method private onSelectObject(Ljava/util/ArrayList;IIFFI)Z
    .locals 11

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v1, :cond_0

    if-nez p3, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isTouchEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->closeControl()V

    :cond_2
    if-nez p1, :cond_3

    const-string/jumbo v1, "SpenInView"

    const-string/jumbo v2, "onSelectObject ObjectList is nulll"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_4

    const-string/jumbo v1, "SpenInView"

    const-string/jumbo v2, "onSelectObject : selected list size is zero."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    goto :goto_0

    :cond_4
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    new-instance v7, Landroid/graphics/PointF;

    move/from16 v0, p5

    invoke-direct {v7, p4, v0}, Landroid/graphics/PointF;-><init>(FF)V

    new-instance v6, Landroid/graphics/RectF;

    invoke-direct {v6}, Landroid/graphics/RectF;-><init>()V

    new-instance v8, Landroid/graphics/Rect;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-direct {v8, v1, v2, v9, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt v2, v1, :cond_5

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_a

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v5, v1, v2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    move-object v2, p1

    move v6, p3

    invoke-interface/range {v1 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenControlListener;->onCreated(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;ILandroid/graphics/PointF;)Z

    move-result v1

    if-nez v1, :cond_8

    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_5
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    if-nez v1, :cond_6

    const-string/jumbo v1, "SpenInView"

    const-string/jumbo v2, "onSelectObject : object is null."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_6
    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v9, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isObjectContained(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z

    move-result v9

    if-nez v9, :cond_7

    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_7
    new-instance v9, Landroid/graphics/Rect;

    invoke-direct {v9}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getRect()Landroid/graphics/RectF;

    move-result-object v1

    invoke-direct {p0, v6, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    invoke-virtual {v6, v9}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    invoke-virtual {v3, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v8, v9}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :cond_8
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getSelectedObjectCount()I

    move-result v1

    if-nez v1, :cond_9

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v1, p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->selectObject(Ljava/util/ArrayList;)V

    :cond_9
    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-direct {v2, v1, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    const/4 v1, 0x0

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->setStyle(I)V

    invoke-virtual {v2, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->setContextMenu(Ljava/util/ArrayList;)V

    invoke-virtual {v2, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->setObject(Ljava/util/ArrayList;)V

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setControl(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)V

    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_a
    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    const/4 v6, 0x0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getSelectedObjectCount()I

    move-result v2

    if-nez v2, :cond_b

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v2, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->selectObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    :cond_b
    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getType()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    move-object v8, v6

    :goto_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    if-eqz v1, :cond_f

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    move-object v2, p1

    move v6, p3

    invoke-interface/range {v1 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenControlListener;->onCreated(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;ILandroid/graphics/PointF;)Z

    move-result v1

    if-nez v1, :cond_f

    const/4 v1, 0x1

    goto/16 :goto_0

    :pswitch_0
    const-string/jumbo v2, "SpenInView"

    const-string/jumbo v6, "Text Selection"

    invoke-static {v2, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x1

    move/from16 v0, p6

    if-ne v0, v2, :cond_c

    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-direct {v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setStyle(I)V

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setTextEraserEnabled(Z)V

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setObject(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V

    const/4 v1, 0x1

    invoke-virtual {v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setEditable(Z)V

    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setDimEnabled(Z)V

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setControl(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)V

    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_c
    const/4 v2, 0x1

    const/4 v6, 0x1

    invoke-virtual {p0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v6

    const/4 v8, 0x6

    if-eq v6, v8, :cond_d

    const/4 v6, 0x2

    invoke-virtual {p0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v6

    const/4 v8, 0x6

    if-eq v6, v8, :cond_d

    const/4 v6, 0x3

    invoke-virtual {p0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v6

    const/4 v8, 0x6

    if-eq v6, v8, :cond_d

    const/4 v6, 0x4

    invoke-virtual {p0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v6

    const/4 v8, 0x6

    if-eq v6, v8, :cond_d

    const/4 v6, 0x2

    if-ne p3, v6, :cond_11

    :cond_d
    const/4 v2, 0x0

    move v8, v2

    :goto_3
    if-eqz v8, :cond_e

    const/4 v2, 0x0

    const/4 v6, 0x2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v2, v6}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setRotatable(Z)V

    :goto_4
    move-object v2, v1

    check-cast v2, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->applyTextSetting(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V

    new-instance v6, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-direct {v6, v2, v9}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    move-object v2, v6

    check-cast v2, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setObject(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V

    move-object v1, v6

    check-cast v1, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v1, v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setEditable(Z)V

    move-object v1, v6

    check-cast v1, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setDimEnabled(Z)V

    move-object v8, v6

    goto/16 :goto_2

    :cond_e
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setRotatable(Z)V

    goto :goto_4

    :pswitch_1
    const-string/jumbo v2, "SpenInView"

    const-string/jumbo v6, "TYPE_IMAGE"

    invoke-static {v2, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v6, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-direct {v6, v2, v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    move-object v2, v6

    check-cast v2, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    invoke-virtual {v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlImage;->setObject(Lcom/samsung/android/sdk/pen/document/SpenObjectImage;)V

    move-object v8, v6

    goto/16 :goto_2

    :pswitch_2
    const-string/jumbo v2, "SpenInView"

    const-string/jumbo v6, "TYPE_STROKE"

    invoke-static {v2, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v6, Lcom/samsung/android/sdk/pen/engine/SpenControlStroke;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-direct {v6, v2, v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlStroke;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    move-object v2, v6

    check-cast v2, Lcom/samsung/android/sdk/pen/engine/SpenControlStroke;

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    invoke-virtual {v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlStroke;->setObject(Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;)V

    move-object v8, v6

    goto/16 :goto_2

    :pswitch_3
    const-string/jumbo v2, "SpenInView"

    const-string/jumbo v6, "TYPE_CONTAINER"

    invoke-static {v2, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v6, Lcom/samsung/android/sdk/pen/engine/SpenControlContainer;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-direct {v6, v2, v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlContainer;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    move-object v2, v6

    check-cast v2, Lcom/samsung/android/sdk/pen/engine/SpenControlContainer;

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    invoke-virtual {v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlContainer;->setObject(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)V

    move-object v8, v6

    goto/16 :goto_2

    :cond_f
    if-eqz v8, :cond_10

    const/4 v1, 0x0

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setStyle(I)V

    invoke-virtual {v8, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setContextMenu(Ljava/util/ArrayList;)V

    const/4 v1, 0x1

    invoke-virtual {v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setFocusable(Z)V

    invoke-virtual {v8}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->requestFocus()Z

    const v1, 0x3ac90

    invoke-virtual {v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setNextFocusDownId(I)V

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenInView$3;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView$3;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V

    invoke-virtual {v8, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    invoke-virtual {p0, v8}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setControl(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)V

    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_10
    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_11
    move v8, v2

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method private onShowRemoverMessage()V
    .locals 3

    const-string/jumbo v0, "SpenInView"

    const-string/jumbo v1, "onShowRemoverMessage"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverToastMessage:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "string_unable_to_erase_heavy_lines"

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getResourceString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverToastMessage:Landroid/widget/Toast;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverToastMessage:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method private onUpdateCanvas(Landroid/graphics/RectF;Z)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateCanvasListener:Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateCanvasListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateCanvasListener:Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateCanvasListener;

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateCanvasListener;->onUpdateCanvas(Landroid/graphics/RectF;Z)V

    :cond_0
    return-void
.end method

.method private onZoom(FFF)V
    .locals 2

    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    iput p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->deltaZoomSizeChanged()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    invoke-interface {v0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;->onZoom(FFF)V

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    instance-of v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->fit(Z)V

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->fit()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->invalidate()V

    :cond_2
    return-void
.end method

.method private raiseOnKeyDown()V
    .locals 4

    const/16 v3, 0x16

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    new-instance v1, Landroid/view/KeyEvent;

    const/4 v2, 0x0

    invoke-direct {v1, v2, v3}, Landroid/view/KeyEvent;-><init>(II)V

    invoke-virtual {v0, v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onKeyDown(ILandroid/view/KeyEvent;)Z

    :cond_0
    return-void
.end method

.method private relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 2

    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->left:F

    iget v0, p2, Landroid/graphics/RectF;->right:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->right:F

    iget v0, p2, Landroid/graphics/RectF;->top:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->top:F

    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v0, v1

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    return-void
.end method

.method private removeHoveringIcon(I)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverEnable:Z

    if-eqz v2, :cond_0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverEnable:Z

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string/jumbo v3, "com.sec.feature.hovering_ui"

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;->setHoveringSpenIcon(I)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    invoke-virtual {v2, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;->removeHoveringSpenCustomIcon(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_4

    :cond_2
    move v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    const-string/jumbo v2, "SpenInView"

    const-string/jumbo v3, "removeCustomHoveringIcon() IllegalArgumentException"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v1

    const-string/jumbo v2, "SpenInView"

    const-string/jumbo v3, "removeCustomHoveringIcon() ClassNotFoundException"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception v1

    const-string/jumbo v2, "SpenInView"

    const-string/jumbo v3, "removeCustomHoveringIcon() NoSuchMethodException"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    :catch_3
    move-exception v1

    const-string/jumbo v2, "SpenInView"

    const-string/jumbo v3, "removeCustomHoveringIcon() IllegalAccessException"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    :catch_4
    move-exception v1

    const-string/jumbo v2, "SpenInView"

    const-string/jumbo v3, "removeCustomHoveringIcon() IllegalAccessException"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0
.end method

.method private savePngFile(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 3

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    sget-object v0, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/4 v2, 0x0

    invoke-virtual {p2, v0, v2, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private setCustomHoveringIcon(Landroid/graphics/drawable/Drawable;)I
    .locals 4

    const/4 v0, -0x1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverEnable:Z

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string/jumbo v2, "com.sec.feature.hovering_ui"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;->setHoveringSpenIcon(ILandroid/graphics/drawable/Drawable;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_5

    move-result v0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string/jumbo v2, "SpenInView"

    const-string/jumbo v3, "setCustomHoveringIcon() IllegalArgumentException"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v1

    const-string/jumbo v2, "SpenInView"

    const-string/jumbo v3, "setCustomHoveringIcon() NotFoundException"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception v1

    const-string/jumbo v2, "SpenInView"

    const-string/jumbo v3, "setCustomHoveringIcon() ClassNotFoundException"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_0

    :catch_3
    move-exception v1

    const-string/jumbo v2, "SpenInView"

    const-string/jumbo v3, "setCustomHoveringIcon() NoSuchMethodException"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0

    :catch_4
    move-exception v1

    const-string/jumbo v2, "SpenInView"

    const-string/jumbo v3, "setCustomHoveringIcon() IllegalAccessException"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    :catch_5
    move-exception v1

    const-string/jumbo v2, "SpenInView"

    const-string/jumbo v3, "setCustomHoveringIcon() InvocationTargetException"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0
.end method

.method private setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string/jumbo v0, "SpenInView"

    const-string/jumbo v1, "setHoverPointerDrawable"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverDrawable:Landroid/graphics/drawable/Drawable;

    goto :goto_0
.end method

.method private updateCanvasLayer(Landroid/graphics/Canvas;Landroid/graphics/RectF;Z)V
    .locals 9

    const v8, 0x3f4800

    const/4 v2, 0x0

    const/4 v7, 0x0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const/4 p2, 0x0

    :cond_0
    if-nez p2, :cond_4

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcDrawRect:Landroid/graphics/Rect;

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    int-to-float v4, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    int-to-float v5, v0

    move-object v0, p0

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->absoluteCoordinate(Landroid/graphics/Rect;FFFF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDstDrawRect:Landroid/graphics/Rect;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    invoke-virtual {v0, v7, v7, v1, v3}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDstDrawRect:Landroid/graphics/Rect;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Rect;->offset(II)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v0, v0

    cmpl-float v0, v0, v2

    if-lez v0, :cond_1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v4, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    int-to-float v5, v0

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBlackPaint:Landroid/graphics/Paint;

    move-object v1, p1

    move v3, v2

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    add-int/2addr v0, v1

    int-to-float v1, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    int-to-float v3, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBlackPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v0, v0

    cmpl-float v0, v0, v2

    if-lez v0, :cond_2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    int-to-float v4, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v5, v0

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBlackPaint:Landroid/graphics/Paint;

    move-object v1, p1

    move v3, v2

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    add-int/2addr v0, v1

    int-to-float v3, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    int-to-float v4, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    int-to-float v5, v0

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBlackPaint:Landroid/graphics/Paint;

    move-object v1, p1

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_2
    :goto_0
    if-eqz p3, :cond_6

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_6

    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDstDrawRect:Landroid/graphics/Rect;

    invoke-direct {v0, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    neg-int v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    neg-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->offset(II)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenFB:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenFB:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v7, v7}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v2

    invoke-virtual {v1, v7, v7, v2}, Landroid/graphics/Bitmap;->setPixel(III)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenFB:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDstDrawRect:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_3
    :goto_1
    return-void

    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcDrawRect:Landroid/graphics/Rect;

    iget v2, p2, Landroid/graphics/RectF;->left:F

    iget v3, p2, Landroid/graphics/RectF;->top:F

    iget v4, p2, Landroid/graphics/RectF;->right:F

    iget v5, p2, Landroid/graphics/RectF;->bottom:F

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->absoluteCoordinate(Landroid/graphics/Rect;FFFF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDstDrawRect:Landroid/graphics/Rect;

    invoke-direct {p0, v0, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->ExtendRectFromRectF(Landroid/graphics/Rect;Landroid/graphics/RectF;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDstDrawRect:Landroid/graphics/Rect;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->offset(II)V

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenFB:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDstDrawRect:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_1

    :cond_6
    move v1, v7

    :goto_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_8

    invoke-virtual {v0, v7, v7}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v2

    invoke-virtual {v0, v7, v7, v2}, Landroid/graphics/Bitmap;->setPixel(III)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    if-nez v2, :cond_d

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcDrawRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcDrawRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    mul-int/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDstDrawRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDstDrawRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    mul-int/2addr v3, v4

    add-int/2addr v2, v3

    if-le v2, v8, :cond_7

    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUseC2D:Z

    if-nez v2, :cond_a

    :cond_7
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcDrawRect:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDstDrawRect:Landroid/graphics/Rect;

    if-nez v1, :cond_9

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcPaint:Landroid/graphics/Paint;

    :goto_3
    invoke-virtual {p1, v0, v3, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_8
    :goto_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_9
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mAntiAliasPaint:Landroid/graphics/Paint;

    goto :goto_3

    :cond_a
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcDrawRect:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDstDrawRect:Landroid/graphics/Rect;

    if-nez v1, :cond_b

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcPaint:Landroid/graphics/Paint;

    :goto_5
    invoke-static {p1, v0, v3, v4, v2}, Lcom/C2Ddrawbitmap/c2ddrawbitmapJNI;->native_drawBitmapJNI(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)I

    move-result v2

    if-gez v2, :cond_8

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcDrawRect:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDstDrawRect:Landroid/graphics/Rect;

    if-nez v1, :cond_c

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcPaint:Landroid/graphics/Paint;

    :goto_6
    invoke-virtual {p1, v0, v3, v4, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_4

    :cond_b
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mAntiAliasPaint:Landroid/graphics/Paint;

    goto :goto_5

    :cond_c
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mAntiAliasPaint:Landroid/graphics/Paint;

    goto :goto_6

    :cond_d
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcDrawRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcDrawRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    mul-int/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDstDrawRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDstDrawRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    mul-int/2addr v3, v4

    add-int/2addr v2, v3

    if-le v2, v8, :cond_e

    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUseC2D:Z

    if-nez v2, :cond_f

    :cond_e
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcDrawRect:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDstDrawRect:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_4

    :cond_f
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcDrawRect:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDstDrawRect:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-static {p1, v0, v2, v3, v4}, Lcom/C2Ddrawbitmap/c2ddrawbitmapJNI;->native_drawBitmapJNI(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)I

    move-result v2

    if-gez v2, :cond_8

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcDrawRect:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDstDrawRect:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_4
.end method

.method private updateCanvasLayer2(Landroid/graphics/Canvas;)V
    .locals 7

    const/4 v2, 0x0

    const/4 v6, 0x0

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    int-to-float v4, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    int-to-float v5, v0

    move-object v0, p0

    move v3, v2

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->absoluteCoordinate(Landroid/graphics/Rect;FFFF)V

    new-instance v4, Landroid/graphics/Rect;

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    invoke-direct {v4, v6, v6, v0, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    move v2, v6

    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v2, v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    invoke-virtual {v0, v6, v6}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v3

    invoke-virtual {v0, v6, v6, v3}, Landroid/graphics/Bitmap;->setPixel(III)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    if-nez v3, :cond_3

    if-nez v2, :cond_2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcPaint:Landroid/graphics/Paint;

    :goto_1
    invoke-virtual {p1, v0, v1, v4, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_1
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mAntiAliasPaint:Landroid/graphics/Paint;

    goto :goto_1

    :cond_3
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v4, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_2
.end method

.method private updateDottedLine(Landroid/graphics/Canvas;Landroid/graphics/RectF;)V
    .locals 8

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineEnable:Z

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    if-eqz v0, :cond_0

    new-instance v7, Landroid/graphics/RectF;

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    int-to-float v1, v1

    invoke-direct {v7, v2, v2, v0, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-direct {p0, v7, v7}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->absoluteCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineIntervalHeight:I

    int-to-float v0, v0

    move v6, v0

    :goto_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineHalfWidth:F

    sub-float v0, v6, v0

    iget v1, v7, Landroid/graphics/RectF;->bottom:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    iget v0, v7, Landroid/graphics/RectF;->top:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineHalfWidth:F

    add-float/2addr v1, v6

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    sub-float v0, v6, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v2, v2

    add-float/2addr v2, v0

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    add-int/2addr v3, v4

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v4, v4

    add-float/2addr v4, v0

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    :cond_2
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineIntervalHeight:I

    int-to-float v0, v0

    add-float/2addr v0, v6

    move v6, v0

    goto :goto_0
.end method

.method private updateFloatingLayer(Landroid/graphics/Canvas;)V
    .locals 8

    const/high16 v3, 0x40000000    # 2.0f

    const/4 v6, 0x0

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFloatingLayer:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFloatingLayer:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFloatingLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v5, v5}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v1

    invoke-virtual {v0, v5, v5, v1}, Landroid/graphics/Bitmap;->setPixel(III)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    int-to-float v2, v2

    cmpg-float v2, v0, v2

    if-gez v2, :cond_1

    :goto_1
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    int-to-float v2, v2

    cmpg-float v2, v1, v2

    if-gez v2, :cond_2

    :goto_2
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    int-to-float v2, v2

    sub-float v0, v2, v0

    div-float/2addr v0, v3

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    int-to-float v2, v2

    sub-float v1, v2, v1

    div-float/2addr v1, v3

    new-instance v2, Landroid/graphics/Rect;

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    invoke-direct {v2, v5, v5, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v3, Landroid/graphics/RectF;

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    int-to-float v4, v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    int-to-float v5, v5

    invoke-direct {v3, v6, v6, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    invoke-direct {p0, v4, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    iget v5, v4, Landroid/graphics/RectF;->left:F

    add-float/2addr v5, v0

    iget v6, v4, Landroid/graphics/RectF;->top:F

    add-float/2addr v6, v1

    iget v7, v4, Landroid/graphics/RectF;->right:F

    add-float/2addr v0, v7

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v1, v4

    invoke-virtual {v3, v5, v6, v0, v1}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFloatingLayer:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mAntiAliasPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v2, v3, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    int-to-float v0, v0

    goto :goto_1

    :cond_2
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    int-to-float v1, v1

    goto :goto_2
.end method

.method private updateHighlight(Landroid/graphics/Canvas;Landroid/graphics/RectF;)V
    .locals 7

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightInfoList:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenHighlightInfo;

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenHighlightInfo;->rect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    sub-float/2addr v4, v5

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->left:F

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenHighlightInfo;->rect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    sub-float/2addr v4, v5

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->top:F

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenHighlightInfo;->rect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    sub-float/2addr v4, v5

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->right:F

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenHighlightInfo;->rect:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    sub-float/2addr v4, v5

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v4, v5

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    int-to-float v4, v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    int-to-float v5, v5

    invoke-virtual {v3, v6, v6, v4, v5}, Landroid/graphics/RectF;->intersect(FFFF)Z

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v4, v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v5, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/RectF;->offset(FF)V

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    int-to-float v4, v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    int-to-float v5, v5

    invoke-virtual {v3, v6, v6, v4, v5}, Landroid/graphics/RectF;->intersect(FFFF)Z

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightPaint:Landroid/graphics/Paint;

    iget v5, v0, Lcom/samsung/android/sdk/pen/engine/SpenHighlightInfo;->color:I

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightPaint:Landroid/graphics/Paint;

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenHighlightInfo;->size:F

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v0, v5

    invoke-virtual {v4, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method


# virtual methods
.method public GetPageEffectWorking()Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->isWorking()Z

    move-result v0

    goto :goto_0
.end method

.method public GetRtoBmpItstScrHeight()I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    return v0
.end method

.method public GetRtoBmpItstScrWidth()I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    return v0
.end method

.method public GetScreenHeight()I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    return v0
.end method

.method public GetScreenStartX()I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    return v0
.end method

.method public GetScreenStartY()I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    return v0
.end method

.method public GetScreenWidth()I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    return v0
.end method

.method public UpdateCanvas(Landroid/graphics/Canvas;Landroid/graphics/RectF;Z)V
    .locals 15
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WrongCall"
        }
    .end annotation

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v9

    if-nez p2, :cond_0

    const-string/jumbo v1, "SpenInView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Performance onUpdateCanvas start rect = null isScreenFramebuffer = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->isWorking()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->drawAnimation(Landroid/graphics/Canvas;)V

    move-wide v1, v9

    move-wide v3, v9

    move-wide v5, v9

    :goto_1
    const-string/jumbo v7, "SpenInView"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "Performance onUpdateCanvas end total = "

    invoke-direct {v8, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v11

    sub-long/2addr v11, v9

    invoke-virtual {v8, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v11, " ms predraw = "

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sub-long v9, v5, v9

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " ms spenview = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    sub-long v5, v3, v5

    invoke-virtual {v8, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, " ms postdraw = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sub-long/2addr v1, v3

    invoke-virtual {v5, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v7, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    const-string/jumbo v1, "SpenInView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Performance onUpdateCanvas start rect = ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    iget v3, v0, Landroid/graphics/RectF;->left:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p2

    iget v3, v0, Landroid/graphics/RectF;->top:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ") ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p2

    iget v3, v0, Landroid/graphics/RectF;->right:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p2

    iget v3, v0, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ") w = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->width()F

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " h = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->height()F

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " isScreenFramebuffer = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v6, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v7, v2

    move-object/from16 v2, p1

    move-object/from16 v8, p2

    invoke-interface/range {v1 .. v8}, Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;->onDraw(Landroid/graphics/Canvas;FFFFFLandroid/graphics/RectF;)V

    :cond_2
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v13

    invoke-direct/range {p0 .. p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateCanvasLayer(Landroid/graphics/Canvas;Landroid/graphics/RectF;Z)V

    if-nez p2, :cond_6

    new-instance v8, Landroid/graphics/RectF;

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    int-to-float v4, v4

    invoke-direct {v8, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    :goto_2
    move-object/from16 v0, p1

    invoke-direct {p0, v0, v8}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateHighlight(Landroid/graphics/Canvas;Landroid/graphics/RectF;)V

    move-object/from16 v0, p1

    invoke-direct {p0, v0, v8}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateDottedLine(Landroid/graphics/Canvas;Landroid/graphics/RectF;)V

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCircleRadius:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_3

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCircleRadius:F

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    :cond_3
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-lez v1, :cond_4

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mView:Landroid/view/View;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mView:Landroid/view/View;

    instance-of v1, v1, Landroid/view/SurfaceView;

    if-eqz v1, :cond_7

    invoke-virtual {v8}, Landroid/graphics/RectF;->width()F

    move-result v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v2, v3

    const/high16 v3, 0x41f00000    # 30.0f

    add-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v2, v3

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_4

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    :cond_4
    :goto_3
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v11

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPostDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPostDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v6, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v7, v2

    move-object/from16 v2, p1

    invoke-interface/range {v1 .. v8}, Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;->onDraw(Landroid/graphics/Canvas;FFFFFLandroid/graphics/RectF;)V

    :cond_5
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->drawScroll(Landroid/graphics/Canvas;)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    if-eqz v3, :cond_9

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->draw(Landroid/graphics/Canvas;)V

    move-wide v3, v11

    move-wide v5, v13

    goto/16 :goto_1

    :cond_6
    invoke-direct/range {p0 .. p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateFloatingLayer(Landroid/graphics/Canvas;)V

    move-object/from16 v8, p2

    goto/16 :goto_2

    :cond_7
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_3

    :cond_8
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getColor()I

    move-result v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    move-wide v1, v9

    move-wide v3, v9

    move-wide v5, v9

    goto/16 :goto_1

    :cond_9
    move-wide v3, v11

    move-wide v5, v13

    goto/16 :goto_1
.end method

.method public cancelStroke()V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_cancelStroke(I)V

    goto :goto_0
.end method

.method public cancelStrokeFrame()V
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/16 v0, 0x8

    const-string/jumbo v1, "The vies has not SpenPageDoc instance. please use to call setPageDoc."

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStrokeFrame:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStrokeFrame:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->cancelStrokeFrame()Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    move-result-object v0

    const-string/jumbo v1, "STROKE_FRAME"

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getExtraDataInt(Ljava/lang/String;)I

    move-result v1

    if-ne v1, v3, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->undoToTag()[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateUndo([Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->clearHistoryTag()V

    :cond_2
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStrokeFrame:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    :cond_3
    return-void

    :cond_4
    const-string/jumbo v1, "STROKE_FRAME"

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getExtraDataInt(Ljava/lang/String;)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->setVisibility(Z)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->update()V

    goto :goto_0
.end method

.method public captureCurrentView(Z)Landroid/graphics/Bitmap;
    .locals 5

    const/4 v1, 0x0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    :goto_0
    return-object v1

    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_updateAllScreenFrameBuffer(I)V

    if-nez p1, :cond_2

    :try_start_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    if-nez p1, :cond_1

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    neg-int v3, v3

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    neg-int v4, v4

    int-to-float v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    :cond_1
    const/4 v3, 0x1

    invoke-direct {p0, v2, v1, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateCanvasLayer(Landroid/graphics/Canvas;Landroid/graphics/RectF;Z)V

    move-object v1, v0

    goto :goto_0

    :cond_2
    :try_start_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string/jumbo v0, "SpenInView"

    const-string/jumbo v2, "Failed to create bitmap"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x2

    const-string/jumbo v2, " : fail createBitmap."

    invoke-static {v0, v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    move-object v0, v1

    goto :goto_1
.end method

.method public capturePage(F)Landroid/graphics/Bitmap;
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    int-to-float v2, v2

    mul-float/2addr v2, p1

    float-to-int v2, v2

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public changeStrokeFrame(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/16 v0, 0x8

    const-string/jumbo v1, "The vies has not SpenPageDoc instance. please use to call setPageDoc."

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    :cond_1
    if-nez p1, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "container is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    invoke-virtual {p1, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setVisibility(Z)V

    invoke-virtual {p1, v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setVisibility(Z)V

    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->update()V

    return-void

    :cond_3
    invoke-virtual {p1, v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setVisibility(Z)V

    invoke-virtual {p1, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->getObject(I)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setVisibility(Z)V

    goto :goto_0
.end method

.method public clearHighlight()V
    .locals 2

    const/4 v1, 0x0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightInfoList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightInfoList:Ljava/util/ArrayList;

    const/4 v0, 0x1

    invoke-direct {p0, v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->onUpdateCanvas(Landroid/graphics/RectF;Z)V

    goto :goto_0
.end method

.method public close()V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDetachReceiver:Lcom/samsung/android/sdk/pen/engine/SpenInView$DetachReceiver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDetachReceiver:Lcom/samsung/android/sdk/pen/engine/SpenInView$DetachReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDetachReceiver:Lcom/samsung/android/sdk/pen/engine/SpenInView$DetachReceiver;

    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->closeControl()V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getReplayState()I

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->stopReplay()V

    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_finalize(I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFloatingLayer:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFloatingLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFloatingLayer:Landroid/graphics/Bitmap;

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_11

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenFB:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenFB:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenFB:Landroid/graphics/Bitmap;

    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->close()V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    :cond_7
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->close()V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    :cond_8
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPointer:Lcom/samsung/android/sdk/pen/engine/SpenInView$SPenHoverPointerIconWrapper;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->close()V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    :cond_9
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mGestureDetector:Landroid/view/GestureDetector;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, v3}, Landroid/view/GestureDetector;->setOnDoubleTapListener(Landroid/view/GestureDetector$OnDoubleTapListener;)V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mGestureDetector:Landroid/view/GestureDetector;

    :cond_a
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->close()V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    :cond_b
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverToastMessage:Landroid/widget/Toast;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverToastMessage:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->cancel()V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverToastMessage:Landroid/widget/Toast;

    :cond_c
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBlackPaint:Landroid/graphics/Paint;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightPaint:Landroid/graphics/Paint;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDebugPaint:Landroid/graphics/Paint;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcPaint:Landroid/graphics/Paint;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mAntiAliasPaint:Landroid/graphics/Paint;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextPaint:Landroid/graphics/Paint;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePaint:Landroid/graphics/Paint;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSelectionSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightInfoList:Ljava/util/ArrayList;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenRemoverChangeListener;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPostDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFlickListener:Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHyperTextListener:Lcom/samsung/android/sdk/pen/engine/SpenHyperTextListener;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLongPressListener:Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mReplayListener:Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenDetachmentListener:Lcom/samsung/android/sdk/pen/engine/SpenPenDetachmentListener;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSelectionChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenSelectionChangeListener;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mThisStrokeFrameListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateStrokeFrameListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mZoomPadListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomPadListener;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateCanvasListener:Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateCanvasListener;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPointerlistener:Landroid/view/View$OnHoverListener;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchUpHandler:Landroid/os/Handler;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchUpHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchUpHandler:Landroid/os/Handler;

    :cond_d
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSetPageDocHandler:Landroid/os/Handler;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSetPageDocHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSetPageDocHandler:Landroid/os/Handler;

    :cond_e
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSpenZoomPad:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSpenZoomPad:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->close()V

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSpenZoomPad:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;

    :cond_f
    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSdkResources:Landroid/content/res/Resources;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContextMenu:Landroid/view/ContextMenu;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSrcDrawRect:Landroid/graphics/Rect;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDstDrawRect:Landroid/graphics/Rect;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mParentView:Landroid/view/ViewGroup;

    iput-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mView:Landroid/view/View;

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUseC2D:Z

    if-eqz v0, :cond_10

    invoke-static {}, Lcom/C2Ddrawbitmap/c2ddrawbitmapJNI;->native_deinit_c2dJNI()V

    :cond_10
    return-void

    :cond_11
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-nez v2, :cond_4

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto/16 :goto_0
.end method

.method public closeControl()V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTransactionClosingControl:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTransactionClosingControl:Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v0, :cond_2

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->close()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTransactionClosingControl:Z

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1
.end method

.method public drawObjectList(Ljava/util/ArrayList;)Landroid/graphics/Bitmap;
    .locals 4

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move-object v0, v1

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2}, Landroid/graphics/RectF;-><init>()V

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    :try_start_0
    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    float-to-int v2, v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-static {v2, v0, p1, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_drawObjectList(ILandroid/graphics/Bitmap;Ljava/util/ArrayList;I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-nez v2, :cond_1

    :goto_2
    move-object v0, v1

    goto :goto_0

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getDrawnRect()Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {p0, v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->joinRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    goto :goto_1

    :catch_0
    move-exception v0

    const-string/jumbo v0, "SpenInView"

    const-string/jumbo v2, "Failed to create bitmap"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_2
.end method

.method public getBlankColor()I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    goto :goto_0
.end method

.method public getCanvasHeight()I
    .locals 2

    const/4 v0, 0x0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getHeight()I

    move-result v0

    goto :goto_0
.end method

.method public getCanvasWidth()I
    .locals 2

    const/4 v0, 0x0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getWidth()I

    move-result v0

    goto :goto_0
.end method

.method public getControl()Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    goto :goto_0
.end method

.method public getEraserSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;
    .locals 2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;-><init>()V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->type:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->type:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    goto :goto_0
.end method

.method public getFrameStartPosition()Landroid/graphics/PointF;
    .locals 3

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/graphics/PointF;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v2, v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    goto :goto_0
.end method

.method public getMaxZoomRatio()F
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getMaxZoomRatio(I)F

    move-result v0

    goto :goto_0
.end method

.method public getMinZoomRatio()F
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getMinZoomRatio(I)F

    move-result v0

    goto :goto_0
.end method

.method public getPan()Landroid/graphics/PointF;
    .locals 2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    invoke-static {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getPan(ILandroid/graphics/PointF;)V

    goto :goto_0
.end method

.method public getPenSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    .locals 2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;-><init>()V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-boolean v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    iput-boolean v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    goto :goto_0
.end method

.method public getRemoverSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    .locals 2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;-><init>()V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    goto :goto_0
.end method

.method public getReplayState()I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getReplayState(I)I

    move-result v0

    goto :goto_0
.end method

.method public getSelectionSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;
    .locals 2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;-><init>()V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSelectionSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSelectionSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;->type:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;->type:I

    goto :goto_0
.end method

.method public getTemporaryStroke()Ljava/util/ArrayList;
    .locals 2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    invoke-static {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getTemporaryStroke(ILjava/util/ArrayList;)V

    goto :goto_0
.end method

.method public getTextSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    .locals 6

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    instance-of v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getTextSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v0

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;-><init>()V

    iget v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getCanvasWidth()I

    move-result v2

    int-to-double v2, v2

    const-wide/high16 v4, 0x4069000000000000L    # 200.0

    div-double/2addr v2, v4

    double-to-float v2, v2

    mul-float/2addr v1, v2

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineIndent:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineIndent:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    goto :goto_0
.end method

.method public getToolTypeAction(I)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    invoke-static {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getToolTypeAction(II)I

    move-result v0

    goto :goto_0
.end method

.method public getZoomPadPosition()Landroid/graphics/PointF;
    .locals 4

    const/4 v0, 0x0

    const/4 v2, 0x0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1, v2, v2}, Landroid/graphics/PointF;-><init>(FF)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSpenZoomPad:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;

    if-nez v2, :cond_1

    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSpenZoomPad:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSpenZoomPad:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;

    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnZoomPadActionListener;

    invoke-direct {v3, p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnZoomPadActionListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenInView$OnZoomPadActionListener;)V

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->setActionListener(Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadActionListener;)V

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSpenZoomPad:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->getZoomViewRect()Landroid/graphics/RectF;

    move-result-object v0

    iget v0, v0, Landroid/graphics/RectF;->left:F

    iput v0, v1, Landroid/graphics/PointF;->x:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSpenZoomPad:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->getZoomViewRect()Landroid/graphics/RectF;

    move-result-object v0

    iget v0, v0, Landroid/graphics/RectF;->top:F

    iput v0, v1, Landroid/graphics/PointF;->y:F

    move-object v0, v1

    goto :goto_0
.end method

.method public getZoomRatio()F
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getZoomRatio(I)F

    move-result v0

    goto :goto_0
.end method

.method public isDottedLineEnabled()Z
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineEnable:Z

    goto :goto_0
.end method

.method public isHorizontalSmartScrollEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsSmartHorizontal:Z

    return v0
.end method

.method public isHyperTextViewEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsHyperText:Z

    return v0
.end method

.method public isScrollBarEnabled()Z
    .locals 2

    const/4 v0, 0x0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->isScroll()Z

    move-result v0

    goto :goto_0
.end method

.method public isSmartScaleEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsSmartScale:Z

    return v0
.end method

.method public isToolTipEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsToolTip:Z

    return v0
.end method

.method public isVerticalSmartScrollEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsSmartVertical:Z

    return v0
.end method

.method public isZoomable()Z
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_isZoomable(I)Z

    move-result v0

    goto :goto_0
.end method

.method joinRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 2

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget v1, p2, Landroid/graphics/RectF;->right:F

    cmpl-float v0, v0, v1

    if-gez v0, :cond_0

    iget v0, p2, Landroid/graphics/RectF;->top:F

    iget v1, p2, Landroid/graphics/RectF;->bottom:F

    cmpl-float v0, v0, v1

    if-gez v0, :cond_0

    iget v0, p1, Landroid/graphics/RectF;->left:F

    iget v1, p1, Landroid/graphics/RectF;->right:F

    cmpl-float v0, v0, v1

    if-gez v0, :cond_2

    iget v0, p1, Landroid/graphics/RectF;->top:F

    iget v1, p1, Landroid/graphics/RectF;->bottom:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_3

    :cond_2
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iput v0, p1, Landroid/graphics/RectF;->left:F

    iget v0, p2, Landroid/graphics/RectF;->right:F

    iput v0, p1, Landroid/graphics/RectF;->right:F

    iget v0, p2, Landroid/graphics/RectF;->top:F

    iput v0, p1, Landroid/graphics/RectF;->top:F

    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    goto :goto_0

    :cond_3
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget v1, p1, Landroid/graphics/RectF;->left:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_4

    iget v0, p2, Landroid/graphics/RectF;->left:F

    iput v0, p1, Landroid/graphics/RectF;->left:F

    :cond_4
    iget v0, p2, Landroid/graphics/RectF;->top:F

    iget v1, p1, Landroid/graphics/RectF;->top:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_5

    iget v0, p2, Landroid/graphics/RectF;->top:F

    iput v0, p1, Landroid/graphics/RectF;->top:F

    :cond_5
    iget v0, p2, Landroid/graphics/RectF;->right:F

    iget v1, p1, Landroid/graphics/RectF;->right:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_6

    iget v0, p2, Landroid/graphics/RectF;->right:F

    iput v0, p1, Landroid/graphics/RectF;->right:F

    :cond_6
    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    iget v1, p1, Landroid/graphics/RectF;->bottom:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    iput v0, p1, Landroid/graphics/RectF;->bottom:F

    goto :goto_0
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 14

    const/4 v2, 0x0

    const/4 v1, 0x1

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    const/16 v0, 0x9

    if-ne v3, v0, :cond_2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x64

    iget-wide v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchProcessingTime:J

    add-long/2addr v6, v8

    cmp-long v0, v4, v6

    if-lez v0, :cond_4

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSkipTouch:Z

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSkipTouch:Z

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v8

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    const-string/jumbo v0, "SpenInView"

    new-instance v12, Ljava/lang/StringBuilder;

    const-string/jumbo v13, "skiptouch hover action = "

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v12, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v12, " eventTime = "

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v6, " downTime = "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v6, " systemTime = "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v6, " diffTime = "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSkipTouch:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v2

    invoke-static {v0, p1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_onHover(ILandroid/view/MotionEvent;I)Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->onHoverEvent(Landroid/view/MotionEvent;)V

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mView:Landroid/view/View;

    invoke-interface {v0, v2, p1}, Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;->onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_0

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method protected onLayout(ZIIIILandroid/graphics/Rect;Landroid/graphics/Rect;)V
    .locals 6
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DrawAllocation"
        }
    .end annotation

    sub-int v2, p4, p2

    sub-int v1, p5, p3

    if-nez p7, :cond_1

    move v0, v1

    :goto_0
    if-eqz v2, :cond_0

    if-nez v1, :cond_2

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget v0, p7, Landroid/graphics/Rect;->bottom:I

    iget v3, p6, Landroid/graphics/Rect;->top:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    add-int/2addr v3, v4

    sub-int/2addr v0, v3

    goto :goto_0

    :cond_2
    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    if-ne v2, v3, :cond_3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    if-ne v1, v3, :cond_3

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeightExceptSIP:I

    if-eq v0, v3, :cond_0

    :cond_3
    const-string/jumbo v3, "SpenInView"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "onLayout("

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v3, "onLayout. parentLayoutRect : "

    invoke-virtual {p0, v3, p6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->printRect(Ljava/lang/String;Landroid/graphics/Rect;)V

    if-eqz p7, :cond_4

    const-string/jumbo v3, "onLayout. windowVisibleRect : "

    invoke-virtual {p0, v3, p7}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->printRect(Ljava/lang/String;Landroid/graphics/Rect;)V

    const-string/jumbo v3, "SpenInView"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "onLayout. hExceptSIP : "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeightExceptSIP:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    invoke-virtual {v0, v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->setScreenSize(II)V

    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v0, v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->setScreenResolution(II)V

    :cond_6
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeightExceptSIP:I

    invoke-static {v0, v2, v1, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setScreenSize(IIII)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    if-le v0, v1, :cond_a

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    :goto_2
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    if-le v1, v2, :cond_b

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    :goto_3
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFramebufferWidth:I

    if-gt v0, v2, :cond_7

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFramebufferHeight:I

    if-le v1, v2, :cond_9

    :cond_7
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFramebufferWidth:I

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFramebufferHeight:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenFB:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenFB:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_8
    :try_start_0
    const-string/jumbo v0, "SpenInView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onLayout. ScreenFB W : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFramebufferWidth:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", H : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFramebufferHeight:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFramebufferWidth:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFramebufferHeight:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenFB:Landroid/graphics/Bitmap;

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenFB:Landroid/graphics/Bitmap;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setScreenFrameBuffer(ILandroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :cond_9
    :goto_4
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateScreenFrameBuffer()V

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->onUpdateCanvas(Landroid/graphics/RectF;Z)V

    goto/16 :goto_1

    :cond_a
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    goto :goto_2

    :cond_b
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    goto :goto_3

    :catch_0
    move-exception v0

    const-string/jumbo v0, "SpenInView"

    const-string/jumbo v1, "Failed to create bitmap"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_4
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    const/4 v0, 0x0

    :goto_1
    if-lt v0, v2, :cond_9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v1, v0, 0xff

    if-nez v1, :cond_4

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x258

    iget-wide v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchProcessingTime:J

    add-long/2addr v4, v6

    cmp-long v0, v2, v4

    if-lez v0, :cond_b

    const/4 v0, 0x1

    :goto_2
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSkipTouch:Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->isWorking()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSkipTouch:Z

    :cond_2
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSkipTouch:Z

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v6

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    const-string/jumbo v0, "SpenInView"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "skiptouch action = "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " eventTime = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " downTime = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " systemTime = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " diffTime = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " mTouchProcessingTime = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchProcessingTime:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchProcessingTime:J

    :cond_4
    if-nez v1, :cond_5

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsControlSelect:Z

    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isTouchEnabled()Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getStyle()I

    move-result v0

    const/4 v2, 0x3

    if-eq v0, v2, :cond_d

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsControlSelect:Z

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    if-ne v1, v0, :cond_6

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsControlSelect:Z

    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onTouchEvent(Landroid/view/MotionEvent;)Z

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->setAction(I)V

    :cond_7
    :goto_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mGestureDetector:Landroid/view/GestureDetector;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    :cond_8
    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_9
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getSize(I)F

    move-result v3

    cmpl-float v3, v3, v1

    if-lez v3, :cond_a

    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_c
    const/4 v0, 0x1

    if-ne v1, v0, :cond_7

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->closeControl()V

    goto :goto_3

    :cond_d
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSkipTouch:Z

    if-nez v0, :cond_1e

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    neg-int v0, v0

    int-to-float v0, v0

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    neg-int v2, v2

    int-to-float v2, v2

    invoke-virtual {p1, v0, v2}, Landroid/view/MotionEvent;->offsetLocation(FF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)V

    :cond_e
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mGestureDetector:Landroid/view/GestureDetector;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    :cond_f
    const/4 v0, 0x1

    if-ne v1, v0, :cond_10

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_10

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchUpHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_10
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const-string/jumbo v0, "SpenInView"

    const-string/jumbo v4, "Performance touch process start"

    invoke-static {v0, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mView:Landroid/view/View;

    invoke-interface {v0, v4, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_11

    const-string/jumbo v0, "SpenInView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Performance pretouch listener has consumed action = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_11
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    if-nez v1, :cond_18

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    const/4 v6, 0x0

    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v6

    invoke-static {v0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getToolTypeAction(II)I

    move-result v0

    iget-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsToolTip:Z

    if-eqz v6, :cond_12

    const/4 v6, 0x3

    if-eq v0, v6, :cond_12

    const/4 v6, 0x4

    if-eq v0, v6, :cond_12

    const/4 v6, 0x5

    if-eq v0, v6, :cond_12

    const/4 v6, 0x0

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverDrawable:Landroid/graphics/drawable/Drawable;

    :cond_12
    const/4 v6, 0x3

    if-eq v0, v6, :cond_13

    const/4 v6, 0x4

    if-ne v0, v6, :cond_15

    :cond_13
    const/4 v6, 0x1

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isEraserCursor:Z

    const/4 v6, 0x3

    if-ne v0, v6, :cond_16

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v0, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v0, v6

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCircleRadius:F

    :cond_14
    :goto_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v7, v7

    add-float/2addr v6, v7

    iput v6, v0, Landroid/graphics/PointF;->x:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v7, v7

    add-float/2addr v6, v7

    iput v6, v0, Landroid/graphics/PointF;->y:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePaint:Landroid/graphics/Paint;

    const/high16 v6, 0x40000000    # 2.0f

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v6, v7

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    :cond_15
    :goto_5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->removerTouchX:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->removerTouchY:F

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    const/4 v6, 0x0

    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v6

    invoke-static {v0, p1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_onTouch(ILandroid/view/MotionEvent;I)Z

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    if-eqz v0, :cond_1d

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mView:Landroid/view/View;

    invoke-interface {v0, v8, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1d

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    sub-long/2addr v10, v2

    iput-wide v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchProcessingTime:J

    const-string/jumbo v0, "SpenInView"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "Performance touch process end total = "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v11

    sub-long/2addr v11, v2

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " ms pretouch = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sub-long v2, v4, v2

    invoke-virtual {v10, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " ms spenview = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sub-long v3, v6, v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " ms posttouch = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sub-long v3, v8, v6

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " ms action = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_16
    const/4 v6, 0x4

    if-ne v0, v6, :cond_14

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    if-nez v0, :cond_17

    const/high16 v0, 0x41a00000    # 20.0f

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v0, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v0, v6

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    goto/16 :goto_4

    :cond_17
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    const/4 v6, 0x1

    if-ne v0, v6, :cond_14

    const/high16 v0, 0x42200000    # 40.0f

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v0, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v0, v6

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    goto/16 :goto_4

    :cond_18
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isEraserCursor:Z

    if-eqz v0, :cond_15

    const/4 v0, 0x2

    if-ne v1, v0, :cond_1c

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    const/4 v6, 0x0

    invoke-virtual {p1, v6}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v6

    invoke-static {v0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_getToolTypeAction(II)I

    move-result v0

    const/4 v6, 0x3

    if-ne v0, v6, :cond_1a

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v0, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v0, v6

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCircleRadius:F

    :cond_19
    :goto_6
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    int-to-float v7, v7

    add-float/2addr v6, v7

    iput v6, v0, Landroid/graphics/PointF;->x:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    int-to-float v7, v7

    add-float/2addr v6, v7

    iput v6, v0, Landroid/graphics/PointF;->y:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePaint:Landroid/graphics/Paint;

    const/high16 v6, 0x40000000    # 2.0f

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v6, v7

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    goto/16 :goto_5

    :cond_1a
    const/4 v6, 0x4

    if-ne v0, v6, :cond_19

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    if-nez v0, :cond_1b

    const/high16 v0, 0x41a00000    # 20.0f

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v0, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v0, v6

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    goto :goto_6

    :cond_1b
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    const/4 v6, 0x1

    if-ne v0, v6, :cond_19

    const/high16 v0, 0x42200000    # 40.0f

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    mul-float/2addr v0, v6

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v0, v6

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    goto :goto_6

    :cond_1c
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isEraserCursor:Z

    const/high16 v0, -0x3d380000    # -100.0f

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCircleRadius:F

    const/high16 v0, -0x3d380000    # -100.0f

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverRadius:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    const/high16 v6, -0x3d380000    # -100.0f

    iput v6, v0, Landroid/graphics/PointF;->x:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;

    const/high16 v6, -0x3d380000    # -100.0f

    iput v6, v0, Landroid/graphics/PointF;->y:F

    goto/16 :goto_5

    :cond_1d
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v8

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    sub-long/2addr v10, v2

    iput-wide v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchProcessingTime:J

    const-string/jumbo v0, "SpenInView"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "Performance touch process end total = "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v11

    sub-long/2addr v11, v2

    invoke-virtual {v10, v11, v12}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, " ms pretouch = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    sub-long v2, v4, v2

    invoke-virtual {v10, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " ms spenview = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sub-long v3, v6, v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " ms posttouch = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sub-long v3, v8, v6

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " ms action = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1e
    const/4 v0, 0x1

    goto/16 :goto_0
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    instance-of v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    if-ne p2, v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->hideSoftInput()V

    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUseC2D:Z

    if-eqz v0, :cond_1

    if-nez p2, :cond_1

    invoke-static {}, Lcom/C2Ddrawbitmap/c2ddrawbitmapJNI;->native_deinit_c2dJNI()V

    invoke-static {}, Lcom/C2Ddrawbitmap/c2ddrawbitmapJNI;->native_init_c2dJNI()V

    :cond_1
    return-void

    :cond_2
    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->showSoftInput()V

    goto :goto_0
.end method

.method public pauseReplay()V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_pauseReplay(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0
.end method

.method printRect(Ljava/lang/String;Landroid/graphics/Rect;)V
    .locals 3

    const-string/jumbo v0, "SpenInView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/Rect;->left:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/Rect;->top:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ") ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/Rect;->right:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ") w = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " h = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method printRect(Ljava/lang/String;Landroid/graphics/RectF;)V
    .locals 3

    if-nez p2, :cond_0

    const-string/jumbo v0, "SpenInView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, " null"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const-string/jumbo v0, "SpenInView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/RectF;->left:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/RectF;->top:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ") ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/RectF;->right:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p2, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ") w = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " h = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public resumeReplay()V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_resumeReplay(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0
.end method

.method public retakeStrokeFrame(Landroid/app/Activity;Landroid/view/ViewGroup;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;)V
    .locals 15

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    const/16 v2, 0x8

    const-string/jumbo v3, "The vies has not SpenPageDoc instance. please use to call setPageDoc."

    invoke-static {v2, v3}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    :cond_1
    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    if-eqz p3, :cond_2

    if-nez p4, :cond_3

    :cond_2
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "Arguments is null. activity = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " viewgroup = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " stroke = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " listener = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p4

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->checkMDMCameraLock()Z

    move-result v2

    if-nez v2, :cond_4

    const/16 v2, 0x22

    move-object/from16 v0, p4

    move-object/from16 v1, p3

    invoke-interface {v0, v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;->onCanceled(ILcom/samsung/android/sdk/pen/document/SpenObjectContainer;)V

    :goto_0
    return-void

    :cond_4
    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->setVisibility(Z)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->update()V

    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    invoke-direct {v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStrokeFrame:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v5

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    const/16 v3, -0x64

    invoke-static {v2, v3, v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setCanvasBitmap(IILandroid/graphics/Bitmap;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStrokeFrame:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    const/4 v3, 0x1

    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->captureCurrentView(Z)Landroid/graphics/Bitmap;

    move-result-object v4

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getWidth()I

    move-result v6

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getHeight()I

    move-result v7

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStrokeFrameType:I

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mThisStrokeFrameListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getPan()Landroid/graphics/PointF;

    move-result-object v11

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getZoomRatio()F

    move-result v12

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getFrameStartPosition()Landroid/graphics/PointF;

    move-result-object v13

    move-object/from16 v3, p1

    move-object/from16 v9, p3

    move-object/from16 v14, p2

    invoke-virtual/range {v2 .. v14}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->retakeStrokeFrame(Landroid/app/Activity;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;IIILcom/samsung/android/sdk/pen/document/SpenObjectContainer;Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;Landroid/graphics/PointF;FLandroid/graphics/PointF;Landroid/view/ViewGroup;)V

    goto :goto_0
.end method

.method public setBackgroundColorChangeListener(Ljava/lang/Object;Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;)V
    .locals 2

    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "SpenInView"

    const-string/jumbo v1, "setBackgroundColorListener"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getBackgroundColor()I

    move-result v0

    shr-int/lit8 v0, v0, 0x18

    and-int/lit16 v0, v0, 0xff

    const/16 v1, 0xff

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMagicPenEnabled:Z

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;->onChanged(Z)V

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMagicPenEnabled:Z

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setBlankColor(I)V
    .locals 2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBlackPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->setPaint(Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public setColorPickerListener(Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mColorPickerListener:Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;

    goto :goto_0
.end method

.method public setControl(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)V
    .locals 3

    const/4 v2, 0x0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->close()V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsControlSelect:Z

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenInView$BaseControlListener;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setListener(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->updateRectList()V

    instance-of v0, p1, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    if-eqz v0, :cond_3

    check-cast p1, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnTextActionListener;

    invoke-direct {v0, p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnTextActionListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenInView$OnTextActionListener;)V

    invoke-virtual {p1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setActionListener(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;)V

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mParentView:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mParentView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public setControlListener(Lcom/samsung/android/sdk/pen/engine/SpenControlListener;)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControlListener:Lcom/samsung/android/sdk/pen/engine/SpenControlListener;

    goto :goto_0
.end method

.method public setDottedLineEnabled(ZIII[FF)V
    .locals 3

    const/4 v2, 0x0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineEnable:Z

    if-ne v0, p1, :cond_3

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineIntervalHeight:I

    if-ne v0, p2, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    if-ne v0, p3, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    int-to-float v1, p4

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    :cond_3
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineEnable:Z

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineEnable:Z

    if-eqz v0, :cond_4

    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineIntervalHeight:I

    int-to-float v0, p4

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLineHalfWidth:F

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    int-to-float v1, p4

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/DashPathEffect;

    invoke-direct {v1, p5, p6}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p3}, Landroid/graphics/Paint;->setColor(I)V

    :goto_1
    const/4 v0, 0x1

    invoke-direct {p0, v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->onUpdateCanvas(Landroid/graphics/RectF;Z)V

    goto :goto_0

    :cond_4
    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDottedLinePaint:Landroid/graphics/Paint;

    goto :goto_1
.end method

.method public setEraserChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

    goto :goto_0
.end method

.method public setEraserSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V
    .locals 5

    const/high16 v4, 0x40000000    # 2.0f

    const/4 v2, 0x1

    const/4 v3, 0x5

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    instance-of v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->type:I

    if-ne v1, v2, :cond_6

    move v1, v2

    :goto_1
    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setTextEraserEnabled(Z)V

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    cmpg-float v0, v0, v4

    if-gez v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iput v4, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    :cond_3
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsToolTip:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-eqz v0, :cond_4

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v3, :cond_4

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v3, :cond_4

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v3, :cond_4

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v3, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableEraserImage(F)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_4
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->type:I

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setEraserType(II)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setEraserSize(IF)V

    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;->onChanged(Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V

    goto :goto_0

    :cond_6
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public setFlickListener(Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mFlickListener:Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;

    goto :goto_0
.end method

.method public setHighlight(Ljava/util/ArrayList;)V
    .locals 2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightInfoList:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHighlightInfoList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->onUpdateCanvas(Landroid/graphics/RectF;Z)V

    goto :goto_0
.end method

.method public setHorizontalSmartScrollEnabled(ZLandroid/graphics/Rect;Landroid/graphics/Rect;II)V
    .locals 6

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    if-eqz v0, :cond_0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsSmartHorizontal:Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->enableHorizontalSmartScroll(ZLandroid/graphics/Rect;Landroid/graphics/Rect;II)V

    goto :goto_0
.end method

.method public setHoverListener(Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;)V
    .locals 2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string/jumbo v1, "4."

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    goto :goto_0

    :cond_1
    const/16 v0, 0xc

    const-string/jumbo v1, "S Pen Hover Listener cannot be supported under android ICS"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public setHyperTextListener(Lcom/samsung/android/sdk/pen/engine/SpenHyperTextListener;)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHyperTextListener:Lcom/samsung/android/sdk/pen/engine/SpenHyperTextListener;

    goto :goto_0
.end method

.method public setHyperTextViewEnabled(Z)V
    .locals 2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsHyperText:Z

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsHyperText:Z

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setHyperTextViewEnabled(IZ)V

    goto :goto_0
.end method

.method public setLongPressListener(Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLongPressListener:Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;

    goto :goto_0
.end method

.method public setMaxZoomRatio(F)Z
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    invoke-static {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setMaxZoomRatio(IF)Z

    move-result v0

    goto :goto_0
.end method

.method public setMinZoomRatio(F)Z
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    invoke-static {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setMinZoomRatio(IF)Z

    move-result v0

    goto :goto_0
.end method

.method public setPageDoc(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;IIF)Z
    .locals 7

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-ne p1, v0, :cond_2

    const-string/jumbo v0, "SpenInView"

    const-string/jumbo v1, "setPageDoc is same"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v2

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v0

    if-nez v0, :cond_3

    const-string/jumbo v0, "SpenInView"

    const-string/jumbo v2, "setPageDoc is closed"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->isWorking()Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "SpenInView"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "setPageDoc, direction="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v0, p3}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->setType(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartX:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenStartY:I

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrWidth:I

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRtoBmpItstScrHeight:I

    invoke-virtual {v0, v3, v4, v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->setCanvasInformation(IIII)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->saveScreenshot()Z

    move-result v0

    if-nez v0, :cond_9

    const-string/jumbo v0, "SpenInView"

    const-string/jumbo v3, "setPageDoc. No enough memory1. Change to PAGE_TRANSITION_EFFECT_NONE"

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    :goto_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->closeControl()V

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->createBitmap(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    const/4 v4, 0x0

    invoke-static {v3, v4, p4, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setPan(IFFZ)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getBackgroundColor()I

    move-result v3

    shr-int/lit8 v3, v3, 0x18

    and-int/lit16 v3, v3, 0xff

    const/16 v4, 0xff

    if-ne v3, v4, :cond_6

    move v3, v2

    :goto_2
    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMagicPenEnabled:Z

    if-eq v3, v4, :cond_4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    invoke-interface {v4, v3}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;->onChanged(Z)V

    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMagicPenEnabled:Z

    :cond_4
    if-eqz v0, :cond_7

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-static {v0, v3, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setPageDoc(ILcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v0, p2}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->startAnimation(I)Z

    move-result v0

    if-nez v0, :cond_5

    const-string/jumbo v0, "SpenInView"

    const-string/jumbo v3, "setPageDoc. No enough memory2. Change to PAGE_TRANSITION_EFFECT_NONE"

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSetPageDocHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_5
    :goto_3
    move v1, v2

    goto/16 :goto_0

    :cond_6
    move v3, v1

    goto :goto_2

    :cond_7
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-static {v0, v3, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setPageDoc(ILcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSetPageDocHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_3

    :cond_8
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    goto :goto_3

    :cond_9
    move v0, v2

    goto :goto_1
.end method

.method public setPageDoc(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-ne p1, v0, :cond_2

    const-string/jumbo v0, "SpenInView"

    const-string/jumbo v2, "setPageDoc is same"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v1

    goto :goto_0

    :cond_2
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v0

    if-nez v0, :cond_3

    const-string/jumbo v0, "SpenInView"

    const-string/jumbo v1, "setPageDoc is closed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectManager:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectManager;->isWorking()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_4
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->closeControl()V

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->createBitmap(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getBackgroundColor()I

    move-result v0

    shr-int/lit8 v0, v0, 0x18

    and-int/lit16 v0, v0, 0xff

    const/16 v3, 0xff

    if-ne v0, v3, :cond_7

    move v0, v1

    :goto_1
    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMagicPenEnabled:Z

    if-eq v0, v3, :cond_5

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    invoke-interface {v3, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;->onChanged(Z)V

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMagicPenEnabled:Z

    :cond_5
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-static {v0, v3, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setPageDoc(ILcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z

    move-result v0

    if-eqz v0, :cond_8

    if-eqz p2, :cond_6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSetPageDocHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_6
    :goto_2
    move v2, v1

    goto :goto_0

    :cond_7
    move v0, v2

    goto :goto_1

    :cond_8
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    goto :goto_2
.end method

.method public setPageEffectListener(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageEffectListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

    return-void
.end method

.method public setPan(Landroid/graphics/PointF;)V
    .locals 4

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    iget v1, p1, Landroid/graphics/PointF;->x:F

    iget v2, p1, Landroid/graphics/PointF;->y:F

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setPan(IFFZ)V

    goto :goto_0
.end method

.method public setParent(Landroid/view/ViewGroup;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mParentView:Landroid/view/ViewGroup;

    return-void
.end method

.method public setPenChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    goto :goto_0
.end method

.method public setPenDetachmentListener(Lcom/samsung/android/sdk/pen/engine/SpenPenDetachmentListener;)V
    .locals 3

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenDetachmentListener:Lcom/samsung/android/sdk/pen/engine/SpenPenDetachmentListener;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenDetachmentListener:Lcom/samsung/android/sdk/pen/engine/SpenPenDetachmentListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDetachReceiver:Lcom/samsung/android/sdk/pen/engine/SpenInView$DetachReceiver;

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/IntentFilter;

    const-string/jumbo v1, "com.samsung.pen.INSERT"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "com.samsung.pen.INSERT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenInView$DetachReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView$DetachReceiver;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenInView$DetachReceiver;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDetachReceiver:Lcom/samsung/android/sdk/pen/engine/SpenInView$DetachReceiver;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDetachReceiver:Lcom/samsung/android/sdk/pen/engine/SpenInView$DetachReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V
    .locals 4

    const/4 v2, 0x5

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    const/high16 v1, 0x41200000    # 10.0f

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    :cond_2
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsToolTip:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v2, :cond_3

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v2, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v2, :cond_3

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v2, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v3, v3, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableImage(Ljava/lang/String;IF)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_3
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setPenStyle(ILjava/lang/String;)Z

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setPenColor(II)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setPenSize(IF)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-boolean v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_enablePenCurve(IZ)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setAdvancedSetting(ILjava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSpenZoomPad:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSpenZoomPad:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;->onChanged(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    goto/16 :goto_0
.end method

.method public setPostDrawListener(Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;)V
    .locals 2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string/jumbo v0, "SpenInView"

    const-string/jumbo v1, "Register setPostDrawListener"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPostDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    goto :goto_0
.end method

.method public setPreDrawListener(Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;)V
    .locals 2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string/jumbo v0, "SpenInView"

    const-string/jumbo v1, "Register setPreDrawListener"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreDrawListener:Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;

    goto :goto_0
.end method

.method public setPreTouchListener(Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    goto :goto_0
.end method

.method public setRemoverChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenRemoverChangeListener;)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenRemoverChangeListener;

    goto :goto_0
.end method

.method public setRemoverSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x5

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    :cond_2
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsToolTip:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-eqz v0, :cond_3

    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v2, :cond_3

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v2, :cond_3

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v2, :cond_3

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v2, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    const/16 v1, 0x14

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableRemoverImage(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_3
    :goto_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setRemoverType(II)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setRemoverSize(IF)V

    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenRemoverChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenRemoverChangeListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenRemoverChangeListener;->onChanged(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    if-ne v0, v3, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableRemoverImage(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method public setReplayListener(Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mReplayListener:Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;

    goto :goto_0
.end method

.method public setReplayPosition(I)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    invoke-static {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setReplayPosition(II)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0
.end method

.method public setReplaySpeed(I)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    invoke-static {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setReplaySpeed(II)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0
.end method

.method public setScrollBarEnabled(Z)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScroll:Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenCanvasViewScroll;->enableScroll(Z)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateScreen()V

    goto :goto_0
.end method

.method public setSelectionChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenSelectionChangeListener;)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSelectionChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenSelectionChangeListener;

    goto :goto_0
.end method

.method public setSelectionSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;)V
    .locals 2

    const/4 v1, 0x5

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSelectionSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsToolTip:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v1, :cond_2

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v1, :cond_2

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v1, :cond_2

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v1, :cond_2

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSelectionSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

    if-eqz v0, :cond_3

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSelectionSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;->type:I

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setSelectionType(II)V

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSelectionChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenSelectionChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSelectionChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenSelectionChangeListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSelectionSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSelectionChangeListener;->onChanged(Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;)V

    goto :goto_0
.end method

.method public setSmartScaleEnabled(ZLandroid/graphics/Rect;IIF)V
    .locals 6

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    if-eqz v0, :cond_0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsSmartScale:Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->enableSmartScale(ZLandroid/graphics/Rect;IIF)V

    goto :goto_0
.end method

.method public setTextChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;

    goto :goto_0
.end method

.method public setTextSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V
    .locals 7

    const/4 v2, 0x2

    const/4 v6, 0x1

    const/4 v1, 0x5

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    instance-of v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setTextSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;

    invoke-interface {v0, p1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;->onChanged(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;I)V

    goto :goto_0

    :cond_3
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsToolTip:Z

    if-eqz v0, :cond_4

    invoke-virtual {p0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v1, :cond_4

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v1, :cond_4

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v1, :cond_4

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    if-eq v0, v1, :cond_4

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    const/high16 v1, 0x41200000    # 10.0f

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getCanvasWidth()I

    move-result v2

    int-to-double v2, v2

    const-wide/high16 v4, 0x4069000000000000L    # 200.0

    div-double/2addr v2, v4

    double-to-float v2, v2

    mul-float/2addr v1, v2

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    if-gez v0, :cond_6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    const/4 v1, 0x0

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextChangeListener:Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    invoke-interface {v0, v1, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;->onChanged(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;I)V

    goto :goto_0
.end method

.method public setToolTipEnabled(Z)V
    .locals 3

    const-string/jumbo v0, "SpenInView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "setToolTipEnabled="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsToolTip:Z

    return-void
.end method

.method public setToolTypeAction(II)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x5

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    if-eqz v0, :cond_1

    if-eq p1, v2, :cond_1

    if-ne p2, v2, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setTouchEnabled(Z)V

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->setToolTypeAction(II)V

    :cond_2
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsToolTip:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x2

    if-ne p2, v0, :cond_6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    if-nez v0, :cond_3

    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v3, v3, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableImage(Ljava/lang/String;IF)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_4
    :goto_2
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    invoke-static {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setToolTypeAction(III)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setTouchEnabled(Z)V

    goto :goto_1

    :cond_6
    const/4 v0, 0x3

    if-ne p2, v0, :cond_8

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    if-nez v0, :cond_7

    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    :cond_7
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mEraserSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableEraserImage(F)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2

    :cond_8
    const/4 v0, 0x4

    if-ne p2, v0, :cond_b

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    if-nez v0, :cond_9

    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    :cond_9
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    const/16 v1, 0x14

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableRemoverImage(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2

    :cond_a
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRemoverSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->type:I

    if-ne v0, v3, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    const/16 v1, 0x28

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableRemoverImage(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2

    :cond_b
    if-ne p2, v2, :cond_c

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mToolTip:Lcom/samsung/android/sdk/pen/engine/SpenToolTip;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenToolTip;->getDrawableHoverImage()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2

    :cond_c
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2
.end method

.method public setTouchListener(Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    goto :goto_0
.end method

.method public setVerticalSmartScrollEnabled(ZLandroid/graphics/Rect;Landroid/graphics/Rect;II)V
    .locals 6

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    if-eqz v0, :cond_0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsSmartVertical:Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->enableVerticalSmartScroll(ZLandroid/graphics/Rect;Landroid/graphics/Rect;II)V

    goto :goto_0
.end method

.method public setView(Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mView:Landroid/view/View;

    return-void
.end method

.method public setZoom(FFF)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    invoke-static {v0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setZoom(IFFF)V

    goto :goto_0
.end method

.method public setZoomListener(Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mZoomListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;

    goto :goto_0
.end method

.method public setZoomPadListener(Lcom/samsung/android/sdk/pen/engine/SpenZoomPadListener;)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mZoomPadListener:Lcom/samsung/android/sdk/pen/engine/SpenZoomPadListener;

    goto :goto_0
.end method

.method public setZoomPadPosition(Landroid/graphics/PointF;)V
    .locals 3

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSpenZoomPad:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;

    if-nez v0, :cond_2

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSpenZoomPad:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSpenZoomPad:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnZoomPadActionListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnZoomPadActionListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenInView$OnZoomPadActionListener;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->setActionListener(Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadActionListener;)V

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSpenZoomPad:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;

    iget v1, p1, Landroid/graphics/PointF;->x:F

    iget v2, p1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->setZoomViewPosition(FF)V

    goto :goto_0
.end method

.method public setZoomable(Z)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mZoomable:Z

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    invoke-static {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_enableZoom(IZ)V

    goto :goto_0
.end method

.method public startReplay()V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_startReplay(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0
.end method

.method public startTemporaryStroke()V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_startTemporaryStroke(I)V

    goto :goto_0
.end method

.method public startZoomPad()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverPointerDrawable(Landroid/graphics/drawable/Drawable;)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-virtual {p0, v2, v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setZoom(FFF)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateScreenFrameBuffer()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSpenZoomPad:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;

    if-nez v0, :cond_1

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSpenZoomPad:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSpenZoomPad:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnZoomPadActionListener;

    invoke-direct {v1, p0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnZoomPadActionListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenInView$OnZoomPadActionListener;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->setActionListener(Lcom/samsung/android/sdk/pen/engine/SpenZoomPad$ZoomPadActionListener;)V

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSpenZoomPad:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeightExceptSIP:I

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->setCanvasSize(III)I

    move-result v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setScreenSize(IIII)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSpenZoomPad:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCanvasLayer:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->setLayer(Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSpenZoomPad:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->setPanAndZoom(FFF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    if-nez v0, :cond_2

    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSpenZoomPad:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPenSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSpenZoomPad:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mParentView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->startZoomPad(Landroid/view/ViewGroup;)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isZoomable()Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPreZoomable:Z

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setZoomable(Z)V

    goto :goto_0
.end method

.method public stopReplay()V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_stopReplay(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0
.end method

.method public stopTemporaryStroke()V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_stopTemporaryStroke(I)V

    goto :goto_0
.end method

.method public stopZoomPad()V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSpenZoomPad:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSpenZoomPad:Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenZoomPad;->stopZoomPad()V

    goto :goto_0
.end method

.method public takeStrokeFrame(Landroid/app/Activity;Landroid/view/ViewGroup;Ljava/util/List;Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;)V
    .locals 14

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/16 v1, 0x8

    const-string/jumbo v2, "The vies has not SpenPageDoc instance. please use to call setPageDoc."

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    :cond_1
    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    if-eqz p3, :cond_2

    if-nez p4, :cond_3

    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Arguments is null. activity = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " viewgroup = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " stroke = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " listener = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->checkMDMCameraLock()Z

    move-result v1

    if-nez v1, :cond_4

    const/16 v1, 0x21

    const/4 v2, 0x0

    move-object/from16 v0, p4

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;->onCanceled(ILcom/samsung/android/sdk/pen/document/SpenObjectContainer;)V

    :goto_0
    return-void

    :cond_4
    new-instance v2, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    invoke-direct {v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;-><init>()V

    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    invoke-direct {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;-><init>()V

    invoke-virtual {v2, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->appendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->update()V

    new-instance v1, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    invoke-direct {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;-><init>()V

    new-instance v3, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;

    invoke-direct {v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;-><init>()V

    invoke-virtual {v1, v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->appendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    new-instance v8, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    invoke-direct {v8}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;-><init>()V

    const/4 v3, 0x0

    invoke-virtual {v8, v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->setOutOfViewEnabled(Z)V

    const/4 v3, 0x0

    invoke-virtual {v8, v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->setRotatable(Z)V

    const/4 v3, 0x0

    invoke-virtual {v8, v3}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->setVisibility(Z)V

    invoke-virtual {v8, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->appendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    invoke-virtual {v8, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->appendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->setHistoryTag()V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v1, v8}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->appendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    const-string/jumbo v1, "STROKE_FRAME"

    const/4 v2, 0x1

    invoke-virtual {v8, v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->setExtraDataInt(Ljava/lang/String;I)V

    move-object/from16 v0, p4

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mUpdateStrokeFrameListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    invoke-direct {v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStrokeFrame:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapHeight:I

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    const/16 v2, -0x64

    invoke-static {v1, v2, v4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_setCanvasBitmap(IILandroid/graphics/Bitmap;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStrokeFrame:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->captureCurrentView(Z)Landroid/graphics/Bitmap;

    move-result-object v3

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getWidth()I

    move-result v5

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getHeight()I

    move-result v6

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mStrokeFrameType:I

    iget-object v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mThisStrokeFrameListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getPan()Landroid/graphics/PointF;

    move-result-object v10

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getZoomRatio()F

    move-result v11

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getFrameStartPosition()Landroid/graphics/PointF;

    move-result-object v12

    move-object v2, p1

    move-object/from16 v13, p2

    invoke-virtual/range {v1 .. v13}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->takeStrokeFrame(Landroid/app/Activity;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;IIILcom/samsung/android/sdk/pen/document/SpenObjectContainer;Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;Landroid/graphics/PointF;FLandroid/graphics/PointF;Landroid/view/ViewGroup;)V

    goto/16 :goto_0

    :cond_5
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v4, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->removeObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    invoke-virtual {v2, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->appendObject(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    goto/16 :goto_1
.end method

.method public update()V
    .locals 2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->createBitmap(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_update(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getBackgroundColor()I

    move-result v0

    shr-int/lit8 v0, v0, 0x18

    and-int/lit16 v0, v0, 0xff

    const/16 v1, 0xff

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_1
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMagicPenEnabled:Z

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBackgroundColorChangeListener:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;->onChanged(Z)V

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mMagicPenEnabled:Z

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public updateRedo([Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;)V
    .locals 2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_1

    array-length v0, p1

    if-nez v0, :cond_2

    :cond_1
    const-string/jumbo v0, "SpenInView"

    const-string/jumbo v1, "The parameter \'userDataList\' cannot be null."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->createBitmap(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    array-length v1, p1

    invoke-static {v0, p1, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_updateRedo(I[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;I)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->fitControlToObject()V

    goto :goto_0
.end method

.method public updateScreen()V
    .locals 4

    const/4 v3, 0x0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getReplayState()I

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/graphics/RectF;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenHeight:I

    int-to-float v2, v2

    invoke-direct {v0, v3, v3, v1, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->onUpdateCanvas(Landroid/graphics/RectF;Z)V

    goto :goto_0
.end method

.method public updateScreenFrameBuffer()V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getReplayState()I

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_updateAllScreenFrameBuffer(I)V

    goto :goto_0
.end method

.method public updateUndo([Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;)V
    .locals 2

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_1

    array-length v0, p1

    if-nez v0, :cond_2

    :cond_1
    const-string/jumbo v0, "SpenInView"

    const-string/jumbo v1, "The parameter \'userDataList\' cannot be null."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->createBitmap(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I

    array-length v1, p1

    invoke-static {v0, p1, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_updateUndo(I[Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;I)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->fitControlToObject()V

    goto :goto_0
.end method
