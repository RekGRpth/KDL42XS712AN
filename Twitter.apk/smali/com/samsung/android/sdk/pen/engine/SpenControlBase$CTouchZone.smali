.class public Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final mTempTouchZone:Landroid/graphics/RectF;

.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)V
    .locals 1

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    return-void
.end method


# virtual methods
.method protected checkTouchPosition(Landroid/graphics/RectF;IILcom/samsung/android/sdk/pen/document/SpenObjectBase;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;)V
    .locals 5

    const/4 v1, 0x0

    const v2, 0x38d1b717    # 1.0E-4f

    invoke-virtual {p5}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->reset()V

    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v0

    cmpg-float v0, v0, v2

    if-ltz v0, :cond_0

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v0

    cmpg-float v0, v0, v2

    if-gez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    move v0, v1

    :goto_1
    const/16 v2, 0xa

    if-ge v0, v2, :cond_0

    invoke-virtual {p0, v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->getRect(ILandroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v2

    int-to-float v3, p2

    int-to-float v4, p3

    invoke-virtual {v2, v3, v4}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-eqz v2, :cond_3

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "zone pressed : "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    iput v0, p5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mStyle:I
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->mStyle:I
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    :cond_2
    iget v0, p5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/16 v1, 0x9

    if-eq v0, v1, :cond_0

    const/4 v0, -0x1

    iput v0, p5, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    goto :goto_0

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method getRect(ILandroid/graphics/RectF;)Landroid/graphics/RectF;
    .locals 11

    const/4 v2, 0x0

    const/high16 v10, 0x41f00000    # 30.0f

    const/high16 v9, 0x40000000    # 2.0f

    const/4 v1, 0x1

    const/4 v8, 0x0

    const/16 v0, 0x14

    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->getType()I

    move-result v3

    if-ne v3, v1, :cond_0

    const/16 v0, 0x22

    :cond_0
    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    float-to-int v4, v4

    mul-int/lit8 v5, v0, 0x4

    if-ge v3, v5, :cond_4

    move v3, v1

    :goto_0
    mul-int/lit8 v5, v0, 0x4

    if-ge v4, v5, :cond_3

    :goto_1
    const/4 v2, 0x5

    if-ge v0, v2, :cond_1

    const/4 v0, 0x5

    :cond_1
    mul-int/lit8 v2, v0, 0x2

    packed-switch p1, :pswitch_data_0

    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    return-object v0

    :pswitch_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    iget v3, p2, Landroid/graphics/RectF;->left:F

    int-to-float v4, v0

    sub-float/2addr v3, v4

    int-to-float v4, v2

    sub-float/2addr v3, v4

    iget v4, p2, Landroid/graphics/RectF;->top:F

    int-to-float v5, v0

    sub-float/2addr v4, v5

    int-to-float v2, v2

    sub-float v2, v4, v2

    iget v4, p2, Landroid/graphics/RectF;->left:F

    int-to-float v5, v0

    add-float/2addr v4, v5

    iget v5, p2, Landroid/graphics/RectF;->top:F

    int-to-float v0, v0

    add-float/2addr v0, v5

    invoke-virtual {v1, v3, v2, v4, v0}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_2

    :pswitch_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    invoke-virtual {p2}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    int-to-float v5, v0

    sub-float/2addr v4, v5

    iget v5, p2, Landroid/graphics/RectF;->top:F

    int-to-float v2, v2

    sub-float v2, v5, v2

    invoke-virtual {p2}, Landroid/graphics/RectF;->centerX()F

    move-result v5

    int-to-float v6, v0

    add-float/2addr v5, v6

    iget v6, p2, Landroid/graphics/RectF;->top:F

    int-to-float v0, v0

    add-float/2addr v0, v6

    invoke-virtual {v1, v4, v2, v5, v0}, Landroid/graphics/RectF;->set(FFFF)V

    if-eqz v3, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    invoke-virtual {v0, v8, v8, v8, v8}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_2

    :pswitch_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    iget v3, p2, Landroid/graphics/RectF;->right:F

    int-to-float v4, v0

    sub-float/2addr v3, v4

    iget v4, p2, Landroid/graphics/RectF;->top:F

    int-to-float v5, v0

    sub-float/2addr v4, v5

    int-to-float v5, v2

    sub-float/2addr v4, v5

    iget v5, p2, Landroid/graphics/RectF;->right:F

    int-to-float v6, v0

    add-float/2addr v5, v6

    int-to-float v2, v2

    add-float/2addr v2, v5

    iget v5, p2, Landroid/graphics/RectF;->top:F

    int-to-float v0, v0

    add-float/2addr v0, v5

    invoke-virtual {v1, v3, v4, v2, v0}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_2

    :pswitch_3
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    iget v4, p2, Landroid/graphics/RectF;->left:F

    int-to-float v5, v0

    sub-float/2addr v4, v5

    int-to-float v2, v2

    sub-float v2, v4, v2

    invoke-virtual {p2}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    int-to-float v5, v0

    sub-float/2addr v4, v5

    iget v5, p2, Landroid/graphics/RectF;->left:F

    int-to-float v6, v0

    add-float/2addr v5, v6

    invoke-virtual {p2}, Landroid/graphics/RectF;->centerY()F

    move-result v6

    int-to-float v0, v0

    add-float/2addr v0, v6

    invoke-virtual {v3, v2, v4, v5, v0}, Landroid/graphics/RectF;->set(FFFF)V

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    invoke-virtual {v0, v8, v8, v8, v8}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_2

    :pswitch_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    iget v1, p2, Landroid/graphics/RectF;->left:F

    iget v2, p2, Landroid/graphics/RectF;->top:F

    iget v3, p2, Landroid/graphics/RectF;->right:F

    iget v4, p2, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_2

    :pswitch_5
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    iget v4, p2, Landroid/graphics/RectF;->right:F

    int-to-float v5, v0

    sub-float/2addr v4, v5

    invoke-virtual {p2}, Landroid/graphics/RectF;->centerY()F

    move-result v5

    int-to-float v6, v0

    sub-float/2addr v5, v6

    iget v6, p2, Landroid/graphics/RectF;->right:F

    int-to-float v7, v0

    add-float/2addr v6, v7

    int-to-float v2, v2

    add-float/2addr v2, v6

    invoke-virtual {p2}, Landroid/graphics/RectF;->centerY()F

    move-result v6

    int-to-float v0, v0

    add-float/2addr v0, v6

    invoke-virtual {v3, v4, v5, v2, v0}, Landroid/graphics/RectF;->set(FFFF)V

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    invoke-virtual {v0, v8, v8, v8, v8}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_2

    :pswitch_6
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    iget v3, p2, Landroid/graphics/RectF;->left:F

    int-to-float v4, v0

    sub-float/2addr v3, v4

    int-to-float v4, v2

    sub-float/2addr v3, v4

    iget v4, p2, Landroid/graphics/RectF;->bottom:F

    int-to-float v5, v0

    sub-float/2addr v4, v5

    iget v5, p2, Landroid/graphics/RectF;->left:F

    int-to-float v6, v0

    add-float/2addr v5, v6

    iget v6, p2, Landroid/graphics/RectF;->bottom:F

    int-to-float v0, v0

    add-float/2addr v0, v6

    int-to-float v2, v2

    add-float/2addr v0, v2

    invoke-virtual {v1, v3, v4, v5, v0}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_2

    :pswitch_7
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    invoke-virtual {p2}, Landroid/graphics/RectF;->centerX()F

    move-result v4

    int-to-float v5, v0

    sub-float/2addr v4, v5

    iget v5, p2, Landroid/graphics/RectF;->bottom:F

    int-to-float v6, v0

    sub-float/2addr v5, v6

    invoke-virtual {p2}, Landroid/graphics/RectF;->centerX()F

    move-result v6

    int-to-float v7, v0

    add-float/2addr v6, v7

    iget v7, p2, Landroid/graphics/RectF;->bottom:F

    int-to-float v0, v0

    add-float/2addr v0, v7

    int-to-float v2, v2

    add-float/2addr v0, v2

    invoke-virtual {v1, v4, v5, v6, v0}, Landroid/graphics/RectF;->set(FFFF)V

    if-eqz v3, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    invoke-virtual {v0, v8, v8, v8, v8}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_2

    :pswitch_8
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    iget v3, p2, Landroid/graphics/RectF;->right:F

    int-to-float v4, v0

    sub-float/2addr v3, v4

    iget v4, p2, Landroid/graphics/RectF;->bottom:F

    int-to-float v5, v0

    sub-float/2addr v4, v5

    iget v5, p2, Landroid/graphics/RectF;->right:F

    int-to-float v6, v0

    add-float/2addr v5, v6

    int-to-float v6, v2

    add-float/2addr v5, v6

    iget v6, p2, Landroid/graphics/RectF;->bottom:F

    int-to-float v0, v0

    add-float/2addr v0, v6

    int-to-float v2, v2

    add-float/2addr v0, v2

    invoke-virtual {v1, v3, v4, v5, v0}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_2

    :pswitch_9
    const/high16 v1, 0x3fc00000    # 1.5f

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchZone;->mTempTouchZone:Landroid/graphics/RectF;

    iget v3, p2, Landroid/graphics/RectF;->left:F

    iget v4, p2, Landroid/graphics/RectF;->right:F

    iget v5, p2, Landroid/graphics/RectF;->left:F

    sub-float/2addr v4, v5

    div-float/2addr v4, v9

    add-float/2addr v3, v4

    int-to-float v4, v0

    mul-float/2addr v4, v1

    sub-float/2addr v3, v4

    iget v4, p2, Landroid/graphics/RectF;->top:F

    mul-int/lit8 v5, v0, 0x4

    int-to-float v5, v5

    mul-float/2addr v5, v1

    sub-float/2addr v4, v5

    sub-float/2addr v4, v10

    int-to-float v5, v0

    add-float/2addr v4, v5

    iget v5, p2, Landroid/graphics/RectF;->left:F

    iget v6, p2, Landroid/graphics/RectF;->right:F

    iget v7, p2, Landroid/graphics/RectF;->left:F

    sub-float/2addr v6, v7

    div-float/2addr v6, v9

    add-float/2addr v5, v6

    int-to-float v6, v0

    mul-float/2addr v6, v1

    add-float/2addr v5, v6

    iget v6, p2, Landroid/graphics/RectF;->top:F

    int-to-float v7, v0

    mul-float/2addr v1, v7

    sub-float v1, v6, v1

    sub-float/2addr v1, v10

    int-to-float v0, v0

    add-float/2addr v0, v1

    invoke-virtual {v2, v3, v4, v5, v0}, Landroid/graphics/RectF;->set(FFFF)V

    goto/16 :goto_2

    :cond_3
    move v1, v2

    goto/16 :goto_1

    :cond_4
    move v3, v2

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_9
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_4
    .end packed-switch
.end method
