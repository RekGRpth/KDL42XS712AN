.class public Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;
.super Landroid/view/SurfaceView;
.source "Twttr"

# interfaces
.implements Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;


# static fields
.field public static final PAGE_TRANSITION_EFFECT_LEFT:I = 0x0

.field public static final PAGE_TRANSITION_EFFECT_RIGHT:I = 0x1

.field public static final PAGE_TRANSITION_EFFECT_TYPE_SHADOW:I = 0x0

.field public static final PAGE_TRANSITION_EFFECT_TYPE_SLIDE:I = 0x1

.field public static final REPLAY_STATE_PAUSED:I = 0x2

.field public static final REPLAY_STATE_PLAYING:I = 0x1

.field public static final REPLAY_STATE_STOPPED:I = 0x0

.field public static final STROKE_FRAME_KEY:Ljava/lang/String; = "STROKE_FRAME"

.field public static final STROKE_FRAME_RETAKE:I = 0x2

.field public static final STROKE_FRAME_TAKE:I = 0x1

.field private static final TAG:Ljava/lang/String; = "SpenSurfaceView"


# instance fields
.field private mHolderCallback:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$HolderCallback;

.field private mListener:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;

.field private mMetricsRect:Landroid/graphics/Rect;

.field private mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mMetricsRect:Landroid/graphics/Rect;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mHolderCallback:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$HolderCallback;

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->construct(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mMetricsRect:Landroid/graphics/Rect;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mHolderCallback:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$HolderCallback;

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->construct(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mMetricsRect:Landroid/graphics/Rect;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mHolderCallback:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$HolderCallback;

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->construct(Landroid/content/Context;)V

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)Lcom/samsung/android/sdk/pen/engine/SpenInView;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    return-object v0
.end method

.method private construct(Landroid/content/Context;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;

    invoke-direct {v0, p1, v1, v4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/engine/SpenInView$UpdateCanvasListener;Z)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_1

    const/16 v0, 0x9

    const-string/jumbo v1, "failed to create SpenInView"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p1, :cond_2

    const/16 v0, 0x8

    const-string/jumbo v1, " : context must not be null"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setView(Landroid/view/View;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    if-le v1, v2, :cond_3

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    :goto_1
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, v3, v3, v0, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mMetricsRect:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$HolderCallback;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$HolderCallback;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$HolderCallback;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mHolderCallback:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$HolderCallback;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mHolderCallback:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$HolderCallback;

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    invoke-interface {v0, v4}, Landroid/view/SurfaceHolder;->setFormat(I)V

    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string/jumbo v1, "4."

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverPointerlistener:Landroid/view/View$OnHoverListener;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    goto :goto_0

    :cond_3
    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    goto :goto_1
.end method


# virtual methods
.method public cancelStroke()V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->cancelStroke()V

    goto :goto_0
.end method

.method public cancelStrokeFrame()V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->cancelStrokeFrame()V

    goto :goto_0
.end method

.method public captureCurrentView(Z)Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->captureCurrentView(Z)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public capturePage(F)Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->capturePage(F)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public changeStrokeFrame(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->changeStrokeFrame(Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;)V

    goto :goto_0
.end method

.method public clearHighlight()V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->clearHighlight()V

    goto :goto_0
.end method

.method public close()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->close()V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mMetricsRect:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mHolderCallback:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$HolderCallback;

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->removeCallback(Landroid/view/SurfaceHolder$Callback;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mHolderCallback:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$HolderCallback;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView$SurfaceUpdateCanvasListener;

    goto :goto_0
.end method

.method public closeControl()V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->closeControl()V

    goto :goto_0
.end method

.method public drawObjectList(Ljava/util/ArrayList;)Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->drawObjectList(Ljava/util/ArrayList;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public getBlankColor()I
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getBlankColor()I

    move-result v0

    goto :goto_0
.end method

.method public getCanvasHeight()I
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getCanvasHeight()I

    move-result v0

    goto :goto_0
.end method

.method public getCanvasWidth()I
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getCanvasWidth()I

    move-result v0

    goto :goto_0
.end method

.method public getControl()Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getControl()Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    move-result-object v0

    goto :goto_0
.end method

.method public getEraserSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getEraserSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    move-result-object v0

    goto :goto_0
.end method

.method public getFrameStartPosition()Landroid/graphics/PointF;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getFrameStartPosition()Landroid/graphics/PointF;

    move-result-object v0

    goto :goto_0
.end method

.method public getMaxZoomRatio()F
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getMaxZoomRatio()F

    move-result v0

    goto :goto_0
.end method

.method public getMinZoomRatio()F
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getMinZoomRatio()F

    move-result v0

    goto :goto_0
.end method

.method public getPan()Landroid/graphics/PointF;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getPan()Landroid/graphics/PointF;

    move-result-object v0

    goto :goto_0
.end method

.method public getPenSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getPenSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v0

    goto :goto_0
.end method

.method public getRemoverSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getRemoverSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    move-result-object v0

    goto :goto_0
.end method

.method public getReplayState()I
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getReplayState()I

    move-result v0

    goto :goto_0
.end method

.method public getSelectionSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getSelectionSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;

    move-result-object v0

    goto :goto_0
.end method

.method public getTemporaryStroke()Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getTemporaryStroke()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method public getTextSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getTextSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v0

    goto :goto_0
.end method

.method public getToolTypeAction(I)I
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    goto :goto_0
.end method

.method public getZoomPadPosition()Landroid/graphics/PointF;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getZoomPadPosition()Landroid/graphics/PointF;

    move-result-object v0

    goto :goto_0
.end method

.method public getZoomRatio()F
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getZoomRatio()F

    move-result v0

    goto :goto_0
.end method

.method public isDottedLineEnabled()Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isDottedLineEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public isHorizontalSmartScrollEnabled()Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isHorizontalSmartScrollEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public isHyperTextViewEnabled()Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isHyperTextViewEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public isScrollBarEnabled()Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isScrollBarEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public isSmartScaleEnabled()Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSmartScaleEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public isToolTipEnabled()Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isToolTipEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public isVerticalSmartScrollEnabled()Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isVerticalSmartScrollEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public isZoomable()Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->isZoomable()Z

    move-result v0

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setParent(Landroid/view/ViewGroup;)V

    invoke-super {p0}, Landroid/view/SurfaceView;->onAttachedToWindow()V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setParent(Landroid/view/ViewGroup;)V

    invoke-super {p0}, Landroid/view/SurfaceView;->onDetachedFromWindow()V

    goto :goto_0
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->onHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 8
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "DrawAllocation",
            "WrongCall"
        }
    .end annotation

    invoke-super/range {p0 .. p5}, Landroid/view/SurfaceView;->onLayout(ZIIII)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v6}, Landroid/view/ViewGroup;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mMetricsRect:Landroid/graphics/Rect;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mMetricsRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v7}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    iget v0, v7, Landroid/graphics/Rect;->bottom:I

    iget v1, v6, Landroid/graphics/Rect;->bottom:I

    if-ge v0, v1, :cond_2

    iget v0, v7, Landroid/graphics/Rect;->bottom:I

    iget v1, v6, Landroid/graphics/Rect;->top:I

    if-gt v0, v1, :cond_3

    :cond_2
    const/4 v7, 0x0

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v7}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->onLayout(ZIIIILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 3

    :try_start_0
    invoke-static {}, Lcom/C2Ddrawbitmap/c2ddrawbitmapJNI;->native_deinit_c2dJNI()V

    invoke-static {}, Lcom/C2Ddrawbitmap/c2ddrawbitmapJNI;->native_init_c2dJNI()V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ExceptionInInitializerError; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const-string/jumbo v0, "SpenSurfaceView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onSizeChanged("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->onVisibilityChanged(Landroid/view/View;I)V

    invoke-super {p0, p1, p2}, Landroid/view/SurfaceView;->onVisibilityChanged(Landroid/view/View;I)V

    goto :goto_0
.end method

.method public pauseReplay()V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->pauseReplay()V

    goto :goto_0
.end method

.method public resumeReplay()V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->resumeReplay()V

    goto :goto_0
.end method

.method public retakeStrokeFrame(Landroid/app/Activity;Landroid/view/ViewGroup;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->retakeStrokeFrame(Landroid/app/Activity;Landroid/view/ViewGroup;Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;)V

    goto :goto_0
.end method

.method public setBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/16 v0, 0xd

    const-string/jumbo v1, " : setBackground not supported"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    return-void
.end method

.method public setBackgroundColor(I)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/16 v0, 0xd

    const-string/jumbo v1, " : setBackgroundColor not supported"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    return-void
.end method

.method public setBackgroundColorChangeListener(Ljava/lang/Object;Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setBackgroundColorChangeListener(Ljava/lang/Object;Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;)V

    goto :goto_0
.end method

.method public setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/16 v0, 0xd

    const-string/jumbo v1, " : setBackgroundDrawable not supported"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    return-void
.end method

.method public setBackgroundResource(I)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/16 v0, 0xd

    const-string/jumbo v1, " : setBackgroundResource not supported"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    return-void
.end method

.method public setBlankColor(I)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setBlankColor(I)V

    goto :goto_0
.end method

.method public setColorPickerListener(Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setColorPickerListener(Lcom/samsung/android/sdk/pen/engine/SpenColorPickerListener;)V

    goto :goto_0
.end method

.method public setControl(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setControl(Lcom/samsung/android/sdk/pen/engine/SpenControlBase;)V

    goto :goto_0
.end method

.method public setControlListener(Lcom/samsung/android/sdk/pen/engine/SpenControlListener;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setControlListener(Lcom/samsung/android/sdk/pen/engine/SpenControlListener;)V

    goto :goto_0
.end method

.method public setDottedLineEnabled(ZIII[FF)V
    .locals 7

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setDottedLineEnabled(ZIII[FF)V

    goto :goto_0
.end method

.method public setEraserChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setEraserChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenEraserChangeListener;)V

    goto :goto_0
.end method

.method public setEraserSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setEraserSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V

    goto :goto_0
.end method

.method public setFlickListener(Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setFlickListener(Lcom/samsung/android/sdk/pen/engine/SpenFlickListener;)V

    goto :goto_0
.end method

.method public setHighlight(Ljava/util/ArrayList;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHighlight(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public setHorizontalSmartScrollEnabled(ZLandroid/graphics/Rect;Landroid/graphics/Rect;II)V
    .locals 6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHorizontalSmartScrollEnabled(ZLandroid/graphics/Rect;Landroid/graphics/Rect;II)V

    goto :goto_0
.end method

.method public setHoverListener(Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHoverListener(Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;)V

    goto :goto_0
.end method

.method public setHyperTextListener(Lcom/samsung/android/sdk/pen/engine/SpenHyperTextListener;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHyperTextListener(Lcom/samsung/android/sdk/pen/engine/SpenHyperTextListener;)V

    goto :goto_0
.end method

.method public setHyperTextViewEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setHyperTextViewEnabled(Z)V

    goto :goto_0
.end method

.method public setLongPressListener(Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setLongPressListener(Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;)V

    goto :goto_0
.end method

.method public setMaxZoomRatio(F)Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setMaxZoomRatio(F)Z

    move-result v0

    goto :goto_0
.end method

.method public setMinZoomRatio(F)Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setMinZoomRatio(F)Z

    move-result v0

    goto :goto_0
.end method

.method public setPageDoc(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;IIF)Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setPageDoc(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;IIF)Z

    move-result v0

    goto :goto_0
.end method

.method public setPageDoc(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setPageDoc(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;Z)Z

    move-result v0

    goto :goto_0
.end method

.method public setPageEffectListener(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setPageEffectListener(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;)V

    goto :goto_0
.end method

.method public setPan(Landroid/graphics/PointF;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setPan(Landroid/graphics/PointF;)V

    goto :goto_0
.end method

.method public setPenChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setPenChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenPenChangeListener;)V

    goto :goto_0
.end method

.method public setPenDetachmentListener(Lcom/samsung/android/sdk/pen/engine/SpenPenDetachmentListener;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setPenDetachmentListener(Lcom/samsung/android/sdk/pen/engine/SpenPenDetachmentListener;)V

    goto :goto_0
.end method

.method public setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    goto :goto_0
.end method

.method public setPostDrawListener(Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setPostDrawListener(Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;)V

    goto :goto_0
.end method

.method public setPreDrawListener(Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setPreDrawListener(Lcom/samsung/android/sdk/pen/engine/SpenDrawListener;)V

    goto :goto_0
.end method

.method public setPreTouchListener(Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setPreTouchListener(Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;)V

    goto :goto_0
.end method

.method public setRemoverChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenRemoverChangeListener;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setRemoverChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenRemoverChangeListener;)V

    goto :goto_0
.end method

.method public setRemoverSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setRemoverSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V

    goto :goto_0
.end method

.method public setReplayListener(Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setReplayListener(Lcom/samsung/android/sdk/pen/engine/SpenReplayListener;)V

    goto :goto_0
.end method

.method public setReplayPosition(I)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setReplayPosition(I)V

    goto :goto_0
.end method

.method public setReplaySpeed(I)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setReplaySpeed(I)V

    goto :goto_0
.end method

.method public setScrollBarEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setScrollBarEnabled(Z)V

    goto :goto_0
.end method

.method public setSelectionChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenSelectionChangeListener;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setSelectionChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenSelectionChangeListener;)V

    goto :goto_0
.end method

.method public setSelectionSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setSelectionSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;)V

    goto :goto_0
.end method

.method public setSmartScaleEnabled(ZLandroid/graphics/Rect;IIF)V
    .locals 6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setSmartScaleEnabled(ZLandroid/graphics/Rect;IIF)V

    goto :goto_0
.end method

.method public setTextChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setTextChangeListener(Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;)V

    goto :goto_0
.end method

.method public setTextSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setTextSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    goto :goto_0
.end method

.method public setToolTipEnabled(Z)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setToolTipEnabled(Z)V

    goto :goto_0
.end method

.method public setToolTypeAction(II)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setToolTypeAction(II)V

    goto :goto_0
.end method

.method public setTouchListener(Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setTouchListener(Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;)V

    goto :goto_0
.end method

.method public setVerticalSmartScrollEnabled(ZLandroid/graphics/Rect;Landroid/graphics/Rect;II)V
    .locals 6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setVerticalSmartScrollEnabled(ZLandroid/graphics/Rect;Landroid/graphics/Rect;II)V

    goto :goto_0
.end method

.method public setZoom(FFF)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setZoom(FFF)V

    goto :goto_0
.end method

.method public setZoomListener(Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setZoomListener(Lcom/samsung/android/sdk/pen/engine/SpenZoomListener;)V

    goto :goto_0
.end method

.method public setZoomPadListener(Lcom/samsung/android/sdk/pen/engine/SpenZoomPadListener;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setZoomPadListener(Lcom/samsung/android/sdk/pen/engine/SpenZoomPadListener;)V

    goto :goto_0
.end method

.method public setZoomPadPosition(Landroid/graphics/PointF;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setZoomPadPosition(Landroid/graphics/PointF;)V

    goto :goto_0
.end method

.method public setZoomable(Z)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setZoomable(Z)V

    goto :goto_0
.end method

.method public startReplay()V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->startReplay()V

    goto :goto_0
.end method

.method public startTemporaryStroke()V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->startTemporaryStroke()V

    goto :goto_0
.end method

.method public startZoomPad()V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->startZoomPad()V

    goto :goto_0
.end method

.method public stopReplay()V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->stopReplay()V

    goto :goto_0
.end method

.method public stopTemporaryStroke()V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->stopTemporaryStroke()V

    goto :goto_0
.end method

.method public stopZoomPad()V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->stopZoomPad()V

    goto :goto_0
.end method

.method public takeStrokeFrame(Landroid/app/Activity;Landroid/view/ViewGroup;Ljava/util/List;Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->takeStrokeFrame(Landroid/app/Activity;Landroid/view/ViewGroup;Ljava/util/List;Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameListener;)V

    goto :goto_0
.end method

.method public update()V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->update()V

    goto :goto_0
.end method

.method public updateRedo([Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateRedo([Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;)V

    goto :goto_0
.end method

.method public updateScreen()V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateScreen()V

    goto :goto_0
.end method

.method public updateScreenFrameBuffer()V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateScreenFrameBuffer()V

    goto :goto_0
.end method

.method public updateUndo([Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSurfaceView;->mSpenInView:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateUndo([Lcom/samsung/android/sdk/pen/document/SpenPageDoc$HistoryUpdateInfo;)V

    goto :goto_0
.end method
