.class public Lcom/samsung/android/sdk/pen/engine/SpenControlList;
.super Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
.source "Twttr"


# static fields
.field private static final CONFIG_LIST_GROUP:Z = true


# instance fields
.field mFlipDirection:I

.field private mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

.field mObjectList:Ljava/util/ArrayList;

.field private mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

.field private final mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

.field private mTextBoxList:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$1;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTextBoxList:Ljava/util/ArrayList;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->initialize()V

    return-void
.end method

.method private drawListHighlight(Landroid/graphics/Canvas;)V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mObject:Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;)Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->getObjectList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->drawHighlightObject(Landroid/graphics/Canvas;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    goto :goto_0
.end method

.method private initialize()V
    .locals 2

    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mFlipDirection:I

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->set(Z)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTextBoxList:Ljava/util/ArrayList;

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    return-void
.end method

.method private updateGroup()V
    .locals 3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mDirtyFlag:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->reset()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mFlag:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mObject:Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;)Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mObject:Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;)Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;

    move-result-object v1

    invoke-super {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getObjectList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->appendObject(Ljava/util/ArrayList;)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->setObjectList(Ljava/util/ArrayList;)V

    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->fit()V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mObject:Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;)Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->getObjectList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->setObjectList(Ljava/util/ArrayList;)V

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTextBoxList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTextBoxList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTextBoxList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    invoke-super {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->close()V

    return-void

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getObjectText()Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {p0, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->onVisibleUpdated(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;Z)V

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->close()V

    goto :goto_0
.end method

.method public fit()V
    .locals 3

    invoke-super {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->fit()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTextBoxList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTextBoxList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V

    goto :goto_0
.end method

.method public getObject()Ljava/util/ArrayList;
    .locals 1

    invoke-super {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getObjectList()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getObjectList()Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mFlag:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mObject:Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;)Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->getObjectList()Ljava/util/ArrayList;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getObjectList()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method public getRect()Landroid/graphics/RectF;
    .locals 4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mFlag:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mObject:Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;)Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mObject:Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;)Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->getRect()Landroid/graphics/RectF;

    move-result-object v1

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-interface {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTempCoordinateInfo:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->relativeCoordinate(Landroid/graphics/RectF;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    :goto_0
    return-object v0

    :cond_1
    invoke-super {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getRect()Landroid/graphics/RectF;

    move-result-object v0

    goto :goto_0
.end method

.method public isGroup()Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mFlag:Z

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 8

    const/4 v7, 0x1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->getStyle()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->getObjectList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    invoke-super {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onAttachedToWindow()V

    return-void

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->getType()I

    move-result v1

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getWidth()I

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v6}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getHeight()I

    move-result v6

    invoke-direct {v3, v4, v1, v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;II)V

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-virtual {v3, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setObjectText(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    invoke-virtual {v3, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextBoxListener(Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;)V

    invoke-virtual {v3, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setViewModeEnabled(Z)V

    invoke-virtual {v3, v7}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTextBoxList:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->invalidate()V

    goto :goto_0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->updateGroup()V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->getStyle()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mFlag:Z

    if-eqz v0, :cond_1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->drawListHighlight(Landroid/graphics/Canvas;)V

    :cond_1
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onDraw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method protected onFlip(ILcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 1

    const-string/jumbo v0, ""

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->w(Ljava/lang/String;)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mFlipDirection:I

    xor-int/2addr v0, p1

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mFlipDirection:I

    invoke-super {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onFlip(ILcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    return-void
.end method

.method protected onObjectChanged()V
    .locals 1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->getObject()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mObjectList:Ljava/util/ArrayList;

    invoke-super {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onObjectChanged()V

    return-void
.end method

.method protected onVisibleUpdated(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;Z)V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    invoke-interface {v1, v0, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onVisibleUpdated(Ljava/util/ArrayList;Z)V

    :cond_0
    return-void
.end method

.method public setGroup(Z)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->set(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->reset()V

    return-void
.end method

.method public setObject(Ljava/util/ArrayList;)V
    .locals 1

    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->setObjectList(Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mGroup:Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mObject:Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;)Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;->appendObject(Ljava/util/ArrayList;)V

    return-void
.end method
