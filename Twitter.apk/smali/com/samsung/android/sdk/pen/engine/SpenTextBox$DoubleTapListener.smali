.class Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/GestureDetector$OnDoubleTapListener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsViewMode:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$27(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mIsTyping:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$28(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->measureText()V

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mLineCount:I
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$29(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I

    move-result v0

    if-lez v0, :cond_b

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v0

    if-nez v0, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mObjectText:Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    const/4 v0, 0x2

    new-array v4, v0, [F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    aput v0, v4, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    aput v0, v4, v1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mScaleMatrix:Landroid/graphics/Matrix;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$30(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/graphics/Matrix;

    move-result-object v0

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$31(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/graphics/Matrix;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTempMatrix:Landroid/graphics/Matrix;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$31(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/graphics/Matrix;->mapPoints([F)V

    aget v0, v4, v1

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mNativeTextView:I
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$32(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)I

    move-result v5

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->native_getPan(I)F
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$33(I)F

    move-result v5

    add-float/2addr v0, v5

    aput v0, v4, v1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    aget v5, v4, v1

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getLineForVertical(F)I
    invoke-static {v0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;F)I

    move-result v5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    aget v6, v4, v2

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getOffsetForHorizontal(IF)I
    invoke-static {v0, v5, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$34(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;IF)I

    move-result v0

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$35(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)[Landroid/graphics/RectF;

    move-result-object v6

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$36(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)[I

    move-result-object v6

    if-nez v6, :cond_5

    :cond_4
    move v0, v1

    goto/16 :goto_0

    :cond_5
    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEndIndex:[I
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$36(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)[I

    move-result-object v6

    aget v5, v6, v5

    if-ne v0, v5, :cond_6

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextRect:[Landroid/graphics/RectF;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$35(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)[Landroid/graphics/RectF;

    move-result-object v5

    aget-object v5, v5, v0

    iget v5, v5, Landroid/graphics/RectF;->right:F

    aget v4, v4, v2

    cmpg-float v4, v5, v4

    if-gez v4, :cond_6

    add-int/lit8 v0, v0, 0x1

    :cond_6
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-lt v0, v4, :cond_8

    add-int/lit8 v4, v0, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    :goto_1
    const/16 v4, 0x20

    if-eq v3, v4, :cond_7

    const/16 v4, 0x9

    if-eq v3, v4, :cond_7

    const/16 v4, 0xa

    if-eq v3, v4, :cond_7

    const/16 v4, 0xd

    if-ne v3, v4, :cond_9

    :cond_7
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v3, v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v2, v0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onSelectionChanged(II)V

    :goto_2
    move v0, v1

    goto/16 :goto_0

    :cond_8
    invoke-virtual {v3, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    goto :goto_1

    :cond_9
    new-instance v3, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;)V

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->findWord(ILcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;)Z
    invoke-static {v4, v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$37(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;ILcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;)Z

    move-result v4

    if-eqz v4, :cond_a

    iget v0, v3, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->startIndex:I

    iget v3, v3, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$Word;->endIndex:I

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v4, v0, v3, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto :goto_2

    :cond_a
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v3, v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto :goto_2

    :cond_b
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$14(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    move-result-object v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, v2, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->onSelectionChanged(II)V

    :cond_c
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, v2, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$DoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showCursorHandle()V
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$13(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    goto :goto_2
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
