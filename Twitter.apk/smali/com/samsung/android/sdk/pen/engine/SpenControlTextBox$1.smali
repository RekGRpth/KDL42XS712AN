.class Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onExceedLimit()V
    .locals 0

    return-void
.end method

.method public onFocusChanged(Z)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;->onFocusChanged(Z)V

    :cond_0
    return-void
.end method

.method public onMoreButtonDown(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;->onMoreButtonDown(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V

    :cond_0
    return-void
.end method

.method public onObjectChanged(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->fit()V
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->invalidate()V

    return-void
.end method

.method public onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    :cond_0
    return-void
.end method

.method public onRequestScroll(FF)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->onRequestScroll(FF)V

    return-void
.end method

.method public onSelectionChanged(II)Z
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "onSelectionChanged is called start : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " end : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;->onSelectionChanged(II)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onSettingTextInfoChanged(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v0

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v0

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v0

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v0

    iget-object v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v0

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v0

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineIndent:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineIndent:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v0

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v0

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;->onSettingTextInfoChanged(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    :cond_1
    return-void
.end method

.method public onUndo()V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->onObjectChanged()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;Z)V

    return-void
.end method

.method public onVisibleUpdated(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;Z)V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getObjectList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onVisibleUpdated(Ljava/util/ArrayList;Z)V

    :cond_0
    return-void
.end method
