.class Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/GestureDetector$OnGestureListener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V

    return-void
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v2

    if-ne v2, v0, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$25(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$25(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    move-result-object v1

    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)V

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 14

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->isSkipTouch:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$18(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLongPressListener:Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$19(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mLongPressListener:Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$19(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenLongPressListener;->onLongPressed(Landroid/view/MotionEvent;)V

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->isEraserCursor:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$20(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$21(Lcom/samsung/android/sdk/pen/engine/SpenInView;Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    const/high16 v1, -0x3d380000    # -100.0f

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$22(Lcom/samsung/android/sdk/pen/engine/SpenInView;F)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    const/high16 v1, -0x3d380000    # -100.0f

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$23(Lcom/samsung/android/sdk/pen/engine/SpenInView;F)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$24(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/graphics/PointF;

    move-result-object v0

    const/high16 v1, -0x3d380000    # -100.0f

    iput v1, v0, Landroid/graphics/PointF;->x:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mCirclePoint:Landroid/graphics/PointF;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$24(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/graphics/PointF;

    move-result-object v0

    const/high16 v1, -0x3d380000    # -100.0f

    iput v1, v0, Landroid/graphics/PointF;->y:F

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$25(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsSmartScale:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$26(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsSmartHorizontal:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$27(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mIsSmartVertical:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$28(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$29(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F

    move-result v1

    div-float/2addr v0, v1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$30(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F

    move-result v1

    add-float/2addr v0, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$29(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F

    move-result v2

    div-float/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$31(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F

    move-result v2

    add-float/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v2

    const/4 v3, 0x6

    invoke-virtual {v2, v3, v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->findTopObjectAtPosition(IFF)Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$25(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->getCenterX()F

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$25(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->getCenterY()F

    move-result v4

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setZoom(FFF)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaX:F
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$30(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F

    move-result v2

    sub-float/2addr v0, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$29(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F

    move-result v2

    mul-float v5, v0, v2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mDeltaY:F
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$31(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F

    move-result v0

    sub-float v0, v1, v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatio:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$29(Lcom/samsung/android/sdk/pen/engine/SpenInView;)F

    move-result v1

    mul-float v6, v0, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPressure()F

    move-result v7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSize()F

    move-result v8

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getMetaState()I

    move-result v9

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getXPrecision()F

    move-result v10

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getYPrecision()F

    move-result v11

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDeviceId()I

    move-result v12

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEdgeFlags()I

    move-result v13

    invoke-static/range {v0 .. v13}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v2

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_onLongPress(ILandroid/view/MotionEvent;I)Z
    invoke-static {v1, v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$32(ILandroid/view/MotionEvent;I)Z

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_onLongPress(ILandroid/view/MotionEvent;I)Z
    invoke-static {v0, p1, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$32(ILandroid/view/MotionEvent;I)Z

    goto/16 :goto_0
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0

    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return v2

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->nativeCanvas:I
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I

    move-result v0

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenInView;->native_onSingleTapUp(ILandroid/view/MotionEvent;I)Z
    invoke-static {v0, p1, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$17(ILandroid/view/MotionEvent;I)Z

    goto :goto_0
.end method
