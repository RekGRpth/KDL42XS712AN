.class public Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static mList:Ljava/util/LinkedList;

.field private static mProtect:I

.field private static mRemoveReservedList:Ljava/util/LinkedList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->mList:Ljava/util/LinkedList;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->mRemoveReservedList:Ljava/util/LinkedList;

    const/4 v0, 0x0

    sput v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->mProtect:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static native _createNativeBitmap(Landroid/graphics/Bitmap;)I
.end method

.method private static native _deleteNativeAncenstor(I)V
.end method

.method private static native _getNativeBitmapRef(I)I
.end method

.method private static bindBitmap(Landroid/graphics/Bitmap;)I
    .locals 3

    sget-object v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->mList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    sget v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->mProtect:I

    if-lez v0, :cond_2

    sget-object v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->mRemoveReservedList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_4

    :cond_2
    new-instance v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;-><init>()V

    iput-object p0, v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->jbitmap:Landroid/graphics/Bitmap;

    invoke-static {p0}, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->_createNativeBitmap(Landroid/graphics/Bitmap;)I

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->handle:I

    iget v1, v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->handle:I

    if-nez v1, :cond_5

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->jbitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->jbitmap:Landroid/graphics/Bitmap;

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->jbitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v0, v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->handle:I

    goto :goto_0

    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->jbitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->mList:Ljava/util/LinkedList;

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    iget v0, v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->handle:I

    goto :goto_0

    :cond_5
    sget-object v1, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->mList:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->printLog()V

    iget v0, v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->handle:I

    goto :goto_0
.end method

.method private static bindMutableClone(I)I
    .locals 5

    const/4 v1, 0x0

    sget-object v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->mList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    return v0

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;

    iget v3, v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->handle:I

    if-ne v3, p0, :cond_0

    new-instance v2, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;

    invoke-direct {v2}, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;-><init>()V

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->jbitmap:Landroid/graphics/Bitmap;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->jbitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    const/4 v4, 0x1

    invoke-virtual {v3, v0, v4}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, v2, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->jbitmap:Landroid/graphics/Bitmap;

    iget-object v0, v2, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->jbitmap:Landroid/graphics/Bitmap;

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->_createNativeBitmap(Landroid/graphics/Bitmap;)I

    move-result v0

    iput v0, v2, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->handle:I

    iget v0, v2, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->handle:I

    if-nez v0, :cond_2

    iget-object v0, v2, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->jbitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    const/4 v0, 0x0

    iput-object v0, v2, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->jbitmap:Landroid/graphics/Bitmap;

    move v0, v1

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->mList:Ljava/util/LinkedList;

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->printLog()V

    iget v0, v2, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->handle:I

    goto :goto_0
.end method

.method private static bitmapCount()I
    .locals 1

    sget-object v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->mList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    return v0
.end method

.method private static createBitmap(Landroid/graphics/Bitmap;)I
    .locals 3

    const/4 v0, 0x0

    if-nez p0, :cond_0

    const-string/jumbo v1, "BitmapManager"

    const-string/jumbo v2, "createBitmap(bitmap==null) failed"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_0
    new-instance v1, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;

    invoke-direct {v1}, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;-><init>()V

    iput-object p0, v1, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->jbitmap:Landroid/graphics/Bitmap;

    invoke-static {p0}, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->_createNativeBitmap(Landroid/graphics/Bitmap;)I

    move-result v2

    iput v2, v1, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->handle:I

    iget v2, v1, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->handle:I

    if-nez v2, :cond_1

    iget-object v2, v1, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->jbitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->jbitmap:Landroid/graphics/Bitmap;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->mList:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->printLog()V

    iget v0, v1, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->handle:I

    goto :goto_0
.end method

.method private static createBitmap(Ljava/lang/String;II)I
    .locals 4

    const/4 v0, 0x0

    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string/jumbo v3, "file"

    invoke-virtual {v2, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-nez v3, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    :cond_0
    :goto_0
    if-nez p1, :cond_2

    if-nez p2, :cond_2

    invoke-static {p0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    :goto_1
    if-nez v1, :cond_3

    const-string/jumbo v1, "BitmapManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "createBitmap("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ") failed"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    return v0

    :cond_1
    const-string/jumbo v1, "spd"

    invoke-virtual {v2, v1}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    invoke-static {p0, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    iget v2, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    div-int/2addr v2, p1

    iget v3, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    div-int/2addr v3, p2

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    iput v2, v1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    iput-boolean v0, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    invoke-static {p0, v1}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_1

    :cond_3
    new-instance v2, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;

    invoke-direct {v2}, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;-><init>()V

    iput-object v1, v2, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->jbitmap:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->_createNativeBitmap(Landroid/graphics/Bitmap;)I

    move-result v1

    iput v1, v2, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->handle:I

    iget v1, v2, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->handle:I

    if-nez v1, :cond_4

    iget-object v1, v2, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->jbitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    const/4 v1, 0x0

    iput-object v1, v2, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->jbitmap:Landroid/graphics/Bitmap;

    goto :goto_2

    :cond_4
    sget-object v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->mList:Ljava/util/LinkedList;

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->printLog()V

    iget v0, v2, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->handle:I

    goto :goto_2
.end method

.method private static createBitmap([IIIIZ)I
    .locals 4

    const/4 v0, 0x0

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p0, p1, p2, v1}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "BitmapManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "createBitmap(width="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", height="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ") failed"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_0
    new-instance v2, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;

    invoke-direct {v2}, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;-><init>()V

    iput-object v1, v2, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->jbitmap:Landroid/graphics/Bitmap;

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->_createNativeBitmap(Landroid/graphics/Bitmap;)I

    move-result v1

    iput v1, v2, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->handle:I

    iget v1, v2, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->handle:I

    if-nez v1, :cond_1

    iget-object v1, v2, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->jbitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    const/4 v1, 0x0

    iput-object v1, v2, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->jbitmap:Landroid/graphics/Bitmap;

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->mList:Ljava/util/LinkedList;

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->printLog()V

    iget v0, v2, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->handle:I

    goto :goto_0
.end method

.method private static decodeBitmapFile(Ljava/lang/String;)I
    .locals 1

    invoke-static {p0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->bindBitmap(Landroid/graphics/Bitmap;)I

    move-result v0

    goto :goto_0
.end method

.method private static findBitmap(I)Landroid/graphics/Bitmap;
    .locals 3

    sget-object v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->mList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;

    iget v2, v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->handle:I

    if-ne v2, p0, :cond_0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->jbitmap:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method private static isBuildTypeEngMode()Z
    .locals 2

    const-string/jumbo v0, "eng"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static printLog()V
    .locals 3

    const-string/jumbo v0, "BitmapManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "\u2193\u2193\u2193\u2193 Bitmap Manager (P"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v2, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->mProtect:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ") \u2193\u2193\u2193\u2193"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v0, "BitmapManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Count = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->mList:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v0, "BitmapManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Reserved Count = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->mRemoveReservedList:Ljava/util/LinkedList;

    invoke-virtual {v2}, Ljava/util/LinkedList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v0, "BitmapManager"

    const-string/jumbo v1, "\u2191\u2191\u2191\u2191\u2191\u2191\u2191\u2191\u2191\u2191\u2191\u2191\u2191\u2191\u2191\u2191\u2191\u2191\u2191\u2191\u2191\u2191\u2191\u2191\u2191\u2191\u2191\u2191\u2191"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private static protectRemoval()V
    .locals 1

    sget v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->mProtect:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->mProtect:I

    return-void
.end method

.method private static releaseBitmap(I)I
    .locals 4

    sget-object v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->mList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;

    iget v1, v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->handle:I

    if-ne v1, p0, :cond_0

    iget v1, v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->handle:I

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->_getNativeBitmapRef(I)I

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_3

    sget v1, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->mProtect:I

    if-lez v1, :cond_2

    sget-object v1, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->mRemoveReservedList:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->printLog()V

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v1, v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->jbitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    iget v0, v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->handle:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->_deleteNativeAncenstor(I)V

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method private static releaseBitmap(Landroid/graphics/Bitmap;)I
    .locals 4

    sget-object v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->mList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->jbitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->handle:I

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->_getNativeBitmapRef(I)I

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_3

    sget v1, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->mProtect:I

    if-lez v1, :cond_2

    sget-object v1, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->mRemoveReservedList:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->printLog()V

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v1, v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->jbitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    iget v0, v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->handle:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->_deleteNativeAncenstor(I)V

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method private static saveBitmap(ILjava/lang/String;)Z
    .locals 5

    const/4 v1, 0x0

    sget-object v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->mList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :cond_1
    :goto_0
    return v0

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;

    iget v3, v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->handle:I

    if-ne v3, p0, :cond_0

    new-instance v4, Ljava/io/File;

    invoke-direct {v4, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x0

    :try_start_0
    invoke-virtual {v4}, Ljava/io/File;->createNewFile()Z

    move-result v2

    if-eqz v2, :cond_5

    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v0, v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->jbitmap:Landroid/graphics/Bitmap;

    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x64

    invoke-virtual {v0, v3, v4, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    :goto_1
    if-eqz v2, :cond_1

    :try_start_2
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    const-string/jumbo v1, "BitmapManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "saveBitmap("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ") failed"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v2, v3

    :goto_2
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const-string/jumbo v0, "BitmapManager"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "saveBitmap("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ") failed"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v2, :cond_4

    :try_start_4
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    move v0, v1

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    const-string/jumbo v0, "BitmapManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "saveBitmap("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ") failed"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    move-object v2, v3

    :goto_3
    if-eqz v2, :cond_3

    :try_start_5
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    :cond_3
    :goto_4
    throw v0

    :catch_3
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    const-string/jumbo v1, "BitmapManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "saveBitmap("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ") failed"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    :catchall_1
    move-exception v0

    goto :goto_3

    :catch_4
    move-exception v0

    goto :goto_2

    :cond_4
    move v0, v1

    goto/16 :goto_0

    :cond_5
    move-object v2, v3

    move v0, v1

    goto/16 :goto_1
.end method

.method private static unprotectRemoval()V
    .locals 3

    sget v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->mProtect:I

    add-int/lit8 v0, v0, -0x1

    sput v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->mProtect:I

    sget v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->mProtect:I

    if-nez v0, :cond_0

    sget-object v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->mRemoveReservedList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->mRemoveReservedList:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    :cond_0
    return-void

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->jbitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    iget v0, v0, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager$Info;->handle:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenBitmapManager;->_deleteNativeAncenstor(I)V

    goto :goto_0
.end method
