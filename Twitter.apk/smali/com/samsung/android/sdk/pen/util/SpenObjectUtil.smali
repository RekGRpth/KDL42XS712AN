.class public Lcom/samsung/android/sdk/pen/util/SpenObjectUtil;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static native ObjectUtil_getObjectExtraAllData(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;Ljava/lang/String;)[B
.end method

.method private static native ObjectUtil_setObjectExtraAllData(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;Ljava/lang/String;[BI)Z
.end method

.method public static getObjectExtraAllData(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;Ljava/lang/String;)[B
    .locals 3

    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    invoke-static {p0, p1}, Lcom/samsung/android/sdk/pen/util/SpenObjectUtil;->ObjectUtil_getObjectExtraAllData(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;Ljava/lang/String;)[B

    move-result-object v0

    if-nez v0, :cond_3

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    const/16 v2, 0x11

    if-ne v1, v2, :cond_2

    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "is not correct"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    :cond_3
    return-object v0
.end method

.method public static setObjectExtraAllData(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;Ljava/lang/String;[B)V
    .locals 3

    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_1
    array-length v0, p2

    invoke-static {p0, p1, p2, v0}, Lcom/samsung/android/sdk/pen/util/SpenObjectUtil;->ObjectUtil_setObjectExtraAllData(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;Ljava/lang/String;[BI)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    const/16 v1, 0x11

    if-ne v0, v1, :cond_2

    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "is not correct"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenInvalidPasswordException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    :cond_3
    return-void
.end method
