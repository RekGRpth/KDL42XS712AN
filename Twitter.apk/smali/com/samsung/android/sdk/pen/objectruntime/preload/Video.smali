.class public Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface;


# static fields
.field private static final EXTRADATASTRING_VIDEOPATH_KEY:Ljava/lang/String; = "VideoPath"

.field private static final GETEXTRADATASTRING_FUNCTION:Ljava/lang/String; = "getExtraDataString"

.field private static final GETEXTRADATASTRING_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;

.field private static HASH_KEY_IMAGE_MARGIN:I = 0x0

.field private static HASH_KEY_IMAGE_SIZE:I = 0x0

.field private static HASH_KEY_PIXEL_1080_1920:I = 0x0

.field private static HASH_KEY_PIXEL_2560_1600:I = 0x0

.field private static HASH_KEY_PIXEL_720_1280:I = 0x0

.field private static HASH_KEY_PIXEL_DEFAULT:I = 0x0

.field private static final SETEXTRADATASTRING_FUNCTION:Ljava/lang/String; = "setExtraDataString"

.field private static final SETEXTRADATASTRING_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;

.field private static final SETIMAGE_FUNCTION:Ljava/lang/String; = "setImage"

.field private static final SETIMAGE_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;

.field private static final SETRECT_FUNCTION:Ljava/lang/String; = "setRect"

.field private static final SETRECT_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;

.field private static final SETRESIZEOPTION_FUNCTION:Ljava/lang/String; = "setResizeOption"

.field private static final SETRESIZEOPTION_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;

.field private static final SETROTATABLE_FUNCTION:Ljava/lang/String; = "setRotatable"

.field private static final SETROTATABLE_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;

.field private static final SETVISIBILITY_FUNCTION:Ljava/lang/String; = "setVisibility"

.field private static final SETVISIBILITY_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;

.field private static final SETWFDTCPDISABLE_FUNCTION:Ljava/lang/String; = "setWFDTcpDisable"

.field private static final SETWFDTCPDISABLE_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;

.field private static final TAG:Ljava/lang/String; = "Video"

.field private static final VIDEOPLAY_IMAGE_NAME:Ljava/lang/String; = "snote_insert_video_icon_cue"

.field private static final VIDEOPLAY_IMAGE_UX_TABLE:Ljava/util/HashMap;


# instance fields
.field private fm:Landroid/app/FragmentManager;

.field private mActivity:Landroid/app/Activity;

.field mAudioLisner:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field mAudioManager:Landroid/media/AudioManager;

.field private mFragment:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;

.field private mFragmentListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;

.field private mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface$UpdateListener;

.field private mObject:Ljava/lang/Object;

.field private mPan:Landroid/graphics/PointF;

.field private mRatio:F

.field private mRect:Landroid/graphics/RectF;

.field private mStartFramePosition:Landroid/graphics/PointF;

.field private mVideoFilePath:Ljava/lang/String;

.field private mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

.field private mVideoViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

.field private mVideoViewListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView$WrapperVideoViewListener;

.field private mViewGroup:Landroid/view/ViewGroup;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v3, 0x2

    const/16 v6, 0x16

    const/4 v5, 0x7

    const/4 v2, 0x1

    const/4 v4, 0x0

    new-array v0, v2, [Ljava/lang/Class;

    const-class v1, Landroid/graphics/Bitmap;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->SETIMAGE_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;

    new-array v0, v3, [Ljava/lang/Class;

    const-class v1, Landroid/graphics/RectF;

    aput-object v1, v0, v4

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v1, v0, v2

    sput-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->SETRECT_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;

    new-array v0, v2, [Ljava/lang/Class;

    const-class v1, Ljava/lang/String;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->GETEXTRADATASTRING_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;

    new-array v0, v3, [Ljava/lang/Class;

    const-class v1, Ljava/lang/String;

    aput-object v1, v0, v4

    const-class v1, Ljava/lang/String;

    aput-object v1, v0, v2

    sput-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->SETEXTRADATASTRING_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;

    new-array v0, v2, [Ljava/lang/Class;

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->SETROTATABLE_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;

    new-array v0, v2, [Ljava/lang/Class;

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->SETVISIBILITY_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;

    new-array v0, v2, [Ljava/lang/Class;

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->SETRESIZEOPTION_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;

    new-array v0, v2, [Ljava/lang/Class;

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v1, v0, v4

    sput-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->SETWFDTCPDISABLE_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;

    sput v4, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->HASH_KEY_IMAGE_SIZE:I

    sget v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->HASH_KEY_IMAGE_SIZE:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->HASH_KEY_IMAGE_MARGIN:I

    const/16 v0, 0xbb8

    sput v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->HASH_KEY_PIXEL_1080_1920:I

    const/16 v0, 0x1040

    sput v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->HASH_KEY_PIXEL_2560_1600:I

    const/16 v0, 0x7d0

    sput v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->HASH_KEY_PIXEL_720_1280:I

    sget v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->HASH_KEY_PIXEL_1080_1920:I

    sput v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->HASH_KEY_PIXEL_DEFAULT:I

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->VIDEOPLAY_IMAGE_UX_TABLE:Ljava/util/HashMap;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v4, v4, v6, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, v4, v5, v5, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sget v3, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->HASH_KEY_IMAGE_SIZE:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->HASH_KEY_IMAGE_MARGIN:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->VIDEOPLAY_IMAGE_UX_TABLE:Ljava/util/HashMap;

    sget v1, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->HASH_KEY_PIXEL_1080_1920:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Landroid/graphics/Rect;

    const/16 v1, 0x21

    const/16 v2, 0x21

    invoke-direct {v0, v4, v4, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v1, Landroid/graphics/Rect;

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v1, v4, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sget v3, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->HASH_KEY_IMAGE_SIZE:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->HASH_KEY_IMAGE_MARGIN:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->VIDEOPLAY_IMAGE_UX_TABLE:Ljava/util/HashMap;

    sget v1, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->HASH_KEY_PIXEL_2560_1600:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v4, v4, v6, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, v4, v5, v5, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sget v3, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->HASH_KEY_IMAGE_SIZE:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->HASH_KEY_IMAGE_MARGIN:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->VIDEOPLAY_IMAGE_UX_TABLE:Ljava/util/HashMap;

    sget v1, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->HASH_KEY_PIXEL_720_1280:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->cancel(I)V

    return-void
.end method

.method static synthetic access$10()[Ljava/lang/Class;
    .locals 1

    sget-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->SETRECT_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;

    return-object v0
.end method

.method static synthetic access$11(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mInvoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$12(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;Landroid/graphics/Bitmap;Landroid/graphics/RectF;)Landroid/graphics/Bitmap;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->createPlayVideoBitmap(Landroid/graphics/Bitmap;Landroid/graphics/RectF;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$13()[Ljava/lang/Class;
    .locals 1

    sget-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->SETIMAGE_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;

    return-object v0
.end method

.method static synthetic access$14()[Ljava/lang/Class;
    .locals 1

    sget-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->SETEXTRADATASTRING_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;

    return-object v0
.end method

.method static synthetic access$15()[Ljava/lang/Class;
    .locals 1

    sget-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->SETROTATABLE_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;

    return-object v0
.end method

.method static synthetic access$16()[Ljava/lang/Class;
    .locals 1

    sget-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->SETRESIZEOPTION_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;

    return-object v0
.end method

.method static synthetic access$17(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface$UpdateListener;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface$UpdateListener;

    return-object v0
.end method

.method static synthetic access$18(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->complete()V

    return-void
.end method

.method static synthetic access$19()[Ljava/lang/Class;
    .locals 1

    sget-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->SETVISIBILITY_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mActivity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoFilePath:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoFilePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;IILandroid/graphics/RectF;)Landroid/graphics/RectF;
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->getRatioRect(IILandroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;Landroid/graphics/RectF;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRect:Landroid/graphics/RectF;

    return-void
.end method

.method static synthetic access$7(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Landroid/graphics/RectF;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRect:Landroid/graphics/RectF;

    return-object v0
.end method

.method static synthetic access$8(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;Landroid/graphics/RectF;)Landroid/graphics/RectF;
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->convertAbsolute(Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$9(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mObject:Ljava/lang/Object;

    return-object v0
.end method

.method private cancel(I)V
    .locals 2

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->cleanResource()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface$UpdateListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mObject:Ljava/lang/Object;

    invoke-interface {v0, p1, v1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface$UpdateListener;->onCanceled(ILjava/lang/Object;)V

    return-void
.end method

.method private cleanResource()V
    .locals 5

    const/4 v2, 0x1

    const/4 v4, 0x0

    new-array v0, v2, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mObject:Ljava/lang/Object;

    const-string/jumbo v2, "setVisibility"

    sget-object v3, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->SETVISIBILITY_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;

    invoke-direct {p0, v1, v2, v3, v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mInvoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface$UpdateListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRect:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mObject:Ljava/lang/Object;

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface$UpdateListener;->onObjectUpdated(Landroid/graphics/RectF;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mAudioLisner:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mViewGroup:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mFragment:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mFragment:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;->setVideoIntentFragmentListener(Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->fm:Landroid/app/FragmentManager;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mFragment:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->fm:Landroid/app/FragmentManager;

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mFragment:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    :cond_1
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mFragment:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;

    :cond_2
    return-void
.end method

.method private complete()V
    .locals 2

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->cleanResource()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface$UpdateListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mObject:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface$UpdateListener;->onCompleted(Ljava/lang/Object;)V

    return-void
.end method

.method private convertAbsolute(Landroid/graphics/RectF;)Landroid/graphics/RectF;
    .locals 3

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iget v1, p1, Landroid/graphics/RectF;->left:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mStartFramePosition:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRatio:F

    div-float/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mPan:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->left:F

    iget v1, p1, Landroid/graphics/RectF;->right:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mStartFramePosition:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRatio:F

    div-float/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mPan:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    iget v1, p1, Landroid/graphics/RectF;->top:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mStartFramePosition:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRatio:F

    div-float/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mPan:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->top:F

    iget v1, p1, Landroid/graphics/RectF;->bottom:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mStartFramePosition:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRatio:F

    div-float/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mPan:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    return-object v0
.end method

.method private createPlayVideoBitmap(Landroid/graphics/Bitmap;Landroid/graphics/RectF;)Landroid/graphics/Bitmap;
    .locals 12

    const/4 v9, 0x0

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mActivity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "Video"

    const-string/jumbo v2, "PackageManager is null."

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    const-string/jumbo v2, "com.samsung.android.sdk.spen30"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    if-nez v3, :cond_1

    const-string/jumbo v1, "Video"

    const-string/jumbo v2, "DisplayMetrics Get is failed"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    :cond_1
    const-string/jumbo v2, "snote_insert_video_icon_cue"

    const-string/jumbo v4, "drawable"

    const-string/jumbo v5, "com.samsung.android.sdk.spen30"

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_2

    const-string/jumbo v1, "Video"

    const-string/jumbo v2, "Resource is not founded"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    :try_start_1
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;
    :try_end_1
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenScreenCodecDecoder;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v4

    if-nez v4, :cond_3

    const-string/jumbo v1, "Video"

    const-string/jumbo v2, "The bitmap of resource is null."

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Landroid/content/res/Resources$NotFoundException;->printStackTrace()V

    goto :goto_0

    :cond_3
    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v2

    float-to-int v2, v2

    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    if-nez v2, :cond_4

    const-string/jumbo v1, "Video"

    const-string/jumbo v2, "The workBitmap of result is null. out fo memory"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    :cond_4
    new-instance v5, Landroid/graphics/Canvas;

    invoke-direct {v5, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-virtual {v6, v9, v9, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-virtual {v7, v9, v9, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    new-instance v8, Landroid/graphics/Paint;

    const/4 v0, 0x7

    invoke-direct {v8, v0}, Landroid/graphics/Paint;-><init>(I)V

    invoke-virtual {v5, p1, v6, v7, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-virtual {v6, v9, v9, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    iget v0, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v1, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    add-int/2addr v0, v1

    sget-object v1, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->VIDEOPLAY_IMAGE_UX_TABLE:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v1, v9}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    sget v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->HASH_KEY_PIXEL_DEFAULT:I

    move v1, v0

    :goto_1
    sget-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->VIDEOPLAY_IMAGE_UX_TABLE:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    sget v9, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->HASH_KEY_IMAGE_MARGIN:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    sget-object v9, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->VIDEOPLAY_IMAGE_UX_TABLE:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    sget v9, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->HASH_KEY_IMAGE_SIZE:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v1, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v10

    int-to-float v10, v10

    iget v11, v3, Landroid/util/DisplayMetrics;->density:F

    invoke-direct {p0, v10, v11}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->getIntValueAppliedDensity(FF)I

    move-result v10

    sub-int/2addr v9, v10

    iget v10, v0, Landroid/graphics/Rect;->right:I

    int-to-float v10, v10

    iget v11, v3, Landroid/util/DisplayMetrics;->density:F

    invoke-direct {p0, v10, v11}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->getIntValueAppliedDensity(FF)I

    move-result v10

    sub-int/2addr v9, v10

    iput v9, v7, Landroid/graphics/Rect;->left:I

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    iget v9, v3, Landroid/util/DisplayMetrics;->density:F

    invoke-direct {p0, v0, v9}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->getIntValueAppliedDensity(FF)I

    move-result v0

    iput v0, v7, Landroid/graphics/Rect;->top:I

    iget v0, v7, Landroid/graphics/Rect;->left:I

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v9

    int-to-float v9, v9

    iget v10, v3, Landroid/util/DisplayMetrics;->density:F

    invoke-direct {p0, v9, v10}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->getIntValueAppliedDensity(FF)I

    move-result v9

    add-int/2addr v0, v9

    iput v0, v7, Landroid/graphics/Rect;->right:I

    iget v0, v7, Landroid/graphics/Rect;->top:I

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    invoke-direct {p0, v1, v3}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->getIntValueAppliedDensity(FF)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, v7, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v5, v4, v6, v7, v8}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    move-object v0, v2

    goto/16 :goto_0

    :cond_5
    move v1, v0

    goto/16 :goto_1
.end method

.method private getIntValueAppliedDensity(FF)I
    .locals 1

    mul-float v0, p1, p2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method private getRatioRect(IILandroid/graphics/RectF;)Landroid/graphics/RectF;
    .locals 7

    const/high16 v6, 0x40000000    # 2.0f

    const v3, 0x3f2aaaab

    int-to-float v0, p1

    int-to-float v1, p2

    div-float v2, v0, v1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRect:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRect:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    if-le p1, p2, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRect:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    mul-float v1, v0, v3

    div-float v0, v1, v2

    :goto_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v3}, Landroid/view/ViewGroup;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v4, v2

    div-float v5, v1, v6

    sub-float/2addr v4, v5

    iput v4, p3, Landroid/graphics/RectF;->left:F

    int-to-float v4, v3

    div-float v5, v0, v6

    sub-float/2addr v4, v5

    iput v4, p3, Landroid/graphics/RectF;->top:F

    iget v4, p3, Landroid/graphics/RectF;->left:F

    add-float/2addr v4, v1

    iput v4, p3, Landroid/graphics/RectF;->right:F

    iget v4, p3, Landroid/graphics/RectF;->top:F

    add-float/2addr v4, v0

    iput v4, p3, Landroid/graphics/RectF;->bottom:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRatio:F

    mul-float/2addr v1, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRatio:F

    mul-float/2addr v0, v4

    new-instance v4, Landroid/graphics/RectF;

    invoke-direct {v4}, Landroid/graphics/RectF;-><init>()V

    int-to-float v2, v2

    div-float v5, v1, v6

    sub-float/2addr v2, v5

    iput v2, v4, Landroid/graphics/RectF;->left:F

    int-to-float v2, v3

    div-float v3, v0, v6

    sub-float/2addr v2, v3

    iput v2, v4, Landroid/graphics/RectF;->top:F

    iget v2, v4, Landroid/graphics/RectF;->left:F

    add-float/2addr v1, v2

    iput v1, v4, Landroid/graphics/RectF;->right:F

    iget v1, v4, Landroid/graphics/RectF;->top:F

    add-float/2addr v0, v1

    iput v0, v4, Landroid/graphics/RectF;->bottom:F

    return-object v4

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRect:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    mul-float/2addr v0, v3

    mul-float v1, v0, v2

    goto :goto_0

    :cond_1
    if-le p1, p2, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRect:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    mul-float v1, v0, v3

    div-float v0, v1, v2

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRect:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    mul-float/2addr v0, v3

    mul-float v1, v0, v2

    goto :goto_0
.end method

.method private varargs mInvoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-virtual {v0, p1, p4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_1

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_1
.end method

.method private startVideoPlay()Z
    .locals 6

    const/4 v0, 0x0

    const/4 v1, 0x1

    new-instance v2, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mActivity:Landroid/app/Activity;

    invoke-direct {v2, v3}, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    if-nez v2, :cond_0

    const-string/jumbo v1, "Video"

    const-string/jumbo v2, "VideoView is null."

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_0
    new-instance v2, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$3;

    invoke-direct {v2, p0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$3;-><init>(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoViewListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView$WrapperVideoViewListener;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoViewListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView$WrapperVideoViewListener;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;->setListener(Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView$WrapperVideoViewListener;)V

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;->requestFocusFromTouch()Z

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    const-string/jumbo v4, "setWFDTcpDisable"

    sget-object v5, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->SETWFDTCPDISABLE_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;

    invoke-direct {p0, v3, v4, v5, v2}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mInvoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoFilePath:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;->setVideoPath(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    invoke-virtual {v2, v1}, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;->setZOrderMediaOverlay(Z)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mViewGroup:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mAudioManager:Landroid/media/AudioManager;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mAudioLisner:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v3, 0x3

    invoke-virtual {v0, v2, v3, v1}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;->start()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    new-instance v2, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$4;

    invoke-direct {v2, p0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$4;-><init>(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)V

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    new-instance v2, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$5;

    invoke-direct {v2, p0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$5;-><init>(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)V

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public getNativeHandle()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getPrivateKeyHint()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getProperty(Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public getType()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onLoad(Landroid/content/Context;)V
    .locals 2

    check-cast p1, Landroid/app/Activity;

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mActivity:Landroid/app/Activity;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->fm:Landroid/app/FragmentManager;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mActivity:Landroid/app/Activity;

    const-string/jumbo v1, "audio"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mAudioManager:Landroid/media/AudioManager;

    new-instance v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$1;-><init>(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mAudioLisner:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)V
    .locals 0

    return-void
.end method

.method public onUnload()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mActivity:Landroid/app/Activity;

    return-void
.end method

.method public setListener(Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface$UpdateListener;)Z
    .locals 2

    if-nez p1, :cond_0

    const-string/jumbo v0, "Video"

    const-string/jumbo v1, "argument listener is null."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface$UpdateListener;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setProperty(Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public setRect(Landroid/graphics/RectF;)V
    .locals 4

    const/4 v3, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mViewGroup:Landroid/view/ViewGroup;

    if-nez v0, :cond_2

    :cond_0
    const-string/jumbo v0, "Video"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "rect = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " or viewGroup = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " This SOR started yet."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRect:Landroid/graphics/RectF;

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRect:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    float-to-int v1, v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRect:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    float-to-int v2, v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    float-to-int v1, v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoViewLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public start(Ljava/lang/Object;Landroid/graphics/RectF;Landroid/graphics/PointF;FLandroid/graphics/PointF;Landroid/view/ViewGroup;)V
    .locals 5

    const/4 v4, 0x1

    if-eqz p1, :cond_0

    if-eqz p6, :cond_0

    if-eqz p2, :cond_0

    if-nez p5, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Argument is null. ObjectBase = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " Rect = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " ViewGroup = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " startFramePosition = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mObject:Ljava/lang/Object;

    iput-object p6, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mViewGroup:Landroid/view/ViewGroup;

    iput-object p3, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mPan:Landroid/graphics/PointF;

    iput p4, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRatio:F

    iput-object p5, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mStartFramePosition:Landroid/graphics/PointF;

    new-array v0, v4, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string/jumbo v2, "VideoPath"

    aput-object v2, v0, v1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mObject:Ljava/lang/Object;

    const-string/jumbo v2, "getExtraDataString"

    sget-object v3, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->GETEXTRADATASTRING_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;

    invoke-direct {p0, v1, v2, v3, v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mInvoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoFilePath:Ljava/lang/String;

    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->setRect(Landroid/graphics/RectF;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoFilePath:Ljava/lang/String;

    if-nez v0, :cond_3

    new-instance v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mFragment:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;

    new-instance v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;-><init>(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mFragmentListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mFragment:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mFragmentListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;->setVideoIntentFragmentListener(Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->fm:Landroid/app/FragmentManager;

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mFragment:Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment;

    const-string/jumbo v2, "PlayVideo"

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    :cond_2
    :goto_0
    return-void

    :cond_3
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->startVideoPlay()Z

    move-result v0

    if-nez v0, :cond_2

    const-string/jumbo v0, "Video"

    const-string/jumbo v1, "StartVideoPlay is failed."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->cancel(I)V

    goto :goto_0
.end method

.method public stop(Z)V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->cleanResource()V

    return-void
.end method

.method public unlock(Ljava/lang/String;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
