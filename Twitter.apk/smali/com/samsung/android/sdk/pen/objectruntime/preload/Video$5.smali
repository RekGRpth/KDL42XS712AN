.class Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$5;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$5;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4

    const/4 v3, 0x1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$5;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$0(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$5;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$0(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;->canPause()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$5;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$0(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;->pause()V

    :cond_0
    :goto_0
    return v3

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$5;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoView:Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$0(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/WrapperVideoView;->start()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$5;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mAudioManager:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$5;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mAudioLisner:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    goto :goto_0
.end method
