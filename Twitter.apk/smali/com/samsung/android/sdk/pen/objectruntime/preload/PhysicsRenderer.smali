.class public Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/hardware/SensorEventListener;
.implements Landroid/opengl/GLSurfaceView$Renderer;


# static fields
.field public static final SAND_PAPER_EFFECT:I = 0x0

.field public static final SNOW_PAPER_EFFECT:I = 0x1

.field static gRenderer:Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;

.field static g_ctx:Landroid/content/Context;

.field private static mContext:Landroid/content/Context;

.field private static mPhysicsEngineJNI:Lcom/samsung/android/sdk/pen/objectruntime/preload/IPhysicsEngineJNI;


# instance fields
.field private final DBG:Z

.field private final TAG:Ljava/lang/String;

.field private captureFlag:Z

.field private doneTest:Z

.field private huristicCnt:I

.field private mCompleteListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer$OnCompleteSandFrameListener;

.field private mEnd:J

.field private mPaperEffectType:I

.field private mScreenHeight:I

.field private mScreenWidth:I

.field private mSensor:Landroid/hardware/Sensor;

.field private final mSensorEventOn:Z

.field private mSensorManager:Landroid/hardware/SensorManager;

.field private mStart:J

.field private shakeFlag:Z

.field private stopFlag:Z

.field private touchFlag:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;I)V
    .locals 5

    const-wide/16 v3, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->DBG:Z

    const-string/jumbo v0, "PhysicsRenderer"

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->TAG:Ljava/lang/String;

    iput v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mPaperEffectType:I

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->stopFlag:Z

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->captureFlag:Z

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->shakeFlag:Z

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->touchFlag:Z

    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mSensorEventOn:Z

    iput-wide v3, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mStart:J

    iput-wide v3, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mEnd:J

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->doneTest:Z

    iput v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->huristicCnt:I

    sput-object p1, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mContext:Landroid/content/Context;

    sput-object p1, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->g_ctx:Landroid/content/Context;

    iput p3, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mPaperEffectType:I

    sput-object p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->gRenderer:Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;

    sget-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mContext:Landroid/content/Context;

    const-string/jumbo v1, "sensor"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/SensorManager;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v0, v2}, Landroid/hardware/SensorManager;->getDefaultSensor(I)Landroid/hardware/Sensor;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mSensor:Landroid/hardware/Sensor;

    new-instance v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsEngineJNI;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsEngineJNI;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mPhysicsEngineJNI:Lcom/samsung/android/sdk/pen/objectruntime/preload/IPhysicsEngineJNI;

    return-void
.end method

.method public static readRawTextFile(I)Ljava/lang/String;
    .locals 5

    sget-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v1

    new-instance v2, Ljava/io/InputStreamReader;

    invoke-direct {v2, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    new-instance v3, Ljava/io/BufferedReader;

    invoke-direct {v3, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    :goto_0
    :try_start_0
    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    if-nez v4, :cond_0

    :try_start_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V

    invoke-virtual {v2}, Ljava/io/InputStreamReader;->close()V

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    :try_start_2
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v4, 0xa

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_3
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V

    invoke-virtual {v2}, Ljava/io/InputStreamReader;->close()V

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v0
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public Init_SetTexture()V
    .locals 7

    const/4 v1, 0x0

    iget v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mPaperEffectType:I

    if-nez v0, :cond_3

    :try_start_0
    sget-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string/jumbo v2, "com.samsung.android.sdk.spen30"

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v0

    const-string/jumbo v2, "sand"

    const-string/jumbo v3, "drawable"

    const-string/jumbo v4, "com.samsung.android.sdk.spen30"

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-static {v0, v2}, Lcom/samsung/android/sdk/pen/util/SpenScreenCodecDecoder;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v1, v0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_2

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    :cond_0
    :goto_0
    sget-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mPhysicsEngineJNI:Lcom/samsung/android/sdk/pen/objectruntime/preload/IPhysicsEngineJNI;

    const-string/jumbo v2, "sand"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v1, v3}, Lcom/samsung/android/sdk/pen/objectruntime/preload/IPhysicsEngineJNI;->SetTexture(Ljava/lang/String;Landroid/graphics/Bitmap;Z)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2}, Landroid/graphics/Canvas;->getWidth()I

    move-result v5

    invoke-virtual {v2}, Landroid/graphics/Canvas;->getHeight()I

    move-result v6

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1

    :cond_3
    iget v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mPaperEffectType:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    :try_start_1
    sget-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string/jumbo v2, "com.samsung.android.sdk.spen30"

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v0

    const-string/jumbo v2, "snow"

    const-string/jumbo v3, "drawable"

    const-string/jumbo v4, "com.samsung.android.sdk.spen30"

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    invoke-static {v0, v2}, Lcom/samsung/android/sdk/pen/util/SpenScreenCodecDecoder;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_5

    instance-of v1, v0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v1, :cond_4

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_2
    sget-object v1, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mPhysicsEngineJNI:Lcom/samsung/android/sdk/pen/objectruntime/preload/IPhysicsEngineJNI;

    const-string/jumbo v2, "snow"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v0, v3}, Lcom/samsung/android/sdk/pen/objectruntime/preload/IPhysicsEngineJNI;->SetTexture(Ljava/lang/String;Landroid/graphics/Bitmap;Z)V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_1

    :cond_4
    :try_start_2
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2}, Landroid/graphics/Canvas;->getWidth()I

    move-result v5

    invoke-virtual {v2}, Landroid/graphics/Canvas;->getHeight()I

    move-result v6

    invoke-virtual {v0, v3, v4, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_1

    move-object v0, v1

    goto :goto_2

    :cond_5
    move-object v0, v1

    goto :goto_2
.end method

.method public ScreenCapture()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->captureFlag:Z

    return-void
.end method

.method public onAccuracyChanged(Landroid/hardware/Sensor;I)V
    .locals 0

    return-void
.end method

.method public onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 10

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->stopFlag:Z

    if-nez v2, :cond_0

    sget-object v2, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mPhysicsEngineJNI:Lcom/samsung/android/sdk/pen/objectruntime/preload/IPhysicsEngineJNI;

    invoke-interface {v2}, Lcom/samsung/android/sdk/pen/objectruntime/preload/IPhysicsEngineJNI;->Draw_PhysicsEngine()V

    :cond_0
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->doneTest:Z

    if-nez v2, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mEnd:J

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "end absolute time: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mEnd:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "init time: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mEnd:J

    iget-wide v6, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mStart:J

    sub-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->doneTest:Z

    :cond_1
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->captureFlag:Z

    if-eqz v2, :cond_5

    const/16 v2, 0xa

    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->shakeFlag:Z

    if-eqz v3, :cond_8

    :goto_0
    iget v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->huristicCnt:I

    if-le v2, v0, :cond_3

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->captureFlag:Z

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->shakeFlag:Z

    iput v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->huristicCnt:I

    :cond_2
    :goto_1
    return-void

    :cond_3
    iget v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mScreenWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mScreenHeight:I

    mul-int/2addr v0, v2

    new-array v8, v0, [I

    iget v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mScreenWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mScreenHeight:I

    mul-int/2addr v0, v2

    new-array v9, v0, [I

    invoke-static {v8}, Ljava/nio/IntBuffer;->wrap([I)Ljava/nio/IntBuffer;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/nio/IntBuffer;->position(I)Ljava/nio/Buffer;

    iget v3, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mScreenWidth:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mScreenHeight:I

    const/16 v5, 0x1908

    const/16 v6, 0x1401

    move-object v0, p1

    move v2, v1

    invoke-interface/range {v0 .. v7}, Ljavax/microedition/khronos/opengles/GL10;->glReadPixels(IIIIIILjava/nio/Buffer;)V

    move v0, v1

    :goto_2
    iget v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mScreenHeight:I

    if-lt v0, v2, :cond_6

    iget v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mScreenWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mScreenHeight:I

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v9, v0, v2, v3}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mCompleteListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer$OnCompleteSandFrameListener;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mCompleteListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer$OnCompleteSandFrameListener;

    invoke-interface {v2, v0, v1}, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer$OnCompleteSandFrameListener;->OnCompleteSandFrameListener(Landroid/graphics/Bitmap;I)V

    :cond_4
    iget v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->huristicCnt:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->huristicCnt:I

    :cond_5
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->stopFlag:Z

    if-eqz v0, :cond_2

    sget-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mPhysicsEngineJNI:Lcom/samsung/android/sdk/pen/objectruntime/preload/IPhysicsEngineJNI;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/IPhysicsEngineJNI;->DeInit_PhysicsEngineJNI()V

    goto :goto_1

    :cond_6
    move v2, v1

    :goto_3
    iget v3, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mScreenWidth:I

    if-lt v2, v3, :cond_7

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_7
    iget v3, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mScreenWidth:I

    mul-int/2addr v3, v0

    add-int/2addr v3, v2

    aget v3, v8, v3

    shr-int/lit8 v4, v3, 0x10

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v5, v3, 0x10

    const/high16 v6, 0xff0000

    and-int/2addr v5, v6

    const v6, -0xff0100

    and-int/2addr v3, v6

    or-int/2addr v3, v5

    or-int/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mScreenHeight:I

    sub-int/2addr v4, v0

    add-int/lit8 v4, v4, -0x1

    iget v5, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mScreenWidth:I

    mul-int/2addr v4, v5

    add-int/2addr v4, v2

    aput v3, v9, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_8
    move v0, v2

    goto/16 :goto_0
.end method

.method public onPause()V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 3

    const-string/jumbo v0, "PhysicsRenderer"

    const-string/jumbo v1, "onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mSensor:Landroid/hardware/Sensor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mSensor:Landroid/hardware/Sensor;

    const/4 v2, 0x2

    invoke-virtual {v0, p0, v1, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    :cond_0
    return-void
.end method

.method public onSensorChanged(Landroid/hardware/SensorEvent;)V
    .locals 7

    const/4 v6, 0x1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->stopFlag:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->touchFlag:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mPhysicsEngineJNI:Lcom/samsung/android/sdk/pen/objectruntime/preload/IPhysicsEngineJNI;

    iget-object v1, p1, Landroid/hardware/SensorEvent;->sensor:Landroid/hardware/Sensor;

    invoke-virtual {v1}, Landroid/hardware/Sensor;->getType()I

    move-result v1

    iget-object v2, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    iget-object v3, p1, Landroid/hardware/SensorEvent;->values:[F

    aget v3, v3, v6

    iget-object v4, p1, Landroid/hardware/SensorEvent;->values:[F

    const/4 v5, 0x2

    aget v4, v4, v5

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/pen/objectruntime/preload/IPhysicsEngineJNI;->onSensorEvent(IFFF)V

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->shakeFlag:Z

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->captureFlag:Z

    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mSensorManager:Landroid/hardware/SensorManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mSensorManager:Landroid/hardware/SensorManager;

    invoke-virtual {v0, p0}, Landroid/hardware/SensorManager;->unregisterListener(Landroid/hardware/SensorEventListener;)V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->stopFlag:Z

    return-void
.end method

.method public onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->stopFlag:Z

    iput p2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mScreenWidth:I

    iput p3, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mScreenHeight:I

    sget-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mPhysicsEngineJNI:Lcom/samsung/android/sdk/pen/objectruntime/preload/IPhysicsEngineJNI;

    iget v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mPaperEffectType:I

    invoke-interface {v0, v1, p2, p3}, Lcom/samsung/android/sdk/pen/objectruntime/preload/IPhysicsEngineJNI;->Init_PhysicsEngine(III)V

    return-void
.end method

.method public onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mStart:J

    sget-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mPhysicsEngineJNI:Lcom/samsung/android/sdk/pen/objectruntime/preload/IPhysicsEngineJNI;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/IPhysicsEngineJNI;->Init_PhysicsEngineJNI()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mSensor:Landroid/hardware/Sensor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mSensorManager:Landroid/hardware/SensorManager;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mSensor:Landroid/hardware/Sensor;

    const/4 v2, 0x2

    invoke-virtual {v0, p0, v1, v2}, Landroid/hardware/SensorManager;->registerListener(Landroid/hardware/SensorEventListener;Landroid/hardware/Sensor;I)Z

    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->Init_SetTexture()V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 12

    const/16 v1, 0xa

    const/4 v9, 0x1

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    new-array v4, v1, [I

    new-array v5, v1, [I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    sget-object v1, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mPhysicsEngineJNI:Lcom/samsung/android/sdk/pen/objectruntime/preload/IPhysicsEngineJNI;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPressure()F

    move-result v6

    invoke-interface {v1, v3, v6}, Lcom/samsung/android/sdk/pen/objectruntime/preload/IPhysicsEngineJNI;->onSettingEvent(IF)V

    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    :goto_0
    return v9

    :pswitch_0
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    float-to-int v0, v0

    aput v0, v4, v1

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    float-to-int v0, v0

    aput v0, v5, v1

    sget-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mPhysicsEngineJNI:Lcom/samsung/android/sdk/pen/objectruntime/preload/IPhysicsEngineJNI;

    invoke-interface/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/objectruntime/preload/IPhysicsEngineJNI;->onTouchEvent(III[I[I)V

    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->captureFlag:Z

    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->shakeFlag:Z

    iput-boolean v9, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->touchFlag:Z

    goto :goto_0

    :pswitch_1
    move v1, v3

    :goto_1
    if-lt v3, v2, :cond_0

    sget-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mPhysicsEngineJNI:Lcom/samsung/android/sdk/pen/objectruntime/preload/IPhysicsEngineJNI;

    const/4 v3, 0x2

    invoke-interface/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/objectruntime/preload/IPhysicsEngineJNI;->onTouchEvent(III[I[I)V

    iput-boolean v9, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->touchFlag:Z

    goto :goto_0

    :cond_0
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    float-to-int v0, v0

    aput v0, v4, v1

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    float-to-int v0, v0

    aput v0, v5, v1

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :pswitch_2
    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v7

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    float-to-int v0, v0

    aput v0, v4, v7

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    float-to-int v0, v0

    aput v0, v5, v7

    sget-object v6, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mPhysicsEngineJNI:Lcom/samsung/android/sdk/pen/objectruntime/preload/IPhysicsEngineJNI;

    move v8, v2

    move-object v10, v4

    move-object v11, v5

    invoke-interface/range {v6 .. v11}, Lcom/samsung/android/sdk/pen/objectruntime/preload/IPhysicsEngineJNI;->onTouchEvent(III[I[I)V

    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->touchFlag:Z

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->ScreenCapture()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public setOnCompleteSandFrameListener(Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer$OnCompleteSandFrameListener;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->mCompleteListener:Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer$OnCompleteSandFrameListener;

    return-void
.end method
