.class public Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;
.super Landroid/opengl/GLSurfaceView;
.source "Twttr"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mIsPaused:Z

.field private mRenderer:Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;

.field private final mSurfaceHolder:Landroid/view/SurfaceHolder;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    const/4 v6, 0x0

    const/16 v1, 0x8

    invoke-direct {p0, p1}, Landroid/opengl/GLSurfaceView;-><init>(Landroid/content/Context;)V

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;->mIsPaused:Z

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Landroid/view/SurfaceHolder;->setKeepScreenOn(Z)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;->detectOpenGLES20()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;->setEGLContextClientVersion(I)V

    const/16 v5, 0x10

    move-object v0, p0

    move v2, v1

    move v3, v1

    move v4, v1

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;->setEGLConfigChooser(IIIIII)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setFormat(I)V

    :goto_0
    return-void

    :cond_0
    const-string/jumbo v0, "WaterEffect"

    const-string/jumbo v1, "this machine does not support OpenGL ES2.0"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private detectOpenGLES20()Z
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;->mContext:Landroid/content/Context;

    const-string/jumbo v2, "activity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getDeviceConfigurationInfo()Landroid/content/pm/ConfigurationInfo;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget v0, v0, Landroid/content/pm/ConfigurationInfo;->reqGlEsVersion:I

    const/high16 v2, 0x20000

    if-lt v0, v2, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public gatherTransparentRegion(Landroid/graphics/Region;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onPause()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;->mIsPaused:Z

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;->requestRender()V

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;->mIsPaused:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;->requestRender()V

    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;->mRenderer:Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->onStop()V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;->mRenderer:Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->onTouchEvent(Landroid/view/MotionEvent;)Z

    const/4 v0, 0x1

    return v0
.end method

.method protected onWindowVisibilityChanged(I)V
    .locals 0

    if-nez p1, :cond_0

    invoke-super {p0, p1}, Landroid/opengl/GLSurfaceView;->onWindowVisibilityChanged(I)V

    :cond_0
    return-void
.end method

.method public setPhysicsRenderer(Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;)V
    .locals 1

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;->mRenderer:Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;->mRenderer:Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;->setRenderer(Landroid/opengl/GLSurfaceView$Renderer;)V

    return-void
.end method
