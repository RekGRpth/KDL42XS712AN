.class Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/samsung/android/sdk/pen/objectruntime/preload/VideoIntentFragment$OnVideoIntentFragmentListener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    const/4 v1, 0x0

    # invokes: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->cancel(I)V
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$1(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;I)V

    return-void
.end method

.method public onResult(Landroid/content/Intent;)V
    .locals 10

    const/4 v3, 0x0

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    if-nez p1, :cond_0

    const-string/jumbo v0, "Video"

    const-string/jumbo v1, "Video file was not selected."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # invokes: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->cancel(I)V
    invoke-static {v0, v7}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$1(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    if-nez v1, :cond_1

    const-string/jumbo v0, "Video"

    const-string/jumbo v1, "The extra data of video file is null."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v0, "android.intent.extra.STREAM"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    move-object v1, v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mActivity:Landroid/app/Activity;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$2(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-array v2, v9, [Ljava/lang/String;

    const-string/jumbo v4, "_id"

    aput-object v4, v2, v7

    const-string/jumbo v4, "_data"

    aput-object v4, v2, v8

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_2

    const-string/jumbo v1, "Video"

    const-string/jumbo v2, "The extra data of video file is empty."

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # invokes: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->cancel(I)V
    invoke-static {v0, v7}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$1(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;I)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    const-string/jumbo v2, "_data"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$3(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;Ljava/lang/String;)V

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoFilePath:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$4(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v8}, Landroid/media/ThumbnailUtils;->createVideoThumbnail(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_3

    const-string/jumbo v0, "Video"

    const-string/jumbo v1, "The bitmap of video is null"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # invokes: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->cancel(I)V
    invoke-static {v0, v7}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$1(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;I)V

    goto :goto_0

    :cond_3
    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    # invokes: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->getRatioRect(IILandroid/graphics/RectF;)Landroid/graphics/RectF;
    invoke-static {v3, v4, v5, v1}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$5(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;IILandroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$6(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;Landroid/graphics/RectF;)V

    new-array v2, v9, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRect:Landroid/graphics/RectF;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$7(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Landroid/graphics/RectF;

    move-result-object v4

    # invokes: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->convertAbsolute(Landroid/graphics/RectF;)Landroid/graphics/RectF;
    invoke-static {v3, v4}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$8(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;Landroid/graphics/RectF;)Landroid/graphics/RectF;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v8

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mObject:Ljava/lang/Object;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$9(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Ljava/lang/Object;

    move-result-object v4

    const-string/jumbo v5, "setRect"

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->SETRECT_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;
    invoke-static {}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$10()[Ljava/lang/Class;

    move-result-object v6

    # invokes: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mInvoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
    invoke-static {v3, v4, v5, v6, v2}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$11(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # invokes: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->createPlayVideoBitmap(Landroid/graphics/Bitmap;Landroid/graphics/RectF;)Landroid/graphics/Bitmap;
    invoke-static {v2, v0, v1}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$12(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;Landroid/graphics/Bitmap;Landroid/graphics/RectF;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_4

    const-string/jumbo v0, "Video"

    const-string/jumbo v1, "PlayVideo merge process is failed"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # invokes: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->cancel(I)V
    invoke-static {v0, v7}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$1(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;I)V

    goto/16 :goto_0

    :cond_4
    new-array v1, v8, [Ljava/lang/Object;

    aput-object v0, v1, v7

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mObject:Ljava/lang/Object;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$9(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Ljava/lang/Object;

    move-result-object v2

    const-string/jumbo v3, "setImage"

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->SETIMAGE_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;
    invoke-static {}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$13()[Ljava/lang/Class;

    move-result-object v4

    # invokes: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mInvoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
    invoke-static {v0, v2, v3, v4, v1}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$11(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    new-array v0, v9, [Ljava/lang/Object;

    const-string/jumbo v1, "VideoPath"

    aput-object v1, v0, v7

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mVideoFilePath:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$4(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v8

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mObject:Ljava/lang/Object;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$9(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Ljava/lang/Object;

    move-result-object v2

    const-string/jumbo v3, "setExtraDataString"

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->SETEXTRADATASTRING_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;
    invoke-static {}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$14()[Ljava/lang/Class;

    move-result-object v4

    # invokes: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mInvoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
    invoke-static {v1, v2, v3, v4, v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$11(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    new-array v0, v8, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v0, v7

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mObject:Ljava/lang/Object;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$9(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Ljava/lang/Object;

    move-result-object v2

    const-string/jumbo v3, "setRotatable"

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->SETROTATABLE_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;
    invoke-static {}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$15()[Ljava/lang/Class;

    move-result-object v4

    # invokes: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mInvoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
    invoke-static {v1, v2, v3, v4, v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$11(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    new-array v0, v8, [Ljava/lang/Object;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v7

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mObject:Ljava/lang/Object;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$9(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Ljava/lang/Object;

    move-result-object v2

    const-string/jumbo v3, "setResizeOption"

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->SETRESIZEOPTION_FUNCTION_ARG_SIGNATURE:[Ljava/lang/Class;
    invoke-static {}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$16()[Ljava/lang/Class;

    move-result-object v4

    # invokes: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mInvoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
    invoke-static {v1, v2, v3, v4, v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$11(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface$UpdateListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$17(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface$UpdateListener;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mRect:Landroid/graphics/RectF;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$7(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Landroid/graphics/RectF;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # getter for: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->mObject:Ljava/lang/Object;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$9(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface$UpdateListener;->onObjectUpdated(Landroid/graphics/RectF;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # invokes: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->complete()V
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$18(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;)V

    goto/16 :goto_0

    :cond_5
    const-string/jumbo v0, "Video"

    const-string/jumbo v1, "The extra data of video file query is failed."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video$2;->this$0:Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;

    # invokes: Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->cancel(I)V
    invoke-static {v0, v7}, Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;->access$1(Lcom/samsung/android/sdk/pen/objectruntime/preload/Video;I)V

    goto/16 :goto_0
.end method
