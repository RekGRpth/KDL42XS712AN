.class public Lcom/samsung/android/sdk/pen/pen/preload/Brush;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field public final nativeBrush:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->native_init()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->nativeBrush:I

    return-void
.end method

.method private static native native_command(IILjava/util/ArrayList;)Ljava/util/ArrayList;
.end method

.method private static native native_construct(I)Z
.end method

.method private static native native_draw(ILandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
.end method

.method private static native native_end(ILandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
.end method

.method private static native native_finalize(I)V
.end method

.method private static native native_getAdvancedSetting(I)Ljava/lang/String;
.end method

.method private static native native_getColor(I)I
.end method

.method private static native native_getMaxSettingValue(I)F
.end method

.method private static native native_getMinSettingValue(I)F
.end method

.method private static native native_getPenAttribute(II)Z
.end method

.method private static native native_getProperty(ILandroid/os/Bundle;)Z
.end method

.method private static native native_getSize(I)F
.end method

.method private static native native_getStrokeRect(I[Landroid/graphics/PointF;[F[IFZLjava/lang/String;)Landroid/graphics/RectF;
.end method

.method private static native native_init()I
.end method

.method private static native native_isCurveEnabled(I)Z
.end method

.method private static native native_move(ILandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
.end method

.method private static native native_onLoad(I)V
.end method

.method private static native native_onUnload(I)V
.end method

.method private static native native_redraw(ILandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
.end method

.method private static native native_setAdvancedSetting(ILjava/lang/String;)Z
.end method

.method private static native native_setBitmap(ILandroid/graphics/Bitmap;)Z
.end method

.method private static native native_setColor(II)Z
.end method

.method private static native native_setCurveEnabled(IZ)Z
.end method

.method private static native native_setProperty(ILandroid/os/Bundle;)Z
.end method

.method private static native native_setReferenceBitmap(ILandroid/graphics/Bitmap;)Z
.end method

.method private static native native_setSize(IF)Z
.end method

.method private static native native_start(ILandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
.end method


# virtual methods
.method public construct()V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->nativeBrush:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->native_construct(I)Z

    return-void
.end method

.method public draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->mBitmap:Landroid/graphics/Bitmap;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, v1, v1}, Landroid/graphics/Bitmap;->setPixel(III)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->nativeBrush:I

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    invoke-static {v0, p1, p2, v1}, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->native_draw(ILandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public getAdvancedSetting()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->nativeBrush:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->native_getAdvancedSetting(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getColor()I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->nativeBrush:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->native_getColor(I)I

    move-result v0

    return v0
.end method

.method public getMaxSettingValue()F
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->nativeBrush:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->native_getMaxSettingValue(I)F

    move-result v0

    return v0
.end method

.method public getMinSettingValue()F
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->nativeBrush:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->native_getMinSettingValue(I)F

    move-result v0

    return v0
.end method

.method public getNativeHandle()I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->nativeBrush:I

    return v0
.end method

.method public getPenAttribute(I)Z
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->nativeBrush:I

    invoke-static {v0, p1}, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->native_getPenAttribute(II)Z

    move-result v0

    return v0
.end method

.method public getPrivateKeyHint()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getProperty(Landroid/os/Bundle;)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->nativeBrush:I

    invoke-static {v0, p1}, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->native_getProperty(ILandroid/os/Bundle;)Z

    return-void
.end method

.method public getSize()F
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->nativeBrush:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->native_getSize(I)F

    move-result v0

    return v0
.end method

.method public getStrokeRect([Landroid/graphics/PointF;[F[IFZLjava/lang/String;)Landroid/graphics/RectF;
    .locals 7

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->nativeBrush:I

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->native_getStrokeRect(I[Landroid/graphics/PointF;[F[IFZLjava/lang/String;)Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public hideAdvancedSetting()V
    .locals 0

    return-void
.end method

.method public isCurveEnabled()Z
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->nativeBrush:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->native_isCurveEnabled(I)Z

    move-result v0

    return v0
.end method

.method public onLoad(Landroid/content/Context;)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->nativeBrush:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->native_onLoad(I)V

    return-void
.end method

.method public onUnload()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->mBitmap:Landroid/graphics/Bitmap;

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->nativeBrush:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->native_onUnload(I)V

    return-void
.end method

.method public redrawPen(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->mBitmap:Landroid/graphics/Bitmap;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, v1, v1}, Landroid/graphics/Bitmap;->setPixel(III)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->nativeBrush:I

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    invoke-static {v0, p1, p2, v1}, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->native_redraw(ILandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public setAdvancedSetting(Ljava/lang/String;)V
    .locals 2

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->nativeBrush:I

    invoke-static {v0, p1}, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->native_setAdvancedSetting(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    :cond_0
    return-void
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 2

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->nativeBrush:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->mBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->native_setBitmap(ILandroid/graphics/Bitmap;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public setColor(I)V
    .locals 2

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->nativeBrush:I

    invoke-static {v0, p1}, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->native_setColor(II)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    :cond_0
    return-void
.end method

.method public setCurveEnabled(Z)V
    .locals 2

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->nativeBrush:I

    invoke-static {v0, p1}, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->native_setCurveEnabled(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    :cond_0
    return-void
.end method

.method public setProperty(Landroid/os/Bundle;)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->nativeBrush:I

    invoke-static {v0, p1}, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->native_setProperty(ILandroid/os/Bundle;)Z

    return-void
.end method

.method public setReferenceBitmap(Landroid/graphics/Bitmap;)V
    .locals 0

    return-void
.end method

.method public setSize(F)V
    .locals 2

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->nativeBrush:I

    invoke-static {v0, p1}, Lcom/samsung/android/sdk/pen/pen/preload/Brush;->native_setSize(IF)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    :cond_0
    return-void
.end method

.method public showAdvancedSetting(Landroid/content/Context;Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;Landroid/view/ViewGroup;)V
    .locals 0

    return-void
.end method

.method public unlock(Ljava/lang/String;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
