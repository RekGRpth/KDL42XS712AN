.class Lcom/samsung/android/sdk/pen/pen/preload/Beautify$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$1;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$1;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSettingListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$0(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    move v0, v1

    :goto_0
    const/16 v3, 0xa

    if-lt v0, v3, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$1;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->setAdvancedSetting(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$1;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSettingListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$0(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;

    move-result-object v0

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;->onChanged(Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->DEFAULT_SETTING_VALUES:[[I
    invoke-static {}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$1()[[I

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$1;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCurrentButtonIndex:I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$2(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)I

    move-result v4

    aget-object v3, v3, v4

    aget v3, v3, v0

    packed-switch v0, :pswitch_data_0

    :goto_1
    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    const/16 v3, 0x3b

    invoke-virtual {v2, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :pswitch_0
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$1;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSettingValues:[[I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$3(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)[[I

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$1;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCurrentButtonIndex:I
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$2(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)I

    move-result v5

    aget-object v4, v4, v5

    aput v3, v4, v1

    goto :goto_1

    :pswitch_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$1;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSettingValues:[[I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$3(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)[[I

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$1;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCurrentButtonIndex:I
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$2(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)I

    move-result v5

    aget-object v4, v4, v5

    const/4 v5, 0x1

    aput v3, v4, v5

    goto :goto_1

    :pswitch_2
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$1;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCursiveSeekBar:Landroid/widget/SeekBar;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$4(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)Landroid/widget/SeekBar;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$1;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSettingValues:[[I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$3(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)[[I

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$1;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCurrentButtonIndex:I
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$2(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)I

    move-result v5

    aget-object v4, v4, v5

    const/4 v5, 0x2

    aput v3, v4, v5

    goto :goto_1

    :pswitch_3
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$1;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSustenanceSeekBar:Landroid/widget/SeekBar;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$5(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)Landroid/widget/SeekBar;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$1;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSettingValues:[[I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$3(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)[[I

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$1;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCurrentButtonIndex:I
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$2(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)I

    move-result v5

    aget-object v4, v4, v5

    const/4 v5, 0x3

    aput v3, v4, v5

    goto :goto_1

    :pswitch_4
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$1;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSettingValues:[[I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$3(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)[[I

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$1;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCurrentButtonIndex:I
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$2(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)I

    move-result v5

    aget-object v4, v4, v5

    const/4 v5, 0x5

    aput v3, v4, v5

    goto :goto_1

    :pswitch_5
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$1;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mDummySeekBar:Landroid/widget/SeekBar;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$6(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)Landroid/widget/SeekBar;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$1;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSettingValues:[[I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$3(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)[[I

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$1;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCurrentButtonIndex:I
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$2(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)I

    move-result v5

    aget-object v4, v4, v5

    const/4 v5, 0x4

    aput v3, v4, v5

    goto/16 :goto_1

    :pswitch_6
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$1;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mModulationSeekBar:Landroid/widget/SeekBar;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$7(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)Landroid/widget/SeekBar;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$1;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSettingValues:[[I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$3(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)[[I

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$1;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCurrentButtonIndex:I
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$2(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)I

    move-result v5

    aget-object v4, v4, v5

    const/4 v5, 0x6

    aput v3, v4, v5

    goto/16 :goto_1

    :pswitch_7
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$1;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSettingValues:[[I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$3(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)[[I

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$1;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCurrentButtonIndex:I
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$2(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)I

    move-result v5

    aget-object v4, v4, v5

    const/4 v5, 0x7

    aput v3, v4, v5

    goto/16 :goto_1

    :pswitch_8
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$1;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSettingValues:[[I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$3(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)[[I

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$1;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCurrentButtonIndex:I
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$2(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)I

    move-result v5

    aget-object v4, v4, v5

    const/16 v5, 0x8

    aput v3, v4, v5

    goto/16 :goto_1

    :pswitch_9
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$1;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSettingValues:[[I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$3(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)[[I

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$1;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCurrentButtonIndex:I
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$2(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)I

    move-result v5

    aget-object v4, v4, v5

    const/16 v5, 0x9

    aput v3, v4, v5

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method
