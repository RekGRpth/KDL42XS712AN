.class public Lcom/samsung/android/sdk/pen/pen/preload/NativePen;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;


# instance fields
.field public final nativeNativePen:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;->native_init()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;->nativeNativePen:I

    return-void
.end method

.method private static native native_command(IILjava/util/ArrayList;)Ljava/util/ArrayList;
.end method

.method private static native native_construct(I)Z
.end method

.method private static native native_finalize(I)V
.end method

.method private static native native_init()I
.end method

.method private static native native_setobject(ILjava/lang/Object;)V
.end method


# virtual methods
.method public construct()V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;->nativeNativePen:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;->native_construct(I)Z

    return-void
.end method

.method public draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V
    .locals 0

    return-void
.end method

.method public getAdvancedSetting()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getColor()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getMaxSettingValue()F
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getMinSettingValue()F
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getNativeHandle()I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;->nativeNativePen:I

    return v0
.end method

.method public getPenAttribute(I)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getPrivateKeyHint()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getProperty(Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public getSize()F
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getStrokeRect([Landroid/graphics/PointF;[F[IFZLjava/lang/String;)Landroid/graphics/RectF;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public hideAdvancedSetting()V
    .locals 0

    return-void
.end method

.method public isCurveEnabled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onLoad(Landroid/content/Context;)V
    .locals 0

    return-void
.end method

.method public onUnload()V
    .locals 0

    return-void
.end method

.method public redrawPen(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V
    .locals 0

    return-void
.end method

.method public setAdvancedSetting(Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 0

    return-void
.end method

.method public setColor(I)V
    .locals 0

    return-void
.end method

.method public setCurveEnabled(Z)V
    .locals 0

    return-void
.end method

.method public setObject(Ljava/lang/Object;)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;->nativeNativePen:I

    invoke-static {v0, p1}, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;->native_setobject(ILjava/lang/Object;)V

    return-void
.end method

.method public setProperty(Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public setReferenceBitmap(Landroid/graphics/Bitmap;)V
    .locals 0

    return-void
.end method

.method public setSize(F)V
    .locals 0

    return-void
.end method

.method public showAdvancedSetting(Landroid/content/Context;Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;Landroid/view/ViewGroup;)V
    .locals 0

    return-void
.end method

.method public unlock(Ljava/lang/String;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
