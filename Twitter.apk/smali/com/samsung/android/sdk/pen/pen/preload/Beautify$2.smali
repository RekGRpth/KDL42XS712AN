.class Lcom/samsung/android/sdk/pen/pen/preload/Beautify$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$2;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 0

    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$2;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSettingListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$0(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    const/16 v0, 0xa

    if-lt v1, v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$2;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->setAdvancedSetting(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$2;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSettingListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$0(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;

    move-result-object v0

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;->onChanged(Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    packed-switch v1, :pswitch_data_0

    :pswitch_0
    const/4 v0, -0x1

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    const/16 v0, 0x3b

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$2;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCursiveSeekBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$4(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$2;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSettingValues:[[I
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$3(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)[[I

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$2;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCurrentButtonIndex:I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$2(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)I

    move-result v4

    aget-object v3, v3, v4

    const/4 v4, 0x2

    aput v0, v3, v4

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$2;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSustenanceSeekBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$5(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$2;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSettingValues:[[I
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$3(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)[[I

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$2;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCurrentButtonIndex:I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$2(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)I

    move-result v4

    aget-object v3, v3, v4

    const/4 v4, 0x3

    aput v0, v3, v4

    goto :goto_1

    :pswitch_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$2;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mDummySeekBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$6(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$2;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSettingValues:[[I
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$3(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)[[I

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$2;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCurrentButtonIndex:I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$2(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)I

    move-result v4

    aget-object v3, v3, v4

    const/4 v4, 0x4

    aput v0, v3, v4

    goto :goto_1

    :pswitch_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$2;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mModulationSeekBar:Landroid/widget/SeekBar;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$7(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)Landroid/widget/SeekBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$2;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSettingValues:[[I
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$3(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)[[I

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$2;->this$0:Lcom/samsung/android/sdk/pen/pen/preload/Beautify;

    # getter for: Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCurrentButtonIndex:I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->access$2(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)I

    move-result v4

    aget-object v3, v3, v4

    const/4 v4, 0x6

    aput v0, v3, v4

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method
