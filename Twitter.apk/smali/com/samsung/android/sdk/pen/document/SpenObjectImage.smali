.class public final Lcom/samsung/android/sdk/pen/document/SpenObjectImage;
.super Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
.source "Twttr"


# static fields
.field public static final BORDER_TYPE_DOT:I = 0x3

.field public static final BORDER_TYPE_IMAGE:I = 0x4

.field public static final BORDER_TYPE_NONE:I = 0x0

.field public static final BORDER_TYPE_SHADOW:I = 0x2

.field public static final BORDER_TYPE_SQUARE:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_init(Z)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method private native Bitmap_saveImageTest()V
.end method

.method private native ObjectImage_clearChangedFlag()V
.end method

.method private native ObjectImage_copy(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z
.end method

.method private native ObjectImage_getBorderType()I
.end method

.method private native ObjectImage_getCropRect()Landroid/graphics/Rect;
.end method

.method private native ObjectImage_getDrawnRect()Landroid/graphics/RectF;
.end method

.method private native ObjectImage_getHintText()Ljava/lang/String;
.end method

.method private native ObjectImage_getHintTextColor()I
.end method

.method private native ObjectImage_getHintTextFontSize()F
.end method

.method private native ObjectImage_getHintTextVerticalOffset()F
.end method

.method private native ObjectImage_getImage()Landroid/graphics/Bitmap;
.end method

.method private native ObjectImage_getImageBorder()Landroid/graphics/Bitmap;
.end method

.method private native ObjectImage_getImageBorderBottomWidth()F
.end method

.method private native ObjectImage_getImageBorderLeftWidth()F
.end method

.method private native ObjectImage_getImageBorderNinePatchRect()Landroid/graphics/Rect;
.end method

.method private native ObjectImage_getImageBorderPath()Ljava/lang/String;
.end method

.method private native ObjectImage_getImageBorderRightWidth()F
.end method

.method private native ObjectImage_getImageBorderTopWidth()F
.end method

.method private native ObjectImage_getImagePath()Ljava/lang/String;
.end method

.method private native ObjectImage_getLineBorderColor()I
.end method

.method private native ObjectImage_getLineBorderWidth()F
.end method

.method private native ObjectImage_getNinePatchRect()Landroid/graphics/Rect;
.end method

.method private native ObjectImage_getTransparency()Z
.end method

.method private native ObjectImage_init(Z)Z
.end method

.method private native ObjectImage_isChanged()Z
.end method

.method private native ObjectImage_isHintTextVisiable()Z
.end method

.method private native ObjectImage_setBorderType(I)Z
.end method

.method private native ObjectImage_setCropRect(Landroid/graphics/Rect;)Z
.end method

.method private native ObjectImage_setHintText(Ljava/lang/String;)Z
.end method

.method private native ObjectImage_setHintTextColor(I)Z
.end method

.method private native ObjectImage_setHintTextFontSize(F)Z
.end method

.method private native ObjectImage_setHintTextVerticalOffset(F)Z
.end method

.method private native ObjectImage_setHintTextVisibility(Z)Z
.end method

.method private native ObjectImage_setImage(Landroid/graphics/Bitmap;)Z
.end method

.method private native ObjectImage_setImage2(Ljava/lang/String;)Z
.end method

.method private native ObjectImage_setImage3(Landroid/graphics/Bitmap;Landroid/graphics/Rect;)Z
.end method

.method private native ObjectImage_setImage4(Ljava/lang/String;Landroid/graphics/Rect;)Z
.end method

.method private native ObjectImage_setImageBorder(Landroid/graphics/Bitmap;Landroid/graphics/Rect;)Z
.end method

.method private native ObjectImage_setImageBorderWidth(FFFF)Z
.end method

.method private native ObjectImage_setLineBorderColor(I)Z
.end method

.method private native ObjectImage_setLineBorderWidth(F)Z
.end method

.method private native ObjectImage_setTransparency(Z)Z
.end method

.method private throwUncheckedException(I)V
    .locals 3

    const/16 v0, 0x13

    if-ne p1, v0, :cond_0

    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "SpenObjectImage("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ") is already closed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {p1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    return-void
.end method


# virtual methods
.method public clearChangedFlag()V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_clearChangedFlag()V

    return-void
.end method

.method public copy(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_copy(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public getBorderType()I
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_getBorderType()I

    move-result v0

    return v0
.end method

.method public getCropRect()Landroid/graphics/Rect;
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_getCropRect()Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public getDrawnRect()Landroid/graphics/RectF;
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_getDrawnRect()Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public getHintText()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_getHintText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHintTextColor()I
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_getHintTextColor()I

    move-result v0

    return v0
.end method

.method public getHintTextFontSize()F
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_getHintTextFontSize()F

    move-result v0

    return v0
.end method

.method public getHintTextVerticalOffset()F
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_getHintTextVerticalOffset()F

    move-result v0

    return v0
.end method

.method public getImage()Landroid/graphics/Bitmap;
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_getImage()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getImageBorder()Landroid/graphics/Bitmap;
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_getImageBorderPath()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public getImageBorderBottomWidth()F
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_getImageBorderBottomWidth()F

    move-result v0

    return v0
.end method

.method public getImageBorderLeftWidth()F
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_getImageBorderLeftWidth()F

    move-result v0

    return v0
.end method

.method public getImageBorderNinePatchRect()Landroid/graphics/Rect;
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_getImageBorderNinePatchRect()Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public getImageBorderPath()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_getImageBorderPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getImageBorderRightWidth()F
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_getImageBorderRightWidth()F

    move-result v0

    return v0
.end method

.method public getImageBorderTopWidth()F
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_getImageBorderTopWidth()F

    move-result v0

    return v0
.end method

.method public getImagePath()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_getImagePath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLineBorderColor()I
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_getLineBorderColor()I

    move-result v0

    return v0
.end method

.method public getLineBorderWidth()F
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_getLineBorderWidth()F

    move-result v0

    return v0
.end method

.method public getNinePatchRect()Landroid/graphics/Rect;
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_getNinePatchRect()Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public getTransparency()Z
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_getTransparency()Z

    move-result v0

    return v0
.end method

.method public isChanged()Z
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_isChanged()Z

    move-result v0

    return v0
.end method

.method public isHintTextEnabled()Z
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_isHintTextVisiable()Z

    move-result v0

    return v0
.end method

.method public setBorderType(I)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_setBorderType(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setCropRect(Landroid/graphics/Rect;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_setCropRect(Landroid/graphics/Rect;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setHintText(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_setHintText(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setHintTextColor(I)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_setHintTextColor(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setHintTextEnabled(Z)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_setHintTextVisibility(Z)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setHintTextFontSize(F)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_setHintTextFontSize(F)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setHintTextVerticalOffset(F)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_setHintTextVerticalOffset(F)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setImage(Landroid/graphics/Bitmap;)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "image is recyled."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_setImage(Landroid/graphics/Bitmap;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->throwUncheckedException(I)V

    :cond_1
    return-void
.end method

.method public setImage(Landroid/graphics/Bitmap;Landroid/graphics/Rect;)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "image is recyled."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_setImage3(Landroid/graphics/Bitmap;Landroid/graphics/Rect;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->throwUncheckedException(I)V

    :cond_1
    return-void
.end method

.method public setImage(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_setImage2(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setImage(Ljava/lang/String;Landroid/graphics/Rect;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_setImage4(Ljava/lang/String;Landroid/graphics/Rect;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setImageBorder(Landroid/graphics/Bitmap;Landroid/graphics/Rect;)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "image is recyled."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_setImageBorder(Landroid/graphics/Bitmap;Landroid/graphics/Rect;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->throwUncheckedException(I)V

    :cond_1
    return-void
.end method

.method public setImageBorderWidth(FFFF)V
    .locals 1

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_setImageBorderWidth(FFFF)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setLineBorderColor(I)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_setLineBorderColor(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setLineBorderWidth(F)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_setLineBorderWidth(F)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setRect(Landroid/graphics/RectF;Z)V
    .locals 0

    invoke-super {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setRect(Landroid/graphics/RectF;Z)V

    return-void
.end method

.method public setTransparency(Z)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->ObjectImage_setTransparency(Z)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectImage;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method
