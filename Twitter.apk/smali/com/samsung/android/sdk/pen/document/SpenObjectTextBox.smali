.class public Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
.super Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
.source "Twttr"


# static fields
.field public static final ALIGN_BOTH:I = 0x3

.field public static final ALIGN_CENTER:I = 0x2

.field public static final ALIGN_LEFT:I = 0x0

.field public static final ALIGN_RIGHT:I = 0x1

.field public static final AUTO_FIT_BOTH:I = 0x3

.field public static final AUTO_FIT_HORIZONTAL:I = 0x1

.field public static final AUTO_FIT_NONE:I = 0x0

.field public static final AUTO_FIT_VERTICAL:I = 0x2

.field public static final BORDER_TYPE_DOT:I = 0x3

.field public static final BORDER_TYPE_NONE:I = 0x0

.field public static final BORDER_TYPE_SHADOW:I = 0x2

.field public static final BORDER_TYPE_SQUARE:I = 0x1

.field public static final DIRECTION_LEFT_TO_RIGHT:I = 0x0

.field public static final DIRECTION_RIGHT_TO_LEFT:I = 0x1

.field public static final ELLIPSIS_DOTS:I = 0x1

.field public static final ELLIPSIS_NONE:I = 0x0

.field public static final ELLIPSIS_TRIANGLE:I = 0x2

.field public static final GRAVITY_BOTTOM:I = 0x2

.field public static final GRAVITY_CENTER:I = 0x1

.field public static final GRAVITY_TOP:I = 0x0

.field public static final HYPER_TEXT_ADDRESS:I = 0x5

.field public static final HYPER_TEXT_DATE:I = 0x4

.field public static final HYPER_TEXT_EMAIL:I = 0x1

.field public static final HYPER_TEXT_TEL:I = 0x2

.field public static final HYPER_TEXT_UNKNOWN:I = 0x0

.field public static final HYPER_TEXT_URL:I = 0x3

.field public static final IMEACTION_PREVIOUS:I = 0x7

.field public static final IME_ACTION_DONE:I = 0x4

.field public static final IME_ACTION_GO:I = 0x2

.field public static final IME_ACTION_NEXT:I = 0x6

.field public static final IME_ACTION_NONE:I = 0x1

.field public static final IME_ACTION_SEARCH:I = 0x3

.field public static final IME_ACTION_SEND:I = 0x5

.field public static final IME_ACTION_UNSPECIFIED:I = 0x0

.field public static final INPUT_TYPE_DATETIME:I = 0x4

.field public static final INPUT_TYPE_NONE:I = 0x0

.field public static final INPUT_TYPE_NUMBER:I = 0x2

.field public static final INPUT_TYPE_PHONE:I = 0x3

.field public static final INPUT_TYPE_TEXT:I = 0x1

.field public static final LINE_SPACING_PERCENT:I = 0x1

.field public static final LINE_SPACING_PIXEL:I = 0x0

.field public static final STYLE_BOLD:I = 0x1

.field public static final STYLE_ITALIC:I = 0x2

.field public static final STYLE_MASK:I = 0x7

.field public static final STYLE_NONE:I = 0x0

.field public static final STYLE_UNDERLINE:I = 0x4


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    return-void
.end method

.method protected constructor <init>(I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_init2(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 3

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_init3(Ljava/lang/String;Ljava/util/ArrayList;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    iget v2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    if-ltz v2, :cond_4

    iget v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    if-gez v0, :cond_0

    :cond_4
    const/4 v0, 0x7

    const-string/jumbo v1, "startPos and endPos of TextSpanInfo should have positive value"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 3

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_init4(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    iget v2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    if-ltz v2, :cond_4

    iget v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    if-gez v0, :cond_0

    :cond_4
    const/4 v0, 0x7

    const-string/jumbo v1, "startPos and endPos of TextSpanInfo should have positive value"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)V
    .locals 3

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_init5(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    iget v2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    if-ltz v2, :cond_4

    iget v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    if-gez v0, :cond_0

    :cond_4
    const/4 v0, 0x7

    const-string/jumbo v1, "startPos and endPos of TextSpanInfo should have positive value"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/util/ArrayList;Z)V
    .locals 3

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_init5(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    iget v2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    if-ltz v2, :cond_4

    iget v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    if-gez v0, :cond_0

    :cond_4
    const/4 v0, 0x7

    const-string/jumbo v1, "startPos and endPos of TextSpanInfo should have positive value"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    invoke-direct {p0, p1, v1, v1, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_init5(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    invoke-direct {p0, v1, v1, v1, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_init5(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method private native ObjectTextBox_appendParagraph(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;)Z
.end method

.method private native ObjectTextBox_appendSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)Z
.end method

.method private native ObjectTextBox_clearChangedFlag()V
.end method

.method private native ObjectTextBox_copy(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z
.end method

.method private native ObjectTextBox_enableReadOnly(Z)Z
.end method

.method private native ObjectTextBox_findParagraphs(II)Ljava/util/ArrayList;
.end method

.method private native ObjectTextBox_findSpans(II)Ljava/util/ArrayList;
.end method

.method private native ObjectTextBox_getAutoFitOption()I
.end method

.method private native ObjectTextBox_getBackgroundColor()I
.end method

.method private native ObjectTextBox_getBorderType()I
.end method

.method private native ObjectTextBox_getBottomMargin()F
.end method

.method private native ObjectTextBox_getBulletType()I
.end method

.method private native ObjectTextBox_getCursorPos()I
.end method

.method private native ObjectTextBox_getDrawnRect()Landroid/graphics/RectF;
.end method

.method private native ObjectTextBox_getEllipsisType()I
.end method

.method private native ObjectTextBox_getFont()Ljava/lang/String;
.end method

.method private native ObjectTextBox_getFontSize()F
.end method

.method private native ObjectTextBox_getGravity()I
.end method

.method private native ObjectTextBox_getHintText()Ljava/lang/String;
.end method

.method private native ObjectTextBox_getHintTextColor()I
.end method

.method private native ObjectTextBox_getHintTextFontSize()F
.end method

.method private native ObjectTextBox_getIMEActionType()I
.end method

.method private native ObjectTextBox_getLeftMargin()F
.end method

.method private native ObjectTextBox_getLineBorderColor()I
.end method

.method private native ObjectTextBox_getLineBorderWidth()F
.end method

.method private native ObjectTextBox_getParagraph()Ljava/util/ArrayList;
.end method

.method private native ObjectTextBox_getRightMargin()F
.end method

.method private native ObjectTextBox_getSpan()Ljava/util/ArrayList;
.end method

.method private native ObjectTextBox_getText()Ljava/lang/String;
.end method

.method private native ObjectTextBox_getTextAlignment()I
.end method

.method private native ObjectTextBox_getTextColor()I
.end method

.method private native ObjectTextBox_getTextDirection()I
.end method

.method private native ObjectTextBox_getTextIndentLevel()I
.end method

.method private native ObjectTextBox_getTextInputType()I
.end method

.method private native ObjectTextBox_getTextLineSpacing()F
.end method

.method private native ObjectTextBox_getTextLineSpacingType()I
.end method

.method private native ObjectTextBox_getTextStyle()I
.end method

.method private native ObjectTextBox_getTopMargin()F
.end method

.method private native ObjectTextBox_getVerticalPan()F
.end method

.method private native ObjectTextBox_init1()Z
.end method

.method private native ObjectTextBox_init2(Ljava/lang/String;)Z
.end method

.method private native ObjectTextBox_init3(Ljava/lang/String;Ljava/util/ArrayList;)Z
.end method

.method private native ObjectTextBox_init4(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;)Z
.end method

.method private native ObjectTextBox_init5(Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Z)Z
.end method

.method private native ObjectTextBox_insertChar(CI)Z
.end method

.method private native ObjectTextBox_insertCharAtCursor(C)Z
.end method

.method private native ObjectTextBox_insertText(Ljava/lang/String;I)Z
.end method

.method private native ObjectTextBox_insertTextAtCursor(Ljava/lang/String;)Z
.end method

.method private native ObjectTextBox_isChanged()Z
.end method

.method private native ObjectTextBox_isHintTextVisiable()Z
.end method

.method private native ObjectTextBox_isReadOnly()Z
.end method

.method private native ObjectTextBox_parseHyperText()Z
.end method

.method private native ObjectTextBox_removeAllText()Z
.end method

.method private native ObjectTextBox_removeParagraph(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;)Z
.end method

.method private native ObjectTextBox_removeSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)Z
.end method

.method private native ObjectTextBox_removeText(II)Z
.end method

.method private native ObjectTextBox_replaceText(Ljava/lang/String;II)Z
.end method

.method private native ObjectTextBox_setAutoFitOption(I)Z
.end method

.method private native ObjectTextBox_setBackgroundColor(I)Z
.end method

.method private native ObjectTextBox_setBorderType(I)Z
.end method

.method private native ObjectTextBox_setBulletType(I)Z
.end method

.method private native ObjectTextBox_setCursorPos(I)Z
.end method

.method private native ObjectTextBox_setEllipsisType(I)Z
.end method

.method private native ObjectTextBox_setFont(Ljava/lang/String;)Z
.end method

.method private native ObjectTextBox_setFontSize(F)Z
.end method

.method private native ObjectTextBox_setGravity(I)Z
.end method

.method private native ObjectTextBox_setHintText(Ljava/lang/String;)Z
.end method

.method private native ObjectTextBox_setHintTextColor(I)Z
.end method

.method private native ObjectTextBox_setHintTextFontSize(F)Z
.end method

.method private native ObjectTextBox_setHintTextVisibility(Z)Z
.end method

.method private native ObjectTextBox_setIMEActionType(I)Z
.end method

.method private native ObjectTextBox_setLineBorderColor(I)Z
.end method

.method private native ObjectTextBox_setLineBorderWidth(F)Z
.end method

.method private native ObjectTextBox_setMargin(FFFF)Z
.end method

.method private native ObjectTextBox_setParagraph(Ljava/util/ArrayList;)Z
.end method

.method private native ObjectTextBox_setSpan(Ljava/util/ArrayList;)Z
.end method

.method private native ObjectTextBox_setText(Ljava/lang/String;)Z
.end method

.method private native ObjectTextBox_setTextAlignment(I)Z
.end method

.method private native ObjectTextBox_setTextColor(I)Z
.end method

.method private native ObjectTextBox_setTextDirection(I)Z
.end method

.method private native ObjectTextBox_setTextIndentLevel(I)Z
.end method

.method private native ObjectTextBox_setTextInputType(I)Z
.end method

.method private native ObjectTextBox_setTextLineSpacingInfo(IF)Z
.end method

.method private native ObjectTextBox_setTextStyle(I)Z
.end method

.method private native ObjectTextBox_setVerticalPan(F)Z
.end method

.method private static parseHyperlink(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 14

    const/4 v13, 0x2

    const/4 v12, 0x3

    const/4 v3, 0x0

    const/4 v2, 0x1

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const-string/jumbo v0, "\\b(?:(?:[\\w]?[\\d]{1,4}[\\-\\s](?:\\d[0-9a-zA-Z]{0,3})?)|(?:\\d[0-9a-zA-Z]{0,3}))\\b"

    const-string/jumbo v0, "(?:\\s|\\,|(?:\\&nbsp;)|(?:\\&middot;)){1,3}"

    const-string/jumbo v0, "\\b(?:(?:[\\d]{4})|(?:[a-zA-Z][a-zA-Z0-9]{1,3}[\\-\\s][0-9][a-zA-Z][a-zA-Z])|(?:[a-zA-Z][0-9][a-zA-Z][\\-\\s][0-9][a-zA-Z][0-9])|(?:[\\d]{5}(?:[\\-\\s][\\d]{4})?))"

    const-string/jumbo v0, "(?:[a-zA-Z\u00a0-\uaf00\\\'\\s]{2,20}(?:\\s|\\,|(?:\\&nbsp;)|(?:\\&middot;)){1,3}[a-zA-Z\u00a0-\uaf00\\\']{2,10})"

    const-string/jumbo v0, "(?:[\\s\\,][\\s]?(?:[Uu]nited\\s)?[\u00a0-\uaf00\\\'\\w\\.]{2,20}\\b)?"

    const-string/jumbo v0, "(?:(?:Ankara)|(?:Athens)|(?:Atlanta)|(?:Baghdad)|(?:Bandung)|(?:Bangalore)|(?:Bangkok)|(?:Barcelona)|(?:Beijing)|(?:Berlin)|(?:Bombay)|(?:Boston)|(?:Brasillia)|(?:Buenos\\sAires)|(?:Busan)|(?:Cairo)|(?:Calcutta)|(?:Casablandca)|(?:Chicago)|(?:Chongqing)|(?:Dallas)|(?:Delhi)|(?:Detroit)|(?:Dhaka)|(?:Guangzhou)|(?:Hanoi)|(?:Hong\\sKong)|(?:Houston)|(?:Istanbul)|(?:Karachi)|(?:Jakarta)|(?:Kobe)|(?:Lagos)|(?:Lahore)|(?:Lima)|(?:London)|(?:Los\\sAngeles)|(?:Madrid)|(?:Melbourne)|(?:Metro\\sManila)|(?:Mexico\\sCity)|(?:Miami)|(?:Milan)|(?:Montreal)|(?:Moscow)|(?:Mumbai)|(?:New\\sYork)|(?:Osaka)|(?:Paris)|(?:Philadelphia)|(?:Phoenix)|(?:Pusan)|(?:Rio\\sde\\sJaneiro)|(?:Santiago)|(?:Sao\\sPaulo)|(?:Seoul)|(?:Shanghai)|(?:Shenyang)|(?:Singapore)|(?:Sydney)|(?:Tehran)|(?:Tianjin)|(?:Tokyo)|(?:Toronto)|(?:Washington(?:[\\,\\s]{1,2}[Dd][\\.]?[Cc][\\.]?))|(?:Wuhan)|(?:Xi[\\\']?an))"

    const-string/jumbo v0, "\\b(?:(?:[\\w]?[\\d]{1,4}[\\-\\s](?:\\d[0-9a-zA-Z]{0,3})?)|(?:\\d[0-9a-zA-Z]{0,3}))\\b(?:\\s|\\,|(?:\\&nbsp;)){1,3}[\\w\u00a0-\uaf00](?:[\u00a0-\uaf00\\\'\\w\\s#@\\-\\,\\.]{4,12})(?:\\s|\\,|(?:\\&nbsp;)|(?:\\&middot;)){1,3}(?i:<[^>]+>(?:[\\s\\x0d\\x0a]|(?:\\&nbsp;)){0,2}){0,5}(?:(?:(?:[a-zA-Z\u00a0-\uaf00\\\'\\s]{2,20}(?:\\s|\\,|(?:\\&nbsp;)|(?:\\&middot;)){1,3}[a-zA-Z\u00a0-\uaf00\\\']{2,10})(?:\\s|\\,|(?:\\&nbsp;)){1,3}\\b(?:(?:[\\d]{4})|(?:[a-zA-Z][a-zA-Z0-9]{1,3}[\\-\\s][0-9][a-zA-Z][a-zA-Z])|(?:[a-zA-Z][0-9][a-zA-Z][\\-\\s][0-9][a-zA-Z][0-9])|(?:[\\d]{5}(?:[\\-\\s][\\d]{4})?))\\b)|(?:\\b(?:(?:[\\d]{4})|(?:[a-zA-Z][a-zA-Z0-9]{1,3}[\\-\\s][0-9][a-zA-Z][a-zA-Z])|(?:[a-zA-Z][0-9][a-zA-Z][\\-\\s][0-9][a-zA-Z][0-9])|(?:[\\d]{5}(?:[\\-\\s][\\d]{4})?))(?:\\s|\\,|(?:\\&nbsp;)){1,3}(?:[a-zA-Z\u00a0-\uaf00\\\'\\s]{2,20}(?:\\s|\\,|(?:\\&nbsp;)|(?:\\&middot;)){1,3}[a-zA-Z\u00a0-\uaf00\\\']{2,10})\\b)|(?:(?:(?:Ankara)|(?:Athens)|(?:Atlanta)|(?:Baghdad)|(?:Bandung)|(?:Bangalore)|(?:Bangkok)|(?:Barcelona)|(?:Beijing)|(?:Berlin)|(?:Bombay)|(?:Boston)|(?:Brasillia)|(?:Buenos\\sAires)|(?:Busan)|(?:Cairo)|(?:Calcutta)|(?:Casablandca)|(?:Chicago)|(?:Chongqing)|(?:Dallas)|(?:Delhi)|(?:Detroit)|(?:Dhaka)|(?:Guangzhou)|(?:Hanoi)|(?:Hong\\sKong)|(?:Houston)|(?:Istanbul)|(?:Karachi)|(?:Jakarta)|(?:Kobe)|(?:Lagos)|(?:Lahore)|(?:Lima)|(?:London)|(?:Los\\sAngeles)|(?:Madrid)|(?:Melbourne)|(?:Metro\\sManila)|(?:Mexico\\sCity)|(?:Miami)|(?:Milan)|(?:Montreal)|(?:Moscow)|(?:Mumbai)|(?:New\\sYork)|(?:Osaka)|(?:Paris)|(?:Philadelphia)|(?:Phoenix)|(?:Pusan)|(?:Rio\\sde\\sJaneiro)|(?:Santiago)|(?:Sao\\sPaulo)|(?:Seoul)|(?:Shanghai)|(?:Shenyang)|(?:Singapore)|(?:Sydney)|(?:Tehran)|(?:Tianjin)|(?:Tokyo)|(?:Toronto)|(?:Washington(?:[\\,\\s]{1,2}[Dd][\\.]?[Cc][\\.]?))|(?:Wuhan)|(?:Xi[\\\']?an))))(?:[\\s\\,][\\s]?(?:[Uu]nited\\s)?[\u00a0-\uaf00\\\'\\w\\.]{2,20}\\b)?"

    const-string/jumbo v0, "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}(?:\\@|(?:\\&\\#[0]*64\\;))[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}(?:\\.[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25})+"

    const-string/jumbo v0, "(([0-9]{4}/[0-9]{1,2}/[0-9]{1,2})|([0-9]{1,2}/[0-9]{1,2}/[0-9]{2,4}))(\\s[0-9]{1,2}(:[0-9]{1,2})?(\\s(AM|Am|am|PM|Pm|pm))?)?"

    const-string/jumbo v0, "(?:\\+[0-9]+[\\- \\.]*)?(?:[0-9]{2,5}[\\- \\.]?[0-9]{3,5}[\\- \\.]?[0-9]{3,5})"

    const-string/jumbo v0, "((https?|ftp)://)?([a-z0-9+!*(),;?&=$_.-]+(:[a-z0-9+!*(),;?&=$_.-]+)?@)?([A-Za-z0-9-.]*)\\.([a-z]{2,3})(:[0-9]{2,5})?(/([A-Za-z0-9+$_-].?[A-Za-z0-9;:@&%=+/$_.-?]*)+)*/?(#[A-Za-z_.-][A-Za-z0-9+$_.-]*)?(\\W|$)"

    new-instance v5, Ljava/util/LinkedHashMap;

    invoke-direct {v5}, Ljava/util/LinkedHashMap;-><init>()V

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string/jumbo v1, "(?:\\+[0-9]+[\\- \\.]*)?(?:[0-9]{2,5}[\\- \\.]?[0-9]{3,5}[\\- \\.]?[0-9]{3,5})"

    invoke-virtual {v5, v0, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string/jumbo v1, "\\b(?:(?:[\\w]?[\\d]{1,4}[\\-\\s](?:\\d[0-9a-zA-Z]{0,3})?)|(?:\\d[0-9a-zA-Z]{0,3}))\\b(?:\\s|\\,|(?:\\&nbsp;)){1,3}[\\w\u00a0-\uaf00](?:[\u00a0-\uaf00\\\'\\w\\s#@\\-\\,\\.]{4,12})(?:\\s|\\,|(?:\\&nbsp;)|(?:\\&middot;)){1,3}(?i:<[^>]+>(?:[\\s\\x0d\\x0a]|(?:\\&nbsp;)){0,2}){0,5}(?:(?:(?:[a-zA-Z\u00a0-\uaf00\\\'\\s]{2,20}(?:\\s|\\,|(?:\\&nbsp;)|(?:\\&middot;)){1,3}[a-zA-Z\u00a0-\uaf00\\\']{2,10})(?:\\s|\\,|(?:\\&nbsp;)){1,3}\\b(?:(?:[\\d]{4})|(?:[a-zA-Z][a-zA-Z0-9]{1,3}[\\-\\s][0-9][a-zA-Z][a-zA-Z])|(?:[a-zA-Z][0-9][a-zA-Z][\\-\\s][0-9][a-zA-Z][0-9])|(?:[\\d]{5}(?:[\\-\\s][\\d]{4})?))\\b)|(?:\\b(?:(?:[\\d]{4})|(?:[a-zA-Z][a-zA-Z0-9]{1,3}[\\-\\s][0-9][a-zA-Z][a-zA-Z])|(?:[a-zA-Z][0-9][a-zA-Z][\\-\\s][0-9][a-zA-Z][0-9])|(?:[\\d]{5}(?:[\\-\\s][\\d]{4})?))(?:\\s|\\,|(?:\\&nbsp;)){1,3}(?:[a-zA-Z\u00a0-\uaf00\\\'\\s]{2,20}(?:\\s|\\,|(?:\\&nbsp;)|(?:\\&middot;)){1,3}[a-zA-Z\u00a0-\uaf00\\\']{2,10})\\b)|(?:(?:(?:Ankara)|(?:Athens)|(?:Atlanta)|(?:Baghdad)|(?:Bandung)|(?:Bangalore)|(?:Bangkok)|(?:Barcelona)|(?:Beijing)|(?:Berlin)|(?:Bombay)|(?:Boston)|(?:Brasillia)|(?:Buenos\\sAires)|(?:Busan)|(?:Cairo)|(?:Calcutta)|(?:Casablandca)|(?:Chicago)|(?:Chongqing)|(?:Dallas)|(?:Delhi)|(?:Detroit)|(?:Dhaka)|(?:Guangzhou)|(?:Hanoi)|(?:Hong\\sKong)|(?:Houston)|(?:Istanbul)|(?:Karachi)|(?:Jakarta)|(?:Kobe)|(?:Lagos)|(?:Lahore)|(?:Lima)|(?:London)|(?:Los\\sAngeles)|(?:Madrid)|(?:Melbourne)|(?:Metro\\sManila)|(?:Mexico\\sCity)|(?:Miami)|(?:Milan)|(?:Montreal)|(?:Moscow)|(?:Mumbai)|(?:New\\sYork)|(?:Osaka)|(?:Paris)|(?:Philadelphia)|(?:Phoenix)|(?:Pusan)|(?:Rio\\sde\\sJaneiro)|(?:Santiago)|(?:Sao\\sPaulo)|(?:Seoul)|(?:Shanghai)|(?:Shenyang)|(?:Singapore)|(?:Sydney)|(?:Tehran)|(?:Tianjin)|(?:Tokyo)|(?:Toronto)|(?:Washington(?:[\\,\\s]{1,2}[Dd][\\.]?[Cc][\\.]?))|(?:Wuhan)|(?:Xi[\\\']?an))))(?:[\\s\\,][\\s]?(?:[Uu]nited\\s)?[\u00a0-\uaf00\\\'\\w\\.]{2,20}\\b)?"

    invoke-virtual {v5, v0, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string/jumbo v1, "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}(?:\\@|(?:\\&\\#[0]*64\\;))[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}(?:\\.[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25})+"

    invoke-virtual {v5, v0, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string/jumbo v1, "(([0-9]{4}/[0-9]{1,2}/[0-9]{1,2})|([0-9]{1,2}/[0-9]{1,2}/[0-9]{2,4}))(\\s[0-9]{1,2}(:[0-9]{1,2})?(\\s(AM|Am|am|PM|Pm|pm))?)?"

    invoke-virtual {v5, v0, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string/jumbo v1, "((https?|ftp)://)?([a-z0-9+!*(),;?&=$_.-]+(:[a-z0-9+!*(),;?&=$_.-]+)?@)?([A-Za-z0-9-.]*)\\.([a-z]{2,3})(:[0-9]{2,5})?(/([A-Za-z0-9+$_-].?[A-Za-z0-9;:@&%=+/$_.-?]*)+)*/?(#[A-Za-z_.-][A-Za-z0-9+$_.-]*)?(\\W|$)"

    invoke-virtual {v5, v0, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v5}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    return-object v4

    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v5, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v7

    :cond_2
    :goto_0
    invoke-virtual {v7}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_0

    new-array v8, v12, [I

    invoke-virtual {v7}, Ljava/util/regex/Matcher;->start()I

    move-result v1

    aput v1, v8, v3

    invoke-virtual {v7}, Ljava/util/regex/Matcher;->end()I

    move-result v1

    aput v1, v8, v2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    aput v1, v8, v13

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v12, :cond_3

    aget v1, v8, v2

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    if-ge v1, v9, :cond_3

    aget v1, v8, v2

    add-int/lit8 v1, v1, -0x1

    aput v1, v8, v2

    :cond_3
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_5

    move v1, v3

    :goto_1
    if-nez v1, :cond_2

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_5
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [I

    aget v10, v8, v3

    aget v11, v1, v3

    if-lt v10, v11, :cond_6

    aget v10, v8, v3

    aget v11, v1, v2

    if-le v10, v11, :cond_7

    :cond_6
    aget v10, v8, v2

    aget v11, v1, v3

    if-lt v10, v11, :cond_4

    aget v10, v8, v2

    aget v1, v1, v2

    if-gt v10, v1, :cond_4

    :cond_7
    move v1, v2

    goto :goto_1
.end method

.method private throwUncheckedException(I)V
    .locals 3

    const/16 v0, 0x13

    if-ne p1, v0, :cond_0

    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "SpenObjectTextBox("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ") is already closed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {p1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    return-void
.end method


# virtual methods
.method public appendParagraph(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_appendParagraph(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public appendSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_appendSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public clearChangedFlag()V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_clearChangedFlag()V

    return-void
.end method

.method public copy(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_copy(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public findParagraphs(II)Ljava/util/ArrayList;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_findParagraphs(II)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public findSpans(II)Ljava/util/ArrayList;
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_findSpans(II)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getAutoFitOption()I
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getAutoFitOption()I

    move-result v0

    return v0
.end method

.method public getBackgroundColor()I
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getBackgroundColor()I

    move-result v0

    return v0
.end method

.method public getBorderType()I
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getBorderType()I

    move-result v0

    return v0
.end method

.method public getBottomMargin()F
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getBottomMargin()F

    move-result v0

    return v0
.end method

.method public getCursorPos()I
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getCursorPos()I

    move-result v0

    return v0
.end method

.method public getDrawnRect()Landroid/graphics/RectF;
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getDrawnRect()Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public getEllipsisType()I
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getEllipsisType()I

    move-result v0

    return v0
.end method

.method public getFont()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getFont()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFontSize()F
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getFontSize()F

    move-result v0

    return v0
.end method

.method public getGravity()I
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getGravity()I

    move-result v0

    return v0
.end method

.method public getHintText()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getHintText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHintTextColor()I
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getHintTextColor()I

    move-result v0

    return v0
.end method

.method public getHintTextFontSize()F
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getHintTextFontSize()F

    move-result v0

    return v0
.end method

.method public getIMEActionType()I
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getIMEActionType()I

    move-result v0

    return v0
.end method

.method public getLeftMargin()F
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getLeftMargin()F

    move-result v0

    return v0
.end method

.method public getLineBorderColor()I
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getLineBorderColor()I

    move-result v0

    return v0
.end method

.method public getLineBorderWidth()F
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getLineBorderWidth()F

    move-result v0

    return v0
.end method

.method public getParagraph()Ljava/util/ArrayList;
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getParagraph()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getRightMargin()F
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getRightMargin()F

    move-result v0

    return v0
.end method

.method public getSpan()Ljava/util/ArrayList;
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getSpan()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTextAlignment()I
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getTextAlignment()I

    move-result v0

    return v0
.end method

.method public getTextColor()I
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getTextColor()I

    move-result v0

    return v0
.end method

.method public getTextDirection()I
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getTextDirection()I

    move-result v0

    return v0
.end method

.method public getTextInputType()I
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getTextInputType()I

    move-result v0

    return v0
.end method

.method public getTextLineSpacing()F
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getTextLineSpacing()F

    move-result v0

    return v0
.end method

.method public getTextLineSpacingType()I
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getTextLineSpacingType()I

    move-result v0

    return v0
.end method

.method public getTextStyle()I
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getTextStyle()I

    move-result v0

    return v0
.end method

.method public getTopMargin()F
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getTopMargin()F

    move-result v0

    return v0
.end method

.method public getVerticalPan()F
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_getVerticalPan()F

    move-result v0

    return v0
.end method

.method public insertText(Ljava/lang/String;I)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_insertText(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public insertTextAtCursor(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_insertTextAtCursor(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public isChanged()Z
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_isChanged()Z

    move-result v0

    return v0
.end method

.method public isHintTextEnabled()Z
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_isHintTextVisiable()Z

    move-result v0

    return v0
.end method

.method public isReadOnlyEnabled()Z
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_isReadOnly()Z

    move-result v0

    return v0
.end method

.method public parseHyperText()V
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_parseHyperText()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public removeAllText()V
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_removeAllText()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public removeParagraph(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_removeParagraph(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public removeSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_removeSpan(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public removeText(II)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_removeText(II)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public replaceText(Ljava/lang/String;II)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_replaceText(Ljava/lang/String;II)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setAutoFitOption(I)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setAutoFitOption(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setBackgroundColor(I)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setBackgroundColor(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setBorderType(I)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setBorderType(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setCursorPos(I)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setCursorPos(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setEllipsisType(I)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setEllipsisType(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setFont(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setFont(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setFontSize(F)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setFontSize(F)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setGravity(I)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setGravity(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setHintText(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setHintText(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setHintTextColor(I)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setHintTextColor(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setHintTextEnabled(Z)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setHintTextVisibility(Z)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setHintTextFontSize(I)V
    .locals 1

    int-to-float v0, p1

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setHintTextFontSize(F)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setIMEActionType(I)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setIMEActionType(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setLineBorderColor(I)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setLineBorderColor(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setLineBorderWidth(F)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setLineBorderWidth(F)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setMargin(FFFF)V
    .locals 1

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setMargin(FFFF)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setParagraph(Ljava/util/ArrayList;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setParagraph(Ljava/util/ArrayList;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setReadOnlyEnabled(Z)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_enableReadOnly(Z)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setRect(Landroid/graphics/RectF;Z)V
    .locals 0

    invoke-super {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setRect(Landroid/graphics/RectF;Z)V

    return-void
.end method

.method public setSpan(Ljava/util/ArrayList;)V
    .locals 3

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setSpan(Ljava/util/ArrayList;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;

    iget v2, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->startPos:I

    if-ltz v2, :cond_4

    iget v0, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;->endPos:I

    if-gez v0, :cond_0

    :cond_4
    const/4 v0, 0x7

    const-string/jumbo v1, "startPos and endPos of TextSpanInfo should have positive value"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setText(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setTextAlignment(I)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setTextAlignment(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setTextColor(I)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setTextColor(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setTextDirection(I)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setTextDirection(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setTextInputType(I)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setTextInputType(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setTextLineSpacingInfo(IF)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setTextLineSpacingInfo(IF)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setTextStyle(I)V
    .locals 2

    and-int/lit8 v0, p1, 0x7

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "style is invalid"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setTextStyle(I)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_1
    return-void
.end method

.method public setVereticalPan(F)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->ObjectTextBox_setVerticalPan(F)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method
