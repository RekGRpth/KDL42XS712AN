.class public Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final TAG:Ljava/lang/String; = "VIShapeRecognition"


# instance fields
.field private final mResultPointsList:Ljava/util/ArrayList;

.field private mShRecLib:Lcom/samsung/vip/engine/d;

.field private mShapeTypeList:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->mShapeTypeList:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->mResultPointsList:Ljava/util/ArrayList;

    return-void
.end method

.method private makeResults([Lef;)V
    .locals 8

    new-instance v1, Lcom/samsung/vip/engine/c;

    invoke-direct {v1}, Lcom/samsung/vip/engine/c;-><init>()V

    array-length v2, p1

    if-lez v2, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->mShapeTypeList:Ljava/util/ArrayList;

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->mResultPointsList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    const/4 v0, 0x0

    :goto_0
    if-lt v0, v2, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->mResultPointsList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Lcom/samsung/vip/engine/c;->a(Ljava/util/ArrayList;)V

    return-void

    :cond_1
    aget-object v3, p1, v0

    if-nez v3, :cond_2

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->mShRecLib:Lcom/samsung/vip/engine/d;

    iget-short v5, v3, Lef;->b:S

    invoke-virtual {v4, v5}, Lcom/samsung/vip/engine/d;->a(I)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "VIShapeRecognition"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "Id: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-short v7, v3, Lef;->a:S

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ", Type: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->mShapeTypeList:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1, v3}, Lcom/samsung/vip/engine/c;->a(Lef;)V

    goto :goto_1
.end method


# virtual methods
.method public addStroke([F[F)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->mShRecLib:Lcom/samsung/vip/engine/d;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/vip/engine/d;->a([F[F)V

    return-void
.end method

.method public dispose()V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->mShRecLib:Lcom/samsung/vip/engine/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->mShRecLib:Lcom/samsung/vip/engine/d;

    invoke-virtual {v0}, Lcom/samsung/vip/engine/d;->b()V

    :cond_0
    return-void
.end method

.method public getResult()Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->mResultPointsList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getShapeTypeList()Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->mShapeTypeList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public init(Landroid/content/Context;)Z
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->mShRecLib:Lcom/samsung/vip/engine/d;

    new-instance v0, Lcom/samsung/vip/engine/d;

    invoke-direct {v0}, Lcom/samsung/vip/engine/d;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->mShRecLib:Lcom/samsung/vip/engine/d;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->mShRecLib:Lcom/samsung/vip/engine/d;

    invoke-virtual {v0}, Lcom/samsung/vip/engine/d;->a()V

    const/4 v0, 0x1

    return v0
.end method

.method public process()Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->mShRecLib:Lcom/samsung/vip/engine/d;

    invoke-virtual {v0}, Lcom/samsung/vip/engine/d;->c()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->mShRecLib:Lcom/samsung/vip/engine/d;

    invoke-virtual {v0}, Lcom/samsung/vip/engine/d;->d()[Lef;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->mResultPointsList:Ljava/util/ArrayList;

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->makeResults([Lef;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->mResultPointsList:Ljava/util/ArrayList;

    goto :goto_0
.end method
