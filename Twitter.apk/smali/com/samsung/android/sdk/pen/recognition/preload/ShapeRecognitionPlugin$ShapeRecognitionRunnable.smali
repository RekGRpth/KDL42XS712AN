.class Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$ShapeRecognitionRunnable;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private mInput:Ljava/util/List;

.field final synthetic this$0:Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;Ljava/util/List;)V
    .locals 1

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$ShapeRecognitionRunnable;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$ShapeRecognitionRunnable;->mInput:Ljava/util/List;

    iput-object p2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$ShapeRecognitionRunnable;->mInput:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    const/high16 v12, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$ShapeRecognitionRunnable;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$ShapeRecognitionRunnable;->mInput:Ljava/util/List;

    check-cast v0, Ljava/util/ArrayList;

    # invokes: Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->addStroke(Ljava/util/ArrayList;)V
    invoke-static {v1, v0}, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->access$1(Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;Ljava/util/ArrayList;)V

    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;
    invoke-static {}, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->access$2()Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->process()Ljava/util/ArrayList;

    move-result-object v1

    const-string/jumbo v0, "ShapeRecognitionPlugin"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "process: pointList size = "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_0

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;
    invoke-static {}, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->access$2()Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->getShapeTypeList()Ljava/util/ArrayList;

    move-result-object v6

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v2, v3

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v4

    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$ShapeRecognitionRunnable;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;

    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->access$3(Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$HandleInfo;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$ShapeRecognitionRunnable;->mInput:Ljava/util/List;

    invoke-direct {v2, v4, v0}, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$HandleInfo;-><init>(Ljava/util/List;Ljava/util/List;)V

    invoke-static {v1, v3, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$ShapeRecognitionRunnable;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;

    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->access$3(Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :cond_1
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/graphics/PointF;

    array-length v1, v0

    const/4 v8, 0x1

    if-ne v1, v8, :cond_2

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_4

    new-instance v8, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;

    invoke-direct {v8, v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;-><init>(Ljava/util/ArrayList;)V

    const-string/jumbo v1, "ShapeRecognitionPlugin"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v0, "process: name = "

    invoke-direct {v9, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v9, "["

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v9, "]"

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v9, "ShapeType"

    add-int/lit8 v1, v2, 0x1

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v8, v9, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectContainer;->setExtraDataString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v0, v1

    :goto_1
    invoke-virtual {v5}, Ljava/util/ArrayList;->clear()V

    move v2, v0

    goto :goto_0

    :cond_2
    array-length v1, v0

    add-int/lit8 v1, v1, 0x1

    new-array v8, v1, [F

    array-length v1, v0

    new-array v9, v1, [I

    move v1, v3

    :goto_2
    array-length v10, v0

    if-lt v1, v10, :cond_3

    array-length v1, v0

    aput v12, v8, v1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$ShapeRecognitionRunnable;->mInput:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    new-instance v10, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPenName()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11, v0, v8, v9}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;-><init>(Ljava/lang/String;[Landroid/graphics/PointF;[F[I)V

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPenSize()F

    move-result v0

    invoke-virtual {v10, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->setPenSize(F)V

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getColor()I

    move-result v0

    invoke-virtual {v10, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->setColor(I)V

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_3
    aput v12, v8, v1

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v10

    long-to-int v10, v10

    aput v10, v9, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_1
.end method
