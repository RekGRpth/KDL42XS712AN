.class Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$EquationRecognitionRunnable;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private mInput:Ljava/util/List;

.field final synthetic this$0:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;Ljava/util/List;)V
    .locals 1

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$EquationRecognitionRunnable;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$EquationRecognitionRunnable;->mInput:Ljava/util/List;

    iput-object p2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$EquationRecognitionRunnable;->mInput:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    const/16 v3, 0x400

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$EquationRecognitionRunnable;->mInput:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;
    invoke-static {}, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->access$1()Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;->process()Ljava/lang/String;

    move-result-object v2

    const/4 v0, 0x0

    if-eqz v2, :cond_1

    const-string/jumbo v0, "recognition-EquationRecognitionPlugin"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "result = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-direct {v3, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$EquationRecognitionRunnable;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;

    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->access$2(Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;)Landroid/os/Handler;

    move-result-object v2

    new-instance v3, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$HandleInfo;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$EquationRecognitionRunnable;->mInput:Ljava/util/List;

    invoke-direct {v3, v4, v0}, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$HandleInfo;-><init>(Ljava/util/List;Ljava/util/List;)V

    invoke-static {v2, v1, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$EquationRecognitionRunnable;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;

    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->access$2(Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPoints()[Landroid/graphics/PointF;

    move-result-object v5

    if-eqz v5, :cond_0

    array-length v0, v5

    if-le v0, v3, :cond_4

    move v2, v3

    :goto_1
    if-lez v2, :cond_0

    new-array v6, v2, [F

    new-array v7, v2, [F

    move v0, v1

    :goto_2
    if-lt v0, v2, :cond_3

    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;
    invoke-static {}, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->access$1()Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;->addStroke([F[F)V

    goto :goto_0

    :cond_3
    aget-object v8, v5, v0

    iget v8, v8, Landroid/graphics/PointF;->x:F

    aput v8, v6, v0

    aget-object v8, v5, v0

    iget v8, v8, Landroid/graphics/PointF;->y:F

    aput v8, v7, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    move v2, v0

    goto :goto_1
.end method
