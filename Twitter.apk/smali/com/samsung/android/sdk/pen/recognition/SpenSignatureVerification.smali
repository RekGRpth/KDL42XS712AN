.class public final Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final VERIFICATION_LEVEL_HIGH:I = 0x2

.field private static final VERIFICATION_LEVEL_KEY:Ljava/lang/String; = "VerificationLevel"

.field public static final VERIFICATION_LEVEL_LOW:I = 0x0

.field public static final VERIFICATION_LEVEL_MEDIUM:I = 0x1


# instance fields
.field private mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

.field private mResultListener:Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification$ResultListener;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mResultListener:Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification$ResultListener;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "E_INVALID_ARG : parameter \'context\' is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p2, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "E_INVALID_ARG : parameter \'pluginObject\' is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;)Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification$ResultListener;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mResultListener:Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification$ResultListener;

    return-object v0
.end method


# virtual methods
.method public getMinimumRequiredCount()I
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "E_INVALID_STATE : SpenSignatureVerification is not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;->getMinimumRequiredCount()I

    move-result v0

    return v0
.end method

.method getPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    return-object v0
.end method

.method public getRegisteredCount()I
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "E_INVALID_STATE : SpenSignatureVerification is not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;->getRegisteredCount()I

    move-result v0

    return v0
.end method

.method public getVerificationLevel()I
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "E_INVALID_STATE : SpenSignatureVerification is not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;->getProperty(Landroid/os/Bundle;)V

    const-string/jumbo v1, "VerificationLevel"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_1
    const-string/jumbo v1, "VerificationLevel"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public register(Ljava/util/List;)V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "E_INVALID_STATE : SpenSignatureVerification is not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "E_INVALID_ARG : stroke is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;->register(Ljava/util/List;)V

    return-void
.end method

.method public request(Ljava/util/List;)V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "E_INVALID_STATE : SpenSignatureVerification is not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;->request(Ljava/util/List;)V

    return-void
.end method

.method public setResultListener(Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification$ResultListener;)V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "E_INVALID_STATE : SpenSignatureVerification is not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mResultListener:Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification$ResultListener;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mResultListener:Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification$ResultListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    new-instance v1, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification$1;-><init>(Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;)V

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;->setResultListener(Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface$ResultListener;)V

    :cond_1
    return-void
.end method

.method public setVerificationLevel(I)V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "E_INVALID_STATE : SpenSignatureVerification is not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v1, "VerificationLevel"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;->setProperty(Landroid/os/Bundle;)V

    return-void
.end method

.method public unregisterAll()V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "E_INVALID_STATE : SpenSignatureVerification is not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenSignatureVerification;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;->unregisterAll()V

    return-void
.end method
