.class Lcom/samsung/android/sdk/pen/recognition/preload/Signature$SignatureVerificationRunnable;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private mInput:Ljava/util/List;

.field final synthetic this$0:Lcom/samsung/android/sdk/pen/recognition/preload/Signature;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/recognition/preload/Signature;Ljava/util/List;)V
    .locals 1

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$SignatureVerificationRunnable;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/Signature;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$SignatureVerificationRunnable;->mInput:Ljava/util/List;

    iput-object p2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$SignatureVerificationRunnable;->mInput:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$SignatureVerificationRunnable;->mInput:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$SignatureVerificationRunnable;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/Signature;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$SignatureVerificationRunnable;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/Signature;

    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mVerificationLevel:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->access$2(Lcom/samsung/android/sdk/pen/recognition/preload/Signature;)I

    move-result v1

    # invokes: Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->verifySignature(I)Z
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->access$3(Lcom/samsung/android/sdk/pen/recognition/preload/Signature;I)Z

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$SignatureVerificationRunnable;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/Signature;

    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->access$4(Lcom/samsung/android/sdk/pen/recognition/preload/Signature;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    new-instance v3, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$HandleInfo;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$SignatureVerificationRunnable;->mInput:Ljava/util/List;

    invoke-direct {v3, v4, v0}, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$HandleInfo;-><init>(Ljava/util/List;Z)V

    invoke-static {v1, v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$SignatureVerificationRunnable;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/Signature;

    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->access$4(Lcom/samsung/android/sdk/pen/recognition/preload/Signature;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPoints()[Landroid/graphics/PointF;

    move-result-object v2

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getTimeStamps()[I

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPressures()[F

    move-result-object v0

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$SignatureVerificationRunnable;->this$0:Lcom/samsung/android/sdk/pen/recognition/preload/Signature;

    # getter for: Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHSVLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->access$1(Lcom/samsung/android/sdk/pen/recognition/preload/Signature;)Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

    move-result-object v4

    invoke-virtual {v4, v2, v3, v0}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->setDrawData([Landroid/graphics/PointF;[I[F)V

    goto :goto_0
.end method
