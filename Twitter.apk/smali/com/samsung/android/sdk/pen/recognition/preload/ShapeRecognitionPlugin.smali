.class public Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;


# static fields
.field private static final TAG:Ljava/lang/String; = "ShapeRecognitionPlugin"

.field private static mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$1;-><init>(Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->mHandler:Landroid/os/Handler;

    const-string/jumbo v0, "ShapeRecognitionPlugin"

    const-string/jumbo v1, "creating shape recognition plugin"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;)Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;Ljava/util/ArrayList;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->addStroke(Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$2()Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;
    .locals 1

    sget-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private addStroke(Ljava/util/ArrayList;)V
    .locals 13

    const/16 v3, 0x400

    const/high16 v12, 0x3f000000    # 0.5f

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPoints()[Landroid/graphics/PointF;

    move-result-object v5

    if-eqz v5, :cond_0

    array-length v0, v5

    if-le v0, v3, :cond_5

    move v2, v3

    :goto_1
    if-lez v2, :cond_0

    new-array v6, v2, [F

    new-array v7, v2, [F

    const/4 v0, 0x0

    move v1, v0

    :goto_2
    if-lt v1, v2, :cond_2

    sget-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;

    invoke-virtual {v0, v6, v7}, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->addStroke([F[F)V

    goto :goto_0

    :cond_2
    aget-object v0, v5, v1

    iget v0, v0, Landroid/graphics/PointF;->x:F

    aget-object v8, v5, v1

    iget v8, v8, Landroid/graphics/PointF;->y:F

    aget-object v9, v5, v1

    iget v9, v9, Landroid/graphics/PointF;->x:F

    float-to-int v9, v9

    aget-object v10, v5, v1

    iget v10, v10, Landroid/graphics/PointF;->y:F

    float-to-int v10, v10

    int-to-float v11, v9

    sub-float/2addr v0, v11

    cmpl-float v0, v0, v12

    if-lez v0, :cond_3

    int-to-float v0, v9

    add-float/2addr v0, v12

    :goto_3
    aput v0, v6, v1

    int-to-float v0, v10

    sub-float v0, v8, v0

    cmpl-float v0, v0, v12

    if-lez v0, :cond_4

    int-to-float v0, v10

    add-float/2addr v0, v12

    :goto_4
    aput v0, v7, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_3
    int-to-float v0, v9

    goto :goto_3

    :cond_4
    int-to-float v0, v10

    goto :goto_4

    :cond_5
    move v2, v0

    goto :goto_1
.end method


# virtual methods
.method public getNativeHandle()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getPrivateKeyHint()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getProperty(Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public initRecognizer(Landroid/content/Context;)Z
    .locals 3

    const/4 v0, 0x0

    sget-object v1, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;

    if-nez v1, :cond_1

    new-instance v1, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;

    invoke-direct {v1}, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;-><init>()V

    sput-object v1, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;

    sget-object v1, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;

    if-nez v1, :cond_0

    const-string/jumbo v1, "ShapeRecognitionPlugin"

    const-string/jumbo v2, "Fail to create Shape recognition instance."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_0
    sget-object v1, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;

    invoke-virtual {v1, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognition;->init(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string/jumbo v1, "ShapeRecognitionPlugin"

    const-string/jumbo v2, "Fail to initialize."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onLoad(Landroid/content/Context;)V
    .locals 2

    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->initRecognizer(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Fail to load Shape recognition engine"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public onUnload()V
    .locals 0

    return-void
.end method

.method public request(Ljava/util/List;)V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    if-nez v0, :cond_0

    const-string/jumbo v0, "ShapeRecognitionPlugin"

    const-string/jumbo v1, "The result listener isn\'t set yet!!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$ShapeRecognitionRunnable;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin$ShapeRecognitionRunnable;-><init>(Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;Ljava/util/List;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public setProperty(Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public setResultListener(Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/ShapeRecognitionPlugin;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    return-void
.end method

.method public unlock(Ljava/lang/String;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
