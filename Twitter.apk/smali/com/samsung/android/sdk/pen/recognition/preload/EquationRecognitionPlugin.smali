.class public Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;


# static fields
.field private static final TAG:Ljava/lang/String; = "recognition-EquationRecognitionPlugin"

.field private static mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;


# instance fields
.field private final mHandler:Landroid/os/Handler;

.field private mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$1;-><init>(Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mHandler:Landroid/os/Handler;

    const-string/jumbo v0, "recognition-EquationRecognitionPlugin"

    const-string/jumbo v1, "creating equation recognition plugin"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;)Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    return-object v0
.end method

.method static synthetic access$1()Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;
    .locals 1

    sget-object v0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public getNativeHandle()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getPrivateKeyHint()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getProperty(Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public initRecognizer(Landroid/content/Context;)Z
    .locals 3

    const/4 v0, 0x0

    sget-object v1, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;

    if-nez v1, :cond_1

    new-instance v1, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;-><init>(Landroid/content/Context;)V

    sput-object v1, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;

    sget-object v1, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;

    if-nez v1, :cond_0

    const-string/jumbo v1, "recognition-EquationRecognitionPlugin"

    const-string/jumbo v2, "Fail to create Equation recognition instance."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_0
    sget-object v1, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;

    invoke-virtual {v1, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;->init(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string/jumbo v1, "recognition-EquationRecognitionPlugin"

    const-string/jumbo v2, "Fail to initialize."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onLoad(Landroid/content/Context;)V
    .locals 2

    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->initRecognizer(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Fail to load Equation recognition engine"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public onUnload()V
    .locals 0

    return-void
.end method

.method public request(Ljava/util/List;)V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    if-nez v0, :cond_0

    const-string/jumbo v0, "recognition-EquationRecognitionPlugin"

    const-string/jumbo v1, "The result listener isn\'t set yet!!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$EquationRecognitionRunnable;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin$EquationRecognitionRunnable;-><init>(Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;Ljava/util/List;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public setProperty(Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public setResultListener(Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognitionPlugin;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    return-void
.end method

.method public unlock(Ljava/lang/String;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
