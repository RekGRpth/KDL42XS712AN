.class public final Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognitionManager;
.super Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;
.source "Twttr"


# static fields
.field public static final SPEN_TEXT:Ljava/lang/String; = "com.samsung.android.sdk.pen.recognition.preload.TextRecognitionPlugin"


# instance fields
.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognitionManager;->mContext:Landroid/content/Context;

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognitionManager;->mContext:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    invoke-super {p0}, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognitionManager;->mContext:Landroid/content/Context;

    return-void
.end method

.method public createRecognition(Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;)Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognition;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognitionManager;->createRecognition(Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;Ljava/lang/String;)Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognition;

    move-result-object v0

    return-object v0
.end method

.method public createRecognition(Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;Ljava/lang/String;)Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognition;
    .locals 4

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "E_INVALID_ARG : parameter \'info\' is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const-string/jumbo v0, "TextRecognition"

    const-string/jumbo v1, "SpenLanguageRecognitionInterface"

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognitionManager;->getInfoList(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;

    const-string/jumbo v1, "There is no available TextRecognition engine"

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/ClassNotFoundException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "The class \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->className:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\' is not founded"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ClassNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;

    iget-object v2, p1, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->className:Ljava/lang/String;

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->className:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :try_start_0
    new-instance v1, Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognition;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognitionManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognitionManager;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v3, v0, p2}, Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognitionManager;->createPluginObject(Landroid/content/Context;Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;Ljava/lang/String;)Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognition;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;)V
    :try_end_0
    .catch Ljava/lang/reflect/UndeclaredThrowableException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception v0

    new-instance v0, Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;

    const-string/jumbo v1, "TextRecognizer is not loaded"

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public createRecognition(Ljava/lang/String;)Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognition;
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognitionManager;->createRecognition(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognition;
    :try_end_0
    .catch Ljava/lang/reflect/UndeclaredThrowableException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v0, Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;

    const-string/jumbo v1, "TextRecognizer is not loaded"

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public createRecognition(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognition;
    .locals 4

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "E_INVALID_ARG : parameter \'className\' is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const-string/jumbo v0, "TextRecognition"

    const-string/jumbo v1, "SpenLanguageRecognitionInterface"

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognitionManager;->getInfoList(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;

    const-string/jumbo v1, "There is no available TextRecognition engine"

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/ClassNotFoundException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "The class \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\' is not founded"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ClassNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;->className:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    :try_start_0
    new-instance v1, Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognition;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognitionManager;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognitionManager;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v3, v0, p2}, Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognitionManager;->createPluginObject(Landroid/content/Context;Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;Ljava/lang/String;)Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognition;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;)V
    :try_end_0
    .catch Ljava/lang/reflect/UndeclaredThrowableException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception v0

    new-instance v0, Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;

    const-string/jumbo v1, "TextRecognizer is not loaded"

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/recognition/SpenCreationFailureException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public destroyRecognition(Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognition;)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognition;->getPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "E_INVALID_STATE : parameter \'recognition\' is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognition;->getPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognitionManager;->destroyPluginObject(Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;)V

    return-void
.end method

.method protected finalize()V
    .locals 1

    invoke-super {p0}, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->finalize()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognitionManager;->mContext:Landroid/content/Context;

    return-void
.end method

.method public getInfoList(II)Ljava/util/List;
    .locals 2

    const-string/jumbo v0, "TextRecognition"

    const-string/jumbo v1, "SpenLanguageRecognitionInterface"

    invoke-virtual {p0, v0, p1, p2, v1}, Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognitionManager;->getInfoList(Ljava/lang/String;IILjava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPrivateKeyHint(Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;)Ljava/lang/String;
    .locals 1

    invoke-super {p0, p1}, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionManager;->getPrivateKeyHint(Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionInfo;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
