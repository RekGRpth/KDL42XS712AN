.class public Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenLanguageRecognitionInterface;


# static fields
.field private static final TAG:Ljava/lang/String; = "recognition-TextRecognitionPlugin"


# instance fields
.field private mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

.field private final mRecogHandler:Landroid/os/Handler;

.field private mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin$1;-><init>(Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogHandler:Landroid/os/Handler;

    const-string/jumbo v0, "recognition-TextRecognitionPlugin"

    const-string/jumbo v1, "creating text recognition plugin"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;)Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;)Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogHandler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public getLanguage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->getCurrentLanguage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNativeHandle()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getPrivateKeyHint()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getProperty(Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public getSupportedLanguage()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->getSupportedLanguage()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method public initRecognizer(Landroid/content/Context;)Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    if-nez v1, :cond_1

    new-instance v1, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    invoke-direct {v1}, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    if-nez v1, :cond_0

    const-string/jumbo v1, "recognition-TextRecognitionPlugin"

    const-string/jumbo v2, "Fail to create TextRecognition instance"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    const-string/jumbo v2, "eng"

    invoke-virtual {v1, p1, v2}, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->init(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onLoad(Landroid/content/Context;)V
    .locals 2

    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->initRecognizer(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Fail to load Text recognition engine"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public onUnload()V
    .locals 0

    return-void
.end method

.method public request(Ljava/util/List;)V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    if-nez v0, :cond_0

    const-string/jumbo v0, "recognition-TextRecognitionPlugin"

    const-string/jumbo v1, "The result listener isn\'t set yet!!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin$TextRecognitionRunnable;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin$TextRecognitionRunnable;-><init>(Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;Ljava/util/List;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public setLanguage(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mRecogManager:Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->setLanguage(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public setProperty(Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public setResultListener(Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognitionPlugin;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;

    return-void
.end method

.method public unlock(Ljava/lang/String;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
