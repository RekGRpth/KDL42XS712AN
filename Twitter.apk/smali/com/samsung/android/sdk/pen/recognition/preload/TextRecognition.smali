.class public Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final TAG:Ljava/lang/String; = "TextRecognition"


# instance fields
.field private mCurrLanguage:Ljava/lang/String;

.field private mDataPath:Ljava/lang/String;

.field private mTxtRecLib:Lcom/samsung/vip/engine/e;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private init(Ljava/lang/String;)Z
    .locals 5

    const/4 v0, 0x1

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mCurrLanguage:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mTxtRecLib:Lcom/samsung/vip/engine/e;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mTxtRecLib:Lcom/samsung/vip/engine/e;

    invoke-virtual {v1}, Lcom/samsung/vip/engine/e;->c()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mTxtRecLib:Lcom/samsung/vip/engine/e;

    :cond_0
    new-instance v1, Lcom/samsung/vip/engine/VITextAllRecognitionLib;

    invoke-direct {v1}, Lcom/samsung/vip/engine/VITextAllRecognitionLib;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mTxtRecLib:Lcom/samsung/vip/engine/e;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mTxtRecLib:Lcom/samsung/vip/engine/e;

    invoke-virtual {v1, p1}, Lcom/samsung/vip/engine/e;->a(Ljava/lang/String;)I

    move-result v1

    if-gez v1, :cond_1

    const-string/jumbo v0, "TextRecognition"

    const-string/jumbo v1, "Not supported language!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    invoke-static {v1, v0}, Lcom/samsung/vip/engine/b;->a(IZ)I

    move-result v1

    invoke-static {v0, v0, v0}, Lcom/samsung/vip/engine/b;->a(ZZZ)S

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mTxtRecLib:Lcom/samsung/vip/engine/e;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mDataPath:Ljava/lang/String;

    invoke-virtual {v3, v4, v1, v2}, Lcom/samsung/vip/engine/e;->a(Ljava/lang/String;IS)I

    move-result v1

    const-string/jumbo v2, "TextRecognition"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "init : ret = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public addStroke([F[F)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mTxtRecLib:Lcom/samsung/vip/engine/e;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mTxtRecLib:Lcom/samsung/vip/engine/e;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/vip/engine/e;->b([F[F)V

    goto :goto_0
.end method

.method public dispose()V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mTxtRecLib:Lcom/samsung/vip/engine/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mTxtRecLib:Lcom/samsung/vip/engine/e;

    invoke-virtual {v0}, Lcom/samsung/vip/engine/e;->c()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mTxtRecLib:Lcom/samsung/vip/engine/e;

    :cond_0
    return-void
.end method

.method public getCurrentLanguage()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mTxtRecLib:Lcom/samsung/vip/engine/e;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mCurrLanguage:Ljava/lang/String;

    goto :goto_0
.end method

.method public getSupportedLanguage()Ljava/util/ArrayList;
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mTxtRecLib:Lcom/samsung/vip/engine/e;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mTxtRecLib:Lcom/samsung/vip/engine/e;

    invoke-virtual {v0}, Lcom/samsung/vip/engine/e;->e()[Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/util/ArrayList;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    goto :goto_0
.end method

.method public init(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 3

    const/4 v0, 0x0

    invoke-static {p1}, Lcom/samsung/android/sdk/pen/recognition/preload/VIRecogUtils;->copyDatabase(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "TextRecognition"

    const-string/jumbo v2, "Fail to copy database."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "/vidata/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mDataPath:Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->init(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string/jumbo v1, "TextRecognition"

    const-string/jumbo v2, "Fail to initialize."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mDataPath:Ljava/lang/String;

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public process()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mTxtRecLib:Lcom/samsung/vip/engine/e;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mTxtRecLib:Lcom/samsung/vip/engine/e;

    invoke-virtual {v0}, Lcom/samsung/vip/engine/e;->d()[Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public setLanguage(Ljava/lang/String;)V
    .locals 3

    const-string/jumbo v0, "TextRecognition"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "setLanguage() : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mTxtRecLib:Lcom/samsung/vip/engine/e;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->mTxtRecLib:Lcom/samsung/vip/engine/e;

    invoke-virtual {v0, p1}, Lcom/samsung/vip/engine/e;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "TextRecognition"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Unsupported language: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/TextRecognition;->init(Ljava/lang/String;)Z

    goto :goto_0
.end method
