.class public Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final SHOW_LOG:Z = true

.field public static final SIGNATURE_REGISTER_ERROR_DIFFERENT:I = -0x3

.field public static final SIGNATURE_REGISTER_ERROR_NORMAL:I = -0x1

.field public static final SIGNATURE_REGISTER_ERROR_SHORT:I = -0x2

.field public static final SIGNATURE_VERIFICATION_LEVEL_HIGH:I = 0x2

.field public static final SIGNATURE_VERIFICATION_LEVEL_LOW:I = 0x0

.field public static final SIGNATURE_VERIFICATION_LEVEL_MEDIUM:I = 0x1

.field public static final SIGNATURE_VERIFICATION_SCORE_DEFAULT:I = 0x1f4

.field public static final SIGNATURE_VERIFICATION_SCORE_HIGH:I = 0x258

.field public static final SIGNATURE_VERIFICATION_SCORE_LOW:I = 0x190

.field public static final SIGNATURE_VERIFICATION_SCORE_MEDIUM:I = 0x1f4

.field private static final TAG:Ljava/lang/String; = "Signature"

.field private static final USER_ID_DEFAULT:I = 0xa


# instance fields
.field private m_HSVJniLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;

.field private m_bRegistered:Z

.field private m_cTotalTag:[C

.field private m_nGENUINE_SCORE_THR:I

.field private m_nMaxNumRegistration:I

.field private m_nMaxPointCount:I

.field private m_nMinXYSize:I

.field private m_nNumRegistration:I

.field private m_nTotalPosition:[I

.field private m_nTotalPressure:[F

.field private m_nTotalTimeStamp:[I

.field private m_nUserID:I

.field private m_sCurrentPointCount:S

.field private m_sVerificationScore:S

.field private m_szSaveDirPath:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalPosition:[I

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalTimeStamp:[I

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_cTotalTag:[C

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalPressure:[F

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_szSaveDirPath:Ljava/lang/String;

    const/16 v0, 0x400

    iput v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nMaxPointCount:I

    const/4 v0, 0x3

    iput v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nMaxNumRegistration:I

    const/16 v0, 0x1f4

    iput v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nGENUINE_SCORE_THR:I

    const/16 v0, 0xa

    iput v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nUserID:I

    const/16 v0, 0xc8

    iput v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nMinXYSize:I

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_HSVJniLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;

    iput-short v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_sCurrentPointCount:S

    iput v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nNumRegistration:I

    iput-short v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_sVerificationScore:S

    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_bRegistered:Z

    return-void
.end method

.method private checkTrainingData(Ljava/lang/String;)Z
    .locals 5

    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "SPenHSVreg00.dat"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, "SPenHSVreg01.dat"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v4, "SPenHSVreg02.dat"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private deleteFolder(Ljava/lang/String;)Z
    .locals 6

    const/4 v0, 0x0

    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, "SPenHSVreg00.dat"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v4, "SPenHSVreg01.dat"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v5, "SPenHSVreg02.dat"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private makeFolder(Ljava/lang/String;)Z
    .locals 3

    const/4 v0, 0x1

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public cancel(I)Z
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->clearSignatureScreen()Z

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_HSVJniLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;

    if-eqz v1, :cond_1

    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->checkRegistration(I)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->clearSignatureScreen()Z

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_HSVJniLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;

    iget v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nUserID:I

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;->jni_HSVDelUser(I)S

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_HSVJniLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;->jni_HSVClose()S

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_HSVJniLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;

    iput v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nNumRegistration:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_szSaveDirPath:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->deleteFolder(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const-string/jumbo v0, "Signature"

    const-string/jumbo v1, "Cencel"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v0, "Signature"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "m_bRegistered = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_bRegistered:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v0, "Signature"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "m_nNumRegistration = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nNumRegistration:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const-string/jumbo v1, "Signature"

    const-string/jumbo v2, "Cencel false"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v1, "Signature"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "m_bRegistered = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_bRegistered:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v1, "Signature"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "m_nNumRegistration = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nNumRegistration:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public checkRegistration(I)Z
    .locals 2

    const/4 v0, 0x0

    const/16 v1, 0xa

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_szSaveDirPath:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->checkTrainingData(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public clearSignatureScreen()Z
    .locals 2

    const/4 v0, 0x0

    iput-short v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_sCurrentPointCount:S

    const-string/jumbo v0, "Signature"

    const-string/jumbo v1, "Retry"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    return v0
.end method

.method public deleteRegistration(I)Z
    .locals 2

    const/4 v0, 0x0

    const/16 v1, 0xa

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_szSaveDirPath:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->deleteFolder(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public getRegisteredDataCount(I)I
    .locals 6

    const/4 v0, 0x0

    const/16 v1, 0xa

    if-ne p1, v1, :cond_2

    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_szSaveDirPath:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, "SPenHSVreg00.dat"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_szSaveDirPath:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v4, "SPenHSVreg01.dat"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_szSaveDirPath:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v5, "SPenHSVreg02.dat"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    add-int/lit8 v0, v0, 0x1

    :cond_1
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    add-int/lit8 v0, v0, 0x1

    :cond_2
    return v0
.end method

.method public initHSV(Ljava/lang/String;IIII)Z
    .locals 3

    const/4 v0, 0x0

    const-string/jumbo v1, "Signature"

    const-string/jumbo v2, "InitHSV"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_0

    const-string/jumbo v1, "Signature"

    const-string/jumbo v2, "save directory path is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_szSaveDirPath:Ljava/lang/String;

    iput p2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nMaxPointCount:I

    iput p3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nMaxNumRegistration:I

    const/16 v1, 0x1f4

    iput v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nGENUINE_SCORE_THR:I

    iput p4, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nUserID:I

    iput p5, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nMinXYSize:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_szSaveDirPath:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->makeFolder(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v1, "Signature"

    const-string/jumbo v2, "make folder success"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    new-instance v1, Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;

    invoke-direct {v1}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_HSVJniLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_HSVJniLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_szSaveDirPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;->jni_HSVInit(Ljava/lang/String;)S
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    iget v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nMaxPointCount:I

    mul-int/lit8 v1, v1, 0x2

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalPosition:[I

    iget v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nMaxPointCount:I

    new-array v1, v1, [I

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalTimeStamp:[I

    iget v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nMaxPointCount:I

    new-array v1, v1, [C

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_cTotalTag:[C

    iget v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nMaxPointCount:I

    new-array v1, v1, [F

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalPressure:[F

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_HSVJniLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;

    iget v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nUserID:I

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;->jni_HSVDelUser(I)S

    move-result v1

    if-gez v1, :cond_2

    const-string/jumbo v1, "Signature"

    const-string/jumbo v2, "delete user failure"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const-string/jumbo v1, "Signature"

    const-string/jumbo v2, "make folder failure"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_HSVJniLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;

    const-string/jumbo v1, "Signature"

    const-string/jumbo v2, "create HSVJniLib failure"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public setDrawData([Landroid/graphics/PointF;[I[F)V
    .locals 6

    const/4 v1, 0x0

    const/high16 v5, 0x3f000000    # 0.5f

    iget-short v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_sCurrentPointCount:S

    array-length v2, p2

    add-int/2addr v0, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nMaxPointCount:I

    if-lt v0, v2, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nMaxPointCount:I

    int-to-short v0, v0

    iput-short v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_sCurrentPointCount:S

    :goto_0
    return-void

    :cond_0
    move v0, v1

    :goto_1
    array-length v2, p2

    if-lt v0, v2, :cond_1

    iget-short v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_sCurrentPointCount:S

    array-length v1, p2

    add-int/2addr v0, v1

    int-to-short v0, v0

    iput-short v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_sCurrentPointCount:S

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalPosition:[I

    iget-short v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_sCurrentPointCount:S

    mul-int/lit8 v3, v3, 0x2

    mul-int/lit8 v4, v0, 0x2

    add-int/2addr v3, v4

    aget-object v4, p1, v0

    iget v4, v4, Landroid/graphics/PointF;->x:F

    add-float/2addr v4, v5

    float-to-int v4, v4

    aput v4, v2, v3

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalPosition:[I

    iget-short v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_sCurrentPointCount:S

    mul-int/lit8 v3, v3, 0x2

    mul-int/lit8 v4, v0, 0x2

    add-int/2addr v3, v4

    add-int/lit8 v3, v3, 0x1

    aget-object v4, p1, v0

    iget v4, v4, Landroid/graphics/PointF;->y:F

    add-float/2addr v4, v5

    float-to-int v4, v4

    aput v4, v2, v3

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalTimeStamp:[I

    iget-short v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_sCurrentPointCount:S

    add-int/2addr v3, v0

    aget v4, p2, v0

    aput v4, v2, v3

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalPressure:[F

    iget-short v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_sCurrentPointCount:S

    add-int/2addr v3, v0

    aget v4, p3, v0

    aput v4, v2, v3

    if-nez v0, :cond_2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_cTotalTag:[C

    iget-short v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_sCurrentPointCount:S

    add-int/2addr v3, v0

    aput-char v1, v2, v3

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_cTotalTag:[C

    iget-short v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_sCurrentPointCount:S

    add-int/2addr v3, v0

    const/4 v4, 0x1

    aput-char v4, v2, v3

    goto :goto_2
.end method

.method public setGenuineScoreThr(I)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nGENUINE_SCORE_THR:I

    return-void
.end method

.method public setMaxNumRegistration(I)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nMaxNumRegistration:I

    return-void
.end method

.method public setMinXYSize(I)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nMinXYSize:I

    return-void
.end method

.method public setUserID(I)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nUserID:I

    return-void
.end method

.method public signatureTraining(I)I
    .locals 11

    const/4 v9, -0x1

    const/4 v10, 0x0

    iget v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nNumRegistration:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_HSVJniLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;

    iget v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nUserID:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;->jni_HSVDelUser(I)S

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_HSVJniLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalPosition:[I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalTimeStamp:[I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_cTotalTag:[C

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalPressure:[F

    iget-short v5, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_sCurrentPointCount:S

    iget v6, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nNumRegistration:I

    iget v7, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nUserID:I

    iget v8, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nMinXYSize:I

    invoke-virtual/range {v0 .. v8}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;->jni_HSVCheckSignature([I[I[C[FSIII)S

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->clearSignatureScreen()Z

    move v0, v9

    :goto_0
    return v0

    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nNumRegistration:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nMaxNumRegistration:I

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_HSVJniLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalPosition:[I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalTimeStamp:[I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_cTotalTag:[C

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalPressure:[F

    iget-short v5, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_sCurrentPointCount:S

    iget v6, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nNumRegistration:I

    iget v7, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nUserID:I

    invoke-virtual/range {v0 .. v7}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;->jni_HSVAddSignatureModel([I[I[C[FSII)S

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_bRegistered:Z

    iput v10, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nNumRegistration:I

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->clearSignatureScreen()Z

    const-string/jumbo v0, "Signature"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "m_bRegistered = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_bRegistered:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v0, "Signature"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "m_nNumRegistration = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nNumRegistration:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nMaxNumRegistration:I

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nNumRegistration:I

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_szSaveDirPath:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->deleteFolder(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v9

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_szSaveDirPath:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->makeFolder(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string/jumbo v0, "Signature"

    const-string/jumbo v1, "make folder success"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_HSVJniLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalPosition:[I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalTimeStamp:[I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_cTotalTag:[C

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalPressure:[F

    iget-short v5, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_sCurrentPointCount:S

    iget v6, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nNumRegistration:I

    iget v7, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nUserID:I

    invoke-virtual/range {v0 .. v7}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;->jni_HSVAddSignatureModel([I[I[C[FSII)S

    iput-boolean v10, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_bRegistered:Z

    iget v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nNumRegistration:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nNumRegistration:I

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->clearSignatureScreen()Z

    const-string/jumbo v0, "Signature"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "m_bRegistered = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_bRegistered:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v0, "Signature"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "m_nNumRegistration = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nNumRegistration:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nNumRegistration:I

    goto/16 :goto_0

    :cond_4
    const-string/jumbo v0, "Signature"

    const-string/jumbo v1, "make folder failure or existed"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_HSVJniLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalPosition:[I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalTimeStamp:[I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_cTotalTag:[C

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalPressure:[F

    iget-short v5, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_sCurrentPointCount:S

    iget v6, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nNumRegistration:I

    iget v7, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nUserID:I

    invoke-virtual/range {v0 .. v7}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;->jni_HSVAddSignatureModel([I[I[C[FSII)S

    iput-boolean v10, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_bRegistered:Z

    iget v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nNumRegistration:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nNumRegistration:I

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->clearSignatureScreen()Z

    const-string/jumbo v0, "Signature"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "m_bRegistered = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_bRegistered:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v0, "Signature"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "m_nNumRegistration = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nNumRegistration:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nNumRegistration:I

    goto/16 :goto_0
.end method

.method public verification(II)Z
    .locals 10

    const/16 v8, 0x1f4

    const/4 v9, 0x0

    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->checkRegistration(I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "Signature"

    const-string/jumbo v1, "StartVerification"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_HSVJniLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;

    iget v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nUserID:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;->jni_HSVDelUser(I)S

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_HSVJniLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;->jni_HSVLoadSignatureModel()S

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_HSVJniLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalPosition:[I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalTimeStamp:[I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_cTotalTag:[C

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nTotalPressure:[F

    iget-short v5, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_sCurrentPointCount:S

    iget v6, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nUserID:I

    iget v7, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_nGENUINE_SCORE_THR:I

    invoke-virtual/range {v0 .. v7}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVJniLib;->jni_HSVVerify([I[I[C[FSII)S

    move-result v0

    iput-short v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_sVerificationScore:S

    packed-switch p2, :pswitch_data_0

    move v0, v8

    :goto_0
    iget-short v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->m_sVerificationScore:S

    if-lt v1, v0, :cond_0

    const-string/jumbo v0, "Signature"

    const-string/jumbo v1, "Verification : Success"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->cancel(I)Z

    const/4 v0, 0x1

    :goto_1
    return v0

    :pswitch_0
    const/16 v0, 0x190

    goto :goto_0

    :pswitch_1
    move v0, v8

    goto :goto_0

    :pswitch_2
    const/16 v0, 0x258

    goto :goto_0

    :cond_0
    const-string/jumbo v0, "Signature"

    const-string/jumbo v1, "Verification : Failure"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->cancel(I)Z

    move v0, v9

    goto :goto_1

    :cond_1
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->cancel(I)Z

    move v0, v9

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
