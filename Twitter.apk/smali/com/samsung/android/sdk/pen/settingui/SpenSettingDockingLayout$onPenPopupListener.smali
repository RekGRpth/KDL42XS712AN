.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$onPenPopupListener;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$PopupListener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$onPenPopupListener;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$onPenPopupListener;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$onPenPopupListener;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)V

    return-void
.end method


# virtual methods
.method public onPopup(Z)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$onPenPopupListener;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setPopup(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$onPenPopupListener;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setPopup(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$onPenPopupListener;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->setPopup(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$onPenPopupListener;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setPopup(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$onPenPopupListener;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$onPenPopupListener;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;->onPopup(I)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$onPenPopupListener;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->setTopBottomToZeroMarginItem()V
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->access$5(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$onPenPopupListener;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setPopup(Z)V

    goto :goto_0
.end method
