.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$10;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->getColorPickerSettingVisible()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->setPreviewTextColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->invalidate()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->invalidate()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    iget v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPreCanvasPenAction:I

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setToolTypeAction(II)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    iget v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mPreCanvasFingerAction:I

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setToolTypeAction(II)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getTextSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setTextSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->hide()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$10;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->setVisibility(I)V

    :cond_1
    return-void
.end method
