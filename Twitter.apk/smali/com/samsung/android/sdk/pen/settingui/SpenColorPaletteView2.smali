.class Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;
.super Landroid/view/View;
.source "Twttr"


# static fields
.field private static final COLOR_COLUMN_NUM:I = 0x7

.field private static final COLOR_NUM_MAX:I = 0xe

.field private static final COLOR_ROW_NUM:I = 0x2

.field private static final CUSTOM_COLOR_IDX:I = 0xc

.field protected static final DEFAULT_COLOR:I = -0x1000000

.field private static ITEM_BORDER_WIDTH:I = 0x0

.field private static ITEM_GAPX:I = 0x0

.field private static ITEM_GAPY:I = 0x0

.field private static final SPOID_ICON:I = -0x1100001

.field private static final USER_COLOR:I = -0x1000002

.field private static final WINDOW_BORDER_WIDTH:I


# instance fields
.field private currentTableIndex:I

.field isRainbow:Z

.field private mBorderPaint:Landroid/graphics/Paint;

.field private mCanvasRect:Landroid/graphics/Rect;

.field private mColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2$OnColorChangedListener;

.field private final mColorContentDescritionTable1:[Ljava/lang/String;

.field private final mColorContentDescritionTable2:[Ljava/lang/String;

.field private final mColorContentDescritionTable3:[Ljava/lang/String;

.field mColorHoverIdx:I

.field private mColorPaint:Landroid/graphics/Paint;

.field private mColorTable:[I

.field private final mColorTable1:[I

.field private final mColorTable2:[I

.field private final mColorTable3:[I

.field private mCustom_imagepath:Ljava/lang/String;

.field private mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

.field private mDrSeleteBox:Landroid/graphics/drawable/BitmapDrawable;

.field private mDrSpoid:Landroid/graphics/drawable/BitmapDrawable;

.field private mDrUserColor:Landroid/graphics/drawable/BitmapDrawable;

.field mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

.field private mFirstExecuted:Z

.field private mItemRect:Landroid/graphics/Rect;

.field mPreColorHoverIdx:I

.field private mSelectRect:Landroid/graphics/Rect;

.field private mSeletedItem:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->ITEM_BORDER_WIDTH:I

    const/4 v0, 0x5

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->ITEM_GAPX:I

    const/4 v0, 0x7

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->ITEM_GAPY:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;F)V
    .locals 10

    const/16 v9, 0xa

    const/4 v8, 0x4

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/16 v5, 0xff

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->isRainbow:Z

    iput v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->currentTableIndex:I

    const/16 v0, 0xe

    new-array v0, v0, [I

    invoke-static {v5, v5, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v6

    const/16 v1, 0xfd

    const/16 v2, 0x2d

    invoke-static {v1, v5, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v7

    const/4 v1, 0x2

    const/16 v2, 0x83

    const/16 v3, 0x5d

    invoke-static {v5, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x3

    const/16 v2, 0x3b

    const/16 v3, 0x5b

    invoke-static {v5, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x49

    const/16 v2, 0xc9

    invoke-static {v5, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v8

    const/4 v1, 0x5

    const/16 v2, 0xca

    const/16 v3, 0x85

    invoke-static {v2, v3, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0x7a

    const/16 v3, 0x37

    const/16 v4, 0xde

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x7

    const/16 v2, 0x94

    const/16 v3, 0x2e

    invoke-static {v7, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x8

    const/16 v2, 0x38

    const/16 v3, 0xa8

    invoke-static {v2, v3, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x9

    const/16 v2, 0x33

    const/16 v3, 0x67

    const/16 v4, 0xfd

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xa6

    const/16 v2, 0xa5

    const/16 v3, 0xa5

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v9

    const/16 v1, 0xb

    invoke-static {v6, v6, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xc

    const v2, -0x1000002

    aput v2, v0, v1

    const/16 v1, 0xd

    const v2, -0x1100001

    aput v2, v0, v1

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable:[I

    const/16 v0, 0xe

    new-array v0, v0, [I

    invoke-static {v5, v5, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v6

    const/16 v1, 0xfd

    const/16 v2, 0x2d

    invoke-static {v1, v5, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v7

    const/4 v1, 0x2

    const/16 v2, 0x83

    const/16 v3, 0x5d

    invoke-static {v5, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x3

    const/16 v2, 0x3b

    const/16 v3, 0x5b

    invoke-static {v5, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x49

    const/16 v2, 0xc9

    invoke-static {v5, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v8

    const/4 v1, 0x5

    const/16 v2, 0xca

    const/16 v3, 0x85

    invoke-static {v2, v3, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0x7a

    const/16 v3, 0x37

    const/16 v4, 0xde

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x7

    const/16 v2, 0x94

    const/16 v3, 0x2e

    invoke-static {v7, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x8

    const/16 v2, 0x38

    const/16 v3, 0xa8

    invoke-static {v2, v3, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x9

    const/16 v2, 0x33

    const/16 v3, 0x67

    const/16 v4, 0xfd

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xa6

    const/16 v2, 0xa5

    const/16 v3, 0xa5

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v9

    const/16 v1, 0xb

    invoke-static {v6, v6, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xc

    const v2, -0x1000002

    aput v2, v0, v1

    const/16 v1, 0xd

    const v2, -0x1100001

    aput v2, v0, v1

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable1:[I

    const/16 v0, 0xe

    new-array v0, v0, [I

    const/16 v1, 0xcd

    const/16 v2, 0xc1

    invoke-static {v5, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v6

    const/16 v1, 0xf9

    const/16 v2, 0xa4

    const/16 v3, 0x8f

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v7

    const/4 v1, 0x2

    const/16 v2, 0xf5

    const/16 v3, 0x7e

    const/16 v4, 0x60

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x3

    const/16 v2, 0x95

    const/16 v3, 0xa5

    invoke-static {v5, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xf5

    const/16 v2, 0x4d

    const/16 v3, 0x67

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v8

    const/4 v1, 0x5

    const/16 v2, 0xed

    const/16 v3, 0x1a

    const/16 v4, 0x3b

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0xbc

    const/16 v3, 0x21

    invoke-static {v2, v8, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x7

    const/16 v2, 0x8b

    const/16 v3, 0x7c

    invoke-static {v2, v6, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x8

    const/16 v2, 0xbc

    const/16 v3, 0x5b

    invoke-static {v2, v8, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x9

    const/16 v2, 0x8c

    const/16 v3, 0x53

    const/16 v4, 0x7a

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x59

    const/16 v2, 0x42

    const/16 v3, 0x54

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v9

    const/16 v1, 0xb

    const/16 v2, 0x66

    const/16 v3, 0x30

    invoke-static {v2, v3, v9}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xc

    const v2, -0x1000002

    aput v2, v0, v1

    const/16 v1, 0xd

    const v2, -0x1100001

    aput v2, v0, v1

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable2:[I

    const/16 v0, 0xe

    new-array v0, v0, [I

    const/16 v1, 0xfc

    const/16 v2, 0xe3

    const/16 v3, 0x3c

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v6

    const/16 v1, 0xfa

    const/16 v2, 0xc7

    const/16 v3, 0x4b

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v7

    const/4 v1, 0x2

    const/16 v2, 0xcb

    const/16 v3, 0xe3

    const/16 v4, 0x6c

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x3

    const/16 v2, 0x5b

    const/16 v3, 0xdd

    const/16 v4, 0x38

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x41

    const/16 v2, 0xa5

    const/16 v3, 0x26

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v8

    const/4 v1, 0x5

    const/16 v2, 0xe

    const/16 v3, 0x80

    const/16 v4, 0x19

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0x66

    const/16 v3, 0x7f

    const/16 v4, 0x2a

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x7

    const/16 v2, 0xe

    const/16 v3, 0x80

    const/16 v4, 0x64

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x8

    const/16 v2, 0x13

    const/16 v3, 0x9b

    const/16 v4, 0x69

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x9

    const/4 v2, 0x5

    const/16 v3, 0x59

    const/16 v4, 0x75

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0x11

    const/16 v2, 0x4b

    const/16 v3, 0x9d

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    aput v1, v0, v9

    const/16 v1, 0xb

    const/16 v2, 0x29

    const/16 v3, 0x6c

    invoke-static {v9, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    aput v2, v0, v1

    const/16 v1, 0xc

    const v2, -0x1000002

    aput v2, v0, v1

    const/16 v1, 0xd

    const v2, -0x1100001

    aput v2, v0, v1

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable3:[I

    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "White, Tap to apply"

    aput-object v1, v0, v6

    const-string/jumbo v1, "Yellow, Tap to apply"

    aput-object v1, v0, v7

    const/4 v1, 0x2

    const-string/jumbo v2, "Coral, Tap to apply"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "Tomato, Tap to apply"

    aput-object v2, v0, v1

    const-string/jumbo v1, "Hotpink, Tap to apply"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    const-string/jumbo v2, "Plum, Tap to apply"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "Blueviolet, Tap to apply"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "Forestgreen, Tap to apply"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "Dodgeblue, Tap to apply"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "Royalblue, Tap to apply"

    aput-object v2, v0, v1

    const-string/jumbo v1, "Darkgray, Tap to apply"

    aput-object v1, v0, v9

    const/16 v1, 0xb

    const-string/jumbo v2, "Black, Tap to apply"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "Custom colour palette, Tap to apply"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "Spuit, Button"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorContentDescritionTable1:[Ljava/lang/String;

    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "Peachpuff, Tap to apply"

    aput-object v1, v0, v6

    const-string/jumbo v1, "Lightsalmon, Tap to apply"

    aput-object v1, v0, v7

    const/4 v1, 0x2

    const-string/jumbo v2, "Darksalmon, Tap to apply"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "linghtpink, Tap to apply"

    aput-object v2, v0, v1

    const-string/jumbo v1, "Palevioletred, Tap to apply"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    const-string/jumbo v2, "Crimson, Tap to apply"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "Red, Tap to apply"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "Mediumvioletred, Tap to apply"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "Purple, Tap to apply"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "MediumOrchid, Tap to apply"

    aput-object v2, v0, v1

    const-string/jumbo v1, "Dimgray, Tap to apply"

    aput-object v1, v0, v9

    const/16 v1, 0xb

    const-string/jumbo v2, "Saddlebrown, Tap to apply"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "Custom colour palette, Tap to apply"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "Spuit, Button"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorContentDescritionTable2:[Ljava/lang/String;

    const/16 v0, 0xe

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "Gold, Tap to apply"

    aput-object v1, v0, v6

    const-string/jumbo v1, "Orange, Tap to apply"

    aput-object v1, v0, v7

    const/4 v1, 0x2

    const-string/jumbo v2, "Greenyellow, Tap to apply"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "Limegreen, Tap to apply"

    aput-object v2, v0, v1

    const-string/jumbo v1, "Seegreen, Tap to apply"

    aput-object v1, v0, v8

    const/4 v1, 0x5

    const-string/jumbo v2, "Green, Tap to apply"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "OliverDrab, Tap to apply"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "DarkCyan, Tap to apply"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "Teal, Tap to apply"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "Steelblue, Tap to apply"

    aput-object v2, v0, v1

    const-string/jumbo v1, "Blue, Tap to apply"

    aput-object v1, v0, v9

    const/16 v1, 0xb

    const-string/jumbo v2, "Darkblue, Tap to apply"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "Custom colour palette, Tap to apply"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "Spuit, Button"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorContentDescritionTable3:[Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2$OnColorChangedListener;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mCustom_imagepath:Ljava/lang/String;

    iput-boolean v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mFirstExecuted:Z

    iput-object p2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mCustom_imagepath:Ljava/lang/String;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mCustom_imagepath:Ljava/lang/String;

    invoke-direct {v0, p1, v1, p3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->initView()V

    return-void
.end method

.method private drawSeleteBox(Landroid/graphics/Canvas;)V
    .locals 5

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    rem-int/lit8 v0, v0, 0x7

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    div-int/lit8 v1, v1, 0x7

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSelectRect:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mItemRect:Landroid/graphics/Rect;

    invoke-virtual {v2, v3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSelectRect:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSelectRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    sget v4, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->ITEM_GAPX:I

    add-int/2addr v3, v4

    mul-int/2addr v0, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSelectRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    sget v4, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->ITEM_GAPY:I

    add-int/2addr v3, v4

    mul-int/2addr v1, v3

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Rect;->offset(II)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSelectRect:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v1, v1, 0x2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrSeleteBox:Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSelectRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrSeleteBox:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    return-void
.end method

.method private getColorIdx(FF)I
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x7

    const/4 v0, 0x2

    move v3, v1

    :goto_0
    if-le v3, v2, :cond_2

    :cond_0
    :goto_1
    if-le v1, v0, :cond_3

    :cond_1
    if-le v3, v2, :cond_5

    :goto_2
    if-le v1, v0, :cond_4

    :goto_3
    add-int/lit8 v0, v0, -0x1

    mul-int/lit8 v0, v0, 0x7

    add-int/lit8 v1, v2, -0x1

    add-int/2addr v0, v1

    return v0

    :cond_2
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mItemRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    mul-int/2addr v4, v3

    add-int/lit8 v5, v3, -0x1

    sget v6, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->ITEM_GAPX:I

    mul-int/2addr v5, v6

    add-int/2addr v4, v5

    int-to-float v4, v4

    cmpg-float v4, p1, v4

    if-lez v4, :cond_0

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_3
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mItemRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    mul-int/2addr v4, v1

    int-to-float v4, v4

    cmpg-float v4, p2, v4

    if-ltz v4, :cond_1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_3

    :cond_5
    move v2, v3

    goto :goto_2
.end method

.method private initView()V
    .locals 4

    const/high16 v1, 0x3f800000    # 1.0f

    const/16 v3, 0x26

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->ITEM_GAPX:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0x5

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->ITEM_GAPY:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    mul-int/lit8 v0, v0, 0x1

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->ITEM_BORDER_WIDTH:I

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mBorderPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mBorderPaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mBorderPaint:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mBorderPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "snote_colorchip_select_box"

    const/16 v2, 0x27

    invoke-virtual {v0, v1, v3, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrSeleteBox:Landroid/graphics/drawable/BitmapDrawable;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "snote_colorchip_shadow"

    invoke-virtual {v0, v1, v3, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "snote_colorbox_mini"

    invoke-virtual {v0, v1, v3, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrUserColor:Landroid/graphics/drawable/BitmapDrawable;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "snote_color_spoid_normal"

    invoke-virtual {v0, v1, v3, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrSpoid:Landroid/graphics/drawable/BitmapDrawable;

    return-void
.end method

.method private onDrawColorSet(Landroid/graphics/Canvas;)V
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mCanvasRect:Landroid/graphics/Rect;

    if-eqz v0, :cond_0

    new-instance v3, Landroid/graphics/Rect;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mItemRect:Landroid/graphics/Rect;

    invoke-direct {v3, v0}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    move v2, v1

    :goto_0
    const/4 v0, 0x2

    if-lt v2, v0, :cond_1

    :cond_0
    return-void

    :cond_1
    move v0, v1

    :goto_1
    const/4 v4, 0x7

    if-lt v0, v4, :cond_2

    iget v0, v3, Landroid/graphics/Rect;->left:I

    neg-int v0, v0

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v4

    sget v5, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->ITEM_GAPY:I

    add-int/2addr v4, v5

    invoke-virtual {v3, v0, v4}, Landroid/graphics/Rect;->offset(II)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable:[I

    mul-int/lit8 v5, v2, 0x7

    add-int/2addr v5, v0

    aget v4, v4, v5

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorPaint:Landroid/graphics/Paint;

    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setColor(I)V

    const v5, -0x1000002

    if-ne v4, v5, :cond_4

    iget-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->isRainbow:Z

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrUserColor:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v4, v3}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrUserColor:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v4, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    :goto_2
    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v4

    sget v5, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->ITEM_GAPX:I

    add-int/2addr v4, v5

    invoke-virtual {v3, v4, v1}, Landroid/graphics/Rect;->offset(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v4, v3}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v4, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_2

    :cond_4
    const v5, -0x1100001

    if-ne v4, v5, :cond_5

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrSpoid:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v4, v3}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrSpoid:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v4, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_2

    :cond_5
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v4, v3}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v4, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_2
.end method


# virtual methods
.method public close()V
    .locals 2

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mCanvasRect:Landroid/graphics/Rect;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mItemRect:Landroid/graphics/Rect;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSelectRect:Landroid/graphics/Rect;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mBorderPaint:Landroid/graphics/Paint;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorPaint:Landroid/graphics/Paint;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2$OnColorChangedListener;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrSeleteBox:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrSeleteBox:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_0
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrSeleteBox:Landroid/graphics/drawable/BitmapDrawable;

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_2
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrUserColor:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrUserColor:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_4
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrUserColor:Landroid/graphics/drawable/BitmapDrawable;

    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrSpoid:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrSpoid:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_6
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrSpoid:Landroid/graphics/drawable/BitmapDrawable;

    :cond_7
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mCustom_imagepath:Ljava/lang/String;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    return-void
.end method

.method public getColor()I
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable:[I

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    aget v0, v0, v1

    return v0
.end method

.method protected getCurrentTableIndex()I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->currentTableIndex:I

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getPaddingLeft()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getPaddingTop()I

    move-result v1

    add-int/lit8 v1, v1, 0x0

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mFirstExecuted:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getWidth()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    div-int/lit8 v2, v0, 0x7

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable:[I

    array-length v0, v0

    int-to-float v0, v0

    const/high16 v3, 0x40e00000    # 7.0f

    div-float v3, v0, v3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable:[I

    array-length v0, v0

    div-int/lit8 v0, v0, 0x7

    int-to-float v4, v0

    cmpl-float v3, v3, v4

    if-lez v3, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    mul-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getPaddingBottom()I

    move-result v2

    add-int/2addr v0, v2

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mFirstExecuted:Z

    :cond_1
    :try_start_0
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->onDrawColorSet(Landroid/graphics/Canvas;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->drawSeleteBox(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    const/4 v0, 0x1

    invoke-super {p0, p1}, Landroid/view/View;->onHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :goto_0
    :pswitch_0
    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mPreColorHoverIdx:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorHoverIdx:I

    if-eq v2, v3, :cond_1

    const v2, 0x8000

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->sendAccessibilityEvent(I)V

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->currentTableIndex:I

    if-ne v2, v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorContentDescritionTable1:[Ljava/lang/String;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorHoverIdx:I

    aget-object v0, v0, v2

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_1
    const/high16 v0, 0x10000

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->sendAccessibilityEvent(I)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorHoverIdx:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mPreColorHoverIdx:I

    :cond_1
    move v0, v1

    :goto_2
    return v0

    :pswitch_1
    invoke-direct {p0, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getColorIdx(FF)I

    move-result v2

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorHoverIdx:I

    goto :goto_0

    :pswitch_2
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorHoverIdx:I

    const/4 v1, -0x1

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mPreColorHoverIdx:I

    goto :goto_2

    :pswitch_3
    invoke-direct {p0, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getColorIdx(FF)I

    move-result v2

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorHoverIdx:I

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->currentTableIndex:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorContentDescritionTable2:[Ljava/lang/String;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorHoverIdx:I

    aget-object v0, v0, v2

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_3
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->currentTableIndex:I

    const/4 v2, 0x3

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorContentDescritionTable3:[Ljava/lang/String;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorHoverIdx:I

    aget-object v0, v0, v2

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4

    const/16 v2, 0xe

    const/4 v0, 0x1

    sparse-switch p1, :sswitch_data_0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/View;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :cond_1
    :goto_0
    return v0

    :sswitch_0
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    add-int/lit8 v1, v1, -0x7

    if-ltz v1, :cond_0

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    add-int/lit8 v1, v1, -0x7

    if-ge v1, v2, :cond_0

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    add-int/lit8 v1, v1, -0x7

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->invalidate()V

    goto :goto_0

    :sswitch_1
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    add-int/lit8 v1, v1, 0x7

    if-ltz v1, :cond_0

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    add-int/lit8 v1, v1, 0x7

    if-ge v1, v2, :cond_0

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    add-int/lit8 v1, v1, 0x7

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->invalidate()V

    goto :goto_0

    :sswitch_2
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    add-int/lit8 v1, v1, -0x1

    if-ltz v1, :cond_0

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    add-int/lit8 v1, v1, -0x1

    if-ge v1, v2, :cond_0

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->invalidate()V

    goto :goto_0

    :sswitch_3
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    add-int/lit8 v1, v1, 0x1

    if-ltz v1, :cond_0

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    add-int/lit8 v1, v1, 0x1

    if-ge v1, v2, :cond_0

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->invalidate()V

    goto :goto_0

    :sswitch_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2$OnColorChangedListener;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2$OnColorChangedListener;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable:[I

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    aget v2, v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    invoke-interface {v1, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2$OnColorChangedListener;->colorChanged(II)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_1
        0x15 -> :sswitch_2
        0x16 -> :sswitch_3
        0x42 -> :sswitch_4
    .end sparse-switch
.end method

.method protected onSizeChanged(IIII)V
    .locals 7

    const/4 v6, 0x0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getPaddingRight()I

    move-result v1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getPaddingBottom()I

    move-result v3

    new-instance v4, Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getLeft()I

    move-result v5

    add-int/2addr v0, v5

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getTop()I

    move-result v5

    add-int/2addr v2, v5

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getRight()I

    move-result v5

    sub-int v1, v5, v1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getBottom()I

    move-result v5

    sub-int v3, v5, v3

    invoke-direct {v4, v0, v2, v1, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mCanvasRect:Landroid/graphics/Rect;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mCanvasRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    sget v1, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->ITEM_GAPX:I

    mul-int/lit8 v1, v1, 0x6

    sub-int/2addr v0, v1

    div-int/lit8 v1, v0, 0x7

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2, v6, v6, v1, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mItemRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mItemRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sget v2, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->ITEM_BORDER_WIDTH:I

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mItemRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sget v3, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->ITEM_BORDER_WIDTH:I

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mItemRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    sget v4, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->ITEM_BORDER_WIDTH:I

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mItemRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    sget v5, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->ITEM_BORDER_WIDTH:I

    add-int/2addr v4, v5

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSelectRect:Landroid/graphics/Rect;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mItemRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    return-void

    :cond_0
    add-int/lit8 v0, v1, -0x1

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    invoke-interface {v1, v4}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    const/16 v1, 0xc

    if-ne v0, v1, :cond_3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_1

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->isRainbow:Z

    if-eqz v1, :cond_1

    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->isRainbow:Z

    :cond_1
    :goto_1
    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorHoverIdx:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2$OnColorChangedListener;

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    :cond_2
    :goto_2
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->invalidate()V

    return v4

    :pswitch_0
    invoke-virtual {p0, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->playSoundEffect(I)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getColorIdx(FF)I

    move-result v0

    goto :goto_0

    :cond_3
    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2$OnColorChangedListener;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable:[I

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    aget v1, v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2$OnColorChangedListener;->colorChanged(II)V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch
.end method

.method protected setBackColorTable(I)V
    .locals 1

    add-int/lit8 v0, p1, -0x1

    if-nez v0, :cond_0

    const/4 v0, 0x3

    :cond_0
    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->currentTableIndex:I

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->currentTableIndex:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->invalidate()V

    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable1:[I

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable:[I

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable2:[I

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable:[I

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable3:[I

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable:[I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setColor(I)V
    .locals 4

    const/high16 v0, -0x1000000

    const/16 v3, 0xc

    or-int v1, p1, v0

    and-int/2addr v0, v1

    const/high16 v2, -0x2000000

    if-eq v0, v2, :cond_6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-gez v0, :cond_1

    :goto_1
    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x2

    :goto_2
    if-gez v0, :cond_3

    :goto_3
    if-gez v0, :cond_0

    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable:[I

    aput v1, v0, v3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable1:[I

    aput v1, v0, v3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable2:[I

    aput v1, v0, v3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable3:[I

    aput v1, v0, v3

    :cond_0
    :goto_4
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->invalidate()V

    return-void

    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable:[I

    aget v2, v2, v0

    if-ne v1, v2, :cond_2

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable1:[I

    aget v2, v2, v0

    if-eq v1, v2, :cond_4

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable2:[I

    aget v2, v2, v0

    if-eq v1, v2, :cond_4

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable3:[I

    aget v2, v2, v0

    if-ne v1, v2, :cond_5

    :cond_4
    const/4 v2, -0x1

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    goto :goto_3

    :cond_5
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    :cond_6
    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable:[I

    aput v1, v0, v3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable1:[I

    aput v1, v0, v3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable2:[I

    aput v1, v0, v3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable3:[I

    aput v1, v0, v3

    goto :goto_4
.end method

.method public setColorPickerColor(I)V
    .locals 3

    const/16 v2, 0xc

    const/high16 v1, -0x1000000

    and-int v0, p1, v1

    if-nez v0, :cond_0

    or-int/2addr p1, v1

    :cond_0
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable:[I

    aput p1, v0, v2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->invalidate()V

    return-void
.end method

.method public setColorPickerMode()V
    .locals 1

    const/16 v0, 0xc

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    return-void
.end method

.method public setInitialValue(Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2$OnColorChangedListener;I)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2$OnColorChangedListener;

    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->setSelectBoxPos(I)V

    return-void
.end method

.method protected setNextColorTable(I)V
    .locals 4

    add-int/lit8 v0, p1, 0x1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->currentTableIndex:I

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->currentTableIndex:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->invalidate()V

    const/16 v0, 0x105

    const/16 v1, 0x53

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getWidth()I

    move-result v2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getHeight()I

    move-result v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->onSizeChanged(IIII)V

    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable1:[I

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable:[I

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable2:[I

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable:[I

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable3:[I

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable:[I

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setSelectBoxPos(I)V
    .locals 3

    const/high16 v0, -0x1000000

    if-ne p1, v0, :cond_0

    move p1, v0

    :cond_0
    or-int v1, p1, v0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-gez v0, :cond_2

    :goto_1
    if-gez v0, :cond_1

    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->invalidate()V

    return-void

    :cond_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mColorTable:[I

    aget v2, v2, v0

    if-ne v1, v2, :cond_3

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->mSeletedItem:I

    goto :goto_1

    :cond_3
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method
