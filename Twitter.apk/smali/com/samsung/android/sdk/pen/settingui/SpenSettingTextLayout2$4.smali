.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    const/4 v7, 0x0

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget-object v2, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextFontSizeList:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42400000    # 48.0f

    invoke-virtual {v1, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x43340000    # 180.0f

    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget v5, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScale:F

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;-><init>(Landroid/view/View;Ljava/util/ArrayList;IIF)V

    invoke-static {v6, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;

    move-result-object v0

    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->setOnItemSelectListner(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2$SizeDropdownSelectListner;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeDropdown:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCurrentFontSize:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v7, v7, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2;->show(IILjava/lang/String;)V

    return-void
.end method
