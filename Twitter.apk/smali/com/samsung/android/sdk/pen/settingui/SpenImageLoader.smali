.class Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

.field public mLoaded:Z

.field private mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

.field private final mSdkVersion:I

.field private mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mSdkVersion:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mLoaded:Z

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    return-void
.end method


# virtual methods
.method public addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    const/4 v2, -0x1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mViewList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mNormalPathList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mPressPathList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mFocusPathList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mWidthList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mHeightList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$10(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mViewList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mNormalPathList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mPressPathList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mFocusPathList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mWidthList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mHeightList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$10(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V
    .locals 3

    const/4 v2, -0x1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mViewList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mImagePathList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mWidthList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mHeightList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;II)V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mViewList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mImagePathList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mWidthList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mHeightList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public close()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mViewList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$1(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mImagePathList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mWidthList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$5(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mHeightList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$7(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;Ljava/util/ArrayList;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mViewList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$1(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mNormalPathList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mPressPathList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$5(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mFocusPathList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$7(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mWidthList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$9(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mHeightList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$10(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$11(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;Ljava/util/ArrayList;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    return-void
.end method

.method public loadImage()V
    .locals 9

    const/16 v8, 0x10

    const/4 v4, 0x0

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mLoaded:Z

    if-eqz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mLoaded:Z

    move v3, v4

    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mViewList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v3, v0, :cond_2

    move v7, v4

    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mViewList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v7, v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mViewList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mWidthList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mHeightList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$10(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mSdkVersion:I

    if-ge v0, v8, :cond_7

    if-gez v4, :cond_6

    if-gez v5, :cond_6

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mNormalPathList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mPressPathList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mFocusPathList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v0, v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_2
    add-int/lit8 v4, v7, 0x1

    move v7, v4

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mViewList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mWidthList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mHeightList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mSdkVersion:I

    if-ge v5, v8, :cond_4

    if-gez v2, :cond_3

    if-gez v1, :cond_3

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mImagePathList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto/16 :goto_0

    :cond_3
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mImagePathList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mWidthList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mHeightList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v5, v1, v6, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_3

    :cond_4
    if-gez v2, :cond_5

    if-gez v1, :cond_5

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mImagePathList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_3

    :cond_5
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mImagePathList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mWidthList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mOneStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->mHeightList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$OneStateImage;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v5, v1, v6, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_3

    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mNormalPathList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mPressPathList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mFocusPathList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    :cond_7
    if-gez v4, :cond_8

    if-gez v5, :cond_8

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mNormalPathList:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mPressPathList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mFocusPathList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v3, v0, v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2

    :cond_8
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mNormalPathList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mPressPathList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mFocusPathList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mWidthList:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mStateImage:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mHeightList:Ljava/util/ArrayList;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->access$10(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_2
.end method
