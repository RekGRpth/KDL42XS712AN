.class Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field protected mContextResources:Landroid/content/res/Resources;

.field protected mCustom_imagepath:Ljava/lang/String;

.field protected mDrawableContext:Landroid/content/Context;

.field protected mOnePT:F

.field protected mSdkResources:Landroid/content/res/Resources;

.field protected mSdkVersion:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;F)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mOnePT:F

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mCustom_imagepath:Ljava/lang/String;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkResources:Landroid/content/res/Resources;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mContextResources:Landroid/content/res/Resources;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkVersion:I

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mDrawableContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mCustom_imagepath:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, p3

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mOnePT:F

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mDrawableContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string/jumbo v1, "com.samsung.android.sdk.spen30"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkResources:Landroid/content/res/Resources;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mDrawableContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mContextResources:Landroid/content/res/Resources;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method protected getIntValueAppliedDensity(F)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mOnePT:F

    mul-float/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public resizeImage(Landroid/content/res/Resources;III)Landroid/graphics/drawable/Drawable;
    .locals 7

    const/4 v1, 0x0

    invoke-virtual {p1, p2}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenScreenCodecDecoder;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v2, p3

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v5, p4

    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    int-to-float v2, v2

    int-to-float v6, v3

    div-float/2addr v2, v6

    int-to-float v5, v5

    int-to-float v6, v4

    div-float v6, v5, v6

    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {v5, v2, v6}, Landroid/graphics/Matrix;->postScale(FF)Z

    const/4 v6, 0x1

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v0, p1, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method protected setDrawableCheckedImg(Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    if-eqz p1, :cond_0

    new-array v1, v4, [I

    const v2, -0x10100a0

    aput v2, v1, v3

    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_0
    if-eqz p2, :cond_1

    new-array v1, v4, [I

    const v2, 0x10100a0    # android.R.attr.state_checked

    aput v2, v1, v3

    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_1
    return-object v0
.end method

.method protected setDrawableCheckedImg(Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    if-eqz p1, :cond_0

    new-array v1, v4, [I

    const v2, -0x10100a0

    aput v2, v1, v3

    invoke-virtual {p0, p1, p3, p4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_0
    if-eqz p2, :cond_1

    new-array v1, v4, [I

    const v2, 0x10100a0    # android.R.attr.state_checked

    aput v2, v1, v3

    invoke-virtual {p0, p2, p3, p4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_1
    return-object v0
.end method

.method protected setDrawableCheckedImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x3

    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    if-eqz p2, :cond_0

    new-array v1, v3, [I

    fill-array-data v1, :array_0

    invoke-virtual {p0, p2, p5, p6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_0
    if-eqz p1, :cond_1

    new-array v1, v3, [I

    fill-array-data v1, :array_1

    invoke-virtual {p0, p1, p5, p6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_1
    if-eqz p3, :cond_2

    new-array v1, v3, [I

    fill-array-data v1, :array_2

    invoke-virtual {p0, p3, p5, p6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_2
    if-eqz p4, :cond_3

    new-array v1, v3, [I

    fill-array-data v1, :array_3

    invoke-virtual {p0, p4, p5, p6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_3
    if-eqz p1, :cond_4

    new-array v1, v4, [I

    fill-array-data v1, :array_4

    invoke-virtual {p0, p1, p5, p6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_4
    if-eqz p2, :cond_5

    new-array v1, v4, [I

    fill-array-data v1, :array_5

    invoke-virtual {p0, p2, p5, p6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_5
    return-object v0

    :array_0
    .array-data 4
        0x10100a0    # android.R.attr.state_checked
        -0x101009c
        0x101009e    # android.R.attr.state_enabled
    .end array-data

    :array_1
    .array-data 4
        -0x10100a0
        -0x101009c
        0x101009e    # android.R.attr.state_enabled
    .end array-data

    :array_2
    .array-data 4
        0x10100a0    # android.R.attr.state_checked
        0x101009c    # android.R.attr.state_focused
        0x101009e    # android.R.attr.state_enabled
    .end array-data

    :array_3
    .array-data 4
        -0x10100a0
        0x101009c    # android.R.attr.state_focused
        0x101009e    # android.R.attr.state_enabled
    .end array-data

    :array_4
    .array-data 4
        -0x10100a0
        0x101009e    # android.R.attr.state_enabled
    .end array-data

    :array_5
    .array-data 4
        0x10100a0    # android.R.attr.state_checked
        0x101009e    # android.R.attr.state_enabled
    .end array-data
.end method

.method protected setDrawableCheckedImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;
    .locals 5

    const/4 v4, 0x4

    const/4 v3, 0x3

    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    if-eqz p2, :cond_0

    new-array v1, v4, [I

    fill-array-data v1, :array_0

    invoke-virtual {p0, p2, p7, p8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_0
    if-eqz p1, :cond_1

    new-array v1, v4, [I

    fill-array-data v1, :array_1

    invoke-virtual {p0, p1, p7, p8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_1
    if-eqz p3, :cond_2

    new-array v1, v4, [I

    fill-array-data v1, :array_2

    invoke-virtual {p0, p3, p7, p8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_2
    if-eqz p4, :cond_3

    new-array v1, v4, [I

    fill-array-data v1, :array_3

    invoke-virtual {p0, p4, p7, p8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_3
    if-eqz p5, :cond_4

    new-array v1, v3, [I

    fill-array-data v1, :array_4

    invoke-virtual {p0, p5, p7, p8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_4
    if-eqz p6, :cond_5

    new-array v1, v3, [I

    fill-array-data v1, :array_5

    invoke-virtual {p0, p6, p7, p8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_5
    if-eqz p1, :cond_6

    new-array v1, v3, [I

    fill-array-data v1, :array_6

    invoke-virtual {p0, p1, p7, p8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_6
    if-eqz p2, :cond_7

    new-array v1, v3, [I

    fill-array-data v1, :array_7

    invoke-virtual {p0, p2, p7, p8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_7
    return-object v0

    :array_0
    .array-data 4
        0x10100a0    # android.R.attr.state_checked
        -0x101009c
        -0x10100a7
        0x101009e    # android.R.attr.state_enabled
    .end array-data

    :array_1
    .array-data 4
        -0x10100a0
        -0x101009c
        -0x10100a7
        0x101009e    # android.R.attr.state_enabled
    .end array-data

    :array_2
    .array-data 4
        0x10100a0    # android.R.attr.state_checked
        0x101009c    # android.R.attr.state_focused
        -0x10100a7
        0x101009e    # android.R.attr.state_enabled
    .end array-data

    :array_3
    .array-data 4
        -0x10100a0
        0x101009c    # android.R.attr.state_focused
        -0x10100a7
        0x101009e    # android.R.attr.state_enabled
    .end array-data

    :array_4
    .array-data 4
        0x10100a0    # android.R.attr.state_checked
        0x10100a7    # android.R.attr.state_pressed
        0x101009e    # android.R.attr.state_enabled
    .end array-data

    :array_5
    .array-data 4
        -0x10100a0
        0x10100a7    # android.R.attr.state_pressed
        0x101009e    # android.R.attr.state_enabled
    .end array-data

    :array_6
    .array-data 4
        -0x10100a0
        -0x10100a7
        0x101009e    # android.R.attr.state_enabled
    .end array-data

    :array_7
    .array-data 4
        0x10100a0    # android.R.attr.state_checked
        -0x10100a7
        0x101009e    # android.R.attr.state_enabled
    .end array-data
.end method

.method protected setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    if-eqz p1, :cond_0

    const/4 v1, 0x4

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-virtual {p0, p1, p5, p6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_0
    if-eqz p2, :cond_1

    new-array v1, v4, [I

    const v2, 0x10100a7    # android.R.attr.state_pressed

    aput v2, v1, v3

    invoke-virtual {p0, p2, p5, p6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_1
    if-eqz p3, :cond_2

    new-array v1, v4, [I

    const v2, 0x101009c    # android.R.attr.state_focused

    aput v2, v1, v3

    invoke-virtual {p0, p3, p5, p6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_2
    if-eqz p4, :cond_3

    new-array v1, v4, [I

    const v2, -0x101009e

    aput v2, v1, v3

    invoke-virtual {p0, p4, p5, p6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_3
    return-object v0

    nop

    :array_0
    .array-data 4
        -0x10100a7
        -0x10100a1
        -0x101009c
        0x101009e    # android.R.attr.state_enabled
    .end array-data
.end method

.method protected setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mCustom_imagepath:Ljava/lang/String;

    const-string/jumbo v2, "spen_sdk_resource_custom"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mContextResources:Landroid/content/res/Resources;

    const-string/jumbo v2, "drawable"

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mDrawableContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, p1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkResources:Landroid/content/res/Resources;

    const-string/jumbo v2, "drawable"

    const-string/jumbo v3, "com.samsung.android.sdk.spen30"

    invoke-virtual {v1, p1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkResources:Landroid/content/res/Resources;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenScreenCodecDecoder;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mContextResources:Landroid/content/res/Resources;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenScreenCodecDecoder;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkResources:Landroid/content/res/Resources;

    const-string/jumbo v2, "drawable"

    const-string/jumbo v3, "com.samsung.android.sdk.spen30"

    invoke-virtual {v1, p1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkResources:Landroid/content/res/Resources;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenScreenCodecDecoder;->getDrawable(Landroid/content/res/Resources;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method protected setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mCustom_imagepath:Ljava/lang/String;

    const-string/jumbo v2, "spen_sdk_resource_custom"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mContextResources:Landroid/content/res/Resources;

    const-string/jumbo v2, "drawable"

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mDrawableContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, p1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkResources:Landroid/content/res/Resources;

    const-string/jumbo v2, "drawable"

    const-string/jumbo v3, "com.samsung.android.sdk.spen30"

    invoke-virtual {v1, p1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkResources:Landroid/content/res/Resources;

    invoke-virtual {p0, v0, v1, p2, p3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->resizeImage(Landroid/content/res/Resources;III)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mContextResources:Landroid/content/res/Resources;

    invoke-virtual {p0, v0, v1, p2, p3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->resizeImage(Landroid/content/res/Resources;III)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkResources:Landroid/content/res/Resources;

    const-string/jumbo v2, "drawable"

    const-string/jumbo v3, "com.samsung.android.sdk.spen30"

    invoke-virtual {v1, p1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkResources:Landroid/content/res/Resources;

    invoke-virtual {p0, v0, v1, p2, p3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->resizeImage(Landroid/content/res/Resources;III)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method

.method protected setDrawableSelectColor(III)Landroid/graphics/drawable/StateListDrawable;
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    const/4 v1, 0x2

    new-array v1, v1, [I

    const v2, -0x10100a7

    aput v2, v1, v3

    const v2, -0x10100a1

    aput v2, v1, v4

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2, p1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    new-array v1, v4, [I

    const v2, 0x10100a7    # android.R.attr.state_pressed

    aput v2, v1, v3

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2, p2}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    new-array v1, v4, [I

    const v2, 0x10100a1    # android.R.attr.state_selected

    aput v2, v1, v3

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2, p3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    return-object v0
.end method

.method protected setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    if-eqz p1, :cond_0

    const/4 v1, 0x3

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_0
    if-eqz p2, :cond_1

    new-array v1, v4, [I

    const v2, 0x10100a7    # android.R.attr.state_pressed

    aput v2, v1, v3

    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_1
    if-eqz p3, :cond_2

    new-array v1, v4, [I

    const v2, 0x101009c    # android.R.attr.state_focused

    aput v2, v1, v3

    invoke-virtual {p0, p3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_2
    if-eqz p2, :cond_3

    new-array v1, v4, [I

    const v2, 0x10100a1    # android.R.attr.state_selected

    aput v2, v1, v3

    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_3
    return-object v0

    nop

    :array_0
    .array-data 4
        -0x10100a7
        -0x10100a1
        -0x101009c
    .end array-data
.end method

.method protected setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    if-eqz p1, :cond_0

    const/4 v1, 0x3

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-virtual {p0, p1, p4, p5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_0
    if-eqz p2, :cond_1

    new-array v1, v4, [I

    const v2, 0x10100a7    # android.R.attr.state_pressed

    aput v2, v1, v3

    invoke-virtual {p0, p2, p4, p5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_1
    if-eqz p3, :cond_2

    new-array v1, v4, [I

    const v2, 0x101009c    # android.R.attr.state_focused

    aput v2, v1, v3

    invoke-virtual {p0, p3, p4, p5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_2
    if-eqz p2, :cond_3

    new-array v1, v4, [I

    const v2, 0x10100a1    # android.R.attr.state_selected

    aput v2, v1, v3

    invoke-virtual {p0, p2, p4, p5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_3
    return-object v0

    nop

    :array_0
    .array-data 4
        -0x10100a7
        -0x10100a1
        -0x101009c
    .end array-data
.end method

.method protected setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    if-eqz p1, :cond_0

    const/4 v1, 0x3

    new-array v1, v1, [I

    const v2, -0x10100a7

    aput v2, v1, v4

    const v2, -0x10100a1

    aput v2, v1, v5

    const/4 v2, 0x2

    const v3, -0x101009c

    aput v3, v1, v2

    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_0
    if-eqz p3, :cond_1

    new-array v1, v5, [I

    const v2, 0x101009c    # android.R.attr.state_focused

    aput v2, v1, v4

    invoke-virtual {p0, p3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_1
    if-eqz p2, :cond_2

    new-array v1, v5, [I

    const v2, 0x10100a7    # android.R.attr.state_pressed

    aput v2, v1, v4

    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_2
    if-eqz p4, :cond_3

    new-array v1, v5, [I

    const v2, 0x10100a1    # android.R.attr.state_selected

    aput v2, v1, v4

    invoke-virtual {p0, p4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_3
    return-object v0
.end method

.method protected setDrawableSelectedFocusImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    if-eqz p1, :cond_0

    const/4 v1, 0x3

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_0
    if-eqz p2, :cond_1

    new-array v1, v4, [I

    const v2, 0x10100a7    # android.R.attr.state_pressed

    aput v2, v1, v3

    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_1
    if-eqz p3, :cond_2

    new-array v1, v5, [I

    fill-array-data v1, :array_1

    invoke-virtual {p0, p3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_2
    if-eqz p4, :cond_3

    new-array v1, v5, [I

    fill-array-data v1, :array_2

    invoke-virtual {p0, p4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_3
    if-eqz p2, :cond_4

    new-array v1, v4, [I

    const v2, 0x10100a1    # android.R.attr.state_selected

    aput v2, v1, v3

    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_4
    return-object v0

    :array_0
    .array-data 4
        -0x10100a7
        -0x10100a1
        -0x101009c
    .end array-data

    :array_1
    .array-data 4
        0x101009c    # android.R.attr.state_focused
        0x10100a1    # android.R.attr.state_selected
    .end array-data

    :array_2
    .array-data 4
        0x101009c    # android.R.attr.state_focused
        -0x10100a1
    .end array-data
.end method

.method public unbindDrawables(Landroid/view/View;)V
    .locals 3

    const/4 v2, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mSdkVersion:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_6

    invoke-virtual {p1, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_1
    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    :cond_2
    instance-of v0, p1, Landroid/view/ViewGroup;

    if-eqz v0, :cond_9

    const/4 v0, 0x0

    move v1, v0

    :goto_2
    move-object v0, p1

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-lt v1, v0, :cond_7

    instance-of v0, p1, Landroid/widget/AdapterView;

    if-nez v0, :cond_8

    move-object v0, p1

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    move-object v1, p1

    :goto_3
    instance-of v0, v1, Landroid/widget/SeekBar;

    if-eqz v0, :cond_3

    move-object v0, v1

    check-cast v0, Landroid/widget/SeekBar;

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_3
    instance-of v0, v1, Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    move-object v0, v1

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    move-object v0, v1

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_4
    instance-of v0, v1, Landroid/widget/ImageButton;

    if-eqz v0, :cond_5

    move-object v0, v1

    check-cast v0, Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    move-object v0, v1

    check-cast v0, Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_5
    if-eqz v1, :cond_0

    goto :goto_0

    :cond_6
    invoke-virtual {p1, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    :cond_7
    move-object v0, p1

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_8
    check-cast p1, Landroid/widget/AdapterView;

    invoke-virtual {p1, v2}, Landroid/widget/AdapterView;->setAdapter(Landroid/widget/Adapter;)V

    move-object v1, v2

    goto :goto_3

    :cond_9
    move-object v1, p1

    goto :goto_3
.end method
