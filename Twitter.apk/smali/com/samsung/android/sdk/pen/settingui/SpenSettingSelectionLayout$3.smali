.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/GestureDetector$OnGestureListener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->mMovableRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->getMovableRect()Landroid/graphics/Rect;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;)Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    float-to-int v1, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    float-to-int v2, v0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    sub-int/2addr v1, v4

    iput v1, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->mXDelta:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    sub-int v0, v2, v0

    iput v0, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->mYDelta:I

    const/4 v0, 0x1

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 7

    const/4 v4, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->mNeedCalculateMargin:Z

    if-eqz v0, :cond_0

    new-array v0, v4, [I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->getLocationOnScreen([I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v2

    aget v3, v0, v5

    int-to-float v3, v3

    sub-float/2addr v2, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    sub-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->mLeftMargin:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    aget v3, v0, v6

    int-to-float v3, v3

    sub-float/2addr v2, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    sub-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->mTopMargin:I

    new-array v1, v4, [I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->getRootView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    aget v3, v0, v5

    aget v4, v1, v5

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    iget v4, v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->mLeftMargin:I

    add-int/2addr v3, v4

    iput v3, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->mTotalLeftMargin:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    aget v0, v0, v6

    aget v1, v1, v6

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    iget v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->mTopMargin:I

    add-int/2addr v0, v1

    iput v0, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->mTotalTopMargin:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    iput-boolean v5, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->mNeedCalculateMargin:Z

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->mFirstLongPress:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->mTitleLayout:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->performHapticFeedback(I)Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    iput-boolean v5, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->mFirstLongPress:Z

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->getMovableRect()Landroid/graphics/Rect;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;)Landroid/graphics/Rect;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v1, v0}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    iput-boolean v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->mMoveSettingLayout:Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout;->mIndicator:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0

    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
