.class Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "Twttr"


# instance fields
.field context:Landroid/content/Context;

.field items:Ljava/util/List;

.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;Landroid/content/Context;ILjava/util/List;)V
    .locals 1

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->items:Ljava/util/List;

    iput-object p4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->items:Ljava/util/List;

    iput-object p2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->context:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    const/4 v3, 0x0

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->context:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x1090008    # android.R.layout.simple_spinner_item

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    :cond_0
    const v0, 0x1020014    # android.R.id.text1

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSingleLine(Z)V

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->items:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v1

    const/high16 v2, 0x41700000    # 15.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v1

    const/high16 v2, -0x3f600000    # -5.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    invoke-virtual {v0, v1, v3, v3, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v1

    const/high16 v2, 0x42140000    # 37.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMinimumHeight(I)V

    return-object p2
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    const/4 v3, 0x0

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->context:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x1090008    # android.R.layout.simple_spinner_item

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    :cond_0
    const v0, 0x1020014    # android.R.id.text1

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSingleLine(Z)V

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->items:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v1

    const/high16 v2, 0x41700000    # 15.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v1

    const/high16 v2, -0x3f600000    # -5.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    invoke-virtual {v0, v1, v3, v3, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$ListAdapter;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;->access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v1

    const/high16 v2, 0x42140000    # 37.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMinimumHeight(I)V

    return-object p2
.end method
