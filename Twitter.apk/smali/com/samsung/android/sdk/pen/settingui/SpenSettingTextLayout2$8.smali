.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$8;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPopupMaxButton:Landroid/view/View;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$PopupListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->access$11(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$PopupListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$PopupListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->access$11(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$PopupListener;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$PopupListener;->onPopup(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPopupMinButton:Landroid/view/View;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$PopupListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->access$11(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$PopupListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$8;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$PopupListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->access$11(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$PopupListener;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$PopupListener;->onPopup(Z)V

    goto :goto_0
.end method
