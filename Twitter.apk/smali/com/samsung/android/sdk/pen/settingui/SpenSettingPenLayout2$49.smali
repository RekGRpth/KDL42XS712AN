.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$49;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$49;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5

    const/high16 v4, 0x425c0000    # 55.0f

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$49;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v0}, Landroid/widget/HorizontalScrollView;->getScrollX()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$49;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$49;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    const/16 v1, 0xa

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$49;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v2}, Landroid/widget/HorizontalScrollView;->getScrollX()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$49;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    int-to-float v3, v3

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->playScrollAnimation(IFF)V
    invoke-static {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$36(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;IFF)V

    :cond_0
    const/4 v0, 0x0

    return v0
.end method
