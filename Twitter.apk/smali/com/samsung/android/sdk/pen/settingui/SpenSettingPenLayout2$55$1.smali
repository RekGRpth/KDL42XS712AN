.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;

.field private final synthetic val$fromFinal:F

.field private final synthetic val$step:F

.field private final synthetic val$toFinal:F


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;FFF)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;

    iput p2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->val$fromFinal:F

    iput p3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->val$toFinal:F

    iput p4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->val$step:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    move-result-object v0

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCountForScrollPen:I
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$38(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$39(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;I)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->val$fromFinal:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->val$toFinal:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->val$fromFinal:F

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCountForScrollPen:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$38(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->val$step:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->val$toFinal:F

    cmpl-float v1, v0, v1

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

    float-to-int v0, v0

    invoke-virtual {v1, v0, v3}, Landroid/widget/HorizontalScrollView;->scrollTo(II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->val$toFinal:F

    float-to-int v1, v1

    invoke-virtual {v0, v1, v3}, Landroid/widget/HorizontalScrollView;->scrollTo(II)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    move-result-object v0

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollTimerForScrollPen:Ljava/util/Timer;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$40(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Ljava/util/Timer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    move-result-object v0

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollTimerForScrollPen:Ljava/util/Timer;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$40(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    move-result-object v0

    invoke-static {v0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$41(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;Ljava/util/Timer;)V

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->val$fromFinal:F

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCountForScrollPen:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$38(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->val$step:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->val$toFinal:F

    cmpg-float v1, v0, v1

    if-gez v1, :cond_3

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

    float-to-int v0, v0

    invoke-virtual {v1, v0, v3}, Landroid/widget/HorizontalScrollView;->scrollTo(II)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->val$toFinal:F

    float-to-int v1, v1

    invoke-virtual {v0, v1, v3}, Landroid/widget/HorizontalScrollView;->scrollTo(II)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    move-result-object v0

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollTimerForScrollPen:Ljava/util/Timer;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$40(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Ljava/util/Timer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    move-result-object v0

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mScrollTimerForScrollPen:Ljava/util/Timer;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$40(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$55;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    move-result-object v0

    invoke-static {v0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$41(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;Ljava/util/Timer;)V

    goto/16 :goto_0
.end method
