.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown$SizeDropdownSelectListner;


# instance fields
.field final synthetic this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSelectItem(I)V
    .locals 4

    const/16 v2, 0x438

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasWidth()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v1}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasHeight()I

    move-result v1

    if-ge v0, v1, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v1}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasWidth()I

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v0

    iget v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v0

    iput v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v0

    iget v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    int-to-double v0, v0

    const-wide v2, 0x4076800000000000L    # 360.0

    div-double/2addr v0, v2

    double-to-float v1, v0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextFontSizeList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v1

    iput v0, v2, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->setPreviewTextSize(F)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextFontSizeList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$5(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v0

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontSizeButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/Button;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontSize:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$7(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->invalidate()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getTextSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setTextSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v1}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasHeight()I

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$6;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v0

    iput v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasWidth:I

    goto/16 :goto_1
.end method
