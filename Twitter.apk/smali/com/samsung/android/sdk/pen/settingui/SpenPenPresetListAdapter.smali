.class Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;
.super Landroid/widget/BaseAdapter;
.source "Twttr"


# instance fields
.field private final mBitmapPaint:Landroid/graphics/Paint;

.field protected mContext:Landroid/content/Context;

.field protected mCurrentSeleted:I

.field protected mCustom_imagepath:Ljava/lang/String;

.field protected mDeletePresetItem:Landroid/view/View$OnClickListener;

.field protected mFocusItemListener:Landroid/view/View$OnFocusChangeListener;

.field protected mList:Ljava/util/List;

.field protected mOnClickPresetItemListener:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$OnClickPresetItemListener;

.field protected mOnTouchListener:Landroid/view/View$OnTouchListener;

.field protected mOnePT:F

.field protected mPenPluginInfoList:Ljava/util/ArrayList;

.field private mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

.field private final mRect:Landroid/graphics/RectF;

.field protected mScale:F

.field protected mSdkResources:Landroid/content/res/Resources;

.field mSecondViewEnter:I

.field protected mSelectLongPresetItem:Landroid/view/View$OnLongClickListener;

.field protected mSelectPresetItem:Landroid/view/View$OnClickListener;

.field protected mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/List;Ljava/lang/String;Ljava/util/ArrayList;F)V
    .locals 2

    const/4 v1, -0x1

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mOnePT:F

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mScale:F

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mCustom_imagepath:Ljava/lang/String;

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mCurrentSeleted:I

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mSecondViewEnter:I

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mSelectPresetItem:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$2;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$3;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mSelectLongPresetItem:Landroid/view/View$OnLongClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$4;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mDeletePresetItem:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$5;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mFocusItemListener:Landroid/view/View$OnFocusChangeListener;

    iput p6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mScale:F

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v0, p6

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mOnePT:F

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    iput-object p3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mList:Ljava/util/List;

    iput-object p4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mCustom_imagepath:Ljava/lang/String;

    iput-object p5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginInfoList:Ljava/util/ArrayList;

    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mBitmapPaint:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mRect:Landroid/graphics/RectF;

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string/jumbo v1, "com.samsung.android.sdk.spen30"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mSdkResources:Landroid/content/res/Resources;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method private bitmapResize(I)Landroid/graphics/drawable/Drawable;
    .locals 5

    const/16 v4, 0x41

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mSdkResources:Landroid/content/res/Resources;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getPresetImageName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "drawable"

    const-string/jumbo v3, "com.samsung.android.sdk.spen30"

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, v0, v4, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->resizeImage(III)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public OnClickPresetItemListener(Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$OnClickPresetItemListener;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mOnClickPresetItemListener:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$OnClickPresetItemListener;

    return-void
.end method

.method public close()V
    .locals 2

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mList:Ljava/util/List;

    :cond_0
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mOnClickPresetItemListener:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$OnClickPresetItemListener;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginInfoList:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginInfoList:Ljava/util/ArrayList;

    :cond_1
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mCustom_imagepath:Ljava/lang/String;

    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method protected getPresetImage(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginInfoList:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    new-instance v3, Ljava/lang/String;

    const-string/jumbo v4, "urlInfo"

    invoke-direct {v3, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPluginInfo()Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    move-result-object v4

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->presetIconImageUri:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string/jumbo v0, "pen_preset_brush"

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPluginInfo()Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->presetIconImageUri:Ljava/lang/String;

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11

    if-nez p2, :cond_0

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mCustom_imagepath:Ljava/lang/String;

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mScale:F

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->getPenPresetListRow()Landroid/view/View;

    move-result-object p2

    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mSecondViewEnter:I

    if-ne p1, v0, :cond_5

    const/4 v0, 0x4

    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    const v0, 0xb82e65

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setClickable(Z)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setFocusable(Z)V

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mSelectPresetItem:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mSelectLongPresetItem:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mFocusItemListener:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    const v1, 0xb82e66

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setClickable(Z)V

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mDeletePresetItem:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mFocusItemListener:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mCurrentSeleted:I

    if-ne p1, v2, :cond_6

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    sget-boolean v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIsSwichTab:Z

    if-nez v2, :cond_1

    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocus()Z

    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v3, "string_delete_preset"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v3, "string_delete_preset"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "%d"

    const-string/jumbo v4, ""

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mList:Ljava/util/List;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;

    const v2, 0xb82ec9

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->bitmapResize(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getFlag()Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->presetPreview(Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;)V

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->setFlag(Z)V

    :cond_3
    const v2, 0xb82f2d

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getPenName()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "com.samsung.android.sdk.pen.pen.preload.MagicPen"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getColor()I

    move-result v3

    shr-int/lit8 v3, v3, 0x18

    and-int/lit16 v3, v3, 0xff

    int-to-double v3, v3

    const-wide v5, 0x406fe00000000000L    # 255.0

    div-double/2addr v3, v5

    double-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setAlpha(F)V

    const-wide v3, 0x400599999999999aL    # 2.7

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getPenSize()F

    move-result v5

    const/high16 v6, 0x41900000    # 18.0f

    sub-float/2addr v5, v6

    const v6, 0x40c9999a    # 6.3f

    mul-float/2addr v5, v6

    const/high16 v6, 0x42280000    # 42.0f

    div-float/2addr v5, v6

    float-to-double v5, v5

    add-double/2addr v3, v5

    double-to-float v3, v3

    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mOnePT:F

    const/high16 v6, 0x41f00000    # 30.0f

    mul-float/2addr v5, v6

    float-to-int v5, v5

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mOnePT:F

    mul-float/2addr v6, v3

    float-to-int v6, v6

    invoke-direct {v4, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mOnePT:F

    const/high16 v6, 0x41980000    # 19.0f

    mul-float/2addr v5, v6

    float-to-int v5, v5

    iput v5, v4, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mOnePT:F

    float-to-double v5, v5

    const-wide v7, 0x4040400000000000L    # 32.5

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v3, v9

    float-to-double v9, v3

    sub-double/2addr v7, v9

    mul-double/2addr v5, v7

    double-to-int v3, v5

    iput v3, v4, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :goto_2
    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_pen_preset"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_pen_preset"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    add-int/lit8 v4, p1, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_4
    return-object p2

    :cond_5
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    :cond_6
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto/16 :goto_1

    :cond_7
    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_2
.end method

.method protected presetPreview(Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;)V
    .locals 20

    if-eqz p1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginInfoList:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v15

    if-eqz v15, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v15, v1}, Landroid/graphics/Bitmap;->eraseColor(I)V

    new-instance v16, Landroid/graphics/Canvas;

    move-object/from16 v0, v16

    invoke-direct {v0, v15}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getPenName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->getPenPluginIndexByPenName(Ljava/lang/String;)I

    move-result v17

    const/4 v1, -0x1

    move/from16 v0, v17

    if-eq v0, v1, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v17

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v1

    if-nez v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getPenName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->loadPenPlugin(Landroid/content/Context;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v17

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v1

    if-eqz v1, :cond_0

    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v17

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v1

    invoke-interface {v1, v15}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setBitmap(Landroid/graphics/Bitmap;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v17

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getPenSize()F

    move-result v2

    invoke-interface {v1, v2}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setSize(F)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v17

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getColor()I

    move-result v2

    invoke-interface {v1, v2}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setColor(I)V

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getPenName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "com.samsung.android.sdk.pen.pen.preload.Beautify"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v17

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v1

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getAdvancedSetting()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setAdvancedSetting(Ljava/lang/String;)V

    :cond_3
    const/4 v1, 0x0

    invoke-virtual {v15, v1}, Landroid/graphics/Bitmap;->eraseColor(I)V

    const/high16 v18, 0x41b00000    # 22.0f

    const/high16 v19, 0x422c0000    # 43.0f

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mOnePT:F

    mul-float v6, v18, v3

    const/high16 v3, 0x42040000    # 33.0f

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mOnePT:F

    mul-float v7, v3, v4

    const/high16 v8, 0x3f000000    # 0.5f

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getPenSize()F

    move-result v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-wide v3, v1

    invoke-static/range {v1 .. v14}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mRect:Landroid/graphics/RectF;

    invoke-interface {v3, v4, v5}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    invoke-virtual {v4}, Landroid/view/MotionEvent;->recycle()V

    const-wide/16 v3, 0x5

    add-long/2addr v3, v1

    const/4 v5, 0x2

    add-float v6, v18, v19

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    move-object/from16 v0, p0

    iget v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mOnePT:F

    mul-float/2addr v6, v7

    const/high16 v7, 0x42040000    # 33.0f

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mOnePT:F

    mul-float/2addr v7, v8

    const/high16 v8, 0x3f000000    # 0.5f

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getPenSize()F

    move-result v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static/range {v1 .. v14}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mRect:Landroid/graphics/RectF;

    invoke-interface {v3, v4, v5}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    invoke-virtual {v4}, Landroid/view/MotionEvent;->recycle()V

    const-wide/16 v3, 0xa

    add-long/2addr v3, v1

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mOnePT:F

    mul-float v6, v6, v19

    const/high16 v7, 0x42040000    # 33.0f

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mOnePT:F

    mul-float/2addr v7, v8

    const/high16 v8, 0x3f000000    # 0.5f

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->getPenSize()F

    move-result v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static/range {v1 .. v14}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v17

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mRect:Landroid/graphics/RectF;

    invoke-interface {v1, v2, v3}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    invoke-virtual {v2}, Landroid/view/MotionEvent;->recycle()V

    const/4 v1, 0x0

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mBitmapPaint:Landroid/graphics/Paint;

    move-object/from16 v0, v16

    invoke-virtual {v0, v15, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0
.end method

.method public resizeImage(III)Landroid/graphics/drawable/Drawable;
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mSdkResources:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenScreenCodecDecoder;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mOnePT:F

    int-to-float v5, p2

    mul-float/2addr v2, v5

    float-to-int v2, v2

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mOnePT:F

    int-to-float v6, p3

    mul-float/2addr v5, v6

    float-to-int v5, v5

    int-to-float v2, v2

    int-to-float v6, v3

    div-float/2addr v2, v6

    int-to-float v5, v5

    int-to-float v6, v4

    div-float v6, v5, v6

    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {v5, v2, v6}, Landroid/graphics/Matrix;->postScale(FF)Z

    const/4 v6, 0x1

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mSdkResources:Landroid/content/res/Resources;

    invoke-direct {v0, v2, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public setPenPlugin(Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;)V
    .locals 1

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->getPenPluginInfoList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mPenPluginInfoList:Ljava/util/ArrayList;

    return-void
.end method
