.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    const/high16 v4, 0x44870000    # 1080.0f

    const/high16 v3, 0x3f800000    # 1.0f

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mAutoIncrement:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getMax()I

    move-result v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCurrentCutterType:I

    aget-object v0, v0, v1

    iget v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasSize:I

    int-to-float v2, v2

    div-float/2addr v2, v4

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->incrementProgressBy(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCurrentCutterType:I

    aget-object v1, v1, v2

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->repeatUpdateHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    const-wide/16 v2, 0x14

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mAutoDecrement:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCurrentCutterType:I

    aget-object v0, v0, v1

    iget v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCanvasSize:I

    int-to-float v2, v2

    div-float/2addr v2, v4

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterSizeSeekBar:Landroid/widget/SeekBar;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->incrementProgressBy(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCurrentCutterType:I

    aget-object v1, v1, v2

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->repeatUpdateHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$RptUpdater;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V

    const-wide/16 v2, 0x14

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method
