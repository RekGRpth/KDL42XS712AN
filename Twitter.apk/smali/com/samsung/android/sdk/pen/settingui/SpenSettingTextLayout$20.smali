.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$20;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$20;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    return-object v0
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsRotated:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsRotated:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsRotated:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsRotated2:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsRotated2:Z

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mMovableRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->getMovableRect()Landroid/graphics/Rect;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsRotated:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->rotatePosition()V
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$14(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsRotated:Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mIsRotated2:Z

    :goto_1
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$20$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$20$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$20;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->checkPosition()V
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$15(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1
.end method
