.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$33;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$33;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$33;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyStyleBtnViews:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$33;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyStyleBtnViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$33;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$33;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCurrentBeautifyStyle:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$32(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)I

    move-result v1

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->resetBeautifyAdvanceDataAndUpdateSeekBarUi(I)V
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$34(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;I)V

    :cond_1
    return-void

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$33;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCurrentBeautifyStyle:I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$32(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)I

    move-result v4

    if-eq v1, v4, :cond_0

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$33;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-static {v4, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$33(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;I)V

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setSelected(Z)V

    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_3
    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto :goto_1
.end method
