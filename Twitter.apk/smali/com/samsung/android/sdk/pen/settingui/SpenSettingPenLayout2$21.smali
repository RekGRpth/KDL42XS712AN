.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$21;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->colorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$22(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getCurrentTableIndex()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPaletteLeftButton:Landroid/view/View;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPaletteLeftButton:Landroid/view/View;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/View;->setEnabled(Z)V

    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->colorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$22(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->setNextColorTable(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->setSelectBoxPos(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->colorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$22(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->getCurrentTableIndex()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPaletteRightButton:Landroid/view/View;

    const v1, 0x3e4ccccd    # 0.2f

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$21;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPaletteRightButton:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    :cond_1
    return-void
.end method
