.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;

.field private final synthetic val$fromFinal:F

.field private final synthetic val$step:F

.field private final synthetic val$toFinal:F


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;FFF)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;

    iput p2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84$1;->val$fromFinal:F

    iput p3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84$1;->val$toFinal:F

    iput p4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84$1;->val$step:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-result-object v0

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCount:I
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$70(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$71(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;I)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84$1;->val$fromFinal:F

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84$1;->val$toFinal:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84$1;->val$fromFinal:F

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCount:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$70(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84$1;->val$step:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84$1;->val$toFinal:F

    cmpl-float v1, v0, v1

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-virtual {v1, v0, v3}, Landroid/widget/HorizontalScrollView;->scrollTo(II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84$1;->val$toFinal:F

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {v0, v1, v3}, Landroid/widget/HorizontalScrollView;->scrollTo(II)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-result-object v0

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScrollTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$72(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Ljava/util/Timer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-result-object v0

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScrollTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$72(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-result-object v0

    invoke-static {v0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$73(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Ljava/util/Timer;)V

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84$1;->val$fromFinal:F

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCount:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$70(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84$1;->val$step:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84$1;->val$toFinal:F

    cmpg-float v1, v0, v1

    if-gez v1, :cond_3

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-virtual {v1, v0, v3}, Landroid/widget/HorizontalScrollView;->scrollTo(II)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84$1;->val$toFinal:F

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {v0, v1, v3}, Landroid/widget/HorizontalScrollView;->scrollTo(II)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-result-object v0

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScrollTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$72(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Ljava/util/Timer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-result-object v0

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScrollTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$72(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    move-result-object v0

    invoke-static {v0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$73(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Ljava/util/Timer;)V

    goto/16 :goto_0
.end method
