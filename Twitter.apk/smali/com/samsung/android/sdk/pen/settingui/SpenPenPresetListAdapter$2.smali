.class Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/View;->clearFocus()V

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/View;->playSoundEffect(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mOnClickPresetItemListener:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$OnClickPresetItemListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->mOnClickPresetItemListener:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$OnClickPresetItemListener;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$OnClickPresetItemListener;->selectPresetItem(I)V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method
