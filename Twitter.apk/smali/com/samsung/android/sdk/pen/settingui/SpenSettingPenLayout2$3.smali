.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2$OnColorChangedListener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public colorChanged(II)V
    .locals 8

    const/4 v7, 0x5

    const/4 v6, 0x2

    const/4 v5, -0x1

    const/4 v4, 0x0

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->isMinimumMode:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xc

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->setVisibility(I)V

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x43230000    # 163.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    invoke-direct {v0, v5, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorSelectPickerLayout:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-static {v0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$5(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-static {v0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x43d60000    # 428.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setExpandBarPosition(I)V
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->access$7(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;I)V

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->getPenPluginIndexByPenName(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v5, :cond_1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    if-eq v1, v0, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    const/16 v0, 0xd

    if-eq p2, v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenSizeSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setAlpha(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifySeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceCursiveSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->invalidate()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceDummySeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->invalidate()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceSustenanceSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->invalidate()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mBeautifyAdvanceModulationSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->invalidate()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getPenSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getPenAttribute(I)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlpha:I

    shl-int/lit8 v0, v0, 0x18

    const/high16 v2, -0x1000000

    and-int/2addr v0, v2

    const v2, 0xffffff

    and-int/2addr v2, p1

    or-int/2addr v0, v2

    iput v0, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v2, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    iput v2, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    iget v2, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->setStrokeColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->invalidate()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenAlphaSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    iget v2, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPenNameIndex:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    iget v2, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-interface {v0, v2}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->setColorPickerColor(I)V

    :cond_3
    const/16 v0, 0xd

    if-ne p2, v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v1, v6}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getToolTypeAction(I)I

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPreCanvasPenAction:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v1, v3}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getToolTypeAction(I)I

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mPreCanvasFingerAction:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v0, v6, v7}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setToolTypeAction(II)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v0, v3, v7}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setToolTypeAction(II)V

    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->setColorPickerMode()V

    sput-boolean v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerShow:Z

    sget-boolean v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerShow:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->show()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mFirstTimeColorPickerShow:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->movePosition(II)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$3;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->mFirstTimeColorPickerShow:Z

    goto/16 :goto_0

    :cond_5
    iput p1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    goto/16 :goto_1
.end method
