.class Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;
.super Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
.source "Twttr"


# static fields
.field public static final IB_PEN_PRESET_DELETE_ID:I = 0xb82e66

.field public static final IB_PEN_PRESET_PREVIEW_ID:I = 0xb82e65

.field public static final IV_PEN_PRESET_ALPHA_PREVIEW_ID:I = 0xb82f2d

.field public static final IV_PEN_PRESET_PREVIEW_ID:I = 0xb82ec9

.field private static final mImagePath_pen_preset_bg:Ljava/lang/String; = "pen_preset_bg"

.field private static final mImagePath_pen_preset_bg_focus:Ljava/lang/String; = "pen_preset_bg_focus"

.field private static final mImagePath_pen_preset_bg_press:Ljava/lang/String; = "pen_preset_bg_press"

.field private static final mImagePath_progress_bg_alpha:Ljava/lang/String; = "preview_alpha"

.field private static final mImagePath_snote_delete:Ljava/lang/String; = "pen_preset_delete_normal"

.field private static final mImagePath_snote_delete_focus:Ljava/lang/String; = "pen_preset_delete_focus"

.field private static final mImagePath_snote_delete_press:Ljava/lang/String; = "pen_preset_delete_press"


# instance fields
.field private final mPresetListItemView:Landroid/view/View;

.field private final mSdkVersion:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;F)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->mSdkVersion:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->mDrawableContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->PresetListItemView(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->mPresetListItemView:Landroid/view/View;

    return-void
.end method

.method private PresetListItemView(Landroid/content/Context;)Landroid/view/View;
    .locals 12

    const/16 v11, 0x10

    const/4 v10, 0x1

    const/high16 v9, 0x42820000    # 65.0f

    const/4 v8, 0x0

    invoke-virtual {p0, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->getIntValueAppliedDensity(F)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x41b80000    # 23.0f

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->getIntValueAppliedDensity(F)I

    move-result v1

    int-to-float v1, v1

    new-instance v2, Landroid/widget/RelativeLayout;

    invoke-direct {v2, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v3, Landroid/widget/AbsListView$LayoutParams;

    const/high16 v4, 0x428a0000    # 69.0f

    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->getIntValueAppliedDensity(F)I

    move-result v4

    const/high16 v5, 0x42940000    # 74.0f

    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->getIntValueAppliedDensity(F)I

    move-result v5

    invoke-direct {v3, v4, v5}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v2, v8, v8, v8, v8}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    invoke-virtual {v2, v8}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    invoke-virtual {v2, v11}, Landroid/widget/RelativeLayout;->setGravity(I)V

    new-instance v3, Landroid/widget/RelativeLayout;

    invoke-direct {v3, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v4, Landroid/widget/RelativeLayout$LayoutParams;

    const/high16 v5, 0x41f00000    # 30.0f

    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->getIntValueAppliedDensity(F)I

    move-result v5

    const/high16 v6, 0x41200000    # 10.0f

    invoke-virtual {p0, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->getIntValueAppliedDensity(F)I

    move-result v6

    invoke-direct {v4, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/high16 v5, 0x41a00000    # 20.0f

    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->getIntValueAppliedDensity(F)I

    move-result v5

    iput v5, v4, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    const/high16 v5, 0x420c0000    # 35.0f

    invoke-virtual {p0, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->getIntValueAppliedDensity(F)I

    move-result v5

    iput v5, v4, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    invoke-virtual {v3, v8, v8, v8, v8}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->mSdkVersion:I

    if-ge v4, v11, :cond_0

    const-string/jumbo v4, "preview_alpha"

    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    const v4, 0xb82f2d

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setId(I)V

    new-instance v4, Landroid/widget/ImageButton;

    invoke-direct {v4, p1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {p0, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->getIntValueAppliedDensity(F)I

    move-result v6

    invoke-virtual {p0, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->getIntValueAppliedDensity(F)I

    move-result v7

    invoke-direct {v5, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v4, v8}, Landroid/widget/ImageButton;->setBackgroundColor(I)V

    invoke-virtual {v4, v8, v8, v8, v8}, Landroid/widget/ImageButton;->setPadding(IIII)V

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    float-to-int v0, v0

    invoke-virtual {v4, v0}, Landroid/widget/ImageButton;->setMaxHeight(I)V

    invoke-virtual {v4, v10}, Landroid/widget/ImageButton;->setClickable(Z)V

    invoke-virtual {v4, v10}, Landroid/widget/ImageButton;->setFocusable(Z)V

    const v0, 0xb82e65

    invoke-virtual {v4, v0}, Landroid/widget/ImageButton;->setId(I)V

    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {p0, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->getIntValueAppliedDensity(F)I

    move-result v6

    invoke-virtual {p0, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->getIntValueAppliedDensity(F)I

    move-result v7

    invoke-direct {v5, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v5, 0xb82ec9

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setId(I)V

    new-instance v5, Landroid/widget/ImageView;

    invoke-direct {v5, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v6, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {p0, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->getIntValueAppliedDensity(F)I

    move-result v7

    invoke-virtual {p0, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->getIntValueAppliedDensity(F)I

    move-result v8

    invoke-direct {v6, v7, v8}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v6, Landroid/widget/ImageButton;

    invoke-direct {v6, p1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    new-instance v7, Landroid/widget/RelativeLayout$LayoutParams;

    float-to-int v8, v1

    float-to-int v1, v1

    invoke-direct {v7, v8, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/high16 v1, -0x40000000    # -2.0f

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->getIntValueAppliedDensity(F)I

    move-result v1

    iput v1, v7, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    const/high16 v1, -0x40800000    # -1.0f

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->getIntValueAppliedDensity(F)I

    move-result v1

    iput v1, v7, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    const/16 v1, 0xb

    invoke-virtual {v7, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v6, v7}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->mSdkVersion:I

    if-ge v1, v11, :cond_1

    const-string/jumbo v1, "pen_preset_bg"

    const-string/jumbo v7, "pen_preset_bg_press"

    const-string/jumbo v8, "pen_preset_bg_focus"

    const-string/jumbo v9, "pen_preset_bg_press"

    invoke-virtual {p0, v1, v7, v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const-string/jumbo v1, "pen_preset_bg"

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const-string/jumbo v1, "pen_preset_delete_normal"

    const-string/jumbo v7, "pen_preset_delete_press"

    const-string/jumbo v8, "pen_preset_delete_focus"

    invoke-virtual {p0, v1, v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    invoke-virtual {v6, v1}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_1
    invoke-virtual {v6, v10}, Landroid/widget/ImageButton;->setClickable(Z)V

    invoke-virtual {v6, v10}, Landroid/widget/ImageButton;->setFocusable(Z)V

    const v1, 0xb82e66

    invoke-virtual {v6, v1}, Landroid/widget/ImageButton;->setId(I)V

    invoke-virtual {v2, v5}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v2, v4}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v2, v6}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    return-object v2

    :cond_0
    const-string/jumbo v4, "preview_alpha"

    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    :cond_1
    const-string/jumbo v1, "pen_preset_bg"

    const-string/jumbo v7, "pen_preset_bg_press"

    const-string/jumbo v8, "pen_preset_bg_focus"

    const-string/jumbo v9, "pen_preset_bg_press"

    invoke-virtual {p0, v1, v7, v8, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    const-string/jumbo v1, "pen_preset_bg"

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v5, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    const-string/jumbo v1, "pen_preset_delete_normal"

    const-string/jumbo v7, "pen_preset_delete_press"

    const-string/jumbo v8, "pen_preset_delete_focus"

    invoke-virtual {p0, v1, v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    invoke-virtual {v6, v1}, Landroid/widget/ImageButton;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method


# virtual methods
.method public getPenPresetListRow()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPresetListItem;->mPresetListItemView:Landroid/view/View;

    return-object v0
.end method
