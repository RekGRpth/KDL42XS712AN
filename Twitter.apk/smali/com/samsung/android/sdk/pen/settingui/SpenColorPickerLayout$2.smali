.class Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 11

    const/4 v8, 0x0

    const/high16 v10, 0x40e00000    # 7.0f

    const/4 v9, 0x1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mParentRelativeLayout:Landroid/widget/RelativeLayout;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/widget/RelativeLayout;->requestDisallowInterceptTouchEvent(Z)V

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    float-to-int v5, v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v0

    float-to-int v6, v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitSettings:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v9}, Landroid/view/ViewGroup;->requestDisallowInterceptTouchEvent(Z)V

    return v9

    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerHandle:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    sub-int v2, v5, v2

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget v0, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    sub-int v0, v6, v0

    invoke-static {v1, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->access$5(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;I)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerHandle:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitdBG:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerColorImage:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerdExitBtn:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerCurrentColor:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mXDelta:I
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)I

    move-result v7

    sub-int/2addr v5, v7

    iput v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mYDelta:I
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->access$7(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)I

    move-result v5

    sub-int v5, v6, v5

    iput v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    if-gez v5, :cond_0

    iput v8, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    :cond_0
    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    if-gez v5, :cond_1

    iput v8, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    :cond_1
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v5

    const/high16 v6, 0x42d40000    # 106.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v6

    const/high16 v7, 0x42180000    # 38.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    iget v7, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mParentRelativeLayout:Landroid/widget/RelativeLayout;
    invoke-static {v8}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)Landroid/widget/RelativeLayout;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v8

    sub-int/2addr v8, v5

    if-le v7, v8, :cond_2

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mParentRelativeLayout:Landroid/widget/RelativeLayout;
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)Landroid/widget/RelativeLayout;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v7

    sub-int v5, v7, v5

    iput v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    :cond_2
    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mParentRelativeLayout:Landroid/widget/RelativeLayout;
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)Landroid/widget/RelativeLayout;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v7

    sub-int/2addr v7, v6

    if-le v5, v7, :cond_3

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mParentRelativeLayout:Landroid/widget/RelativeLayout;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)Landroid/widget/RelativeLayout;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v5

    sub-int/2addr v5, v6

    iput v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    :cond_3
    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v6

    const/high16 v7, 0x41b00000    # 22.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    add-int/2addr v5, v6

    iput v5, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    add-int/lit8 v5, v5, 0x0

    iput v5, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v6

    const/high16 v7, 0x41c80000    # 25.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    add-int/2addr v5, v6

    iput v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v6

    invoke-virtual {v6, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    add-int/2addr v5, v6

    iput v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v6

    const/high16 v7, 0x42480000    # 50.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    add-int/2addr v5, v6

    iput v5, v4, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v6

    invoke-virtual {v6, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    add-int/2addr v5, v6

    iput v5, v4, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    move-result-object v6

    const/high16 v7, 0x429c0000    # 78.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    add-int/2addr v5, v6

    iput v5, v3, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iput v5, v3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerHandle:Landroid/view/View;

    invoke-virtual {v5, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitdBG:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerColorImage:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerCurrentColor:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout$2;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerdExitBtn:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
