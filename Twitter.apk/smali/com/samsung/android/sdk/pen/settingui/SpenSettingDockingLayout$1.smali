.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, 0x0

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    const/4 v1, 0x0

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, 0x0

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    const/4 v1, 0x0

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, 0x0

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    const/4 v1, 0x0

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
