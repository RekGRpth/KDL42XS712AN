.class Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;
.super Landroid/view/View;
.source "Twttr"


# static fields
.field protected static final TEXT_PREVIEW_WIDTH:I = 0x140


# instance fields
.field private isCheckUnderLine:Z

.field private mBold:I

.field private mColor:I

.field private mOnePoint:F

.field private final mPaint:Landroid/graphics/Paint;

.field private final mTextRect:Landroid/graphics/Rect;

.field private mTextSize:F

.field private mTextSkewValue:F

.field private mTypeFace:Landroid/graphics/Typeface;

.field private maxFontSize:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mColor:I

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextSize:F

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mBold:I

    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextSkewValue:F

    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->isCheckUnderLine:Z

    sget-object v0, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTypeFace:Landroid/graphics/Typeface;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mPaint:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextRect:Landroid/graphics/Rect;

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mOnePoint:F

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->maxFontSize:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 13

    const/4 v11, 0x1

    const/4 v1, 0x0

    const/4 v4, 0x0

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mColor:I

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextSize:F

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mBold:I

    const/high16 v0, -0x80000000

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextSkewValue:F

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->isCheckUnderLine:Z

    sget-object v0, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTypeFace:Landroid/graphics/Typeface;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mPaint:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextRect:Landroid/graphics/Rect;

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mOnePoint:F

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->maxFontSize:F

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mOnePoint:F

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getFontList()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    const-string/jumbo v5, "ABC abc"

    const-string/jumbo v2, ""

    int-to-double v6, p2

    const-wide v8, 0x4076800000000000L    # 360.0

    div-double/2addr v6, v8

    double-to-float v6, v6

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->getFontSizeList()Ljava/util/ArrayList;

    move-result-object v7

    move v3, v4

    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-lt v3, v8, :cond_0

    move v1, v4

    :goto_1
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_2

    :goto_2
    return-void

    :cond_0
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mPaint:Landroid/graphics/Paint;

    const/high16 v9, 0x41f00000    # 30.0f

    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v8, v11}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mPaint:Landroid/graphics/Paint;

    invoke-static {v3}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getTypeFace(I)Landroid/graphics/Typeface;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v9

    iget-object v10, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextRect:Landroid/graphics/Rect;

    invoke-virtual {v8, v5, v4, v9, v10}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextRect:Landroid/graphics/Rect;

    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v8

    int-to-float v8, v8

    cmpl-float v8, v8, v1

    if-lez v8, :cond_1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v2, v1

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move v12, v2

    move-object v2, v1

    move v1, v12

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v6

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v11}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mPaint:Landroid/graphics/Paint;

    invoke-static {v2}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getTypeFace(Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v3

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v5, v4, v3, v8}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    const/high16 v3, 0x43960000    # 300.0f

    iget v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mOnePoint:F

    mul-float/2addr v3, v8

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_3

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v6

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->maxFontSize:F

    goto/16 :goto_2

    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_1
.end method


# virtual methods
.method protected getFontSizeList()Ljava/util/ArrayList;
    .locals 3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/16 v0, 0x8

    :goto_0
    const/16 v2, 0x15

    if-lt v0, v2, :cond_0

    const/16 v0, 0x16

    :goto_1
    const/16 v2, 0x21

    if-lt v0, v2, :cond_1

    const/16 v0, 0x24

    :goto_2
    const/16 v2, 0x41

    if-lt v0, v2, :cond_2

    return-object v1

    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x2

    goto :goto_1

    :cond_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x4

    goto :goto_2
.end method

.method public getPreviewTextColor()I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mColor:I

    return v0
.end method

.method public getTextStyle()C
    .locals 3

    const/4 v0, 0x0

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mBold:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    int-to-char v0, v0

    :cond_0
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextSkewValue:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_1

    or-int/lit8 v0, v0, 0x2

    int-to-char v0, v0

    :cond_1
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->isCheckUnderLine:Z

    if-eqz v1, :cond_2

    or-int/lit8 v0, v0, 0x4

    int-to-char v0, v0

    :cond_2
    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5

    const/4 v4, 0x0

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    const-string/jumbo v0, "ABC abc"

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mColor:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextSize:F

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mPaint:Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mPaint:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTypeFace:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mBold:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setFlags(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextSkewValue:F

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSkewX(F)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mPaint:Landroid/graphics/Paint;

    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->isCheckUnderLine:Z

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setUnderlineText(Z)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextRect:Landroid/graphics/Rect;

    invoke-virtual {v1, v0, v4, v2, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextSize:F

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->maxFontSize:F

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_0

    const-string/jumbo v0, "AB ab"

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextRect:Landroid/graphics/Rect;

    invoke-virtual {v1, v0, v4, v2, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    return-void
.end method

.method public setPreviewBold(Z)V
    .locals 1

    if-eqz p1, :cond_0

    const/16 v0, 0x20

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mBold:I

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mBold:I

    goto :goto_0
.end method

.method public setPreviewTextColor(I)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mColor:I

    return-void
.end method

.method public setPreviewTextSize(F)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextSize:F

    return-void
.end method

.method public setPreviewTextSkewX(Z)V
    .locals 1

    if-eqz p1, :cond_0

    const v0, -0x41666666    # -0.3f

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextSkewValue:F

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTextSkewValue:F

    goto :goto_0
.end method

.method public setPreviewTypeface(Landroid/graphics/Typeface;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->mTypeFace:Landroid/graphics/Typeface;

    return-void
.end method

.method public setPreviewUnderLine(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->isCheckUnderLine:Z

    return-void
.end method
