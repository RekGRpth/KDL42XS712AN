.class Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private mFocusPathList:Ljava/util/ArrayList;

.field private mHeightList:Ljava/util/ArrayList;

.field private mNormalPathList:Ljava/util/ArrayList;

.field private mPressPathList:Ljava/util/ArrayList;

.field private mViewList:Ljava/util/ArrayList;

.field private mWidthList:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mViewList:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mNormalPathList:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mPressPathList:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mFocusPathList:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mWidthList:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mHeightList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mViewList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;Ljava/util/ArrayList;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mViewList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$10(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mHeightList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$11(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;Ljava/util/ArrayList;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mHeightList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mNormalPathList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;Ljava/util/ArrayList;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mNormalPathList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mPressPathList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;Ljava/util/ArrayList;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mPressPathList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mFocusPathList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$7(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;Ljava/util/ArrayList;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mFocusPathList:Ljava/util/ArrayList;

    return-void
.end method

.method static synthetic access$8(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mWidthList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$9(Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;Ljava/util/ArrayList;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader$StateImage;->mWidthList:Ljava/util/ArrayList;

    return-void
.end method
