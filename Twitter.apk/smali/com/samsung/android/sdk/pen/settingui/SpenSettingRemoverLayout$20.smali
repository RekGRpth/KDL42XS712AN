.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$20;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    const/4 v0, 0x0

    return v0

    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterType02:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v2, "snote_eraser_popup_text_press"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mCutterType02:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout$20;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v2, "snote_eraser_popup_text"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
