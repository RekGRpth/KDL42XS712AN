.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$NameDropdownSelectListner;


# instance fields
.field final synthetic this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSelectItem(I)V
    .locals 3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v1

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getFontList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->access$7(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;

    invoke-static {p1}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getTypeFace(I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->setPreviewTypeface(Landroid/graphics/Typeface;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->invalidate()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getTextSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getFontList()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v2

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCurrentFontName:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getFontName(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setTextSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v0

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontTypeButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->access$9(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)Landroid/widget/Button;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCurrentFontName:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$5;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v0

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontTypeButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->access$9(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)Landroid/widget/Button;

    move-result-object v0

    invoke-static {p1}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getTypeFace(I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    return-void
.end method
