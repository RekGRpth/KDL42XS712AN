.class public Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;
.super Landroid/widget/LinearLayout;
.source "Twttr"


# static fields
.field private static final BEAUTIFY_ADVANCE_CURSIVE_MAX_VALUE:I = 0xc

.field private static final BEAUTIFY_ADVANCE_DEFAULT_MAX_VALUE:I = 0x14

.field private static final BEAUTIFY_ADVANCE_DEFAULT_SETTING_VALUES:[[I

.field private static final BEAUTIFY_ADVANCE_DUMMY_MAX_VALUE:I = 0x14

.field private static final BEAUTIFY_ADVANCE_MODULATION_MAX_VALUE:I = 0x64

.field private static final BEAUTIFY_ADVANCE_SUSTENANCE_MAX_VALUE:I = 0x10

.field private static final BEAUTIFY_MAX_STYLEID_COUNT:I = 0x6

.field private static final BEAUTIFY_PARAMETER_CURSIVE:I = 0x2

.field private static final BEAUTIFY_PARAMETER_DUMMY:I = 0x4

.field private static final BEAUTIFY_PARAMETER_MODULATION:I = 0x6

.field private static final BEAUTIFY_PARAMETER_SLANT:I = 0x9

.field private static final BEAUTIFY_PARAMETER_STYLEID:I = 0x0

.field private static final BEAUTIFY_PARAMETER_SUSTENANCE:I = 0x3

.field private static final BEAUTIFY_PEN_NAME:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.Beautify"

.field private static final BEAUTIFY_RUNNING_HAND_S_INDEX:I = 0x5

.field private static final BEAUTIFY_STYLEID_CURSIVE_LM:I = 0xb

.field private static final BEAUTIFY_STYLEID_HUAI:I = 0xc

.field private static final BEAUTIFY_STYLEID_HUANG:I = 0x5

.field private static final BEAUTIFY_STYLEID_HUI:I = 0x6

.field private static final BEAUTIFY_STYLEID_RUNNING_HAND_S:I = 0x1

.field private static final BEAUTIFY_STYLEID_WANG:I = 0x3

.field protected static final BOTTOM_LAYOUT_HEIGHT:I = 0x10

.field private static final CHINESE_PEN_NAME:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

.field private static final IB_PEN_ADD_ID:I = 0xb82ff9

.field private static final MAGIC_PEN_NAME:Ljava/lang/String; = "com.samsung.android.sdk.pen.pen.preload.MagicPen"

.field protected static final MAXIMUM_PRESET_NUMBER:I = 0x24

.field private static final MAX_PARAMETER_INDEX:I = 0xa

.field protected static final PEN_SIZE_MAX:I = 0x63

.field private static final REP_DELAY:I = 0x14

.field protected static final SEEKBAR_LAYOUT_HEIGHT:I = 0x32

.field private static final TAG:Ljava/lang/String; = "settingui-settingPen"

.field public static final VIEW_MODE_COLOR:I = 0x6

.field public static final VIEW_MODE_EXTENSION:I = 0x2

.field public static final VIEW_MODE_EXTENSION_WITHOUT_PRESET:I = 0x3

.field public static final VIEW_MODE_EXTENSION_WITHOUT_PRESET_NO_RESIZE:I = 0x9

.field public static final VIEW_MODE_MINIMUM:I = 0x1

.field public static final VIEW_MODE_NORMAL:I = 0x0

.field public static final VIEW_MODE_PRESET:I = 0x7

.field public static final VIEW_MODE_SIZE:I = 0x5

.field public static final VIEW_MODE_TITLE:I = 0x8

.field public static final VIEW_MODE_TYPE:I = 0x4

.field private static final bottomExpandPath:Ljava/lang/String;

.field private static final bottomExpandPresetPath:Ljava/lang/String;

.field private static final bottomExpandPresetPressPath:Ljava/lang/String;

.field private static final bottomExpandPressPath:Ljava/lang/String;

.field private static final bottomHandlePath:Ljava/lang/String;

.field private static final exitPath:Ljava/lang/String;

.field private static final exitPressPath:Ljava/lang/String;

.field private static final exitfocusPath:Ljava/lang/String;

.field private static final grayBodyLeftPath:Ljava/lang/String;

.field private static final grayBodyRightPath:Ljava/lang/String;

.field private static final handelFocusPath:Ljava/lang/String;

.field private static final handelPath:Ljava/lang/String;

.field private static final handelPressPath:Ljava/lang/String;

.field private static isHighlightPenRemoved:Z

.field private static isMagicPenRemoved:Z

.field private static final leftBgDimPath:Ljava/lang/String;

.field private static final leftBgFocusPath:Ljava/lang/String;

.field private static final leftBgPath:Ljava/lang/String;

.field private static final leftBgPressPath:Ljava/lang/String;

.field private static final lightBodyLeftPath:Ljava/lang/String;

.field private static final lightBodyRightPath:Ljava/lang/String;

.field private static final lineDivider:Ljava/lang/String;

.field private static final linePath:Ljava/lang/String;

.field protected static mDefaultPath:Ljava/lang/String;

.field private static final mImagePath_snote_add:Ljava/lang/String;

.field private static final mImagePath_snote_add_dim:Ljava/lang/String;

.field private static final mImagePath_snote_add_press:Ljava/lang/String;

.field protected static mIsSwichTab:Z

.field protected static mPresetInfoList:Ljava/util/List;

.field protected static final mPreviewBgPath:Ljava/lang/String;

.field protected static mScale:F

.field private static final mSdkVersion:I

.field private static final minusBgDimPath:Ljava/lang/String;

.field private static final minusBgFocusPath:Ljava/lang/String;

.field private static final minusBgPath:Ljava/lang/String;

.field private static final minusBgPressPath:Ljava/lang/String;

.field private static final plusBgDimPath:Ljava/lang/String;

.field private static final plusBgFocusPath:Ljava/lang/String;

.field private static final plusBgPath:Ljava/lang/String;

.field private static final plusBgPressPath:Ljava/lang/String;

.field private static final popupBtnBgFocusPath:Ljava/lang/String;

.field private static final popupBtnBgNomalPath:Ljava/lang/String;

.field private static final popupBtnBgPressPath:Ljava/lang/String;

.field private static final presetAddFocusPath:Ljava/lang/String;

.field private static final presetAddPath:Ljava/lang/String;

.field private static final presetAddPressPath:Ljava/lang/String;

.field private static final previewAlphaPath:Ljava/lang/String;

.field private static final progressAlphaPath:Ljava/lang/String;

.field private static final progressBgPath:Ljava/lang/String;

.field private static final progressShadowPath:Ljava/lang/String;

.field private static final rightBgDimPath:Ljava/lang/String;

.field private static final rightBgFocusPath:Ljava/lang/String;

.field private static final rightBgPath:Ljava/lang/String;

.field private static final rightBgPressPath:Ljava/lang/String;

.field private static final switchCheckFalseBgPath:Ljava/lang/String;

.field private static final switchCheckTrueBgPath:Ljava/lang/String;

.field private static final switchThumbPath:Ljava/lang/String;

.field private static final tabBorderLinePath:Ljava/lang/String;

.field private static final tabLinePath:Ljava/lang/String;

.field private static final tabLineSelectPath:Ljava/lang/String;

.field private static final tabLineSelectedFocusPath:Ljava/lang/String;

.field private static final tabLineUnselectedFocusPath:Ljava/lang/String;

.field private static final titleCenterPath:Ljava/lang/String;

.field private static final titleLeftPath:Ljava/lang/String;

.field private static final titleRightPath:Ljava/lang/String;


# instance fields
.field protected final BEAUTIFY_ENABLE_BUTTON_HEIGHT:I

.field protected final BEAUTIFY_RESET_BUTTON_HEIGHT:I

.field protected final BODY_LAYOUT_HEIGHT:I

.field protected final BODY_LAYOUT_HEIGHT_BEAUTIFY_PEN:I

.field protected final BODY_LAYOUT_HEIGHT_BEAUTIFY_RAINBOW:I

.field protected final BODY_LAYOUT_HEIGHT_CHINESE:I

.field protected final BODY_LAYOUT_HEIGHT_MAGIC_PEN:I

.field protected final BODY_LAYOUT_HEIGHT_WITH_ALPHA:I

.field protected EXIT_BUTTON_HEIGHT:I

.field protected final EXIT_BUTTON_RAW_HEIGHT:I

.field protected final EXIT_BUTTON_RAW_WIDTH:I

.field protected EXIT_BUTTON_RIGHT_MARGIN:F

.field protected EXIT_BUTTON_TOP_MARGIN:F

.field protected EXIT_BUTTON_WIDTH:I

.field protected final GRADATION_HEIGHT:I

.field protected LINE_BUTTON_HEIGHT:F

.field protected final LINE_BUTTON_RAW_HEIGHT:I

.field protected final LINE_BUTTON_RAW_WIDTH:I

.field protected LINE_BUTTON_TOP_MARGIN:F

.field protected LINE_BUTTON_WIDTH:F

.field private final MAX_HEIGHT_FLAG:I

.field protected final TITLE_LAYOUT_HEIGHT:I

.field protected final TOTAL_LAYOUT_WIDTH:I

.field protected final TYPE_SELECTOR_LAYOUT_HEIGHT:I

.field private alphaDrawable:Landroid/graphics/drawable/Drawable;

.field private colorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

.field private currenMagicPenHeight:I

.field private deltaOfFirstTime:I

.field private deltaOfMiniMode:I

.field handler:Landroid/os/Handler;

.field private final horizontalScrollViewTouchListener:Landroid/view/View$OnTouchListener;

.field private isFirstTime:Z

.field private isMagicPenEnable:Z

.field private isMinimumMode:Z

.field private isPresetClicked:Z

.field private localPenTypeViewGroup:Landroid/widget/RelativeLayout;

.field protected mActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$ActionListener;

.field private mAdvancedSettingButton:Landroid/view/View;

.field private final mAdvancedSettingButtonListner:Landroid/view/View$OnClickListener;

.field private final mAdvancedSettingListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;

.field private mAdvancedSettingShow:Z

.field private mBeautifyAdvanceCursiveAutoDecrement:Z

.field private mBeautifyAdvanceCursiveAutoIncrement:Z

.field private final mBeautifyAdvanceCursiveChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private final mBeautifyAdvanceCursiveKeyListner:Landroid/view/View$OnKeyListener;

.field protected mBeautifyAdvanceCursiveMinusButton:Landroid/view/View;

.field private final mBeautifyAdvanceCursiveMinusButtonListener:Landroid/view/View$OnClickListener;

.field private final mBeautifyAdvanceCursiveMinusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

.field private final mBeautifyAdvanceCursiveMinusButtonTouchListener:Landroid/view/View$OnTouchListener;

.field protected mBeautifyAdvanceCursivePlusButton:Landroid/view/View;

.field private final mBeautifyAdvanceCursivePlusButtonListener:Landroid/view/View$OnClickListener;

.field private final mBeautifyAdvanceCursivePlusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

.field private final mBeautifyAdvanceCursivePlusButtonTouchListener:Landroid/view/View$OnTouchListener;

.field protected mBeautifyAdvanceCursiveSeekbar:Landroid/widget/SeekBar;

.field private mBeautifyAdvanceDummyAutoDecrement:Z

.field private mBeautifyAdvanceDummyAutoIncrement:Z

.field private final mBeautifyAdvanceDummyChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private final mBeautifyAdvanceDummyKeyListner:Landroid/view/View$OnKeyListener;

.field protected mBeautifyAdvanceDummyMinusButton:Landroid/view/View;

.field private final mBeautifyAdvanceDummyMinusButtonListener:Landroid/view/View$OnClickListener;

.field private final mBeautifyAdvanceDummyMinusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

.field private final mBeautifyAdvanceDummyMinusButtonTouchListener:Landroid/view/View$OnTouchListener;

.field protected mBeautifyAdvanceDummyPlusButton:Landroid/view/View;

.field private final mBeautifyAdvanceDummyPlusButtonListener:Landroid/view/View$OnClickListener;

.field private final mBeautifyAdvanceDummyPlusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

.field private final mBeautifyAdvanceDummyPlusButtonTouchListener:Landroid/view/View$OnTouchListener;

.field protected mBeautifyAdvanceDummySeekbar:Landroid/widget/SeekBar;

.field private mBeautifyAdvanceModulationAutoDecrement:Z

.field private mBeautifyAdvanceModulationAutoIncrement:Z

.field private final mBeautifyAdvanceModulationChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private final mBeautifyAdvanceModulationKeyListner:Landroid/view/View$OnKeyListener;

.field protected mBeautifyAdvanceModulationMinusButton:Landroid/view/View;

.field private final mBeautifyAdvanceModulationMinusButtonListener:Landroid/view/View$OnClickListener;

.field private final mBeautifyAdvanceModulationMinusButtonTouchListener:Landroid/view/View$OnTouchListener;

.field private final mBeautifyAdvanceModulationMinusButtonlongClickListener:Landroid/view/View$OnLongClickListener;

.field protected mBeautifyAdvanceModulationPlusButton:Landroid/view/View;

.field private final mBeautifyAdvanceModulationPlusButtonListener:Landroid/view/View$OnClickListener;

.field private final mBeautifyAdvanceModulationPlusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

.field private final mBeautifyAdvanceModulationPlusButtonTouchListener:Landroid/view/View$OnTouchListener;

.field protected mBeautifyAdvanceModulationSeekbar:Landroid/widget/SeekBar;

.field protected mBeautifyAdvanceResetButton:Landroid/widget/Button;

.field private final mBeautifyAdvanceResetButtonListener:Landroid/view/View$OnClickListener;

.field protected mBeautifyAdvanceSettingLayout:Landroid/view/View;

.field private mBeautifyAdvanceSustenanceAutoDecrement:Z

.field private mBeautifyAdvanceSustenanceAutoIncrement:Z

.field private final mBeautifyAdvanceSustenanceChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private final mBeautifyAdvanceSustenanceKeyListner:Landroid/view/View$OnKeyListener;

.field protected mBeautifyAdvanceSustenanceMinusButton:Landroid/view/View;

.field private final mBeautifyAdvanceSustenanceMinusButtonListener:Landroid/view/View$OnClickListener;

.field private final mBeautifyAdvanceSustenanceMinusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

.field private final mBeautifyAdvanceSustenanceMinusButtonTouchListener:Landroid/view/View$OnTouchListener;

.field protected mBeautifyAdvanceSustenancePlusButton:Landroid/view/View;

.field private final mBeautifyAdvanceSustenancePlusButtonListener:Landroid/view/View$OnClickListener;

.field private final mBeautifyAdvanceSustenancePlusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

.field private final mBeautifyAdvanceSustenancePlusButtonTouchListener:Landroid/view/View$OnTouchListener;

.field protected mBeautifyAdvanceSustenanceSeekbar:Landroid/widget/SeekBar;

.field protected mBeautifyCursiveTextView:Landroid/widget/TextView;

.field protected mBeautifyDummyTextView:Landroid/widget/TextView;

.field protected mBeautifyEnableLayout:Landroid/view/View;

.field protected mBeautifyEnableSwitchView:Landroid/widget/Switch;

.field protected mBeautifyEnableTextView:Landroid/widget/TextView;

.field private final mBeautifyEnablecheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field protected mBeautifyModulationTextView:Landroid/widget/TextView;

.field protected mBeautifyStyleBtnViews:Ljava/util/ArrayList;

.field protected mBeautifyStyleBtnsLayout:Landroid/view/View;

.field private final mBeautifyStyleBtnsListener:Landroid/view/View$OnClickListener;

.field protected mBeautifySustenanceTextView:Landroid/widget/TextView;

.field protected mBodyBg:Landroid/view/View;

.field protected mBodyLayout:Landroid/widget/RelativeLayout;

.field protected mBodyLayoutHeight:I

.field protected mBottomExtendBg:Landroid/widget/ImageView;

.field protected mBottomHandle:Landroid/widget/ImageView;

.field protected mBottomLayout:Landroid/view/View;

.field protected mCanvasLayout:Landroid/widget/RelativeLayout;

.field protected mCanvasSize:I

.field protected mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

.field protected mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

.field protected mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

.field private final mColorPickerColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView$onColorChangedListener;

.field protected mColorPickerColorImage:Landroid/view/View;

.field protected mColorPickerCurrentColor:Landroid/view/View;

.field private final mColorPickerCurrentColorListener:Landroid/view/View$OnClickListener;

.field protected mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

.field protected mColorPickerSettingExitButton:Landroid/view/View;

.field protected mColorSelectPickerLayout:Landroid/view/View;

.field protected mContext:Landroid/content/Context;

.field private mCount:I

.field private final mCurrentBeautifyAdvanceSettingValues:[[I

.field private mCurrentBeautifyStyle:I

.field protected mCurrentTopMargin:I

.field protected mCursiveSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

.field protected mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

.field protected mDummySeekbarColor:Landroid/graphics/drawable/GradientDrawable;

.field protected mExitButton:Landroid/view/View;

.field private final mExitButtonListener:Landroid/view/View$OnClickListener;

.field protected mExpandFlag:Z

.field protected mExpend:Z

.field mExpendBarHoverListener:Landroid/view/View$OnHoverListener;

.field mExpendBarListener:Landroid/view/View$OnTouchListener;

.field protected mFirstLongPress:Z

.field protected mGestureDetector:Landroid/view/GestureDetector;

.field private final mGestureListener:Landroid/view/GestureDetector$OnGestureListener;

.field private final mHandler:Landroid/os/Handler;

.field private mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

.field protected mIndicator:Landroid/widget/ImageView;

.field mIsMaxHeight:Z

.field protected mIsRotated:Z

.field protected mIsRotated2:Z

.field mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

.field protected mLeftMargin:I

.field protected mLine1Button:Landroid/view/View;

.field protected mLine2Button:Landroid/view/View;

.field protected mModulationSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

.field protected mMovableRect:Landroid/graphics/Rect;

.field protected mMoveSettingLayout:Z

.field protected mNeedCalculateMargin:Z

.field mNumberOfPenExist:I

.field protected mOldLocation:[I

.field protected mOldMovableRect:Landroid/graphics/Rect;

.field private final mOnClickPresetItemListener:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$OnClickPresetItemListener;

.field private final mOnColorChangedListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView$OnColorChangedListener;

.field private final mOnConsumedHoverListener:Landroid/view/View$OnHoverListener;

.field private final mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

.field private final mOnTouchListener:Landroid/view/View$OnTouchListener;

.field protected mOnsizeChange:Z

.field protected mOpacityMinusButton:Landroid/view/View;

.field protected mOpacityPlusButton:Landroid/view/View;

.field private final mPaletteBackButtonListener:Landroid/view/View$OnClickListener;

.field protected mPaletteBg:Landroid/view/View;

.field protected mPaletteLeftButton:Landroid/view/View;

.field private final mPaletteNextButtonListener:Landroid/view/View$OnClickListener;

.field protected mPaletteRightButton:Landroid/view/View;

.field protected mPaletteView:Landroid/view/View;

.field protected mPenAlpha:I

.field private mPenAlphaAutoDecrement:Z

.field private mPenAlphaAutoIncrement:Z

.field private final mPenAlphaChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private final mPenAlphaKeyListener:Landroid/view/View$OnKeyListener;

.field private final mPenAlphaMinusButtonKeyListener:Landroid/view/View$OnKeyListener;

.field private final mPenAlphaMinusButtonListener:Landroid/view/View$OnClickListener;

.field private final mPenAlphaMinusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

.field private final mPenAlphaMinusButtonTouchListener:Landroid/view/View$OnTouchListener;

.field private final mPenAlphaPlusButtonKeyListener:Landroid/view/View$OnKeyListener;

.field private final mPenAlphaPlusButtonListener:Landroid/view/View$OnClickListener;

.field private final mPenAlphaPlusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

.field private final mPenAlphaPlusButtonTouchListener:Landroid/view/View$OnTouchListener;

.field protected mPenAlphaPreview:Landroid/widget/RelativeLayout;

.field protected mPenAlphaSeekbar:Landroid/widget/SeekBar;

.field protected mPenAlphaSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

.field protected mPenAlphaSeekbarView:Landroid/view/View;

.field protected mPenAlphaTextView:Landroid/widget/TextView;

.field protected mPenButton:Landroid/widget/TextView;

.field protected mPenButtonLayout:Landroid/view/View;

.field protected mPenDataList:Ljava/util/List;

.field protected mPenLayout:Landroid/view/View;

.field protected mPenNameIndex:I

.field protected mPenPluginCount:I

.field protected mPenPluginInfoList:Ljava/util/ArrayList;

.field protected mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

.field protected mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

.field protected mPenSeekbarLayout:Landroid/view/View;

.field private mPenSharedPreferencesManager:Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;

.field private mPenSizeAutoDecrement:Z

.field private mPenSizeAutoIncrement:Z

.field private final mPenSizeChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private final mPenSizeKeyListener:Landroid/view/View$OnKeyListener;

.field protected mPenSizeMinusButton:Landroid/view/View;

.field private final mPenSizeMinusButtonKeyListener:Landroid/view/View$OnKeyListener;

.field private final mPenSizeMinusButtonListener:Landroid/view/View$OnClickListener;

.field private final mPenSizeMinusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

.field private final mPenSizeMinusButtonTouchListener:Landroid/view/View$OnTouchListener;

.field protected mPenSizePlusButton:Landroid/view/View;

.field private final mPenSizePlusButtonKeyListener:Landroid/view/View$OnKeyListener;

.field private final mPenSizePlusButtonListener:Landroid/view/View$OnClickListener;

.field private final mPenSizePlusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

.field private final mPenSizePlusButtonTouchListener:Landroid/view/View$OnTouchListener;

.field protected mPenSizeSeekbar:Landroid/widget/SeekBar;

.field protected mPenSizeSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

.field protected mPenSizeSeekbarView:Landroid/view/View;

.field protected mPenSizeTextView:Landroid/widget/TextView;

.field protected mPenTypeHorizontalScrollView:Landroid/view/ViewGroup;

.field protected mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

.field protected mPenTypeHorizontalScrollView3:Landroid/view/ViewGroup;

.field private final mPenTypeHoverListener:Landroid/view/View$OnHoverListener;

.field private final mPenTypeKeyListener:Landroid/view/View$OnKeyListener;

.field protected mPenTypeLayout:Landroid/view/ViewGroup;

.field private final mPenTypeListner:Landroid/view/View$OnClickListener;

.field private final mPenTypeTouchListener:Landroid/view/View$OnTouchListener;

.field protected mPenTypeView:Ljava/util/ArrayList;

.field protected mPickerView:Landroid/view/View;

.field protected mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

.field protected mPreCanvasFingerAction:I

.field protected mPreCanvasMouseAction:I

.field protected mPreCanvasPenAction:I

.field private final mPreSetAddButtonListner:Landroid/view/View$OnClickListener;

.field protected mPresetAddButton:Landroid/view/View;

.field protected mPresetButton:Landroid/widget/TextView;

.field protected mPresetDataList:Ljava/util/List;

.field protected mPresetGridView:Landroid/widget/GridView;

.field protected mPresetLayout:Landroid/widget/LinearLayout;

.field protected mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

.field protected mPresetListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$PresetListener;

.field protected mPresetTextView:Landroid/widget/TextView;

.field protected mPresetTypeLayout:Landroid/view/View;

.field protected mPreviewLayout:Landroid/view/View;

.field protected mPreviousSelectedPresetIndex:I

.field private mScrollTimer:Ljava/util/Timer;

.field protected mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

.field private final mScrollViewListner:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView$scrollChangedListener;

.field protected mScrollY:I

.field protected mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

.field protected mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

.field private mSupportBeautifyPen:Z

.field protected mSustenanceSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

.field private final mTabSelectListener:Landroid/view/View$OnClickListener;

.field protected mTablineLayout:Landroid/view/View;

.field protected mTitleLayout:Landroid/view/View;

.field private mTitleView:Landroid/widget/TextView;

.field protected mTopMargin:I

.field protected mTotalLeftMargin:I

.field protected mTotalTopMargin:I

.field protected mTypeSelectorLayout:Landroid/widget/LinearLayout;

.field protected mViewMode:I

.field protected mVisibilityListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$ViewListener;

.field protected mWindowHeight:I

.field protected mXDelta:I

.field protected mYDelta:I

.field private penTypeLayout:Landroid/widget/RelativeLayout;

.field private previousPenMagicSelected:Z

.field private final repeatUpdateHandler:Landroid/os/Handler;

.field protected requestLayoutDisable:Z


# direct methods
.method static constructor <clinit>()V
    .locals 10

    const/4 v9, 0x0

    const/4 v8, 0x5

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/high16 v0, 0x3f800000    # 1.0f

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScale:F

    const-string/jumbo v0, ""

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    sput-boolean v9, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isHighlightPenRemoved:Z

    sput-boolean v9, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isMagicPenRemoved:Z

    sput-boolean v9, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIsSwichTab:Z

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSdkVersion:I

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_title_left"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->titleLeftPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_title_center"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->titleCenterPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_title_right"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->titleRightPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_close"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->exitPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_close_focus"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->exitfocusPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_close_press"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->exitPressPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_add"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->presetAddPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_add_focus"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->presetAddFocusPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_add_press"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->presetAddPressPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_divider"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->linePath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_line"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->lineDivider:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_bg_left"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->lightBodyLeftPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_bg_right"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->lightBodyRightPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_bg02_left"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->grayBodyLeftPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_bg02_right"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->grayBodyRightPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_sub_tab_bg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->tabLinePath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_sub_tab_selected"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->tabLineSelectPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_sub_tab_selected_focus"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->tabLineSelectedFocusPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_sub_tab_bg_focus"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->tabLineUnselectedFocusPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_sub_tab_line"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->tabBorderLinePath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_preview_bg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPreviewBgPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_arrow_left_normal"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->leftBgPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_arrow_left_press"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->leftBgPressPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_arrow_left_focus"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->leftBgFocusPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_arrow_left_dim"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->leftBgDimPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_arrow_right_normal"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->rightBgPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_arrow_right_press"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->rightBgPressPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_arrow_right_focus"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->rightBgFocusPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_arrow_right_dim"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->rightBgDimPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_progress_btn_plus_normal"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->plusBgPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_progress_btn_plus_press"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->plusBgPressPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_progress_btn_plus_focus"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->plusBgFocusPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_progress_btn_plus_dim"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->plusBgDimPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_progress_btn_minus_normal"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->minusBgPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_progress_btn_minus_press"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->minusBgPressPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_progress_btn_minus_focus"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->minusBgFocusPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_progress_btn_minus_dim"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->minusBgDimPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "progress_handle_normal"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->handelPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "progress_handle_press"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->handelPressPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "progress_handle_focus"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->handelFocusPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "progress_bg"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->progressBgPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "progress_shadow"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->progressShadowPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "progress_bg_alpha"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->progressAlphaPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_pensetting_preview_alpha"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->previewAlphaPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "beautify_switch_off"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->switchCheckFalseBgPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "beautify_switch_on"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->switchCheckTrueBgPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "beautify_switch_thumb"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->switchThumbPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_btn_normal"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->popupBtnBgNomalPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_btn_focus"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->popupBtnBgFocusPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_btn_press"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->popupBtnBgPressPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_bg_expand"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_bg_expand_press"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPressPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_bg_expand_preset"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPresetPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_bg_expand_preset_press"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPresetPressPath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_popup_handler"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomHandlePath:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_add"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImagePath_snote_add:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_add_press"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImagePath_snote_add_press:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "snote_add_dim"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImagePath_snote_add_dim:Ljava/lang/String;

    const/4 v0, 0x6

    new-array v0, v0, [[I

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/16 v2, 0xb

    aput v2, v1, v9

    aput v6, v1, v5

    aput v8, v1, v6

    const/16 v2, 0x8

    aput v2, v1, v7

    const/4 v2, 0x4

    aput v5, v1, v2

    const/16 v2, 0xf

    aput v2, v1, v8

    const/4 v2, 0x6

    const/16 v3, 0x46

    aput v3, v1, v2

    const/4 v2, 0x7

    const/16 v3, 0xb

    aput v3, v1, v2

    const/16 v2, 0x9

    aput v5, v1, v2

    aput-object v1, v0, v9

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/16 v2, 0xc

    aput v2, v1, v9

    aput v6, v1, v5

    aput v8, v1, v6

    aput v5, v1, v7

    const/4 v2, 0x4

    aput v5, v1, v2

    const/16 v2, 0x12

    aput v2, v1, v8

    const/4 v2, 0x6

    const/16 v3, 0x46

    aput v3, v1, v2

    const/4 v2, 0x7

    const/16 v3, 0xd

    aput v3, v1, v2

    const/16 v2, 0x9

    aput v5, v1, v2

    aput-object v1, v0, v5

    const/16 v1, 0xa

    new-array v1, v1, [I

    aput v8, v1, v9

    aput v6, v1, v5

    aput v6, v1, v6

    aput v6, v1, v7

    const/4 v2, 0x4

    aput v5, v1, v2

    const/16 v2, 0x12

    aput v2, v1, v8

    const/4 v2, 0x6

    const/16 v3, 0x46

    aput v3, v1, v2

    const/4 v2, 0x7

    aput v8, v1, v2

    const/16 v2, 0x9

    aput v5, v1, v2

    aput-object v1, v0, v6

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v2, 0x6

    aput v2, v1, v9

    aput v6, v1, v5

    aput v7, v1, v6

    aput v7, v1, v7

    const/4 v2, 0x4

    aput v5, v1, v2

    const/16 v2, 0xc

    aput v2, v1, v8

    const/4 v2, 0x6

    const/16 v3, 0x46

    aput v3, v1, v2

    const/4 v2, 0x7

    const/4 v3, 0x6

    aput v3, v1, v2

    const/16 v2, 0x9

    aput v5, v1, v2

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const/16 v2, 0xa

    new-array v2, v2, [I

    aput v5, v2, v9

    aput v6, v2, v5

    aput v6, v2, v6

    const/16 v3, 0x8

    aput v3, v2, v7

    const/4 v3, 0x4

    aput v5, v2, v3

    const/16 v3, 0xf

    aput v3, v2, v8

    const/4 v3, 0x6

    const/16 v4, 0x46

    aput v4, v2, v3

    const/4 v3, 0x7

    aput v7, v2, v3

    const/16 v3, 0x9

    aput v5, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    aput v7, v1, v9

    aput v5, v1, v5

    aput v7, v1, v6

    aput v8, v1, v7

    const/4 v2, 0x4

    aput v5, v1, v2

    const/16 v2, 0xc

    aput v2, v1, v8

    const/4 v2, 0x6

    const/16 v3, 0x46

    aput v3, v1, v2

    const/4 v2, 0x7

    aput v5, v1, v2

    const/16 v2, 0x9

    aput v5, v1, v2

    aput-object v1, v0, v8

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->BEAUTIFY_ADVANCE_DEFAULT_SETTING_VALUES:[[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/widget/RelativeLayout;)V
    .locals 10

    const/4 v9, 0x5

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    const/16 v0, 0xff

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlpha:I

    const/16 v0, 0x438

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasSize:I

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mExpend:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mGestureDetector:Landroid/view/GestureDetector;

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->requestLayoutDisable:Z

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mExpandFlag:Z

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOnsizeChange:Z

    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCurrentTopMargin:I

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMoveSettingLayout:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;

    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$PresetListener;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$ActionListener;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mVisibilityListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$ViewListener;

    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mViewMode:I

    const/4 v0, -0x2

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayoutHeight:I

    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mWindowHeight:I

    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScrollY:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPreviousSelectedPresetIndex:I

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mNeedCalculateMargin:Z

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mFirstLongPress:Z

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIsRotated:Z

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIsRotated2:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->colorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    const v0, 0x1869f

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->MAX_HEIGHT_FLAG:I

    const v0, 0x1869f

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->currenMagicPenHeight:I

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->previousPenMagicSelected:Z

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isMagicPenEnable:Z

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isPresetClicked:Z

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIsMaxHeight:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->penTypeLayout:Landroid/widget/RelativeLayout;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->repeatUpdateHandler:Landroid/os/Handler;

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeAutoIncrement:Z

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeAutoDecrement:Z

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaAutoIncrement:Z

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaAutoDecrement:Z

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveAutoIncrement:Z

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveAutoDecrement:Z

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceAutoIncrement:Z

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceAutoDecrement:Z

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyAutoIncrement:Z

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyAutoDecrement:Z

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationAutoIncrement:Z

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationAutoDecrement:Z

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mAdvancedSettingListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;

    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCurrentBeautifyStyle:I

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSupportBeautifyPen:Z

    const/16 v0, 0x25b

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->BODY_LAYOUT_HEIGHT_BEAUTIFY_PEN:I

    const/16 v0, 0x1e5

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->BODY_LAYOUT_HEIGHT_BEAUTIFY_RAINBOW:I

    const/16 v0, 0xfa

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->TOTAL_LAYOUT_WIDTH:I

    const/16 v0, 0x111

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->BODY_LAYOUT_HEIGHT:I

    const/16 v0, 0x143

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->BODY_LAYOUT_HEIGHT_WITH_ALPHA:I

    const/16 v0, 0x138

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->BODY_LAYOUT_HEIGHT_CHINESE:I

    const/16 v0, 0xce

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->BODY_LAYOUT_HEIGHT_MAGIC_PEN:I

    const/16 v0, 0x30

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->GRADATION_HEIGHT:I

    const/16 v0, 0x29

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->TITLE_LAYOUT_HEIGHT:I

    const/16 v0, 0x1e

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->TYPE_SELECTOR_LAYOUT_HEIGHT:I

    const/16 v0, 0x26

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->EXIT_BUTTON_RAW_WIDTH:I

    const/16 v0, 0x22

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->EXIT_BUTTON_RAW_HEIGHT:I

    iput v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->LINE_BUTTON_RAW_WIDTH:I

    const/16 v0, 0x11

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->LINE_BUTTON_RAW_HEIGHT:I

    const/16 v0, 0x28

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->BEAUTIFY_RESET_BUTTON_HEIGHT:I

    const/16 v0, 0x27

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->BEAUTIFY_ENABLE_BUTTON_HEIGHT:I

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$2;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOnClickPresetItemListener:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$OnClickPresetItemListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$3;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOnColorChangedListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView$OnColorChangedListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$4;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPickerColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView$onColorChangedListener;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->handler:Landroid/os/Handler;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$5;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$6;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$6;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$7;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$7;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizePlusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$8;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$8;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeTouchListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$9;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$9;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeKeyListener:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeHoverListener:Landroid/view/View$OnHoverListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$11;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$11;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizePlusButtonTouchListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$12;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$12;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizePlusButtonKeyListener:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$13;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$13;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeMinusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$14;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$14;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeMinusButtonTouchListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$15;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$15;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeMinusButtonKeyListener:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$16;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$16;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaPlusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$17;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$17;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaPlusButtonTouchListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$18;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$18;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaPlusButtonKeyListener:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$19;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$19;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaMinusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaMinusButtonTouchListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaMinusButtonKeyListener:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$22;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$22;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizePlusButtonListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$23;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$23;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeMinusButtonListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$24;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$24;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaPlusButtonListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$25;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$25;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaMinusButtonListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$26;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$26;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPaletteNextButtonListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$27;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$27;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPaletteBackButtonListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$28;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$28;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeListner:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$29;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$29;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPickerCurrentColorListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$30;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$30;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mExitButtonListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$31;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$31;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$32;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$32;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOnConsumedHoverListener:Landroid/view/View$OnHoverListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$33;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$33;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$34;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$34;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mAdvancedSettingButtonListner:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$35;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$35;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPreSetAddButtonListner:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$36;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$36;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mTabSelectListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$37;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$37;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mExpendBarHoverListener:Landroid/view/View$OnHoverListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$38;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$38;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mExpendBarListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mGestureListener:Landroid/view/GestureDetector$OnGestureListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$40;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$40;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyEnablecheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$41;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$41;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyStyleBtnsListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$42;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$42;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$43;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$43;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$44;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$44;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$45;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$45;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$46;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$46;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursivePlusButtonListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$47;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$47;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveMinusButtonListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$48;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$48;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenancePlusButtonListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$49;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$49;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceMinusButtonListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$50;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$50;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyPlusButtonListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$51;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$51;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyMinusButtonListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$52;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$52;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationPlusButtonListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$53;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$53;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationMinusButtonListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$54;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$54;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceResetButtonListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$55;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$55;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeKeyListener:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$56;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$56;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaKeyListener:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$57;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$57;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursivePlusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$58;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$58;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenancePlusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$59;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$59;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyPlusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$60;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$60;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationPlusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$61;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$61;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveMinusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$62;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$62;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceMinusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$63;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$63;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyMinusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$64;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$64;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationMinusButtonlongClickListener:Landroid/view/View$OnLongClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$65;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$65;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursivePlusButtonTouchListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$66;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$66;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenancePlusButtonTouchListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$67;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$67;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyPlusButtonTouchListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$68;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$68;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationPlusButtonTouchListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$69;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$69;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveMinusButtonTouchListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$70;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$70;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceMinusButtonTouchListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$71;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$71;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyMinusButtonTouchListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$72;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$72;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationMinusButtonTouchListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$73;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$73;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveKeyListner:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$74;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$74;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceKeyListner:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$75;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$75;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyKeyListner:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$76;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$76;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationKeyListner:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$77;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$77;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->horizontalScrollViewTouchListener:Landroid/view/View$OnTouchListener;

    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfMiniMode:I

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isMinimumMode:Z

    const/16 v0, 0x34

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfFirstTime:I

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isFirstTime:Z

    const/4 v0, 0x6

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mNumberOfPenExist:I

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mHandler:Landroid/os/Handler;

    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCount:I

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$78;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$78;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$79;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$79;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScrollViewListner:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView$scrollChangedListener;

    const/4 v0, 0x6

    new-array v0, v0, [[I

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/16 v2, 0xb

    aput v2, v1, v5

    aput v7, v1, v6

    aput v9, v1, v7

    const/16 v2, 0x8

    aput v2, v1, v8

    const/4 v2, 0x4

    aput v6, v1, v2

    const/16 v2, 0xf

    aput v2, v1, v9

    const/4 v2, 0x6

    const/16 v3, 0x46

    aput v3, v1, v2

    const/4 v2, 0x7

    const/16 v3, 0xb

    aput v3, v1, v2

    const/16 v2, 0x9

    aput v6, v1, v2

    aput-object v1, v0, v5

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/16 v2, 0xc

    aput v2, v1, v5

    aput v7, v1, v6

    aput v9, v1, v7

    aput v6, v1, v8

    const/4 v2, 0x4

    aput v6, v1, v2

    const/16 v2, 0x12

    aput v2, v1, v9

    const/4 v2, 0x6

    const/16 v3, 0x46

    aput v3, v1, v2

    const/4 v2, 0x7

    const/16 v3, 0xd

    aput v3, v1, v2

    const/16 v2, 0x9

    aput v6, v1, v2

    aput-object v1, v0, v6

    const/16 v1, 0xa

    new-array v1, v1, [I

    aput v9, v1, v5

    aput v7, v1, v6

    aput v7, v1, v7

    aput v7, v1, v8

    const/4 v2, 0x4

    aput v6, v1, v2

    const/16 v2, 0x12

    aput v2, v1, v9

    const/4 v2, 0x6

    const/16 v3, 0x46

    aput v3, v1, v2

    const/4 v2, 0x7

    aput v9, v1, v2

    const/16 v2, 0x9

    aput v6, v1, v2

    aput-object v1, v0, v7

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v2, 0x6

    aput v2, v1, v5

    aput v7, v1, v6

    aput v8, v1, v7

    aput v8, v1, v8

    const/4 v2, 0x4

    aput v6, v1, v2

    const/16 v2, 0xc

    aput v2, v1, v9

    const/4 v2, 0x6

    const/16 v3, 0x46

    aput v3, v1, v2

    const/4 v2, 0x7

    const/4 v3, 0x6

    aput v3, v1, v2

    const/16 v2, 0x9

    aput v6, v1, v2

    aput-object v1, v0, v8

    const/4 v1, 0x4

    const/16 v2, 0xa

    new-array v2, v2, [I

    aput v6, v2, v5

    aput v7, v2, v6

    aput v7, v2, v7

    const/16 v3, 0x8

    aput v3, v2, v8

    const/4 v3, 0x4

    aput v6, v2, v3

    const/16 v3, 0xf

    aput v3, v2, v9

    const/4 v3, 0x6

    const/16 v4, 0x46

    aput v4, v2, v3

    const/4 v3, 0x7

    aput v8, v2, v3

    const/16 v3, 0x9

    aput v6, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    aput v8, v1, v5

    aput v6, v1, v6

    aput v8, v1, v7

    aput v9, v1, v8

    const/4 v2, 0x4

    aput v6, v1, v2

    const/16 v2, 0xc

    aput v2, v1, v9

    const/4 v2, 0x6

    const/16 v3, 0x46

    aput v3, v1, v2

    const/4 v2, 0x7

    aput v6, v1, v2

    const/16 v2, 0x9

    aput v6, v1, v2

    aput-object v1, v0, v9

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCurrentBeautifyAdvanceSettingValues:[[I

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->requestLayoutDisable:Z

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScale:F

    invoke-direct {v0, p1, p2, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    const/4 v0, -0x2

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayoutHeight:I

    invoke-static {p1}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->initPenPlugin(Landroid/content/Context;)V

    :cond_0
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSupportBeautifyPen:Z

    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->initButtonValue()V

    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->initView(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setListener()V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMovableRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOldMovableRect:Landroid/graphics/Rect;

    new-array v0, v7, [I

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOldLocation:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/widget/RelativeLayout;F)V
    .locals 10

    const/4 v9, 0x5

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    const/16 v0, 0xff

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlpha:I

    const/16 v0, 0x438

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasSize:I

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mExpend:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mGestureDetector:Landroid/view/GestureDetector;

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->requestLayoutDisable:Z

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mExpandFlag:Z

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOnsizeChange:Z

    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCurrentTopMargin:I

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMoveSettingLayout:Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;

    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$PresetListener;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$ActionListener;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mVisibilityListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$ViewListener;

    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mViewMode:I

    const/4 v0, -0x2

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayoutHeight:I

    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mWindowHeight:I

    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScrollY:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPreviousSelectedPresetIndex:I

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mNeedCalculateMargin:Z

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mFirstLongPress:Z

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIsRotated:Z

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIsRotated2:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->colorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    const v0, 0x1869f

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->MAX_HEIGHT_FLAG:I

    const v0, 0x1869f

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->currenMagicPenHeight:I

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->previousPenMagicSelected:Z

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isMagicPenEnable:Z

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isPresetClicked:Z

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIsMaxHeight:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->penTypeLayout:Landroid/widget/RelativeLayout;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->repeatUpdateHandler:Landroid/os/Handler;

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeAutoIncrement:Z

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeAutoDecrement:Z

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaAutoIncrement:Z

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaAutoDecrement:Z

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveAutoIncrement:Z

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveAutoDecrement:Z

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceAutoIncrement:Z

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceAutoDecrement:Z

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyAutoIncrement:Z

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyAutoDecrement:Z

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationAutoIncrement:Z

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationAutoDecrement:Z

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mAdvancedSettingListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;

    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCurrentBeautifyStyle:I

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSupportBeautifyPen:Z

    const/16 v0, 0x25b

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->BODY_LAYOUT_HEIGHT_BEAUTIFY_PEN:I

    const/16 v0, 0x1e5

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->BODY_LAYOUT_HEIGHT_BEAUTIFY_RAINBOW:I

    const/16 v0, 0xfa

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->TOTAL_LAYOUT_WIDTH:I

    const/16 v0, 0x111

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->BODY_LAYOUT_HEIGHT:I

    const/16 v0, 0x143

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->BODY_LAYOUT_HEIGHT_WITH_ALPHA:I

    const/16 v0, 0x138

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->BODY_LAYOUT_HEIGHT_CHINESE:I

    const/16 v0, 0xce

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->BODY_LAYOUT_HEIGHT_MAGIC_PEN:I

    const/16 v0, 0x30

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->GRADATION_HEIGHT:I

    const/16 v0, 0x29

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->TITLE_LAYOUT_HEIGHT:I

    const/16 v0, 0x1e

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->TYPE_SELECTOR_LAYOUT_HEIGHT:I

    const/16 v0, 0x26

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->EXIT_BUTTON_RAW_WIDTH:I

    const/16 v0, 0x22

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->EXIT_BUTTON_RAW_HEIGHT:I

    iput v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->LINE_BUTTON_RAW_WIDTH:I

    const/16 v0, 0x11

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->LINE_BUTTON_RAW_HEIGHT:I

    const/16 v0, 0x28

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->BEAUTIFY_RESET_BUTTON_HEIGHT:I

    const/16 v0, 0x27

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->BEAUTIFY_ENABLE_BUTTON_HEIGHT:I

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$2;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOnClickPresetItemListener:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$OnClickPresetItemListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$3;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOnColorChangedListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView$OnColorChangedListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$4;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPickerColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView$onColorChangedListener;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->handler:Landroid/os/Handler;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$5;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$6;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$6;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$7;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$7;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizePlusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$8;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$8;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeTouchListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$9;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$9;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeKeyListener:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$10;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeHoverListener:Landroid/view/View$OnHoverListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$11;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$11;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizePlusButtonTouchListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$12;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$12;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizePlusButtonKeyListener:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$13;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$13;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeMinusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$14;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$14;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeMinusButtonTouchListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$15;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$15;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeMinusButtonKeyListener:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$16;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$16;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaPlusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$17;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$17;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaPlusButtonTouchListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$18;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$18;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaPlusButtonKeyListener:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$19;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$19;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaMinusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$20;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaMinusButtonTouchListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$21;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaMinusButtonKeyListener:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$22;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$22;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizePlusButtonListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$23;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$23;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeMinusButtonListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$24;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$24;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaPlusButtonListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$25;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$25;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaMinusButtonListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$26;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$26;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPaletteNextButtonListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$27;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$27;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPaletteBackButtonListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$28;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$28;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeListner:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$29;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$29;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPickerCurrentColorListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$30;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$30;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mExitButtonListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$31;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$31;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$32;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$32;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOnConsumedHoverListener:Landroid/view/View$OnHoverListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$33;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$33;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$34;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$34;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mAdvancedSettingButtonListner:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$35;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$35;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPreSetAddButtonListner:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$36;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$36;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mTabSelectListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$37;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$37;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mExpendBarHoverListener:Landroid/view/View$OnHoverListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$38;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$38;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mExpendBarListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$39;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mGestureListener:Landroid/view/GestureDetector$OnGestureListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$40;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$40;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyEnablecheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$41;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$41;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyStyleBtnsListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$42;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$42;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$43;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$43;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$44;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$44;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$45;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$45;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$46;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$46;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursivePlusButtonListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$47;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$47;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveMinusButtonListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$48;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$48;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenancePlusButtonListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$49;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$49;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceMinusButtonListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$50;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$50;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyPlusButtonListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$51;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$51;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyMinusButtonListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$52;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$52;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationPlusButtonListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$53;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$53;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationMinusButtonListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$54;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$54;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceResetButtonListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$55;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$55;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeKeyListener:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$56;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$56;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaKeyListener:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$57;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$57;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursivePlusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$58;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$58;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenancePlusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$59;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$59;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyPlusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$60;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$60;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationPlusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$61;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$61;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveMinusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$62;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$62;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceMinusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$63;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$63;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyMinusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$64;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$64;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationMinusButtonlongClickListener:Landroid/view/View$OnLongClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$65;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$65;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursivePlusButtonTouchListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$66;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$66;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenancePlusButtonTouchListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$67;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$67;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyPlusButtonTouchListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$68;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$68;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationPlusButtonTouchListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$69;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$69;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveMinusButtonTouchListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$70;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$70;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceMinusButtonTouchListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$71;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$71;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyMinusButtonTouchListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$72;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$72;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationMinusButtonTouchListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$73;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$73;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveKeyListner:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$74;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$74;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceKeyListner:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$75;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$75;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyKeyListner:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$76;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$76;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationKeyListner:Landroid/view/View$OnKeyListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$77;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$77;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->horizontalScrollViewTouchListener:Landroid/view/View$OnTouchListener;

    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfMiniMode:I

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isMinimumMode:Z

    const/16 v0, 0x34

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfFirstTime:I

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isFirstTime:Z

    const/4 v0, 0x6

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mNumberOfPenExist:I

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mHandler:Landroid/os/Handler;

    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCount:I

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$78;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$78;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$79;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$79;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScrollViewListner:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView$scrollChangedListener;

    const/4 v0, 0x6

    new-array v0, v0, [[I

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/16 v2, 0xb

    aput v2, v1, v5

    aput v7, v1, v6

    aput v9, v1, v7

    const/16 v2, 0x8

    aput v2, v1, v8

    const/4 v2, 0x4

    aput v6, v1, v2

    const/16 v2, 0xf

    aput v2, v1, v9

    const/4 v2, 0x6

    const/16 v3, 0x46

    aput v3, v1, v2

    const/4 v2, 0x7

    const/16 v3, 0xb

    aput v3, v1, v2

    const/16 v2, 0x9

    aput v6, v1, v2

    aput-object v1, v0, v5

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/16 v2, 0xc

    aput v2, v1, v5

    aput v7, v1, v6

    aput v9, v1, v7

    aput v6, v1, v8

    const/4 v2, 0x4

    aput v6, v1, v2

    const/16 v2, 0x12

    aput v2, v1, v9

    const/4 v2, 0x6

    const/16 v3, 0x46

    aput v3, v1, v2

    const/4 v2, 0x7

    const/16 v3, 0xd

    aput v3, v1, v2

    const/16 v2, 0x9

    aput v6, v1, v2

    aput-object v1, v0, v6

    const/16 v1, 0xa

    new-array v1, v1, [I

    aput v9, v1, v5

    aput v7, v1, v6

    aput v7, v1, v7

    aput v7, v1, v8

    const/4 v2, 0x4

    aput v6, v1, v2

    const/16 v2, 0x12

    aput v2, v1, v9

    const/4 v2, 0x6

    const/16 v3, 0x46

    aput v3, v1, v2

    const/4 v2, 0x7

    aput v9, v1, v2

    const/16 v2, 0x9

    aput v6, v1, v2

    aput-object v1, v0, v7

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v2, 0x6

    aput v2, v1, v5

    aput v7, v1, v6

    aput v8, v1, v7

    aput v8, v1, v8

    const/4 v2, 0x4

    aput v6, v1, v2

    const/16 v2, 0xc

    aput v2, v1, v9

    const/4 v2, 0x6

    const/16 v3, 0x46

    aput v3, v1, v2

    const/4 v2, 0x7

    const/4 v3, 0x6

    aput v3, v1, v2

    const/16 v2, 0x9

    aput v6, v1, v2

    aput-object v1, v0, v8

    const/4 v1, 0x4

    const/16 v2, 0xa

    new-array v2, v2, [I

    aput v6, v2, v5

    aput v7, v2, v6

    aput v7, v2, v7

    const/16 v3, 0x8

    aput v3, v2, v8

    const/4 v3, 0x4

    aput v6, v2, v3

    const/16 v3, 0xf

    aput v3, v2, v9

    const/4 v3, 0x6

    const/16 v4, 0x46

    aput v4, v2, v3

    const/4 v3, 0x7

    aput v8, v2, v3

    const/16 v3, 0x9

    aput v6, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    aput v8, v1, v5

    aput v6, v1, v6

    aput v8, v1, v7

    aput v9, v1, v8

    const/4 v2, 0x4

    aput v6, v1, v2

    const/16 v2, 0xc

    aput v2, v1, v9

    const/4 v2, 0x6

    const/16 v3, 0x46

    aput v3, v1, v2

    const/4 v2, 0x7

    aput v6, v1, v2

    const/16 v2, 0x9

    aput v6, v1, v2

    aput-object v1, v0, v9

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCurrentBeautifyAdvanceSettingValues:[[I

    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->requestLayoutDisable:Z

    sput p4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScale:F

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-direct {v0, p1, p2, p4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    const/4 v0, -0x2

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayoutHeight:I

    invoke-static {p1}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->initPenPlugin(Landroid/content/Context;)V

    :cond_0
    iput-boolean v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSupportBeautifyPen:Z

    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->initButtonValue()V

    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->initView(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setListener()V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMovableRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOldMovableRect:Landroid/graphics/Rect;

    new-array v0, v7, [I

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOldLocation:[I

    return-void
.end method

.method private ColorPickerSettingInit()V
    .locals 6

    const/4 v4, 0x0

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    sget v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScale:F

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;-><init>(Landroid/content/Context;Landroid/widget/RelativeLayout;FII)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerdExitBtn:Landroid/view/View;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPickerSettingExitButton:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPickerSettingExitButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mExitButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerColorImage:Landroid/view/View;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPickerColorImage:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mColorPickerCurrentColor:Landroid/view/View;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPickerCurrentColor:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPickerCurrentColor:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPickerCurrentColorListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method private PaletteView()Landroid/view/ViewGroup;
    .locals 11

    const/high16 v10, 0x41900000    # 18.0f

    const/high16 v4, 0x41000000    # 8.0f

    const/high16 v9, 0x425c0000    # 55.0f

    const/16 v6, 0x37

    const/16 v5, 0x12

    new-instance v7, Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v7, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x40c00000    # 6.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPaletteRightButton:Landroid/view/View;

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPaletteRightButton:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPaletteRightButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_next"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSdkVersion:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPaletteRightButton:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->rightBgPath:Ljava/lang/String;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->rightBgPressPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->rightBgFocusPath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->rightBgDimPath:Ljava/lang/String;

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPaletteLeftButton:Landroid/view/View;

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPaletteLeftButton:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPaletteLeftButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_back"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSdkVersion:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_1

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPaletteLeftButton:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->leftBgPath:Ljava/lang/String;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->leftBgPressPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->leftBgFocusPath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->leftBgDimPath:Ljava/lang/String;

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->colorPaletteView()Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPaletteLeftButton:Landroid/view/View;

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPaletteRightButton:Landroid/view/View;

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    return-object v7

    :cond_0
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPaletteRightButton:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->rightBgPath:Ljava/lang/String;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->rightBgPressPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->rightBgFocusPath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->rightBgDimPath:Ljava/lang/String;

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_1
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPaletteLeftButton:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->leftBgPath:Ljava/lang/String;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->leftBgPressPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->leftBgFocusPath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->leftBgDimPath:Ljava/lang/String;

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method private PickerView()Landroid/view/ViewGroup;
    .locals 6

    const/high16 v5, 0x41e00000    # 28.0f

    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x42400000    # 48.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42840000    # 66.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->colorGradationView()Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->presetDisplay()V

    return-void
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Ljava/lang/String;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isBeautifyPen(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$10(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isMagicPenEnable:Z

    return v0
.end method

.method static synthetic access$11(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/graphics/drawable/Drawable;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->alphaDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$12(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaAutoIncrement:Z

    return v0
.end method

.method static synthetic access$13(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaAutoIncrement:Z

    return-void
.end method

.method static synthetic access$14(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaAutoDecrement:Z

    return v0
.end method

.method static synthetic access$15(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaAutoDecrement:Z

    return-void
.end method

.method static synthetic access$16(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeAutoIncrement:Z

    return v0
.end method

.method static synthetic access$17(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeAutoIncrement:Z

    return-void
.end method

.method static synthetic access$18(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeAutoDecrement:Z

    return v0
.end method

.method static synthetic access$19(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeAutoDecrement:Z

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isMagicPenEnable:Z

    return-void
.end method

.method static synthetic access$20(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->repeatUpdateHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$21(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isPresetClicked:Z

    return v0
.end method

.method static synthetic access$22(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/widget/RelativeLayout;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->localPenTypeViewGroup:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method static synthetic access$23(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->penSelectIndex(I)V

    return-void
.end method

.method static synthetic access$24(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->colorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    return-object v0
.end method

.method static synthetic access$25(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mAdvancedSettingShow:Z

    return-void
.end method

.method static synthetic access$26(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mAdvancedSettingShow:Z

    return v0
.end method

.method static synthetic access$27(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mAdvancedSettingListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;

    return-object v0
.end method

.method static synthetic access$28(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCurrentBeautifyStyle:I

    return v0
.end method

.method static synthetic access$29(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setButtonFocus(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isMinimumMode:Z

    return v0
.end method

.method static synthetic access$30(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->showBeautifyEnableLayout(Z)V

    return-void
.end method

.method static synthetic access$31(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->showBeautifyStyleBtnsLayout(Z)V

    return-void
.end method

.method static synthetic access$32(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isPresetClicked:Z

    return-void
.end method

.method static synthetic access$33(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setColorSelectorViewForBeautifyPen()V

    return-void
.end method

.method static synthetic access$34(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    return-object v0
.end method

.method static synthetic access$35()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$36()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPressPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$37()I
    .locals 1

    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSdkVersion:I

    return v0
.end method

.method static synthetic access$38(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Ljava/lang/String;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isChinesePen(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$39(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->showBeautifySettingViews(Z)V

    return-void
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isMinimumMode:Z

    return-void
.end method

.method static synthetic access$40(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->currenMagicPenHeight:I

    return v0
.end method

.method static synthetic access$41(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setMagicPenMode(I)V

    return-void
.end method

.method static synthetic access$42()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPresetPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$43()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPresetPressPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$44(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/graphics/Rect;
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$45(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->updateBeautifySettingData()V

    return-void
.end method

.method static synthetic access$46(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->beautifyUpdateSettingUI(Z)V

    return-void
.end method

.method static synthetic access$47(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;I)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCurrentBeautifyStyle:I

    return-void
.end method

.method static synthetic access$48(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->resetBeautifyAdvanceDataAndUpdateSeekBarUi(I)V

    return-void
.end method

.method static synthetic access$49(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setBeautifyAdvancedDataToPlugin(II)V

    return-void
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;I)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfMiniMode:I

    return-void
.end method

.method static synthetic access$50(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveAutoIncrement:Z

    return v0
.end method

.method static synthetic access$51(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveAutoIncrement:Z

    return-void
.end method

.method static synthetic access$52(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveAutoDecrement:Z

    return v0
.end method

.method static synthetic access$53(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveAutoDecrement:Z

    return-void
.end method

.method static synthetic access$54(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceAutoIncrement:Z

    return v0
.end method

.method static synthetic access$55(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceAutoIncrement:Z

    return-void
.end method

.method static synthetic access$56(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceAutoDecrement:Z

    return v0
.end method

.method static synthetic access$57(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceAutoDecrement:Z

    return-void
.end method

.method static synthetic access$58(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyAutoIncrement:Z

    return v0
.end method

.method static synthetic access$59(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyAutoIncrement:Z

    return-void
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V

    return-void
.end method

.method static synthetic access$60(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyAutoDecrement:Z

    return v0
.end method

.method static synthetic access$61(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyAutoDecrement:Z

    return-void
.end method

.method static synthetic access$62(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationAutoIncrement:Z

    return v0
.end method

.method static synthetic access$63(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationAutoIncrement:Z

    return-void
.end method

.method static synthetic access$64(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationAutoDecrement:Z

    return v0
.end method

.method static synthetic access$65(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationAutoDecrement:Z

    return-void
.end method

.method static synthetic access$66(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;IFF)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->playScrollAnimation(IFF)V

    return-void
.end method

.method static synthetic access$67(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->rotatePosition()V

    return-void
.end method

.method static synthetic access$68(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->checkPosition()V

    return-void
.end method

.method static synthetic access$69(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$7(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setBeautifyAdvanceSeekbarColor(I)V

    return-void
.end method

.method static synthetic access$70(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCount:I

    return v0
.end method

.method static synthetic access$71(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;I)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCount:I

    return-void
.end method

.method static synthetic access$72(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Ljava/util/Timer;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScrollTimer:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic access$73(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Ljava/util/Timer;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScrollTimer:Ljava/util/Timer;

    return-void
.end method

.method static synthetic access$8(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isFirstTime:Z

    return v0
.end method

.method static synthetic access$9(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isFirstTime:Z

    return-void
.end method

.method private advancedSettingButton()Landroid/view/View;
    .locals 6

    const/4 v2, -0x2

    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setFocusable(Z)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImagePath_snote_add:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImagePath_snote_add_press:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImagePath_snote_add_press:Ljava/lang/String;

    sget-object v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImagePath_snote_add_dim:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    return-object v0
.end method

.method private beautifyAdvanceCursive()Landroid/view/View;
    .locals 12

    const/4 v4, 0x0

    const/16 v5, 0x19

    const/4 v11, -0x1

    const/high16 v10, 0x41c80000    # 25.0f

    const/4 v9, 0x1

    new-instance v7, Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v7, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v8, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v8, v11, v11}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v7, v8}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v7, v9}, Landroid/widget/LinearLayout;->setOrientation(I)V

    invoke-virtual {v7, v9}, Landroid/widget/LinearLayout;->setGravity(I)V

    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyCursiveTextView:Landroid/widget/TextView;

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v11, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x40a00000    # 5.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyCursiveTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyCursiveTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41400000    # 12.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v4, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyCursiveTextView:Landroid/widget/TextView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyCursiveTextView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v9}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyCursiveTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x420c0000    # 35.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x420c0000    # 35.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-virtual {v0, v1, v4, v2, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyCursiveTextView:Landroid/widget/TextView;

    const/16 v1, 0x56

    const/16 v2, 0x57

    const/16 v3, 0x5b

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyCursiveTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_cursive"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursivePlusButton:Landroid/view/View;

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-boolean v9, v0, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursivePlusButton:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursivePlusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_plus"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursivePlusButton:Landroid/view/View;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->plusBgPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->plusBgPressPath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->plusBgFocusPath:Ljava/lang/String;

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveMinusButton:Landroid/view/View;

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-boolean v9, v0, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveMinusButton:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveMinusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_minus"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveMinusButton:Landroid/view/View;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->minusBgPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->minusBgPressPath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->minusBgFocusPath:Ljava/lang/String;

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {v1, v11, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x40400000    # 3.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getBeautifyAdvanceMaxValue(I)I

    move-result v1

    new-instance v2, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v2}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCursiveSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCursiveSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->beautifyAdvanceSeekBar(Landroid/graphics/drawable/GradientDrawable;)Landroid/widget/SeekBar;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveSeekbar:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v2, v1}, Landroid/widget/SeekBar;->setMax(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveMinusButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursivePlusButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyCursiveTextView:Landroid/widget/TextView;

    invoke-virtual {v7, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v7, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {v1, v11, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-boolean v9, v1, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->lineDivider:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    new-instance v1, Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v8}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v1, v7}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    return-object v1
.end method

.method private beautifyAdvanceDummy()Landroid/view/View;
    .locals 11

    const/4 v4, 0x0

    const/16 v5, 0x19

    const/4 v10, -0x1

    const/4 v9, 0x1

    const/high16 v8, 0x41c80000    # 25.0f

    new-instance v7, Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v7, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v10, v10}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v7, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v7, v9}, Landroid/widget/LinearLayout;->setOrientation(I)V

    invoke-virtual {v7, v9}, Landroid/widget/LinearLayout;->setGravity(I)V

    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyDummyTextView:Landroid/widget/TextView;

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v10, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x40a00000    # 5.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyDummyTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyDummyTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41400000    # 12.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v4, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyDummyTextView:Landroid/widget/TextView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyDummyTextView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v9}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyDummyTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x420c0000    # 35.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x420c0000    # 35.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-virtual {v0, v1, v4, v2, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyDummyTextView:Landroid/widget/TextView;

    const/16 v1, 0x56

    const/16 v2, 0x57

    const/16 v3, 0x5b

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyDummyTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_dummy"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyPlusButton:Landroid/view/View;

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-boolean v9, v0, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyPlusButton:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyPlusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_plus"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyPlusButton:Landroid/view/View;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->plusBgPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->plusBgPressPath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->plusBgFocusPath:Ljava/lang/String;

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyMinusButton:Landroid/view/View;

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-boolean v9, v0, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyMinusButton:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyMinusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_minus"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyMinusButton:Landroid/view/View;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->minusBgPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->minusBgPressPath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->minusBgFocusPath:Ljava/lang/String;

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {v1, v10, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x40400000    # 3.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v1, 0x4

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getBeautifyAdvanceMaxValue(I)I

    move-result v1

    new-instance v2, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v2}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDummySeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDummySeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->beautifyAdvanceSeekBar(Landroid/graphics/drawable/GradientDrawable;)Landroid/widget/SeekBar;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummySeekbar:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummySeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v2, v1}, Landroid/widget/SeekBar;->setMax(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyMinusButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummySeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyPlusButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyDummyTextView:Landroid/widget/TextView;

    invoke-virtual {v7, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v7, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-object v7
.end method

.method private beautifyAdvanceModulation()Landroid/view/View;
    .locals 11

    const/4 v4, 0x0

    const/16 v5, 0x19

    const/4 v10, -0x1

    const/4 v9, 0x1

    const/high16 v8, 0x41c80000    # 25.0f

    new-instance v7, Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v7, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v10, v10}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v7, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v7, v9}, Landroid/widget/LinearLayout;->setOrientation(I)V

    invoke-virtual {v7, v9}, Landroid/widget/LinearLayout;->setGravity(I)V

    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyModulationTextView:Landroid/widget/TextView;

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v10, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x40a00000    # 5.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyModulationTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyModulationTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41400000    # 12.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v4, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyModulationTextView:Landroid/widget/TextView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyModulationTextView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v9}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyModulationTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x420c0000    # 35.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x420c0000    # 35.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-virtual {v0, v1, v4, v2, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyModulationTextView:Landroid/widget/TextView;

    const/16 v1, 0x56

    const/16 v2, 0x57

    const/16 v3, 0x5b

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyModulationTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_modulation"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationPlusButton:Landroid/view/View;

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-boolean v9, v0, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationPlusButton:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationPlusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_plus"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationPlusButton:Landroid/view/View;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->plusBgPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->plusBgPressPath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->plusBgFocusPath:Ljava/lang/String;

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationMinusButton:Landroid/view/View;

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-boolean v9, v0, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationMinusButton:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationMinusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_minus"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationMinusButton:Landroid/view/View;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->minusBgPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->minusBgPressPath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->minusBgFocusPath:Ljava/lang/String;

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {v1, v10, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x40400000    # 3.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v1, 0x6

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getBeautifyAdvanceMaxValue(I)I

    move-result v1

    new-instance v2, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v2}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mModulationSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mModulationSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->beautifyAdvanceSeekBar(Landroid/graphics/drawable/GradientDrawable;)Landroid/widget/SeekBar;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationSeekbar:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v2, v1}, Landroid/widget/SeekBar;->setMax(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationMinusButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationPlusButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyModulationTextView:Landroid/widget/TextView;

    invoke-virtual {v7, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v7, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-object v7
.end method

.method private beautifyAdvanceResetBtn()Landroid/widget/Button;
    .locals 5

    const/high16 v4, 0x40a00000    # 5.0f

    new-instance v0, Landroid/widget/Button;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceResetButton:Landroid/widget/Button;

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42200000    # 40.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceResetButton:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceResetButton:Landroid/widget/Button;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->popupBtnBgNomalPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->popupBtnBgPressPath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->popupBtnBgFocusPath:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceResetButton:Landroid/widget/Button;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceResetButton:Landroid/widget/Button;

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setGravity(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceResetButton:Landroid/widget/Button;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41600000    # 14.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/Button;->setTextSize(IF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceResetButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_reset"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceResetButton:Landroid/widget/Button;

    return-object v0
.end method

.method private beautifyAdvanceSeekBar(Landroid/graphics/drawable/GradientDrawable;)Landroid/widget/SeekBar;
    .locals 10

    const/16 v4, 0x16

    const/high16 v3, 0x41200000    # 10.0f

    const/4 v9, 0x1

    const/4 v8, 0x0

    new-instance v6, Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v6, v0}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x433e0000    # 190.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-boolean v9, v0, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v6, v0}, Landroid/widget/SeekBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    invoke-virtual {v6, v0, v8, v1, v8}, Landroid/widget/SeekBar;->setPadding(IIII)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->handelPath:Ljava/lang/String;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->handelPressPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->handelFocusPath:Ljava/lang/String;

    move v5, v4

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/widget/SeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p1, v8}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x40900000    # 4.5f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    new-instance v7, Landroid/graphics/drawable/ClipDrawable;

    const/4 v0, 0x3

    invoke-direct {v7, p1, v0, v9}, Landroid/graphics/drawable/ClipDrawable;-><init>(Landroid/graphics/drawable/Drawable;II)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->progressBgPath:Ljava/lang/String;

    const/16 v2, 0xbe

    const/16 v3, 0x9

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    new-instance v0, Landroid/graphics/drawable/InsetDrawable;

    move v2, v8

    move v3, v8

    move v4, v8

    move v5, v8

    invoke-direct/range {v0 .. v5}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    new-instance v1, Landroid/graphics/drawable/LayerDrawable;

    const/4 v2, 0x2

    new-array v2, v2, [Landroid/graphics/drawable/Drawable;

    aput-object v0, v2, v8

    aput-object v7, v2, v9

    invoke-direct {v1, v2}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v6, v1}, Landroid/widget/SeekBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    return-object v6
.end method

.method private beautifyAdvanceSettingSeekbars()Landroid/view/View;
    .locals 4

    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->beautifyAdvanceCursive()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->beautifyAdvanceSustenance()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->beautifyAdvanceDummy()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->beautifyAdvanceModulation()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->beautifyAdvanceResetBtn()Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method private beautifyAdvanceSustenance()Landroid/view/View;
    .locals 11

    const/4 v4, 0x0

    const/16 v5, 0x19

    const/4 v10, -0x1

    const/4 v9, 0x1

    const/high16 v8, 0x41c80000    # 25.0f

    new-instance v7, Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v7, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v10, v10}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v7, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v7, v9}, Landroid/widget/LinearLayout;->setOrientation(I)V

    invoke-virtual {v7, v9}, Landroid/widget/LinearLayout;->setGravity(I)V

    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifySustenanceTextView:Landroid/widget/TextView;

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v10, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x40a00000    # 5.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifySustenanceTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifySustenanceTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41400000    # 12.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v4, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifySustenanceTextView:Landroid/widget/TextView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifySustenanceTextView:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v9}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifySustenanceTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x420c0000    # 35.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x420c0000    # 35.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-virtual {v0, v1, v4, v2, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifySustenanceTextView:Landroid/widget/TextView;

    const/16 v1, 0x56

    const/16 v2, 0x57

    const/16 v3, 0x5b

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifySustenanceTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_sustenance"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenancePlusButton:Landroid/view/View;

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-boolean v9, v0, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenancePlusButton:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenancePlusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_plus"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenancePlusButton:Landroid/view/View;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->plusBgPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->plusBgPressPath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->plusBgFocusPath:Ljava/lang/String;

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceMinusButton:Landroid/view/View;

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-boolean v9, v0, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceMinusButton:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceMinusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_minus"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceMinusButton:Landroid/view/View;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->minusBgPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->minusBgPressPath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->minusBgFocusPath:Ljava/lang/String;

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {v1, v10, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x40400000    # 3.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v1, 0x3

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getBeautifyAdvanceMaxValue(I)I

    move-result v1

    new-instance v2, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v2}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSustenanceSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSustenanceSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->beautifyAdvanceSeekBar(Landroid/graphics/drawable/GradientDrawable;)Landroid/widget/SeekBar;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceSeekbar:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v2, v1}, Landroid/widget/SeekBar;->setMax(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceMinusButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenancePlusButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifySustenanceTextView:Landroid/widget/TextView;

    invoke-virtual {v7, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v7, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-object v7
.end method

.method private beautifyEnableLayout()Landroid/view/View;
    .locals 10

    const/16 v9, 0xf

    const/4 v6, -0x2

    const/4 v8, 0x0

    const/4 v5, -0x1

    const/4 v7, 0x1

    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x421c0000    # 39.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {v1, v5, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x40d00000    # 6.5f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x40b00000    # 5.5f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v2, v5, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-boolean v7, v2, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->lineDivider:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    new-instance v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyEnableTextView:Landroid/widget/TextView;

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v5, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-boolean v7, v1, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41580000    # 13.5f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v1, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyEnableTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyEnableTextView:Landroid/widget/TextView;

    sget-object v2, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyEnableTextView:Landroid/widget/TextView;

    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyEnableTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41700000    # 15.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v8, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyEnableTextView:Landroid/widget/TextView;

    const/16 v2, 0x13

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyEnableTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v3, "string_beutify"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyEnableTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setFocusable(Z)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyEnableTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v3, "string_beutify"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    new-instance v1, Landroid/widget/Switch;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/Switch;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyEnableSwitchView:Landroid/widget/Switch;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x41c80000    # 25.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v2, v6, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-boolean v7, v2, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x41080000    # 8.5f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    const/16 v3, 0xb

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v2, v9}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyEnableSwitchView:Landroid/widget/Switch;

    invoke-virtual {v3, v2}, Landroid/widget/Switch;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/16 v2, 0x10

    if-lt v1, v2, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyEnableSwitchView:Landroid/widget/Switch;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->switchThumbPath:Ljava/lang/String;

    const/16 v4, 0x18

    const/16 v5, 0x18

    invoke-virtual {v2, v3, v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Switch;->setThumbDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyEnableSwitchView:Landroid/widget/Switch;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->switchCheckFalseBgPath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->switchCheckTrueBgPath:Ljava/lang/String;

    const/16 v5, 0x2e

    const/16 v6, 0x19

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableCheckedImg(Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Switch;->setTrackDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyEnableSwitchView:Landroid/widget/Switch;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42380000    # 46.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/Switch;->setSwitchMinWidth(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyEnableSwitchView:Landroid/widget/Switch;

    invoke-virtual {v1, v7}, Landroid/widget/Switch;->setThumbTextPadding(I)V

    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyEnableSwitchView:Landroid/widget/Switch;

    invoke-virtual {v1, v8}, Landroid/widget/Switch;->setChecked(Z)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyEnableTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyEnableSwitchView:Landroid/widget/Switch;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method private beautifyStyleBtnsLayout()Landroid/view/View;
    .locals 15

    const/high16 v14, 0x42040000    # 33.0f

    const/high16 v3, 0x41300000    # 11.0f

    const/high16 v13, 0x40c00000    # 6.0f

    const/high16 v12, 0x40800000    # 4.0f

    const/high16 v11, 0x40a00000    # 5.0f

    new-instance v8, Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v8, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v9, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v0, -0x1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x422c0000    # 43.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    invoke-direct {v9, v0, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v13}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-virtual {v8, v0, v1, v2, v3}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyStyleBtnViews:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyStyleBtnViews:Ljava/util/ArrayList;

    :cond_0
    const/4 v0, 0x0

    move v7, v0

    :goto_0
    const/4 v0, 0x6

    if-lt v7, v0, :cond_1

    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v13}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->lineDivider:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    new-instance v1, Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v9}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v1, v8}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    return-object v1

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "chinabrush_mode_0"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v1, v7, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "chinabrush_mode_0"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v2, v7, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, "_press"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sget-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_2
    new-instance v10, Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v10, v0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x42180000    # 38.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v4, v14}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-direct {v0, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v10, v0}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v4, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    invoke-virtual {v10, v0, v3, v4, v5}, Landroid/widget/ImageButton;->setPadding(IIII)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/16 v4, 0x1c

    const/16 v5, 0x18

    move-object v3, v2

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v10, v0}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->popupBtnBgNomalPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->popupBtnBgPressPath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->popupBtnBgFocusPath:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x42180000    # 38.0f

    invoke-virtual {v1, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v14}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    move-object v1, v10

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    if-nez v7, :cond_3

    const/4 v0, 0x1

    invoke-virtual {v10, v0}, Landroid/widget/ImageButton;->setSelected(Z)V

    :cond_3
    invoke-virtual {v8, v10}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyStyleBtnViews:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto/16 :goto_0
.end method

.method private beautifyUpdateSettingUI(Z)V
    .locals 7

    const/16 v6, 0x438

    const/high16 v5, 0x41200000    # 10.0f

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getMinSettingValue()F

    move-result v1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getMaxSettingValue()F

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasWidth()I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v3}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasHeight()I

    move-result v3

    if-ge v2, v3, :cond_2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasWidth()I

    move-result v2

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasSize:I

    :goto_0
    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasSize:I

    if-nez v2, :cond_0

    iput v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasSize:I

    :cond_0
    :goto_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    const/high16 v3, 0x43b40000    # 360.0f

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasSize:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    sub-float/2addr v2, v1

    mul-float/2addr v2, v5

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeSeekbar:Landroid/widget/SeekBar;

    sub-float/2addr v0, v1

    mul-float/2addr v0, v5

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/widget/SeekBar;->setMax(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->updateBeautifySeekBarsFromString(Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->setPenType(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->setStrokeSize(F)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->setStrokeColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->setStrokeAdvancedSetting(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->invalidate()V

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCurrentBeautifyStyle:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCursiveSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSustenanceSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDummySeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mModulationSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    :goto_2
    return-void

    :cond_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasHeight()I

    move-result v2

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasSize:I

    goto :goto_0

    :cond_3
    iput v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasSize:I

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCursiveSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSustenanceSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDummySeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mModulationSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    goto :goto_2
.end method

.method private bodyBg()Landroid/view/ViewGroup;
    .locals 9

    const/16 v8, 0xa

    const/4 v7, 0x1

    const v6, 0x42fa999a    # 125.3f

    const/4 v5, -0x1

    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v2, v3, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-boolean v7, v2, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/16 v3, 0x9

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v2, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v2, Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v4, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-direct {v3, v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-boolean v7, v3, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/16 v4, 0xb

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v3, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->lightBodyLeftPath:Ljava/lang/String;

    invoke-virtual {v3, v1, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->lightBodyRightPath:Ljava/lang/String;

    invoke-virtual {v3, v2, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOnConsumedHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method private bodyLayout()Landroid/widget/RelativeLayout;
    .locals 4

    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x437a0000    # 250.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->penLayout()Landroid/view/ViewGroup;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenLayout:Landroid/view/View;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->presetLayout()Landroid/widget/LinearLayout;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetLayout:Landroid/widget/LinearLayout;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bodyBg()Landroid/view/ViewGroup;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyBg:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyBg:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomLayout()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomLayout:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomLayout:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v3, "string_resize"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method private bottomLayout()Landroid/view/View;
    .locals 8

    const/16 v7, 0x10

    const/4 v5, -0x1

    const/4 v4, 0x0

    const/high16 v6, 0x41800000    # 16.0f

    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {v1, v5, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v3, 0x43808000    # 257.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-virtual {v1, v4, v2, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomExtendBg:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomExtendBg:Landroid/widget/ImageView;

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v2, v5, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomExtendBg:Landroid/widget/ImageView;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPressPath:Ljava/lang/String;

    sget-object v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPressPath:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sget v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSdkVersion:I

    if-ge v1, v7, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomExtendBg:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPressPath:Ljava/lang/String;

    sget-object v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPressPath:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    new-instance v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomHandle:Landroid/widget/ImageView;

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41b00000    # 22.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/16 v2, 0xe

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, -0x40800000    # -1.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomHandle:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomHandle:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomHandlePath:Ljava/lang/String;

    const/16 v4, 0x16

    invoke-virtual {v2, v3, v4, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomExtendBg:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomHandle:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOnConsumedHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomExtendBg:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPressPath:Ljava/lang/String;

    sget-object v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bottomExpandPressPath:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private checkPosition()V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v0, 0x2

    new-array v1, v0, [I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x437a0000    # 250.0f

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x43230000    # 163.0f

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getLocationOnScreen([I)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    aget v4, v1, v6

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    if-ge v4, v5, :cond_0

    iput v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    :cond_0
    aget v4, v1, v7

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    if-ge v4, v5, :cond_1

    iput v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    aget v5, v1, v6

    sub-int/2addr v4, v5

    if-ge v4, v2, :cond_2

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    sub-int v2, v4, v2

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    if-gez v2, :cond_2

    iput v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    :cond_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    aget v1, v1, v7

    sub-int v1, v2, v1

    if-ge v1, v3, :cond_3

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    sub-int/2addr v1, v3

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    if-gez v1, :cond_3

    iput v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    :cond_3
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private colorGradationView()Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;
    .locals 6

    const/4 v5, 0x0

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mCustom_imagepath:Ljava/lang/String;

    sget v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScale:F

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x43400000    # 192.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x42400000    # 48.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setClickable(Z)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_gradation"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v5, v5, v5, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setPadding(IIII)V

    return-object v0
.end method

.method private colorPaletteGradationLayout()Landroid/view/ViewGroup;
    .locals 5

    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x42ec0000    # 118.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->PickerView()Landroid/view/ViewGroup;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPickerView:Landroid/view/View;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->PaletteView()Landroid/view/ViewGroup;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPaletteView:Landroid/view/View;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->paletteBg()Landroid/view/ViewGroup;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPaletteBg:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPaletteBg:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPaletteView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPickerView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method private colorPaletteView()Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;
    .locals 5

    const/4 v4, 0x1

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->mCustom_imagepath:Ljava/lang/String;

    sget v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScale:F

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->colorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x43420000    # 194.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-boolean v4, v0, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->colorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->colorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setClickable(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->colorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->colorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    return-object v0
.end method

.method private exitButton()Landroid/view/View;
    .locals 7

    new-instance v1, Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->EXIT_BUTTON_WIDTH:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->EXIT_BUTTON_HEIGHT:I

    int-to-float v4, v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v2, 0xb

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v2, 0x14

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    const/16 v2, 0x9

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setFocusable(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_close"

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->exitPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->exitPressPath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->exitfocusPath:Ljava/lang/String;

    const/16 v5, 0x26

    const/16 v6, 0x22

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    return-object v1
.end method

.method private findMinValue(Landroid/widget/TextView;I)V
    .locals 3

    const/4 v2, 0x0

    const/high16 v0, 0x41800000    # 16.0f

    :goto_0
    invoke-virtual {p1, v2, v2}, Landroid/widget/TextView;->measure(II)V

    invoke-virtual {p1}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v1

    if-le v1, p2, :cond_0

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v0, v1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1, v2, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    return-void
.end method

.method private getBeautifyAdvanceArrayDataToString([I)Ljava/lang/String;
    .locals 3

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v0, 0x0

    :goto_0
    const/16 v2, 0xa

    if-lt v0, v2, :cond_0

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    aget v2, p1, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    const/16 v2, 0x3b

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private getBeautifyAdvanceMaxValue(I)I
    .locals 1

    const/16 v0, 0x14

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    const/16 v0, 0xc

    goto :goto_0

    :pswitch_2
    const/16 v0, 0x10

    goto :goto_0

    :pswitch_3
    const/16 v0, 0x64

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private getBeautifyAdvanceParamDataFromArray([II)I
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    array-length v1, p1

    const/16 v2, 0xa

    if-eq v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    aget v0, p1, p2

    goto :goto_0
.end method

.method private getBeautifyAdvanceParamDataFromString(Ljava/lang/String;I)I
    .locals 4

    const/4 v0, 0x0

    const-string/jumbo v1, ";"

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    array-length v2, v1

    const/16 v3, 0xa

    if-eq v2, v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    aget-object v0, v1, p2

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method private getBeautifyStyleBtnIndex(Ljava/lang/String;)I
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getBeautifyAdvanceParamDataFromString(Ljava/lang/String;I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    :pswitch_4
    const/4 v0, 0x4

    goto :goto_0

    :pswitch_5
    const/4 v0, 0x5

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getMovableRect()Landroid/graphics/Rect;
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x2

    new-array v0, v0, [I

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->getLocationOnScreen([I)V

    aget v2, v0, v4

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mLeftMargin:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    aget v2, v0, v5

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mTopMargin:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    aget v2, v0, v4

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    aget v0, v0, v5

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v2

    add-int/2addr v0, v2

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    return-object v1
.end method

.method private hasBeautifyPen()Z
    .locals 4

    const/4 v0, 0x0

    const/4 v1, -0x1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    const-string/jumbo v3, "com.samsung.android.sdk.pen.pen.preload.Beautify"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->getPenPluginIndexByPenName(Ljava/lang/String;)I

    move-result v2

    if-ge v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private initButtonValue()V
    .locals 1

    const/high16 v0, 0x40a00000    # 5.0f

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->EXIT_BUTTON_TOP_MARGIN:F

    const/high16 v0, 0x40400000    # 3.0f

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->EXIT_BUTTON_RIGHT_MARGIN:F

    const/16 v0, 0x26

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->EXIT_BUTTON_WIDTH:I

    const/16 v0, 0x22

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->EXIT_BUTTON_HEIGHT:I

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->LINE_BUTTON_WIDTH:F

    const/high16 v0, 0x41880000    # 17.0f

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->LINE_BUTTON_HEIGHT:F

    const/high16 v0, 0x41480000    # 12.5f

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->LINE_BUTTON_TOP_MARGIN:F

    return-void
.end method

.method private initColorSelecteView()V
    .locals 3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOnColorChangedListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView$OnColorChangedListener;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setInitialValue(Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView$OnColorChangedListener;I)V

    :cond_0
    return-void
.end method

.method private initPenPlugin(Landroid/content/Context;)V
    .locals 2

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;-><init>(Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->getPenCount()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginCount:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->getPenPluginInfoList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    return-void
.end method

.method private isBeautifyPen(I)Z
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v2, "com.samsung.android.sdk.pen.pen.preload.Beautify"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private isBeautifyPen(Ljava/lang/String;)Z
    .locals 2

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    const-string/jumbo v1, "com.samsung.android.sdk.pen.pen.preload.Beautify"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private isChinesePen(Ljava/lang/String;)Z
    .locals 2

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    const-string/jumbo v1, "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method private lineButton1()Landroid/view/View;
    .locals 5

    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x41880000    # 17.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v2, 0xb

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->LINE_BUTTON_TOP_MARGIN:F

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    add-int/lit8 v2, v2, 0x9

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->EXIT_BUTTON_WIDTH:I

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->LINE_BUTTON_WIDTH:F

    add-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->EXIT_BUTTON_WIDTH:I

    int-to-float v4, v4

    add-float/2addr v3, v4

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    add-int/lit8 v2, v2, 0x9

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->linePath:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    return-object v0
.end method

.method private lineButton2()Landroid/view/View;
    .locals 5

    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x41880000    # 17.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v2, 0xb

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->LINE_BUTTON_TOP_MARGIN:F

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    add-int/lit8 v2, v2, 0x9

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->EXIT_BUTTON_WIDTH:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    add-int/lit8 v2, v2, 0x9

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->linePath:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    return-object v0
.end method

.method private paletteBg()Landroid/view/ViewGroup;
    .locals 9

    const/16 v8, 0xa

    const/4 v7, 0x1

    const v6, 0x42fa999a    # 125.3f

    const/4 v5, -0x1

    const/4 v2, 0x0

    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2, v2, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    new-instance v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v2, v3, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-boolean v7, v2, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/16 v3, 0x9

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v2, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v2, Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v4, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-direct {v3, v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-boolean v7, v3, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/16 v4, 0xb

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v3, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->grayBodyLeftPath:Ljava/lang/String;

    invoke-virtual {v3, v1, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->grayBodyRightPath:Ljava/lang/String;

    invoke-virtual {v3, v2, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method private penAlphaLayout()Landroid/view/ViewGroup;
    .locals 13

    const/4 v12, -0x1

    const/4 v11, 0x1

    const/4 v10, 0x0

    const/high16 v9, 0x41c00000    # 24.0f

    const/16 v5, 0x18

    new-instance v7, Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v7, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x42480000    # 50.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    invoke-direct {v0, v12, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x41000000    # 8.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    invoke-virtual {v7, v10, v10, v10, v0}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {v1, v12, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-boolean v11, v1, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v2, 0xe

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x3f000000    # 0.5f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x40000000    # 2.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->lineDivider:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOpacityPlusButton:Landroid/view/View;

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-boolean v11, v0, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOpacityPlusButton:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOpacityPlusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_plus"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSdkVersion:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOpacityPlusButton:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->plusBgPath:Ljava/lang/String;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->plusBgPressPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->plusBgFocusPath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->plusBgDimPath:Ljava/lang/String;

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOpacityMinusButton:Landroid/view/View;

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-boolean v11, v0, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOpacityMinusButton:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOpacityMinusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_minus"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSdkVersion:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_1

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOpacityMinusButton:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->minusBgPath:Ljava/lang/String;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->minusBgPressPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->minusBgFocusPath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->minusBgDimPath:Ljava/lang/String;

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->penAlphaSeekbar()Landroid/widget/SeekBar;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaSeekbar:Landroid/widget/SeekBar;

    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaTextView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaTextView:Landroid/widget/TextView;

    sget-object v1, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1, v11}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaTextView:Landroid/widget/TextView;

    const/16 v1, 0x56

    const/16 v2, 0x57

    const/16 v3, 0x5b

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41400000    # 12.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v10, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaTextView:Landroid/widget/TextView;

    const/16 v1, 0x33

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41500000    # 13.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    invoke-direct {v0, v12, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaTextView:Landroid/widget/TextView;

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOpacityPlusButton:Landroid/view/View;

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOpacityMinusButton:Landroid/view/View;

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    return-object v7

    :cond_0
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOpacityPlusButton:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->plusBgPath:Ljava/lang/String;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->plusBgPressPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->plusBgFocusPath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->plusBgDimPath:Ljava/lang/String;

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    :cond_1
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOpacityMinusButton:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->minusBgPath:Ljava/lang/String;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->minusBgPressPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->minusBgFocusPath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->minusBgDimPath:Ljava/lang/String;

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1
.end method

.method private penAlphaSeekbar()Landroid/widget/SeekBar;
    .locals 12

    const/16 v6, 0x9

    const/4 v11, 0x3

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v10, 0x1

    const/4 v9, 0x0

    new-instance v7, Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v7, v0}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x433e0000    # 190.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41b00000    # 22.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v9, v9, v9, v9}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iput-boolean v10, v0, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v7, v0}, Landroid/widget/SeekBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    invoke-virtual {v7, v0, v9, v1, v9}, Landroid/widget/SeekBar;->setPadding(IIII)V

    const/16 v0, 0x63

    invoke-virtual {v7, v0}, Landroid/widget/SeekBar;->setMax(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->handelPath:Ljava/lang/String;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->handelPressPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->handelFocusPath:Ljava/lang/String;

    const/16 v4, 0x16

    const/16 v5, 0x16

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/widget/SeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v1, 0x3fd9999a    # 1.7f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    invoke-virtual {v7, v0}, Landroid/widget/SeekBar;->setThumbOffset(I)V

    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, v9}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v2, 0x40833333    # 4.1f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    new-instance v8, Landroid/graphics/drawable/ClipDrawable;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v8, v0, v11, v10}, Landroid/graphics/drawable/ClipDrawable;-><init>(Landroid/graphics/drawable/Drawable;II)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->progressShadowPath:Ljava/lang/String;

    const/16 v2, 0xbe

    invoke-virtual {v0, v1, v2, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->progressAlphaPath:Ljava/lang/String;

    const/16 v3, 0xbe

    invoke-virtual {v0, v2, v3, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->alphaDrawable:Landroid/graphics/drawable/Drawable;

    new-instance v0, Landroid/graphics/drawable/InsetDrawable;

    move v2, v9

    move v3, v9

    move v4, v9

    move v5, v9

    invoke-direct/range {v0 .. v5}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    new-instance v1, Landroid/graphics/drawable/InsetDrawable;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->alphaDrawable:Landroid/graphics/drawable/Drawable;

    move v3, v9

    move v4, v9

    move v5, v9

    move v6, v9

    invoke-direct/range {v1 .. v6}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    new-instance v2, Landroid/graphics/drawable/LayerDrawable;

    new-array v3, v11, [Landroid/graphics/drawable/Drawable;

    aput-object v1, v3, v9

    aput-object v8, v3, v10

    const/4 v1, 0x2

    aput-object v0, v3, v1

    invoke-direct {v2, v3}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v7, v2}, Landroid/widget/SeekBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$82;

    invoke-direct {v0, p0, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$82;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Landroid/widget/SeekBar;)V

    invoke-virtual {v7, v0}, Landroid/widget/SeekBar;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-object v7
.end method

.method private penLayout()Landroid/view/ViewGroup;
    .locals 9

    const/4 v8, 0x1

    const/4 v7, -0x1

    const/4 v6, -0x2

    const/4 v5, 0x0

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v7, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41f00000    # 30.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    const/16 v2, 0x9

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x41800000    # 16.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-virtual {v0, v5, v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v0, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->setVerticalFadingEdgeEnabled(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v0, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->setFadingEdgeLength(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->setVerticalScrollBarEnabled(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->setOverScrollMode(I)V

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v7, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->setOrientation(I)V

    const/16 v1, -0x9

    invoke-virtual {v0, v5, v5, v1, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->setPadding(IIII)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->penTypeLayout()Landroid/view/ViewGroup;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeLayout:Landroid/view/ViewGroup;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->colorPaletteGradationLayout()Landroid/view/ViewGroup;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorSelectPickerLayout:Landroid/view/View;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->beautifyEnableLayout()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyEnableLayout:Landroid/view/View;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->beautifyStyleBtnsLayout()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyStyleBtnsLayout:Landroid/view/View;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->penSeekbarLayout()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSeekbarLayout:Landroid/view/View;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->typeSelectorlayout()Landroid/widget/LinearLayout;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mTypeSelectorLayout:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyEnableLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyStyleBtnsLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSeekbarLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorSelectPickerLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->addView(Landroid/view/View;)V

    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v7, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mTypeSelectorLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method private penSeekbarLayout()Landroid/view/View;
    .locals 6

    const/4 v5, 0x0

    const/high16 v4, 0x40a00000    # 5.0f

    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-virtual {v0, v1, v5, v2, v5}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->penAlphaLayout()Landroid/view/ViewGroup;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaSeekbarView:Landroid/view/View;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->penSizeLayout()Landroid/view/ViewGroup;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeSeekbarView:Landroid/view/View;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->beautifyAdvanceSettingSeekbars()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSettingLayout:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeSeekbarView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaSeekbarView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSettingLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method private penSelectIndex(I)V
    .locals 5

    const/4 v4, 0x1

    const/4 v2, 0x0

    move v1, v2

    :goto_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginCount:I

    if-lt v1, v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    const-string/jumbo v3, "com.samsung.android.sdk.pen.pen.preload.MagicPen"

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->getPenPluginIndexByPenName(Ljava/lang/String;)I

    move-result v0

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaPreview:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getY()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setMagicPenMode(I)V

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->previousPenMagicSelected:Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->alphaDrawable:Landroid/graphics/drawable/Drawable;

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlpha:I

    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    :goto_1
    if-ne p1, v1, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setSelected(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->penSelection(Landroid/view/View;)V

    :goto_2
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayoutHeight:I

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaPreview:Landroid/widget/RelativeLayout;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->previousPenMagicSelected:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getY()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->currenMagicPenHeight:I

    :cond_2
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isMagicPenEnable:Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->alphaDrawable:Landroid/graphics/drawable/Drawable;

    const/16 v3, 0xff

    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorSelectPickerLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIsMaxHeight:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v3, 0x43a18000    # 323.0f

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V

    :cond_3
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->previousPenMagicSelected:Z

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    goto :goto_2
.end method

.method private penSelection(Landroid/view/View;)V
    .locals 11

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    const-string/jumbo v2, "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->getPenPluginIndexByPenName(Ljava/lang/String;)I

    move-result v3

    const/4 v0, -0x1

    if-eq v3, v0, :cond_0

    const/4 v0, 0x0

    move v10, v0

    move v0, v1

    move v1, v10

    :goto_1
    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginCount:I

    if-lt v1, v2, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->drawExpendImage(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    const/high16 v2, 0x41a00000    # 20.0f

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->selectColorForGradiation(ID)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setSelected(Z)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-le v2, v1, :cond_3

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_15

    :cond_3
    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isBeautifyPen(I)Z

    move-result v2

    if-eqz v2, :cond_5

    :goto_2
    if-eqz v2, :cond_6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/view/View;->setSelected(Z)V

    :goto_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v1, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    if-ne v3, v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyEnableSwitchView:Landroid/widget/Switch;

    invoke-virtual {v0}, Landroid/widget/Switch;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    const-string/jumbo v4, "com.samsung.android.sdk.pen.pen.preload.Beautify"

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->getPenPluginIndexByPenName(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    :cond_4
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->loadPenPlugin(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    if-nez v0, :cond_7

    :cond_5
    :goto_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto/16 :goto_1

    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/view/View;->setSelected(Z)V

    goto :goto_3

    :cond_7
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenDataList:Ljava/util/List;

    if-eqz v0, :cond_f

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenDataList:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_f

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->isLoaded()Z

    move-result v0

    if-nez v0, :cond_f

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenName()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v4, ""

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenDataList:Ljava/util/List;

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    iput v0, v4, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenDataList:Ljava/util/List;

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    iput-boolean v0, v4, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenDataList:Ljava/util/List;

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    iput v0, v4, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenDataList:Ljava/util/List;

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    iput-object v0, v4, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    :goto_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->isLoaded()Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->setLoaded(Z)V

    :cond_8
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasWidth()I

    move-result v0

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v4}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasHeight()I

    move-result v4

    if-ge v0, v4, :cond_11

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasWidth()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasSize:I

    :goto_6
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasSize:I

    if-nez v0, :cond_9

    const/16 v0, 0x438

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasSize:I

    :cond_9
    :goto_7
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getMinSettingValue()F

    move-result v4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getMaxSettingValue()F

    move-result v5

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasSize:I

    int-to-float v0, v0

    mul-float/2addr v0, v5

    float-to-double v6, v0

    const-wide v8, 0x4076800000000000L    # 360.0

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-long v8, v0

    cmp-long v0, v6, v8

    if-gez v0, :cond_a

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasSize:I

    int-to-float v6, v6

    mul-float/2addr v6, v5

    float-to-double v6, v6

    const-wide v8, 0x4076800000000000L    # 360.0

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    long-to-float v6, v6

    iput v6, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v6, v6, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    invoke-interface {v0, v6}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setSize(F)V

    :cond_a
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasSize:I

    int-to-float v0, v0

    mul-float/2addr v0, v4

    float-to-double v6, v0

    const-wide v8, 0x4076800000000000L    # 360.0

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-long v8, v0

    cmp-long v0, v6, v8

    if-lez v0, :cond_b

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasSize:I

    int-to-float v6, v6

    mul-float/2addr v6, v4

    float-to-double v6, v6

    const-wide v8, 0x4076800000000000L    # 360.0

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    long-to-float v6, v6

    iput v6, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v6, v6, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    invoke-interface {v0, v6}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setSize(F)V

    :cond_b
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v6, v6, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-interface {v0, v6}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    const/4 v6, 0x4

    invoke-interface {v0, v6}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getPenAttribute(I)Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-interface {v0, v6}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setAdvancedSetting(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setBeautifyAdvanceStringToCurrentAdvanceData(Ljava/lang/String;)V

    :cond_c
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-interface {v0, v6}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    :cond_d
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v6, v6, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    const/high16 v7, 0x43b40000    # 360.0f

    mul-float/2addr v6, v7

    iget v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasSize:I

    int-to-float v7, v7

    div-float/2addr v6, v7

    sub-float/2addr v6, v4

    const/high16 v7, 0x41200000    # 10.0f

    mul-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeSeekbar:Landroid/widget/SeekBar;

    sub-float v4, v5, v4

    const/high16 v5, 0x41200000    # 10.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-virtual {v7, v4}, Landroid/widget/SeekBar;->setMax(I)V

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v4, v6}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iput v0, v4, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    const/4 v4, 0x1

    invoke-interface {v0, v4}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getPenAttribute(I)Z

    move-result v0

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    shr-int/lit8 v0, v0, 0x18

    and-int/lit16 v0, v0, 0xff

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlpha:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaSeekbar:Landroid/widget/SeekBar;

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlpha:I

    int-to-float v4, v4

    const/high16 v5, 0x437f0000    # 255.0f

    div-float/2addr v4, v5

    const/high16 v5, 0x42c60000    # 99.0f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    const v5, 0xffffff

    and-int/2addr v4, v5

    const/high16 v5, -0x1000000

    or-int/2addr v4, v5

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    :goto_8
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    const/16 v4, 0xff

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/GradientDrawable;->setAlpha(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->setPenType(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->setStrokeSize(F)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->setStrokeColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->setStrokeAdvancedSetting(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->invalidate()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isBeautifyPen(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_14

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    const/4 v4, 0x4

    invoke-interface {v0, v4}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getPenAttribute(I)Z

    move-result v0

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mAdvancedSettingButton:Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    :goto_9
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    if-nez v0, :cond_e

    const/16 v0, 0xa

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v4}, Landroid/widget/HorizontalScrollView;->getScrollX()I

    move-result v4

    int-to-float v4, v4

    const/4 v5, 0x0

    invoke-direct {p0, v0, v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->playScrollAnimation(IFF)V

    :cond_e
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    const/4 v4, 0x5

    if-ne v0, v4, :cond_5

    const/16 v0, 0xa

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v4}, Landroid/widget/HorizontalScrollView;->getScrollX()I

    move-result v4

    int-to-float v4, v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x42300000    # 44.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    int-to-float v5, v5

    invoke-direct {p0, v0, v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->playScrollAnimation(IFF)V

    goto/16 :goto_4

    :cond_f
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getColor()I

    move-result v0

    iput v0, v4, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getSize()F

    move-result v0

    iput v0, v4, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    const/4 v4, 0x4

    invoke-interface {v0, v4}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getPenAttribute(I)Z

    move-result v0

    if-eqz v0, :cond_10

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getAdvancedSetting()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    goto/16 :goto_5

    :cond_10
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    const-string/jumbo v4, ""

    iput-object v4, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    goto/16 :goto_5

    :cond_11
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasHeight()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasSize:I

    goto/16 :goto_6

    :cond_12
    const/16 v0, 0x438

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasSize:I

    goto/16 :goto_7

    :cond_13
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v4, v4, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setBeautifyAdvanceSeekbarColor(I)V

    goto/16 :goto_8

    :cond_14
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mAdvancedSettingButton:Landroid/view/View;

    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_9

    :cond_15
    move v2, v0

    goto/16 :goto_2
.end method

.method private penSizeLayout()Landroid/view/ViewGroup;
    .locals 12

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v11, 0x1

    const/4 v10, 0x0

    const/high16 v9, 0x41c00000    # 24.0f

    const/16 v5, 0x18

    new-instance v7, Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v7, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42480000    # 50.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x41000000    # 8.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    invoke-virtual {v7, v10, v10, v10, v0}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-boolean v11, v1, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->lineDivider:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizePlusButton:Landroid/view/View;

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-boolean v11, v0, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizePlusButton:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizePlusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_plus"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSdkVersion:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizePlusButton:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->plusBgPath:Ljava/lang/String;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->plusBgPressPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->plusBgFocusPath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->plusBgDimPath:Ljava/lang/String;

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeMinusButton:Landroid/view/View;

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-boolean v11, v0, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeMinusButton:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeMinusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_minus"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSdkVersion:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_1

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeMinusButton:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->minusBgPath:Ljava/lang/String;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->minusBgPressPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->minusBgFocusPath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->minusBgDimPath:Ljava/lang/String;

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->penSizeSeekbar()Landroid/widget/SeekBar;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeSeekbar:Landroid/widget/SeekBar;

    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeTextView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeTextView:Landroid/widget/TextView;

    sget-object v1, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1, v11}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeTextView:Landroid/widget/TextView;

    const/16 v1, 0x56

    const/16 v2, 0x57

    const/16 v3, 0x5b

    invoke-static {v1, v2, v3}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41400000    # 12.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v10, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeTextView:Landroid/widget/TextView;

    const/16 v1, 0x33

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41500000    # 13.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeTextView:Landroid/widget/TextView;

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizePlusButton:Landroid/view/View;

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeMinusButton:Landroid/view/View;

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    return-object v7

    :cond_0
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizePlusButton:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->plusBgPath:Ljava/lang/String;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->plusBgPressPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->plusBgFocusPath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->plusBgDimPath:Ljava/lang/String;

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    :cond_1
    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeMinusButton:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->minusBgPath:Ljava/lang/String;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->minusBgPressPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->minusBgFocusPath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->minusBgDimPath:Ljava/lang/String;

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableDimImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v8, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1
.end method

.method private penSizeSeekbar()Landroid/widget/SeekBar;
    .locals 12

    const/16 v6, 0x9

    const/4 v11, 0x3

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v10, 0x1

    const/4 v9, 0x0

    new-instance v7, Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v7, v0}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x433e0000    # 190.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41b00000    # 22.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v9, v9, v9, v9}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iput-boolean v10, v0, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v7, v0}, Landroid/widget/SeekBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    invoke-virtual {v7, v0, v9, v1, v9}, Landroid/widget/SeekBar;->setPadding(IIII)V

    const/16 v0, 0x63

    invoke-virtual {v7, v0}, Landroid/widget/SeekBar;->setMax(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->handelPath:Ljava/lang/String;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->handelPressPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->handelFocusPath:Ljava/lang/String;

    const/16 v4, 0x16

    const/16 v5, 0x16

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/widget/SeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v1, 0x3fd9999a    # 1.7f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    invoke-virtual {v7, v0}, Landroid/widget/SeekBar;->setThumbOffset(I)V

    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, v9}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v2, 0x40833333    # 4.1f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    new-instance v8, Landroid/graphics/drawable/ClipDrawable;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v8, v0, v11, v10}, Landroid/graphics/drawable/ClipDrawable;-><init>(Landroid/graphics/drawable/Drawable;II)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->progressBgPath:Ljava/lang/String;

    const/16 v2, 0xbe

    invoke-virtual {v0, v1, v2, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->progressShadowPath:Ljava/lang/String;

    const/16 v3, 0xbe

    invoke-virtual {v0, v2, v3, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    new-instance v0, Landroid/graphics/drawable/InsetDrawable;

    move v2, v9

    move v3, v9

    move v4, v9

    move v5, v9

    invoke-direct/range {v0 .. v5}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    new-instance v1, Landroid/graphics/drawable/InsetDrawable;

    move-object v2, v6

    move v3, v9

    move v4, v9

    move v5, v9

    move v6, v9

    invoke-direct/range {v1 .. v6}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    new-instance v2, Landroid/graphics/drawable/LayerDrawable;

    new-array v3, v11, [Landroid/graphics/drawable/Drawable;

    aput-object v0, v3, v9

    aput-object v8, v3, v10

    const/4 v0, 0x2

    aput-object v1, v3, v0

    invoke-direct {v2, v3}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v7, v2}, Landroid/widget/SeekBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$81;

    invoke-direct {v0, p0, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$81;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;Landroid/widget/SeekBar;)V

    invoke-virtual {v7, v0}, Landroid/widget/SeekBar;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-object v7
.end method

.method private penTypeButton()Landroid/view/View;
    .locals 12

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v10, 0x2

    const/4 v9, -0x1

    const/4 v8, 0x1

    const/4 v7, 0x0

    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42fa0000    # 125.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    add-int/lit8 v2, v2, -0x9

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x41f00000    # 30.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/16 v2, 0xa

    invoke-virtual {v1, v2, v7, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setOrientation(I)V

    new-instance v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v9, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    sget v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSdkVersion:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->tabLinePath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->tabLineSelectPath:Ljava/lang/String;

    sget-object v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->tabLineSelectedFocusPath:Ljava/lang/String;

    sget-object v6, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->tabLineUnselectedFocusPath:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectedFocusImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setSingleLine(Z)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v3, "string_pen"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x3f000000    # 0.5f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v4, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v1, v2, v3, v4, v9}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v3, "string_pen_tab"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v3, "string_selected"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    const/4 v1, 0x3

    new-array v1, v1, [[I

    new-array v2, v10, [I

    const v3, -0x10100a7

    aput v3, v2, v7

    const v3, -0x10100a1

    aput v3, v2, v8

    aput-object v2, v1, v7

    new-array v2, v8, [I

    const v3, 0x10100a7    # android.R.attr.state_pressed

    aput v3, v2, v7

    aput-object v2, v1, v8

    new-array v2, v8, [I

    const v3, 0x10100a1    # android.R.attr.state_selected

    aput v3, v2, v7

    aput-object v2, v1, v10

    const/4 v2, 0x3

    new-array v2, v2, [I

    invoke-static {v7, v7, v7}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v2, v7

    const/16 v3, 0x8b

    const/16 v4, 0xd2

    invoke-static {v8, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v2, v8

    const/16 v3, 0x8b

    const/16 v4, 0xd2

    invoke-static {v8, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v2, v10

    new-instance v3, Landroid/content/res/ColorStateList;

    invoke-direct {v3, v1, v2}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setClickable(Z)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setFocusable(Z)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;

    invoke-virtual {v1, v7, v7, v7, v7}, Landroid/widget/TextView;->setPadding(IIII)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;

    const v2, 0xb82ff9

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setNextFocusUpId(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->tabLinePath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->tabLineSelectPath:Ljava/lang/String;

    sget-object v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->tabLineSelectedFocusPath:Ljava/lang/String;

    sget-object v6, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->tabLineUnselectedFocusPath:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectedFocusImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0
.end method

.method private penTypeLayout()Landroid/view/ViewGroup;
    .locals 11

    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->penTypeLayout:Landroid/widget/RelativeLayout;

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x43910000    # 290.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42960000    # 75.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->penTypeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v1, Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeHorizontalScrollView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeHorizontalScrollView:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->getPenPluginInfoList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v9

    const/4 v0, 0x0

    move v8, v0

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x42960000    # 75.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setFocusable(Z)V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setBackgroundColor(I)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->penTypeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeHorizontalScrollView:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->penTypeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    new-instance v0, Landroid/widget/HorizontalScrollView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeHorizontalScrollView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->setFadingEdgeLength(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->setHorizontalScrollBarEnabled(Z)V

    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeHorizontalScrollView3:Landroid/view/ViewGroup;

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x437a0000    # 250.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42960000    # 75.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeHorizontalScrollView3:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeHorizontalScrollView3:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x40a00000    # 5.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x40a00000    # 5.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/ViewGroup;->setPadding(IIII)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->preview()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPreviewLayout:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeHorizontalScrollView3:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPreviewLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeHorizontalScrollView3:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeHorizontalScrollView3:Landroid/view/ViewGroup;

    return-object v0

    :cond_0
    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    new-instance v1, Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPluginInfo()Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    move-result-object v0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->iconImageUri:Ljava/lang/String;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPluginInfo()Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    move-result-object v0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->selectedIconImageUri:Ljava/lang/String;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPluginInfo()Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    move-result-object v0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->focusedIconImageUri:Ljava/lang/String;

    new-instance v0, Ljava/lang/String;

    const-string/jumbo v5, "iconImageUri"

    invoke-direct {v0, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    new-instance v5, Ljava/lang/String;

    const-string/jumbo v6, "selectedIconImageURI"

    invoke-direct {v5, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    new-instance v6, Ljava/lang/String;

    const-string/jumbo v10, "uriInfo"

    invoke-direct {v6, v10}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v2, "snote_popup_pensetting_brush"

    :cond_1
    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v3, "snote_popup_pensetting_brush_select"

    :cond_2
    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string/jumbo v4, "snote_popup_pensetting_brush_focus"

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const/16 v5, 0x45

    const/16 v6, 0x45

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->penTypeLayout:Landroid/widget/RelativeLayout;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setFocusable(Z)V

    add-int/lit16 v0, v8, 0x3e8

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setId(I)V

    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPluginInfo()Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    const-string/jumbo v2, "Brush"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_brush"

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_4
    :goto_1
    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isBeautifyPen(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    :cond_5
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x428a0000    # 69.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x428a0000    # 69.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    if-lez v8, :cond_b

    const/4 v2, 0x1

    invoke-virtual {v1}, Landroid/widget/ImageButton;->getId()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, -0x3e380000    # -25.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->penTypeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v1, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :goto_2
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto/16 :goto_0

    :cond_6
    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPluginInfo()Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    const-string/jumbo v2, "ChineseBrush"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_chinese_brush"

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_7
    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPluginInfo()Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    const-string/jumbo v2, "InkPen"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_pen"

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_8
    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPluginInfo()Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    const-string/jumbo v2, "Marker"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_marker"

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_9
    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPluginInfo()Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    const-string/jumbo v2, "Pencil"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_pencil"

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_a
    invoke-virtual {v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPluginInfo()Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    const-string/jumbo v2, "MagicPen"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_correction_pen"

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_b
    const/16 v2, 0x9

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v3, 0x40accccd    # 5.4f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->penTypeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v1, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_2
.end method

.method private playScrollAnimation(IFF)V
    .locals 6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScrollTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScrollTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    int-to-float v2, v0

    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScrollTimer:Ljava/util/Timer;

    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCount:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScrollTimer:Ljava/util/Timer;

    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;

    invoke-direct {v1, p0, p2, p3, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$84;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;FFF)V

    const-wide/16 v2, 0xa

    int-to-long v4, p1

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    return-void
.end method

.method private presetAddButton()Landroid/view/View;
    .locals 7

    new-instance v1, Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->EXIT_BUTTON_WIDTH:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->EXIT_BUTTON_HEIGHT:I

    int-to-float v4, v4

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v2, 0xb

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v2, 0x14

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->EXIT_BUTTON_WIDTH:I

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->LINE_BUTTON_WIDTH:F

    add-float/2addr v3, v4

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    add-int/lit8 v2, v2, 0x9

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setFocusable(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_add_preset"

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    sget-object v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->presetAddPath:Ljava/lang/String;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->presetAddPressPath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->presetAddFocusPath:Ljava/lang/String;

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->EXIT_BUTTON_WIDTH:I

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->EXIT_BUTTON_HEIGHT:I

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    return-object v1
.end method

.method private presetDisplay()V
    .locals 3

    const/16 v2, 0x8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetGridView:Landroid/widget/GridView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetGridView:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->getCount()I

    move-result v0

    if-gtz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetGridView:Landroid/widget/GridView;

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetGridView:Landroid/widget/GridView;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private presetLayout()Landroid/widget/LinearLayout;
    .locals 10

    const/4 v9, 0x0

    const/16 v8, 0x5e

    const/4 v7, -0x1

    const/4 v6, 0x0

    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41700000    # 15.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41f00000    # 30.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x40800000    # 4.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x41800000    # 16.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    new-instance v1, Landroid/widget/GridView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/GridView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetGridView:Landroid/widget/GridView;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v7, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetGridView:Landroid/widget/GridView;

    invoke-virtual {v2, v1}, Landroid/widget/GridView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetGridView:Landroid/widget/GridView;

    invoke-virtual {v1, v6}, Landroid/widget/GridView;->setBackgroundColor(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetGridView:Landroid/widget/GridView;

    invoke-virtual {v1, v6}, Landroid/widget/GridView;->setCacheColorHint(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetGridView:Landroid/widget/GridView;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setVerticalScrollBarEnabled(Z)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetGridView:Landroid/widget/GridView;

    invoke-virtual {v1, v6}, Landroid/widget/GridView;->setHorizontalScrollBarEnabled(Z)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetGridView:Landroid/widget/GridView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setVerticalSpacing(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetGridView:Landroid/widget/GridView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setHorizontalSpacing(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetGridView:Landroid/widget/GridView;

    invoke-virtual {v1, v6}, Landroid/widget/GridView;->setFocusable(Z)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetGridView:Landroid/widget/GridView;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/widget/GridView;->setNumColumns(I)V

    new-instance v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetTextView:Landroid/widget/TextView;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x434c0000    # 204.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {v1, v2, v7}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41000000    # 8.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-virtual {v1, v2, v6, v6, v6}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetTextView:Landroid/widget/TextView;

    sget-object v2, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetTextView:Landroid/widget/TextView;

    invoke-static {v8, v8, v8}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41680000    # 14.5f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v6, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetTextView:Landroid/widget/TextView;

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v3, "string_no_preset"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setFocusable(Z)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetTextView:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v3, "string_no_preset"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42c80000    # 100.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-virtual {v1, v6, v2, v6, v6}, Landroid/widget/TextView;->setPadding(IIII)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetGridView:Landroid/widget/GridView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method private presetTypeButton()Landroid/view/View;
    .locals 12

    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v10, 0x2

    const/4 v9, -0x1

    const/4 v8, 0x1

    const/4 v7, 0x0

    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42fa0000    # 125.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    add-int/lit8 v2, v2, -0x9

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x41f00000    # 30.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/16 v2, 0x9

    invoke-virtual {v1, v7, v7, v2, v7}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v0, v8}, Landroid/widget/LinearLayout;->setOrientation(I)V

    new-instance v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v9, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    sget v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSdkVersion:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->tabLinePath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->tabLineSelectPath:Ljava/lang/String;

    sget-object v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->tabLineSelectedFocusPath:Ljava/lang/String;

    sget-object v6, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->tabLineUnselectedFocusPath:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectedFocusImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setSingleLine(Z)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v3, "string_preset"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x3f000000    # 0.5f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v4, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v1, v2, v3, v4, v9}, Landroid/widget/TextView;->setShadowLayer(FFFI)V

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v3, "string_preset_tab"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v3, "string_not_selected"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    const/4 v1, 0x3

    new-array v1, v1, [[I

    new-array v2, v10, [I

    const v3, -0x10100a7

    aput v3, v2, v7

    const v3, -0x10100a1

    aput v3, v2, v8

    aput-object v2, v1, v7

    new-array v2, v8, [I

    const v3, 0x10100a7    # android.R.attr.state_pressed

    aput v3, v2, v7

    aput-object v2, v1, v8

    new-array v2, v8, [I

    const v3, 0x10100a1    # android.R.attr.state_selected

    aput v3, v2, v7

    aput-object v2, v1, v10

    const/4 v2, 0x3

    new-array v2, v2, [I

    invoke-static {v7, v7, v7}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v2, v7

    const/16 v3, 0x1c

    const/16 v4, 0x7e

    const/16 v5, 0xc4

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v2, v8

    const/16 v3, 0x1c

    const/16 v4, 0x7e

    const/16 v5, 0xc4

    invoke-static {v3, v4, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    aput v3, v2, v10

    new-instance v3, Landroid/content/res/ColorStateList;

    invoke-direct {v3, v1, v2}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setClickable(Z)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;

    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setFocusable(Z)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;

    invoke-virtual {v1, v7, v7, v7, v7}, Landroid/widget/TextView;->setPadding(IIII)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;

    const v2, 0xb82ff9

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setNextFocusUpId(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->tabLinePath:Ljava/lang/String;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->tabLineSelectPath:Ljava/lang/String;

    sget-object v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->tabLineSelectedFocusPath:Ljava/lang/String;

    sget-object v6, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->tabLineUnselectedFocusPath:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectedFocusImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0
.end method

.method private preview()Landroid/view/View;
    .locals 9

    const/16 v8, 0x8

    const/4 v7, 0x0

    const/4 v6, -0x1

    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42960000    # 75.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {v1, v6, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v0, v7}, Landroid/widget/RelativeLayout;->setBackgroundColor(I)V

    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->setPenPlugin(Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;)V

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x2

    invoke-direct {v1, v6, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v3, -0x3f466666    # -5.8f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, -0x40800000    # -1.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, -0x3fc00000    # -3.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-virtual {v1, v2, v3, v4, v7}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    invoke-virtual {v2, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    invoke-virtual {v1, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->setBackgroundColor(I)V

    new-instance v1, Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaPreview:Landroid/widget/RelativeLayout;

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41200000    # 10.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {v1, v6, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaPreview:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41900000    # 18.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x40000000    # 2.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x40a00000    # 5.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    sget v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSdkVersion:I

    const/16 v2, 0x10

    if-ge v1, v2, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaPreview:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->previewAlphaPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaPreview:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v8}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaPreview:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->advancedSettingButton()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mAdvancedSettingButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mAdvancedSettingButton:Landroid/view/View;

    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mAdvancedSettingButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaPreview:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->previewAlphaPath:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private resetBeautifyAdvanceDataAndUpdateSeekBarUi(I)V
    .locals 2

    sget-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->BEAUTIFY_ADVANCE_DEFAULT_SETTING_VALUES:[[I

    aget-object v0, v0, p1

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->updateBeautifySeekBarsFromArray([I)V

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isFirstTime:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v1, 0x440ac000    # 555.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v1, 0x4416c000    # 603.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V

    goto :goto_0
.end method

.method private rotatePosition()V
    .locals 14

    const/high16 v0, 0x3f800000    # 1.0f

    const v13, 0x3f7d70a4    # 0.99f

    const/4 v12, 0x0

    const/4 v1, 0x0

    const-string/jumbo v2, "settingui-settingPen"

    const-string/jumbo v3, "==== SettingPen ===="

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v2, "settingui-settingPen"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "old  = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v2, "settingui-settingPen"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "new  = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMovableRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOldLocation:[I

    aget v2, v2, v12

    iput v2, v4, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOldLocation:[I

    const/4 v3, 0x1

    aget v2, v2, v3

    iput v2, v4, Landroid/graphics/Rect;->top:I

    iget v2, v4, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v4, Landroid/graphics/Rect;->right:I

    iget v2, v4, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getHeight()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v4, Landroid/graphics/Rect;->bottom:I

    const-string/jumbo v2, "settingui-settingPen"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "view = "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, v4, Landroid/graphics/Rect;->left:I

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v5, v4, Landroid/graphics/Rect;->top:I

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v5, v4, Landroid/graphics/Rect;->right:I

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v5, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v2, v4, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    int-to-float v5, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iget v3, v4, Landroid/graphics/Rect;->right:I

    sub-int/2addr v2, v3

    int-to-float v6, v2

    iget v2, v4, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v3

    int-to-float v7, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOldMovableRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    iget v3, v4, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v2, v3

    int-to-float v8, v2

    add-float v2, v5, v6

    div-float v3, v5, v2

    add-float v2, v7, v8

    div-float v2, v7, v2

    const-string/jumbo v9, "settingui-settingPen"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "left :"

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v10, ", right :"

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v9, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v5, "settingui-settingPen"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v9, "top :"

    invoke-direct {v6, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ", bottom :"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v5, "settingui-settingPen"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "hRatio = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ", vRatio = "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    cmpl-float v5, v3, v13

    if-lez v5, :cond_2

    move v3, v0

    :cond_0
    :goto_0
    cmpl-float v5, v2, v13

    if-lez v5, :cond_3

    move v1, v0

    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v2

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    if-ge v2, v5, :cond_4

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v5

    sub-int/2addr v2, v5

    int-to-float v2, v2

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    :goto_2
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    if-ge v2, v3, :cond_5

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    :goto_3
    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCurrentTopMargin:I

    const-string/jumbo v1, "settingui-settingPen"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "lMargin = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", tMargin = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void

    :cond_2
    cmpg-float v5, v3, v1

    if-gez v5, :cond_0

    move v3, v1

    goto :goto_0

    :cond_3
    cmpg-float v0, v2, v1

    if-ltz v0, :cond_1

    move v1, v2

    goto :goto_1

    :cond_4
    iput v12, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    goto :goto_2

    :cond_5
    iput v12, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    goto :goto_3
.end method

.method private setBeautifyAdvanceSeekbarColor(I)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCursiveSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDummySeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSustenanceSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mModulationSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    return-void
.end method

.method private setBeautifyAdvanceStringToCurrentAdvanceData(Ljava/lang/String;)V
    .locals 6

    const/16 v5, 0xa

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getBeautifyStyleBtnIndex(Ljava/lang/String;)I

    move-result v1

    const-string/jumbo v0, ";"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCurrentBeautifyStyle:I

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    array-length v0, v2

    if-ne v0, v5, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-lt v0, v5, :cond_2

    :cond_1
    return-void

    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCurrentBeautifyAdvanceSettingValues:[[I

    aget-object v3, v3, v1

    aget-object v4, v2, v0

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    aput v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private setBeautifyAdvancedDataToPlugin(II)V
    .locals 3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getPenAttribute(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCurrentBeautifyAdvanceSettingValues:[[I

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCurrentBeautifyStyle:I

    aget-object v0, v0, v1

    aput p2, v0, p1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCurrentBeautifyAdvanceSettingValues:[[I

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCurrentBeautifyStyle:I

    aget-object v0, v0, v1

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getBeautifyAdvanceArrayDataToString([I)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setAdvancedSetting(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private setButtonFocus(Landroid/view/View;)V
    .locals 6

    const/4 v5, 0x1

    const/high16 v4, 0x41600000    # 14.0f

    const/high16 v3, 0x41500000    # 13.0f

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;

    sget-object v1, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1, v5}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;

    sget-object v1, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;

    sget-object v1, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1, v5}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;

    sget-object v1, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    goto :goto_0
.end method

.method private setColorSelectorViewForBeautifyPen()V
    .locals 3

    const/4 v2, 0x0

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCurrentBeautifyStyle:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isBeautifyPen(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isSelected()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isMagicPenEnable:Z

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorSelectPickerLayout:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCursiveSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSustenanceSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDummySeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mModulationSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorSelectPickerLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getPenAttribute(I)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    shr-int/lit8 v0, v0, 0x18

    and-int/lit16 v0, v0, 0xff

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlpha:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaSeekbar:Landroid/widget/SeekBar;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlpha:I

    int-to-float v1, v1

    const/high16 v2, 0x437f0000    # 255.0f

    div-float/2addr v1, v2

    const/high16 v2, 0x42c60000    # 99.0f

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    const v2, 0xffffff

    and-int/2addr v1, v2

    const/high16 v2, -0x1000000

    or-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setBeautifyAdvanceSeekbarColor(I)V

    goto :goto_0
.end method

.method private setExpandBarPosition(I)V
    .locals 8

    const/high16 v7, 0x42640000    # 57.0f

    const/4 v6, 0x1

    const/4 v5, -0x1

    const/4 v3, -0x2

    const/4 v4, 0x0

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isFirstTime:Z

    if-nez v0, :cond_b

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isMinimumMode:Z

    if-nez v0, :cond_b

    const/16 v0, 0x34

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfFirstTime:I

    :goto_0
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isMagicPenEnable:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x434e0000    # 206.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    if-lt p1, v0, :cond_c

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x434e0000    # 206.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result p1

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v5, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyBg:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v5, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v2, 0x43a18000    # 323.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    invoke-virtual {v0, v4, v1, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIsMaxHeight:Z

    :cond_0
    :goto_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setColorSelectorViewForBeautifyPen()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isBeautifyPen(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isChinesePen(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    :cond_1
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSupportBeautifyPen:Z

    if-eqz v0, :cond_14

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->hasBeautifyPen()Z

    move-result v0

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isBeautifyPen(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCurrentBeautifyStyle:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_e

    if-gez p1, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v1, 0x43f28000    # 485.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result p1

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v1, 0x43f28000    # 485.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    if-lt p1, v0, :cond_d

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v1, 0x43f28000    # 485.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result p1

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v5, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyBg:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v5, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v2, 0x43f28000    # 485.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    invoke-virtual {v0, v4, v1, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIsMaxHeight:Z

    :cond_3
    :goto_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    if-lez v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    sub-int/2addr v0, v1

    if-lt p1, v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    sub-int p1, v0, v1

    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCurrentTopMargin:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    sub-int/2addr v0, v1

    if-lt p1, v0, :cond_5

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCurrentTopMargin:I

    if-eqz v0, :cond_5

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOnsizeChange:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCurrentTopMargin:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    sub-int p1, v0, v1

    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x42d20000    # 105.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    if-ge p1, v0, :cond_6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x42d20000    # 105.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result p1

    :cond_6
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mViewMode:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_7

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaSeekbarView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1a

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x43020000    # 130.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result p1

    :cond_7
    :goto_3
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mViewMode:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_8

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x43130000    # 147.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result p1

    :cond_8
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mViewMode:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_9

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x42d20000    # 105.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result p1

    :cond_9
    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayoutHeight:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayoutHeight:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41800000    # 16.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyBg:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayoutHeight:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41800000    # 16.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyBg:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaSeekbarView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1b

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPaletteBg:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x42ec0000    # 118.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v3, 0x43a18000    # 323.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayoutHeight:I

    sub-int/2addr v2, v3

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScrollY:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    :goto_4
    invoke-virtual {v0, v4, v4, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iget v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    if-ltz v1, :cond_a

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPaletteBg:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_a
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayoutHeight:I

    invoke-virtual {v0, v4, v1, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOnsizeChange:Z

    return-void

    :cond_b
    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfFirstTime:I

    goto/16 :goto_0

    :cond_c
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIsMaxHeight:Z

    goto/16 :goto_1

    :cond_d
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIsMaxHeight:Z

    goto/16 :goto_2

    :cond_e
    if-gez p1, :cond_f

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfMiniMode:I

    rsub-int v1, v1, 0x25b

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfFirstTime:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result p1

    :cond_f
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfMiniMode:I

    rsub-int v1, v1, 0x25b

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfFirstTime:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    if-lt p1, v0, :cond_10

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfMiniMode:I

    rsub-int v1, v1, 0x25b

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfFirstTime:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result p1

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v5, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyBg:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v5, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfMiniMode:I

    rsub-int v2, v2, 0x25b

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfFirstTime:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    invoke-virtual {v0, v4, v1, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIsMaxHeight:Z

    goto/16 :goto_2

    :cond_10
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIsMaxHeight:Z

    goto/16 :goto_2

    :cond_11
    if-gez p1, :cond_12

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfMiniMode:I

    rsub-int v1, v1, 0x138

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfFirstTime:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result p1

    :cond_12
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfMiniMode:I

    rsub-int v1, v1, 0x138

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfFirstTime:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    if-lt p1, v0, :cond_13

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfMiniMode:I

    rsub-int v1, v1, 0x138

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfFirstTime:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result p1

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v5, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyBg:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v5, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfMiniMode:I

    rsub-int v2, v2, 0x138

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfFirstTime:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    invoke-virtual {v0, v4, v1, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIsMaxHeight:Z

    goto/16 :goto_2

    :cond_13
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIsMaxHeight:Z

    goto/16 :goto_2

    :cond_14
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaSeekbarView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_17

    if-gez p1, :cond_15

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfMiniMode:I

    rsub-int v1, v1, 0x143

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfFirstTime:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result p1

    :cond_15
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfMiniMode:I

    rsub-int v1, v1, 0x143

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfFirstTime:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    if-lt p1, v0, :cond_16

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfMiniMode:I

    rsub-int v1, v1, 0x143

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfFirstTime:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result p1

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v5, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyBg:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v5, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfMiniMode:I

    rsub-int v2, v2, 0x143

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfFirstTime:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    invoke-virtual {v0, v4, v1, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIsMaxHeight:Z

    goto/16 :goto_2

    :cond_16
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isMagicPenEnable:Z

    if-nez v0, :cond_3

    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIsMaxHeight:Z

    goto/16 :goto_2

    :cond_17
    if-gez p1, :cond_18

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfMiniMode:I

    rsub-int v1, v1, 0x111

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfFirstTime:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result p1

    :cond_18
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfMiniMode:I

    rsub-int v1, v1, 0x111

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfFirstTime:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    if-lt p1, v0, :cond_19

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfMiniMode:I

    rsub-int v1, v1, 0x111

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfFirstTime:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result p1

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v5, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyBg:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v5, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfMiniMode:I

    rsub-int v2, v2, 0x111

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfFirstTime:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    invoke-virtual {v0, v4, v1, v4, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIsMaxHeight:Z

    goto/16 :goto_2

    :cond_19
    iput-boolean v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIsMaxHeight:Z

    goto/16 :goto_2

    :cond_1a
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x42a00000    # 80.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result p1

    goto/16 :goto_3

    :cond_1b
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPaletteBg:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x42ec0000    # 118.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v3, 0x43888000    # 273.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayoutHeight:I

    sub-int/2addr v2, v3

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScrollY:I

    add-int/2addr v1, v2

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    goto/16 :goto_4
.end method

.method private setListener()V
    .locals 4

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mTitleLayout:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mTitleLayout:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOnTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mExitButton:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mExitButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mExitButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_20

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeSeekbar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeSeekbar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeSeekbar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaSeekbar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaSeekbar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaSeekbar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaSeekbar:Landroid/widget/SeekBar;

    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$80;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$80;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPickerColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView$onColorChangedListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setColorPickerColorChangeListener(Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView$onColorChangedListener;)V

    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomLayout:Landroid/view/View;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomLayout:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mExpendBarListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomLayout:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mExpendBarHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizePlusButton:Landroid/view/View;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizePlusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizePlusButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizePlusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizePlusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizePlusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizePlusButtonTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizePlusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizePlusButtonKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    :cond_7
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeMinusButton:Landroid/view/View;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeMinusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeMinusButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeMinusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeMinusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeMinusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeMinusButtonTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeMinusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeMinusButtonKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    :cond_8
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOpacityPlusButton:Landroid/view/View;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOpacityPlusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaPlusButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOpacityPlusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaPlusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOpacityPlusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaPlusButtonTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOpacityPlusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaPlusButtonKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    :cond_9
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOpacityMinusButton:Landroid/view/View;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOpacityMinusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaMinusButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOpacityMinusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaMinusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOpacityMinusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaMinusButtonTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOpacityMinusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaMinusButtonKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    :cond_a
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPaletteRightButton:Landroid/view/View;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPaletteRightButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPaletteNextButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_b
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPaletteLeftButton:Landroid/view/View;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPaletteLeftButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPaletteBackButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_c
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetAddButton:Landroid/view/View;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetAddButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPreSetAddButtonListner:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_d
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mAdvancedSettingButton:Landroid/view/View;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mAdvancedSettingButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mAdvancedSettingButtonListner:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_e
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOnClickPresetItemListener:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$OnClickPresetItemListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->OnClickPresetItemListener(Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter$OnClickPresetItemListener;)V

    :cond_f
    new-instance v0, Landroid/view/GestureDetector;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mGestureListener:Landroid/view/GestureDetector$OnGestureListener;

    invoke-direct {v0, v1, v3}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mGestureDetector:Landroid/view/GestureDetector;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyEnableSwitchView:Landroid/widget/Switch;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyEnableSwitchView:Landroid/widget/Switch;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyEnablecheckedChangeListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    :cond_10
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyStyleBtnViews:Ljava/util/ArrayList;

    if-eqz v0, :cond_11

    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyStyleBtnViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v2, v0, :cond_22

    :cond_11
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveSeekbar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveSeekbar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveSeekbar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveKeyListner:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    :cond_12
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceSeekbar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_13

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceSeekbar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceSeekbar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceKeyListner:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    :cond_13
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummySeekbar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummySeekbar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummySeekbar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyKeyListner:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    :cond_14
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationSeekbar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationSeekbar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationChangeListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationSeekbar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationKeyListner:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    :cond_15
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursivePlusButton:Landroid/view/View;

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursivePlusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursivePlusButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursivePlusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursivePlusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursivePlusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursivePlusButtonTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_16
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenancePlusButton:Landroid/view/View;

    if-eqz v0, :cond_17

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenancePlusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenancePlusButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenancePlusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenancePlusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenancePlusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenancePlusButtonTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_17
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyPlusButton:Landroid/view/View;

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyPlusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyPlusButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyPlusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyPlusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyPlusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyPlusButtonTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_18
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationPlusButton:Landroid/view/View;

    if-eqz v0, :cond_19

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationPlusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationPlusButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationPlusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationPlusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationPlusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationPlusButtonTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_19
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveMinusButton:Landroid/view/View;

    if-eqz v0, :cond_1a

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveMinusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveMinusButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveMinusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveMinusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveMinusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveMinusButtonTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_1a
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceMinusButton:Landroid/view/View;

    if-eqz v0, :cond_1b

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceMinusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceMinusButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceMinusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceMinusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceMinusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceMinusButtonTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_1b
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyMinusButton:Landroid/view/View;

    if-eqz v0, :cond_1c

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyMinusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyMinusButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyMinusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyMinusButtonLongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyMinusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyMinusButtonTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_1c
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationMinusButton:Landroid/view/View;

    if-eqz v0, :cond_1d

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationMinusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationMinusButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationMinusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationMinusButtonlongClickListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationMinusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationMinusButtonTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_1d
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceResetButton:Landroid/widget/Button;

    if-eqz v0, :cond_1e

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceResetButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceResetButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1e
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    if-eqz v0, :cond_1f

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScrollViewListner:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView$scrollChangedListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->setOnScrollChangedListener(Lcom/samsung/android/sdk/pen/settingui/SpenScrollView$scrollChangedListener;)V

    :cond_1f
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->horizontalScrollViewTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void

    :cond_20
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_21

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeListner:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_21
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    :cond_22
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyStyleBtnViews:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyStyleBtnsListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1
.end method

.method private setMagicPenMode(I)V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isMagicPenEnable:Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorSelectPickerLayout:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaSeekbarView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    const v0, 0x1869f

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x434e0000    # 206.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V

    goto :goto_0
.end method

.method private showAdvanceSettingLayout(Z)V
    .locals 2

    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSupportBeautifyPen:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->hasBeautifyPen()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSettingLayout:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSettingLayout:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v0, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSettingLayout:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private showBeautifyEnableLayout(Z)V
    .locals 2

    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSupportBeautifyPen:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->hasBeautifyPen()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyEnableLayout:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyEnableLayout:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v0, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyEnableLayout:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private showBeautifySettingViews(Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->showBeautifyStyleBtnsLayout(Z)V

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->showAdvanceSettingLayout(Z)V

    return-void
.end method

.method private showBeautifyStyleBtnsLayout(Z)V
    .locals 2

    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSupportBeautifyPen:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->hasBeautifyPen()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyStyleBtnsLayout:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyStyleBtnsLayout:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eq v1, v0, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyStyleBtnsLayout:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private tabLine()Landroid/view/View;
    .locals 6

    const/4 v5, -0x1

    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x41f00000    # 30.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->tabBorderLinePath:Ljava/lang/String;

    invoke-virtual {v2, v1, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method private titleBg()Landroid/view/View;
    .locals 9

    const/16 v8, 0xa

    const/4 v7, 0x1

    const v6, 0x42fa999a    # 125.3f

    const/4 v5, -0x1

    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v2, v3, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-boolean v7, v2, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/16 v3, 0x9

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v2, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v2, Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIndicator:Landroid/widget/ImageView;

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v3, -0x2

    invoke-direct {v2, v3, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIndicator:Landroid/widget/ImageView;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v2, Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v4, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-direct {v3, v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-boolean v7, v3, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/16 v4, 0xb

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v3, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->titleLeftPath:Ljava/lang/String;

    invoke-virtual {v3, v1, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIndicator:Landroid/widget/ImageView;

    sget-object v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->titleCenterPath:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    sget-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->titleRightPath:Ljava/lang/String;

    invoke-virtual {v3, v2, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIndicator:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    return-object v0
.end method

.method private titleLayout()Landroid/view/ViewGroup;
    .locals 5

    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x42240000    # 41.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->exitButton()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mExitButton:Landroid/view/View;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->lineButton2()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mLine2Button:Landroid/view/View;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->presetAddButton()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetAddButton:Landroid/view/View;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->lineButton1()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mLine1Button:Landroid/view/View;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->titleBg()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->titleText()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mLine1Button:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mLine2Button:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mExitButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetAddButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetAddButton:Landroid/view/View;

    const v2, 0xb82ff9

    invoke-virtual {v1, v2}, Landroid/view/View;->setId(I)V

    return-object v0
.end method

.method private titleText()Landroid/view/View;
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, -0x1

    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mTitleView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mTitleView:Landroid/widget/TextView;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mTitleView:Landroid/widget/TextView;

    const/16 v1, 0x13

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setFocusable(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mTitleView:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setSingleLine(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mTitleView:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mTitleView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_pen_settings"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mTitleView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41800000    # 16.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mTitleView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_pen_settings"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mTitleView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41200000    # 10.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    add-int/lit8 v1, v1, 0x9

    const/16 v2, 0xf

    invoke-virtual {v0, v1, v2, v3, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mTitleView:Landroid/widget/TextView;

    return-object v0
.end method

.method private totalLayout()V
    .locals 3

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x437a0000    # 250.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setOrientation(I)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->titleLayout()Landroid/view/ViewGroup;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mTitleLayout:Landroid/view/View;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->bodyLayout()Landroid/widget/RelativeLayout;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayout:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mTitleLayout:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->addView(Landroid/view/View;)V

    return-void
.end method

.method private typeSelectorlayout()Landroid/widget/LinearLayout;
    .locals 5

    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x437a0000    # 250.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    add-int/lit8 v2, v2, -0xa

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x41f00000    # 30.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->penTypeButton()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButtonLayout:Landroid/view/View;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->tabLine()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mTablineLayout:Landroid/view/View;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->presetTypeButton()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetTypeLayout:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButtonLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mTablineLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetTypeLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method private updateBeautifySeekBars(IIII)V
    .locals 5

    const/16 v0, 0x64

    const/16 v2, 0x14

    const/16 v3, 0x10

    const/16 v4, 0xc

    const/4 v1, 0x0

    if-ge v4, p1, :cond_8

    move p1, v4

    :cond_0
    :goto_0
    if-ge v3, p2, :cond_9

    move p2, v3

    :cond_1
    :goto_1
    if-ge v2, p3, :cond_a

    move p3, v2

    :cond_2
    :goto_2
    if-ge v0, p4, :cond_b

    move p4, v0

    :cond_3
    :goto_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveSeekbar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setProgress(I)V

    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceSeekbar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p2}, Landroid/widget/SeekBar;->setProgress(I)V

    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummySeekbar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummySeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p3}, Landroid/widget/SeekBar;->setProgress(I)V

    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationSeekbar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p4}, Landroid/widget/SeekBar;->setProgress(I)V

    :cond_7
    return-void

    :cond_8
    if-gez p1, :cond_0

    move p1, v1

    goto :goto_0

    :cond_9
    if-gez p2, :cond_1

    move p2, v1

    goto :goto_1

    :cond_a
    if-gez p3, :cond_2

    move p3, v1

    goto :goto_2

    :cond_b
    if-gez p4, :cond_3

    move p4, v1

    goto :goto_3
.end method

.method private updateBeautifySeekBarsFromArray([I)V
    .locals 4

    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getBeautifyAdvanceParamDataFromArray([II)I

    move-result v0

    const/4 v1, 0x3

    invoke-direct {p0, p1, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getBeautifyAdvanceParamDataFromArray([II)I

    move-result v1

    const/4 v2, 0x4

    invoke-direct {p0, p1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getBeautifyAdvanceParamDataFromArray([II)I

    move-result v2

    const/4 v3, 0x6

    invoke-direct {p0, p1, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getBeautifyAdvanceParamDataFromArray([II)I

    move-result v3

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->updateBeautifySeekBars(IIII)V

    return-void
.end method

.method private updateBeautifySeekBarsFromString(Ljava/lang/String;)V
    .locals 4

    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getBeautifyAdvanceParamDataFromString(Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x3

    invoke-direct {p0, p1, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getBeautifyAdvanceParamDataFromString(Ljava/lang/String;I)I

    move-result v1

    const/4 v2, 0x4

    invoke-direct {p0, p1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getBeautifyAdvanceParamDataFromString(Ljava/lang/String;I)I

    move-result v2

    const/4 v3, 0x6

    invoke-direct {p0, p1, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getBeautifyAdvanceParamDataFromString(Ljava/lang/String;I)I

    move-result v3

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->updateBeautifySeekBars(IIII)V

    return-void
.end method

.method private updateBeautifySettingData()V
    .locals 3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->loadPenPlugin(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getSize()F

    move-result v0

    iput v0, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getColor()I

    move-result v0

    iput v0, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getPenAttribute(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getAdvancedSetting()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getPenSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    goto/16 :goto_0

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    const-string/jumbo v1, ""

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    goto :goto_1
.end method

.method private updateBeautifyStyleBtnFromString(Ljava/lang/String;)V
    .locals 5

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getBeautifyStyleBtnIndex(Ljava/lang/String;)I

    move-result v3

    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCurrentBeautifyStyle:I

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyStyleBtnViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyStyleBtnViews:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    if-ne v3, v1, :cond_1

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Landroid/widget/ImageButton;->setSelected(Z)V

    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto :goto_1
.end method


# virtual methods
.method public close()V
    .locals 5

    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->savePreferences()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    if-eqz v0, :cond_1

    move v1, v2

    :goto_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginCount:I

    if-lt v1, v0, :cond_c

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_e

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenDataList:Ljava/util/List;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenDataList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenDataList:Ljava/util/List;

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetDataList:Ljava/util/List;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetDataList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetDataList:Ljava/util/List;

    :cond_4
    sget-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetInfoList:Ljava/util/List;

    if-eqz v0, :cond_5

    move v1, v2

    :goto_3
    sget-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetInfoList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v1, v0, :cond_f

    sget-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetInfoList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    sput-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetInfoList:Ljava/util/List;

    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->close()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->close()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    :cond_7
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeLayout:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaSeekbar:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeSeekbar:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->close()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    :cond_8
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->close()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    :cond_9
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaSeekbarView:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaSeekbarView:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomLayout:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mExitButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mExitButton:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeSeekbarView:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeSeekbarView:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPreviewLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPreviewLayout:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorSelectPickerLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorSelectPickerLayout:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSeekbarLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSeekbarLayout:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyEnableTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyEnableTextView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyEnableSwitchView:Landroid/widget/Switch;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyEnableSwitchView:Landroid/widget/Switch;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyEnableLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyEnableLayout:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyStyleBtnViews:Ljava/util/ArrayList;

    if-eqz v0, :cond_a

    :goto_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyStyleBtnViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v2, v0, :cond_10

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyStyleBtnViews:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyStyleBtnViews:Ljava/util/ArrayList;

    :cond_a
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyStyleBtnsLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyStyleBtnsLayout:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyCursiveTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyCursiveTextView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifySustenanceTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifySustenanceTextView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyDummyTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyDummyTextView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyModulationTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyModulationTextView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveSeekbar:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceSeekbar:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummySeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummySeekbar:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationSeekbar:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursivePlusButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursivePlusButton:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenancePlusButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenancePlusButton:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyPlusButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyPlusButton:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationPlusButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationPlusButton:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveMinusButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceCursiveMinusButton:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceMinusButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSustenanceMinusButton:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyMinusButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceDummyMinusButton:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationMinusButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceModulationMinusButton:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceResetButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceResetButton:Landroid/widget/Button;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSettingLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyAdvanceSettingLayout:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetGridView:Landroid/widget/GridView;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetGridView:Landroid/widget/GridView;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetTextView:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetLayout:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetAddButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetAddButton:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPickerColorImage:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPickerColorImage:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPickerCurrentColor:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPickerCurrentColor:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPickerSettingExitButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPickerSettingExitButton:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeHorizontalScrollView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeHorizontalScrollView:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mTitleLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mTitleLayout:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyBg:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyBg:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayout:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->close()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;

    :cond_b
    sput-object v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDefaultPath:Ljava/lang/String;

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCursiveSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSustenanceSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDummySeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mModulationSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mGestureDetector:Landroid/view/GestureDetector;

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$PresetListener;

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$ActionListener;

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mVisibilityListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$ViewListener;

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->close()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    goto/16 :goto_0

    :cond_c
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    invoke-interface {v0, v4}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setBitmap(Landroid/graphics/Bitmap;)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->unloadPlugin(Ljava/lang/Object;)V

    :cond_d
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_1

    :cond_e
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->close()V

    goto/16 :goto_2

    :cond_f
    sget-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetInfoList:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->close()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_3

    :cond_10
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyStyleBtnViews:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_4
.end method

.method protected drawExpendImage(Ljava/lang/String;)V
    .locals 8

    const/16 v7, 0x3e8

    const/4 v6, 0x1

    const/16 v5, 0x8

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->getPenPluginIndexByPenName(Ljava/lang/String;)I

    move-result v1

    const/4 v0, -0x1

    if-eq v1, v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->loadPenPlugin(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    if-eqz v0, :cond_0

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isSelected()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setColorSelectorViewForBeautifyPen()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isBeautifyPen(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isChinesePen(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    :cond_3
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSupportBeautifyPen:Z

    if-eqz v0, :cond_d

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->hasBeautifyPen()Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isBeautifyPen(Ljava/lang/String;)Z

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyEnableSwitchView:Landroid/widget/Switch;

    invoke-virtual {v1}, Landroid/widget/Switch;->isChecked()Z

    move-result v1

    if-eq v1, v0, :cond_4

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyEnableSwitchView:Landroid/widget/Switch;

    invoke-virtual {v1, v0}, Landroid/widget/Switch;->setChecked(Z)V

    :cond_4
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->isSelected()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-direct {p0, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->showBeautifyEnableLayout(Z)V

    :cond_5
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyStyleBtnsLayout:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-eqz v1, :cond_7

    if-eqz v0, :cond_6

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->isSelected()Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->showBeautifyStyleBtnsLayout(Z)V

    :cond_6
    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->showAdvanceSettingLayout(Z)V

    :cond_7
    if-eqz v0, :cond_8

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->updateBeautifyStyleBtnFromString(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->updateBeautifySeekBarsFromString(Ljava/lang/String;)V

    :cond_8
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaSeekbarView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_9

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaSeekbarView:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    :cond_9
    if-eqz v0, :cond_b

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isFirstTime:Z

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v1, 0x440ac000    # 555.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V

    goto/16 :goto_0

    :cond_a
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v1, 0x4416c000    # 603.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V

    goto/16 :goto_0

    :cond_b
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isFirstTime:Z

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x43840000    # 264.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V

    goto/16 :goto_0

    :cond_c
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x439c0000    # 312.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V

    goto/16 :goto_0

    :cond_d
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    invoke-interface {v0, v6}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getPenAttribute(I)Z

    move-result v0

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaSeekbarView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_f

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyEnableLayout:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_e

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->showBeautifyEnableLayout(Z)V

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->showBeautifySettingViews(Z)V

    :cond_e
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayoutHeight:I

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V

    goto/16 :goto_0

    :cond_f
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaSeekbarView:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyEnableLayout:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_10

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->showBeautifyEnableLayout(Z)V

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->showBeautifySettingViews(Z)V

    :cond_10
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayoutHeight:I

    if-gtz v0, :cond_11

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v1, 0x43a18000    # 323.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V

    goto/16 :goto_0

    :cond_11
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayoutHeight:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfMiniMode:I

    rsub-int v2, v2, 0x111

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfFirstTime:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    if-lt v0, v1, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mWindowHeight:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayoutHeight:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42d60000    # 107.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    add-int/2addr v1, v2

    if-le v0, v1, :cond_13

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayoutHeight:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x42480000    # 50.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    add-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V

    :cond_12
    :goto_1
    invoke-direct {p0, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V

    goto/16 :goto_0

    :cond_13
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mViewMode:I

    if-ne v0, v5, :cond_12

    invoke-direct {p0, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V

    goto :goto_1

    :cond_14
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaSeekbarView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-ne v0, v5, :cond_15

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyEnableLayout:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-ne v0, v5, :cond_15

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->showBeautifyEnableLayout(Z)V

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->showBeautifySettingViews(Z)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayoutHeight:I

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V

    goto/16 :goto_0

    :cond_15
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaSeekbarView:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->showBeautifyEnableLayout(Z)V

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->showBeautifySettingViews(Z)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayoutHeight:I

    if-gtz v0, :cond_16

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v1, 0x43888000    # 273.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V

    goto/16 :goto_0

    :cond_16
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayoutHeight:I

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V

    goto/16 :goto_0
.end method

.method public getInfo()Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    return-object v0
.end method

.method public getViewMode()I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mViewMode:I

    return v0
.end method

.method protected initView(Ljava/lang/String;)V
    .locals 7

    const/4 v2, 0x0

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    sget v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScale:F

    invoke-direct {v0, v1, v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;-><init>(Landroid/content/Context;Ljava/util/ArrayList;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSharedPreferencesManager:Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSharedPreferencesManager:Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->getPenDataList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenDataList:Ljava/util/List;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSharedPreferencesManager:Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->getPresetData()Ljava/util/ArrayList;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetInfoList:Ljava/util/List;

    sget-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetInfoList:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetInfoList:Ljava/util/List;

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenDataList:Ljava/util/List;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenDataList:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetDataList:Ljava/util/List;

    if-nez v0, :cond_2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetDataList:Ljava/util/List;

    :cond_2
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    sget-object v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetInfoList:Ljava/util/List;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    sget v6, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScale:F

    move-object v4, p1

    invoke-direct/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;-><init>(Landroid/content/Context;ILjava/util/List;Ljava/lang/String;Ljava/util/ArrayList;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->setPenPlugin(Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->totalLayout()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mTitleView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->EXIT_BUTTON_WIDTH:I

    mul-int/lit8 v3, v3, 0x2

    rsub-int v3, v3, 0xfa

    add-int/lit8 v3, v3, -0x8

    int-to-float v3, v3

    invoke-virtual {v1, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->findMinValue(Landroid/widget/TextView;I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetGridView:Landroid/widget/GridView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetGridView:Landroid/widget/GridView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeHorizontalScrollView:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->localPenTypeViewGroup:Landroid/widget/RelativeLayout;

    move v0, v2

    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->localPenTypeViewGroup:Landroid/widget/RelativeLayout;

    invoke-virtual {v1}, Landroid/widget/RelativeLayout;->getChildCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-lt v0, v1, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mTabSelectListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mTabSelectListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSelected(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSelected(Z)V

    :cond_4
    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->showBeautifySettingViews(Z)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->initColorSelecteView()V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->ColorPickerSettingInit()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetListAdapter:Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetListAdapter;->notifyDataSetChanged()V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->presetDisplay()V

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setVisibility(I)V

    return-void

    :cond_5
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->localPenTypeViewGroup:Landroid/widget/RelativeLayout;

    invoke-virtual {v3, v0}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public loadPreferences()V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSharedPreferencesManager:Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSharedPreferencesManager:Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->getCurrentPenName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->getPenPluginIndexByPenName(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSharedPreferencesManager:Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->removeCurrentPenData()V

    const/4 v0, 0x1

    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->penSelection(Landroid/view/View;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSharedPreferencesManager:Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->clearSharedPenData()V

    goto :goto_0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3

    const-string/jumbo v0, "settingui-settingPen"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onConfig pen "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getVisibility()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mNeedCalculateMargin:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCurrentTopMargin:I

    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOldMovableRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOldLocation:[I

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getLocationOnScreen([I)V

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIsRotated:Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->setRotation()V

    :cond_2
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected onScroll(Landroid/view/MotionEvent;)V
    .locals 0

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIsRotated2:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIsRotated2:Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    invoke-virtual {p0, p1, v0, p3, p4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->onSizeChanged(IIII)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->requestLayout()V

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_1
    iput p2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mWindowHeight:I

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mExpandFlag:Z

    if-nez v0, :cond_2

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayoutHeight:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x42640000    # 57.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    add-int/2addr v0, v1

    if-le v0, p2, :cond_2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mOnsizeChange:Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x42640000    # 57.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    sub-int v0, p2, v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V

    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->requestLayoutDisable:Z

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$85;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$85;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;->onSizeChanged(IIII)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 7

    const/4 v0, 0x0

    if-ne p1, p0, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mVisibilityListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$ViewListener;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mVisibilityListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$ViewListener;

    invoke-interface {v1, p2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$ViewListener;->onVisibilityChanged(I)V

    :cond_0
    sget-boolean v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isMagicPenRemoved:Z

    if-nez v1, :cond_1

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_4

    :cond_1
    if-ne p1, p0, :cond_3

    if-nez p2, :cond_3

    const/4 v0, 0x2

    new-array v0, v0, [I

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getLocationOnScreen([I)V

    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mNeedCalculateMargin:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x2

    new-array v1, v1, [I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->getLocationOnScreen([I)V

    const/4 v2, 0x0

    aget v2, v0, v2

    const/4 v3, 0x0

    aget v3, v1, v3

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mLeftMargin:I

    const/4 v2, 0x1

    aget v2, v0, v2

    const/4 v3, 0x1

    aget v3, v1, v3

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mTopMargin:I

    const/4 v2, 0x2

    new-array v2, v2, [I

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getRootView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    const/4 v3, 0x0

    aget v3, v1, v3

    const/4 v4, 0x0

    aget v4, v2, v4

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mLeftMargin:I

    add-int/2addr v3, v4

    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mTotalLeftMargin:I

    const/4 v3, 0x1

    aget v1, v1, v3

    const/4 v3, 0x1

    aget v2, v2, v3

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mTopMargin:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mTotalTopMargin:I

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mNeedCalculateMargin:Z

    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMovableRect:Landroid/graphics/Rect;

    new-instance v2, Landroid/graphics/Rect;

    const/4 v3, 0x0

    aget v3, v0, v3

    const/4 v4, 0x1

    aget v4, v0, v4

    const/4 v5, 0x0

    aget v5, v0, v5

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getWidth()I

    move-result v6

    add-int/2addr v5, v6

    const/4 v6, 0x1

    aget v0, v0, v6

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getHeight()I

    move-result v6

    add-int/2addr v0, v6

    invoke-direct {v2, v3, v4, v5, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->checkPosition()V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    :goto_1
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onVisibilityChanged(Landroid/view/View;I)V

    return-void

    :cond_4
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    const/16 v2, 0x3ed

    if-ne v0, v2, :cond_5

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->currenMagicPenHeight:I

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setMagicPenMode(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaPreview:Landroid/widget/RelativeLayout;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method protected onWindowVisibilityChanged(I)V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitSettings:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitSettings:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->hide()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    const/4 v1, 0x2

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPreCanvasPenAction:I

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setToolTypeAction(II)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    const/4 v1, 0x1

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPreCanvasFingerAction:I

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setToolTypeAction(II)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    const/4 v1, 0x3

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPreCanvasMouseAction:I

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setToolTypeAction(II)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_1
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onWindowVisibilityChanged(I)V

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public removePen(I)V
    .locals 6

    const/high16 v3, 0x43700000    # 240.0f

    const/high16 v5, 0x42960000    # 75.0f

    const/high16 v4, 0x428a0000    # 69.0f

    const/4 v0, 0x5

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isMagicPenRemoved:Z

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeHorizontalScrollView:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mNumberOfPenExist:I

    if-lt p1, v0, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mNumberOfPenExist:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mNumberOfPenExist:I

    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v2, 0xd

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->penTypeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeHorizontalScrollView:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeHorizontalScrollView:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->localPenTypeViewGroup:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->localPenTypeViewGroup:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getId()I

    move-result v0

    if-eqz p1, :cond_3

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->localPenTypeViewGroup:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, p1}, Landroid/widget/RelativeLayout;->removeViewAt(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->localPenTypeViewGroup:Landroid/widget/RelativeLayout;

    add-int/lit8 v2, p1, -0x1

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setId(I)V

    goto :goto_0

    :cond_3
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->localPenTypeViewGroup:Landroid/widget/RelativeLayout;

    add-int/lit8 v2, p1, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x9

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->localPenTypeViewGroup:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, p1}, Landroid/widget/RelativeLayout;->removeViewAt(I)V

    goto/16 :goto_0
.end method

.method public requestLayout()V
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->requestLayoutDisable:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/widget/LinearLayout;->requestLayout()V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->requestLayoutDisable:Z

    return-void
.end method

.method public savePreferences()V
    .locals 8

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSharedPreferencesManager:Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSharedPreferencesManager:Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSharedPreferencesManager:Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->setPenDataList(Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSharedPreferencesManager:Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->setCurrentPenName(Ljava/lang/String;)V

    sget-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetInfoList:Ljava/util/List;

    if-eqz v0, :cond_0

    :goto_2
    sget-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetInfoList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lt v2, v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSharedPreferencesManager:Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;

    sget-object v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetInfoList:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSharedPreferencesManager;->setPresetData(Ljava/util/List;)V

    goto :goto_0

    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v5

    if-nez v5, :cond_4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->loadPenPlugin(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v5

    if-nez v5, :cond_4

    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_4
    new-instance v5, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-direct {v5}, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;-><init>()V

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenName()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v6

    invoke-interface {v6}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getSize()F

    move-result v6

    iput v6, v5, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getColor()I

    move-result v0

    iput v0, v5, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_5
    sget-object v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetInfoList:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->setPresetOrder(I)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_2
.end method

.method public setActionListener(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$ActionListener;)V
    .locals 0

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$ActionListener;

    :cond_0
    return-void
.end method

.method public setBeautifyOptionEnabled(Z)V
    .locals 2

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->hasBeautifyPen()Z

    move-result v0

    if-eqz v0, :cond_2

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSupportBeautifyPen:Z

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyEnableSwitchView:Landroid/widget/Switch;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBeautifyEnableSwitchView:Landroid/widget/Switch;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setChecked(Z)V

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isChinesePen(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isBeautifyPen(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->drawExpendImage(Ljava/lang/String;)V

    :cond_2
    return-void
.end method

.method public setCanvasView(Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;)V
    .locals 3

    const/16 v2, 0x438

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$83;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$83;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V

    invoke-interface {p1, p0, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setBackgroundColorChangeListener(Ljava/lang/Object;Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-nez v0, :cond_5

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasWidth()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v1}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasHeight()I

    move-result v1

    if-ge v0, v1, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasWidth()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasSize:I

    :goto_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasSize:I

    if-nez v0, :cond_2

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasSize:I

    :cond_2
    :goto_2
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->loadPreferences()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasHeight()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasSize:I

    goto :goto_1

    :cond_4
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasSize:I

    goto :goto_2

    :cond_5
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    goto :goto_2
.end method

.method public setColorPickerPosition(II)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->movePosition(II)V

    :cond_0
    return-void
.end method

.method public setIndicatorPosition(I)V
    .locals 4

    const/4 v3, 0x0

    if-gez p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIndicator:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMoveSettingLayout:Z

    :goto_0
    return-void

    :cond_0
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMoveSettingLayout:Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIndicator:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x2

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, p1, v3, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mIndicator:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public setInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V
    .locals 11

    const/16 v5, 0x438

    const/4 v2, 0x0

    const/high16 v10, 0x41200000    # 10.0f

    const/4 v9, 0x1

    const-wide v7, 0x4076800000000000L    # 360.0

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "E_INVALID_ARG : parameter \'settingInfo\' is null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    iget-object v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->getPenPluginIndexByPenName(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_f

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v3, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    iput v3, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-boolean v3, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    iput-boolean v3, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v3, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v3, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    iput v3, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v3, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mContext:Landroid/content/Context;

    iget-object v4, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->loadPenPlugin(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    if-nez v0, :cond_1

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getMinSettingValue()F

    move-result v3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getMaxSettingValue()F

    move-result v4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasWidth()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v1}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasHeight()I

    move-result v1

    if-ge v0, v1, :cond_a

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasWidth()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasSize:I

    :goto_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasSize:I

    if-nez v0, :cond_2

    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasSize:I

    :cond_2
    :goto_2
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasSize:I

    int-to-float v0, v0

    mul-float/2addr v0, v4

    float-to-double v0, v0

    div-double/2addr v0, v7

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v5, v5, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    int-to-long v5, v5

    cmp-long v0, v0, v5

    if-gez v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasSize:I

    int-to-float v1, v1

    mul-float/2addr v1, v4

    float-to-double v5, v1

    div-double/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Math;->round(D)J

    move-result-wide v5

    long-to-float v1, v5

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    :cond_3
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasSize:I

    int-to-float v0, v0

    mul-float/2addr v0, v3

    float-to-double v0, v0

    div-double/2addr v0, v7

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v5, v5, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    int-to-long v5, v5

    cmp-long v0, v0, v5

    if-lez v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasSize:I

    int-to-float v1, v1

    mul-float/2addr v1, v3

    float-to-double v5, v1

    div-double/2addr v5, v7

    invoke-static {v5, v6}, Ljava/lang/Math;->round(D)J

    move-result-wide v5

    long-to-float v1, v5

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setSize(F)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    invoke-interface {v0, v9}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getPenAttribute(I)Z

    move-result v0

    if-eqz v0, :cond_5

    iget v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    shr-int/lit8 v0, v0, 0x18

    and-int/lit16 v0, v0, 0xff

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlpha:I

    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getPenAttribute(I)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setAdvancedSetting(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setBeautifyAdvanceStringToCurrentAdvanceData(Ljava/lang/String;)V

    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->isLoaded()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginInfoList:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v0, v9}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->setLoaded(Z)V

    :cond_7
    move v1, v2

    :goto_3
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginCount:I

    if-lt v1, v0, :cond_c

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isBeautifyPen(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    const-string/jumbo v1, "com.samsung.android.sdk.pen.pen.preload.ChineseBrush"

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->getPenPluginIndexByPenName(Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setSelected(Z)V

    :goto_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    const/high16 v2, 0x43b40000    # 360.0f

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasSize:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    sub-float/2addr v1, v3

    mul-float/2addr v1, v10

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeSeekbar:Landroid/widget/SeekBar;

    sub-float v3, v4, v3

    mul-float/2addr v3, v10

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/SeekBar;->setMax(I)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v2, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iput v0, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    const v2, 0xffffff

    and-int/2addr v1, v2

    const/high16 v2, -0x1000000

    or-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSizeSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setAlpha(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaSeekbar:Landroid/widget/SeekBar;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlpha:I

    int-to-float v1, v1

    const/high16 v2, 0x437f0000    # 255.0f

    div-float/2addr v1, v2

    const/high16 v2, 0x42c60000    # 99.0f

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaSeekbarColor:Landroid/graphics/drawable/GradientDrawable;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setColor(I)V

    sget-boolean v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->IS_COLOR_GRADATION_SELECT:Z

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    const/high16 v2, 0x41a00000    # 20.0f

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->selectColorForGradiation(ID)V

    :cond_8
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setBeautifyAdvanceSeekbarColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->setPenType(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->setStrokeSize(F)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->setStrokeColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->setStrokeAdvancedSetting(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenPreview:Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->invalidate()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->setColorPickerColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getPenSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    move-result-object v0

    if-eqz v0, :cond_9

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-boolean v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    iput-boolean v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    :cond_9
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->drawExpendImage(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_a
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasHeight()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasSize:I

    goto/16 :goto_1

    :cond_b
    iput v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasSize:I

    goto/16 :goto_2

    :cond_c
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v1, :cond_d

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    :cond_d
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_3

    :cond_e
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenNameIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v9}, Landroid/view/View;->setSelected(Z)V

    goto/16 :goto_4

    :cond_f
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "E_INVALID_ARG : parameter \'SettingPenInfo.name\' is incorrect."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setPenInfoList(Ljava/util/List;)V
    .locals 0

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenDataList:Ljava/util/List;

    :cond_0
    return-void
.end method

.method public setPosition(II)V
    .locals 1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iput p2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iput p2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCurrentTopMargin:I

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public setPresetListener(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$PresetListener;)V
    .locals 0

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$PresetListener;

    :cond_0
    return-void
.end method

.method public setViewMode(I)V
    .locals 11

    const/high16 v10, 0x42240000    # 41.0f

    const/high16 v9, 0x41800000    # 16.0f

    const/4 v8, 0x1

    const/16 v7, 0x8

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v4, "string_pen_tab"

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v4, "string_selected"

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v4, "string_preset_tab"

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v4, "string_not_selected"

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mViewMode:I

    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->requestLayoutDisable:Z

    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->requestLayoutDisable:Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitSettings:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->hide()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    const/4 v3, 0x2

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPreCanvasPenAction:I

    invoke-interface {v0, v3, v4}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setToolTypeAction(II)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPreCanvasFingerAction:I

    invoke-interface {v0, v8, v3}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setToolTypeAction(II)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    const/4 v3, 0x3

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPreCanvasMouseAction:I

    invoke-interface {v0, v3, v4}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setToolTypeAction(II)V

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setSelected(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSelected(Z)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mViewMode:I

    if-ne v0, v8, :cond_5

    const/16 v0, 0x34

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfMiniMode:I

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x1

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x42880000    # 68.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-direct {v0, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorSelectPickerLayout:Landroid/view/View;

    invoke-virtual {v3, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isMinimumMode:Z

    :goto_0
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x1

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x42ec0000    # 118.0f

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfMiniMode:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-direct {v0, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorSelectPickerLayout:Landroid/view/View;

    invoke-virtual {v3, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->setScrollingEnabled(Z)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mViewMode:I

    packed-switch v0, :pswitch_data_0

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mViewMode:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPreviewLayout:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSeekbarLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorSelectPickerLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetAddButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->drawExpendImage(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v0

    if-lez v0, :cond_f

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayoutHeight:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v3

    if-le v0, v3, :cond_f

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    sub-int/2addr v0, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    sub-int/2addr v0, v3

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V

    :goto_1
    :pswitch_0
    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->requestLayoutDisable:Z

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCount:I

    move v1, v2

    :goto_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_10

    :cond_1
    sget-boolean v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isHighlightPenRemoved:Z

    if-nez v0, :cond_2

    sget-boolean v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isMagicPenRemoved:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

    if-eqz v0, :cond_2

    const/4 v0, 0x5

    if-ge v1, v0, :cond_11

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42300000    # 44.0f

    invoke-virtual {v1, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    invoke-virtual {v0, v1, v2}, Landroid/widget/HorizontalScrollView;->scrollTo(II)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v0, v2}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    const/16 v0, 0xa

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x42300000    # 44.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    int-to-float v1, v1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->playScrollAnimation(IFF)V

    :cond_2
    :goto_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaPreview:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    int-to-double v1, v1

    const-wide/high16 v3, 0x4059000000000000L    # 100.0

    div-double/2addr v1, v3

    double-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setAlpha(F)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaSeekbar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaTextView:Landroid/widget/TextView;

    const-string/jumbo v1, "1%"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41c00000    # 24.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setX(F)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x40a00000    # 5.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setY(F)V

    :cond_3
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isMagicPenEnable:Z

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->currenMagicPenHeight:I

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setMagicPenMode(I)V

    :cond_4
    return-void

    :cond_5
    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfMiniMode:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setVisibility(I)V

    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isMinimumMode:Z

    goto/16 :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mTypeSelectorLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPreviewLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSeekbarLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorSelectPickerLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetAddButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->drawExpendImage(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v0

    if-lez v0, :cond_6

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayoutHeight:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v3

    if-le v0, v3, :cond_6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    sub-int/2addr v0, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    sub-int/2addr v0, v3

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V

    goto/16 :goto_1

    :cond_6
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isFirstTime:Z

    if-nez v0, :cond_7

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x1

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x42840000    # 66.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-direct {v0, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorSelectPickerLayout:Landroid/view/View;

    invoke-virtual {v3, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :goto_4
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayoutHeight:I

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V

    goto/16 :goto_1

    :cond_7
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setVisibility(I)V

    goto :goto_4

    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mTypeSelectorLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPreviewLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSeekbarLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorSelectPickerLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetAddButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->drawExpendImage(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaSeekbarView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x43450000    # 197.0f

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V

    goto/16 :goto_1

    :cond_8
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x43170000    # 151.0f

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V

    goto/16 :goto_1

    :pswitch_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mTypeSelectorLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPreviewLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSeekbarLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorSelectPickerLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetAddButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->drawExpendImage(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v0

    if-lez v0, :cond_9

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayoutHeight:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v3

    if-le v0, v3, :cond_9

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    sub-int/2addr v0, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    sub-int/2addr v0, v3

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V

    goto/16 :goto_1

    :cond_9
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isFirstTime:Z

    if-nez v0, :cond_a

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v3, -0x1

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x42840000    # 66.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-direct {v0, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorSelectPickerLayout:Landroid/view/View;

    invoke-virtual {v3, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :goto_5
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayoutHeight:I

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V

    goto/16 :goto_1

    :cond_a
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setVisibility(I)V

    goto :goto_5

    :pswitch_4
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isFirstTime:Z

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfFirstTime:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mTypeSelectorLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPreviewLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSeekbarLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorSelectPickerLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetAddButton:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->drawExpendImage(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v0

    if-lez v0, :cond_b

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayoutHeight:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v3

    if-le v0, v3, :cond_b

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    sub-int/2addr v0, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    sub-int/2addr v0, v3

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V

    goto/16 :goto_1

    :cond_b
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayoutHeight:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x42500000    # 52.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    add-int/2addr v0, v3

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V

    goto/16 :goto_1

    :pswitch_5
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isFirstTime:Z

    iput v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->deltaOfFirstTime:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mTypeSelectorLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPreviewLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSeekbarLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorSelectPickerLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetAddButton:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mLine1Button:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mTablineLayout:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x43700000    # 240.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x41f00000    # 30.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-direct {v0, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x40a00000    # 5.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-virtual {v0, v3, v2, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButtonLayout:Landroid/view/View;

    invoke-virtual {v3, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->drawExpendImage(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v0

    if-lez v0, :cond_c

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayoutHeight:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v3

    if-le v0, v3, :cond_c

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    sub-int/2addr v0, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    sub-int/2addr v0, v3

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V

    goto/16 :goto_1

    :cond_c
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayoutHeight:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x42500000    # 52.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    add-int/2addr v0, v3

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V

    goto/16 :goto_1

    :pswitch_6
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mTypeSelectorLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPreviewLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSeekbarLayout:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorSelectPickerLayout:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetAddButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->drawExpendImage(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42960000    # 75.0f

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->setScrollingEnabled(Z)V

    goto/16 :goto_1

    :pswitch_7
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mTypeSelectorLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPreviewLayout:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSeekbarLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorSelectPickerLayout:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetAddButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->drawExpendImage(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenAlphaSeekbarView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_d

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x43020000    # 130.0f

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V

    :goto_6
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->setScrollingEnabled(Z)V

    goto/16 :goto_1

    :cond_d
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42a00000    # 80.0f

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V

    goto :goto_6

    :pswitch_8
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mTypeSelectorLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPreviewLayout:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSeekbarLayout:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorGradationView:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorSelectPickerLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetAddButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->drawExpendImage(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x43130000    # 147.0f

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPaletteBg:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x43130000    # 147.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    iput v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPaletteBg:Landroid/view/View;

    invoke-virtual {v3, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mScrollView:Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenScrollView;->setScrollingEnabled(Z)V

    goto/16 :goto_1

    :pswitch_9
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v5, "string_pen_tab"

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v5, "string_not_selected"

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v5, "string_preset_tab"

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v5, "string_selected"

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mTypeSelectorLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v7}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPreviewLayout:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0, v7}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenSeekbarLayout:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorSelectPickerLayout:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPaletteView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;

    invoke-virtual {v0, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBottomLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetAddButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPresetButton:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->drawExpendImage(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v0

    if-lez v0, :cond_e

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayoutHeight:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v3

    if-le v0, v3, :cond_e

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    sub-int/2addr v0, v3

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    sub-int/2addr v0, v3

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V

    :goto_7
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorSelectPickerLayout:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    :cond_e
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayoutHeight:I

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V

    goto :goto_7

    :cond_f
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mBodyLayoutHeight:I

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V

    goto/16 :goto_1

    :cond_10
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeView:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-nez v0, :cond_1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_2

    :cond_11
    const/4 v0, 0x5

    if-ne v1, v0, :cond_2

    const/16 v0, 0xa

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenTypeHorizontalScrollView2:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v1}, Landroid/widget/HorizontalScrollView;->getScrollX()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42300000    # 44.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->playScrollAnimation(IFF)V

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method public setVisibility(I)V
    .locals 3

    if-nez p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mLoaded:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->loadImage()V

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    if-nez v0, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitSettings:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->mSpuitSettings:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout;->hide()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    const/4 v1, 0x2

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPreCanvasPenAction:I

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setToolTypeAction(II)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    const/4 v1, 0x1

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPreCanvasFingerAction:I

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setToolTypeAction(II)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    const/4 v1, 0x3

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPreCanvasMouseAction:I

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setToolTypeAction(II)V

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mPenButton:Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setButtonFocus(Landroid/view/View;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->isPresetClicked:Z

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setVisibilityChangedListener(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$ViewListener;)V
    .locals 0

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mVisibilityListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$ViewListener;

    :cond_0
    return-void
.end method
