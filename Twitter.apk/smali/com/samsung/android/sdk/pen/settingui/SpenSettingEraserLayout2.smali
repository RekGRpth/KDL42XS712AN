.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;
.super Landroid/widget/LinearLayout;
.source "Twttr"


# static fields
.field protected static final BODY_LAYOUT_HEIGHT:I = 0x90

.field protected static final ERASER_SIZE_MAX:I = 0x63

.field protected static final EXIT_BUTTON_RAW_HEIGHT:I = 0x24

.field protected static final EXIT_BUTTON_RAW_WIDTH:I = 0x26

.field protected static final LINE_BUTTON_RAW_HEIGHT:I = 0x11

.field protected static final LINE_BUTTON_RAW_WIDTH:I = 0x1

.field private static final REP_DELAY:I = 0x14

.field protected static final TITLE_LAYOUT_HEIGHT:I = 0x2a

.field public static final VIEW_MODE_NORMAL:I = 0x0

.field public static final VIEW_MODE_SIZE:I = 0x2

.field public static final VIEW_MODE_TITLE:I = 0x3

.field public static final VIEW_MODE_TYPE:I = 0x1

.field private static final bodyLeftPath:Ljava/lang/String; = "vienna_popup_bg"

.field private static final btnFocusPath:Ljava/lang/String; = "snote_popup_btn_focus"

.field private static final btnNoramlPath:Ljava/lang/String; = "snote_popup_btn_normal"

.field private static final btnPressPath:Ljava/lang/String; = "snote_popup_btn_press"

.field private static final eraserPopupDrawPress:Ljava/lang/String; = "snote_eraser_popup_draw_press"

.field private static final eraserPopupDrawUnselect:Ljava/lang/String; = "snote_eraser_popup_draw"

.field private static final eraserPopupTextPress:Ljava/lang/String; = "snote_eraser_popup_text_press"

.field private static final eraserPopupTextUnselect:Ljava/lang/String; = "snote_eraser_popup_text"

.field private static final handelFocusPath:Ljava/lang/String; = "progress_handle_focus"

.field private static final handelPath:Ljava/lang/String; = "progress_handle_normal"

.field private static final handelPressPath:Ljava/lang/String; = "progress_handle_press"

.field private static final minusBgFocusPath:Ljava/lang/String; = "snote_popup_progress_btn_minus_focus"

.field private static final minusBgPath:Ljava/lang/String; = "snote_popup_progress_btn_minus_normal"

.field private static final minusBgPressPath:Ljava/lang/String; = "snote_popup_progress_btn_minus_press"

.field private static final plusBgFocusPath:Ljava/lang/String; = "snote_popup_progress_btn_plus_focus"

.field private static final plusBgPath:Ljava/lang/String; = "snote_popup_progress_btn_plus_normal"

.field private static final plusBgPressPath:Ljava/lang/String; = "snote_popup_progress_btn_plus_press"

.field private static final popupMaxPath:Ljava/lang/String; = "snote_popup_arrow_max_normal"

.field private static final popupMinPath:Ljava/lang/String; = "snote_popup_arrow_min_normal"

.field private static final progressBgPath:Ljava/lang/String; = "progress_bg"

.field private static final progressShadowPath:Ljava/lang/String; = "progress_shadow"

.field private static final titleLeftPath:Ljava/lang/String; = "vienna_popup_title_bg"


# instance fields
.field protected EXIT_BUTTON_HEIGHT:I

.field protected EXIT_BUTTON_RIGHT_MARGIN:F

.field protected EXIT_BUTTON_TOP_MARGIN:F

.field protected EXIT_BUTTON_WIDTH:I

.field protected LINE_BUTTON_TOP_MARGIN:F

.field protected mActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$ActionListener;

.field private mAutoDecrement:Z

.field private mAutoIncrement:Z

.field protected mBodyLayout:Landroid/view/View;

.field protected mCanvasLayout:Landroid/widget/RelativeLayout;

.field protected mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

.field protected mClearAllButton:Landroid/view/View;

.field private final mClearAllListener:Landroid/view/View$OnClickListener;

.field protected mContext:Landroid/content/Context;

.field private mCount:I

.field protected mCurrentEraserType:I

.field protected mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

.field protected mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

.field private final mEraserKeyListener:Landroid/view/View$OnKeyListener;

.field protected mEraserListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$EventListener;

.field protected mEraserMinusButton:Landroid/view/View;

.field protected mEraserPlusButton:Landroid/view/View;

.field protected mEraserSizeButtonSeekbar:Landroid/view/ViewGroup;

.field private final mEraserSizeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field protected mEraserSizeSeekBar:Landroid/widget/SeekBar;

.field protected mEraserSizeTextView:Landroid/widget/TextView;

.field protected mEraserType01:Landroid/widget/ImageButton;

.field protected mEraserType02:Landroid/widget/ImageButton;

.field protected mEraserTypeLayout:Landroid/view/ViewGroup;

.field private final mEraserTypeListner:Landroid/view/View$OnClickListener;

.field protected mEraserTypeView:[Landroid/view/View;

.field protected mFirstLongPress:Z

.field private final mHandler:Landroid/os/Handler;

.field private mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

.field protected mLeftMargin:I

.field private final mLongClickMinusButtonListener:Landroid/view/View$OnLongClickListener;

.field private final mLongClickPlusButtonListener:Landroid/view/View$OnLongClickListener;

.field private final mMinusButtonListener:Landroid/view/View$OnClickListener;

.field protected mMovableRect:Landroid/graphics/Rect;

.field protected mMoveErasersettingLayout:Z

.field private final mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

.field private final mPlusButtonListener:Landroid/view/View$OnClickListener;

.field private final mPopupButtonListener:Landroid/view/View$OnClickListener;

.field private mPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$PopupListener;

.field protected mPopupMaxButton:Landroid/view/View;

.field protected mPopupMinButton:Landroid/view/View;

.field protected mScale:F

.field private mScrollTimer:Ljava/util/Timer;

.field protected mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

.field protected mSettingSizeLayout:Landroid/view/ViewGroup;

.field protected mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

.field protected mTitleLayout:Landroid/view/View;

.field protected mTopMargin:I

.field protected mTotalLeftMargin:I

.field protected mTotalTopMargin:I

.field private final mTouchMinusButtonListener:Landroid/view/View$OnTouchListener;

.field private final mTouchPlusButtonListener:Landroid/view/View$OnTouchListener;

.field protected mViewMode:I

.field protected mXDelta:I

.field protected mYDelta:I

.field private final repeatUpdateHandler:Landroid/os/Handler;

.field protected requestLayoutDisable:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/widget/RelativeLayout;)V
    .locals 4

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$PopupListener;

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->requestLayoutDisable:Z

    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mScale:F

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCurrentEraserType:I

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMoveErasersettingLayout:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mFirstLongPress:Z

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$EventListener;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$ActionListener;

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mViewMode:I

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$2;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mClearAllListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$3;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupButtonListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$4;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPlusButtonListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$5;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMinusButtonListener:Landroid/view/View$OnClickListener;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->repeatUpdateHandler:Landroid/os/Handler;

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mAutoIncrement:Z

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mAutoDecrement:Z

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$6;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$6;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mLongClickPlusButtonListener:Landroid/view/View$OnLongClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$7;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$7;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTouchPlusButtonListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$8;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$8;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mLongClickMinusButtonListener:Landroid/view/View$OnLongClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$9;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$9;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTouchMinusButtonListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$11;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$11;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeListner:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$12;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$12;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserKeyListener:Landroid/view/View$OnKeyListener;

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->requestLayoutDisable:Z

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-direct {v0, p1, p2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->initButtonValue()V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->initView()V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->setListener()V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/widget/RelativeLayout;F)V
    .locals 3

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$PopupListener;

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->requestLayoutDisable:Z

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mScale:F

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCurrentEraserType:I

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMoveErasersettingLayout:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mFirstLongPress:Z

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$EventListener;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$ActionListener;

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mViewMode:I

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mHandler:Landroid/os/Handler;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$2;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mClearAllListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$3;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupButtonListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$4;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPlusButtonListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$5;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$5;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMinusButtonListener:Landroid/view/View$OnClickListener;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->repeatUpdateHandler:Landroid/os/Handler;

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mAutoIncrement:Z

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mAutoDecrement:Z

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$6;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$6;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mLongClickPlusButtonListener:Landroid/view/View$OnLongClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$7;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$7;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTouchPlusButtonListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$8;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$8;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mLongClickMinusButtonListener:Landroid/view/View$OnLongClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$9;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$9;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTouchMinusButtonListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$10;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$11;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$11;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeListner:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$12;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$12;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserKeyListener:Landroid/view/View$OnKeyListener;

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->requestLayoutDisable:Z

    iput p4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mScale:F

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-direct {v0, p1, p2, p4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mContext:Landroid/content/Context;

    iput-object p3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->initButtonValue()V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->initView()V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->setListener()V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$PopupListener;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$PopupListener;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mAutoIncrement:Z

    return-void
.end method

.method static synthetic access$10(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)Ljava/util/Timer;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mScrollTimer:Ljava/util/Timer;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->repeatUpdateHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mAutoIncrement:Z

    return v0
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mAutoDecrement:Z

    return-void
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mAutoDecrement:Z

    return v0
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;Landroid/view/View;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->eraserTypeSetting(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$7(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$8(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCount:I

    return v0
.end method

.method static synthetic access$9(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;I)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCount:I

    return-void
.end method

.method private bodyBg()Landroid/view/ViewGroup;
    .locals 6

    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v4, 0x43a48000    # 329.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x43100000    # 144.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-direct {v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/4 v3, 0x1

    iput-boolean v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/16 v3, 0x9

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string/jumbo v3, "vienna_popup_bg"

    invoke-virtual {v2, v1, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method private bodyLayout()Landroid/view/ViewGroup;
    .locals 4

    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->bodyBg()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->eraserSettingClearAllButton()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mClearAllButton:Landroid/view/View;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->eraserTypeLayout()Landroid/view/ViewGroup;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeLayout:Landroid/view/ViewGroup;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->eraserSizeLayout()Landroid/view/ViewGroup;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mSettingSizeLayout:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeLayout:Landroid/view/ViewGroup;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mSettingSizeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mClearAllButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method private checkPosition()V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v0, 0x2

    new-array v1, v0, [I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x43640000    # 228.0f

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x43010000    # 129.0f

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->getLocationOnScreen([I)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    aget v4, v1, v6

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    if-ge v4, v5, :cond_0

    iput v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    :cond_0
    aget v4, v1, v7

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    if-ge v4, v5, :cond_1

    iput v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    :cond_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    aget v5, v1, v6

    sub-int/2addr v4, v5

    if-ge v4, v2, :cond_2

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    sub-int v2, v4, v2

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    if-gez v2, :cond_2

    iput v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    :cond_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    aget v1, v1, v7

    sub-int v1, v2, v1

    if-ge v1, v3, :cond_3

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    sub-int/2addr v1, v3

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    if-gez v1, :cond_3

    iput v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    :cond_3
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private eraserSettingClearAllButton()Landroid/view/View;
    .locals 10

    const/4 v9, 0x2

    const/high16 v8, 0x41300000    # 11.0f

    const/4 v7, 0x1

    const/4 v6, 0x0

    new-instance v0, Landroid/widget/Button;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v3, 0x43998000    # 307.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x423c0000    # 47.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x42ac0000    # 86.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x41800000    # 16.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v5, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v0, v7}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_clear_all"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    new-array v1, v9, [[I

    new-array v2, v7, [I

    const v3, 0x10100a7    # android.R.attr.state_pressed

    aput v3, v2, v6

    aput-object v2, v1, v6

    new-array v2, v6, [I

    aput-object v2, v1, v7

    new-array v2, v9, [I

    const/4 v3, -0x1

    aput v3, v2, v6

    const/high16 v3, -0x1000000

    aput v3, v2, v7

    new-instance v3, Landroid/content/res/ColorStateList;

    invoke-direct {v3, v1, v2}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setTextColor(Landroid/content/res/ColorStateList;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41600000    # 14.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v6, v1}, Landroid/widget/Button;->setTextSize(IF)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string/jumbo v2, "snote_popup_btn_normal"

    const-string/jumbo v3, "snote_popup_btn_press"

    const-string/jumbo v4, "snote_popup_btn_focus"

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private eraserSettingSizeSeekBar()Landroid/widget/SeekBar;
    .locals 12

    const/4 v11, 0x3

    const/high16 v6, 0x41700000    # 15.0f

    const/high16 v4, 0x40000000    # 2.0f

    const/4 v10, 0x1

    const/4 v9, 0x0

    new-instance v7, Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mContext:Landroid/content/Context;

    invoke-direct {v7, v0}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x43650000    # 229.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42040000    # 33.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x40400000    # 3.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x40a00000    # 5.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iput-boolean v10, v0, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v7, v0}, Landroid/widget/SeekBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    invoke-virtual {v7, v0, v9, v1, v9}, Landroid/widget/SeekBar;->setPadding(IIII)V

    const/16 v0, 0x63

    invoke-virtual {v7, v0}, Landroid/widget/SeekBar;->setMax(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "progress_handle_normal"

    const-string/jumbo v2, "progress_handle_press"

    const-string/jumbo v3, "progress_handle_focus"

    const/16 v4, 0x21

    const/16 v5, 0x21

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/widget/SeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    invoke-virtual {v0, v9}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x40e00000    # 7.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    new-instance v8, Landroid/graphics/drawable/ClipDrawable;

    invoke-direct {v8, v0, v11, v10}, Landroid/graphics/drawable/ClipDrawable;-><init>(Landroid/graphics/drawable/Drawable;II)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "progress_bg"

    const/16 v2, 0xe5

    const/16 v3, 0xc

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v2, "progress_shadow"

    const/16 v3, 0xe5

    const/16 v4, 0xc

    invoke-virtual {v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    new-instance v0, Landroid/graphics/drawable/InsetDrawable;

    move v2, v9

    move v3, v9

    move v4, v9

    move v5, v9

    invoke-direct/range {v0 .. v5}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    new-instance v1, Landroid/graphics/drawable/InsetDrawable;

    move-object v2, v6

    move v3, v9

    move v4, v9

    move v5, v9

    move v6, v9

    invoke-direct/range {v1 .. v6}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    new-instance v2, Landroid/graphics/drawable/LayerDrawable;

    new-array v3, v11, [Landroid/graphics/drawable/Drawable;

    aput-object v0, v3, v9

    aput-object v1, v3, v10

    const/4 v0, 0x2

    aput-object v8, v3, v0

    invoke-direct {v2, v3}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v7, v2}, Landroid/widget/SeekBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    return-object v7
.end method

.method private eraserSizeButtonSeekbar()Landroid/view/ViewGroup;
    .locals 12

    const/high16 v11, 0x41000000    # 8.0f

    const/high16 v10, 0x40a00000    # 5.0f

    const/high16 v9, 0x40000000    # 2.0f

    const/16 v5, 0x24

    const/high16 v8, 0x42100000    # 36.0f

    new-instance v7, Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mContext:Landroid/content/Context;

    invoke-direct {v7, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserPlusButton:Landroid/view/View;

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v4, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserPlusButton:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserPlusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_plus"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserPlusButton:Landroid/view/View;

    const-string/jumbo v2, "snote_popup_progress_btn_plus_normal"

    const-string/jumbo v3, "snote_popup_progress_btn_plus_press"

    const-string/jumbo v4, "snote_popup_progress_btn_plus_focus"

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    new-instance v0, Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserMinusButton:Landroid/view/View;

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/16 v1, 0x9

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v1, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v4, v10}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserMinusButton:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserMinusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_minus"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserMinusButton:Landroid/view/View;

    const-string/jumbo v2, "snote_popup_progress_btn_minus_normal"

    const-string/jumbo v3, "snote_popup_progress_btn_minus_press"

    const-string/jumbo v4, "snote_popup_progress_btn_minus_focus"

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->eraserSettingSizeSeekBar()Landroid/widget/SeekBar;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserPlusButton:Landroid/view/View;

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserMinusButton:Landroid/view/View;

    invoke-virtual {v7, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    return-object v7
.end method

.method private eraserSizeLayout()Landroid/view/ViewGroup;
    .locals 6

    const/4 v5, 0x0

    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x42780000    # 62.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41300000    # 11.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-virtual {v1, v5, v2, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeTextView:Landroid/widget/TextView;

    sget-object v2, Landroid/graphics/Typeface;->SANS_SERIF:Landroid/graphics/Typeface;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeTextView:Landroid/widget/TextView;

    const/16 v2, 0x56

    const/16 v3, 0x57

    const/16 v4, 0x5b

    invoke-static {v2, v3, v4}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeTextView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41400000    # 12.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v5, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42480000    # 50.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x41500000    # 13.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->eraserSizeButtonSeekbar()Landroid/view/ViewGroup;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeButtonSeekbar:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeButtonSeekbar:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method private eraserTypeLayout()Landroid/view/ViewGroup;
    .locals 4

    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v1, Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserType01:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserType01:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v3, "snote_eraser_popup_draw"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserType01:Landroid/widget/ImageButton;

    new-instance v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$13;

    invoke-direct {v2, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$13;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserType01:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    new-instance v1, Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserType02:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserType02:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v3, "snote_eraser_popup_text"

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserType02:Landroid/widget/ImageButton;

    new-instance v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$14;

    invoke-direct {v2, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$14;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserType02:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method private eraserTypeSetting(Landroid/view/View;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v1, 0x0

    move v0, v1

    :goto_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeView:[Landroid/view/View;

    array-length v2, v2

    if-lt v0, v2, :cond_0

    invoke-virtual {p1, v4}, Landroid/view/View;->setSelected(Z)V

    return-void

    :cond_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeView:[Landroid/view/View;

    aget-object v2, v2, v0

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeView:[Landroid/view/View;

    aget-object v2, v2, v0

    invoke-virtual {v2, v1}, Landroid/view/View;->setSelected(Z)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeView:[Landroid/view/View;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeView:[Landroid/view/View;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Landroid/view/View;->invalidate()V

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCurrentEraserType:I

    if-nez v0, :cond_3

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCurrentEraserType:I

    :cond_1
    :goto_1
    packed-switch v0, :pswitch_data_0

    :cond_2
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    if-ne v0, v4, :cond_1

    iput v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCurrentEraserType:I

    goto :goto_1

    :pswitch_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getEraserSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    move-result-object v2

    if-eqz v2, :cond_2

    iput v1, v2, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->type:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v3, v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setEraserSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V

    goto :goto_2

    :pswitch_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getEraserSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    move-result-object v2

    if-eqz v2, :cond_2

    iput v4, v2, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->type:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v3, v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setEraserSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getMovableRect()Landroid/graphics/Rect;
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x2

    new-array v0, v0, [I

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2, v0}, Landroid/widget/RelativeLayout;->getLocationOnScreen([I)V

    aget v2, v0, v4

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mLeftMargin:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->left:I

    aget v2, v0, v5

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTopMargin:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    aget v2, v0, v4

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    aget v0, v0, v5

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v2

    add-int/2addr v0, v2

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    return-object v1
.end method

.method private initButtonValue()V
    .locals 4

    const/high16 v3, 0x3f800000    # 1.0f

    const/high16 v0, 0x41200000    # 10.0f

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mScale:F

    sub-float/2addr v1, v3

    const/high16 v2, 0x40c00000    # 6.0f

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->EXIT_BUTTON_TOP_MARGIN:F

    const v0, 0x408aaaab

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mScale:F

    sub-float/2addr v1, v3

    const v2, 0x3feaaaab

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->EXIT_BUTTON_RIGHT_MARGIN:F

    const/high16 v0, 0x42180000    # 38.0f

    const/high16 v1, 0x42280000    # 42.0f

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->EXIT_BUTTON_TOP_MARGIN:F

    sub-float/2addr v1, v2

    mul-float/2addr v0, v1

    const/high16 v1, 0x42100000    # 36.0f

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->EXIT_BUTTON_WIDTH:I

    const/16 v0, 0x24

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->EXIT_BUTTON_HEIGHT:I

    const/high16 v0, 0x41880000    # 17.0f

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mScale:F

    sub-float/2addr v1, v3

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->LINE_BUTTON_TOP_MARGIN:F

    return-void
.end method

.method private playScrollAnimationForBottomBar(III)V
    .locals 6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mScrollTimer:Ljava/util/Timer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mScrollTimer:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    :cond_0
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mScrollTimer:Ljava/util/Timer;

    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCount:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mScrollTimer:Ljava/util/Timer;

    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$15;

    invoke-direct {v1, p0, p2, p3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$15;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;II)V

    const-wide/16 v2, 0xa

    int-to-long v4, p1

    invoke-virtual/range {v0 .. v5}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;JJ)V

    return-void
.end method

.method private popupMaxButton()Landroid/view/View;
    .locals 7

    const/16 v5, 0x20

    const/high16 v4, 0x42280000    # 42.0f

    new-instance v1, Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mContext:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v2, 0xb

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x40800000    # 4.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setFocusable(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_close"

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string/jumbo v2, "snote_popup_arrow_max_normal"

    const-string/jumbo v3, "snote_popup_arrow_max_normal"

    const-string/jumbo v4, "snote_popup_arrow_max_normal"

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    return-object v1
.end method

.method private popupMinButton()Landroid/view/View;
    .locals 7

    const/16 v5, 0x20

    const/high16 v4, 0x42280000    # 42.0f

    new-instance v1, Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mContext:Landroid/content/Context;

    invoke-direct {v1, v0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v2, 0xb

    invoke-virtual {v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x40800000    # 4.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iput v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setFocusable(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_close"

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string/jumbo v2, "snote_popup_arrow_min_normal"

    const-string/jumbo v3, "snote_popup_arrow_min_normal"

    const-string/jumbo v4, "snote_popup_arrow_min_normal"

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    return-object v1
.end method

.method private rotatePosition()V
    .locals 11

    const/high16 v0, 0x3f800000    # 1.0f

    const v10, 0x3f7d70a4    # 0.99f

    const/4 v9, 0x0

    const/4 v1, 0x0

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTotalTopMargin:I

    if-eq v2, v3, :cond_2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTotalTopMargin:I

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTotalLeftMargin:I

    add-int/2addr v2, v3

    iput v2, v4, Landroid/graphics/Rect;->left:I

    :goto_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTotalLeftMargin:I

    if-eq v2, v3, :cond_3

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTotalLeftMargin:I

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTotalTopMargin:I

    add-int/2addr v2, v3

    iput v2, v4, Landroid/graphics/Rect;->top:I

    :goto_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    iput v2, v4, Landroid/graphics/Rect;->right:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iput v2, v4, Landroid/graphics/Rect;->bottom:I

    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5}, Landroid/graphics/Rect;-><init>()V

    const/4 v2, 0x2

    new-array v2, v2, [I

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->getLocationOnScreen([I)V

    aget v3, v2, v9

    iput v3, v5, Landroid/graphics/Rect;->left:I

    const/4 v3, 0x1

    aget v2, v2, v3

    iput v2, v5, Landroid/graphics/Rect;->top:I

    iget v2, v5, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v5, Landroid/graphics/Rect;->right:I

    iget v2, v5, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->getHeight()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v5, Landroid/graphics/Rect;->bottom:I

    iget v2, v5, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget v6, v5, Landroid/graphics/Rect;->right:I

    sub-int/2addr v3, v6

    int-to-float v3, v3

    iget v6, v5, Landroid/graphics/Rect;->top:I

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    sub-int/2addr v6, v7

    int-to-float v6, v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    iget v8, v5, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v7, v8

    int-to-float v7, v7

    add-float/2addr v3, v2

    div-float v3, v2, v3

    add-float v2, v6, v7

    div-float v2, v6, v2

    cmpl-float v6, v3, v10

    if-lez v6, :cond_4

    move v3, v0

    :cond_0
    :goto_2
    cmpl-float v6, v2, v10

    if-lez v6, :cond_5

    move v1, v0

    :cond_1
    :goto_3
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v6

    if-ge v2, v6, :cond_6

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v6

    sub-int/2addr v2, v6

    int-to-float v2, v2

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    :goto_4
    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v3

    if-ge v2, v3, :cond_7

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v2

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v3

    sub-int/2addr v2, v3

    int-to-float v2, v2

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    :goto_5
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void

    :cond_2
    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTotalLeftMargin:I

    iput v2, v4, Landroid/graphics/Rect;->left:I

    goto/16 :goto_0

    :cond_3
    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTotalTopMargin:I

    iput v2, v4, Landroid/graphics/Rect;->top:I

    goto/16 :goto_1

    :cond_4
    cmpg-float v6, v3, v1

    if-gez v6, :cond_0

    move v3, v1

    goto :goto_2

    :cond_5
    cmpg-float v0, v2, v1

    if-ltz v0, :cond_1

    move v1, v2

    goto :goto_3

    :cond_6
    iput v9, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    goto :goto_4

    :cond_7
    iput v9, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    goto :goto_5
.end method

.method private setListener()V
    .locals 3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTitleLayout:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTitleLayout:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mOnConsumedTouchListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupMaxButton:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupMaxButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupMinButton:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupMinButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserMinusButton:Landroid/view/View;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserMinusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMinusButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserMinusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mLongClickMinusButtonListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserMinusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTouchMinusButtonListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserPlusButton:Landroid/view/View;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserPlusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPlusButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserPlusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mLongClickPlusButtonListener:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserPlusButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTouchPlusButtonListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mClearAllButton:Landroid/view/View;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mClearAllButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mClearAllListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeView:[Landroid/view/View;

    if-eqz v0, :cond_7

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x1

    if-le v0, v1, :cond_9

    :cond_7
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    :cond_8
    return-void

    :cond_9
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeView:[Landroid/view/View;

    aget-object v1, v1, v0

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeView:[Landroid/view/View;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeListner:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private titleBg()Landroid/view/View;
    .locals 9

    const/16 v8, 0xa

    const/4 v7, 0x1

    const/4 v6, -0x1

    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v6, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v1, Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-boolean v7, v2, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/16 v3, 0x9

    invoke-virtual {v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v2, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v2, Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v5, 0x43248000    # 164.5f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-direct {v3, v4, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-boolean v7, v3, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/16 v4, 0xb

    invoke-virtual {v3, v4}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v3, v8}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string/jumbo v3, "vienna_popup_title_bg"

    invoke-virtual {v2, v1, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method private titleLayout()Landroid/view/ViewGroup;
    .locals 5

    new-instance v0, Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x42280000    # 42.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->popupMaxButton()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupMaxButton:Landroid/view/View;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->popupMinButton()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupMinButton:Landroid/view/View;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->titleBg()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->titleText()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupMaxButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupMinButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupMaxButton:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    return-object v0
.end method

.method private titleText()Landroid/view/View;
    .locals 8

    const/16 v7, 0x13

    const/4 v6, 0x1

    const/4 v5, -0x1

    const/4 v4, 0x0

    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42280000    # 42.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {v1, v5, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setGravity(I)V

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setFocusable(Z)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_eraser_settings"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setSingleLine(Z)V

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x1c

    if-le v2, v3, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41400000    # 12.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v4, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_eraser_settings"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41500000    # 13.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    invoke-virtual {v0, v1, v4, v4, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    return-object v0

    :cond_0
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v7, :cond_1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41600000    # 14.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v4, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41a00000    # 20.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v4, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_0
.end method

.method private totalLayout()V
    .locals 3

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const v2, 0x43a48000    # 329.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->setOrientation(I)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->titleLayout()Landroid/view/ViewGroup;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTitleLayout:Landroid/view/View;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->bodyLayout()Landroid/view/ViewGroup;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mBodyLayout:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTitleLayout:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mBodyLayout:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->addView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 5

    const/4 v1, 0x0

    const/4 v4, 0x0

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCanvasLayout:Landroid/widget/RelativeLayout;

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeButtonSeekbar:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeButtonSeekbar:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserPlusButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserPlusButton:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserMinusButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserMinusButton:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mClearAllButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mClearAllButton:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mSettingSizeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mSettingSizeLayout:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserType01:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserType01:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserType02:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserType02:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeLayout:Landroid/view/ViewGroup;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-lt v0, v2, :cond_3

    :cond_1
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeView:[Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeLayout:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTitleLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTitleLayout:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mBodyLayout:Landroid/view/View;

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    aput-object v4, v0, v1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    const/4 v1, 0x1

    aput-object v4, v0, v1

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    :cond_2
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$EventListener;

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$ActionListener;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->close()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    goto/16 :goto_0

    :cond_3
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeView:[Landroid/view/View;

    aget-object v3, v3, v0

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeView:[Landroid/view/View;

    aput-object v4, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public getInfo()Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    return-object v0
.end method

.method public getViewMode()I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mViewMode:I

    return v0
.end method

.method protected initView()V
    .locals 5

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->totalLayout()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeLayout:Landroid/view/ViewGroup;

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    new-array v1, v1, [Landroid/view/View;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeView:[Landroid/view/View;

    move v1, v2

    :goto_0
    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    if-lt v1, v3, :cond_1

    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    if-nez v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    new-instance v1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    invoke-direct {v1}, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;-><init>()V

    aput-object v1, v0, v2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    const/4 v1, 0x1

    new-instance v2, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    invoke-direct {v2}, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;-><init>()V

    aput-object v2, v0, v1

    :cond_0
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->setVisibility(I)V

    return-void

    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeView:[Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    aput-object v4, v3, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mFirstLongPress:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->rotatePosition()V

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x43640000    # 228.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x43010000    # 129.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    if-lt p1, v0, :cond_0

    if-ge p2, v1, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->checkPosition()V

    :cond_1
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$16;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$16;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/LinearLayout;->onSizeChanged(IIII)V

    return-void
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    if-ne p1, p0, :cond_1

    if-nez p2, :cond_1

    const/4 v0, 0x2

    new-array v0, v0, [I

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->getLocationOnScreen([I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->getMovableRect()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mMovableRect:Landroid/graphics/Rect;

    new-instance v2, Landroid/graphics/Rect;

    aget v3, v0, v7

    aget v4, v0, v8

    aget v5, v0, v7

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->getWidth()I

    move-result v6

    add-int/2addr v5, v6

    aget v0, v0, v8

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->getHeight()I

    move-result v6

    add-int/2addr v0, v6

    invoke-direct {v2, v3, v4, v5, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->contains(Landroid/graphics/Rect;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->checkPosition()V

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v1, 0x42240000    # 41.0f

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x43480000    # 200.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v2}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x42c80000    # 100.0f

    div-float/2addr v2, v3

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41700000    # 15.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v1, v2

    float-to-int v1, v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeTextView:Landroid/widget/TextView;

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setX(F)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeTextView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x40a00000    # 5.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setY(F)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeTextView:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onVisibilityChanged(Landroid/view/View;I)V

    return-void
.end method

.method public requestLayout()V
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->requestLayoutDisable:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Landroid/widget/LinearLayout;->requestLayout()V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->requestLayoutDisable:Z

    return-void
.end method

.method public setActionListener(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$ActionListener;)V
    .locals 0

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$ActionListener;

    :cond_0
    return-void
.end method

.method public setCanvasView(Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;)V
    .locals 0

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    :cond_0
    return-void
.end method

.method public setEraserInfoList([Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V
    .locals 0

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    :cond_0
    return-void
.end method

.method public setEraserListener(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$EventListener;)V
    .locals 0

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$EventListener;

    :cond_0
    return-void
.end method

.method public setIndicatorPosition(I)V
    .locals 0

    return-void
.end method

.method public setInfo(Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V
    .locals 2

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->type:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCurrentEraserType:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserDataList:[Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mCurrentEraserType:I

    aget-object v0, v0, v1

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->type:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->type:I

    goto :goto_0
.end method

.method public setLayoutHeight(I)V
    .locals 1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iput p1, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public setPopup(Z)V
    .locals 4

    const/16 v2, 0x8

    const/4 v3, 0x2

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupMaxButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupMinButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x433a0000    # 186.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    invoke-direct {p0, v3, v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->playScrollAnimationForBottomBar(III)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupMaxButton:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupMinButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x42280000    # 42.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v1

    invoke-direct {p0, v3, v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->playScrollAnimationForBottomBar(III)V

    goto :goto_0
.end method

.method public setPopupListenr(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$PopupListener;)V
    .locals 0

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mPopupListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2$PopupListener;

    :cond_0
    return-void
.end method

.method public setPosition(II)V
    .locals 1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iput p2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mFirstLongPress:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mLeftMargin:I

    sub-int/2addr v0, p1

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mLeftMargin:I

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTopMargin:I

    sub-int/2addr v0, p2

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTopMargin:I

    :cond_0
    return-void
.end method

.method public setViewMode(I)V
    .locals 5

    const/16 v4, 0x8

    const/4 v3, 0x0

    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mViewMode:I

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->requestLayoutDisable:Z

    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->requestLayoutDisable:Z

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mViewMode:I

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mSettingSizeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mClearAllButton:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->requestLayoutDisable:Z

    return-void

    :cond_0
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mViewMode:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mClearAllButton:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mSettingSizeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0

    :cond_1
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mViewMode:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mClearAllButton:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mSettingSizeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0

    :cond_2
    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mViewMode:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mTitleLayout:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_3
    iput v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mViewMode:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mBodyLayout:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mEraserTypeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mSettingSizeLayout:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mClearAllButton:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public setVisibility(I)V
    .locals 1

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mLoaded:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->loadImage()V

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    return-void
.end method
