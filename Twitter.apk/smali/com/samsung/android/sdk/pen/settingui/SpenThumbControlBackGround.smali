.class Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;
.super Landroid/widget/LinearLayout;
.source "Twttr"


# instance fields
.field private mPaint:Landroid/graphics/Paint;

.field private mTopPadding:I

.field private mTrackWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public initPaint()V
    .locals 2

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->mPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->mPaint:Landroid/graphics/Paint;

    const v1, -0xd4d4d5

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->mPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->mTrackWidth:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->mPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onDraw(Landroid/graphics/Canvas;)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v1, v0

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->mTopPadding:I

    add-int/lit8 v2, v2, 0x3

    int-to-float v2, v2

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->getHeight()I

    move-result v0

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->mTopPadding:I

    sub-int/2addr v0, v4

    add-int/lit8 v0, v0, -0x5

    int-to-float v4, v0

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->mPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method public setTopPadding(I)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->mTopPadding:I

    return-void
.end method

.method public setTrackWidth(I)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->mTrackWidth:I

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenThumbControlBackGround;->initPaint()V

    return-void
.end method
