.class Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->context:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$0(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Landroid/content/Context;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->windowHeight:I
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$1(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->window:Landroid/widget/PopupWindow;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->anchor:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/widget/PopupWindow;->getMaxAvailableHeight(Landroid/view/View;)I

    move-result v1

    if-le v0, v1, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->window:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->anchor:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->getMaxAvailableHeight(Landroid/view/View;)I

    move-result v0

    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->window:Landroid/widget/PopupWindow;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->anchor:Landroid/view/View;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->windowWidth:I
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$2(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)I

    move-result v3

    invoke-virtual {v1, v2, v3, v0}, Landroid/widget/PopupWindow;->update(Landroid/view/View;II)V

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->window:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$3(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->runnable:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$4(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$1;->this$0:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->windowHeight:I
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;->access$1(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1
.end method
