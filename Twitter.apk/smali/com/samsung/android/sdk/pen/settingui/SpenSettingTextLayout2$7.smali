.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$7;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->getColorPickerSettingVisible()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPreCanvasPenAction:I

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setToolTypeAction(II)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mPreCanvasFingerAction:I

    invoke-interface {v0, v1, v2}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setToolTypeAction(II)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getTextSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setTextSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->setPreviewTextColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->invalidate()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorSelectView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorSelectView:Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPaletteView2;->invalidate()V

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerSetting:Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->hide()V

    :cond_2
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mColorPickerShow:Z

    return-void
.end method
