.class public interface abstract Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final ACTION_COLOR_PICKER:I = 0x5

.field public static final ACTION_ERASER:I = 0x3

.field public static final ACTION_GESTURE:I = 0x1

.field public static final ACTION_NONE:I = 0x0

.field public static final ACTION_SELECTION:I = 0x6

.field public static final ACTION_STROKE:I = 0x2

.field public static final ACTION_STROKE_REMOVER:I = 0x4

.field public static final ACTION_TEXT:I = 0x7

.field public static final TOOL_ERASER:I = 0x4

.field public static final TOOL_FINGER:I = 0x1

.field public static final TOOL_MOUSE:I = 0x3

.field public static final TOOL_MULTI_TOUCH:I = 0x5

.field public static final TOOL_SPEN:I = 0x2

.field public static final TOOL_UNKNOWN:I


# virtual methods
.method public abstract closeControl()V
.end method

.method public abstract getCanvasHeight()I
.end method

.method public abstract getCanvasWidth()I
.end method

.method public abstract getEraserSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;
.end method

.method public abstract getPenSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
.end method

.method public abstract getRemoverSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
.end method

.method public abstract getSelectionSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;
.end method

.method public abstract getTextSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
.end method

.method public abstract getToolTypeAction(I)I
.end method

.method public abstract setBackgroundColorChangeListener(Ljava/lang/Object;Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;)V
.end method

.method public abstract setEraserSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V
.end method

.method public abstract setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V
.end method

.method public abstract setRemoverSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V
.end method

.method public abstract setSelectionSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;)V
.end method

.method public abstract setTextSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V
.end method

.method public abstract setToolTypeAction(II)V
.end method
