.class public Lcom/samsung/android/sdk/pen/Spen;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final DEFAULT_MAX_CACHE_SIZE:I = 0x5

.field public static final DEVICE_PEN:I = 0x0

.field public static final SPEN_NATIVE_PACKAGE_NAME:Ljava/lang/String; = "com.samsung.android.sdk.spen30"

.field private static final VERSION:Ljava/lang/String; = "3.0.0"

.field private static final VERSION_LEVEL:I = 0x2

.field private static mIsInitialized:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/android/sdk/pen/Spen;->mIsInitialized:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static native SPenSdk_init(Ljava/lang/String;II)Z
.end method

.method private static native SPenSdk_init2(Ljava/lang/String;III)Z
.end method


# virtual methods
.method B21433A29DE24(I)I
    .locals 1

    mul-int v0, p1, p1

    add-int/2addr v0, p1

    return v0
.end method

.method public getVersionCode()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public getVersionName()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "3.0.0"

    return-object v0
.end method

.method public initialize(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, Lcom/samsung/android/sdk/pen/Spen;->initialize(Landroid/content/Context;I)V

    return-void
.end method

.method public initialize(Landroid/content/Context;I)V
    .locals 11

    const/4 v10, 0x4

    const/4 v9, 0x1

    const/4 v2, 0x0

    const-string/jumbo v0, "Spen"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "SpenSdk jar version = "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/Spen;->getVersionCode()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "context is invalid."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    if-nez v3, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "resource is null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v9, :cond_2

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    :goto_0
    sget-boolean v3, Lcom/samsung/android/sdk/pen/Spen;->mIsInitialized:Z

    if-eqz v3, :cond_4

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1, v0, p2}, Lcom/samsung/android/sdk/pen/Spen;->SPenSdk_init2(Ljava/lang/String;III)Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "SDK Cache directory is not initialized."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    goto :goto_0

    :cond_3
    const-string/jumbo v0, "Spen"

    const-string/jumbo v1, "initialize complete"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    return-void

    :cond_4
    sget-object v3, Landroid/os/Build;->BRAND:Ljava/lang/String;

    sget-object v4, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    if-eqz v3, :cond_5

    if-nez v4, :cond_6

    :cond_5
    new-instance v0, Lcom/samsung/android/sdk/SsdkUnsupportedException;

    const-string/jumbo v1, "Vendor is not supported"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/SsdkUnsupportedException;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_6
    const-string/jumbo v5, "Samsung"

    invoke-virtual {v3, v5}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_7

    const-string/jumbo v3, "Samsung"

    invoke-virtual {v4, v3}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v3

    if-eqz v3, :cond_7

    new-instance v0, Lcom/samsung/android/sdk/SsdkUnsupportedException;

    const-string/jumbo v1, "Vendor is not supported"

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/SsdkUnsupportedException;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_7
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    :try_start_0
    const-string/jumbo v4, "com.samsung.android.sdk.spen30"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    const-string/jumbo v4, "Spen"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "SpenSdk apk version = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v3, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, v3, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    const-string/jumbo v4, "\\."

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v3, "3.0.0"

    const-string/jumbo v5, "\\."

    invoke-virtual {v3, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    const/4 v3, 0x4

    new-array v6, v3, [I

    const/4 v3, 0x4

    new-array v7, v3, [I

    move v3, v2

    :goto_2
    if-lt v3, v10, :cond_9

    :goto_3
    if-lt v2, v10, :cond_c

    const/4 v2, 0x0

    aget v2, v7, v2

    const/4 v3, 0x0

    aget v3, v6, v3

    if-gt v2, v3, :cond_8

    const/4 v2, 0x0

    aget v2, v7, v2

    const/4 v3, 0x0

    aget v3, v6, v3

    if-ne v2, v3, :cond_f

    const/4 v2, 0x1

    aget v2, v7, v2

    const/4 v3, 0x1

    aget v3, v6, v3

    if-le v2, v3, :cond_f

    :cond_8
    const-string/jumbo v0, "Spen"

    const-string/jumbo v1, "You SHOULD UPDATE SpenSdk apk file !!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/samsung/android/sdk/SsdkUnsupportedException;

    const-string/jumbo v1, "SpenSdk apk version is low."

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/SsdkUnsupportedException;-><init>(Ljava/lang/String;I)V

    throw v0
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    const-string/jumbo v0, "Spen"

    const-string/jumbo v1, "You SHOULD INSTALL SpenSdk apk file !!!!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/samsung/android/sdk/SsdkUnsupportedException;

    const-string/jumbo v1, "SpenSdk apk is not installed."

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/sdk/SsdkUnsupportedException;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_9
    :try_start_1
    array-length v8, v4

    if-gt v8, v3, :cond_a

    const/4 v8, 0x0

    aput v8, v6, v3

    :goto_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_a
    aget-object v8, v4, v3

    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_b

    const/4 v8, 0x0

    aput v8, v6, v3

    goto :goto_4

    :cond_b
    aget-object v8, v4, v3

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    aput v8, v6, v3

    goto :goto_4

    :cond_c
    array-length v3, v5

    if-gt v3, v2, :cond_d

    const/4 v3, 0x0

    aput v3, v7, v2

    :goto_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_d
    aget-object v3, v5, v2

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_e

    const/4 v3, 0x0

    aput v3, v7, v2

    goto :goto_5

    :cond_e
    aget-object v3, v5, v2

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    aput v3, v7, v2

    goto :goto_5

    :cond_f
    const-string/jumbo v2, "Spen"

    const-string/jumbo v3, "Server UPDATE Check"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    const-string/jumbo v2, "SPenBase"

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_10

    const-string/jumbo v2, "SPenPluginFW"

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_10

    const-string/jumbo v2, "SPenInit"

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_10

    const-string/jumbo v2, "SPenModel"

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_10

    const-string/jumbo v2, "SPenSkia"

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_10

    const-string/jumbo v2, "SPenEngine"

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_10

    const-string/jumbo v2, "SPenBrush"

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_10

    const-string/jumbo v2, "SPenChineseBrush"

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_10

    const-string/jumbo v2, "SPenInkPen"

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_10

    const-string/jumbo v2, "SPenMarker"

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_10

    const-string/jumbo v2, "SPenPencil"

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_10

    const-string/jumbo v2, "SPenNativePen"

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_10

    const-string/jumbo v2, "SPenMagicPen"

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_10

    const-string/jumbo v2, "SPenHSV"

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_10

    const-string/jumbo v2, "SPenVIRecognition"

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_10

    const-string/jumbo v2, "SPenVITextAll"

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/Spen;->loadLibrary(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_11

    :cond_10
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_11
    const-string/jumbo v2, "Spen"

    const-string/jumbo v3, "SpenSdk Libraries are loaded."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1, v0, p2}, Lcom/samsung/android/sdk/pen/Spen;->SPenSdk_init2(Ljava/lang/String;III)Z

    move-result v0

    if-nez v0, :cond_12

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "SDK Cache directory is not initialized."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_12
    sput-boolean v9, Lcom/samsung/android/sdk/pen/Spen;->mIsInitialized:Z

    const-string/jumbo v0, "Spen"

    const-string/jumbo v1, "initialize complete"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method public isFeatureEnabled(I)Z
    .locals 2

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :pswitch_0
    new-instance v0, Ljava/io/File;

    const-string/jumbo v1, "/sys/class/sec/sec_epen"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method loadLibrary(Ljava/lang/String;)Z
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SdCardPath"
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "/data/data/com.samsung.android.sdk.spen30/lib/lib"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".so"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/System;->load(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
