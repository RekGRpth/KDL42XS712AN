.class public Liq;
.super Lcom/twitter/library/service/o;
.source "Twttr"


# instance fields
.field private d:Lcom/twitter/library/api/ao;

.field private final e:J

.field private final f:J

.field private g:Lcom/twitter/library/api/PromotedContent;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JJ)V
    .locals 1

    const-class v0, Liq;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    iput-wide p5, p0, Liq;->e:J

    iput-wide p3, p0, Liq;->f:J

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/library/service/e;)Lcom/twitter/internal/network/HttpOperation;
    .locals 8

    const/4 v6, 0x1

    iget-object v0, p0, Liq;->m:Lcom/twitter/library/network/aa;

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "1.1"

    aput-object v3, v1, v2

    const-string/jumbo v2, "friendships"

    aput-object v2, v1, v6

    const/4 v2, 0x2

    const-string/jumbo v3, "destroy"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v3, "user_id"

    iget-wide v4, p0, Liq;->e:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Liq;->g:Lcom/twitter/library/api/PromotedContent;

    if-eqz v2, :cond_1

    iget-object v2, p0, Liq;->g:Lcom/twitter/library/api/PromotedContent;

    iget-object v2, v2, Lcom/twitter/library/api/PromotedContent;->impressionId:Ljava/lang/String;

    if-eqz v2, :cond_0

    const-string/jumbo v2, "impression_id"

    iget-object v3, p0, Liq;->g:Lcom/twitter/library/api/PromotedContent;

    iget-object v3, v3, Lcom/twitter/library/api/PromotedContent;->impressionId:Ljava/lang/String;

    invoke-static {v0, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v2, p0, Liq;->g:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {v2}, Lcom/twitter/library/api/PromotedContent;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string/jumbo v2, "earned"

    invoke-static {v0, v2, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    :cond_1
    const/16 v2, 0x11

    new-instance v3, Lcom/twitter/library/util/f;

    iget-object v4, p0, Liq;->l:Landroid/content/Context;

    iget-wide v5, p0, Liq;->f:J

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    invoke-static {v2, v3}, Lcom/twitter/library/api/ao;->a(ILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v2

    iput-object v2, p0, Liq;->d:Lcom/twitter/library/api/ao;

    invoke-virtual {p0}, Liq;->s()Lcom/twitter/library/service/p;

    move-result-object v2

    new-instance v3, Lcom/twitter/library/network/d;

    iget-object v4, p0, Liq;->l:Landroid/content/Context;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v4, v2, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v3, v4, v5}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v0

    sget-object v3, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v0, v3}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v0

    new-instance v3, Lcom/twitter/library/network/n;

    iget-object v2, v2, Lcom/twitter/library/service/p;->d:Lcom/twitter/library/network/OAuthToken;

    invoke-direct {v3, v2}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    invoke-virtual {v0, v3}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v0

    iget-object v1, p0, Liq;->d:Lcom/twitter/library/api/ao;

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/api/PromotedContent;)Liq;
    .locals 0

    iput-object p1, p0, Liq;->g:Lcom/twitter/library/api/PromotedContent;

    return-object p0
.end method

.method protected a(Lcom/twitter/internal/network/HttpOperation;Lcom/twitter/library/service/e;)V
    .locals 6

    invoke-virtual {p1}, Lcom/twitter/internal/network/HttpOperation;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Liq;->l:Landroid/content/Context;

    iget-wide v1, p0, Liq;->f:J

    invoke-static {v0, v1, v2}, Lcom/twitter/library/provider/az;->a(Landroid/content/Context;J)Lcom/twitter/library/provider/az;

    move-result-object v0

    iget-object v1, p0, Liq;->d:Lcom/twitter/library/api/ao;

    invoke-virtual {v1}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/twitter/library/api/TwitterUser;

    iget-wide v1, v4, Lcom/twitter/library/api/TwitterUser;->userId:J

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/provider/az;->e(JI)V

    const/4 v1, 0x0

    iget-wide v2, p0, Liq;->f:J

    iget-wide v4, v4, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/provider/az;->a(IJJ)V

    :cond_0
    invoke-virtual {p2, p1}, Lcom/twitter/library/service/e;->a(Lcom/twitter/internal/network/HttpOperation;)V

    return-void
.end method

.method public final e()J
    .locals 2

    iget-wide v0, p0, Liq;->e:J

    return-wide v0
.end method
