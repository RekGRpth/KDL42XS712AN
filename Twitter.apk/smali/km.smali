.class public Lkm;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:Z

.field private static b:Ljava/lang/String;

.field private static c:Z

.field private static d:Z

.field private static e:Z

.field private static f:I

.field private static g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    sput-boolean v1, Lkm;->a:Z

    const-string/jumbo v0, "unassigned"

    sput-object v0, Lkm;->b:Ljava/lang/String;

    sput-boolean v2, Lkm;->c:Z

    sput-boolean v2, Lkm;->d:Z

    sput-boolean v1, Lkm;->e:Z

    sput v2, Lkm;->f:I

    sput-boolean v1, Lkm;->g:Z

    return-void
.end method

.method public static a()Ljava/lang/String;
    .locals 1

    sget-object v0, Lkm;->b:Ljava/lang/String;

    return-object v0
.end method

.method public static a(Landroid/content/Context;J)V
    .locals 5

    const/4 v2, 0x0

    const/4 v4, 0x1

    const-string/jumbo v0, "android_ptr_design_1870"

    invoke-static {p0, p1, p2, v0}, Lju;->b(Landroid/content/Context;JLjava/lang/String;)V

    const-string/jumbo v0, "android_ptr_design_1870"

    invoke-static {p0, p1, p2, v0}, Lju;->a(Landroid/content/Context;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lkm;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    sput-object v0, Lkm;->b:Ljava/lang/String;

    invoke-static {v0}, Lkk;->d(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    sput-boolean v4, Lkm;->a:Z

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    new-instance v2, Ljava/util/StringTokenizer;

    const-string/jumbo v3, "_"

    invoke-direct {v2, v0, v3}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    const-string/jumbo v0, "tweet"

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lkm;->c:Z

    const-string/jumbo v0, "chrome"

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lkm;->d:Z

    const-string/jumbo v0, "pivot"

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lkm;->e:Z

    const-string/jumbo v0, "all"

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lkm;->g:Z

    const-string/jumbo v0, "x"

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sput v4, Lkm;->f:I

    :cond_1
    :goto_1
    return-void

    :cond_2
    const-string/jumbo v0, "press"

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    sput v0, Lkm;->f:I

    goto :goto_1

    :cond_3
    sput-boolean v2, Lkm;->a:Z

    sput-boolean v4, Lkm;->c:Z

    sput-boolean v4, Lkm;->d:Z

    sput-boolean v2, Lkm;->e:Z

    sput v4, Lkm;->f:I

    sput-boolean v2, Lkm;->g:Z

    goto :goto_1
.end method

.method public static b()Z
    .locals 1

    sget-boolean v0, Lkm;->a:Z

    return v0
.end method

.method public static c()Z
    .locals 1

    sget-boolean v0, Lkm;->c:Z

    return v0
.end method

.method public static d()Z
    .locals 1

    sget-boolean v0, Lkm;->d:Z

    return v0
.end method

.method public static e()Z
    .locals 1

    sget-boolean v0, Lkm;->e:Z

    return v0
.end method

.method public static f()I
    .locals 1

    sget v0, Lkm;->f:I

    return v0
.end method

.method public static g()Z
    .locals 1

    sget-boolean v0, Lkm;->g:Z

    return v0
.end method
