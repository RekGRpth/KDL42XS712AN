.class final Lfu;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lfg;


# instance fields
.field public final a:Lfo;

.field public final b:Lga;

.field private c:Z


# direct methods
.method public constructor <init>(Lga;)V
    .locals 1

    new-instance v0, Lfo;

    invoke-direct {v0}, Lfo;-><init>()V

    invoke-direct {p0, p1, v0}, Lfu;-><init>(Lga;Lfo;)V

    return-void
.end method

.method public constructor <init>(Lga;Lfo;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "sink == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p2, p0, Lfu;->a:Lfo;

    iput-object p1, p0, Lfu;->b:Lga;

    return-void
.end method

.method static synthetic a(Lfu;)Z
    .locals 1

    iget-boolean v0, p0, Lfu;->c:Z

    return v0
.end method

.method private e()V
    .locals 2

    iget-boolean v0, p0, Lfu;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method public a(I)Lfg;
    .locals 1

    invoke-direct {p0}, Lfu;->e()V

    iget-object v0, p0, Lfu;->a:Lfo;

    invoke-virtual {v0, p1}, Lfo;->d(I)Lfo;

    invoke-virtual {p0}, Lfu;->c()Lfg;

    move-result-object v0

    return-object v0
.end method

.method public a(Lfi;)Lfg;
    .locals 1

    invoke-direct {p0}, Lfu;->e()V

    iget-object v0, p0, Lfu;->a:Lfo;

    invoke-virtual {v0, p1}, Lfo;->b(Lfi;)Lfo;

    invoke-virtual {p0}, Lfu;->c()Lfg;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lfg;
    .locals 1

    invoke-direct {p0}, Lfu;->e()V

    iget-object v0, p0, Lfu;->a:Lfo;

    invoke-virtual {v0, p1}, Lfo;->b(Ljava/lang/String;)Lfo;

    invoke-virtual {p0}, Lfu;->c()Lfg;

    move-result-object v0

    return-object v0
.end method

.method public a([B)Lfg;
    .locals 1

    invoke-direct {p0}, Lfu;->e()V

    iget-object v0, p0, Lfu;->a:Lfo;

    invoke-virtual {v0, p1}, Lfo;->b([B)Lfo;

    invoke-virtual {p0}, Lfu;->c()Lfg;

    move-result-object v0

    return-object v0
.end method

.method public a([BII)Lfg;
    .locals 1

    invoke-direct {p0}, Lfu;->e()V

    iget-object v0, p0, Lfu;->a:Lfo;

    invoke-virtual {v0, p1, p2, p3}, Lfo;->c([BII)Lfo;

    invoke-virtual {p0}, Lfu;->c()Lfg;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 4

    invoke-direct {p0}, Lfu;->e()V

    iget-object v0, p0, Lfu;->a:Lfo;

    iget-wide v0, v0, Lfo;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, Lfu;->b:Lga;

    iget-object v1, p0, Lfu;->a:Lfo;

    iget-object v2, p0, Lfu;->a:Lfo;

    iget-wide v2, v2, Lfo;->b:J

    invoke-interface {v0, v1, v2, v3}, Lga;->a(Lfo;J)V

    :cond_0
    iget-object v0, p0, Lfu;->b:Lga;

    invoke-interface {v0}, Lga;->a()V

    return-void
.end method

.method public a(Lfo;J)V
    .locals 1

    invoke-direct {p0}, Lfu;->e()V

    iget-object v0, p0, Lfu;->a:Lfo;

    invoke-virtual {v0, p1, p2, p3}, Lfo;->a(Lfo;J)V

    invoke-virtual {p0}, Lfu;->c()Lfg;

    return-void
.end method

.method public b(I)Lfg;
    .locals 1

    invoke-direct {p0}, Lfu;->e()V

    iget-object v0, p0, Lfu;->a:Lfo;

    invoke-virtual {v0, p1}, Lfo;->e(I)Lfo;

    invoke-virtual {p0}, Lfu;->c()Lfg;

    move-result-object v0

    return-object v0
.end method

.method public b()Lfo;
    .locals 1

    iget-object v0, p0, Lfu;->a:Lfo;

    return-object v0
.end method

.method public c()Lfg;
    .locals 4

    invoke-direct {p0}, Lfu;->e()V

    iget-object v0, p0, Lfu;->a:Lfo;

    invoke-virtual {v0}, Lfo;->n()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    iget-object v2, p0, Lfu;->b:Lga;

    iget-object v3, p0, Lfu;->a:Lfo;

    invoke-interface {v2, v3, v0, v1}, Lga;->a(Lfo;J)V

    :cond_0
    return-object p0
.end method

.method public close()V
    .locals 5

    iget-boolean v0, p0, Lfu;->c:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lfu;->a:Lfo;

    iget-wide v1, v1, Lfo;->b:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_2

    iget-object v1, p0, Lfu;->b:Lga;

    iget-object v2, p0, Lfu;->a:Lfo;

    iget-object v3, p0, Lfu;->a:Lfo;

    iget-wide v3, v3, Lfo;->b:J

    invoke-interface {v1, v2, v3, v4}, Lga;->a(Lfo;J)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    :cond_2
    :goto_1
    :try_start_1
    iget-object v1, p0, Lfu;->b:Lga;

    invoke-interface {v1}, Lga;->close()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    :cond_3
    :goto_2
    const/4 v1, 0x1

    iput-boolean v1, p0, Lfu;->c:Z

    if-eqz v0, :cond_0

    invoke-static {v0}, Lgc;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    :catch_0
    move-exception v1

    if-nez v0, :cond_3

    move-object v0, v1

    goto :goto_2

    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public d()Ljava/io/OutputStream;
    .locals 1

    new-instance v0, Lfv;

    invoke-direct {v0, p0}, Lfv;-><init>(Lfu;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "buffer("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lfu;->b:Lga;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
