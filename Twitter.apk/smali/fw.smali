.class final Lfw;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lfh;


# instance fields
.field public final a:Lfo;

.field public final b:Lgb;

.field private c:Z


# direct methods
.method public constructor <init>(Lgb;)V
    .locals 1

    new-instance v0, Lfo;

    invoke-direct {v0}, Lfo;-><init>()V

    invoke-direct {p0, p1, v0}, Lfw;-><init>(Lgb;Lfo;)V

    return-void
.end method

.method public constructor <init>(Lgb;Lfo;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "source == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p2, p0, Lfw;->a:Lfo;

    iput-object p1, p0, Lfw;->b:Lgb;

    return-void
.end method

.method private a()V
    .locals 2

    iget-boolean v0, p0, Lfw;->c:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method static synthetic a(Lfw;)Z
    .locals 1

    iget-boolean v0, p0, Lfw;->c:Z

    return v0
.end method


# virtual methods
.method public a(B)J
    .locals 8

    const-wide/16 v6, -0x1

    invoke-direct {p0}, Lfw;->a()V

    const-wide/16 v0, 0x0

    :cond_0
    iget-object v2, p0, Lfw;->a:Lfo;

    invoke-virtual {v2, p1, v0, v1}, Lfo;->a(BJ)J

    move-result-wide v0

    cmp-long v2, v0, v6

    if-nez v2, :cond_1

    iget-object v0, p0, Lfw;->a:Lfo;

    iget-wide v0, v0, Lfo;->b:J

    iget-object v2, p0, Lfw;->b:Lgb;

    iget-object v3, p0, Lfw;->a:Lfo;

    const-wide/16 v4, 0x800

    invoke-interface {v2, v3, v4, v5}, Lgb;->b(Lfo;J)J

    move-result-wide v2

    cmp-long v2, v2, v6

    if-nez v2, :cond_0

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    :cond_1
    return-wide v0
.end method

.method public a(Z)Ljava/lang/String;
    .locals 12

    const-wide/16 v10, -0x1

    const-wide/16 v8, 0x1

    const-wide/16 v2, 0x0

    invoke-direct {p0}, Lfw;->a()V

    move-wide v0, v2

    :cond_0
    iget-object v4, p0, Lfw;->a:Lfo;

    const/16 v5, 0xa

    invoke-virtual {v4, v5, v0, v1}, Lfo;->a(BJ)J

    move-result-wide v0

    cmp-long v4, v0, v10

    if-nez v4, :cond_3

    iget-object v0, p0, Lfw;->a:Lfo;

    iget-wide v0, v0, Lfo;->b:J

    iget-object v4, p0, Lfw;->b:Lgb;

    iget-object v5, p0, Lfw;->a:Lfo;

    const-wide/16 v6, 0x800

    invoke-interface {v4, v5, v6, v7}, Lgb;->b(Lfo;J)J

    move-result-wide v4

    cmp-long v4, v4, v10

    if-nez v4, :cond_0

    if-eqz p1, :cond_1

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    :cond_1
    iget-object v0, p0, Lfw;->a:Lfo;

    iget-wide v0, v0, Lfo;->b:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    iget-object v0, p0, Lfw;->a:Lfo;

    iget-wide v0, v0, Lfo;->b:J

    invoke-virtual {p0, v0, v1}, Lfw;->d(J)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    cmp-long v2, v0, v2

    if-lez v2, :cond_4

    iget-object v2, p0, Lfw;->a:Lfo;

    sub-long v3, v0, v8

    invoke-virtual {v2, v3, v4}, Lfo;->d(J)B

    move-result v2

    const/16 v3, 0xd

    if-ne v2, v3, :cond_4

    sub-long/2addr v0, v8

    invoke-virtual {p0, v0, v1}, Lfw;->d(J)Ljava/lang/String;

    move-result-object v0

    const-wide/16 v1, 0x2

    invoke-virtual {p0, v1, v2}, Lfw;->b(J)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0, v0, v1}, Lfw;->d(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v8, v9}, Lfw;->b(J)V

    goto :goto_0
.end method

.method public a(J)V
    .locals 4

    invoke-direct {p0}, Lfw;->a()V

    :cond_0
    iget-object v0, p0, Lfw;->a:Lfo;

    iget-wide v0, v0, Lfo;->b:J

    cmp-long v0, v0, p1

    if-gez v0, :cond_1

    iget-object v0, p0, Lfw;->b:Lgb;

    iget-object v1, p0, Lfw;->a:Lfo;

    const-wide/16 v2, 0x800

    invoke-interface {v0, v1, v2, v3}, Lgb;->b(Lfo;J)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    :cond_1
    return-void
.end method

.method public b(Lfo;J)J
    .locals 6

    const-wide/16 v4, 0x0

    const-wide/16 v0, -0x1

    cmp-long v2, p2, v4

    if-gez v2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "byteCount < 0: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-direct {p0}, Lfw;->a()V

    iget-object v2, p0, Lfw;->a:Lfo;

    iget-wide v2, v2, Lfo;->b:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    iget-object v2, p0, Lfw;->b:Lgb;

    iget-object v3, p0, Lfw;->a:Lfo;

    const-wide/16 v4, 0x800

    invoke-interface {v2, v3, v4, v5}, Lgb;->b(Lfo;J)J

    move-result-wide v2

    cmp-long v2, v2, v0

    if-nez v2, :cond_1

    :goto_0
    return-wide v0

    :cond_1
    iget-object v0, p0, Lfw;->a:Lfo;

    iget-wide v0, v0, Lfo;->b:J

    invoke-static {p2, p3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iget-object v2, p0, Lfw;->a:Lfo;

    invoke-virtual {v2, p1, v0, v1}, Lfo;->b(Lfo;J)J

    move-result-wide v0

    goto :goto_0
.end method

.method public b()Lfo;
    .locals 1

    iget-object v0, p0, Lfw;->a:Lfo;

    return-object v0
.end method

.method public b(J)V
    .locals 6

    const-wide/16 v4, 0x0

    invoke-direct {p0}, Lfw;->a()V

    :goto_0
    cmp-long v0, p1, v4

    if-lez v0, :cond_1

    iget-object v0, p0, Lfw;->a:Lfo;

    iget-wide v0, v0, Lfo;->b:J

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    iget-object v0, p0, Lfw;->b:Lgb;

    iget-object v1, p0, Lfw;->a:Lfo;

    const-wide/16 v2, 0x800

    invoke-interface {v0, v1, v2, v3}, Lgb;->b(Lfo;J)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lfw;->a:Lfo;

    invoke-virtual {v0}, Lfo;->l()J

    move-result-wide v0

    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iget-object v2, p0, Lfw;->a:Lfo;

    invoke-virtual {v2, v0, v1}, Lfo;->b(J)V

    sub-long/2addr p1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method public c(J)Lfi;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lfw;->a(J)V

    iget-object v0, p0, Lfw;->a:Lfo;

    invoke-virtual {v0, p1, p2}, Lfo;->c(J)Lfi;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 1

    iget-boolean v0, p0, Lfw;->c:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfw;->c:Z

    iget-object v0, p0, Lfw;->b:Lgb;

    invoke-interface {v0}, Lgb;->close()V

    iget-object v0, p0, Lfw;->a:Lfo;

    invoke-virtual {v0}, Lfo;->o()V

    goto :goto_0
.end method

.method public d(J)Ljava/lang/String;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lfw;->a(J)V

    iget-object v0, p0, Lfw;->a:Lfo;

    invoke-virtual {v0, p1, p2}, Lfo;->e(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()Z
    .locals 4

    invoke-direct {p0}, Lfw;->a()V

    iget-object v0, p0, Lfw;->a:Lfo;

    invoke-virtual {v0}, Lfo;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfw;->b:Lgb;

    iget-object v1, p0, Lfw;->a:Lfo;

    const-wide/16 v2, 0x800

    invoke-interface {v0, v1, v2, v3}, Lgb;->b(Lfo;J)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()B
    .locals 2

    const-wide/16 v0, 0x1

    invoke-virtual {p0, v0, v1}, Lfw;->a(J)V

    iget-object v0, p0, Lfw;->a:Lfo;

    invoke-virtual {v0}, Lfo;->f()B

    move-result v0

    return v0
.end method

.method public g()S
    .locals 2

    const-wide/16 v0, 0x2

    invoke-virtual {p0, v0, v1}, Lfw;->a(J)V

    iget-object v0, p0, Lfw;->a:Lfo;

    invoke-virtual {v0}, Lfo;->g()S

    move-result v0

    return v0
.end method

.method public h()I
    .locals 2

    const-wide/16 v0, 0x2

    invoke-virtual {p0, v0, v1}, Lfw;->a(J)V

    iget-object v0, p0, Lfw;->a:Lfo;

    invoke-virtual {v0}, Lfo;->h()I

    move-result v0

    return v0
.end method

.method public i()I
    .locals 2

    const-wide/16 v0, 0x4

    invoke-virtual {p0, v0, v1}, Lfw;->a(J)V

    iget-object v0, p0, Lfw;->a:Lfo;

    invoke-virtual {v0}, Lfo;->i()I

    move-result v0

    return v0
.end method

.method public j()I
    .locals 2

    const-wide/16 v0, 0x4

    invoke-virtual {p0, v0, v1}, Lfw;->a(J)V

    iget-object v0, p0, Lfw;->a:Lfo;

    invoke-virtual {v0}, Lfo;->j()I

    move-result v0

    return v0
.end method

.method public k()Ljava/io/InputStream;
    .locals 1

    new-instance v0, Lfx;

    invoke-direct {v0, p0}, Lfx;-><init>(Lfw;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "buffer("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lfw;->b:Lgb;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
