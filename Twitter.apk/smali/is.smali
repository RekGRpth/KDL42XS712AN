.class public Lis;
.super Lcom/twitter/library/service/o;
.source "Twttr"


# instance fields
.field private d:I

.field private e:J

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 1

    const-class v0, Lis;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    return-void
.end method


# virtual methods
.method public a(I)Lis;
    .locals 0

    iput p1, p0, Lis;->d:I

    return-object p0
.end method

.method public a(J)Lis;
    .locals 0

    iput-wide p1, p0, Lis;->e:J

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lis;
    .locals 0

    iput-object p1, p0, Lis;->f:Ljava/lang/String;

    return-object p0
.end method

.method protected b(Lcom/twitter/library/service/e;)V
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Lit;

    iget-object v1, p0, Lis;->l:Landroid/content/Context;

    invoke-virtual {p0}, Lis;->s()Lcom/twitter/library/service/p;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lit;-><init>(Landroid/content/Context;Lcom/twitter/library/service/p;)V

    invoke-virtual {v0, v3}, Lit;->a(I)Lit;

    move-result-object v0

    iget v1, p0, Lis;->d:I

    invoke-virtual {v0, v1}, Lit;->b(I)Lit;

    move-result-object v0

    iget-wide v1, p0, Lis;->e:J

    invoke-virtual {v0, v1, v2}, Lit;->a(J)Lit;

    move-result-object v0

    iget-object v1, p0, Lis;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lit;->a(Ljava/lang/String;)Lit;

    move-result-object v0

    invoke-virtual {v0, v3}, Lit;->a(Z)Lit;

    move-result-object v1

    invoke-virtual {p0, v1}, Lis;->a(Lcom/twitter/internal/android/service/a;)Lcom/twitter/library/service/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lit;->e()Z

    move-result v0

    new-instance v1, Lit;

    iget-object v2, p0, Lis;->l:Landroid/content/Context;

    invoke-virtual {p0}, Lis;->s()Lcom/twitter/library/service/p;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lit;-><init>(Landroid/content/Context;Lcom/twitter/library/service/p;)V

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lit;->a(I)Lit;

    move-result-object v1

    iget v2, p0, Lis;->d:I

    invoke-virtual {v1, v2}, Lit;->b(I)Lit;

    move-result-object v1

    iget-wide v2, p0, Lis;->e:J

    invoke-virtual {v1, v2, v3}, Lit;->a(J)Lit;

    move-result-object v1

    iget-object v2, p0, Lis;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lit;->a(Ljava/lang/String;)Lit;

    move-result-object v1

    invoke-virtual {v1, v0}, Lit;->a(Z)Lit;

    move-result-object v0

    invoke-virtual {p0, v0}, Lis;->a(Lcom/twitter/internal/android/service/a;)Lcom/twitter/library/service/e;

    move-result-object v0

    :cond_0
    invoke-virtual {p1, v0}, Lcom/twitter/library/service/e;->a(Lcom/twitter/library/service/e;)V

    return-void
.end method
