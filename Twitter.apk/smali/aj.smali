.class public interface abstract Laj;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract a()Lcom/google/android/gms/maps/model/CameraPosition;
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/PolylineOptions;)Lcv;
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/CircleOptions;)Ldb;
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/GroundOverlayOptions;)Lde;
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/MarkerOptions;)Ldh;
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/PolygonOptions;)Ldk;
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/TileOverlayOptions;)Ldn;
.end method

.method public abstract a(I)V
.end method

.method public abstract a(IIII)V
.end method

.method public abstract a(Lam;)V
.end method

.method public abstract a(Lbi;)V
.end method

.method public abstract a(Lbl;)V
.end method

.method public abstract a(Lbo;)V
.end method

.method public abstract a(Lbu;)V
.end method

.method public abstract a(Lbx;)V
.end method

.method public abstract a(Lca;)V
.end method

.method public abstract a(Lcd;)V
.end method

.method public abstract a(Lcg;)V
.end method

.method public abstract a(Lcj;)V
.end method

.method public abstract a(Lcm;)V
.end method

.method public abstract a(Lcom/google/android/gms/dynamic/k;)V
.end method

.method public abstract a(Lcom/google/android/gms/dynamic/k;ILbc;)V
.end method

.method public abstract a(Lcom/google/android/gms/dynamic/k;Lbc;)V
.end method

.method public abstract a(Lcp;Lcom/google/android/gms/dynamic/k;)V
.end method

.method public abstract a(Z)V
.end method

.method public abstract b()F
.end method

.method public abstract b(Lcom/google/android/gms/dynamic/k;)V
.end method

.method public abstract b(Z)Z
.end method

.method public abstract c()F
.end method

.method public abstract c(Z)V
.end method

.method public abstract d()V
.end method

.method public abstract d(Z)V
.end method

.method public abstract e()V
.end method

.method public abstract f()I
.end method

.method public abstract g()Z
.end method

.method public abstract h()Z
.end method

.method public abstract i()Z
.end method

.method public abstract j()Landroid/location/Location;
.end method

.method public abstract k()Lay;
.end method

.method public abstract l()Lav;
.end method

.method public abstract m()Lcom/google/android/gms/dynamic/k;
.end method

.method public abstract n()Z
.end method
