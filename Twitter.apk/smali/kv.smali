.class public Lkv;
.super Lkw;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lkw;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Landroid/net/Uri;Z)Lkx;
    .locals 1

    iget-object v0, p0, Lkv;->j:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/twitter/library/util/n;->a(Landroid/content/Context;Landroid/net/Uri;)I

    move-result v0

    iput v0, p0, Lkv;->m:I

    invoke-super {p0, p1, p2}, Lkw;->a(Landroid/net/Uri;Z)Lkx;

    move-result-object v0

    return-object v0
.end method

.method protected a(Ljava/io/FileInputStream;Z)Lkx;
    .locals 8

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v7, 0x1

    invoke-virtual {p1}, Ljava/io/FileInputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v1

    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v0, v4, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    iget v0, p0, Lkv;->a:I

    if-lez v0, :cond_0

    iget v0, p0, Lkv;->b:I

    if-gtz v0, :cond_1

    :cond_0
    if-eqz p2, :cond_4

    :cond_1
    iput-boolean v7, v4, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    invoke-static {v1, v3, v4}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    iget-boolean v0, p0, Lkv;->d:Z

    if-nez v0, :cond_3

    iget v0, p0, Lkv;->m:I

    const/4 v5, 0x6

    if-eq v0, v5, :cond_2

    iget v0, p0, Lkv;->m:I

    const/16 v5, 0x8

    if-ne v0, v5, :cond_3

    :cond_2
    iget v0, v4, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iget v5, v4, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iput v5, v4, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iput v0, v4, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    :cond_3
    iget v0, v4, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v5, v4, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-virtual {p0, v0, v5}, Lkv;->c(II)I

    move-result v0

    iput v0, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    iget v0, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    if-le v0, v7, :cond_5

    move v0, v7

    :goto_0
    iput-boolean v0, p0, Lkv;->h:Z

    :cond_4
    if-eqz p2, :cond_6

    iget v0, v4, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v1, v4, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-virtual {p0, v0, v1}, Lkv;->d(II)Landroid/graphics/Rect;

    move-result-object v1

    new-instance v0, Lkx;

    invoke-direct {v0, v1}, Lkx;-><init>(Landroid/graphics/Rect;)V

    :goto_1
    return-object v0

    :cond_5
    move v0, v2

    goto :goto_0

    :cond_6
    iput-boolean v2, v4, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    invoke-static {v1, v3, v4}, Landroid/graphics/BitmapFactory;->decodeFileDescriptor(Ljava/io/FileDescriptor;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    iget-boolean v0, p0, Lkv;->d:Z

    if-nez v0, :cond_7

    iget v0, p0, Lkv;->m:I

    invoke-static {v0}, Lcom/twitter/library/util/n;->a(I)I

    move-result v0

    if-eqz v0, :cond_7

    iget-object v4, p0, Lkv;->j:Landroid/content/Context;

    invoke-static {v4, v0, v1}, Lcom/twitter/library/util/n;->a(Landroid/content/Context;ILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    :cond_7
    if-eqz v1, :cond_a

    iget-boolean v0, p0, Lkv;->g:Z

    if-eqz v0, :cond_9

    iget v0, p0, Lkv;->a:I

    if-lez v0, :cond_9

    iget v0, p0, Lkv;->b:I

    if-lez v0, :cond_9

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-virtual {p0, v0, v3}, Lkv;->e(II)F

    move-result v0

    const/high16 v3, 0x3f800000    # 1.0f

    cmpg-float v3, v0, v3

    if-gez v3, :cond_9

    new-instance v6, Landroid/graphics/Matrix;

    invoke-direct {v6}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {v6, v0, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    iget-object v0, p0, Lkv;->j:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    move v3, v2

    invoke-static/range {v0 .. v7}, Lcom/twitter/library/util/n;->a(Landroid/content/Context;Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    move-object v1, v0

    :cond_8
    iput-boolean v7, p0, Lkv;->h:Z

    :cond_9
    new-instance v0, Lkx;

    invoke-direct {v0, v1}, Lkx;-><init>(Landroid/graphics/Bitmap;)V

    goto :goto_1

    :cond_a
    move-object v0, v3

    goto :goto_1
.end method
