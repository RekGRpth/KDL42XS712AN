.class public final Lil;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final abbr_number_unit_millions:I = 0x7f0f0001

.field public static final abbr_number_unit_thousands:I = 0x7f0f0002

.field public static final amazon_app_store_url_format:I = 0x7f0f0026

.field public static final amplify_button_fullscreen:I = 0x7f0f0027

.field public static final amplify_playback_forbidden:I = 0x7f0f0028

.field public static final amplify_playback_forbidden_device:I = 0x7f0f0029

.field public static final amplify_playlist_download_failed:I = 0x7f0f002a

.field public static final amplify_preroll_countdown:I = 0x7f0f002b

.field public static final app_download_title:I = 0x7f0f0030

.field public static final app_launch_title:I = 0x7f0f0031

.field public static final app_name:I = 0x7f0f0032

.field public static final auth_client_needs_enabling_title:I = 0x7f0f0035

.field public static final auth_client_needs_installation_title:I = 0x7f0f0036

.field public static final auth_client_needs_update_title:I = 0x7f0f0037

.field public static final auth_client_play_services_err_notification_msg:I = 0x7f0f0038

.field public static final auth_client_requested_by_msg:I = 0x7f0f0039

.field public static final auth_client_using_bad_version_title:I = 0x7f0f003a

.field public static final base_host:I = 0x7f0f0046

.field public static final base_upload_host:I = 0x7f0f0047

.field public static final base_url:I = 0x7f0f0048

.field public static final base_url_mobile:I = 0x7f0f0049

.field public static final call_now:I = 0x7f0f0086

.field public static final call_now_title:I = 0x7f0f0087

.field public static final common_google_play_services_enable_button:I = 0x7f0f00ac

.field public static final common_google_play_services_enable_text:I = 0x7f0f00ad

.field public static final common_google_play_services_enable_title:I = 0x7f0f00ae

.field public static final common_google_play_services_install_button:I = 0x7f0f00af

.field public static final common_google_play_services_install_text_phone:I = 0x7f0f00b0

.field public static final common_google_play_services_install_text_tablet:I = 0x7f0f00b1

.field public static final common_google_play_services_install_title:I = 0x7f0f00b2

.field public static final common_google_play_services_invalid_account_text:I = 0x7f0f00b3

.field public static final common_google_play_services_invalid_account_title:I = 0x7f0f00b4

.field public static final common_google_play_services_network_error_text:I = 0x7f0f00b5

.field public static final common_google_play_services_network_error_title:I = 0x7f0f00b6

.field public static final common_google_play_services_unknown_issue:I = 0x7f0f00b7

.field public static final common_google_play_services_unsupported_date_text:I = 0x7f0f00b8

.field public static final common_google_play_services_unsupported_text:I = 0x7f0f00b9

.field public static final common_google_play_services_unsupported_title:I = 0x7f0f00ba

.field public static final common_google_play_services_update_button:I = 0x7f0f00bb

.field public static final common_google_play_services_update_text:I = 0x7f0f00bc

.field public static final common_google_play_services_update_title:I = 0x7f0f00bd

.field public static final common_signin_button_text:I = 0x7f0f00be

.field public static final common_signin_button_text_long:I = 0x7f0f00bf

.field public static final content_description_follow_button:I = 0x7f0f00d1

.field public static final content_description_unfollow_button:I = 0x7f0f00d2

.field public static final convo_to_reply:I = 0x7f0f00d4

.field public static final convo_to_you:I = 0x7f0f00d5

.field public static final convo_tweet_someone_and_you:I = 0x7f0f00d6

.field public static final convo_tweet_you_and_someone:I = 0x7f0f00d7

.field public static final date_format_long:I = 0x7f0f00e8

.field public static final date_format_short:I = 0x7f0f00e9

.field public static final default_user_value:I = 0x7f0f00f1

.field public static final file_photo_name:I = 0x7f0f018b

.field public static final google_play_details_url_format:I = 0x7f0f01b8

.field public static final google_play_web_details_url_format:I = 0x7f0f01b9

.field public static final highlight_context_nearby:I = 0x7f0f01c0

.field public static final highlight_context_popular:I = 0x7f0f01c1

.field public static final highlight_tweet:I = 0x7f0f01cb

.field public static final highlight_tweet_today:I = 0x7f0f01cc

.field public static final intent_chooser_title:I = 0x7f0f01f0

.field public static final label_direct_message:I = 0x7f0f01fc

.field public static final label_quote_tweet:I = 0x7f0f0200

.field public static final lifeline_alert:I = 0x7f0f0205

.field public static final magic_recs_favorite_notif:I = 0x7f0f025b

.field public static final magic_recs_retweet_notif:I = 0x7f0f025d

.field public static final media_error_audio_focus_rejected:I = 0x7f0f0266

.field public static final media_tag_multiple_summary:I = 0x7f0f026f

.field public static final media_tag_two_summary:I = 0x7f0f0274

.field public static final media_tag_user_display_name:I = 0x7f0f0275

.field public static final media_tag_you:I = 0x7f0f0277

.field public static final media_tag_you_multiple_summary:I = 0x7f0f0278

.field public static final media_tag_you_two_summary:I = 0x7f0f0279

.field public static final mobile_config_url:I = 0x7f0f028d

.field public static final now:I = 0x7f0f02d2

.field public static final pause:I = 0x7f0f02f5

.field public static final play:I = 0x7f0f030a

.field public static final preference_notification_error:I = 0x7f0f031e

.field public static final preference_notification_success:I = 0x7f0f031f

.field public static final preference_notification_too_many_devices:I = 0x7f0f0320

.field public static final promoted_by:I = 0x7f0f0333

.field public static final promoted_without_advertiser:I = 0x7f0f0335

.field public static final quote_format:I = 0x7f0f033f

.field public static final recent_tweets_header_title:I = 0x7f0f0345

.field public static final recommended_default:I = 0x7f0f0346

.field public static final replay:I = 0x7f0f0352

.field public static final search_share_long_format:I = 0x7f0f03b5

.field public static final search_share_short_format:I = 0x7f0f03b6

.field public static final search_share_subject_long_format:I = 0x7f0f03b7

.field public static final social_both_follow:I = 0x7f0f0452

.field public static final social_both_followed_by:I = 0x7f0f0453

.field public static final social_context_mutual_follow:I = 0x7f0f0454

.field public static final social_conversation_tweet:I = 0x7f0f0455

.field public static final social_conversation_tweet_two:I = 0x7f0f0456

.field public static final social_fav_with_two_user:I = 0x7f0f0457

.field public static final social_fav_with_two_user_accessibility_description:I = 0x7f0f0458

.field public static final social_fav_with_user:I = 0x7f0f0459

.field public static final social_fav_with_user_accessibility_description:I = 0x7f0f045a

.field public static final social_follow_and_fav:I = 0x7f0f045b

.field public static final social_follow_and_fav_accessibility_description:I = 0x7f0f045c

.field public static final social_follow_and_follow:I = 0x7f0f045d

.field public static final social_follow_and_follow_accessibility_description:I = 0x7f0f045e

.field public static final social_follow_and_more_follow:I = 0x7f0f045f

.field public static final social_follow_and_reply:I = 0x7f0f0460

.field public static final social_follow_and_reply_accessibility_description:I = 0x7f0f0461

.field public static final social_follower_and_fav:I = 0x7f0f0462

.field public static final social_follower_and_fav_accessibility_description:I = 0x7f0f0463

.field public static final social_follower_and_reply:I = 0x7f0f0464

.field public static final social_follower_and_reply_accessibility_description:I = 0x7f0f0465

.field public static final social_follower_and_retweets:I = 0x7f0f0466

.field public static final social_follower_and_retweets_accessibility_description:I = 0x7f0f0467

.field public static final social_follower_of_follower:I = 0x7f0f0468

.field public static final social_following:I = 0x7f0f0469

.field public static final social_follows_you:I = 0x7f0f046a

.field public static final social_reply_to_follow:I = 0x7f0f046b

.field public static final social_reply_to_follower:I = 0x7f0f046c

.field public static final social_retweet_and_fav_count:I = 0x7f0f046d

.field public static final social_retweet_with_user:I = 0x7f0f046e

.field public static final social_retweet_with_user_accessibility_description:I = 0x7f0f046f

.field public static final social_top_news:I = 0x7f0f0471

.field public static final social_trending_topic:I = 0x7f0f0472

.field public static final social_who_to_follow:I = 0x7f0f0473

.field public static final timeline_share_long_format:I = 0x7f0f04a8

.field public static final timeline_share_short_format:I = 0x7f0f04a9

.field public static final timeline_share_subject_long_format:I = 0x7f0f04aa

.field public static final timeline_tweet_format:I = 0x7f0f04ab

.field public static final top_tweet:I = 0x7f0f04ae

.field public static final tweet_date_format:I = 0x7f0f04d0

.field public static final tweets_retweeted:I = 0x7f0f04f0

.field public static final tweets_retweeted_accessibility_description:I = 0x7f0f04f1

.field public static final tweets_share_long_format:I = 0x7f0f04f3

.field public static final tweets_share_short_format:I = 0x7f0f04f4

.field public static final tweets_share_status:I = 0x7f0f04f5

.field public static final tweets_share_subject_long_format:I = 0x7f0f04f6

.field public static final twitter_authority:I = 0x7f0f04fd

.field public static final twitter_support_authority:I = 0x7f0f0504

.field public static final user_share_long_format:I = 0x7f0f051a

.field public static final user_share_short_format:I = 0x7f0f051b

.field public static final user_share_subject_long_format:I = 0x7f0f051c
