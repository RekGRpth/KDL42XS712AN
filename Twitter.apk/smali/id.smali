.class public final Lid;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final alert:I = 0x7f0b0001

.field public static final black:I = 0x7f0b0003

.field public static final border_gray:I = 0x7f0b0004

.field public static final clear:I = 0x7f0b0022

.field public static final common_action_bar_splitter:I = 0x7f0b0023

.field public static final common_signin_btn_dark_text_default:I = 0x7f0b0024

.field public static final common_signin_btn_dark_text_disabled:I = 0x7f0b0025

.field public static final common_signin_btn_dark_text_focused:I = 0x7f0b0026

.field public static final common_signin_btn_dark_text_pressed:I = 0x7f0b0027

.field public static final common_signin_btn_default_background:I = 0x7f0b0028

.field public static final common_signin_btn_light_text_default:I = 0x7f0b0029

.field public static final common_signin_btn_light_text_disabled:I = 0x7f0b002a

.field public static final common_signin_btn_light_text_focused:I = 0x7f0b002b

.field public static final common_signin_btn_light_text_pressed:I = 0x7f0b002c

.field public static final common_signin_btn_text_dark:I = 0x7f0b0091

.field public static final common_signin_btn_text_light:I = 0x7f0b0092

.field public static final dark_blue:I = 0x7f0b0030

.field public static final dark_gray:I = 0x7f0b0031

.field public static final dark_green:I = 0x7f0b0032

.field public static final dark_orange:I = 0x7f0b0033

.field public static final dark_purple:I = 0x7f0b0034

.field public static final dark_red:I = 0x7f0b0035

.field public static final dark_transparent_black:I = 0x7f0b0036

.field public static final deep_blue:I = 0x7f0b0042

.field public static final deep_green:I = 0x7f0b0043

.field public static final deep_purple:I = 0x7f0b0044

.field public static final deep_red:I = 0x7f0b0045

.field public static final deep_transparent_black:I = 0x7f0b0046

.field public static final faded_blue:I = 0x7f0b004d

.field public static final faded_green:I = 0x7f0b004e

.field public static final faded_purple:I = 0x7f0b004f

.field public static final faded_red:I = 0x7f0b0050

.field public static final faded_yellow:I = 0x7f0b0051

.field public static final faint_gray:I = 0x7f0b0052

.field public static final faint_transparent_black:I = 0x7f0b0053

.field public static final faint_transparent_white:I = 0x7f0b0054

.field public static final gray:I = 0x7f0b0057

.field public static final green:I = 0x7f0b0058

.field public static final light_blue:I = 0x7f0b005a

.field public static final light_green:I = 0x7f0b005b

.field public static final light_purple:I = 0x7f0b005c

.field public static final light_red:I = 0x7f0b005d

.field public static final light_transparent_black:I = 0x7f0b005e

.field public static final light_yellow:I = 0x7f0b005f

.field public static final medium_gray:I = 0x7f0b0064

.field public static final orange:I = 0x7f0b0066

.field public static final placeholder_bg:I = 0x7f0b0069

.field public static final pressed:I = 0x7f0b006c

.field public static final purple:I = 0x7f0b0071

.field public static final red:I = 0x7f0b0072

.field public static final soft_white:I = 0x7f0b0075

.field public static final strong_white:I = 0x7f0b0076

.field public static final text:I = 0x7f0b0077

.field public static final transparent_black:I = 0x7f0b0078

.field public static final transparent_white:I = 0x7f0b007a

.field public static final twitter_blue:I = 0x7f0b007b

.field public static final unread:I = 0x7f0b007c

.field public static final vine_green:I = 0x7f0b007d

.field public static final white:I = 0x7f0b007e

.field public static final yellow:I = 0x7f0b0084
