.class public Lma;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/spongycastle/crypto/a;


# instance fields
.field private a:Ljava/security/SecureRandom;

.field private b:Lorg/spongycastle/crypto/a;

.field private c:Z

.field private d:Z

.field private e:Z


# direct methods
.method public constructor <init>(Lorg/spongycastle/crypto/a;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lma;->b:Lorg/spongycastle/crypto/a;

    invoke-direct {p0}, Lma;->c()Z

    move-result v0

    iput-boolean v0, p0, Lma;->e:Z

    return-void
.end method

.method private b([BII)[B
    .locals 4

    const/4 v0, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0}, Lma;->a()I

    move-result v1

    if-le p3, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "input data too large"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v1, p0, Lma;->b:Lorg/spongycastle/crypto/a;

    invoke-interface {v1}, Lorg/spongycastle/crypto/a;->a()I

    move-result v1

    new-array v1, v1, [B

    iget-boolean v2, p0, Lma;->d:Z

    if-eqz v2, :cond_1

    aput-byte v0, v1, v3

    :goto_0
    array-length v2, v1

    sub-int/2addr v2, p3

    add-int/lit8 v2, v2, -0x1

    if-eq v0, v2, :cond_3

    const/4 v2, -0x1

    aput-byte v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lma;->a:Ljava/security/SecureRandom;

    invoke-virtual {v2, v1}, Ljava/security/SecureRandom;->nextBytes([B)V

    const/4 v2, 0x2

    aput-byte v2, v1, v3

    :goto_1
    array-length v2, v1

    sub-int/2addr v2, p3

    add-int/lit8 v2, v2, -0x1

    if-eq v0, v2, :cond_3

    :goto_2
    aget-byte v2, v1, v0

    if-nez v2, :cond_2

    iget-object v2, p0, Lma;->a:Ljava/security/SecureRandom;

    invoke-virtual {v2}, Ljava/security/SecureRandom;->nextInt()I

    move-result v2

    int-to-byte v2, v2

    aput-byte v2, v1, v0

    goto :goto_2

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    array-length v0, v1

    sub-int/2addr v0, p3

    add-int/lit8 v0, v0, -0x1

    aput-byte v3, v1, v0

    array-length v0, v1

    sub-int/2addr v0, p3

    invoke-static {p1, p2, v1, v0, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v0, p0, Lma;->b:Lorg/spongycastle/crypto/a;

    array-length v2, v1

    invoke-interface {v0, v1, v3, v2}, Lorg/spongycastle/crypto/a;->a([BII)[B

    move-result-object v0

    return-object v0
.end method

.method private c()Z
    .locals 2

    new-instance v0, Lmb;

    invoke-direct {v0, p0}, Lmb;-><init>(Lma;)V

    invoke-static {v0}, Ljava/security/AccessController;->doPrivileged(Ljava/security/PrivilegedAction;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string/jumbo v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c([BII)[B
    .locals 7

    const/4 v6, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lma;->b:Lorg/spongycastle/crypto/a;

    invoke-interface {v0, p1, p2, p3}, Lorg/spongycastle/crypto/a;->a([BII)[B

    move-result-object v2

    array-length v0, v2

    invoke-virtual {p0}, Lma;->b()I

    move-result v3

    if-ge v0, v3, :cond_0

    new-instance v0, Lorg/spongycastle/crypto/InvalidCipherTextException;

    const-string/jumbo v1, "block truncated"

    invoke-direct {v0, v1}, Lorg/spongycastle/crypto/InvalidCipherTextException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    aget-byte v3, v2, v6

    if-eq v3, v1, :cond_1

    const/4 v0, 0x2

    if-eq v3, v0, :cond_1

    new-instance v0, Lorg/spongycastle/crypto/InvalidCipherTextException;

    const-string/jumbo v1, "unknown block type"

    invoke-direct {v0, v1}, Lorg/spongycastle/crypto/InvalidCipherTextException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-boolean v0, p0, Lma;->e:Z

    if-eqz v0, :cond_2

    array-length v0, v2

    iget-object v4, p0, Lma;->b:Lorg/spongycastle/crypto/a;

    invoke-interface {v4}, Lorg/spongycastle/crypto/a;->b()I

    move-result v4

    if-eq v0, v4, :cond_2

    new-instance v0, Lorg/spongycastle/crypto/InvalidCipherTextException;

    const-string/jumbo v1, "block incorrect size"

    invoke-direct {v0, v1}, Lorg/spongycastle/crypto/InvalidCipherTextException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    move v0, v1

    :goto_0
    array-length v4, v2

    if-eq v0, v4, :cond_3

    aget-byte v4, v2, v0

    if-nez v4, :cond_5

    :cond_3
    add-int/lit8 v0, v0, 0x1

    array-length v1, v2

    if-gt v0, v1, :cond_4

    const/16 v1, 0xa

    if-ge v0, v1, :cond_7

    :cond_4
    new-instance v0, Lorg/spongycastle/crypto/InvalidCipherTextException;

    const-string/jumbo v1, "no data in block"

    invoke-direct {v0, v1}, Lorg/spongycastle/crypto/InvalidCipherTextException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    if-ne v3, v1, :cond_6

    const/4 v5, -0x1

    if-eq v4, v5, :cond_6

    new-instance v0, Lorg/spongycastle/crypto/InvalidCipherTextException;

    const-string/jumbo v1, "block padding incorrect"

    invoke-direct {v0, v1}, Lorg/spongycastle/crypto/InvalidCipherTextException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_7
    array-length v1, v2

    sub-int/2addr v1, v0

    new-array v1, v1, [B

    array-length v3, v1

    invoke-static {v2, v0, v1, v6, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v1
.end method


# virtual methods
.method public a()I
    .locals 2

    iget-object v0, p0, Lma;->b:Lorg/spongycastle/crypto/a;

    invoke-interface {v0}, Lorg/spongycastle/crypto/a;->a()I

    move-result v0

    iget-boolean v1, p0, Lma;->c:Z

    if-eqz v1, :cond_0

    add-int/lit8 v0, v0, -0xa

    :cond_0
    return v0
.end method

.method public a(ZLorg/spongycastle/crypto/c;)V
    .locals 2

    instance-of v0, p2, Lmg;

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Lmg;

    invoke-virtual {v0}, Lmg;->a()Ljava/security/SecureRandom;

    move-result-object v1

    iput-object v1, p0, Lma;->a:Ljava/security/SecureRandom;

    invoke-virtual {v0}, Lmg;->b()Lorg/spongycastle/crypto/c;

    move-result-object v0

    check-cast v0, Lmf;

    :goto_0
    iget-object v1, p0, Lma;->b:Lorg/spongycastle/crypto/a;

    invoke-interface {v1, p1, p2}, Lorg/spongycastle/crypto/a;->a(ZLorg/spongycastle/crypto/c;)V

    invoke-virtual {v0}, Lmf;->a()Z

    move-result v0

    iput-boolean v0, p0, Lma;->d:Z

    iput-boolean p1, p0, Lma;->c:Z

    return-void

    :cond_0
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    iput-object v0, p0, Lma;->a:Ljava/security/SecureRandom;

    move-object v0, p2

    check-cast v0, Lmf;

    goto :goto_0
.end method

.method public a([BII)[B
    .locals 1

    iget-boolean v0, p0, Lma;->c:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2, p3}, Lma;->b([BII)[B

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lma;->c([BII)[B

    move-result-object v0

    goto :goto_0
.end method

.method public b()I
    .locals 2

    iget-object v0, p0, Lma;->b:Lorg/spongycastle/crypto/a;

    invoke-interface {v0}, Lorg/spongycastle/crypto/a;->b()I

    move-result v0

    iget-boolean v1, p0, Lma;->c:Z

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    add-int/lit8 v0, v0, -0xa

    goto :goto_0
.end method
