.class public Len;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public a:Landroid/graphics/Point;

.field public b:Landroid/graphics/Point;

.field public c:Landroid/graphics/Point;

.field public d:I

.field public e:I

.field public f:F

.field public g:F

.field public h:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Len;->h:I

    return v0
.end method

.method public a(F)V
    .locals 0

    iput p1, p0, Len;->f:F

    return-void
.end method

.method public a(I)V
    .locals 0

    iput p1, p0, Len;->h:I

    return-void
.end method

.method public a(Landroid/graphics/Point;)V
    .locals 0

    iput-object p1, p0, Len;->a:Landroid/graphics/Point;

    return-void
.end method

.method public b()Landroid/graphics/Point;
    .locals 1

    iget-object v0, p0, Len;->a:Landroid/graphics/Point;

    return-object v0
.end method

.method public b(F)V
    .locals 0

    iput p1, p0, Len;->g:F

    return-void
.end method

.method public b(I)V
    .locals 0

    iput p1, p0, Len;->d:I

    return-void
.end method

.method public b(Landroid/graphics/Point;)V
    .locals 0

    iput-object p1, p0, Len;->b:Landroid/graphics/Point;

    return-void
.end method

.method public c()Landroid/graphics/Point;
    .locals 1

    iget-object v0, p0, Len;->b:Landroid/graphics/Point;

    return-object v0
.end method

.method public c(I)V
    .locals 0

    iput p1, p0, Len;->e:I

    return-void
.end method

.method public c(Landroid/graphics/Point;)V
    .locals 0

    iput-object p1, p0, Len;->c:Landroid/graphics/Point;

    return-void
.end method

.method public d()Landroid/graphics/Point;
    .locals 1

    iget-object v0, p0, Len;->c:Landroid/graphics/Point;

    return-object v0
.end method

.method public e()I
    .locals 1

    iget v0, p0, Len;->d:I

    return v0
.end method

.method public f()I
    .locals 1

    iget v0, p0, Len;->e:I

    return v0
.end method

.method public g()F
    .locals 1

    iget v0, p0, Len;->f:F

    return v0
.end method

.method public h()F
    .locals 1

    iget v0, p0, Len;->g:F

    return v0
.end method
