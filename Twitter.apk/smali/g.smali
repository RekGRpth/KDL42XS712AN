.class public abstract Lg;
.super La;
.source "Twttr"


# static fields
.field protected static final g:[I


# instance fields
.field protected final h:Lcom/fasterxml/jackson/core/io/c;

.field protected i:[I

.field protected j:I

.field protected k:Lcom/fasterxml/jackson/core/io/CharacterEscapes;

.field protected l:Lcom/fasterxml/jackson/core/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/fasterxml/jackson/core/io/b;->f()[I

    move-result-object v0

    sput-object v0, Lg;->g:[I

    return-void
.end method

.method public constructor <init>(Lcom/fasterxml/jackson/core/io/c;ILcom/fasterxml/jackson/core/b;)V
    .locals 1

    invoke-direct {p0, p2, p3}, La;-><init>(ILcom/fasterxml/jackson/core/b;)V

    sget-object v0, Lg;->g:[I

    iput-object v0, p0, Lg;->i:[I

    sget-object v0, Lcom/fasterxml/jackson/core/util/DefaultPrettyPrinter;->a:Lcom/fasterxml/jackson/core/io/SerializedString;

    iput-object v0, p0, Lg;->l:Lcom/fasterxml/jackson/core/d;

    iput-object p1, p0, Lg;->h:Lcom/fasterxml/jackson/core/io/c;

    sget-object v0, Lcom/fasterxml/jackson/core/JsonGenerator$Feature;->g:Lcom/fasterxml/jackson/core/JsonGenerator$Feature;

    invoke-virtual {p0, v0}, Lg;->a(Lcom/fasterxml/jackson/core/JsonGenerator$Feature;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x7f

    invoke-virtual {p0, v0}, Lg;->a(I)Lcom/fasterxml/jackson/core/JsonGenerator;

    :cond_0
    return-void
.end method


# virtual methods
.method public a(I)Lcom/fasterxml/jackson/core/JsonGenerator;
    .locals 0

    if-gez p1, :cond_0

    const/4 p1, 0x0

    :cond_0
    iput p1, p0, Lg;->j:I

    return-object p0
.end method

.method public a(Lcom/fasterxml/jackson/core/d;)Lcom/fasterxml/jackson/core/JsonGenerator;
    .locals 0

    iput-object p1, p0, Lg;->l:Lcom/fasterxml/jackson/core/d;

    return-object p0
.end method

.method public a(Lcom/fasterxml/jackson/core/io/CharacterEscapes;)Lcom/fasterxml/jackson/core/JsonGenerator;
    .locals 1

    iput-object p1, p0, Lg;->k:Lcom/fasterxml/jackson/core/io/CharacterEscapes;

    if-nez p1, :cond_0

    sget-object v0, Lg;->g:[I

    iput-object v0, p0, Lg;->i:[I

    :goto_0
    return-object p0

    :cond_0
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/io/CharacterEscapes;->a()[I

    move-result-object v0

    iput-object v0, p0, Lg;->i:[I

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-virtual {p0, p1}, Lg;->a(Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lg;->b(Ljava/lang/String;)V

    return-void
.end method
