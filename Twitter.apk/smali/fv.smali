.class Lfv;
.super Ljava/io/OutputStream;
.source "Twttr"


# instance fields
.field final synthetic a:Lfu;


# direct methods
.method constructor <init>(Lfu;)V
    .locals 0

    iput-object p1, p0, Lfv;->a:Lfu;

    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    iget-object v0, p0, Lfv;->a:Lfu;

    invoke-static {v0}, Lfu;->a(Lfu;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    iget-object v0, p0, Lfv;->a:Lfu;

    invoke-virtual {v0}, Lfu;->close()V

    return-void
.end method

.method public flush()V
    .locals 1

    iget-object v0, p0, Lfv;->a:Lfu;

    invoke-static {v0}, Lfu;->a(Lfu;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lfv;->a:Lfu;

    invoke-virtual {v0}, Lfu;->a()V

    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lfv;->a:Lfu;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".outputStream()"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public write(I)V
    .locals 2

    invoke-direct {p0}, Lfv;->a()V

    iget-object v0, p0, Lfv;->a:Lfu;

    iget-object v0, v0, Lfu;->a:Lfo;

    int-to-byte v1, p1

    invoke-virtual {v0, v1}, Lfo;->c(I)Lfo;

    iget-object v0, p0, Lfv;->a:Lfu;

    invoke-virtual {v0}, Lfu;->c()Lfg;

    return-void
.end method

.method public write([BII)V
    .locals 1

    invoke-direct {p0}, Lfv;->a()V

    iget-object v0, p0, Lfv;->a:Lfu;

    iget-object v0, v0, Lfu;->a:Lfo;

    invoke-virtual {v0, p1, p2, p3}, Lfo;->c([BII)Lfo;

    iget-object v0, p0, Lfv;->a:Lfu;

    invoke-virtual {v0}, Lfu;->c()Lfg;

    return-void
.end method
