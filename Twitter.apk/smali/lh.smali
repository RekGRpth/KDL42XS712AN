.class final Llh;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field final synthetic a:Lld;

.field private final b:Ljava/lang/String;

.field private final c:[J

.field private d:Z

.field private e:Llf;


# direct methods
.method private constructor <init>(Lld;Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Llh;->a:Lld;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Llh;->b:Ljava/lang/String;

    invoke-static {p1}, Lld;->e(Lld;)I

    move-result v0

    new-array v0, v0, [J

    iput-object v0, p0, Llh;->c:[J

    return-void
.end method

.method synthetic constructor <init>(Lld;Ljava/lang/String;Lle;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Llh;-><init>(Lld;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Llh;Llf;)Llf;
    .locals 0

    iput-object p1, p0, Llh;->e:Llf;

    return-object p1
.end method

.method static synthetic a(Llh;Z)Z
    .locals 0

    iput-boolean p1, p0, Llh;->d:Z

    return p1
.end method

.method static synthetic a(Llh;)[J
    .locals 1

    iget-object v0, p0, Llh;->c:[J

    return-object v0
.end method

.method static synthetic b(Llh;)Llf;
    .locals 1

    iget-object v0, p0, Llh;->e:Llf;

    return-object v0
.end method

.method static synthetic c(Llh;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Llh;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Llh;)Z
    .locals 1

    iget-boolean v0, p0, Llh;->d:Z

    return v0
.end method


# virtual methods
.method public a(I)Ljava/io/File;
    .locals 4

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Llh;->a:Lld;

    invoke-static {v1}, Lld;->f(Lld;)Ljava/io/File;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Llh;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method public b(I)Ljava/io/File;
    .locals 4

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Llh;->a:Lld;

    invoke-static {v1}, Lld;->f(Lld;)Ljava/io/File;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Llh;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ".tmp"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method
