.class public Ljo;
.super Lcom/twitter/library/service/o;
.source "Twttr"


# instance fields
.field private final d:J

.field private final e:J

.field private f:J

.field private g:J

.field private n:J

.field private o:Lcom/twitter/library/api/ao;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JJ)V
    .locals 1

    const-class v0, Ljo;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    iput-wide p3, p0, Ljo;->d:J

    iput-wide p5, p0, Ljo;->e:J

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/library/service/e;)Lcom/twitter/internal/network/HttpOperation;
    .locals 7

    const-wide/16 v4, 0x0

    const/4 v6, 0x1

    iget-object v0, p0, Ljo;->m:Lcom/twitter/library/network/aa;

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "1.1"

    aput-object v3, v1, v2

    const-string/jumbo v2, "lists"

    aput-object v2, v1, v6

    const/4 v2, 0x2

    const-string/jumbo v3, "statuses"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "list_id"

    iget-wide v2, p0, Ljo;->e:J

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    iget-wide v1, p0, Ljo;->f:J

    cmp-long v1, v1, v4

    if-lez v1, :cond_0

    const-string/jumbo v1, "since_id"

    iget-wide v2, p0, Ljo;->f:J

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    :cond_0
    iget-wide v1, p0, Ljo;->g:J

    cmp-long v1, v1, v4

    if-lez v1, :cond_1

    const-string/jumbo v1, "max_id"

    iget-wide v2, p0, Ljo;->g:J

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    :cond_1
    iget-wide v1, p0, Ljo;->n:J

    cmp-long v1, v1, v4

    if-lez v1, :cond_2

    const-string/jumbo v1, "per_page"

    iget-wide v2, p0, Ljo;->n:J

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    :cond_2
    const-string/jumbo v1, "include_entities"

    invoke-static {v0, v1, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v1, "include_media_features"

    invoke-static {v0, v1, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v1, "include_cards"

    invoke-static {v0, v1, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v1, "include_user_entities"

    invoke-static {v0, v1, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v1, "include_rts"

    invoke-static {v0, v1, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    iget-object v1, p0, Ljo;->m:Lcom/twitter/library/network/aa;

    invoke-virtual {v1, v0}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;)V

    new-instance v1, Lcom/twitter/library/util/f;

    iget-object v2, p0, Ljo;->l:Landroid/content/Context;

    iget-wide v3, p0, Ljo;->d:J

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    invoke-static {v6, v1}, Lcom/twitter/library/api/ao;->a(ILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v1

    iput-object v1, p0, Ljo;->o:Lcom/twitter/library/api/ao;

    invoke-virtual {p0}, Ljo;->s()Lcom/twitter/library/service/p;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/network/d;

    iget-object v3, p0, Ljo;->l:Landroid/content/Context;

    invoke-direct {v2, v3, v0}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v3, v1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v0

    new-instance v2, Lcom/twitter/library/network/n;

    iget-object v1, v1, Lcom/twitter/library/service/p;->d:Lcom/twitter/library/network/OAuthToken;

    invoke-direct {v2, v1}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    invoke-virtual {v0, v2}, Lcom/twitter/library/network/d;->b(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    iget-object v1, p0, Ljo;->o:Lcom/twitter/library/api/ao;

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    return-object v0
.end method

.method public a(J)Ljo;
    .locals 0

    iput-wide p1, p0, Ljo;->f:J

    return-object p0
.end method

.method protected a(Lcom/twitter/internal/network/HttpOperation;Lcom/twitter/library/service/e;)V
    .locals 17

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/internal/network/HttpOperation;->k()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Ljo;->o:Lcom/twitter/library/api/ao;

    invoke-virtual {v2}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-wide v4, v0, Ljo;->g:J

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-lez v2, :cond_1

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_1

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/TwitterStatus;

    iget-wide v4, v2, Lcom/twitter/library/api/TwitterStatus;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v12

    :goto_0
    move-object/from16 v0, p0

    iget-wide v4, v0, Ljo;->g:J

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-lez v2, :cond_2

    const/4 v10, 0x1

    :goto_1
    invoke-virtual/range {p0 .. p0}, Ljo;->w()Lcom/twitter/library/provider/az;

    move-result-object v2

    move-object/from16 v0, p0

    iget-wide v4, v0, Ljo;->d:J

    const/16 v6, 0x9

    move-object/from16 v0, p0

    iget-wide v7, v0, Ljo;->e:J

    move-object/from16 v0, p0

    iget-wide v13, v0, Ljo;->g:J

    const-wide/16 v15, 0x0

    cmp-long v9, v13, v15

    if-lez v9, :cond_3

    const/4 v9, 0x1

    :goto_2
    if-nez v12, :cond_4

    const/4 v11, 0x1

    :goto_3
    const/4 v13, 0x1

    const/4 v14, 0x1

    const/4 v15, 0x1

    invoke-virtual/range {v2 .. v15}, Lcom/twitter/library/provider/az;->a(Ljava/util/Collection;JIJZZZLjava/lang/String;ZZZ)Ljava/util/Collection;

    :cond_0
    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/e;->a(Lcom/twitter/internal/network/HttpOperation;)V

    return-void

    :cond_1
    const/4 v12, 0x0

    goto :goto_0

    :cond_2
    const/4 v10, 0x0

    goto :goto_1

    :cond_3
    const/4 v9, 0x0

    goto :goto_2

    :cond_4
    const/4 v11, 0x0

    goto :goto_3
.end method

.method public b(J)Ljo;
    .locals 0

    iput-wide p1, p0, Ljo;->g:J

    return-object p0
.end method

.method public c(J)Ljo;
    .locals 0

    iput-wide p1, p0, Ljo;->n:J

    return-object p0
.end method
