.class public final Lh;
.super Lcom/fasterxml/jackson/core/a;
.source "Twttr"


# instance fields
.field protected final c:Lh;

.field protected d:I

.field protected e:I

.field protected f:Ljava/lang/String;

.field protected g:Lh;


# direct methods
.method public constructor <init>(Lh;III)V
    .locals 1

    invoke-direct {p0}, Lcom/fasterxml/jackson/core/a;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lh;->g:Lh;

    iput p2, p0, Lh;->a:I

    iput-object p1, p0, Lh;->c:Lh;

    iput p3, p0, Lh;->d:I

    iput p4, p0, Lh;->e:I

    const/4 v0, -0x1

    iput v0, p0, Lh;->b:I

    return-void
.end method

.method public static g()Lh;
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Lh;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {v0, v1, v3, v2, v3}, Lh;-><init>(Lh;III)V

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Lcom/fasterxml/jackson/core/JsonLocation;
    .locals 6

    const-wide/16 v2, -0x1

    new-instance v0, Lcom/fasterxml/jackson/core/JsonLocation;

    iget v4, p0, Lh;->d:I

    iget v5, p0, Lh;->e:I

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/fasterxml/jackson/core/JsonLocation;-><init>(Ljava/lang/Object;JII)V

    return-object v0
.end method

.method public a(II)Lh;
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lh;->g:Lh;

    if-nez v0, :cond_0

    new-instance v0, Lh;

    invoke-direct {v0, p0, v1, p1, p2}, Lh;-><init>(Lh;III)V

    iput-object v0, p0, Lh;->g:Lh;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0, v1, p1, p2}, Lh;->a(III)V

    goto :goto_0
.end method

.method protected a(III)V
    .locals 1

    iput p1, p0, Lh;->a:I

    const/4 v0, -0x1

    iput v0, p0, Lh;->b:I

    iput p2, p0, Lh;->d:I

    iput p3, p0, Lh;->e:I

    const/4 v0, 0x0

    iput-object v0, p0, Lh;->f:Ljava/lang/String;

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lh;->f:Ljava/lang/String;

    return-void
.end method

.method public b(II)Lh;
    .locals 2

    const/4 v1, 0x2

    iget-object v0, p0, Lh;->g:Lh;

    if-nez v0, :cond_0

    new-instance v0, Lh;

    invoke-direct {v0, p0, v1, p1, p2}, Lh;-><init>(Lh;III)V

    iput-object v0, p0, Lh;->g:Lh;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0, v1, p1, p2}, Lh;->a(III)V

    goto :goto_0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lh;->f:Ljava/lang/String;

    return-object v0
.end method

.method public i()Lh;
    .locals 1

    iget-object v0, p0, Lh;->c:Lh;

    return-object v0
.end method

.method public j()Z
    .locals 2

    iget v0, p0, Lh;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lh;->b:I

    iget v1, p0, Lh;->a:I

    if-eqz v1, :cond_0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    const/16 v2, 0x22

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    iget v1, p0, Lh;->a:I

    packed-switch v1, :pswitch_data_0

    :goto_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :pswitch_0
    const-string/jumbo v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :pswitch_1
    const/16 v1, 0x5b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lh;->f()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    :pswitch_2
    const/16 v1, 0x7b

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lh;->f:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lh;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/fasterxml/jackson/core/io/b;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :goto_1
    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    const/16 v1, 0x3f

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
