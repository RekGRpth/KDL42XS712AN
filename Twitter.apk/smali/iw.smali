.class public Liw;
.super Lcom/twitter/library/service/o;
.source "Twttr"


# instance fields
.field private final d:I

.field private final e:J

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;IJ)V
    .locals 1

    const-class v0, Liw;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    iput p3, p0, Liw;->d:I

    iput-wide p4, p0, Liw;->e:J

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Liw;
    .locals 0

    iput-object p1, p0, Liw;->f:Ljava/lang/String;

    return-object p0
.end method

.method protected b(Lcom/twitter/library/service/e;)V
    .locals 6

    iget v0, p0, Liw;->d:I

    iget-wide v1, p0, Liw;->e:J

    invoke-virtual {p0}, Liw;->w()Lcom/twitter/library/provider/az;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [J

    const/4 v5, 0x0

    aput-wide v1, v4, v5

    invoke-virtual {v3, v0, v4}, Lcom/twitter/library/provider/az;->a(I[J)I

    iget-object v0, p0, Liw;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v0, Lix;

    iget-object v1, p0, Liw;->l:Landroid/content/Context;

    invoke-virtual {p0}, Liw;->s()Lcom/twitter/library/service/p;

    move-result-object v2

    sget-object v3, Lcom/twitter/library/api/PromotedEvent;->i:Lcom/twitter/library/api/PromotedEvent;

    invoke-direct {v0, v1, v2, v3}, Lix;-><init>(Landroid/content/Context;Lcom/twitter/library/service/p;Lcom/twitter/library/api/PromotedEvent;)V

    iget-object v1, p0, Liw;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lix;->b(Ljava/lang/String;)Lix;

    move-result-object v0

    invoke-virtual {p0, v0}, Liw;->b(Lcom/twitter/internal/android/service/a;)V

    :cond_0
    return-void
.end method
