.class public final Lfr;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Lga;)Lfg;
    .locals 1

    new-instance v0, Lfu;

    invoke-direct {v0, p0}, Lfu;-><init>(Lga;)V

    return-object v0
.end method

.method public static a(Lgb;)Lfh;
    .locals 1

    new-instance v0, Lfw;

    invoke-direct {v0, p0}, Lfw;-><init>(Lgb;)V

    return-object v0
.end method

.method public static a(Ljava/io/OutputStream;)Lga;
    .locals 1

    new-instance v0, Lfs;

    invoke-direct {v0, p0}, Lfs;-><init>(Ljava/io/OutputStream;)V

    return-object v0
.end method

.method public static a(Ljava/io/InputStream;)Lgb;
    .locals 1

    new-instance v0, Lft;

    invoke-direct {v0, p0}, Lft;-><init>(Ljava/io/InputStream;)V

    return-object v0
.end method

.method public static a(Lfo;JJLjava/io/OutputStream;)V
    .locals 8

    const-wide/16 v6, 0x0

    iget-wide v0, p0, Lfo;->b:J

    move-wide v2, p1

    move-wide v4, p3

    invoke-static/range {v0 .. v5}, Lgc;->a(JJJ)V

    iget-object v0, p0, Lfo;->a:Lfy;

    :goto_0
    iget v1, v0, Lfy;->c:I

    iget v2, v0, Lfy;->b:I

    sub-int/2addr v1, v2

    int-to-long v1, v1

    cmp-long v1, p1, v1

    if-ltz v1, :cond_0

    iget v1, v0, Lfy;->c:I

    iget v2, v0, Lfy;->b:I

    sub-int/2addr v1, v2

    int-to-long v1, v1

    sub-long/2addr p1, v1

    iget-object v0, v0, Lfy;->d:Lfy;

    goto :goto_0

    :cond_0
    :goto_1
    cmp-long v1, p3, v6

    if-lez v1, :cond_1

    iget v1, v0, Lfy;->b:I

    int-to-long v1, v1

    add-long/2addr v1, p1

    long-to-int v1, v1

    iget v2, v0, Lfy;->c:I

    sub-int/2addr v2, v1

    int-to-long v2, v2

    invoke-static {v2, v3, p3, p4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-int v2, v2

    iget-object v3, v0, Lfy;->a:[B

    invoke-virtual {p5, v3, v1, v2}, Ljava/io/OutputStream;->write([BII)V

    int-to-long v1, v2

    sub-long/2addr p3, v1

    move-wide p1, v6

    goto :goto_1

    :cond_1
    return-void
.end method
