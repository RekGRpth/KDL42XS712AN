.class Lfx;
.super Ljava/io/InputStream;
.source "Twttr"


# instance fields
.field final synthetic a:Lfw;


# direct methods
.method constructor <init>(Lfw;)V
    .locals 0

    iput-object p1, p0, Lfx;->a:Lfw;

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    iget-object v0, p0, Lfx;->a:Lfw;

    invoke-static {v0}, Lfw;->a(Lfw;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method public available()I
    .locals 4

    invoke-direct {p0}, Lfx;->a()V

    iget-object v0, p0, Lfx;->a:Lfw;

    iget-object v0, v0, Lfw;->a:Lfo;

    iget-wide v0, v0, Lfo;->b:J

    const-wide/32 v2, 0x7fffffff

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public close()V
    .locals 1

    iget-object v0, p0, Lfx;->a:Lfw;

    invoke-virtual {v0}, Lfw;->close()V

    return-void
.end method

.method public read()I
    .locals 4

    invoke-direct {p0}, Lfx;->a()V

    iget-object v0, p0, Lfx;->a:Lfw;

    iget-object v0, v0, Lfw;->a:Lfo;

    iget-wide v0, v0, Lfo;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lfx;->a:Lfw;

    iget-object v0, v0, Lfw;->b:Lgb;

    iget-object v1, p0, Lfx;->a:Lfw;

    iget-object v1, v1, Lfw;->a:Lfo;

    const-wide/16 v2, 0x800

    invoke-interface {v0, v1, v2, v3}, Lgb;->b(Lfo;J)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lfx;->a:Lfw;

    iget-object v0, v0, Lfw;->a:Lfo;

    invoke-virtual {v0}, Lfo;->f()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    goto :goto_0
.end method

.method public read([BII)I
    .locals 6

    invoke-direct {p0}, Lfx;->a()V

    array-length v0, p1

    int-to-long v0, v0

    int-to-long v2, p2

    int-to-long v4, p3

    invoke-static/range {v0 .. v5}, Lgc;->a(JJJ)V

    iget-object v0, p0, Lfx;->a:Lfw;

    iget-object v0, v0, Lfw;->a:Lfo;

    iget-wide v0, v0, Lfo;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lfx;->a:Lfw;

    iget-object v0, v0, Lfw;->b:Lgb;

    iget-object v1, p0, Lfx;->a:Lfw;

    iget-object v1, v1, Lfw;->a:Lfo;

    const-wide/16 v2, 0x800

    invoke-interface {v0, v1, v2, v3}, Lgb;->b(Lfo;J)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lfx;->a:Lfw;

    iget-object v0, v0, Lfw;->a:Lfo;

    invoke-virtual {v0, p1, p2, p3}, Lfo;->b([BII)I

    move-result v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lfx;->a:Lfw;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".inputStream()"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
