.class public final Lim;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final AdsAttrs:[I

.field public static final AdsAttrs_adSize:I = 0x0

.field public static final AdsAttrs_adUnitId:I = 0x1

.field public static final BadgeIndicator:[I

.field public static final BadgeIndicator_badgeMode:I = 0x7

.field public static final BadgeIndicator_indicatorDrawable:I = 0x0

.field public static final BadgeIndicator_indicatorMarginBottom:I = 0x1

.field public static final BadgeIndicator_numberBackground:I = 0x2

.field public static final BadgeIndicator_numberColor:I = 0x3

.field public static final BadgeIndicator_numberMinHeight:I = 0x6

.field public static final BadgeIndicator_numberMinWidth:I = 0x5

.field public static final BadgeIndicator_numberTextSize:I = 0x4

.field public static final CardView:[I

.field public static final CardView_elementPressedColor:I = 0x0

.field public static final CardView_imagePlaceholderColor:I = 0x1

.field public static final CardView_playerOverlay:I = 0x2

.field public static final ClipRowView:[I

.field public static final ClipRowView_insetDividerColor:I = 0x4

.field public static final ClipRowView_insetDividerHeight:I = 0x3

.field public static final ClipRowView_insetHeight:I = 0x2

.field public static final ClipRowView_insetPaddingStart:I = 0x0

.field public static final ClipRowView_insetWidth:I = 0x1

.field public static final ComposerBar:[I

.field public static final ComposerBar_Layout:[I

.field public static final ComposerBar_Layout_layout_position:I = 0x1

.field public static final ComposerBar_Layout_pinned:I = 0x0

.field public static final ComposerBar_animation_duration:I = 0x1

.field public static final ComposerBar_delay_duration:I = 0x0

.field public static final CroppableImageView:[I

.field public static final CroppableImageView_cropRectPadding:I = 0x1

.field public static final CroppableImageView_cropRectStrokeColor:I = 0x2

.field public static final CroppableImageView_cropRectStrokeWidth:I = 0x3

.field public static final CroppableImageView_cropShadowColor:I = 0x4

.field public static final CroppableImageView_draggableCorners:I = 0x5

.field public static final CroppableImageView_gridColor:I = 0x7

.field public static final CroppableImageView_showGrid:I = 0x6

.field public static final CroppableImageView_toolbarMargin:I = 0x0

.field public static final DismissableOverlayImageView:[I

.field public static final DismissableOverlayImageView_dismissOverlayDrawable:I = 0x0

.field public static final DockLayout:[I

.field public static final DockLayout_autoUnlock:I = 0x6

.field public static final DockLayout_bottomDockId:I = 0x1

.field public static final DockLayout_bottomPeek:I = 0x4

.field public static final DockLayout_scrollDrive:I = 0x5

.field public static final DockLayout_topDockId:I = 0x0

.field public static final DockLayout_topPeek:I = 0x3

.field public static final DockLayout_turtle:I = 0x2

.field public static final DragSortListView:[I

.field public static final DragSortListView_click_remove_id:I = 0x10

.field public static final DragSortListView_collapsed_height:I = 0x0

.field public static final DragSortListView_drag_enabled:I = 0xa

.field public static final DragSortListView_drag_handle_id:I = 0xe

.field public static final DragSortListView_drag_scroll_start:I = 0x1

.field public static final DragSortListView_drag_start_mode:I = 0xd

.field public static final DragSortListView_drop_animation_duration:I = 0x9

.field public static final DragSortListView_fling_handle_id:I = 0xf

.field public static final DragSortListView_float_alpha:I = 0x6

.field public static final DragSortListView_float_background_color:I = 0x3

.field public static final DragSortListView_max_drag_scroll_speed:I = 0x2

.field public static final DragSortListView_remove_animation_duration:I = 0x8

.field public static final DragSortListView_remove_enabled:I = 0xc

.field public static final DragSortListView_remove_mode:I = 0x4

.field public static final DragSortListView_slide_shuffle_speed:I = 0x7

.field public static final DragSortListView_sort_enabled:I = 0xb

.field public static final DragSortListView_track_drag_sort:I = 0x5

.field public static final DragSortListView_use_default_controller:I = 0x11

.field public static final GroupedRowView:[I

.field public static final GroupedRowView_borderColor:I = 0x4

.field public static final GroupedRowView_borderHeight:I = 0x5

.field public static final GroupedRowView_cardStyle:I = 0x0

.field public static final GroupedRowView_fillColor:I = 0x3

.field public static final GroupedRowView_gapSize:I = 0x6

.field public static final GroupedRowView_groupStyle:I = 0x1

.field public static final GroupedRowView_single:I = 0x2

.field public static final HiddenDrawerLayout:[I

.field public static final HiddenDrawerLayout_bgColorHint:I = 0x6

.field public static final HiddenDrawerLayout_closeAnimDuration:I = 0x3

.field public static final HiddenDrawerLayout_closeInterpolator:I = 0x1

.field public static final HiddenDrawerLayout_draggable:I = 0x9

.field public static final HiddenDrawerLayout_draggableEdgeSize:I = 0x8

.field public static final HiddenDrawerLayout_drawerDirection:I = 0x7

.field public static final HiddenDrawerLayout_gutterColor:I = 0x5

.field public static final HiddenDrawerLayout_gutterSize:I = 0x4

.field public static final HiddenDrawerLayout_openAnimDuration:I = 0x2

.field public static final HiddenDrawerLayout_openInterpolator:I = 0x0

.field public static final HorizontalListView:[I

.field public static final HorizontalListView_dividerWidth:I = 0x1

.field public static final HorizontalListView_edgePadding:I = 0x2

.field public static final HorizontalListView_fillMode:I = 0x7

.field public static final HorizontalListView_fillWidthHeightRatio:I = 0x3

.field public static final HorizontalListView_leftFadeInDrawable:I = 0x8

.field public static final HorizontalListView_listDivider:I = 0x0

.field public static final HorizontalListView_rightFadeInDrawable:I = 0x9

.field public static final HorizontalListView_scrollDrawable:I = 0x5

.field public static final HorizontalListView_scrollHeight:I = 0x6

.field public static final HorizontalListView_scrollOffset:I = 0x4

.field public static final MapAttrs:[I

.field public static final MapAttrs_cameraBearing:I = 0x1

.field public static final MapAttrs_cameraTargetLat:I = 0x2

.field public static final MapAttrs_cameraTargetLng:I = 0x3

.field public static final MapAttrs_cameraTilt:I = 0x4

.field public static final MapAttrs_cameraZoom:I = 0x5

.field public static final MapAttrs_mapType:I = 0x0

.field public static final MapAttrs_uiCompass:I = 0x6

.field public static final MapAttrs_uiRotateGestures:I = 0x7

.field public static final MapAttrs_uiScrollGestures:I = 0x8

.field public static final MapAttrs_uiTiltGestures:I = 0x9

.field public static final MapAttrs_uiZoomControls:I = 0xa

.field public static final MapAttrs_uiZoomGestures:I = 0xb

.field public static final MapAttrs_useViewLifecycle:I = 0xc

.field public static final MapAttrs_zOrderOnTop:I = 0xd

.field public static final NavItemView:[I

.field public static final NavItemView_badgeIndicatorStyle:I = 0x0

.field public static final NavItemView_displayMode:I = 0x5

.field public static final NavItemView_selectedTextStyle:I = 0x1

.field public static final NavItemView_textColor:I = 0x2

.field public static final NavItemView_textSize:I = 0x3

.field public static final NavItemView_textStyle:I = 0x4

.field public static final NotchView:[I

.field public static final NotchView_notchBg:I = 0x1

.field public static final NotchView_notchDrawable:I = 0x0

.field public static final NotchView_notchLeftOffset:I = 0x2

.field public static final OverlayImageView:[I

.field public static final OverlayImageView_overlayDrawable:I = 0x0

.field public static final PageableListView:[I

.field public static final PageableListView_defaultPosition:I = 0x3

.field public static final PageableListView_loadingFooterLayout:I = 0x2

.field public static final PageableListView_loadingHeaderDivider:I = 0x1

.field public static final PageableListView_loadingHeaderLayout:I = 0x0

.field public static final PopupEditText:[I

.field public static final PopupEditText_popupMenuXOffset:I = 0x0

.field public static final PopupEditText_popupMenuYOffset:I = 0x1

.field public static final PopupEditText_showAsDropdown:I = 0x2

.field public static final PopupEditText_showFullScreen:I = 0x3

.field public static final PopupEditText_showPopupOnInitialFocus:I = 0x5

.field public static final PopupEditText_threshold:I = 0x4

.field public static final RoundedGroupedRowView:[I

.field public static final RoundedGroupedRowView_cardStyle:I = 0x0

.field public static final RoundedGroupedRowView_cornerRadius:I = 0xd

.field public static final RoundedGroupedRowView_dividerColor:I = 0x11

.field public static final RoundedGroupedRowView_dividerHeight:I = 0x10

.field public static final RoundedGroupedRowView_groupStyle:I = 0x1

.field public static final RoundedGroupedRowView_inset:I = 0x7

.field public static final RoundedGroupedRowView_insetBottom:I = 0xb

.field public static final RoundedGroupedRowView_insetBottomFillColor:I = 0xc

.field public static final RoundedGroupedRowView_insetLeft:I = 0x8

.field public static final RoundedGroupedRowView_insetRight:I = 0xa

.field public static final RoundedGroupedRowView_insetTop:I = 0x9

.field public static final RoundedGroupedRowView_shadowColor:I = 0x2

.field public static final RoundedGroupedRowView_shadowDx:I = 0x3

.field public static final RoundedGroupedRowView_shadowDy:I = 0x4

.field public static final RoundedGroupedRowView_shadowRadius:I = 0x5

.field public static final RoundedGroupedRowView_single:I = 0x6

.field public static final RoundedGroupedRowView_strokeColor:I = 0xf

.field public static final RoundedGroupedRowView_strokeWidth:I = 0xe

.field public static final SearchQueryView:[I

.field public static final SearchQueryView_clearDrawablePosition:I = 0x0

.field public static final SegmentedControl:[I

.field public static final SegmentedControl_segmentDivider:I = 0x7

.field public static final SegmentedControl_segmentLabels:I = 0x8

.field public static final SegmentedControl_shadowColor:I = 0x0

.field public static final SegmentedControl_shadowDx:I = 0x1

.field public static final SegmentedControl_shadowDy:I = 0x2

.field public static final SegmentedControl_shadowRadius:I = 0x3

.field public static final SegmentedControl_src:I = 0x6

.field public static final SegmentedControl_textColor:I = 0x4

.field public static final SegmentedControl_textSize:I = 0x5

.field public static final ShadowTextView:[I

.field public static final ShadowTextView_shadowColor:I = 0x0

.field public static final ShadowTextView_shadowDx:I = 0x1

.field public static final ShadowTextView_shadowDy:I = 0x2

.field public static final ShadowTextView_shadowRadius:I = 0x3

.field public static final SlidingPanel:[I

.field public static final SlidingPanel_panelContentLayoutId:I = 0x1

.field public static final SlidingPanel_panelHeaderLayoutId:I = 0x0

.field public static final SocialBylineView:[I

.field public static final SocialBylineView_iconMargin:I = 0x0

.field public static final SocialBylineView_labelColor:I = 0x3

.field public static final SocialBylineView_labelSize:I = 0x2

.field public static final SocialBylineView_minIconWidth:I = 0x1

.field public static final StyleableRadioButton:[I

.field public static final StyleableRadioButton_checkedStyle:I = 0x1

.field public static final StyleableRadioButton_normalStyle:I = 0x0

.field public static final ToolBar:[I

.field public static final ToolBarHomeView:[I

.field public static final ToolBarHomeView_allCaps:I = 0x7

.field public static final ToolBarHomeView_numberBackground:I = 0x3

.field public static final ToolBarHomeView_numberColor:I = 0x4

.field public static final ToolBarHomeView_subtitleTextSize:I = 0x5

.field public static final ToolBarHomeView_textColor:I = 0x0

.field public static final ToolBarHomeView_textSize:I = 0x1

.field public static final ToolBarHomeView_textStyle:I = 0x2

.field public static final ToolBarHomeView_upIndicatorDescription:I = 0x6

.field public static final ToolBarItem:[I

.field public static final ToolBarItemView:[I

.field public static final ToolBarItemView_badgeIndicatorStyle:I = 0x0

.field public static final ToolBarItemView_textColor:I = 0x1

.field public static final ToolBarItemView_textSize:I = 0x2

.field public static final ToolBarItem_actionLayout:I = 0xb

.field public static final ToolBarItem_android_contentDescription:I = 0x5

.field public static final ToolBarItem_android_enabled:I = 0x1

.field public static final ToolBarItem_android_icon:I = 0x0

.field public static final ToolBarItem_android_id:I = 0x2

.field public static final ToolBarItem_android_title:I = 0x4

.field public static final ToolBarItem_android_visible:I = 0x3

.field public static final ToolBarItem_order:I = 0x6

.field public static final ToolBarItem_overflowIcon:I = 0x9

.field public static final ToolBarItem_primaryItem:I = 0xa

.field public static final ToolBarItem_priority:I = 0x7

.field public static final ToolBarItem_showAsAction:I = 0xc

.field public static final ToolBarItem_subtitle:I = 0x8

.field public static final ToolBarLayout:[I

.field public static final ToolBarLayout_android_layout_gravity:I = 0x0

.field public static final ToolBar_popupMenuXOffset:I = 0x0

.field public static final ToolBar_popupMenuYOffset:I = 0x1

.field public static final ToolBar_stackFromRight:I = 0xd

.field public static final ToolBar_toolBarCustomViewId:I = 0xa

.field public static final ToolBar_toolBarDisplayOptions:I = 0xc

.field public static final ToolBar_toolBarHomeStyle:I = 0x2

.field public static final ToolBar_toolBarIcon:I = 0x7

.field public static final ToolBar_toolBarItemBackground:I = 0x4

.field public static final ToolBar_toolBarItemPadding:I = 0x5

.field public static final ToolBar_toolBarItemStyle:I = 0x3

.field public static final ToolBar_toolBarOverflowContentDescription:I = 0xb

.field public static final ToolBar_toolBarOverflowDrawable:I = 0x9

.field public static final ToolBar_toolBarTitle:I = 0x6

.field public static final ToolBar_toolBarUpIndicator:I = 0x8

.field public static final TouchInterceptor:[I

.field public static final TouchInterceptor_dragBackground:I = 0x2

.field public static final TouchInterceptor_expandedItemHeight:I = 0x1

.field public static final TouchInterceptor_normalItemHeight:I = 0x0

.field public static final TweetMediaImagesView:[I

.field public static final TweetMediaImagesView_divider_size:I = 0x0

.field public static final TweetView:[I

.field public static final TweetView_actionPromptBackgroundColorBlue:I = 0x74

.field public static final TweetView_actionPromptBackgroundColorGray:I = 0x75

.field public static final TweetView_actionPromptBodyColorGray:I = 0x72

.field public static final TweetView_actionPromptBodyColorWhite:I = 0x73

.field public static final TweetView_actionPromptBorderColor:I = 0x77

.field public static final TweetView_actionPromptBorderOffset:I = 0x69

.field public static final TweetView_actionPromptBorderWidth:I = 0x68

.field public static final TweetView_actionPromptContentBottomPadding:I = 0x6b

.field public static final TweetView_actionPromptContentLeftPadding:I = 0x6c

.field public static final TweetView_actionPromptContentRightPadding:I = 0x6d

.field public static final TweetView_actionPromptContentTopPadding:I = 0x6a

.field public static final TweetView_actionPromptDismissGray:I = 0x78

.field public static final TweetView_actionPromptDismissMargin:I = 0x7a

.field public static final TweetView_actionPromptDismissWhite:I = 0x79

.field public static final TweetView_actionPromptFavoriteIcon:I = 0x7b

.field public static final TweetView_actionPromptMarginTop:I = 0x67

.field public static final TweetView_actionPromptPointerHeight:I = 0x6e

.field public static final TweetView_actionPromptPressedBackgroundColor:I = 0x76

.field public static final TweetView_actionPromptReplyIcon:I = 0x7c

.field public static final TweetView_actionPromptTitleColorGray:I = 0x6f

.field public static final TweetView_actionPromptTitleColorWhite:I = 0x70

.field public static final TweetView_actionPromptTitleMarginBottom:I = 0x71

.field public static final TweetView_alertDrawable:I = 0x0

.field public static final TweetView_appDownloadImageHeight:I = 0x41

.field public static final TweetView_appDownloadImageWidth:I = 0x40

.field public static final TweetView_attributionColor:I = 0xe

.field public static final TweetView_attributionTextSize:I = 0xf

.field public static final TweetView_autoLink:I = 0x5f

.field public static final TweetView_badgeSpacing:I = 0x36

.field public static final TweetView_bylineColor:I = 0xb

.field public static final TweetView_bylineSize:I = 0x1

.field public static final TweetView_contentColor:I = 0x8

.field public static final TweetView_contentSize:I = 0x2

.field public static final TweetView_dismissDrawable:I = 0x12

.field public static final TweetView_doubleTapFavoriteAnimation:I = 0x59

.field public static final TweetView_doubleTapFavoriteDrawable:I = 0x58

.field public static final TweetView_iconSpacing:I = 0x10

.field public static final TweetView_inlineActionCountMarginRight:I = 0x29

.field public static final TweetView_inlineActionFavoriteOffDrawable:I = 0x19

.field public static final TweetView_inlineActionFavoriteOnDrawable:I = 0x18

.field public static final TweetView_inlineActionFollowOffDrawable:I = 0x1f

.field public static final TweetView_inlineActionFollowOnDrawable:I = 0x1e

.field public static final TweetView_inlineActionLabelFavoriteColor:I = 0x2a

.field public static final TweetView_inlineActionLabelMarginLeft:I = 0x27

.field public static final TweetView_inlineActionLabelMarginRight:I = 0x28

.field public static final TweetView_inlineActionLabelRetweetColor:I = 0x2b

.field public static final TweetView_inlineActionMarginBottom:I = 0x24

.field public static final TweetView_inlineActionMarginMediaBottom:I = 0x25

.field public static final TweetView_inlineActionMarginMediaTop:I = 0x23

.field public static final TweetView_inlineActionMarginRight:I = 0x26

.field public static final TweetView_inlineActionMarginTop:I = 0x22

.field public static final TweetView_inlineActionPAcFollowOffDrawable:I = 0x20

.field public static final TweetView_inlineActionPAcFollowOnDrawable:I = 0x21

.field public static final TweetView_inlineActionReplyOffDrawable:I = 0x1d

.field public static final TweetView_inlineActionRetweetDisabledDrawable:I = 0x1c

.field public static final TweetView_inlineActionRetweetOffDrawable:I = 0x1b

.field public static final TweetView_inlineActionRetweetOnDrawable:I = 0x1a

.field public static final TweetView_lineSpacingExtra:I = 0x7

.field public static final TweetView_lineSpacingMultiplier:I = 0x6

.field public static final TweetView_linkColor:I = 0x60

.field public static final TweetView_linkSelectedColor:I = 0x61

.field public static final TweetView_mediaBottomMargin:I = 0x5b

.field public static final TweetView_mediaColor:I = 0x38

.field public static final TweetView_mediaDivider:I = 0x5e

.field public static final TweetView_mediaGradientDrawable:I = 0x37

.field public static final TweetView_mediaIcon:I = 0x14

.field public static final TweetView_mediaPlaceholderDrawable:I = 0x55

.field public static final TweetView_mediaRetweetDrawable:I = 0x39

.field public static final TweetView_mediaTagBottomMargin:I = 0x5d

.field public static final TweetView_mediaTagIcon:I = 0x65

.field public static final TweetView_mediaTagSummaryColor:I = 0x64

.field public static final TweetView_mediaTagSummarySize:I = 0x63

.field public static final TweetView_mediaTagTopMargin:I = 0x5c

.field public static final TweetView_mediaTopMargin:I = 0x5a

.field public static final TweetView_photoErrorOverlayDrawable:I = 0x57

.field public static final TweetView_pivotDividerColor:I = 0x49

.field public static final TweetView_pivotDividerThickness:I = 0x4a

.field public static final TweetView_pivotTextColor:I = 0x4b

.field public static final TweetView_placeIcon:I = 0x66

.field public static final TweetView_placeholderDrawable:I = 0x11

.field public static final TweetView_playerErrorOverlayDrawable:I = 0x56

.field public static final TweetView_playerIcon:I = 0x15

.field public static final TweetView_playerOverlay:I = 0x3

.field public static final TweetView_politicalDrawable:I = 0x4

.field public static final TweetView_previewFlags:I = 0x62

.field public static final TweetView_profileImageHeight:I = 0x32

.field public static final TweetView_profileImageMarginLeft:I = 0x2d

.field public static final TweetView_profileImageMarginRight:I = 0x2f

.field public static final TweetView_profileImageMarginTop:I = 0x2e

.field public static final TweetView_profileImageOverlayDrawable:I = 0x30

.field public static final TweetView_profileImageWidth:I = 0x31

.field public static final TweetView_promoBGColor:I = 0x48

.field public static final TweetView_promoCTCColor:I = 0x44

.field public static final TweetView_promoDividerColor:I = 0x45

.field public static final TweetView_promoMarginBottom:I = 0x43

.field public static final TweetView_promoMarginTop:I = 0x42

.field public static final TweetView_promoPadding:I = 0x46

.field public static final TweetView_promotedDrawable:I = 0x5

.field public static final TweetView_screenNameColor:I = 0xd

.field public static final TweetView_socialProofCollection:I = 0x54

.field public static final TweetView_socialProofConvoReplyDrawable:I = 0x4f

.field public static final TweetView_socialProofFavDrawable:I = 0x4d

.field public static final TweetView_socialProofFollowDrawable:I = 0x50

.field public static final TweetView_socialProofNearbyDrawable:I = 0x51

.field public static final TweetView_socialProofPopularDrawable:I = 0x52

.field public static final TweetView_socialProofRecommendation:I = 0x53

.field public static final TweetView_socialProofReplyDrawable:I = 0x4e

.field public static final TweetView_socialProofRetweetDrawable:I = 0x4c

.field public static final TweetView_statsSpacing:I = 0x2c

.field public static final TweetView_summaryBgColor:I = 0x47

.field public static final TweetView_summaryContentColor:I = 0x9

.field public static final TweetView_summaryIcon:I = 0x16

.field public static final TweetView_summaryImageHeight:I = 0x3c

.field public static final TweetView_summaryImageWidth:I = 0x3b

.field public static final TweetView_summaryPadding:I = 0x3a

.field public static final TweetView_summaryPreviewMarginBottom:I = 0x3f

.field public static final TweetView_summaryPreviewMarginTop:I = 0x3e

.field public static final TweetView_summaryTextSize:I = 0xa

.field public static final TweetView_summaryUserImageSize:I = 0x3d

.field public static final TweetView_timestampColor:I = 0xc

.field public static final TweetView_topPillDrawable:I = 0x13

.field public static final TweetView_translationIcon:I = 0x17

.field public static final TweetView_verticalConnector:I = 0x33

.field public static final TweetView_verticalConnectorMargin:I = 0x35

.field public static final TweetView_verticalConnectorWidth:I = 0x34

.field public static final UserSocialView:[I

.field public static final UserSocialView_bylineSize:I = 0x0

.field public static final UserSocialView_contentSize:I = 0x1

.field public static final UserView:[I

.field public static final UserView_actionButtonPadding:I = 0x3

.field public static final UserView_actionButtonPaddingBottom:I = 0x7

.field public static final UserView_actionButtonPaddingLeft:I = 0x4

.field public static final UserView_actionButtonPaddingRight:I = 0x6

.field public static final UserView_actionButtonPaddingTop:I = 0x5

.field public static final UserView_defaultProfileImageDrawable:I = 0x2

.field public static final UserView_politicalDrawable:I = 0x0

.field public static final UserView_profileTextColor:I = 0x8

.field public static final UserView_promotedDrawable:I = 0x1

.field public static final ViewPagerScrollBar:[I

.field public static final ViewPagerScrollBar_tabDrawable:I = 0x0

.field public static final ViewPagerScrollBar_tabMaxHeight:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x2

    new-array v0, v2, [I

    fill-array-data v0, :array_0

    sput-object v0, Lim;->AdsAttrs:[I

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lim;->BadgeIndicator:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_2

    sput-object v0, Lim;->CardView:[I

    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lim;->ClipRowView:[I

    new-array v0, v2, [I

    fill-array-data v0, :array_4

    sput-object v0, Lim;->ComposerBar:[I

    new-array v0, v2, [I

    fill-array-data v0, :array_5

    sput-object v0, Lim;->ComposerBar_Layout:[I

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_6

    sput-object v0, Lim;->CroppableImageView:[I

    new-array v0, v4, [I

    const v1, 0x7f01000c    # com.twitter.android.R.attr.dismissOverlayDrawable

    aput v1, v0, v3

    sput-object v0, Lim;->DismissableOverlayImageView:[I

    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_7

    sput-object v0, Lim;->DockLayout:[I

    const/16 v0, 0x12

    new-array v0, v0, [I

    fill-array-data v0, :array_8

    sput-object v0, Lim;->DragSortListView:[I

    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_9

    sput-object v0, Lim;->GroupedRowView:[I

    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_a

    sput-object v0, Lim;->HiddenDrawerLayout:[I

    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_b

    sput-object v0, Lim;->HorizontalListView:[I

    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_c

    sput-object v0, Lim;->MapAttrs:[I

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_d

    sput-object v0, Lim;->NavItemView:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_e

    sput-object v0, Lim;->NotchView:[I

    new-array v0, v4, [I

    const v1, 0x7f0100d3    # com.twitter.android.R.attr.overlayDrawable

    aput v1, v0, v3

    sput-object v0, Lim;->OverlayImageView:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_f

    sput-object v0, Lim;->PageableListView:[I

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_10

    sput-object v0, Lim;->PopupEditText:[I

    const/16 v0, 0x12

    new-array v0, v0, [I

    fill-array-data v0, :array_11

    sput-object v0, Lim;->RoundedGroupedRowView:[I

    new-array v0, v4, [I

    const v1, 0x7f010102    # com.twitter.android.R.attr.clearDrawablePosition

    aput v1, v0, v3

    sput-object v0, Lim;->SearchQueryView:[I

    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_12

    sput-object v0, Lim;->SegmentedControl:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_13

    sput-object v0, Lim;->ShadowTextView:[I

    new-array v0, v2, [I

    fill-array-data v0, :array_14

    sput-object v0, Lim;->SlidingPanel:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_15

    sput-object v0, Lim;->SocialBylineView:[I

    new-array v0, v2, [I

    fill-array-data v0, :array_16

    sput-object v0, Lim;->StyleableRadioButton:[I

    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_17

    sput-object v0, Lim;->ToolBar:[I

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_18

    sput-object v0, Lim;->ToolBarHomeView:[I

    const/16 v0, 0xd

    new-array v0, v0, [I

    fill-array-data v0, :array_19

    sput-object v0, Lim;->ToolBarItem:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_1a

    sput-object v0, Lim;->ToolBarItemView:[I

    new-array v0, v4, [I

    const v1, 0x10100b3    # android.R.attr.layout_gravity

    aput v1, v0, v3

    sput-object v0, Lim;->ToolBarLayout:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_1b

    sput-object v0, Lim;->TouchInterceptor:[I

    new-array v0, v4, [I

    const v1, 0x7f010139    # com.twitter.android.R.attr.divider_size

    aput v1, v0, v3

    sput-object v0, Lim;->TweetMediaImagesView:[I

    const/16 v0, 0x7d

    new-array v0, v0, [I

    fill-array-data v0, :array_1c

    sput-object v0, Lim;->TweetView:[I

    new-array v0, v2, [I

    fill-array-data v0, :array_1d

    sput-object v0, Lim;->UserSocialView:[I

    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_1e

    sput-object v0, Lim;->UserView:[I

    new-array v0, v2, [I

    fill-array-data v0, :array_1f

    sput-object v0, Lim;->ViewPagerScrollBar:[I

    return-void

    :array_0
    .array-data 4
        0x7f010063    # com.twitter.android.R.attr.adSize
        0x7f010064    # com.twitter.android.R.attr.adUnitId
    .end array-data

    :array_1
    .array-data 4
        0x7f010065    # com.twitter.android.R.attr.indicatorDrawable
        0x7f010066    # com.twitter.android.R.attr.indicatorMarginBottom
        0x7f010067    # com.twitter.android.R.attr.numberBackground
        0x7f010068    # com.twitter.android.R.attr.numberColor
        0x7f010069    # com.twitter.android.R.attr.numberTextSize
        0x7f01006a    # com.twitter.android.R.attr.numberMinWidth
        0x7f01006b    # com.twitter.android.R.attr.numberMinHeight
        0x7f01006c    # com.twitter.android.R.attr.badgeMode
    .end array-data

    :array_2
    .array-data 4
        0x7f010011    # com.twitter.android.R.attr.elementPressedColor
        0x7f010016    # com.twitter.android.R.attr.imagePlaceholderColor
        0x7f010024    # com.twitter.android.R.attr.playerOverlay
    .end array-data

    :array_3
    .array-data 4
        0x7f01006e    # com.twitter.android.R.attr.insetPaddingStart
        0x7f01006f    # com.twitter.android.R.attr.insetWidth
        0x7f010070    # com.twitter.android.R.attr.insetHeight
        0x7f010071    # com.twitter.android.R.attr.insetDividerHeight
        0x7f010072    # com.twitter.android.R.attr.insetDividerColor
    .end array-data

    :array_4
    .array-data 4
        0x7f010073    # com.twitter.android.R.attr.delay_duration
        0x7f010074    # com.twitter.android.R.attr.animation_duration
    .end array-data

    :array_5
    .array-data 4
        0x7f010075    # com.twitter.android.R.attr.pinned
        0x7f010076    # com.twitter.android.R.attr.layout_position
    .end array-data

    :array_6
    .array-data 4
        0x7f010079    # com.twitter.android.R.attr.toolbarMargin
        0x7f01007a    # com.twitter.android.R.attr.cropRectPadding
        0x7f01007b    # com.twitter.android.R.attr.cropRectStrokeColor
        0x7f01007c    # com.twitter.android.R.attr.cropRectStrokeWidth
        0x7f01007d    # com.twitter.android.R.attr.cropShadowColor
        0x7f01007e    # com.twitter.android.R.attr.draggableCorners
        0x7f01007f    # com.twitter.android.R.attr.showGrid
        0x7f010080    # com.twitter.android.R.attr.gridColor
    .end array-data

    :array_7
    .array-data 4
        0x7f010083    # com.twitter.android.R.attr.topDockId
        0x7f010084    # com.twitter.android.R.attr.bottomDockId
        0x7f010085    # com.twitter.android.R.attr.turtle
        0x7f010086    # com.twitter.android.R.attr.topPeek
        0x7f010087    # com.twitter.android.R.attr.bottomPeek
        0x7f010088    # com.twitter.android.R.attr.scrollDrive
        0x7f010089    # com.twitter.android.R.attr.autoUnlock
    .end array-data

    :array_8
    .array-data 4
        0x7f01008a    # com.twitter.android.R.attr.collapsed_height
        0x7f01008b    # com.twitter.android.R.attr.drag_scroll_start
        0x7f01008c    # com.twitter.android.R.attr.max_drag_scroll_speed
        0x7f01008d    # com.twitter.android.R.attr.float_background_color
        0x7f01008e    # com.twitter.android.R.attr.remove_mode
        0x7f01008f    # com.twitter.android.R.attr.track_drag_sort
        0x7f010090    # com.twitter.android.R.attr.float_alpha
        0x7f010091    # com.twitter.android.R.attr.slide_shuffle_speed
        0x7f010092    # com.twitter.android.R.attr.remove_animation_duration
        0x7f010093    # com.twitter.android.R.attr.drop_animation_duration
        0x7f010094    # com.twitter.android.R.attr.drag_enabled
        0x7f010095    # com.twitter.android.R.attr.sort_enabled
        0x7f010096    # com.twitter.android.R.attr.remove_enabled
        0x7f010097    # com.twitter.android.R.attr.drag_start_mode
        0x7f010098    # com.twitter.android.R.attr.drag_handle_id
        0x7f010099    # com.twitter.android.R.attr.fling_handle_id
        0x7f01009a    # com.twitter.android.R.attr.click_remove_id
        0x7f01009b    # com.twitter.android.R.attr.use_default_controller
    .end array-data

    :array_9
    .array-data 4
        0x7f010005    # com.twitter.android.R.attr.cardStyle
        0x7f010012    # com.twitter.android.R.attr.groupStyle
        0x7f010038    # com.twitter.android.R.attr.single
        0x7f0100a9    # com.twitter.android.R.attr.fillColor
        0x7f0100aa    # com.twitter.android.R.attr.borderColor
        0x7f0100ab    # com.twitter.android.R.attr.borderHeight
        0x7f0100ac    # com.twitter.android.R.attr.gapSize
    .end array-data

    :array_a
    .array-data 4
        0x7f0100ad    # com.twitter.android.R.attr.openInterpolator
        0x7f0100ae    # com.twitter.android.R.attr.closeInterpolator
        0x7f0100af    # com.twitter.android.R.attr.openAnimDuration
        0x7f0100b0    # com.twitter.android.R.attr.closeAnimDuration
        0x7f0100b1    # com.twitter.android.R.attr.gutterSize
        0x7f0100b2    # com.twitter.android.R.attr.gutterColor
        0x7f0100b3    # com.twitter.android.R.attr.bgColorHint
        0x7f0100b4    # com.twitter.android.R.attr.drawerDirection
        0x7f0100b5    # com.twitter.android.R.attr.draggableEdgeSize
        0x7f0100b6    # com.twitter.android.R.attr.draggable
    .end array-data

    :array_b
    .array-data 4
        0x7f0100b7    # com.twitter.android.R.attr.listDivider
        0x7f0100b8    # com.twitter.android.R.attr.dividerWidth
        0x7f0100b9    # com.twitter.android.R.attr.edgePadding
        0x7f0100ba    # com.twitter.android.R.attr.fillWidthHeightRatio
        0x7f0100bb    # com.twitter.android.R.attr.scrollOffset
        0x7f0100bc    # com.twitter.android.R.attr.scrollDrawable
        0x7f0100bd    # com.twitter.android.R.attr.scrollHeight
        0x7f0100be    # com.twitter.android.R.attr.fillMode
        0x7f0100bf    # com.twitter.android.R.attr.leftFadeInDrawable
        0x7f0100c0    # com.twitter.android.R.attr.rightFadeInDrawable
    .end array-data

    :array_c
    .array-data 4
        0x7f0100c1    # com.twitter.android.R.attr.mapType
        0x7f0100c2    # com.twitter.android.R.attr.cameraBearing
        0x7f0100c3    # com.twitter.android.R.attr.cameraTargetLat
        0x7f0100c4    # com.twitter.android.R.attr.cameraTargetLng
        0x7f0100c5    # com.twitter.android.R.attr.cameraTilt
        0x7f0100c6    # com.twitter.android.R.attr.cameraZoom
        0x7f0100c7    # com.twitter.android.R.attr.uiCompass
        0x7f0100c8    # com.twitter.android.R.attr.uiRotateGestures
        0x7f0100c9    # com.twitter.android.R.attr.uiScrollGestures
        0x7f0100ca    # com.twitter.android.R.attr.uiTiltGestures
        0x7f0100cb    # com.twitter.android.R.attr.uiZoomControls
        0x7f0100cc    # com.twitter.android.R.attr.uiZoomGestures
        0x7f0100cd    # com.twitter.android.R.attr.useViewLifecycle
        0x7f0100ce    # com.twitter.android.R.attr.zOrderOnTop
    .end array-data

    :array_d
    .array-data 4
        0x7f010003    # com.twitter.android.R.attr.badgeIndicatorStyle
        0x7f010032    # com.twitter.android.R.attr.selectedTextStyle
        0x7f01004f    # com.twitter.android.R.attr.textColor
        0x7f010050    # com.twitter.android.R.attr.textSize
        0x7f010051    # com.twitter.android.R.attr.textStyle
        0x7f0100cf    # com.twitter.android.R.attr.displayMode
    .end array-data

    :array_e
    .array-data 4
        0x7f0100d0    # com.twitter.android.R.attr.notchDrawable
        0x7f0100d1    # com.twitter.android.R.attr.notchBg
        0x7f0100d2    # com.twitter.android.R.attr.notchLeftOffset
    .end array-data

    :array_f
    .array-data 4
        0x7f0100d4    # com.twitter.android.R.attr.loadingHeaderLayout
        0x7f0100d5    # com.twitter.android.R.attr.loadingHeaderDivider
        0x7f0100d6    # com.twitter.android.R.attr.loadingFooterLayout
        0x7f0100d7    # com.twitter.android.R.attr.defaultPosition
    .end array-data

    :array_10
    .array-data 4
        0x7f010029    # com.twitter.android.R.attr.popupMenuXOffset
        0x7f01002a    # com.twitter.android.R.attr.popupMenuYOffset
        0x7f0100e6    # com.twitter.android.R.attr.showAsDropdown
        0x7f0100e7    # com.twitter.android.R.attr.showFullScreen
        0x7f0100e8    # com.twitter.android.R.attr.threshold
        0x7f0100e9    # com.twitter.android.R.attr.showPopupOnInitialFocus
    .end array-data

    :array_11
    .array-data 4
        0x7f010005    # com.twitter.android.R.attr.cardStyle
        0x7f010012    # com.twitter.android.R.attr.groupStyle
        0x7f010033    # com.twitter.android.R.attr.shadowColor
        0x7f010034    # com.twitter.android.R.attr.shadowDx
        0x7f010035    # com.twitter.android.R.attr.shadowDy
        0x7f010036    # com.twitter.android.R.attr.shadowRadius
        0x7f010038    # com.twitter.android.R.attr.single
        0x7f0100f7    # com.twitter.android.R.attr.inset
        0x7f0100f8    # com.twitter.android.R.attr.insetLeft
        0x7f0100f9    # com.twitter.android.R.attr.insetTop
        0x7f0100fa    # com.twitter.android.R.attr.insetRight
        0x7f0100fb    # com.twitter.android.R.attr.insetBottom
        0x7f0100fc    # com.twitter.android.R.attr.insetBottomFillColor
        0x7f0100fd    # com.twitter.android.R.attr.cornerRadius
        0x7f0100fe    # com.twitter.android.R.attr.strokeWidth
        0x7f0100ff    # com.twitter.android.R.attr.strokeColor
        0x7f010100    # com.twitter.android.R.attr.dividerHeight
        0x7f010101    # com.twitter.android.R.attr.dividerColor
    .end array-data

    :array_12
    .array-data 4
        0x7f010033    # com.twitter.android.R.attr.shadowColor
        0x7f010034    # com.twitter.android.R.attr.shadowDx
        0x7f010035    # com.twitter.android.R.attr.shadowDy
        0x7f010036    # com.twitter.android.R.attr.shadowRadius
        0x7f01004f    # com.twitter.android.R.attr.textColor
        0x7f010050    # com.twitter.android.R.attr.textSize
        0x7f010103    # com.twitter.android.R.attr.src
        0x7f010104    # com.twitter.android.R.attr.segmentDivider
        0x7f010105    # com.twitter.android.R.attr.segmentLabels
    .end array-data

    :array_13
    .array-data 4
        0x7f010033    # com.twitter.android.R.attr.shadowColor
        0x7f010034    # com.twitter.android.R.attr.shadowDx
        0x7f010035    # com.twitter.android.R.attr.shadowDy
        0x7f010036    # com.twitter.android.R.attr.shadowRadius
    .end array-data

    :array_14
    .array-data 4
        0x7f010106    # com.twitter.android.R.attr.panelHeaderLayoutId
        0x7f010107    # com.twitter.android.R.attr.panelContentLayoutId
    .end array-data

    :array_15
    .array-data 4
        0x7f010108    # com.twitter.android.R.attr.iconMargin
        0x7f010109    # com.twitter.android.R.attr.minIconWidth
        0x7f01010a    # com.twitter.android.R.attr.labelSize
        0x7f01010b    # com.twitter.android.R.attr.labelColor
    .end array-data

    :array_16
    .array-data 4
        0x7f01010e    # com.twitter.android.R.attr.normalStyle
        0x7f01010f    # com.twitter.android.R.attr.checkedStyle
    .end array-data

    :array_17
    .array-data 4
        0x7f010029    # com.twitter.android.R.attr.popupMenuXOffset
        0x7f01002a    # com.twitter.android.R.attr.popupMenuYOffset
        0x7f010052    # com.twitter.android.R.attr.toolBarHomeStyle
        0x7f010053    # com.twitter.android.R.attr.toolBarItemStyle
        0x7f010124    # com.twitter.android.R.attr.toolBarItemBackground
        0x7f010125    # com.twitter.android.R.attr.toolBarItemPadding
        0x7f010126    # com.twitter.android.R.attr.toolBarTitle
        0x7f010127    # com.twitter.android.R.attr.toolBarIcon
        0x7f010128    # com.twitter.android.R.attr.toolBarUpIndicator
        0x7f010129    # com.twitter.android.R.attr.toolBarOverflowDrawable
        0x7f01012a    # com.twitter.android.R.attr.toolBarCustomViewId
        0x7f01012b    # com.twitter.android.R.attr.toolBarOverflowContentDescription
        0x7f01012c    # com.twitter.android.R.attr.toolBarDisplayOptions
        0x7f01012d    # com.twitter.android.R.attr.stackFromRight
    .end array-data

    :array_18
    .array-data 4
        0x7f01004f    # com.twitter.android.R.attr.textColor
        0x7f010050    # com.twitter.android.R.attr.textSize
        0x7f010051    # com.twitter.android.R.attr.textStyle
        0x7f010067    # com.twitter.android.R.attr.numberBackground
        0x7f010068    # com.twitter.android.R.attr.numberColor
        0x7f01012e    # com.twitter.android.R.attr.subtitleTextSize
        0x7f01012f    # com.twitter.android.R.attr.upIndicatorDescription
        0x7f010130    # com.twitter.android.R.attr.allCaps
    .end array-data

    :array_19
    .array-data 4
        0x1010002    # android.R.attr.icon
        0x101000e    # android.R.attr.enabled
        0x10100d0    # android.R.attr.id
        0x1010194    # android.R.attr.visible
        0x10101e1    # android.R.attr.title
        0x1010273    # android.R.attr.contentDescription
        0x7f01001f    # com.twitter.android.R.attr.order
        0x7f01002b    # com.twitter.android.R.attr.priority
        0x7f010131    # com.twitter.android.R.attr.subtitle
        0x7f010132    # com.twitter.android.R.attr.overflowIcon
        0x7f010133    # com.twitter.android.R.attr.primaryItem
        0x7f010134    # com.twitter.android.R.attr.actionLayout
        0x7f010135    # com.twitter.android.R.attr.showAsAction
    .end array-data

    :array_1a
    .array-data 4
        0x7f010003    # com.twitter.android.R.attr.badgeIndicatorStyle
        0x7f01004f    # com.twitter.android.R.attr.textColor
        0x7f010050    # com.twitter.android.R.attr.textSize
    .end array-data

    :array_1b
    .array-data 4
        0x7f010136    # com.twitter.android.R.attr.normalItemHeight
        0x7f010137    # com.twitter.android.R.attr.expandedItemHeight
        0x7f010138    # com.twitter.android.R.attr.dragBackground
    .end array-data

    :array_1c
    .array-data 4
        0x7f010002    # com.twitter.android.R.attr.alertDrawable
        0x7f010004    # com.twitter.android.R.attr.bylineSize
        0x7f01000a    # com.twitter.android.R.attr.contentSize
        0x7f010024    # com.twitter.android.R.attr.playerOverlay
        0x7f010025    # com.twitter.android.R.attr.politicalDrawable
        0x7f01002c    # com.twitter.android.R.attr.promotedDrawable
        0x7f01013e    # com.twitter.android.R.attr.lineSpacingMultiplier
        0x7f01013f    # com.twitter.android.R.attr.lineSpacingExtra
        0x7f010140    # com.twitter.android.R.attr.contentColor
        0x7f010141    # com.twitter.android.R.attr.summaryContentColor
        0x7f010142    # com.twitter.android.R.attr.summaryTextSize
        0x7f010143    # com.twitter.android.R.attr.bylineColor
        0x7f010144    # com.twitter.android.R.attr.timestampColor
        0x7f010145    # com.twitter.android.R.attr.screenNameColor
        0x7f010146    # com.twitter.android.R.attr.attributionColor
        0x7f010147    # com.twitter.android.R.attr.attributionTextSize
        0x7f010148    # com.twitter.android.R.attr.iconSpacing
        0x7f010149    # com.twitter.android.R.attr.placeholderDrawable
        0x7f01014a    # com.twitter.android.R.attr.dismissDrawable
        0x7f01014b    # com.twitter.android.R.attr.topPillDrawable
        0x7f01014c    # com.twitter.android.R.attr.mediaIcon
        0x7f01014d    # com.twitter.android.R.attr.playerIcon
        0x7f01014e    # com.twitter.android.R.attr.summaryIcon
        0x7f01014f    # com.twitter.android.R.attr.translationIcon
        0x7f010150    # com.twitter.android.R.attr.inlineActionFavoriteOnDrawable
        0x7f010151    # com.twitter.android.R.attr.inlineActionFavoriteOffDrawable
        0x7f010152    # com.twitter.android.R.attr.inlineActionRetweetOnDrawable
        0x7f010153    # com.twitter.android.R.attr.inlineActionRetweetOffDrawable
        0x7f010154    # com.twitter.android.R.attr.inlineActionRetweetDisabledDrawable
        0x7f010155    # com.twitter.android.R.attr.inlineActionReplyOffDrawable
        0x7f010156    # com.twitter.android.R.attr.inlineActionFollowOnDrawable
        0x7f010157    # com.twitter.android.R.attr.inlineActionFollowOffDrawable
        0x7f010158    # com.twitter.android.R.attr.inlineActionPAcFollowOffDrawable
        0x7f010159    # com.twitter.android.R.attr.inlineActionPAcFollowOnDrawable
        0x7f01015a    # com.twitter.android.R.attr.inlineActionMarginTop
        0x7f01015b    # com.twitter.android.R.attr.inlineActionMarginMediaTop
        0x7f01015c    # com.twitter.android.R.attr.inlineActionMarginBottom
        0x7f01015d    # com.twitter.android.R.attr.inlineActionMarginMediaBottom
        0x7f01015e    # com.twitter.android.R.attr.inlineActionMarginRight
        0x7f01015f    # com.twitter.android.R.attr.inlineActionLabelMarginLeft
        0x7f010160    # com.twitter.android.R.attr.inlineActionLabelMarginRight
        0x7f010161    # com.twitter.android.R.attr.inlineActionCountMarginRight
        0x7f010162    # com.twitter.android.R.attr.inlineActionLabelFavoriteColor
        0x7f010163    # com.twitter.android.R.attr.inlineActionLabelRetweetColor
        0x7f010164    # com.twitter.android.R.attr.statsSpacing
        0x7f010165    # com.twitter.android.R.attr.profileImageMarginLeft
        0x7f010166    # com.twitter.android.R.attr.profileImageMarginTop
        0x7f010167    # com.twitter.android.R.attr.profileImageMarginRight
        0x7f010168    # com.twitter.android.R.attr.profileImageOverlayDrawable
        0x7f010169    # com.twitter.android.R.attr.profileImageWidth
        0x7f01016a    # com.twitter.android.R.attr.profileImageHeight
        0x7f01016b    # com.twitter.android.R.attr.verticalConnector
        0x7f01016c    # com.twitter.android.R.attr.verticalConnectorWidth
        0x7f01016d    # com.twitter.android.R.attr.verticalConnectorMargin
        0x7f01016e    # com.twitter.android.R.attr.badgeSpacing
        0x7f01016f    # com.twitter.android.R.attr.mediaGradientDrawable
        0x7f010170    # com.twitter.android.R.attr.mediaColor
        0x7f010171    # com.twitter.android.R.attr.mediaRetweetDrawable
        0x7f010172    # com.twitter.android.R.attr.summaryPadding
        0x7f010173    # com.twitter.android.R.attr.summaryImageWidth
        0x7f010174    # com.twitter.android.R.attr.summaryImageHeight
        0x7f010175    # com.twitter.android.R.attr.summaryUserImageSize
        0x7f010176    # com.twitter.android.R.attr.summaryPreviewMarginTop
        0x7f010177    # com.twitter.android.R.attr.summaryPreviewMarginBottom
        0x7f010178    # com.twitter.android.R.attr.appDownloadImageWidth
        0x7f010179    # com.twitter.android.R.attr.appDownloadImageHeight
        0x7f01017a    # com.twitter.android.R.attr.promoMarginTop
        0x7f01017b    # com.twitter.android.R.attr.promoMarginBottom
        0x7f01017c    # com.twitter.android.R.attr.promoCTCColor
        0x7f01017d    # com.twitter.android.R.attr.promoDividerColor
        0x7f01017e    # com.twitter.android.R.attr.promoPadding
        0x7f01017f    # com.twitter.android.R.attr.summaryBgColor
        0x7f010180    # com.twitter.android.R.attr.promoBGColor
        0x7f010181    # com.twitter.android.R.attr.pivotDividerColor
        0x7f010182    # com.twitter.android.R.attr.pivotDividerThickness
        0x7f010183    # com.twitter.android.R.attr.pivotTextColor
        0x7f010184    # com.twitter.android.R.attr.socialProofRetweetDrawable
        0x7f010185    # com.twitter.android.R.attr.socialProofFavDrawable
        0x7f010186    # com.twitter.android.R.attr.socialProofReplyDrawable
        0x7f010187    # com.twitter.android.R.attr.socialProofConvoReplyDrawable
        0x7f010188    # com.twitter.android.R.attr.socialProofFollowDrawable
        0x7f010189    # com.twitter.android.R.attr.socialProofNearbyDrawable
        0x7f01018a    # com.twitter.android.R.attr.socialProofPopularDrawable
        0x7f01018b    # com.twitter.android.R.attr.socialProofRecommendation
        0x7f01018c    # com.twitter.android.R.attr.socialProofCollection
        0x7f01018d    # com.twitter.android.R.attr.mediaPlaceholderDrawable
        0x7f01018e    # com.twitter.android.R.attr.playerErrorOverlayDrawable
        0x7f01018f    # com.twitter.android.R.attr.photoErrorOverlayDrawable
        0x7f010190    # com.twitter.android.R.attr.doubleTapFavoriteDrawable
        0x7f010191    # com.twitter.android.R.attr.doubleTapFavoriteAnimation
        0x7f010192    # com.twitter.android.R.attr.mediaTopMargin
        0x7f010193    # com.twitter.android.R.attr.mediaBottomMargin
        0x7f010194    # com.twitter.android.R.attr.mediaTagTopMargin
        0x7f010195    # com.twitter.android.R.attr.mediaTagBottomMargin
        0x7f010196    # com.twitter.android.R.attr.mediaDivider
        0x7f010197    # com.twitter.android.R.attr.autoLink
        0x7f010198    # com.twitter.android.R.attr.linkColor
        0x7f010199    # com.twitter.android.R.attr.linkSelectedColor
        0x7f01019a    # com.twitter.android.R.attr.previewFlags
        0x7f01019b    # com.twitter.android.R.attr.mediaTagSummarySize
        0x7f01019c    # com.twitter.android.R.attr.mediaTagSummaryColor
        0x7f01019d    # com.twitter.android.R.attr.mediaTagIcon
        0x7f01019e    # com.twitter.android.R.attr.placeIcon
        0x7f01019f    # com.twitter.android.R.attr.actionPromptMarginTop
        0x7f0101a0    # com.twitter.android.R.attr.actionPromptBorderWidth
        0x7f0101a1    # com.twitter.android.R.attr.actionPromptBorderOffset
        0x7f0101a2    # com.twitter.android.R.attr.actionPromptContentTopPadding
        0x7f0101a3    # com.twitter.android.R.attr.actionPromptContentBottomPadding
        0x7f0101a4    # com.twitter.android.R.attr.actionPromptContentLeftPadding
        0x7f0101a5    # com.twitter.android.R.attr.actionPromptContentRightPadding
        0x7f0101a6    # com.twitter.android.R.attr.actionPromptPointerHeight
        0x7f0101a7    # com.twitter.android.R.attr.actionPromptTitleColorGray
        0x7f0101a8    # com.twitter.android.R.attr.actionPromptTitleColorWhite
        0x7f0101a9    # com.twitter.android.R.attr.actionPromptTitleMarginBottom
        0x7f0101aa    # com.twitter.android.R.attr.actionPromptBodyColorGray
        0x7f0101ab    # com.twitter.android.R.attr.actionPromptBodyColorWhite
        0x7f0101ac    # com.twitter.android.R.attr.actionPromptBackgroundColorBlue
        0x7f0101ad    # com.twitter.android.R.attr.actionPromptBackgroundColorGray
        0x7f0101ae    # com.twitter.android.R.attr.actionPromptPressedBackgroundColor
        0x7f0101af    # com.twitter.android.R.attr.actionPromptBorderColor
        0x7f0101b0    # com.twitter.android.R.attr.actionPromptDismissGray
        0x7f0101b1    # com.twitter.android.R.attr.actionPromptDismissWhite
        0x7f0101b2    # com.twitter.android.R.attr.actionPromptDismissMargin
        0x7f0101b3    # com.twitter.android.R.attr.actionPromptFavoriteIcon
        0x7f0101b4    # com.twitter.android.R.attr.actionPromptReplyIcon
    .end array-data

    :array_1d
    .array-data 4
        0x7f010004    # com.twitter.android.R.attr.bylineSize
        0x7f01000a    # com.twitter.android.R.attr.contentSize
    .end array-data

    :array_1e
    .array-data 4
        0x7f010025    # com.twitter.android.R.attr.politicalDrawable
        0x7f01002c    # com.twitter.android.R.attr.promotedDrawable
        0x7f0101b5    # com.twitter.android.R.attr.defaultProfileImageDrawable
        0x7f0101b6    # com.twitter.android.R.attr.actionButtonPadding
        0x7f0101b7    # com.twitter.android.R.attr.actionButtonPaddingLeft
        0x7f0101b8    # com.twitter.android.R.attr.actionButtonPaddingTop
        0x7f0101b9    # com.twitter.android.R.attr.actionButtonPaddingRight
        0x7f0101ba    # com.twitter.android.R.attr.actionButtonPaddingBottom
        0x7f0101bb    # com.twitter.android.R.attr.profileTextColor
    .end array-data

    :array_1f
    .array-data 4
        0x7f0101bc    # com.twitter.android.R.attr.tabDrawable
        0x7f0101bd    # com.twitter.android.R.attr.tabMaxHeight
    .end array-data
.end method
