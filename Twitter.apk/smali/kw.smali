.class public abstract Lkw;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field protected a:I

.field protected b:I

.field protected c:Landroid/graphics/Bitmap$Config;

.field protected d:Z

.field protected e:Z

.field protected f:Z

.field protected g:Z

.field protected h:Z

.field protected i:Landroid/net/Uri;

.field protected j:Landroid/content/Context;

.field protected k:Ljava/io/InputStream;

.field protected l:Ljava/io/File;

.field protected m:I


# direct methods
.method protected constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;)Lkw;
    .locals 1

    invoke-static {}, Lkw;->e()Lkw;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lkw;->b(Landroid/content/Context;Landroid/net/Uri;)Lkw;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/io/InputStream;)Lkw;
    .locals 1

    invoke-static {}, Lkw;->e()Lkw;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lkw;->b(Landroid/content/Context;Ljava/io/InputStream;)Lkw;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/io/File;)Lkw;
    .locals 1

    invoke-static {}, Lkw;->e()Lkw;

    move-result-object v0

    invoke-virtual {v0, p0}, Lkw;->b(Ljava/io/File;)Lkw;

    move-result-object v0

    return-object v0
.end method

.method protected static e()Lkw;
    .locals 1

    new-instance v0, Lkv;

    invoke-direct {v0}, Lkv;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a()Lkw;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lkw;->d:Z

    return-object p0
.end method

.method public a(I)Lkw;
    .locals 1

    iput p1, p0, Lkw;->a:I

    iput p1, p0, Lkw;->b:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lkw;->g:Z

    return-object p0
.end method

.method public a(II)Lkw;
    .locals 1

    const/4 v0, 0x1

    iput p1, p0, Lkw;->a:I

    iput p2, p0, Lkw;->b:I

    iput-boolean v0, p0, Lkw;->e:Z

    iput-boolean v0, p0, Lkw;->f:Z

    return-object p0
.end method

.method public a(Landroid/graphics/Bitmap$Config;)Lkw;
    .locals 0

    iput-object p1, p0, Lkw;->c:Landroid/graphics/Bitmap$Config;

    return-object p0
.end method

.method protected a(Landroid/net/Uri;Z)Lkx;
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lkw;->j:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    :try_start_1
    invoke-virtual {p0, v1, p2}, Lkw;->a(Ljava/io/InputStream;Z)Lkx;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    move-object v1, v0

    :goto_1
    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_2
    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_2

    :catch_1
    move-exception v2

    goto :goto_1
.end method

.method protected a(Ljava/io/File;Z)Lkx;
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {p0, v1, p2}, Lkw;->a(Ljava/io/FileInputStream;Z)Lkx;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    move-object v1, v0

    :goto_1
    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_2
    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_2

    :catch_1
    move-exception v2

    goto :goto_1
.end method

.method protected abstract a(Ljava/io/FileInputStream;Z)Lkx;
.end method

.method protected a(Ljava/io/InputStream;Z)Lkx;
    .locals 6

    const/4 v0, 0x0

    instance-of v1, p1, Ljava/io/FileInputStream;

    if-eqz v1, :cond_0

    :try_start_0
    check-cast p1, Ljava/io/FileInputStream;

    invoke-virtual {p0, p1, p2}, Lkw;->a(Ljava/io/FileInputStream;Z)Lkx;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    iget-object v2, p0, Lkw;->j:Landroid/content/Context;

    invoke-static {v2, v1}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lkw;->j:Landroid/content/Context;

    invoke-static {v1}, Lcom/twitter/library/util/Util;->j(Landroid/content/Context;)Ljava/io/File;

    move-result-object v3

    :try_start_1
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/16 v2, 0x1000

    :try_start_2
    invoke-static {p1, v1, v2}, Lcom/twitter/internal/util/h;->a(Ljava/io/InputStream;Ljava/io/OutputStream;I)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_4
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    move-result v2

    if-nez v2, :cond_1

    :try_start_3
    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    goto :goto_0

    :cond_1
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->flush()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    :try_start_5
    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, v3}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_6
    .catch Ljava/lang/OutOfMemoryError; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :try_start_7
    invoke-virtual {p0, v2, p2}, Lkw;->a(Ljava/io/FileInputStream;Z)Lkx;
    :try_end_7
    .catch Ljava/lang/OutOfMemoryError; {:try_start_7 .. :try_end_7} :catch_3
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    move-result-object v0

    :try_start_8
    invoke-static {v2}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    goto :goto_0

    :catch_1
    move-exception v1

    move-object v1, v0

    :goto_1
    :try_start_9
    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    goto :goto_0

    :catchall_0
    move-exception v1

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    :goto_2
    :try_start_a
    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    throw v0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :catchall_1
    move-exception v0

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    throw v0

    :catch_2
    move-exception v1

    move-object v2, v0

    :goto_3
    :try_start_b
    iget-object v4, p0, Lkw;->j:Landroid/content/Context;

    invoke-static {v4, v1}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Ljava/lang/Throwable;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    :try_start_c
    invoke-static {v2}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    goto :goto_0

    :catchall_2
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_4
    :try_start_d
    invoke-static {v2}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    throw v0
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    :catchall_3
    move-exception v0

    goto :goto_4

    :catch_3
    move-exception v1

    goto :goto_3

    :catchall_4
    move-exception v0

    goto :goto_2

    :catch_4
    move-exception v2

    goto :goto_1
.end method

.method protected a(Z)Lkx;
    .locals 2

    iget-object v0, p0, Lkw;->i:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkw;->i:Landroid/net/Uri;

    invoke-virtual {p0, v0, p1}, Lkw;->a(Landroid/net/Uri;Z)Lkx;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lkw;->k:Ljava/io/InputStream;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lkw;->k:Ljava/io/InputStream;

    invoke-virtual {p0, v0, p1}, Lkw;->a(Ljava/io/InputStream;Z)Lkx;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lkw;->l:Ljava/io/File;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lkw;->l:Ljava/io/File;

    invoke-virtual {p0, v0, p1}, Lkw;->a(Ljava/io/File;Z)Lkx;

    move-result-object v0

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "No bitmap source specified."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()Landroid/graphics/Bitmap;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lkw;->a(Z)Lkx;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lkx;->a:Landroid/graphics/Bitmap;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(I)Lkw;
    .locals 1

    iput p1, p0, Lkw;->a:I

    iput p1, p0, Lkw;->b:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lkw;->e:Z

    return-object p0
.end method

.method public b(II)Lkw;
    .locals 1

    iput p1, p0, Lkw;->a:I

    iput p2, p0, Lkw;->b:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lkw;->f:Z

    return-object p0
.end method

.method public b(Landroid/content/Context;Landroid/net/Uri;)Lkw;
    .locals 0

    iput-object p1, p0, Lkw;->j:Landroid/content/Context;

    iput-object p2, p0, Lkw;->i:Landroid/net/Uri;

    return-object p0
.end method

.method public b(Landroid/content/Context;Ljava/io/InputStream;)Lkw;
    .locals 0

    iput-object p1, p0, Lkw;->j:Landroid/content/Context;

    iput-object p2, p0, Lkw;->k:Ljava/io/InputStream;

    return-object p0
.end method

.method public b(Ljava/io/File;)Lkw;
    .locals 0

    iput-object p1, p0, Lkw;->l:Ljava/io/File;

    return-object p0
.end method

.method public c(II)I
    .locals 4

    const/4 v2, 0x1

    iget v0, p0, Lkw;->a:I

    if-eqz v0, :cond_0

    iget v0, p0, Lkw;->b:I

    if-nez v0, :cond_1

    :cond_0
    return v2

    :cond_1
    iget-boolean v0, p0, Lkw;->f:Z

    if-eqz v0, :cond_3

    int-to-float v0, p1

    int-to-float v1, p2

    div-float/2addr v0, v1

    iget v1, p0, Lkw;->a:I

    int-to-float v1, v1

    iget v3, p0, Lkw;->b:I

    int-to-float v3, v3

    div-float/2addr v1, v3

    cmpl-float v0, v0, v1

    if-lez v0, :cond_2

    iget v0, p0, Lkw;->a:I

    :goto_0
    iget-boolean v1, p0, Lkw;->e:Z

    if-eqz v1, :cond_4

    move v1, v2

    :goto_1
    div-int v3, p1, v1

    if-lt v3, v0, :cond_0

    div-int/lit8 p1, p1, 0x2

    mul-int/lit8 v2, v2, 0x2

    goto :goto_1

    :cond_2
    iget v0, p0, Lkw;->b:I

    move p1, p2

    goto :goto_0

    :cond_3
    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result p1

    iget v0, p0, Lkw;->a:I

    iget v1, p0, Lkw;->b:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_0

    :cond_4
    const/4 v1, 0x2

    goto :goto_1
.end method

.method public c()Landroid/graphics/Rect;
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lkw;->a(Z)Lkx;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lkx;->b:Landroid/graphics/Rect;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(I)Lkw;
    .locals 0

    iput p1, p0, Lkw;->a:I

    iput p1, p0, Lkw;->b:I

    return-object p0
.end method

.method public d(II)Landroid/graphics/Rect;
    .locals 9

    const/high16 v2, 0x3f800000    # 1.0f

    const/high16 v8, 0x3f000000    # 0.5f

    const/4 v4, 0x1

    const/4 v7, 0x0

    iget v0, p0, Lkw;->a:I

    if-eqz v0, :cond_0

    iget v0, p0, Lkw;->b:I

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v7, v7, p1, p2}, Landroid/graphics/Rect;-><init>(IIII)V

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0, p1, p2}, Lkw;->c(II)I

    move-result v5

    move v1, p2

    move v0, p1

    move v3, v4

    :goto_1
    if-ge v3, v5, :cond_4

    const/16 v6, 0x8

    if-gt v3, v6, :cond_4

    rem-int/lit8 v6, v0, 0x2

    if-ne v6, v4, :cond_2

    add-int/lit8 v0, v0, 0x1

    :cond_2
    rem-int/lit8 v6, v1, 0x2

    if-ne v6, v4, :cond_3

    add-int/lit8 v1, v1, 0x1

    :cond_3
    div-int/lit8 v0, v0, 0x2

    div-int/lit8 v1, v1, 0x2

    mul-int/lit8 v3, v3, 0x2

    goto :goto_1

    :cond_4
    mul-int v3, v5, v0

    div-int/2addr v3, p1

    div-int v5, v0, v3

    div-int v3, v1, v3

    iget-boolean v0, p0, Lkw;->g:Z

    if-eqz v0, :cond_6

    invoke-virtual {p0, v5, v3}, Lkw;->e(II)F

    move-result v0

    cmpl-float v1, v0, v2

    if-eqz v1, :cond_5

    iput-boolean v4, p0, Lkw;->h:Z

    :cond_5
    :goto_2
    new-instance v1, Landroid/graphics/Rect;

    int-to-float v2, v5

    mul-float/2addr v2, v0

    add-float/2addr v2, v8

    float-to-double v4, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v2, v4

    int-to-float v3, v3

    mul-float/2addr v0, v3

    add-float/2addr v0, v8

    float-to-double v3, v0

    invoke-static {v3, v4}, Ljava/lang/Math;->floor(D)D

    move-result-wide v3

    double-to-int v0, v3

    invoke-direct {v1, v7, v7, v2, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object v0, v1

    goto :goto_0

    :cond_6
    move v0, v2

    goto :goto_2
.end method

.method public d()Z
    .locals 1

    iget-boolean v0, p0, Lkw;->h:Z

    return v0
.end method

.method protected e(II)F
    .locals 3

    iget v0, p0, Lkw;->a:I

    iget v1, p0, Lkw;->b:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-static {p1, p2}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget-boolean v2, p0, Lkw;->f:Z

    if-eqz v2, :cond_2

    int-to-float v0, p1

    int-to-float v1, p2

    div-float/2addr v0, v1

    iget v1, p0, Lkw;->a:I

    int-to-float v1, v1

    iget v2, p0, Lkw;->b:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget v0, p0, Lkw;->a:I

    :goto_0
    if-le p1, v0, :cond_1

    int-to-float v0, v0

    int-to-float v1, p1

    div-float/2addr v0, v1

    :goto_1
    return v0

    :cond_0
    iget v0, p0, Lkw;->b:I

    move p1, p2

    goto :goto_0

    :cond_1
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_1

    :cond_2
    move p1, v0

    move v0, v1

    goto :goto_0
.end method
