.class abstract Lkb;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/os/Handler;

.field private final b:Landroid/view/View$OnTouchListener;

.field private c:I

.field private d:Landroid/view/View;


# direct methods
.method public constructor <init>(Z)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lkb;->c:I

    new-instance v0, Lkd;

    invoke-direct {v0, p0}, Lkd;-><init>(Lkb;)V

    iput-object v0, p0, Lkb;->a:Landroid/os/Handler;

    if-eqz p1, :cond_0

    new-instance v0, Lkc;

    invoke-direct {v0, p0}, Lkc;-><init>(Lkb;)V

    iput-object v0, p0, Lkb;->b:Landroid/view/View$OnTouchListener;

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lkb;->b:Landroid/view/View$OnTouchListener;

    goto :goto_0
.end method

.method private a()V
    .locals 1

    iget-object v0, p0, Lkb;->d:Landroid/view/View;

    invoke-virtual {p0, v0}, Lkb;->b(Landroid/view/View;)V

    invoke-direct {p0}, Lkb;->b()V

    return-void
.end method

.method private a(Landroid/view/MotionEvent;)V
    .locals 4

    const/4 v3, 0x2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    iput v3, p0, Lkb;->c:I

    iget-object v0, p0, Lkb;->d:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lkb;->a:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lkb;->a:Landroid/os/Handler;

    const-wide/16 v1, 0x12c

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_0
    return-void
.end method

.method static synthetic a(Lkb;)V
    .locals 0

    invoke-direct {p0}, Lkb;->a()V

    return-void
.end method

.method static synthetic a(Lkb;Landroid/view/MotionEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lkb;->a(Landroid/view/MotionEvent;)V

    return-void
.end method

.method private b()V
    .locals 2

    const/4 v1, 0x0

    iget v0, p0, Lkb;->c:I

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput v0, p0, Lkb;->c:I

    iget-object v0, p0, Lkb;->b:Landroid/view/View$OnTouchListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkb;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_0
    iget-object v0, p0, Lkb;->d:Landroid/view/View;

    invoke-virtual {p0, v0}, Lkb;->e(Landroid/view/View;)V

    iput-object v1, p0, Lkb;->d:Landroid/view/View;

    :cond_1
    return-void
.end method

.method static synthetic b(Lkb;)V
    .locals 0

    invoke-direct {p0}, Lkb;->b()V

    return-void
.end method


# virtual methods
.method protected a(Landroid/view/View;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x2

    iget v0, p0, Lkb;->c:I

    if-nez v0, :cond_2

    iput-object p1, p0, Lkb;->d:Landroid/view/View;

    iget-object v0, p0, Lkb;->b:Landroid/view/View$OnTouchListener;

    if-eqz v0, :cond_1

    iput v4, p0, Lkb;->c:I

    iget-object v0, p0, Lkb;->d:Landroid/view/View;

    iget-object v1, p0, Lkb;->b:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lkb;->a:Landroid/os/Handler;

    invoke-static {}, Ljy;->d()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v4, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :goto_0
    iget-object v0, p0, Lkb;->d:Landroid/view/View;

    invoke-virtual {p0, v0}, Lkb;->c(Landroid/view/View;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    iput v3, p0, Lkb;->c:I

    iget-object v0, p0, Lkb;->a:Landroid/os/Handler;

    const-wide/16 v1, 0x190

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    :cond_2
    iget v0, p0, Lkb;->c:I

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lkb;->d:Landroid/view/View;

    if-ne v0, p1, :cond_3

    const/4 v0, 0x0

    iput v0, p0, Lkb;->c:I

    iget-object v0, p0, Lkb;->a:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lkb;->d:Landroid/view/View;

    invoke-virtual {p0, v0}, Lkb;->d(Landroid/view/View;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lkb;->d:Landroid/view/View;

    goto :goto_1

    :cond_3
    invoke-direct {p0}, Lkb;->b()V

    goto :goto_1
.end method

.method protected abstract b(Landroid/view/View;)V
.end method

.method protected abstract c(Landroid/view/View;)V
.end method

.method protected abstract d(Landroid/view/View;)V
.end method

.method protected abstract e(Landroid/view/View;)V
.end method
