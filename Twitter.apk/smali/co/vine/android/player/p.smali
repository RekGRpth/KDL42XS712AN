.class Lco/vine/android/player/p;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/media/MediaPlayer$OnVideoSizeChangedListener;


# instance fields
.field final synthetic a:Lco/vine/android/player/SdkVideoView;


# direct methods
.method constructor <init>(Lco/vine/android/player/SdkVideoView;)V
    .locals 0

    iput-object p1, p0, Lco/vine/android/player/p;->a:Lco/vine/android/player/SdkVideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onVideoSizeChanged(Landroid/media/MediaPlayer;II)V
    .locals 2

    iget-object v0, p0, Lco/vine/android/player/p;->a:Lco/vine/android/player/SdkVideoView;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v1

    invoke-static {v0, v1}, Lco/vine/android/player/SdkVideoView;->a(Lco/vine/android/player/SdkVideoView;I)I

    iget-object v0, p0, Lco/vine/android/player/p;->a:Lco/vine/android/player/SdkVideoView;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v1

    invoke-static {v0, v1}, Lco/vine/android/player/SdkVideoView;->b(Lco/vine/android/player/SdkVideoView;I)I

    iget-object v0, p0, Lco/vine/android/player/p;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v0}, Lco/vine/android/player/SdkVideoView;->a(Lco/vine/android/player/SdkVideoView;)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/vine/android/player/p;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v0}, Lco/vine/android/player/SdkVideoView;->b(Lco/vine/android/player/SdkVideoView;)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/vine/android/player/p;->a:Lco/vine/android/player/SdkVideoView;

    invoke-virtual {v0}, Lco/vine/android/player/SdkVideoView;->requestLayout()V

    :cond_0
    return-void
.end method
