.class public Lco/vine/android/player/SdkVideoView;
.super Landroid/view/TextureView;
.source "Twttr"

# interfaces
.implements Landroid/widget/MediaController$MediaPlayerControl;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# static fields
.field private static final d:Ljava/lang/String;


# instance fields
.field private A:Z

.field private B:Landroid/util/AttributeSet;

.field private C:I

.field private D:Z

.field private final E:[I

.field private F:Z

.field private G:Z

.field private H:Landroid/media/MediaPlayer$OnCompletionListener;

.field private I:Landroid/media/MediaPlayer$OnErrorListener;

.field private J:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

.field private K:Landroid/view/TextureView$SurfaceTextureListener;

.field public a:I

.field b:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

.field c:Landroid/media/MediaPlayer$OnPreparedListener;

.field private e:I

.field private f:I

.field private g:Landroid/view/Surface;

.field private h:Landroid/media/MediaPlayer;

.field private i:I

.field private j:I

.field private k:Landroid/widget/MediaController;

.field private l:Landroid/media/MediaPlayer$OnCompletionListener;

.field private m:Landroid/media/MediaPlayer$OnPreparedListener;

.field private n:I

.field private o:Landroid/media/MediaPlayer$OnErrorListener;

.field private p:Landroid/media/MediaPlayer$OnInfoListener;

.field private q:I

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:I

.field private v:Landroid/net/Uri;

.field private w:Ljava/util/Map;

.field private x:I

.field private y:Landroid/content/Context;

.field private z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lco/vine/android/player/SdkVideoView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lco/vine/android/player/SdkVideoView;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/view/TextureView;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    iput v1, p0, Lco/vine/android/player/SdkVideoView;->a:I

    iput v1, p0, Lco/vine/android/player/SdkVideoView;->u:I

    new-array v0, v1, [I

    iput-object v0, p0, Lco/vine/android/player/SdkVideoView;->E:[I

    iput-boolean v1, p0, Lco/vine/android/player/SdkVideoView;->F:Z

    iput-boolean v1, p0, Lco/vine/android/player/SdkVideoView;->G:Z

    new-instance v0, Lco/vine/android/player/p;

    invoke-direct {v0, p0}, Lco/vine/android/player/p;-><init>(Lco/vine/android/player/SdkVideoView;)V

    iput-object v0, p0, Lco/vine/android/player/SdkVideoView;->b:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    new-instance v0, Lco/vine/android/player/q;

    invoke-direct {v0, p0}, Lco/vine/android/player/q;-><init>(Lco/vine/android/player/SdkVideoView;)V

    iput-object v0, p0, Lco/vine/android/player/SdkVideoView;->c:Landroid/media/MediaPlayer$OnPreparedListener;

    new-instance v0, Lco/vine/android/player/r;

    invoke-direct {v0, p0}, Lco/vine/android/player/r;-><init>(Lco/vine/android/player/SdkVideoView;)V

    iput-object v0, p0, Lco/vine/android/player/SdkVideoView;->H:Landroid/media/MediaPlayer$OnCompletionListener;

    new-instance v0, Lco/vine/android/player/s;

    invoke-direct {v0, p0}, Lco/vine/android/player/s;-><init>(Lco/vine/android/player/SdkVideoView;)V

    iput-object v0, p0, Lco/vine/android/player/SdkVideoView;->I:Landroid/media/MediaPlayer$OnErrorListener;

    new-instance v0, Lco/vine/android/player/t;

    invoke-direct {v0, p0}, Lco/vine/android/player/t;-><init>(Lco/vine/android/player/SdkVideoView;)V

    iput-object v0, p0, Lco/vine/android/player/SdkVideoView;->J:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    new-instance v0, Lco/vine/android/player/u;

    invoke-direct {v0, p0}, Lco/vine/android/player/u;-><init>(Lco/vine/android/player/SdkVideoView;)V

    iput-object v0, p0, Lco/vine/android/player/SdkVideoView;->K:Landroid/view/TextureView$SurfaceTextureListener;

    invoke-direct {p0}, Lco/vine/android/player/SdkVideoView;->e()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2}, Landroid/view/TextureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    iput v1, p0, Lco/vine/android/player/SdkVideoView;->a:I

    iput v1, p0, Lco/vine/android/player/SdkVideoView;->u:I

    new-array v0, v1, [I

    iput-object v0, p0, Lco/vine/android/player/SdkVideoView;->E:[I

    iput-boolean v1, p0, Lco/vine/android/player/SdkVideoView;->F:Z

    iput-boolean v1, p0, Lco/vine/android/player/SdkVideoView;->G:Z

    new-instance v0, Lco/vine/android/player/p;

    invoke-direct {v0, p0}, Lco/vine/android/player/p;-><init>(Lco/vine/android/player/SdkVideoView;)V

    iput-object v0, p0, Lco/vine/android/player/SdkVideoView;->b:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    new-instance v0, Lco/vine/android/player/q;

    invoke-direct {v0, p0}, Lco/vine/android/player/q;-><init>(Lco/vine/android/player/SdkVideoView;)V

    iput-object v0, p0, Lco/vine/android/player/SdkVideoView;->c:Landroid/media/MediaPlayer$OnPreparedListener;

    new-instance v0, Lco/vine/android/player/r;

    invoke-direct {v0, p0}, Lco/vine/android/player/r;-><init>(Lco/vine/android/player/SdkVideoView;)V

    iput-object v0, p0, Lco/vine/android/player/SdkVideoView;->H:Landroid/media/MediaPlayer$OnCompletionListener;

    new-instance v0, Lco/vine/android/player/s;

    invoke-direct {v0, p0}, Lco/vine/android/player/s;-><init>(Lco/vine/android/player/SdkVideoView;)V

    iput-object v0, p0, Lco/vine/android/player/SdkVideoView;->I:Landroid/media/MediaPlayer$OnErrorListener;

    new-instance v0, Lco/vine/android/player/t;

    invoke-direct {v0, p0}, Lco/vine/android/player/t;-><init>(Lco/vine/android/player/SdkVideoView;)V

    iput-object v0, p0, Lco/vine/android/player/SdkVideoView;->J:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    new-instance v0, Lco/vine/android/player/u;

    invoke-direct {v0, p0}, Lco/vine/android/player/u;-><init>(Lco/vine/android/player/SdkVideoView;)V

    iput-object v0, p0, Lco/vine/android/player/SdkVideoView;->K:Landroid/view/TextureView$SurfaceTextureListener;

    iput-object p2, p0, Lco/vine/android/player/SdkVideoView;->B:Landroid/util/AttributeSet;

    invoke-direct {p0}, Lco/vine/android/player/SdkVideoView;->e()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/TextureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    iput v1, p0, Lco/vine/android/player/SdkVideoView;->a:I

    iput v1, p0, Lco/vine/android/player/SdkVideoView;->u:I

    new-array v0, v1, [I

    iput-object v0, p0, Lco/vine/android/player/SdkVideoView;->E:[I

    iput-boolean v1, p0, Lco/vine/android/player/SdkVideoView;->F:Z

    iput-boolean v1, p0, Lco/vine/android/player/SdkVideoView;->G:Z

    new-instance v0, Lco/vine/android/player/p;

    invoke-direct {v0, p0}, Lco/vine/android/player/p;-><init>(Lco/vine/android/player/SdkVideoView;)V

    iput-object v0, p0, Lco/vine/android/player/SdkVideoView;->b:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    new-instance v0, Lco/vine/android/player/q;

    invoke-direct {v0, p0}, Lco/vine/android/player/q;-><init>(Lco/vine/android/player/SdkVideoView;)V

    iput-object v0, p0, Lco/vine/android/player/SdkVideoView;->c:Landroid/media/MediaPlayer$OnPreparedListener;

    new-instance v0, Lco/vine/android/player/r;

    invoke-direct {v0, p0}, Lco/vine/android/player/r;-><init>(Lco/vine/android/player/SdkVideoView;)V

    iput-object v0, p0, Lco/vine/android/player/SdkVideoView;->H:Landroid/media/MediaPlayer$OnCompletionListener;

    new-instance v0, Lco/vine/android/player/s;

    invoke-direct {v0, p0}, Lco/vine/android/player/s;-><init>(Lco/vine/android/player/SdkVideoView;)V

    iput-object v0, p0, Lco/vine/android/player/SdkVideoView;->I:Landroid/media/MediaPlayer$OnErrorListener;

    new-instance v0, Lco/vine/android/player/t;

    invoke-direct {v0, p0}, Lco/vine/android/player/t;-><init>(Lco/vine/android/player/SdkVideoView;)V

    iput-object v0, p0, Lco/vine/android/player/SdkVideoView;->J:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    new-instance v0, Lco/vine/android/player/u;

    invoke-direct {v0, p0}, Lco/vine/android/player/u;-><init>(Lco/vine/android/player/SdkVideoView;)V

    iput-object v0, p0, Lco/vine/android/player/SdkVideoView;->K:Landroid/view/TextureView$SurfaceTextureListener;

    iput-object p2, p0, Lco/vine/android/player/SdkVideoView;->B:Landroid/util/AttributeSet;

    invoke-direct {p0}, Lco/vine/android/player/SdkVideoView;->e()V

    return-void
.end method

.method static synthetic a(Lco/vine/android/player/SdkVideoView;)I
    .locals 1

    iget v0, p0, Lco/vine/android/player/SdkVideoView;->e:I

    return v0
.end method

.method static synthetic a(Lco/vine/android/player/SdkVideoView;I)I
    .locals 0

    iput p1, p0, Lco/vine/android/player/SdkVideoView;->e:I

    return p1
.end method

.method static synthetic a(Lco/vine/android/player/SdkVideoView;Landroid/view/Surface;)Landroid/view/Surface;
    .locals 0

    iput-object p1, p0, Lco/vine/android/player/SdkVideoView;->g:Landroid/view/Surface;

    return-object p1
.end method

.method static synthetic a(Lco/vine/android/player/SdkVideoView;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lco/vine/android/player/SdkVideoView;->z:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lco/vine/android/player/SdkVideoView;Z)Z
    .locals 0

    iput-boolean p1, p0, Lco/vine/android/player/SdkVideoView;->r:Z

    return p1
.end method

.method static synthetic b(Lco/vine/android/player/SdkVideoView;)I
    .locals 1

    iget v0, p0, Lco/vine/android/player/SdkVideoView;->f:I

    return v0
.end method

.method static synthetic b(Lco/vine/android/player/SdkVideoView;I)I
    .locals 0

    iput p1, p0, Lco/vine/android/player/SdkVideoView;->f:I

    return p1
.end method

.method private declared-synchronized b(Z)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lco/vine/android/player/SdkVideoView;->E:[I

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_1

    :try_start_2
    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V
    :try_end_2
    .catch Ljava/lang/IllegalStateException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    :goto_0
    :try_start_3
    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    const/4 v0, 0x0

    iput-object v0, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    const/4 v0, 0x0

    iput v0, p0, Lco/vine/android/player/SdkVideoView;->a:I

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    iput v0, p0, Lco/vine/android/player/SdkVideoView;->u:I

    const/4 v0, 0x0

    iput-object v0, p0, Lco/vine/android/player/SdkVideoView;->z:Ljava/lang/String;

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lco/vine/android/player/SdkVideoView;->setKeepScreenOn(Z)V

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic b(Lco/vine/android/player/SdkVideoView;Z)Z
    .locals 0

    iput-boolean p1, p0, Lco/vine/android/player/SdkVideoView;->s:Z

    return p1
.end method

.method static synthetic c(Lco/vine/android/player/SdkVideoView;I)I
    .locals 0

    iput p1, p0, Lco/vine/android/player/SdkVideoView;->u:I

    return p1
.end method

.method static synthetic c(Lco/vine/android/player/SdkVideoView;)Landroid/media/MediaPlayer$OnPreparedListener;
    .locals 1

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->m:Landroid/media/MediaPlayer$OnPreparedListener;

    return-object v0
.end method

.method static synthetic c(Lco/vine/android/player/SdkVideoView;Z)Z
    .locals 0

    iput-boolean p1, p0, Lco/vine/android/player/SdkVideoView;->t:Z

    return p1
.end method

.method static synthetic d(Lco/vine/android/player/SdkVideoView;I)I
    .locals 0

    iput p1, p0, Lco/vine/android/player/SdkVideoView;->n:I

    return p1
.end method

.method static synthetic d(Lco/vine/android/player/SdkVideoView;)Landroid/media/MediaPlayer;
    .locals 1

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method static synthetic d()Ljava/lang/String;
    .locals 1

    sget-object v0, Lco/vine/android/player/SdkVideoView;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lco/vine/android/player/SdkVideoView;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lco/vine/android/player/SdkVideoView;->b(Z)V

    return-void
.end method

.method static synthetic e(Lco/vine/android/player/SdkVideoView;I)I
    .locals 0

    iput p1, p0, Lco/vine/android/player/SdkVideoView;->i:I

    return p1
.end method

.method static synthetic e(Lco/vine/android/player/SdkVideoView;)Landroid/widget/MediaController;
    .locals 1

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->k:Landroid/widget/MediaController;

    return-object v0
.end method

.method private e()V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0}, Lco/vine/android/player/SdkVideoView;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lco/vine/android/player/SdkVideoView;->y:Landroid/content/Context;

    iput v1, p0, Lco/vine/android/player/SdkVideoView;->e:I

    iput v1, p0, Lco/vine/android/player/SdkVideoView;->f:I

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->K:Landroid/view/TextureView$SurfaceTextureListener;

    invoke-virtual {p0, v0}, Lco/vine/android/player/SdkVideoView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    invoke-virtual {p0, v2}, Lco/vine/android/player/SdkVideoView;->setFocusable(Z)V

    invoke-virtual {p0, v2}, Lco/vine/android/player/SdkVideoView;->setFocusableInTouchMode(Z)V

    invoke-virtual {p0}, Lco/vine/android/player/SdkVideoView;->requestFocus()Z

    iput v1, p0, Lco/vine/android/player/SdkVideoView;->a:I

    iput v1, p0, Lco/vine/android/player/SdkVideoView;->u:I

    return-void
.end method

.method static synthetic f(Lco/vine/android/player/SdkVideoView;)I
    .locals 1

    iget v0, p0, Lco/vine/android/player/SdkVideoView;->q:I

    return v0
.end method

.method static synthetic f(Lco/vine/android/player/SdkVideoView;I)I
    .locals 0

    iput p1, p0, Lco/vine/android/player/SdkVideoView;->j:I

    return p1
.end method

.method private f()V
    .locals 5

    iget-object v1, p0, Lco/vine/android/player/SdkVideoView;->E:[I

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->v:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->g:Landroid/view/Surface;

    if-nez v0, :cond_1

    :cond_0
    monitor-exit v1

    :goto_0
    return-void

    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v2, "com.android.music.musicservicecommand"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "command"

    const-string/jumbo v3, "pause"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lco/vine/android/player/SdkVideoView;->y:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lco/vine/android/player/SdkVideoView;->b(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lco/vine/android/player/SdkVideoView;->c:Landroid/media/MediaPlayer$OnPreparedListener;

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lco/vine/android/player/SdkVideoView;->b:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    const/4 v0, -0x1

    iput v0, p0, Lco/vine/android/player/SdkVideoView;->x:I

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lco/vine/android/player/SdkVideoView;->H:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lco/vine/android/player/SdkVideoView;->I:Landroid/media/MediaPlayer$OnErrorListener;

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lco/vine/android/player/SdkVideoView;->p:Landroid/media/MediaPlayer$OnInfoListener;

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lco/vine/android/player/SdkVideoView;->J:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V

    const/4 v0, 0x0

    iput v0, p0, Lco/vine/android/player/SdkVideoView;->n:I

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lco/vine/android/player/SdkVideoView;->y:Landroid/content/Context;

    iget-object v3, p0, Lco/vine/android/player/SdkVideoView;->v:Landroid/net/Uri;

    iget-object v4, p0, Lco/vine/android/player/SdkVideoView;->w:Ljava/util/Map;

    invoke-virtual {v0, v2, v3, v4}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    iget-object v2, p0, Lco/vine/android/player/SdkVideoView;->g:Landroid/view/Surface;

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setSurface(Landroid/view/Surface;)V

    iget-boolean v0, p0, Lco/vine/android/player/SdkVideoView;->A:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setLooping(Z)V

    :cond_2
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lco/vine/android/player/SdkVideoView;->setKeepScreenOn(Z)V

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    iget-boolean v0, p0, Lco/vine/android/player/SdkVideoView;->F:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/media/MediaPlayer;->setVolume(FF)V

    :cond_3
    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V

    const/4 v0, 0x1

    iput v0, p0, Lco/vine/android/player/SdkVideoView;->a:I

    invoke-direct {p0}, Lco/vine/android/player/SdkVideoView;->g()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    :try_start_2
    monitor-exit v1

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catch_0
    move-exception v0

    :try_start_3
    sget-object v2, Lco/vine/android/player/SdkVideoView;->d:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Unable to open content "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lco/vine/android/player/SdkVideoView;->v:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, -0x1

    iput v0, p0, Lco/vine/android/player/SdkVideoView;->a:I

    const/4 v0, -0x1

    iput v0, p0, Lco/vine/android/player/SdkVideoView;->u:I

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->I:Landroid/media/MediaPlayer$OnErrorListener;

    iget-object v2, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-interface {v0, v2, v3, v4}, Landroid/media/MediaPlayer$OnErrorListener;->onError(Landroid/media/MediaPlayer;II)Z

    goto :goto_1

    :catch_1
    move-exception v0

    sget-object v2, Lco/vine/android/player/SdkVideoView;->d:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Unable to open content "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lco/vine/android/player/SdkVideoView;->v:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, -0x1

    iput v0, p0, Lco/vine/android/player/SdkVideoView;->a:I

    const/4 v0, -0x1

    iput v0, p0, Lco/vine/android/player/SdkVideoView;->u:I

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->I:Landroid/media/MediaPlayer$OnErrorListener;

    iget-object v2, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-interface {v0, v2, v3, v4}, Landroid/media/MediaPlayer$OnErrorListener;->onError(Landroid/media/MediaPlayer;II)Z

    goto :goto_1

    :catch_2
    move-exception v0

    sget-object v2, Lco/vine/android/player/SdkVideoView;->d:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Unable to open content "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lco/vine/android/player/SdkVideoView;->v:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, -0x1

    iput v0, p0, Lco/vine/android/player/SdkVideoView;->a:I

    const/4 v0, -0x1

    iput v0, p0, Lco/vine/android/player/SdkVideoView;->u:I

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->I:Landroid/media/MediaPlayer$OnErrorListener;

    iget-object v2, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-interface {v0, v2, v3, v4}, Landroid/media/MediaPlayer$OnErrorListener;->onError(Landroid/media/MediaPlayer;II)Z

    goto/16 :goto_1

    :catch_3
    move-exception v0

    sget-object v2, Lco/vine/android/player/SdkVideoView;->d:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Unable to open content "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lco/vine/android/player/SdkVideoView;->v:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v0, -0x1

    iput v0, p0, Lco/vine/android/player/SdkVideoView;->a:I

    const/4 v0, -0x1

    iput v0, p0, Lco/vine/android/player/SdkVideoView;->u:I

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->I:Landroid/media/MediaPlayer$OnErrorListener;

    iget-object v2, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-interface {v0, v2, v3, v4}, Landroid/media/MediaPlayer$OnErrorListener;->onError(Landroid/media/MediaPlayer;II)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1
.end method

.method static synthetic g(Lco/vine/android/player/SdkVideoView;)I
    .locals 1

    iget v0, p0, Lco/vine/android/player/SdkVideoView;->i:I

    return v0
.end method

.method private g()V
    .locals 2

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->k:Landroid/widget/MediaController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->k:Landroid/widget/MediaController;

    invoke-virtual {v0, p0}, Landroid/widget/MediaController;->setMediaPlayer(Landroid/widget/MediaController$MediaPlayerControl;)V

    invoke-virtual {p0}, Lco/vine/android/player/SdkVideoView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    instance-of v0, v0, Landroid/view/View;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lco/vine/android/player/SdkVideoView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    :goto_0
    iget-object v1, p0, Lco/vine/android/player/SdkVideoView;->k:Landroid/widget/MediaController;

    invoke-virtual {v1, v0}, Landroid/widget/MediaController;->setAnchorView(Landroid/view/View;)V

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->k:Landroid/widget/MediaController;

    invoke-virtual {p0}, Lco/vine/android/player/SdkVideoView;->c()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/MediaController;->setEnabled(Z)V

    :cond_0
    return-void

    :cond_1
    move-object v0, p0

    goto :goto_0
.end method

.method static synthetic h(Lco/vine/android/player/SdkVideoView;)I
    .locals 1

    iget v0, p0, Lco/vine/android/player/SdkVideoView;->j:I

    return v0
.end method

.method private h()V
    .locals 1

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->k:Landroid/widget/MediaController;

    invoke-virtual {v0}, Landroid/widget/MediaController;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->k:Landroid/widget/MediaController;

    invoke-virtual {v0}, Landroid/widget/MediaController;->hide()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->k:Landroid/widget/MediaController;

    invoke-virtual {v0}, Landroid/widget/MediaController;->show()V

    goto :goto_0
.end method

.method static synthetic i(Lco/vine/android/player/SdkVideoView;)I
    .locals 1

    iget v0, p0, Lco/vine/android/player/SdkVideoView;->u:I

    return v0
.end method

.method static synthetic j(Lco/vine/android/player/SdkVideoView;)Landroid/media/MediaPlayer$OnCompletionListener;
    .locals 1

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->l:Landroid/media/MediaPlayer$OnCompletionListener;

    return-object v0
.end method

.method static synthetic k(Lco/vine/android/player/SdkVideoView;)Landroid/media/MediaPlayer$OnErrorListener;
    .locals 1

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->o:Landroid/media/MediaPlayer$OnErrorListener;

    return-object v0
.end method

.method static synthetic l(Lco/vine/android/player/SdkVideoView;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->y:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic m(Lco/vine/android/player/SdkVideoView;)V
    .locals 0

    invoke-direct {p0}, Lco/vine/android/player/SdkVideoView;->f()V

    return-void
.end method

.method private setVideoURI(Landroid/net/Uri;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lco/vine/android/player/SdkVideoView;->a(Landroid/net/Uri;Ljava/util/Map;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/net/Uri;Ljava/util/Map;)V
    .locals 1

    iput-object p1, p0, Lco/vine/android/player/SdkVideoView;->v:Landroid/net/Uri;

    iput-object p2, p0, Lco/vine/android/player/SdkVideoView;->w:Ljava/util/Map;

    const/4 v0, 0x0

    iput v0, p0, Lco/vine/android/player/SdkVideoView;->q:I

    invoke-direct {p0}, Lco/vine/android/player/SdkVideoView;->f()V

    invoke-virtual {p0}, Lco/vine/android/player/SdkVideoView;->requestLayout()V

    invoke-virtual {p0}, Lco/vine/android/player/SdkVideoView;->invalidate()V

    return-void
.end method

.method public a(Z)V
    .locals 1

    if-nez p1, :cond_0

    iget-boolean v0, p0, Lco/vine/android/player/SdkVideoView;->D:Z

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lco/vine/android/player/SdkVideoView;->D:Z

    invoke-direct {p0}, Lco/vine/android/player/SdkVideoView;->f()V

    :cond_1
    return-void
.end method

.method public a()Z
    .locals 2

    iget v0, p0, Lco/vine/android/player/SdkVideoView;->a:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    iget-boolean v0, p0, Lco/vine/android/player/SdkVideoView;->F:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lco/vine/android/player/SdkVideoView;->G:Z

    iget-boolean v0, p0, Lco/vine/android/player/SdkVideoView;->F:Z

    invoke-virtual {p0, v0}, Lco/vine/android/player/SdkVideoView;->setMute(Z)V

    invoke-virtual {p0}, Lco/vine/android/player/SdkVideoView;->start()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    iget v1, p0, Lco/vine/android/player/SdkVideoView;->a:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget v1, p0, Lco/vine/android/player/SdkVideoView;->a:I

    if-eqz v1, :cond_0

    iget v1, p0, Lco/vine/android/player/SdkVideoView;->a:I

    if-eq v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public canPause()Z
    .locals 1

    iget-boolean v0, p0, Lco/vine/android/player/SdkVideoView;->r:Z

    return v0
.end method

.method public canSeekBackward()Z
    .locals 1

    iget-boolean v0, p0, Lco/vine/android/player/SdkVideoView;->s:Z

    return v0
.end method

.method public canSeekForward()Z
    .locals 1

    iget-boolean v0, p0, Lco/vine/android/player/SdkVideoView;->t:Z

    return v0
.end method

.method public getAttributes()Landroid/util/AttributeSet;
    .locals 1

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->B:Landroid/util/AttributeSet;

    return-object v0
.end method

.method public getAudioSessionId()I
    .locals 1

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    rem-int/lit8 v0, v0, 0xa

    return v0
.end method

.method public getBufferPercentage()I
    .locals 1

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget v0, p0, Lco/vine/android/player/SdkVideoView;->n:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCurrentPosition()I
    .locals 1

    :try_start_0
    invoke-virtual {p0}, Lco/vine/android/player/SdkVideoView;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getCurrentPosition()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, -0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDuration()I
    .locals 1

    invoke-virtual {p0}, Lco/vine/android/player/SdkVideoView;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lco/vine/android/player/SdkVideoView;->x:I

    if-lez v0, :cond_0

    iget v0, p0, Lco/vine/android/player/SdkVideoView;->x:I

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v0

    iput v0, p0, Lco/vine/android/player/SdkVideoView;->x:I

    iget v0, p0, Lco/vine/android/player/SdkVideoView;->x:I

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Lco/vine/android/player/SdkVideoView;->x:I

    iget v0, p0, Lco/vine/android/player/SdkVideoView;->x:I

    goto :goto_0
.end method

.method public getMediaPlayer()Landroid/media/MediaPlayer;
    .locals 1

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->z:Ljava/lang/String;

    return-object v0
.end method

.method public getPlayingPosition()I
    .locals 1

    iget v0, p0, Lco/vine/android/player/SdkVideoView;->C:I

    return v0
.end method

.method public isPlaying()Z
    .locals 1

    invoke-virtual {p0}, Lco/vine/android/player/SdkVideoView;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 5

    :try_start_0
    invoke-super {p0}, Landroid/view/TextureView;->onDetachedFromWindow()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    const/4 v0, 0x1

    :try_start_1
    invoke-virtual {p0}, Lco/vine/android/player/SdkVideoView;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/graphics/SurfaceTexture;->release()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    :goto_1
    sget-object v2, Lco/vine/android/player/SdkVideoView;->d:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Failed to detach from window: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v2, v0

    const/4 v0, 0x0

    sget-object v3, Lco/vine/android/player/SdkVideoView;->d:Ljava/lang/String;

    const-string/jumbo v4, "Failed to release"

    invoke-static {v3, v4, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/view/TextureView;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    const-class v0, Lco/vine/android/player/SdkVideoView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/view/TextureView;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    const-class v0, Lco/vine/android/player/SdkVideoView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3

    const/4 v1, 0x1

    const/4 v0, 0x4

    if-eq p1, v0, :cond_2

    const/16 v0, 0x18

    if-eq p1, v0, :cond_2

    const/16 v0, 0x19

    if-eq p1, v0, :cond_2

    const/16 v0, 0xa4

    if-eq p1, v0, :cond_2

    const/16 v0, 0x52

    if-eq p1, v0, :cond_2

    const/4 v0, 0x5

    if-eq p1, v0, :cond_2

    const/4 v0, 0x6

    if-eq p1, v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {p0}, Lco/vine/android/player/SdkVideoView;->c()Z

    move-result v2

    if-eqz v2, :cond_8

    if-eqz v0, :cond_8

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->k:Landroid/widget/MediaController;

    if-eqz v0, :cond_8

    const/16 v0, 0x4f

    if-eq p1, v0, :cond_0

    const/16 v0, 0x55

    if-ne p1, v0, :cond_4

    :cond_0
    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lco/vine/android/player/SdkVideoView;->pause()V

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->k:Landroid/widget/MediaController;

    invoke-virtual {v0}, Landroid/widget/MediaController;->show()V

    :cond_1
    :goto_1
    return v1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lco/vine/android/player/SdkVideoView;->start()V

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->k:Landroid/widget/MediaController;

    invoke-virtual {v0}, Landroid/widget/MediaController;->hide()V

    goto :goto_1

    :cond_4
    const/16 v0, 0x7e

    if-ne p1, v0, :cond_5

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lco/vine/android/player/SdkVideoView;->start()V

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->k:Landroid/widget/MediaController;

    invoke-virtual {v0}, Landroid/widget/MediaController;->hide()V

    goto :goto_1

    :cond_5
    const/16 v0, 0x56

    if-eq p1, v0, :cond_6

    const/16 v0, 0x7f

    if-ne p1, v0, :cond_7

    :cond_6
    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lco/vine/android/player/SdkVideoView;->pause()V

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->k:Landroid/widget/MediaController;

    invoke-virtual {v0}, Landroid/widget/MediaController;->show()V

    goto :goto_1

    :cond_7
    invoke-direct {p0}, Lco/vine/android/player/SdkVideoView;->h()V

    :cond_8
    invoke-super {p0, p1, p2}, Landroid/view/TextureView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 4

    iget v0, p0, Lco/vine/android/player/SdkVideoView;->e:I

    invoke-static {v0, p1}, Lco/vine/android/player/SdkVideoView;->getDefaultSize(II)I

    move-result v1

    iget v0, p0, Lco/vine/android/player/SdkVideoView;->f:I

    invoke-static {v0, p2}, Lco/vine/android/player/SdkVideoView;->getDefaultSize(II)I

    move-result v0

    iget v2, p0, Lco/vine/android/player/SdkVideoView;->e:I

    if-lez v2, :cond_0

    iget v2, p0, Lco/vine/android/player/SdkVideoView;->f:I

    if-lez v2, :cond_0

    iget v2, p0, Lco/vine/android/player/SdkVideoView;->e:I

    mul-int/2addr v2, v0

    iget v3, p0, Lco/vine/android/player/SdkVideoView;->f:I

    mul-int/2addr v3, v1

    if-le v2, v3, :cond_1

    iget v0, p0, Lco/vine/android/player/SdkVideoView;->f:I

    mul-int/2addr v0, v1

    iget v2, p0, Lco/vine/android/player/SdkVideoView;->e:I

    div-int/2addr v0, v2

    :cond_0
    :goto_0
    invoke-virtual {p0, v1, v0}, Lco/vine/android/player/SdkVideoView;->setMeasuredDimension(II)V

    return-void

    :cond_1
    iget v2, p0, Lco/vine/android/player/SdkVideoView;->e:I

    mul-int/2addr v2, v0

    iget v3, p0, Lco/vine/android/player/SdkVideoView;->f:I

    mul-int/2addr v3, v1

    if-ge v2, v3, :cond_0

    iget v1, p0, Lco/vine/android/player/SdkVideoView;->e:I

    mul-int/2addr v1, v0

    iget v2, p0, Lco/vine/android/player/SdkVideoView;->f:I

    div-int/2addr v1, v2

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-virtual {p0}, Lco/vine/android/player/SdkVideoView;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->k:Landroid/widget/MediaController;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lco/vine/android/player/SdkVideoView;->h()V

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onTrackballEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    invoke-virtual {p0}, Lco/vine/android/player/SdkVideoView;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->k:Landroid/widget/MediaController;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lco/vine/android/player/SdkVideoView;->h()V

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public pause()V
    .locals 2

    const/4 v1, 0x4

    invoke-virtual {p0}, Lco/vine/android/player/SdkVideoView;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    iput v1, p0, Lco/vine/android/player/SdkVideoView;->a:I

    :cond_0
    iput v1, p0, Lco/vine/android/player/SdkVideoView;->u:I

    return-void
.end method

.method public seekTo(I)V
    .locals 1

    invoke-virtual {p0}, Lco/vine/android/player/SdkVideoView;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->seekTo(I)V

    const/4 v0, 0x0

    iput v0, p0, Lco/vine/android/player/SdkVideoView;->q:I

    :goto_0
    return-void

    :cond_0
    iput p1, p0, Lco/vine/android/player/SdkVideoView;->q:I

    goto :goto_0
.end method

.method public setLooping(Z)V
    .locals 0

    iput-boolean p1, p0, Lco/vine/android/player/SdkVideoView;->A:Z

    return-void
.end method

.method public setMediaController(Landroid/widget/MediaController;)V
    .locals 1

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->k:Landroid/widget/MediaController;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->k:Landroid/widget/MediaController;

    invoke-virtual {v0}, Landroid/widget/MediaController;->hide()V

    :cond_0
    iput-object p1, p0, Lco/vine/android/player/SdkVideoView;->k:Landroid/widget/MediaController;

    invoke-direct {p0}, Lco/vine/android/player/SdkVideoView;->g()V

    return-void
.end method

.method public setMute(Z)V
    .locals 4

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    iput-boolean p1, p0, Lco/vine/android/player/SdkVideoView;->F:Z

    invoke-virtual {p0}, Lco/vine/android/player/SdkVideoView;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lco/vine/android/player/SdkVideoView;->G:Z

    iget-boolean v1, p0, Lco/vine/android/player/SdkVideoView;->F:Z

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lco/vine/android/player/SdkVideoView;->F:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v2, v2}, Landroid/media/MediaPlayer;->setVolume(FF)V

    :cond_0
    :goto_0
    iget-boolean v0, p0, Lco/vine/android/player/SdkVideoView;->F:Z

    iput-boolean v0, p0, Lco/vine/android/player/SdkVideoView;->G:Z

    return-void

    :cond_1
    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v3, v3}, Landroid/media/MediaPlayer;->setVolume(FF)V

    goto :goto_0
.end method

.method public setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V
    .locals 0

    iput-object p1, p0, Lco/vine/android/player/SdkVideoView;->l:Landroid/media/MediaPlayer$OnCompletionListener;

    return-void
.end method

.method public setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V
    .locals 0

    iput-object p1, p0, Lco/vine/android/player/SdkVideoView;->o:Landroid/media/MediaPlayer$OnErrorListener;

    return-void
.end method

.method public setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V
    .locals 0

    iput-object p1, p0, Lco/vine/android/player/SdkVideoView;->p:Landroid/media/MediaPlayer$OnInfoListener;

    return-void
.end method

.method public setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V
    .locals 0

    iput-object p1, p0, Lco/vine/android/player/SdkVideoView;->m:Landroid/media/MediaPlayer$OnPreparedListener;

    return-void
.end method

.method public setPlayingPosition(I)V
    .locals 0

    iput p1, p0, Lco/vine/android/player/SdkVideoView;->C:I

    return-void
.end method

.method public setVideoPath(Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lco/vine/android/player/SdkVideoView;->z:Ljava/lang/String;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lco/vine/android/player/SdkVideoView;->setVideoURI(Landroid/net/Uri;)V

    return-void
.end method

.method public start()V
    .locals 2

    const/4 v1, 0x3

    invoke-virtual {p0}, Lco/vine/android/player/SdkVideoView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lco/vine/android/player/SdkVideoView;->setVisibility(I)V

    :cond_0
    invoke-virtual {p0}, Lco/vine/android/player/SdkVideoView;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/vine/android/player/SdkVideoView;->h:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    iput v1, p0, Lco/vine/android/player/SdkVideoView;->a:I

    :cond_1
    iput v1, p0, Lco/vine/android/player/SdkVideoView;->u:I

    return-void
.end method
