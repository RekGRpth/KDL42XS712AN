.class Lco/vine/android/player/q;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# instance fields
.field final synthetic a:Lco/vine/android/player/SdkVideoView;


# direct methods
.method constructor <init>(Lco/vine/android/player/SdkVideoView;)V
    .locals 0

    iput-object p1, p0, Lco/vine/android/player/q;->a:Lco/vine/android/player/SdkVideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 5

    const/4 v4, 0x3

    const/4 v3, 0x1

    iget-object v0, p0, Lco/vine/android/player/q;->a:Lco/vine/android/player/SdkVideoView;

    const/4 v1, 0x2

    iput v1, v0, Lco/vine/android/player/SdkVideoView;->a:I

    iget-object v0, p0, Lco/vine/android/player/q;->a:Lco/vine/android/player/SdkVideoView;

    iget-object v1, p0, Lco/vine/android/player/q;->a:Lco/vine/android/player/SdkVideoView;

    iget-object v2, p0, Lco/vine/android/player/q;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v2, v3}, Lco/vine/android/player/SdkVideoView;->c(Lco/vine/android/player/SdkVideoView;Z)Z

    move-result v2

    invoke-static {v1, v2}, Lco/vine/android/player/SdkVideoView;->b(Lco/vine/android/player/SdkVideoView;Z)Z

    move-result v1

    invoke-static {v0, v1}, Lco/vine/android/player/SdkVideoView;->a(Lco/vine/android/player/SdkVideoView;Z)Z

    iget-object v0, p0, Lco/vine/android/player/q;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v0}, Lco/vine/android/player/SdkVideoView;->c(Lco/vine/android/player/SdkVideoView;)Landroid/media/MediaPlayer$OnPreparedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/vine/android/player/q;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v0}, Lco/vine/android/player/SdkVideoView;->c(Lco/vine/android/player/SdkVideoView;)Landroid/media/MediaPlayer$OnPreparedListener;

    move-result-object v0

    iget-object v1, p0, Lco/vine/android/player/q;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v1}, Lco/vine/android/player/SdkVideoView;->d(Lco/vine/android/player/SdkVideoView;)Landroid/media/MediaPlayer;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/media/MediaPlayer$OnPreparedListener;->onPrepared(Landroid/media/MediaPlayer;)V

    :cond_0
    iget-object v0, p0, Lco/vine/android/player/q;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v0}, Lco/vine/android/player/SdkVideoView;->e(Lco/vine/android/player/SdkVideoView;)Landroid/widget/MediaController;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/vine/android/player/q;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v0}, Lco/vine/android/player/SdkVideoView;->e(Lco/vine/android/player/SdkVideoView;)Landroid/widget/MediaController;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/MediaController;->setEnabled(Z)V

    :cond_1
    iget-object v0, p0, Lco/vine/android/player/q;->a:Lco/vine/android/player/SdkVideoView;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoWidth()I

    move-result v1

    invoke-static {v0, v1}, Lco/vine/android/player/SdkVideoView;->a(Lco/vine/android/player/SdkVideoView;I)I

    iget-object v0, p0, Lco/vine/android/player/q;->a:Lco/vine/android/player/SdkVideoView;

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getVideoHeight()I

    move-result v1

    invoke-static {v0, v1}, Lco/vine/android/player/SdkVideoView;->b(Lco/vine/android/player/SdkVideoView;I)I

    iget-object v0, p0, Lco/vine/android/player/q;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v0}, Lco/vine/android/player/SdkVideoView;->f(Lco/vine/android/player/SdkVideoView;)I

    move-result v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lco/vine/android/player/q;->a:Lco/vine/android/player/SdkVideoView;

    invoke-virtual {v1, v0}, Lco/vine/android/player/SdkVideoView;->seekTo(I)V

    :cond_2
    iget-object v1, p0, Lco/vine/android/player/q;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v1}, Lco/vine/android/player/SdkVideoView;->a(Lco/vine/android/player/SdkVideoView;)I

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lco/vine/android/player/q;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v1}, Lco/vine/android/player/SdkVideoView;->b(Lco/vine/android/player/SdkVideoView;)I

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lco/vine/android/player/q;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v1}, Lco/vine/android/player/SdkVideoView;->g(Lco/vine/android/player/SdkVideoView;)I

    move-result v1

    iget-object v2, p0, Lco/vine/android/player/q;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v2}, Lco/vine/android/player/SdkVideoView;->a(Lco/vine/android/player/SdkVideoView;)I

    move-result v2

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lco/vine/android/player/q;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v1}, Lco/vine/android/player/SdkVideoView;->h(Lco/vine/android/player/SdkVideoView;)I

    move-result v1

    iget-object v2, p0, Lco/vine/android/player/q;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v2}, Lco/vine/android/player/SdkVideoView;->b(Lco/vine/android/player/SdkVideoView;)I

    move-result v2

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lco/vine/android/player/q;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v1}, Lco/vine/android/player/SdkVideoView;->i(Lco/vine/android/player/SdkVideoView;)I

    move-result v1

    if-ne v1, v4, :cond_4

    iget-object v0, p0, Lco/vine/android/player/q;->a:Lco/vine/android/player/SdkVideoView;

    invoke-virtual {v0}, Lco/vine/android/player/SdkVideoView;->start()V

    iget-object v0, p0, Lco/vine/android/player/q;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v0}, Lco/vine/android/player/SdkVideoView;->e(Lco/vine/android/player/SdkVideoView;)Landroid/widget/MediaController;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lco/vine/android/player/q;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v0}, Lco/vine/android/player/SdkVideoView;->e(Lco/vine/android/player/SdkVideoView;)Landroid/widget/MediaController;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/MediaController;->show()V

    :cond_3
    :goto_0
    return-void

    :cond_4
    iget-object v1, p0, Lco/vine/android/player/q;->a:Lco/vine/android/player/SdkVideoView;

    invoke-virtual {v1}, Lco/vine/android/player/SdkVideoView;->isPlaying()Z

    move-result v1

    if-nez v1, :cond_3

    if-nez v0, :cond_5

    iget-object v0, p0, Lco/vine/android/player/q;->a:Lco/vine/android/player/SdkVideoView;

    invoke-virtual {v0}, Lco/vine/android/player/SdkVideoView;->getCurrentPosition()I

    move-result v0

    if-lez v0, :cond_3

    :cond_5
    iget-object v0, p0, Lco/vine/android/player/q;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v0}, Lco/vine/android/player/SdkVideoView;->e(Lco/vine/android/player/SdkVideoView;)Landroid/widget/MediaController;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lco/vine/android/player/q;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v0}, Lco/vine/android/player/SdkVideoView;->e(Lco/vine/android/player/SdkVideoView;)Landroid/widget/MediaController;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/MediaController;->show(I)V

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lco/vine/android/player/q;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v0}, Lco/vine/android/player/SdkVideoView;->i(Lco/vine/android/player/SdkVideoView;)I

    move-result v0

    if-ne v0, v4, :cond_3

    iget-object v0, p0, Lco/vine/android/player/q;->a:Lco/vine/android/player/SdkVideoView;

    invoke-virtual {v0}, Lco/vine/android/player/SdkVideoView;->start()V

    goto :goto_0
.end method
