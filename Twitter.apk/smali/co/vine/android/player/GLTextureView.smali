.class public Lco/vine/android/player/GLTextureView;
.super Landroid/view/TextureView;
.source "Twttr"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;
.implements Landroid/view/View$OnLayoutChangeListener;


# static fields
.field private static final a:Lco/vine/android/player/k;


# instance fields
.field private final b:Ljava/lang/ref/WeakReference;

.field private c:Lco/vine/android/player/j;

.field private d:Lco/vine/android/player/n;

.field private e:Z

.field private f:Lco/vine/android/player/f;

.field private g:Lco/vine/android/player/g;

.field private h:Lco/vine/android/player/h;

.field private i:Lco/vine/android/player/l;

.field private j:I

.field private k:I

.field private l:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lco/vine/android/player/k;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lco/vine/android/player/k;-><init>(Lco/vine/android/player/a;)V

    sput-object v0, Lco/vine/android/player/GLTextureView;->a:Lco/vine/android/player/k;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/view/TextureView;-><init>(Landroid/content/Context;)V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lco/vine/android/player/GLTextureView;->b:Ljava/lang/ref/WeakReference;

    invoke-direct {p0}, Lco/vine/android/player/GLTextureView;->c()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/view/TextureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lco/vine/android/player/GLTextureView;->b:Ljava/lang/ref/WeakReference;

    invoke-direct {p0}, Lco/vine/android/player/GLTextureView;->c()V

    return-void
.end method

.method static synthetic a(Lco/vine/android/player/GLTextureView;)I
    .locals 1

    iget v0, p0, Lco/vine/android/player/GLTextureView;->k:I

    return v0
.end method

.method static synthetic b(Lco/vine/android/player/GLTextureView;)Lco/vine/android/player/f;
    .locals 1

    iget-object v0, p0, Lco/vine/android/player/GLTextureView;->f:Lco/vine/android/player/f;

    return-object v0
.end method

.method static synthetic b()Lco/vine/android/player/k;
    .locals 1

    sget-object v0, Lco/vine/android/player/GLTextureView;->a:Lco/vine/android/player/k;

    return-object v0
.end method

.method static synthetic c(Lco/vine/android/player/GLTextureView;)Lco/vine/android/player/g;
    .locals 1

    iget-object v0, p0, Lco/vine/android/player/GLTextureView;->g:Lco/vine/android/player/g;

    return-object v0
.end method

.method private c()V
    .locals 1

    invoke-virtual {p0, p0}, Lco/vine/android/player/GLTextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    invoke-virtual {p0, p0}, Lco/vine/android/player/GLTextureView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lco/vine/android/player/GLTextureView;->setEGLContextClientVersion(I)V

    return-void
.end method

.method static synthetic d(Lco/vine/android/player/GLTextureView;)Lco/vine/android/player/h;
    .locals 1

    iget-object v0, p0, Lco/vine/android/player/GLTextureView;->h:Lco/vine/android/player/h;

    return-object v0
.end method

.method private d()V
    .locals 2

    iget-object v0, p0, Lco/vine/android/player/GLTextureView;->c:Lco/vine/android/player/j;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "setRenderer has already been called for this instance."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method static synthetic e(Lco/vine/android/player/GLTextureView;)Lco/vine/android/player/l;
    .locals 1

    iget-object v0, p0, Lco/vine/android/player/GLTextureView;->i:Lco/vine/android/player/l;

    return-object v0
.end method

.method static synthetic f(Lco/vine/android/player/GLTextureView;)I
    .locals 1

    iget v0, p0, Lco/vine/android/player/GLTextureView;->j:I

    return v0
.end method

.method static synthetic g(Lco/vine/android/player/GLTextureView;)Z
    .locals 1

    iget-boolean v0, p0, Lco/vine/android/player/GLTextureView;->l:Z

    return v0
.end method

.method static synthetic h(Lco/vine/android/player/GLTextureView;)Lco/vine/android/player/n;
    .locals 1

    iget-object v0, p0, Lco/vine/android/player/GLTextureView;->d:Lco/vine/android/player/n;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lco/vine/android/player/GLTextureView;->c:Lco/vine/android/player/j;

    invoke-virtual {v0}, Lco/vine/android/player/j;->c()V

    return-void
.end method

.method public a(Landroid/graphics/SurfaceTexture;)V
    .locals 1

    iget-object v0, p0, Lco/vine/android/player/GLTextureView;->c:Lco/vine/android/player/j;

    invoke-virtual {v0}, Lco/vine/android/player/j;->d()V

    return-void
.end method

.method public a(Landroid/graphics/SurfaceTexture;III)V
    .locals 1

    iget-object v0, p0, Lco/vine/android/player/GLTextureView;->c:Lco/vine/android/player/j;

    invoke-virtual {v0, p3, p4}, Lco/vine/android/player/j;->a(II)V

    return-void
.end method

.method public b(Landroid/graphics/SurfaceTexture;)V
    .locals 1

    iget-object v0, p0, Lco/vine/android/player/GLTextureView;->c:Lco/vine/android/player/j;

    invoke-virtual {v0}, Lco/vine/android/player/j;->e()V

    return-void
.end method

.method protected finalize()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lco/vine/android/player/GLTextureView;->c:Lco/vine/android/player/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/vine/android/player/GLTextureView;->c:Lco/vine/android/player/j;

    invoke-virtual {v0}, Lco/vine/android/player/j;->f()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    return-void

    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method public getDebugFlags()I
    .locals 1

    iget v0, p0, Lco/vine/android/player/GLTextureView;->j:I

    return v0
.end method

.method public getPreserveEGLContextOnPause()Z
    .locals 1

    iget-boolean v0, p0, Lco/vine/android/player/GLTextureView;->l:Z

    return v0
.end method

.method public getRenderMode()I
    .locals 1

    iget-object v0, p0, Lco/vine/android/player/GLTextureView;->c:Lco/vine/android/player/j;

    invoke-virtual {v0}, Lco/vine/android/player/j;->b()I

    move-result v0

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 4

    const/4 v1, 0x1

    invoke-super {p0}, Landroid/view/TextureView;->onAttachedToWindow()V

    iget-boolean v0, p0, Lco/vine/android/player/GLTextureView;->e:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/vine/android/player/GLTextureView;->d:Lco/vine/android/player/n;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/vine/android/player/GLTextureView;->c:Lco/vine/android/player/j;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lco/vine/android/player/GLTextureView;->c:Lco/vine/android/player/j;

    invoke-virtual {v0}, Lco/vine/android/player/j;->b()I

    move-result v0

    :goto_0
    new-instance v2, Lco/vine/android/player/j;

    iget-object v3, p0, Lco/vine/android/player/GLTextureView;->b:Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v3}, Lco/vine/android/player/j;-><init>(Ljava/lang/ref/WeakReference;)V

    iput-object v2, p0, Lco/vine/android/player/GLTextureView;->c:Lco/vine/android/player/j;

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lco/vine/android/player/GLTextureView;->c:Lco/vine/android/player/j;

    invoke-virtual {v1, v0}, Lco/vine/android/player/j;->a(I)V

    :cond_0
    iget-object v0, p0, Lco/vine/android/player/GLTextureView;->c:Lco/vine/android/player/j;

    invoke-virtual {v0}, Lco/vine/android/player/j;->start()V

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lco/vine/android/player/GLTextureView;->e:Z

    return-void

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    iget-object v0, p0, Lco/vine/android/player/GLTextureView;->c:Lco/vine/android/player/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/vine/android/player/GLTextureView;->c:Lco/vine/android/player/j;

    invoke-virtual {v0}, Lco/vine/android/player/j;->f()V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lco/vine/android/player/GLTextureView;->e:Z

    invoke-super {p0}, Landroid/view/TextureView;->onDetachedFromWindow()V

    return-void
.end method

.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 4

    invoke-virtual {p0}, Lco/vine/android/player/GLTextureView;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v0

    const/4 v1, 0x0

    sub-int v2, p4, p2

    sub-int v3, p5, p3

    invoke-virtual {p0, v0, v1, v2, v3}, Lco/vine/android/player/GLTextureView;->a(Landroid/graphics/SurfaceTexture;III)V

    return-void
.end method

.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 0

    invoke-virtual {p0, p1}, Lco/vine/android/player/GLTextureView;->a(Landroid/graphics/SurfaceTexture;)V

    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lco/vine/android/player/GLTextureView;->b(Landroid/graphics/SurfaceTexture;)V

    const/4 v0, 0x1

    return v0
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2, p3}, Lco/vine/android/player/GLTextureView;->a(Landroid/graphics/SurfaceTexture;III)V

    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0

    invoke-virtual {p0}, Lco/vine/android/player/GLTextureView;->a()V

    return-void
.end method

.method public setDebugFlags(I)V
    .locals 0

    iput p1, p0, Lco/vine/android/player/GLTextureView;->j:I

    return-void
.end method

.method public setEGLConfigChooser(Lco/vine/android/player/f;)V
    .locals 0

    invoke-direct {p0}, Lco/vine/android/player/GLTextureView;->d()V

    iput-object p1, p0, Lco/vine/android/player/GLTextureView;->f:Lco/vine/android/player/f;

    return-void
.end method

.method public setEGLConfigChooser(Z)V
    .locals 1

    new-instance v0, Lco/vine/android/player/o;

    invoke-direct {v0, p0, p1}, Lco/vine/android/player/o;-><init>(Lco/vine/android/player/GLTextureView;Z)V

    invoke-virtual {p0, v0}, Lco/vine/android/player/GLTextureView;->setEGLConfigChooser(Lco/vine/android/player/f;)V

    return-void
.end method

.method public setEGLContextClientVersion(I)V
    .locals 0

    invoke-direct {p0}, Lco/vine/android/player/GLTextureView;->d()V

    iput p1, p0, Lco/vine/android/player/GLTextureView;->k:I

    return-void
.end method

.method public setEGLContextFactory(Lco/vine/android/player/g;)V
    .locals 0

    invoke-direct {p0}, Lco/vine/android/player/GLTextureView;->d()V

    iput-object p1, p0, Lco/vine/android/player/GLTextureView;->g:Lco/vine/android/player/g;

    return-void
.end method

.method public setEGLWindowSurfaceFactory(Lco/vine/android/player/h;)V
    .locals 0

    invoke-direct {p0}, Lco/vine/android/player/GLTextureView;->d()V

    iput-object p1, p0, Lco/vine/android/player/GLTextureView;->h:Lco/vine/android/player/h;

    return-void
.end method

.method public setGLWrapper(Lco/vine/android/player/l;)V
    .locals 0

    iput-object p1, p0, Lco/vine/android/player/GLTextureView;->i:Lco/vine/android/player/l;

    return-void
.end method

.method public setPreserveEGLContextOnPause(Z)V
    .locals 0

    iput-boolean p1, p0, Lco/vine/android/player/GLTextureView;->l:Z

    return-void
.end method

.method public setRenderMode(I)V
    .locals 1

    iget-object v0, p0, Lco/vine/android/player/GLTextureView;->c:Lco/vine/android/player/j;

    invoke-virtual {v0, p1}, Lco/vine/android/player/j;->a(I)V

    return-void
.end method

.method public setRenderer(Lco/vine/android/player/n;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lco/vine/android/player/GLTextureView;->d()V

    iget-object v0, p0, Lco/vine/android/player/GLTextureView;->f:Lco/vine/android/player/f;

    if-nez v0, :cond_0

    new-instance v0, Lco/vine/android/player/o;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lco/vine/android/player/o;-><init>(Lco/vine/android/player/GLTextureView;Z)V

    iput-object v0, p0, Lco/vine/android/player/GLTextureView;->f:Lco/vine/android/player/f;

    :cond_0
    iget-object v0, p0, Lco/vine/android/player/GLTextureView;->g:Lco/vine/android/player/g;

    if-nez v0, :cond_1

    new-instance v0, Lco/vine/android/player/d;

    invoke-direct {v0, p0, v2}, Lco/vine/android/player/d;-><init>(Lco/vine/android/player/GLTextureView;Lco/vine/android/player/a;)V

    iput-object v0, p0, Lco/vine/android/player/GLTextureView;->g:Lco/vine/android/player/g;

    :cond_1
    iget-object v0, p0, Lco/vine/android/player/GLTextureView;->h:Lco/vine/android/player/h;

    if-nez v0, :cond_2

    new-instance v0, Lco/vine/android/player/e;

    invoke-direct {v0, v2}, Lco/vine/android/player/e;-><init>(Lco/vine/android/player/a;)V

    iput-object v0, p0, Lco/vine/android/player/GLTextureView;->h:Lco/vine/android/player/h;

    :cond_2
    iput-object p1, p0, Lco/vine/android/player/GLTextureView;->d:Lco/vine/android/player/n;

    new-instance v0, Lco/vine/android/player/j;

    iget-object v1, p0, Lco/vine/android/player/GLTextureView;->b:Ljava/lang/ref/WeakReference;

    invoke-direct {v0, v1}, Lco/vine/android/player/j;-><init>(Ljava/lang/ref/WeakReference;)V

    iput-object v0, p0, Lco/vine/android/player/GLTextureView;->c:Lco/vine/android/player/j;

    iget-object v0, p0, Lco/vine/android/player/GLTextureView;->c:Lco/vine/android/player/j;

    invoke-virtual {v0}, Lco/vine/android/player/j;->start()V

    return-void
.end method
