.class final Lgy;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/client/c;

.field final synthetic b:J

.field final synthetic c:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/twitter/android/client/c;JLandroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lgy;->a:Lcom/twitter/android/client/c;

    iput-wide p2, p0, Lgy;->b:J

    iput-object p4, p0, Lgy;->c:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x1

    iget-object v0, p0, Lgy;->a:Lcom/twitter/android/client/c;

    iget-wide v1, p0, Lgy;->b:J

    new-array v3, v5, [Ljava/lang/String;

    const-string/jumbo v4, "::tweet_to_void_dialog::accept"

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    new-instance v0, Lcom/twitter/android/FollowFlowController;

    invoke-direct {v0}, Lcom/twitter/android/FollowFlowController;-><init>()V

    sget-object v1, Lcom/twitter/android/FollowFlowController$Initiator;->b:Lcom/twitter/android/FollowFlowController$Initiator;

    invoke-virtual {v0, v1}, Lcom/twitter/android/FollowFlowController;->a(Lcom/twitter/android/FollowFlowController$Initiator;)Lcom/twitter/android/FollowFlowController;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "follow_friends"

    aput-object v2, v1, v6

    const-string/jumbo v2, "nux_tag_invite"

    aput-object v2, v1, v5

    const/4 v2, 0x2

    const-string/jumbo v3, "follow_recommendations"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/android/FollowFlowController;->a([Ljava/lang/String;)Lcom/twitter/android/FollowFlowController;

    move-result-object v0

    iget-object v1, p0, Lgy;->c:Landroid/content/Context;

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lgy;->c:Landroid/content/Context;

    const-class v4, Lcom/twitter/android/FollowActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v3, "map_contacts"

    invoke-virtual {v2, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "flow_controller"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method
