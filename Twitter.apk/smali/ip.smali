.class public Lip;
.super Lcom/twitter/library/service/o;
.source "Twttr"


# instance fields
.field private d:Lcom/twitter/library/api/ao;

.field private final e:J

.field private final f:J

.field private g:Z

.field private n:Z

.field private o:Z

.field private p:Lcom/twitter/library/api/PromotedContent;

.field private q:[I

.field private r:J

.field private s:Lcom/twitter/library/api/TwitterUser;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JJ)V
    .locals 1

    const-class v0, Lip;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    iput-wide p5, p0, Lip;->e:J

    iput-wide p3, p0, Lip;->f:J

    return-void
.end method

.method private a(Ljava/util/ArrayList;)V
    .locals 4

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/al;

    iget v2, v0, Lcom/twitter/library/api/al;->a:I

    const/16 v3, 0xfa

    if-ne v2, v3, :cond_0

    iget-wide v0, v0, Lcom/twitter/library/api/al;->c:J

    iput-wide v0, p0, Lip;->r:J

    :cond_1
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/library/service/e;)Lcom/twitter/internal/network/HttpOperation;
    .locals 8

    const/4 v6, 0x1

    iget-object v0, p0, Lip;->m:Lcom/twitter/library/network/aa;

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "1.1"

    aput-object v3, v1, v2

    const-string/jumbo v2, "friendships"

    aput-object v2, v1, v6

    const/4 v2, 0x2

    const-string/jumbo v3, "create"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "send_error_codes"

    invoke-static {v0, v1, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v3, "user_id"

    iget-wide v4, p0, Lip;->e:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-boolean v2, p0, Lip;->g:Z

    if-eqz v2, :cond_0

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v3, "follow"

    const-string/jumbo v4, "true"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-boolean v2, p0, Lip;->n:Z

    if-eqz v2, :cond_1

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v3, "lifeline"

    const-string/jumbo v4, "true"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v2, p0, Lip;->p:Lcom/twitter/library/api/PromotedContent;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lip;->p:Lcom/twitter/library/api/PromotedContent;

    iget-object v2, v2, Lcom/twitter/library/api/PromotedContent;->impressionId:Ljava/lang/String;

    if-eqz v2, :cond_2

    const-string/jumbo v2, "impression_id"

    iget-object v3, p0, Lip;->p:Lcom/twitter/library/api/PromotedContent;

    iget-object v3, v3, Lcom/twitter/library/api/PromotedContent;->impressionId:Ljava/lang/String;

    invoke-static {v0, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v2, p0, Lip;->p:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {v2}, Lcom/twitter/library/api/PromotedContent;->b()Z

    move-result v2

    if-eqz v2, :cond_3

    const-string/jumbo v2, "earned"

    invoke-static {v0, v2, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    :cond_3
    iget-boolean v2, p0, Lip;->o:Z

    if-eqz v2, :cond_4

    const-string/jumbo v2, "challenges_passed"

    invoke-static {v0, v2, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    :cond_4
    const-string/jumbo v2, "handles_challenges"

    const-string/jumbo v3, "1"

    invoke-static {v0, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const/16 v2, 0x11

    new-instance v3, Lcom/twitter/library/util/f;

    iget-object v4, p0, Lip;->l:Landroid/content/Context;

    iget-wide v5, p0, Lip;->f:J

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v4, v5, v6, v7}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    invoke-static {v2, v3}, Lcom/twitter/library/api/ao;->a(ILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v2

    iput-object v2, p0, Lip;->d:Lcom/twitter/library/api/ao;

    invoke-virtual {p0}, Lip;->s()Lcom/twitter/library/service/p;

    move-result-object v2

    new-instance v3, Lcom/twitter/library/network/d;

    iget-object v4, p0, Lip;->l:Landroid/content/Context;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v4, v0}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v4, v2, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v3, v4, v5}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v0

    sget-object v3, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v0, v3}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v0

    new-instance v3, Lcom/twitter/library/network/n;

    iget-object v2, v2, Lcom/twitter/library/service/p;->d:Lcom/twitter/library/network/OAuthToken;

    invoke-direct {v3, v2}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    invoke-virtual {v0, v3}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v0

    iget-object v1, p0, Lip;->d:Lcom/twitter/library/api/ao;

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/api/PromotedContent;)Lip;
    .locals 0

    iput-object p1, p0, Lip;->p:Lcom/twitter/library/api/PromotedContent;

    return-object p0
.end method

.method protected a(Lcom/twitter/internal/network/HttpOperation;Lcom/twitter/library/service/e;)V
    .locals 13

    const-wide/16 v11, 0x0

    const/4 v7, 0x0

    const/4 v9, 0x1

    invoke-virtual {p1}, Lcom/twitter/internal/network/HttpOperation;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lip;->l:Landroid/content/Context;

    iget-wide v1, p0, Lip;->f:J

    invoke-static {v0, v1, v2}, Lcom/twitter/library/provider/az;->a(Landroid/content/Context;J)Lcom/twitter/library/provider/az;

    move-result-object v0

    iget-object v1, p0, Lip;->d:Lcom/twitter/library/api/ao;

    invoke-virtual {v1}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/twitter/library/api/TwitterUser;

    iput-object v10, p0, Lip;->s:Lcom/twitter/library/api/TwitterUser;

    iget-boolean v1, p0, Lip;->n:Z

    if-eqz v1, :cond_1

    const/16 v1, 0x101

    :goto_0
    iget-wide v2, v10, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-virtual {v0, v2, v3, v1}, Lcom/twitter/library/provider/az;->d(JI)V

    iget-wide v1, v10, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/provider/az;->h(J)I

    move-result v1

    iput v1, v10, Lcom/twitter/library/api/TwitterUser;->friendship:I

    invoke-static {v10}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    iget-wide v2, p0, Lip;->f:J

    const/4 v4, 0x0

    const-wide/16 v5, -0x1

    move-object v8, v7

    invoke-virtual/range {v0 .. v9}, Lcom/twitter/library/provider/az;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;Z)I

    const/4 v1, 0x2

    iget-wide v2, p0, Lip;->f:J

    iget-wide v4, p0, Lip;->f:J

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/provider/az;->a(IJJ)V

    invoke-virtual {p0}, Lip;->s()Lcom/twitter/library/service/p;

    move-result-object v2

    new-instance v0, Lcom/twitter/library/api/search/a;

    iget-object v1, p0, Lip;->l:Landroid/content/Context;

    invoke-direct {v0, v1, v2, v10}, Lcom/twitter/library/api/search/a;-><init>(Landroid/content/Context;Lcom/twitter/library/service/p;Lcom/twitter/library/api/TwitterUser;)V

    invoke-virtual {p0, v0}, Lip;->b(Lcom/twitter/internal/android/service/a;)V

    new-instance v0, Ljl;

    iget-object v1, p0, Lip;->l:Landroid/content/Context;

    const/4 v8, -0x2

    move-object v3, v10

    move-wide v4, v11

    move-wide v6, v11

    invoke-direct/range {v0 .. v8}, Ljl;-><init>(Landroid/content/Context;Lcom/twitter/library/service/p;Lcom/twitter/library/api/TwitterUser;JJI)V

    invoke-virtual {v0, v9}, Ljl;->c(Z)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-virtual {p0, v0}, Lip;->b(Lcom/twitter/internal/android/service/a;)V

    :goto_1
    invoke-virtual {p2, p1}, Lcom/twitter/library/service/e;->a(Lcom/twitter/internal/network/HttpOperation;)V

    return-void

    :cond_0
    iget-object v0, p0, Lip;->d:Lcom/twitter/library/api/ao;

    invoke-virtual {v0}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-static {v0}, Lip;->c(Ljava/util/ArrayList;)[I

    move-result-object v1

    iput-object v1, p0, Lip;->q:[I

    invoke-direct {p0, v0}, Lip;->a(Ljava/util/ArrayList;)V

    goto :goto_1

    :cond_1
    move v1, v9

    goto :goto_0
.end method

.method public final e()[I
    .locals 1

    iget-object v0, p0, Lip;->q:[I

    return-object v0
.end method

.method public final f()J
    .locals 2

    iget-wide v0, p0, Lip;->r:J

    return-wide v0
.end method

.method public final g()J
    .locals 2

    iget-wide v0, p0, Lip;->e:J

    return-wide v0
.end method

.method public final h()Lcom/twitter/library/api/PromotedContent;
    .locals 1

    iget-object v0, p0, Lip;->p:Lcom/twitter/library/api/PromotedContent;

    return-object v0
.end method
