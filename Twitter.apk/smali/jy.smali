.class public final Ljy;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:Z

.field private static b:Ljava/lang/String;

.field private static c:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string/jumbo v0, "unassigned"

    sput-object v0, Ljy;->b:Ljava/lang/String;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    sput-object v0, Ljy;->c:Landroid/os/Handler;

    return-void
.end method

.method public static a(Lcom/twitter/library/view/h;)Landroid/view/View$OnClickListener;
    .locals 1

    const/4 v0, 0x1

    invoke-static {p0, v0}, Ljy;->a(Lcom/twitter/library/view/h;Z)Landroid/view/View$OnClickListener;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/twitter/library/view/h;Z)Landroid/view/View$OnClickListener;
    .locals 3

    new-instance v0, Lkg;

    const-string/jumbo v1, "android_double_tap_favorite_qster3_1650"

    new-instance v2, Lke;

    invoke-direct {v2, p0, p1}, Lke;-><init>(Lcom/twitter/library/view/h;Z)V

    invoke-direct {v0, v1, p0, v2}, Lkg;-><init>(Ljava/lang/String;Landroid/view/View$OnClickListener;Landroid/view/View$OnClickListener;)V

    return-object v0
.end method

.method public static a(Lcom/twitter/library/view/i;)Landroid/widget/AdapterView$OnItemClickListener;
    .locals 3

    new-instance v0, Lkh;

    const-string/jumbo v1, "android_double_tap_favorite_qster3_1650"

    new-instance v2, Lkf;

    invoke-direct {v2, p0}, Lkf;-><init>(Lcom/twitter/library/view/i;)V

    invoke-direct {v0, v1, p0, v2}, Lkh;-><init>(Ljava/lang/String;Landroid/widget/AdapterView$OnItemClickListener;Landroid/widget/AdapterView$OnItemClickListener;)V

    return-object v0
.end method

.method static synthetic a(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    sput-object p0, Ljy;->b:Ljava/lang/String;

    return-object p0
.end method

.method public static a()V
    .locals 1

    new-instance v0, Ljz;

    invoke-direct {v0}, Ljz;-><init>()V

    invoke-static {v0}, Lju;->a(Ljv;)V

    return-void
.end method

.method public static a(Landroid/content/Context;I)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    sget-boolean v0, Ljy;->a:Z

    if-nez v0, :cond_0

    invoke-static {}, Ljy;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "double_tap_prefs"

    invoke-virtual {p0, v0, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "double_tap_has_shown_notification"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p0, p1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    const/16 v2, 0x11

    invoke-virtual {v1, v2, v3, v3}, Landroid/widget/Toast;->setGravity(III)V

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "double_tap_has_shown_notification"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    sput-boolean v4, Ljy;->a:Z

    :cond_0
    return-void
.end method

.method public static a(Lcom/twitter/library/widget/TweetView;)V
    .locals 4

    sget-object v0, Ljy;->c:Landroid/os/Handler;

    new-instance v1, Lka;

    invoke-direct {v1, p0}, Lka;-><init>(Lcom/twitter/library/widget/TweetView;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public static b()Z
    .locals 1

    sget-object v0, Ljy;->b:Ljava/lang/String;

    invoke-static {v0}, Lkk;->d(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static c()Z
    .locals 2

    sget-object v0, Ljy;->b:Ljava/lang/String;

    const-string/jumbo v1, "double_tap_long_pause"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static d()I
    .locals 1

    invoke-static {}, Ljy;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/view/ViewConfiguration;->getDoubleTapTimeout()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0xc8

    goto :goto_0
.end method
