.class public Lhd;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/net/Uri;

.field private final b:Ljava/lang/Class;

.field private c:Landroid/os/Bundle;

.field private d:Ljava/lang/CharSequence;

.field private e:I

.field private f:Z

.field private g:Ljava/lang/String;

.field private h:I

.field private i:Z

.field private j:Z


# direct methods
.method public constructor <init>(Landroid/net/Uri;Ljava/lang/Class;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lhd;->j:Z

    iput-object p1, p0, Lhd;->a:Landroid/net/Uri;

    iput-object p2, p0, Lhd;->b:Ljava/lang/Class;

    return-void
.end method


# virtual methods
.method public a()Lhb;
    .locals 12

    new-instance v0, Lhb;

    iget-object v1, p0, Lhd;->a:Landroid/net/Uri;

    iget-object v2, p0, Lhd;->b:Ljava/lang/Class;

    iget-object v3, p0, Lhd;->c:Landroid/os/Bundle;

    iget-object v4, p0, Lhd;->d:Ljava/lang/CharSequence;

    iget v5, p0, Lhd;->e:I

    iget-boolean v6, p0, Lhd;->f:Z

    iget-object v7, p0, Lhd;->g:Ljava/lang/String;

    iget v8, p0, Lhd;->h:I

    iget-boolean v9, p0, Lhd;->i:Z

    iget-boolean v10, p0, Lhd;->j:Z

    const/4 v11, 0x0

    invoke-direct/range {v0 .. v11}, Lhb;-><init>(Landroid/net/Uri;Ljava/lang/Class;Landroid/os/Bundle;Ljava/lang/CharSequence;IZLjava/lang/String;IZZLhc;)V

    return-object v0
.end method

.method public a(I)Lhd;
    .locals 0

    iput p1, p0, Lhd;->e:I

    return-object p0
.end method

.method public a(Landroid/os/Bundle;)Lhd;
    .locals 0

    iput-object p1, p0, Lhd;->c:Landroid/os/Bundle;

    return-object p0
.end method

.method public a(Ljava/lang/CharSequence;)Lhd;
    .locals 0

    iput-object p1, p0, Lhd;->d:Ljava/lang/CharSequence;

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lhd;
    .locals 0

    iput-object p1, p0, Lhd;->g:Ljava/lang/String;

    return-object p0
.end method

.method public a(Z)Lhd;
    .locals 0

    iput-boolean p1, p0, Lhd;->i:Z

    return-object p0
.end method

.method public b(I)Lhd;
    .locals 0

    iput p1, p0, Lhd;->h:I

    return-object p0
.end method

.method public b(Z)Lhd;
    .locals 0

    iput-boolean p1, p0, Lhd;->j:Z

    return-object p0
.end method
