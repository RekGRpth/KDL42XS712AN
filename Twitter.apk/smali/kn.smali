.class public Lkn;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a()V
    .locals 1

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->I()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "android_public_conversation_1975"

    invoke-static {v0}, Lkk;->b(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static b()Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->I()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string/jumbo v2, "android_public_conversation_1975"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "reply_anatomy"

    aput-object v4, v3, v1

    const-string/jumbo v4, "reply_anatomy_w_global_composer"

    aput-object v4, v3, v0

    const/4 v4, 0x2

    const-string/jumbo v5, "reply_anatomy_w_hint"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string/jumbo v5, "full_package"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Lko;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public static c()Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->I()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "android_public_conversation_1975"

    new-array v3, v0, [Ljava/lang/String;

    const-string/jumbo v4, "reply_anatomy_w_global_composer"

    aput-object v4, v3, v1

    invoke-static {v2, v3}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static d()Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->I()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "android_public_conversation_1975"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "reply_anatomy_w_hint"

    aput-object v4, v3, v1

    const-string/jumbo v4, "full_package"

    aput-object v4, v3, v0

    invoke-static {v2, v3}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static e()Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->I()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string/jumbo v2, "android_public_conversation_1975"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "realtime"

    aput-object v4, v3, v1

    const-string/jumbo v4, "full_package"

    aput-object v4, v3, v0

    invoke-static {v2, v3}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Lko;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public static f()Z
    .locals 1

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->J()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lkn;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g()Z
    .locals 1

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->K()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lkn;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static h()Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->I()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "android_public_conversation_1975"

    new-array v3, v0, [Ljava/lang/String;

    const-string/jumbo v4, "unassigned"

    aput-object v4, v3, v1

    invoke-static {v2, v3}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method
