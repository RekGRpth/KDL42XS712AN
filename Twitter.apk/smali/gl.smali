.class public Lgl;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Z)I
    .locals 1

    if-eqz p0, :cond_0

    invoke-static {}, Lgl;->j()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const v0, 0x7f020201    # com.twitter.android.R.drawable.ic_profile_fav_people_active

    :goto_0
    return v0

    :pswitch_0
    const v0, 0x7f020203    # com.twitter.android.R.drawable.ic_profile_favheart_active

    goto :goto_0

    :pswitch_1
    const v0, 0x7f020205    # com.twitter.android.R.drawable.ic_profile_favsmile_active

    goto :goto_0

    :cond_0
    invoke-static {}, Lgl;->j()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    const v0, 0x7f020202    # com.twitter.android.R.drawable.ic_profile_fav_people_default

    goto :goto_0

    :pswitch_2
    const v0, 0x7f020204    # com.twitter.android.R.drawable.ic_profile_favheart_default

    goto :goto_0

    :pswitch_3
    const v0, 0x7f020206    # com.twitter.android.R.drawable.ic_profile_favsmile_default

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a()Z
    .locals 1

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->ay()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lgl;->i()V

    const-string/jumbo v0, "android_favorite_people_timeline_u4_1989"

    invoke-static {v0}, Lkk;->c(Ljava/lang/String;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Z)I
    .locals 2

    const v0, 0x7f020079    # com.twitter.android.R.drawable.btn_fav_people_bg_default

    if-eqz p0, :cond_0

    invoke-static {}, Lgl;->j()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    return v0

    :pswitch_0
    const v0, 0x7f020078    # com.twitter.android.R.drawable.btn_fav_people_bg_active_yellow

    goto :goto_0

    :pswitch_1
    const v0, 0x7f020077    # com.twitter.android.R.drawable.btn_fav_people_bg_active_red

    goto :goto_0

    :pswitch_2
    const v0, 0x7f020076    # com.twitter.android.R.drawable.btn_fav_people_bg_active_blue

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static b()Z
    .locals 2

    const/4 v0, 0x1

    invoke-static {}, Lgl;->k()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c()I
    .locals 2

    const v0, 0x7f02007e    # com.twitter.android.R.drawable.btn_favorite_action

    invoke-static {}, Lgl;->j()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    const v0, 0x7f02007c    # com.twitter.android.R.drawable.btn_favheart_action

    goto :goto_0

    :pswitch_2
    const v0, 0x7f020080    # com.twitter.android.R.drawable.btn_favsmile_action

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static d()I
    .locals 1

    invoke-static {}, Lgl;->j()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const v0, 0x7f02007f    # com.twitter.android.R.drawable.btn_favorite_action_bg

    :goto_0
    return v0

    :pswitch_0
    const v0, 0x7f020082    # com.twitter.android.R.drawable.btn_favstar_action_bg

    goto :goto_0

    :pswitch_1
    const v0, 0x7f02007d    # com.twitter.android.R.drawable.btn_favheart_action_bg

    goto :goto_0

    :pswitch_2
    const v0, 0x7f020081    # com.twitter.android.R.drawable.btn_favsmile_action_bg

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static e()I
    .locals 1

    invoke-static {}, Lgl;->k()I

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lgl;->j()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const v0, 0x7f0f0149    # com.twitter.android.R.string.empty_favorite_people_desc_star

    :goto_0
    return v0

    :pswitch_0
    const v0, 0x7f0f0144    # com.twitter.android.R.string.empty_favorite_people_desc_heart

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0f0148    # com.twitter.android.R.string.empty_favorite_people_desc_smiley

    goto :goto_0

    :cond_0
    invoke-static {}, Lgl;->j()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    const v0, 0x7f0f0147    # com.twitter.android.R.string.empty_favorite_people_desc_no_notifications_star

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0f0145    # com.twitter.android.R.string.empty_favorite_people_desc_no_notifications_heart

    goto :goto_0

    :pswitch_3
    const v0, 0x7f0f0146    # com.twitter.android.R.string.empty_favorite_people_desc_no_notifications_smiley

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static f()I
    .locals 1

    invoke-static {}, Lgl;->j()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const v0, 0x7f0200db    # com.twitter.android.R.drawable.fave_people_nux_star

    :goto_0
    return v0

    :pswitch_0
    const v0, 0x7f0200d9    # com.twitter.android.R.drawable.fave_people_nux_heart

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0200da    # com.twitter.android.R.drawable.fave_people_nux_smiley

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static g()I
    .locals 1

    invoke-static {}, Lgl;->k()I

    move-result v0

    if-nez v0, :cond_0

    const v0, 0x7f0f0544    # com.twitter.android.R.string.users_tweet_notifications_dialog_message_with_timeline

    :goto_0
    return v0

    :cond_0
    const v0, 0x7f0f0545    # com.twitter.android.R.string.users_tweet_notifications_dialog_message_with_timeline_no_default

    goto :goto_0
.end method

.method public static h()I
    .locals 1

    invoke-static {}, Lgl;->k()I

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lgl;->j()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const v0, 0x7f0f0408    # com.twitter.android.R.string.settings_notif_tweets_summary_favorite_people

    :goto_0
    return v0

    :pswitch_0
    const v0, 0x7f0f0409    # com.twitter.android.R.string.settings_notif_tweets_summary_favorite_people_heart

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0f040d    # com.twitter.android.R.string.settings_notif_tweets_summary_favorite_people_smiley

    goto :goto_0

    :cond_0
    invoke-static {}, Lgl;->j()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    const v0, 0x7f0f040a    # com.twitter.android.R.string.settings_notif_tweets_summary_favorite_people_no_default

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0f040b    # com.twitter.android.R.string.settings_notif_tweets_summary_favorite_people_no_default_heart

    goto :goto_0

    :pswitch_3
    const v0, 0x7f0f040c    # com.twitter.android.R.string.settings_notif_tweets_summary_favorite_people_no_default_smiley

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static i()V
    .locals 1

    const-string/jumbo v0, "android_favorite_people_timeline_u4_1989"

    invoke-static {v0}, Lkk;->b(Ljava/lang/String;)V

    return-void
.end method

.method private static j()I
    .locals 7

    const/4 v2, 0x3

    const/4 v1, 0x2

    const/4 v0, 0x1

    const/4 v3, 0x0

    invoke-static {}, Lgl;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    const-string/jumbo v4, "android_favorite_people_timeline_u4_1989"

    invoke-static {v4}, Lkk;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v4, -0x1

    invoke-virtual {v5}, Ljava/lang/String;->hashCode()I

    move-result v6

    sparse-switch v6, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v4, :pswitch_data_0

    move v0, v3

    :goto_1
    :pswitch_0
    return v0

    :sswitch_0
    const-string/jumbo v6, "star_icon_default_notif"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    move v4, v3

    goto :goto_0

    :sswitch_1
    const-string/jumbo v6, "star_icon_no_notif"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    move v4, v0

    goto :goto_0

    :sswitch_2
    const-string/jumbo v6, "heart_icon_default_notif"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    move v4, v1

    goto :goto_0

    :sswitch_3
    const-string/jumbo v6, "heart_icon_no_notif"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    move v4, v2

    goto :goto_0

    :sswitch_4
    const-string/jumbo v6, "smiley_icon_default_notif"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v4, 0x4

    goto :goto_0

    :sswitch_5
    const-string/jumbo v6, "smiley_icon_no_notif"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    const/4 v4, 0x5

    goto :goto_0

    :pswitch_1
    move v0, v1

    goto :goto_1

    :pswitch_2
    move v0, v2

    goto :goto_1

    :cond_1
    move v0, v3

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        -0x43b506f5 -> :sswitch_1
        -0x21e9b0fb -> :sswitch_2
        -0x1d547a87 -> :sswitch_0
        -0x1452d06 -> :sswitch_4
        0x23cf7eea -> :sswitch_5
        0x3b5367ff -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private static k()I
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {}, Lgl;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string/jumbo v2, "android_favorite_people_timeline_u4_1989"

    invoke-static {v2}, Lkk;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v2, -0x1

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v2, :pswitch_data_0

    move v0, v1

    :goto_1
    :pswitch_0
    return v0

    :sswitch_0
    const-string/jumbo v4, "star_icon_no_notif"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v2, v1

    goto :goto_0

    :sswitch_1
    const-string/jumbo v4, "heart_icon_no_notif"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move v2, v0

    goto :goto_0

    :sswitch_2
    const-string/jumbo v4, "smiley_icon_no_notif"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v2, 0x2

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x43b506f5 -> :sswitch_0
        0x23cf7eea -> :sswitch_2
        0x3b5367ff -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
