.class public Lmc;
.super Ljava/lang/Object;

# interfaces
.implements Lorg/spongycastle/crypto/a;


# static fields
.field private static a:Ljava/math/BigInteger;


# instance fields
.field private b:Lmd;

.field private c:Lmi;

.field private d:Ljava/security/SecureRandom;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-wide/16 v0, 0x1

    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    sput-object v0, Lmc;->a:Ljava/math/BigInteger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lmd;

    invoke-direct {v0}, Lmd;-><init>()V

    iput-object v0, p0, Lmc;->b:Lmd;

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget-object v0, p0, Lmc;->b:Lmd;

    invoke-virtual {v0}, Lmd;->a()I

    move-result v0

    return v0
.end method

.method public a(ZLorg/spongycastle/crypto/c;)V
    .locals 1

    iget-object v0, p0, Lmc;->b:Lmd;

    invoke-virtual {v0, p1, p2}, Lmd;->a(ZLorg/spongycastle/crypto/c;)V

    instance-of v0, p2, Lmg;

    if-eqz v0, :cond_0

    check-cast p2, Lmg;

    invoke-virtual {p2}, Lmg;->b()Lorg/spongycastle/crypto/c;

    move-result-object v0

    check-cast v0, Lmi;

    iput-object v0, p0, Lmc;->c:Lmi;

    invoke-virtual {p2}, Lmg;->a()Ljava/security/SecureRandom;

    move-result-object v0

    iput-object v0, p0, Lmc;->d:Ljava/security/SecureRandom;

    :goto_0
    return-void

    :cond_0
    check-cast p2, Lmi;

    iput-object p2, p0, Lmc;->c:Lmi;

    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    iput-object v0, p0, Lmc;->d:Ljava/security/SecureRandom;

    goto :goto_0
.end method

.method public a([BII)[B
    .locals 6

    iget-object v0, p0, Lmc;->c:Lmi;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "RSA engine not initialised"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lmc;->b:Lmd;

    invoke-virtual {v0, p1, p2, p3}, Lmd;->a([BII)Ljava/math/BigInteger;

    move-result-object v1

    iget-object v0, p0, Lmc;->c:Lmi;

    instance-of v0, v0, Lmj;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmc;->c:Lmi;

    check-cast v0, Lmj;

    invoke-virtual {v0}, Lmj;->d()Ljava/math/BigInteger;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Lmj;->b()Ljava/math/BigInteger;

    move-result-object v0

    sget-object v3, Lmc;->a:Ljava/math/BigInteger;

    sget-object v4, Lmc;->a:Ljava/math/BigInteger;

    invoke-virtual {v0, v4}, Ljava/math/BigInteger;->subtract(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v4

    iget-object v5, p0, Lmc;->d:Ljava/security/SecureRandom;

    invoke-static {v3, v4, v5}, Lmp;->a(Ljava/math/BigInteger;Ljava/math/BigInteger;Ljava/security/SecureRandom;)Ljava/math/BigInteger;

    move-result-object v3

    invoke-virtual {v3, v2, v0}, Ljava/math/BigInteger;->modPow(Ljava/math/BigInteger;Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/math/BigInteger;->multiply(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/math/BigInteger;->mod(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v1

    iget-object v2, p0, Lmc;->b:Lmd;

    invoke-virtual {v2, v1}, Lmd;->b(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v3, v0}, Ljava/math/BigInteger;->modInverse(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/math/BigInteger;->multiply(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/math/BigInteger;->mod(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lmc;->b:Lmd;

    invoke-virtual {v1, v0}, Lmd;->a(Ljava/math/BigInteger;)[B

    move-result-object v0

    return-object v0

    :cond_1
    iget-object v0, p0, Lmc;->b:Lmd;

    invoke-virtual {v0, v1}, Lmd;->b(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lmc;->b:Lmd;

    invoke-virtual {v0, v1}, Lmd;->b(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    iget-object v0, p0, Lmc;->b:Lmd;

    invoke-virtual {v0}, Lmd;->b()I

    move-result v0

    return v0
.end method
