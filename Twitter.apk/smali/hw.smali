.class public Lhw;
.super Ljava/io/InputStream;
.source "Twttr"


# static fields
.field private static final a:Lcom/twitter/internal/util/m;


# instance fields
.field private final b:Ljava/io/InputStream;

.field private c:J

.field private d:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lcom/twitter/internal/util/i;->c()Lcom/twitter/internal/util/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/util/i;->b()Lcom/twitter/internal/util/m;

    move-result-object v0

    sput-object v0, Lhw;->a:Lcom/twitter/internal/util/m;

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 0

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    iput-object p1, p0, Lhw;->b:Ljava/io/InputStream;

    return-void
.end method


# virtual methods
.method public a()J
    .locals 4

    iget-wide v0, p0, Lhw;->c:J

    const-wide/32 v2, 0xf4240

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public available()I
    .locals 1

    iget-object v0, p0, Lhw;->b:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v0

    return v0
.end method

.method public b()J
    .locals 2

    iget-wide v0, p0, Lhw;->d:J

    return-wide v0
.end method

.method public close()V
    .locals 1

    iget-object v0, p0, Lhw;->b:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    return-void
.end method

.method public mark(I)V
    .locals 1

    iget-object v0, p0, Lhw;->b:Ljava/io/InputStream;

    invoke-virtual {v0, p1}, Ljava/io/InputStream;->mark(I)V

    return-void
.end method

.method public markSupported()Z
    .locals 1

    iget-object v0, p0, Lhw;->b:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->markSupported()Z

    move-result v0

    return v0
.end method

.method public read()I
    .locals 7

    sget-object v0, Lhw;->a:Lcom/twitter/internal/util/m;

    invoke-interface {v0}, Lcom/twitter/internal/util/m;->b()J

    move-result-wide v0

    iget-object v2, p0, Lhw;->b:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->read()I

    move-result v2

    iget-wide v3, p0, Lhw;->c:J

    sget-object v5, Lhw;->a:Lcom/twitter/internal/util/m;

    invoke-interface {v5}, Lcom/twitter/internal/util/m;->b()J

    move-result-wide v5

    sub-long v0, v5, v0

    add-long/2addr v0, v3

    iput-wide v0, p0, Lhw;->c:J

    const/4 v0, -0x1

    if-eq v2, v0, :cond_0

    iget-wide v0, p0, Lhw;->d:J

    const-wide/16 v3, 0x1

    add-long/2addr v0, v3

    iput-wide v0, p0, Lhw;->d:J

    :cond_0
    return v2
.end method

.method public read([B)I
    .locals 7

    sget-object v0, Lhw;->a:Lcom/twitter/internal/util/m;

    invoke-interface {v0}, Lcom/twitter/internal/util/m;->b()J

    move-result-wide v0

    iget-object v2, p0, Lhw;->b:Ljava/io/InputStream;

    invoke-virtual {v2, p1}, Ljava/io/InputStream;->read([B)I

    move-result v2

    iget-wide v3, p0, Lhw;->c:J

    sget-object v5, Lhw;->a:Lcom/twitter/internal/util/m;

    invoke-interface {v5}, Lcom/twitter/internal/util/m;->b()J

    move-result-wide v5

    sub-long v0, v5, v0

    add-long/2addr v0, v3

    iput-wide v0, p0, Lhw;->c:J

    const/4 v0, -0x1

    if-eq v2, v0, :cond_0

    iget-wide v0, p0, Lhw;->d:J

    int-to-long v3, v2

    add-long/2addr v0, v3

    iput-wide v0, p0, Lhw;->d:J

    :cond_0
    return v2
.end method

.method public read([BII)I
    .locals 7

    sget-object v0, Lhw;->a:Lcom/twitter/internal/util/m;

    invoke-interface {v0}, Lcom/twitter/internal/util/m;->b()J

    move-result-wide v0

    iget-object v2, p0, Lhw;->b:Ljava/io/InputStream;

    invoke-virtual {v2, p1, p2, p3}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    iget-wide v3, p0, Lhw;->c:J

    sget-object v5, Lhw;->a:Lcom/twitter/internal/util/m;

    invoke-interface {v5}, Lcom/twitter/internal/util/m;->b()J

    move-result-wide v5

    sub-long v0, v5, v0

    add-long/2addr v0, v3

    iput-wide v0, p0, Lhw;->c:J

    const/4 v0, -0x1

    if-eq v2, v0, :cond_0

    iget-wide v0, p0, Lhw;->d:J

    int-to-long v3, v2

    add-long/2addr v0, v3

    iput-wide v0, p0, Lhw;->d:J

    :cond_0
    return v2
.end method

.method public declared-synchronized reset()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhw;->b:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->reset()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public skip(J)J
    .locals 8

    sget-object v0, Lhw;->a:Lcom/twitter/internal/util/m;

    invoke-interface {v0}, Lcom/twitter/internal/util/m;->b()J

    move-result-wide v0

    invoke-super {p0, p1, p2}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v2

    iget-wide v4, p0, Lhw;->c:J

    sget-object v6, Lhw;->a:Lcom/twitter/internal/util/m;

    invoke-interface {v6}, Lcom/twitter/internal/util/m;->b()J

    move-result-wide v6

    sub-long v0, v6, v0

    add-long/2addr v0, v4

    iput-wide v0, p0, Lhw;->c:J

    const-wide/16 v0, -0x1

    cmp-long v0, v2, v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lhw;->d:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lhw;->d:J

    :cond_0
    return-wide v2
.end method
