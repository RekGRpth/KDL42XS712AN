.class public Lcom/android/calendar/agenda/AgendaAdapter;
.super Landroid/widget/ResourceCursorAdapter;
.source "AgendaAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private COLOR_CHIP_ALL_DAY_HEIGHT:I

.field private COLOR_CHIP_HEIGHT:I

.field private final mDeclinedColor:I

.field private final mFormatter:Ljava/util/Formatter;

.field private final mNoTitleLabel:Ljava/lang/String;

.field private final mResources:Landroid/content/res/Resources;

.field private mScale:F

.field private final mStandardColor:I

.field private final mStringBuilder:Ljava/lang/StringBuilder;

.field private final mTZUpdater:Ljava/lang/Runnable;

.field private final mWhereColor:I

.field private final mWhereDeclinedColor:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # I

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/widget/ResourceCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;)V

    new-instance v0, Lcom/android/calendar/agenda/AgendaAdapter$1;

    invoke-direct {v0, p0}, Lcom/android/calendar/agenda/AgendaAdapter$1;-><init>(Lcom/android/calendar/agenda/AgendaAdapter;)V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaAdapter;->mTZUpdater:Ljava/lang/Runnable;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaAdapter;->mResources:Landroid/content/res/Resources;

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaAdapter;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0c000c    # com.android.calendar.R.string.no_title_label

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaAdapter;->mNoTitleLabel:Ljava/lang/String;

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaAdapter;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f080031    # com.android.calendar.R.color.agenda_item_declined_color

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/agenda/AgendaAdapter;->mDeclinedColor:I

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaAdapter;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f080032    # com.android.calendar.R.color.agenda_item_standard_color

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/agenda/AgendaAdapter;->mStandardColor:I

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaAdapter;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f080039    # com.android.calendar.R.color.agenda_item_where_declined_text_color

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/agenda/AgendaAdapter;->mWhereDeclinedColor:I

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaAdapter;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f080038    # com.android.calendar.R.color.agenda_item_where_text_color

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/agenda/AgendaAdapter;->mWhereColor:I

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaAdapter;->mStringBuilder:Ljava/lang/StringBuilder;

    new-instance v0, Ljava/util/Formatter;

    iget-object v1, p0, Lcom/android/calendar/agenda/AgendaAdapter;->mStringBuilder:Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/android/calendar/agenda/AgendaAdapter;->mFormatter:Ljava/util/Formatter;

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaAdapter;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0b0006    # com.android.calendar.R.integer.color_chip_all_day_height

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/agenda/AgendaAdapter;->COLOR_CHIP_ALL_DAY_HEIGHT:I

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaAdapter;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f0b0007    # com.android.calendar.R.integer.color_chip_height

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/agenda/AgendaAdapter;->COLOR_CHIP_HEIGHT:I

    iget v0, p0, Lcom/android/calendar/agenda/AgendaAdapter;->mScale:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/agenda/AgendaAdapter;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/android/calendar/agenda/AgendaAdapter;->mScale:F

    iget v0, p0, Lcom/android/calendar/agenda/AgendaAdapter;->mScale:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/android/calendar/agenda/AgendaAdapter;->COLOR_CHIP_ALL_DAY_HEIGHT:I

    int-to-float v0, v0

    iget v1, p0, Lcom/android/calendar/agenda/AgendaAdapter;->mScale:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/android/calendar/agenda/AgendaAdapter;->COLOR_CHIP_ALL_DAY_HEIGHT:I

    iget v0, p0, Lcom/android/calendar/agenda/AgendaAdapter;->COLOR_CHIP_HEIGHT:I

    int-to-float v0, v0

    iget v1, p0, Lcom/android/calendar/agenda/AgendaAdapter;->mScale:F

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/android/calendar/agenda/AgendaAdapter;->COLOR_CHIP_HEIGHT:I

    :cond_0
    return-void
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 29
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/content/Context;
    .param p3    # Landroid/database/Cursor;

    const/16 v16, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v21

    move-object/from16 v0, v21

    instance-of v2, v0, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;

    if-eqz v2, :cond_0

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;

    :cond_0
    if-nez v16, :cond_1

    new-instance v16, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;

    invoke-direct/range {v16 .. v16}, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;-><init>()V

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    const v2, 0x7f100013    # com.android.calendar.R.id.title

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    move-object/from16 v0, v16

    iput-object v2, v0, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->title:Landroid/widget/TextView;

    const v2, 0x7f100014    # com.android.calendar.R.id.when

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    move-object/from16 v0, v16

    iput-object v2, v0, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->when:Landroid/widget/TextView;

    const v2, 0x7f100015    # com.android.calendar.R.id.where

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    move-object/from16 v0, v16

    iput-object v2, v0, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->where:Landroid/widget/TextView;

    const v2, 0x7f100012    # com.android.calendar.R.id.agenda_item_text_container

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    move-object/from16 v0, v16

    iput-object v2, v0, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->textContainer:Landroid/widget/LinearLayout;

    const v2, 0x7f100016    # com.android.calendar.R.id.selected_marker

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, v16

    iput-object v2, v0, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->selectedMarker:Landroid/view/View;

    const v2, 0x7f100011    # com.android.calendar.R.id.agenda_item_color

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/ColorChipView;

    move-object/from16 v0, v16

    iput-object v2, v0, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->colorChip:Lcom/android/calendar/ColorChipView;

    :cond_1
    const/4 v2, 0x7

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    move-object/from16 v0, v16

    iput-wide v2, v0, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->startTimeMilli:J

    const/4 v2, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_8

    const/4 v10, 0x1

    :goto_0
    move-object/from16 v0, v16

    iput-boolean v10, v0, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->allDay:Z

    const/16 v2, 0xc

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v20

    const/4 v2, 0x2

    move/from16 v0, v20

    if-ne v0, v2, :cond_9

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->title:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/agenda/AgendaAdapter;->mDeclinedColor:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->when:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/agenda/AgendaAdapter;->mWhereDeclinedColor:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->where:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/agenda/AgendaAdapter;->mWhereDeclinedColor:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->colorChip:Lcom/android/calendar/ColorChipView;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/android/calendar/ColorChipView;->setDrawStyle(I)V

    :goto_1
    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->colorChip:Lcom/android/calendar/ColorChipView;

    invoke-virtual {v2}, Lcom/android/calendar/ColorChipView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v19

    if-eqz v10, :cond_b

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/agenda/AgendaAdapter;->COLOR_CHIP_ALL_DAY_HEIGHT:I

    move-object/from16 v0, v19

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    :goto_2
    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->colorChip:Lcom/android/calendar/ColorChipView;

    move-object/from16 v0, v19

    invoke-virtual {v2, v0}, Lcom/android/calendar/ColorChipView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/16 v2, 0xf

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    if-nez v11, :cond_2

    const/16 v2, 0xe

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    const/16 v2, 0xd

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->colorChip:Lcom/android/calendar/ColorChipView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/android/calendar/ColorChipView;->setDrawStyle(I)V

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->title:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/agenda/AgendaAdapter;->mStandardColor:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->when:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/agenda/AgendaAdapter;->mStandardColor:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->where:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/agenda/AgendaAdapter;->mStandardColor:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_2
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->title:Landroid/widget/TextView;

    move-object/from16 v22, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->when:Landroid/widget/TextView;

    move-object/from16 v25, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->where:Landroid/widget/TextView;

    move-object/from16 v27, v0

    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    move-object/from16 v0, v16

    iput-wide v2, v0, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->instanceId:J

    const/4 v2, 0x5

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Lcom/android/calendar/Utils;->getDisplayColorFromColor(I)I

    move-result v12

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->colorChip:Lcom/android/calendar/ColorChipView;

    invoke-virtual {v2, v12}, Lcom/android/calendar/ColorChipView;->setColor(I)V

    const/4 v2, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v23

    if-eqz v23, :cond_3

    invoke-virtual/range {v23 .. v23}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_4

    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/agenda/AgendaAdapter;->mNoTitleLabel:Ljava/lang/String;

    move-object/from16 v23, v0

    :cond_4
    invoke-virtual/range {v22 .. v23}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v2, 0x7

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const/16 v2, 0x8

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    const/16 v2, 0x10

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaAdapter;->mTZUpdater:Ljava/lang/Runnable;

    move-object/from16 v0, p2

    invoke-static {v0, v2}, Lcom/android/calendar/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v9

    if-eqz v10, :cond_c

    const-string v9, "UTC"

    :goto_3
    invoke-static/range {p2 .. p2}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_5

    or-int/lit16 v8, v8, 0x80

    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaAdapter;->mStringBuilder:Ljava/lang/StringBuilder;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/agenda/AgendaAdapter;->mFormatter:Ljava/util/Formatter;

    move-object/from16 v2, p2

    invoke-static/range {v2 .. v9}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;Ljava/util/Formatter;JJILjava/lang/String;)Ljava/util/Formatter;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v26

    if-nez v10, :cond_7

    invoke-static {v9, v15}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    new-instance v13, Landroid/text/format/Time;

    invoke-direct {v13, v9}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13, v4, v5}, Landroid/text/format/Time;->set(J)V

    invoke-static {v9}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v24

    if-eqz v24, :cond_6

    invoke-virtual/range {v24 .. v24}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v2

    const-string v3, "GMT"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    :cond_6
    move-object v14, v9

    :goto_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v26

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v26

    :cond_7
    invoke-virtual/range {v25 .. v26}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v2, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v28

    if-eqz v28, :cond_f

    invoke-virtual/range {v28 .. v28}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_f

    const/4 v2, 0x0

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual/range {v27 .. v28}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_5
    return-void

    :cond_8
    const/4 v10, 0x0

    goto/16 :goto_0

    :cond_9
    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->title:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/agenda/AgendaAdapter;->mStandardColor:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->when:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/agenda/AgendaAdapter;->mWhereColor:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->where:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/agenda/AgendaAdapter;->mWhereColor:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    const/4 v2, 0x3

    move/from16 v0, v20

    if-ne v0, v2, :cond_a

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->colorChip:Lcom/android/calendar/ColorChipView;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/android/calendar/ColorChipView;->setDrawStyle(I)V

    goto/16 :goto_1

    :cond_a
    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/android/calendar/agenda/AgendaAdapter$ViewHolder;->colorChip:Lcom/android/calendar/ColorChipView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/android/calendar/ColorChipView;->setDrawStyle(I)V

    goto/16 :goto_1

    :cond_b
    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/agenda/AgendaAdapter;->COLOR_CHIP_HEIGHT:I

    move-object/from16 v0, v19

    iput v2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto/16 :goto_2

    :cond_c
    const/4 v8, 0x1

    goto/16 :goto_3

    :cond_d
    iget v2, v13, Landroid/text/format/Time;->isDst:I

    if-eqz v2, :cond_e

    const/4 v2, 0x1

    :goto_6
    const/4 v3, 0x0

    move-object/from16 v0, v24

    invoke-virtual {v0, v2, v3}, Ljava/util/TimeZone;->getDisplayName(ZI)Ljava/lang/String;

    move-result-object v14

    goto/16 :goto_4

    :cond_e
    const/4 v2, 0x0

    goto :goto_6

    :cond_f
    const/16 v2, 0x8

    move-object/from16 v0, v27

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_5
.end method
