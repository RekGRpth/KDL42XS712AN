.class Lcom/android/calendar/alerts/AlertService$NotificationInfo;
.super Ljava/lang/Object;
.source "AlertService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/calendar/alerts/AlertService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "NotificationInfo"
.end annotation


# instance fields
.field allDay:Z

.field description:Ljava/lang/String;

.field endMillis:J

.field eventId:J

.field eventName:Ljava/lang/String;

.field location:Ljava/lang/String;

.field newAlert:Z

.field startMillis:J


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJZZ)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # J
    .param p6    # J
    .param p8    # J
    .param p10    # Z
    .param p11    # Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/android/calendar/alerts/AlertService$NotificationInfo;->eventName:Ljava/lang/String;

    iput-object p2, p0, Lcom/android/calendar/alerts/AlertService$NotificationInfo;->location:Ljava/lang/String;

    iput-object p3, p0, Lcom/android/calendar/alerts/AlertService$NotificationInfo;->description:Ljava/lang/String;

    iput-wide p4, p0, Lcom/android/calendar/alerts/AlertService$NotificationInfo;->startMillis:J

    iput-wide p6, p0, Lcom/android/calendar/alerts/AlertService$NotificationInfo;->endMillis:J

    iput-wide p8, p0, Lcom/android/calendar/alerts/AlertService$NotificationInfo;->eventId:J

    iput-boolean p11, p0, Lcom/android/calendar/alerts/AlertService$NotificationInfo;->newAlert:Z

    iput-boolean p10, p0, Lcom/android/calendar/alerts/AlertService$NotificationInfo;->allDay:Z

    return-void
.end method
