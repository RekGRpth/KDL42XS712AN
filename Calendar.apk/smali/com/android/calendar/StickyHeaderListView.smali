.class public Lcom/android/calendar/StickyHeaderListView;
.super Landroid/widget/FrameLayout;
.source "StickyHeaderListView.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/calendar/StickyHeaderListView$HeaderHeightListener;,
        Lcom/android/calendar/StickyHeaderListView$HeaderIndexer;
    }
.end annotation


# instance fields
.field protected mAdapter:Landroid/widget/Adapter;

.field protected mChildViewsCreated:Z

.field protected mContext:Landroid/content/Context;

.field protected mCurrentSectionPos:I

.field protected mDoHeaderReset:Z

.field protected mDummyHeader:Landroid/view/View;

.field protected mHeaderHeightListener:Lcom/android/calendar/StickyHeaderListView$HeaderHeightListener;

.field protected mIndexer:Lcom/android/calendar/StickyHeaderListView$HeaderIndexer;

.field private mLastStickyHeaderHeight:I

.field protected mListView:Landroid/widget/ListView;

.field protected mListViewHeadersCount:I

.field protected mListener:Landroid/widget/AbsListView$OnScrollListener;

.field protected mNextSectionPosition:I

.field private mSeparatorView:Landroid/view/View;

.field private mSeparatorWidth:I

.field protected mStickyHeader:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v2, -0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-boolean v1, p0, Lcom/android/calendar/StickyHeaderListView;->mChildViewsCreated:Z

    iput-boolean v1, p0, Lcom/android/calendar/StickyHeaderListView;->mDoHeaderReset:Z

    iput-object v0, p0, Lcom/android/calendar/StickyHeaderListView;->mContext:Landroid/content/Context;

    iput-object v0, p0, Lcom/android/calendar/StickyHeaderListView;->mAdapter:Landroid/widget/Adapter;

    iput-object v0, p0, Lcom/android/calendar/StickyHeaderListView;->mIndexer:Lcom/android/calendar/StickyHeaderListView$HeaderIndexer;

    iput-object v0, p0, Lcom/android/calendar/StickyHeaderListView;->mHeaderHeightListener:Lcom/android/calendar/StickyHeaderListView$HeaderHeightListener;

    iput-object v0, p0, Lcom/android/calendar/StickyHeaderListView;->mStickyHeader:Landroid/view/View;

    iput-object v0, p0, Lcom/android/calendar/StickyHeaderListView;->mDummyHeader:Landroid/view/View;

    iput-object v0, p0, Lcom/android/calendar/StickyHeaderListView;->mListView:Landroid/widget/ListView;

    iput-object v0, p0, Lcom/android/calendar/StickyHeaderListView;->mListener:Landroid/widget/AbsListView$OnScrollListener;

    iput v1, p0, Lcom/android/calendar/StickyHeaderListView;->mLastStickyHeaderHeight:I

    iput v2, p0, Lcom/android/calendar/StickyHeaderListView;->mCurrentSectionPos:I

    iput v2, p0, Lcom/android/calendar/StickyHeaderListView;->mNextSectionPosition:I

    iput v1, p0, Lcom/android/calendar/StickyHeaderListView;->mListViewHeadersCount:I

    iput-object p1, p0, Lcom/android/calendar/StickyHeaderListView;->mContext:Landroid/content/Context;

    return-void
.end method

.method private setChildViews()V
    .locals 7

    const/4 v6, 0x1

    invoke-virtual {p0}, Lcom/android/calendar/StickyHeaderListView;->getChildCount()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    invoke-virtual {p0, v0}, Lcom/android/calendar/StickyHeaderListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    instance-of v4, v3, Landroid/widget/ListView;

    if-eqz v4, :cond_0

    check-cast v3, Landroid/widget/ListView;

    invoke-virtual {p0, v3}, Lcom/android/calendar/StickyHeaderListView;->setListView(Landroid/widget/ListView;)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/android/calendar/StickyHeaderListView;->mListView:Landroid/widget/ListView;

    if-nez v4, :cond_2

    new-instance v4, Landroid/widget/ListView;

    iget-object v5, p0, Lcom/android/calendar/StickyHeaderListView;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0, v4}, Lcom/android/calendar/StickyHeaderListView;->setListView(Landroid/widget/ListView;)V

    :cond_2
    new-instance v4, Landroid/view/View;

    iget-object v5, p0, Lcom/android/calendar/StickyHeaderListView;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/android/calendar/StickyHeaderListView;->mDummyHeader:Landroid/view/View;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v4, -0x1

    const/16 v5, 0x30

    invoke-direct {v2, v4, v6, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    iget-object v4, p0, Lcom/android/calendar/StickyHeaderListView;->mDummyHeader:Landroid/view/View;

    invoke-virtual {v4, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v4, p0, Lcom/android/calendar/StickyHeaderListView;->mDummyHeader:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setBackgroundColor(I)V

    iput-boolean v6, p0, Lcom/android/calendar/StickyHeaderListView;->mChildViewsCreated:Z

    return-void
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 1

    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    iget-boolean v0, p0, Lcom/android/calendar/StickyHeaderListView;->mChildViewsCreated:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/calendar/StickyHeaderListView;->setChildViews()V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/StickyHeaderListView;->mDoHeaderReset:Z

    return-void
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    iget-boolean v0, p0, Lcom/android/calendar/StickyHeaderListView;->mChildViewsCreated:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/android/calendar/StickyHeaderListView;->setChildViews()V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/StickyHeaderListView;->mDoHeaderReset:Z

    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 1
    .param p1    # Landroid/widget/AbsListView;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-virtual {p0, p2}, Lcom/android/calendar/StickyHeaderListView;->updateStickyHeader(I)V

    iget-object v0, p0, Lcom/android/calendar/StickyHeaderListView;->mListener:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/StickyHeaderListView;->mListener:Landroid/widget/AbsListView$OnScrollListener;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/widget/AbsListView$OnScrollListener;->onScroll(Landroid/widget/AbsListView;III)V

    :cond_0
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1
    .param p1    # Landroid/widget/AbsListView;
    .param p2    # I

    iget-object v0, p0, Lcom/android/calendar/StickyHeaderListView;->mListener:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/StickyHeaderListView;->mListener:Landroid/widget/AbsListView$OnScrollListener;

    invoke-interface {v0, p1, p2}, Landroid/widget/AbsListView$OnScrollListener;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    :cond_0
    return-void
.end method

.method public setAdapter(Landroid/widget/Adapter;)V
    .locals 0
    .param p1    # Landroid/widget/Adapter;

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/android/calendar/StickyHeaderListView;->mAdapter:Landroid/widget/Adapter;

    :cond_0
    return-void
.end method

.method public setHeaderHeightListener(Lcom/android/calendar/StickyHeaderListView$HeaderHeightListener;)V
    .locals 0
    .param p1    # Lcom/android/calendar/StickyHeaderListView$HeaderHeightListener;

    iput-object p1, p0, Lcom/android/calendar/StickyHeaderListView;->mHeaderHeightListener:Lcom/android/calendar/StickyHeaderListView$HeaderHeightListener;

    return-void
.end method

.method public setHeaderSeparator(II)V
    .locals 3
    .param p1    # I
    .param p2    # I

    new-instance v1, Landroid/view/View;

    iget-object v2, p0, Lcom/android/calendar/StickyHeaderListView;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/calendar/StickyHeaderListView;->mSeparatorView:Landroid/view/View;

    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x1

    const/16 v2, 0x30

    invoke-direct {v0, v1, p2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    iget-object v1, p0, Lcom/android/calendar/StickyHeaderListView;->mSeparatorView:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/android/calendar/StickyHeaderListView;->mSeparatorView:Landroid/view/View;

    invoke-virtual {v1, p1}, Landroid/view/View;->setBackgroundColor(I)V

    iput p2, p0, Lcom/android/calendar/StickyHeaderListView;->mSeparatorWidth:I

    iget-object v1, p0, Lcom/android/calendar/StickyHeaderListView;->mSeparatorView:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/android/calendar/StickyHeaderListView;->addView(Landroid/view/View;)V

    return-void
.end method

.method public setIndexer(Lcom/android/calendar/StickyHeaderListView$HeaderIndexer;)V
    .locals 0
    .param p1    # Lcom/android/calendar/StickyHeaderListView$HeaderIndexer;

    iput-object p1, p0, Lcom/android/calendar/StickyHeaderListView;->mIndexer:Lcom/android/calendar/StickyHeaderListView$HeaderIndexer;

    return-void
.end method

.method public setListView(Landroid/widget/ListView;)V
    .locals 1
    .param p1    # Landroid/widget/ListView;

    iput-object p1, p0, Lcom/android/calendar/StickyHeaderListView;->mListView:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/android/calendar/StickyHeaderListView;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    iget-object v0, p0, Lcom/android/calendar/StickyHeaderListView;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v0

    iput v0, p0, Lcom/android/calendar/StickyHeaderListView;->mListViewHeadersCount:I

    return-void
.end method

.method public setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 0
    .param p1    # Landroid/widget/AbsListView$OnScrollListener;

    iput-object p1, p0, Lcom/android/calendar/StickyHeaderListView;->mListener:Landroid/widget/AbsListView$OnScrollListener;

    return-void
.end method

.method protected updateStickyHeader(I)V
    .locals 13
    .param p1    # I

    iget-object v9, p0, Lcom/android/calendar/StickyHeaderListView;->mAdapter:Landroid/widget/Adapter;

    if-nez v9, :cond_0

    iget-object v9, p0, Lcom/android/calendar/StickyHeaderListView;->mListView:Landroid/widget/ListView;

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/android/calendar/StickyHeaderListView;->mListView:Landroid/widget/ListView;

    invoke-virtual {v9}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v9

    invoke-virtual {p0, v9}, Lcom/android/calendar/StickyHeaderListView;->setAdapter(Landroid/widget/Adapter;)V

    :cond_0
    iget v9, p0, Lcom/android/calendar/StickyHeaderListView;->mListViewHeadersCount:I

    sub-int/2addr p1, v9

    iget-object v9, p0, Lcom/android/calendar/StickyHeaderListView;->mAdapter:Landroid/widget/Adapter;

    if-eqz v9, :cond_7

    iget-object v9, p0, Lcom/android/calendar/StickyHeaderListView;->mIndexer:Lcom/android/calendar/StickyHeaderListView$HeaderIndexer;

    if-eqz v9, :cond_7

    iget-boolean v9, p0, Lcom/android/calendar/StickyHeaderListView;->mDoHeaderReset:Z

    if-eqz v9, :cond_7

    const/4 v6, 0x0

    iget-object v9, p0, Lcom/android/calendar/StickyHeaderListView;->mIndexer:Lcom/android/calendar/StickyHeaderListView$HeaderIndexer;

    invoke-interface {v9, p1}, Lcom/android/calendar/StickyHeaderListView$HeaderIndexer;->getHeaderPositionFromItemPosition(I)I

    move-result v5

    const/4 v2, 0x0

    iget v9, p0, Lcom/android/calendar/StickyHeaderListView;->mCurrentSectionPos:I

    if-eq v5, v9, :cond_2

    const/4 v9, -0x1

    if-ne v5, v9, :cond_8

    const/4 v6, 0x0

    iget-object v9, p0, Lcom/android/calendar/StickyHeaderListView;->mStickyHeader:Landroid/view/View;

    invoke-virtual {p0, v9}, Lcom/android/calendar/StickyHeaderListView;->removeView(Landroid/view/View;)V

    iget-object v9, p0, Lcom/android/calendar/StickyHeaderListView;->mDummyHeader:Landroid/view/View;

    iput-object v9, p0, Lcom/android/calendar/StickyHeaderListView;->mStickyHeader:Landroid/view/View;

    iget-object v9, p0, Lcom/android/calendar/StickyHeaderListView;->mSeparatorView:Landroid/view/View;

    if-eqz v9, :cond_1

    iget-object v9, p0, Lcom/android/calendar/StickyHeaderListView;->mSeparatorView:Landroid/view/View;

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    const/4 v2, 0x1

    :goto_0
    iput v5, p0, Lcom/android/calendar/StickyHeaderListView;->mCurrentSectionPos:I

    add-int v9, v6, v5

    add-int/lit8 v9, v9, 0x1

    iput v9, p0, Lcom/android/calendar/StickyHeaderListView;->mNextSectionPosition:I

    :cond_2
    iget-object v9, p0, Lcom/android/calendar/StickyHeaderListView;->mStickyHeader:Landroid/view/View;

    if-eqz v9, :cond_7

    iget v9, p0, Lcom/android/calendar/StickyHeaderListView;->mNextSectionPosition:I

    sub-int/2addr v9, p1

    add-int/lit8 v4, v9, -0x1

    iget-object v9, p0, Lcom/android/calendar/StickyHeaderListView;->mStickyHeader:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getHeight()I

    move-result v7

    if-nez v7, :cond_3

    iget-object v9, p0, Lcom/android/calendar/StickyHeaderListView;->mStickyHeader:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    :cond_3
    iget-object v9, p0, Lcom/android/calendar/StickyHeaderListView;->mHeaderHeightListener:Lcom/android/calendar/StickyHeaderListView$HeaderHeightListener;

    if-eqz v9, :cond_4

    iget v9, p0, Lcom/android/calendar/StickyHeaderListView;->mLastStickyHeaderHeight:I

    if-eq v9, v7, :cond_4

    iput v7, p0, Lcom/android/calendar/StickyHeaderListView;->mLastStickyHeaderHeight:I

    iget-object v9, p0, Lcom/android/calendar/StickyHeaderListView;->mHeaderHeightListener:Lcom/android/calendar/StickyHeaderListView$HeaderHeightListener;

    invoke-interface {v9, v7}, Lcom/android/calendar/StickyHeaderListView$HeaderHeightListener;->OnHeaderHeightChanged(I)V

    :cond_4
    iget-object v9, p0, Lcom/android/calendar/StickyHeaderListView;->mListView:Landroid/widget/ListView;

    invoke-virtual {v9, v4}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v9

    if-gt v9, v7, :cond_9

    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v1

    iget-object v9, p0, Lcom/android/calendar/StickyHeaderListView;->mStickyHeader:Landroid/view/View;

    sub-int v10, v1, v7

    int-to-float v10, v10

    invoke-virtual {v9, v10}, Landroid/view/View;->setTranslationY(F)V

    iget-object v9, p0, Lcom/android/calendar/StickyHeaderListView;->mSeparatorView:Landroid/view/View;

    if-eqz v9, :cond_5

    iget-object v9, p0, Lcom/android/calendar/StickyHeaderListView;->mSeparatorView:Landroid/view/View;

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    :cond_5
    :goto_1
    if-eqz v2, :cond_7

    iget-object v9, p0, Lcom/android/calendar/StickyHeaderListView;->mStickyHeader:Landroid/view/View;

    const/4 v10, 0x4

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    iget-object v9, p0, Lcom/android/calendar/StickyHeaderListView;->mStickyHeader:Landroid/view/View;

    invoke-virtual {p0, v9}, Lcom/android/calendar/StickyHeaderListView;->addView(Landroid/view/View;)V

    iget-object v9, p0, Lcom/android/calendar/StickyHeaderListView;->mSeparatorView:Landroid/view/View;

    if-eqz v9, :cond_6

    iget-object v9, p0, Lcom/android/calendar/StickyHeaderListView;->mStickyHeader:Landroid/view/View;

    iget-object v10, p0, Lcom/android/calendar/StickyHeaderListView;->mDummyHeader:Landroid/view/View;

    invoke-virtual {v9, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_6

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v9, -0x1

    iget v10, p0, Lcom/android/calendar/StickyHeaderListView;->mSeparatorWidth:I

    invoke-direct {v3, v9, v10}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/android/calendar/StickyHeaderListView;->mStickyHeader:Landroid/view/View;

    invoke-virtual {v10}, Landroid/view/View;->getMeasuredHeight()I

    move-result v10

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v3, v9, v10, v11, v12}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    iget-object v9, p0, Lcom/android/calendar/StickyHeaderListView;->mSeparatorView:Landroid/view/View;

    invoke-virtual {v9, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v9, p0, Lcom/android/calendar/StickyHeaderListView;->mSeparatorView:Landroid/view/View;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    :cond_6
    iget-object v9, p0, Lcom/android/calendar/StickyHeaderListView;->mStickyHeader:Landroid/view/View;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    :cond_7
    return-void

    :cond_8
    iget-object v9, p0, Lcom/android/calendar/StickyHeaderListView;->mIndexer:Lcom/android/calendar/StickyHeaderListView$HeaderIndexer;

    invoke-interface {v9, v5}, Lcom/android/calendar/StickyHeaderListView$HeaderIndexer;->getHeaderItemsNumber(I)I

    move-result v6

    iget-object v9, p0, Lcom/android/calendar/StickyHeaderListView;->mAdapter:Landroid/widget/Adapter;

    iget v10, p0, Lcom/android/calendar/StickyHeaderListView;->mListViewHeadersCount:I

    add-int/2addr v10, v5

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/android/calendar/StickyHeaderListView;->mListView:Landroid/widget/ListView;

    invoke-interface {v9, v10, v11, v12}, Landroid/widget/Adapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    iget-object v9, p0, Lcom/android/calendar/StickyHeaderListView;->mListView:Landroid/widget/ListView;

    invoke-virtual {v9}, Landroid/widget/ListView;->getWidth()I

    move-result v9

    const/high16 v10, 0x40000000    # 2.0f

    invoke-static {v9, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    iget-object v10, p0, Lcom/android/calendar/StickyHeaderListView;->mListView:Landroid/widget/ListView;

    invoke-virtual {v10}, Landroid/widget/ListView;->getHeight()I

    move-result v10

    const/high16 v11, -0x80000000

    invoke-static {v10, v11}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    invoke-virtual {v8, v9, v10}, Landroid/view/View;->measure(II)V

    iget-object v9, p0, Lcom/android/calendar/StickyHeaderListView;->mStickyHeader:Landroid/view/View;

    invoke-virtual {p0, v9}, Lcom/android/calendar/StickyHeaderListView;->removeView(Landroid/view/View;)V

    iput-object v8, p0, Lcom/android/calendar/StickyHeaderListView;->mStickyHeader:Landroid/view/View;

    const/4 v2, 0x1

    goto/16 :goto_0

    :cond_9
    if-eqz v7, :cond_5

    iget-object v9, p0, Lcom/android/calendar/StickyHeaderListView;->mStickyHeader:Landroid/view/View;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/view/View;->setTranslationY(F)V

    iget-object v9, p0, Lcom/android/calendar/StickyHeaderListView;->mSeparatorView:Landroid/view/View;

    if-eqz v9, :cond_5

    iget-object v9, p0, Lcom/android/calendar/StickyHeaderListView;->mStickyHeader:Landroid/view/View;

    iget-object v10, p0, Lcom/android/calendar/StickyHeaderListView;->mDummyHeader:Landroid/view/View;

    invoke-virtual {v9, v10}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_5

    iget-object v9, p0, Lcom/android/calendar/StickyHeaderListView;->mSeparatorView:Landroid/view/View;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1
.end method
