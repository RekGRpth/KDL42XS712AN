.class public Lcom/android/calendar/DayFragment;
.super Landroid/app/Fragment;
.source "DayFragment.java"

# interfaces
.implements Landroid/widget/ViewSwitcher$ViewFactory;
.implements Lcom/android/calendar/CalendarController$EventHandler;


# instance fields
.field mEventLoader:Lcom/android/calendar/EventLoader;

.field protected mInAnimationBackward:Landroid/view/animation/Animation;

.field protected mInAnimationForward:Landroid/view/animation/Animation;

.field private mNumDays:I

.field protected mOutAnimationBackward:Landroid/view/animation/Animation;

.field protected mOutAnimationForward:Landroid/view/animation/Animation;

.field mSelectedDay:Landroid/text/format/Time;

.field private final mTZUpdater:Ljava/lang/Runnable;

.field protected mViewSwitcher:Landroid/widget/ViewSwitcher;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/DayFragment;->mSelectedDay:Landroid/text/format/Time;

    new-instance v0, Lcom/android/calendar/DayFragment$1;

    invoke-direct {v0, p0}, Lcom/android/calendar/DayFragment$1;-><init>(Lcom/android/calendar/DayFragment;)V

    iput-object v0, p0, Lcom/android/calendar/DayFragment;->mTZUpdater:Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/android/calendar/DayFragment;->mSelectedDay:Landroid/text/format/Time;

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    return-void
.end method

.method public constructor <init>(JI)V
    .locals 2
    .param p1    # J
    .param p3    # I

    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/DayFragment;->mSelectedDay:Landroid/text/format/Time;

    new-instance v0, Lcom/android/calendar/DayFragment$1;

    invoke-direct {v0, p0}, Lcom/android/calendar/DayFragment$1;-><init>(Lcom/android/calendar/DayFragment;)V

    iput-object v0, p0, Lcom/android/calendar/DayFragment;->mTZUpdater:Ljava/lang/Runnable;

    iput p3, p0, Lcom/android/calendar/DayFragment;->mNumDays:I

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/DayFragment;->mSelectedDay:Landroid/text/format/Time;

    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/DayFragment;->mSelectedDay:Landroid/text/format/Time;

    invoke-virtual {v0, p1, p2}, Landroid/text/format/Time;->set(J)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/android/calendar/DayFragment;)Ljava/lang/Runnable;
    .locals 1
    .param p0    # Lcom/android/calendar/DayFragment;

    iget-object v0, p0, Lcom/android/calendar/DayFragment;->mTZUpdater:Ljava/lang/Runnable;

    return-object v0
.end method

.method private goTo(Landroid/text/format/Time;ZZ)V
    .locals 5
    .param p1    # Landroid/text/format/Time;
    .param p2    # Z
    .param p3    # Z

    iget-object v3, p0, Lcom/android/calendar/DayFragment;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/android/calendar/DayFragment;->mSelectedDay:Landroid/text/format/Time;

    invoke-virtual {v3, p1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    :goto_0
    return-void

    :cond_0
    iget-object v3, p0, Lcom/android/calendar/DayFragment;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v3}, Landroid/widget/ViewSwitcher;->getCurrentView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/DayView;

    invoke-virtual {v0, p1}, Lcom/android/calendar/DayView;->compareToVisibleTimeRange(Landroid/text/format/Time;)I

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/calendar/DayView;->setSelected(Landroid/text/format/Time;ZZ)V

    goto :goto_0

    :cond_1
    if-lez v1, :cond_3

    iget-object v3, p0, Lcom/android/calendar/DayFragment;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    iget-object v4, p0, Lcom/android/calendar/DayFragment;->mInAnimationForward:Landroid/view/animation/Animation;

    invoke-virtual {v3, v4}, Landroid/widget/ViewSwitcher;->setInAnimation(Landroid/view/animation/Animation;)V

    iget-object v3, p0, Lcom/android/calendar/DayFragment;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    iget-object v4, p0, Lcom/android/calendar/DayFragment;->mOutAnimationForward:Landroid/view/animation/Animation;

    invoke-virtual {v3, v4}, Landroid/widget/ViewSwitcher;->setOutAnimation(Landroid/view/animation/Animation;)V

    :goto_1
    iget-object v3, p0, Lcom/android/calendar/DayFragment;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v3}, Landroid/widget/ViewSwitcher;->getNextView()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/android/calendar/DayView;

    if-eqz p2, :cond_2

    invoke-virtual {v0}, Lcom/android/calendar/DayView;->getFirstVisibleHour()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/android/calendar/DayView;->setFirstVisibleHour(I)V

    :cond_2
    invoke-virtual {v2, p1, p2, p3}, Lcom/android/calendar/DayView;->setSelected(Landroid/text/format/Time;ZZ)V

    invoke-virtual {v2}, Lcom/android/calendar/DayView;->reloadEvents()V

    iget-object v3, p0, Lcom/android/calendar/DayFragment;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v3}, Landroid/widget/ViewSwitcher;->showNext()V

    invoke-virtual {v2}, Lcom/android/calendar/DayView;->requestFocus()Z

    invoke-virtual {v2}, Lcom/android/calendar/DayView;->updateTitle()V

    invoke-virtual {v2}, Lcom/android/calendar/DayView;->restartCurrentTimeUpdates()V

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/android/calendar/DayFragment;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    iget-object v4, p0, Lcom/android/calendar/DayFragment;->mInAnimationBackward:Landroid/view/animation/Animation;

    invoke-virtual {v3, v4}, Landroid/widget/ViewSwitcher;->setInAnimation(Landroid/view/animation/Animation;)V

    iget-object v3, p0, Lcom/android/calendar/DayFragment;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    iget-object v4, p0, Lcom/android/calendar/DayFragment;->mOutAnimationBackward:Landroid/view/animation/Animation;

    invoke-virtual {v3, v4}, Landroid/widget/ViewSwitcher;->setOutAnimation(Landroid/view/animation/Animation;)V

    goto :goto_1
.end method


# virtual methods
.method public eventsChanged()V
    .locals 2

    iget-object v1, p0, Lcom/android/calendar/DayFragment;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/calendar/DayFragment;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v1}, Landroid/widget/ViewSwitcher;->getCurrentView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/DayView;

    invoke-virtual {v0}, Lcom/android/calendar/DayView;->clearCachedEvents()V

    invoke-virtual {v0}, Lcom/android/calendar/DayView;->reloadEvents()V

    iget-object v1, p0, Lcom/android/calendar/DayFragment;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v1}, Landroid/widget/ViewSwitcher;->getNextView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/DayView;

    invoke-virtual {v0}, Lcom/android/calendar/DayView;->clearCachedEvents()V

    goto :goto_0
.end method

.method public getSelectedTimeInMillis()J
    .locals 4

    const-wide/16 v1, -0x1

    iget-object v3, p0, Lcom/android/calendar/DayFragment;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return-wide v1

    :cond_1
    iget-object v3, p0, Lcom/android/calendar/DayFragment;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v3}, Landroid/widget/ViewSwitcher;->getCurrentView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/DayView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/calendar/DayView;->getSelectedTimeInMillis()J

    move-result-wide v1

    goto :goto_0
.end method

.method public getSupportedEventTypes()J
    .locals 2

    const-wide/16 v0, 0xa0

    return-wide v0
.end method

.method public handleEvent(Lcom/android/calendar/CalendarController$EventInfo;)V
    .locals 10
    .param p1    # Lcom/android/calendar/CalendarController$EventInfo;

    const-wide/16 v8, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-wide v3, p1, Lcom/android/calendar/CalendarController$EventInfo;->eventType:J

    const-wide/16 v5, 0x20

    cmp-long v0, v3, v5

    if-nez v0, :cond_3

    iget-object v3, p1, Lcom/android/calendar/CalendarController$EventInfo;->selectedTime:Landroid/text/format/Time;

    iget-wide v4, p1, Lcom/android/calendar/CalendarController$EventInfo;->extraLong:J

    const-wide/16 v6, 0x1

    and-long/2addr v4, v6

    cmp-long v0, v4, v8

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iget-wide v4, p1, Lcom/android/calendar/CalendarController$EventInfo;->extraLong:J

    const-wide/16 v6, 0x8

    and-long/2addr v4, v6

    cmp-long v4, v4, v8

    if-eqz v4, :cond_2

    :goto_1
    invoke-direct {p0, v3, v0, v1}, Lcom/android/calendar/DayFragment;->goTo(Landroid/text/format/Time;ZZ)V

    :cond_0
    :goto_2
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    iget-wide v0, p1, Lcom/android/calendar/CalendarController$EventInfo;->eventType:J

    const-wide/16 v2, 0x80

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/android/calendar/DayFragment;->eventsChanged()V

    goto :goto_2
.end method

.method public makeView()Landroid/view/View;
    .locals 8

    const/4 v7, 0x0

    const/4 v6, -0x1

    iget-object v1, p0, Lcom/android/calendar/DayFragment;->mTZUpdater:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    new-instance v0, Lcom/android/calendar/DayView;

    invoke-virtual {p0}, Lcom/android/calendar/DayFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/android/calendar/DayFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/android/calendar/CalendarController;->getInstance(Landroid/content/Context;)Lcom/android/calendar/CalendarController;

    move-result-object v2

    iget-object v3, p0, Lcom/android/calendar/DayFragment;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    iget-object v4, p0, Lcom/android/calendar/DayFragment;->mEventLoader:Lcom/android/calendar/EventLoader;

    iget v5, p0, Lcom/android/calendar/DayFragment;->mNumDays:I

    invoke-direct/range {v0 .. v5}, Lcom/android/calendar/DayView;-><init>(Landroid/content/Context;Lcom/android/calendar/CalendarController;Landroid/widget/ViewSwitcher;Lcom/android/calendar/EventLoader;I)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/calendar/DayView;->setId(I)V

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v6, v6}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/android/calendar/DayView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/android/calendar/DayFragment;->mSelectedDay:Landroid/text/format/Time;

    invoke-virtual {v0, v1, v7, v7}, Lcom/android/calendar/DayView;->setSelected(Landroid/text/format/Time;ZZ)V

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/calendar/DayFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const/high16 v1, 0x7f050000    # com.android.calendar.R.anim.slide_left_in

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/DayFragment;->mInAnimationForward:Landroid/view/animation/Animation;

    const v1, 0x7f050001    # com.android.calendar.R.anim.slide_left_out

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/DayFragment;->mOutAnimationForward:Landroid/view/animation/Animation;

    const v1, 0x7f050002    # com.android.calendar.R.anim.slide_right_in

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/DayFragment;->mInAnimationBackward:Landroid/view/animation/Animation;

    const v1, 0x7f050003    # com.android.calendar.R.anim.slide_right_out

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/DayFragment;->mOutAnimationBackward:Landroid/view/animation/Animation;

    new-instance v1, Lcom/android/calendar/EventLoader;

    invoke-direct {v1, v0}, Lcom/android/calendar/EventLoader;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/calendar/DayFragment;->mEventLoader:Lcom/android/calendar/EventLoader;

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1    # Landroid/view/LayoutInflater;
    .param p2    # Landroid/view/ViewGroup;
    .param p3    # Landroid/os/Bundle;

    const v1, 0x7f04001a    # com.android.calendar.R.layout.day_activity

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f10003a    # com.android.calendar.R.id.switcher

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ViewSwitcher;

    iput-object v1, p0, Lcom/android/calendar/DayFragment;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    iget-object v1, p0, Lcom/android/calendar/DayFragment;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v1, p0}, Landroid/widget/ViewSwitcher;->setFactory(Landroid/widget/ViewSwitcher$ViewFactory;)V

    iget-object v1, p0, Lcom/android/calendar/DayFragment;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v1}, Landroid/widget/ViewSwitcher;->getCurrentView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    iget-object v1, p0, Lcom/android/calendar/DayFragment;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v1}, Landroid/widget/ViewSwitcher;->getCurrentView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/DayView;

    invoke-virtual {v1}, Lcom/android/calendar/DayView;->updateTitle()V

    return-object v0
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Landroid/app/Fragment;->onPause()V

    iget-object v1, p0, Lcom/android/calendar/DayFragment;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v1}, Landroid/widget/ViewSwitcher;->getCurrentView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/DayView;

    invoke-virtual {v0}, Lcom/android/calendar/DayView;->cleanup()V

    iget-object v1, p0, Lcom/android/calendar/DayFragment;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v1}, Landroid/widget/ViewSwitcher;->getNextView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/DayView;

    invoke-virtual {v0}, Lcom/android/calendar/DayView;->cleanup()V

    iget-object v1, p0, Lcom/android/calendar/DayFragment;->mEventLoader:Lcom/android/calendar/EventLoader;

    invoke-virtual {v1}, Lcom/android/calendar/EventLoader;->stopBackgroundThread()V

    invoke-virtual {v0}, Lcom/android/calendar/DayView;->stopEventsAnimation()V

    iget-object v1, p0, Lcom/android/calendar/DayFragment;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v1}, Landroid/widget/ViewSwitcher;->getNextView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/android/calendar/DayView;

    invoke-virtual {v1}, Lcom/android/calendar/DayView;->stopEventsAnimation()V

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    iget-object v1, p0, Lcom/android/calendar/DayFragment;->mEventLoader:Lcom/android/calendar/EventLoader;

    invoke-virtual {v1}, Lcom/android/calendar/EventLoader;->startBackgroundThread()V

    iget-object v1, p0, Lcom/android/calendar/DayFragment;->mTZUpdater:Ljava/lang/Runnable;

    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    invoke-virtual {p0}, Lcom/android/calendar/DayFragment;->eventsChanged()V

    iget-object v1, p0, Lcom/android/calendar/DayFragment;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v1}, Landroid/widget/ViewSwitcher;->getCurrentView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/DayView;

    invoke-virtual {v0}, Lcom/android/calendar/DayView;->handleOnResume()V

    invoke-virtual {v0}, Lcom/android/calendar/DayView;->restartCurrentTimeUpdates()V

    iget-object v1, p0, Lcom/android/calendar/DayFragment;->mViewSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v1}, Landroid/widget/ViewSwitcher;->getNextView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/calendar/DayView;

    invoke-virtual {v0}, Lcom/android/calendar/DayView;->handleOnResume()V

    invoke-virtual {v0}, Lcom/android/calendar/DayView;->restartCurrentTimeUpdates()V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/android/calendar/DayFragment;->getSelectedTimeInMillis()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    const-string v2, "key_restore_time"

    invoke-virtual {p1, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    :cond_0
    return-void
.end method
