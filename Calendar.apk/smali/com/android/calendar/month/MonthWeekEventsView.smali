.class public Lcom/android/calendar/month/MonthWeekEventsView;
.super Lcom/android/calendar/month/SimpleWeekView;
.source "MonthWeekEventsView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/calendar/month/MonthWeekEventsView$FloatRef;,
        Lcom/android/calendar/month/MonthWeekEventsView$TodayAnimatorListener;
    }
.end annotation


# static fields
.field private static CONFLICT_COLOR:I

.field private static DAY_SEPARATOR_INNER_WIDTH:I

.field private static DAY_SEPARATOR_OUTER_WIDTH:I

.field private static DAY_SEPARATOR_VERTICAL_LENGHT_PORTRAIT:I

.field private static DAY_SEPARATOR_VERTICAL_LENGTH:I

.field private static DEFAULT_EDGE_SPACING:I

.field private static DNA_ALL_DAY_HEIGHT:I

.field private static DNA_ALL_DAY_WIDTH:I

.field private static DNA_MARGIN:I

.field private static DNA_MIN_SEGMENT_HEIGHT:I

.field private static DNA_SIDE_PADDING:I

.field private static DNA_WIDTH:I

.field private static EVENT_BOTTOM_PADDING:I

.field private static EVENT_LINE_PADDING:I

.field private static EVENT_RIGHT_PADDING:I

.field private static EVENT_SQUARE_BORDER:I

.field private static EVENT_SQUARE_WIDTH:I

.field private static EVENT_TEXT_COLOR:I

.field private static EVENT_X_OFFSET_LANDSCAPE:I

.field private static EVENT_Y_OFFSET_LANDSCAPE:I

.field private static EVENT_Y_OFFSET_PORTRAIT:I

.field private static MIN_WEEK_WIDTH:I

.field private static SIDE_PADDING_MONTH_NUMBER:I

.field private static SIDE_PADDING_WEEK_NUMBER:I

.field private static SPACING_WEEK_NUMBER:I

.field private static TEXT_SIZE_EVENT:I

.field private static TEXT_SIZE_EVENT_TITLE:I

.field private static TEXT_SIZE_MONTH_NAME:I

.field private static TEXT_SIZE_MONTH_NUMBER:I

.field private static TEXT_SIZE_MORE_EVENTS:I

.field private static TEXT_SIZE_WEEK_NUM:I

.field private static TODAY_HIGHLIGHT_WIDTH:I

.field private static TOP_PADDING_MONTH_NUMBER:I

.field private static TOP_PADDING_WEEK_NUMBER:I

.field protected static mFormatter:Ljava/util/Formatter;

.field private static mInitialized:Z

.field private static mShowDetailsInMonth:Z

.field protected static mStringBuilder:Ljava/lang/StringBuilder;


# instance fields
.field private mAnimateToday:Z

.field private mAnimateTodayAlpha:I

.field private final mAnimatorListener:Lcom/android/calendar/month/MonthWeekEventsView$TodayAnimatorListener;

.field private mClickedDayColor:I

.field private mClickedDayIndex:I

.field protected mDNAAllDayPaint:Landroid/graphics/Paint;

.field protected mDNATimePaint:Landroid/graphics/Paint;

.field protected mDaySeparatorInnerColor:I

.field private mDayXs:[I

.field protected mDeclinedEventPaint:Landroid/text/TextPaint;

.field mDna:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/android/calendar/Utils$DNAStrand;",
            ">;"
        }
    .end annotation
.end field

.field protected mEventAscentHeight:I

.field protected mEventChipOutlineColor:I

.field protected mEventDeclinedExtrasPaint:Landroid/text/TextPaint;

.field protected mEventExtrasPaint:Landroid/text/TextPaint;

.field protected mEventHeight:I

.field protected mEventOutlines:Lcom/android/calendar/month/MonthWeekEventsView$FloatRef;

.field protected mEventPaint:Landroid/text/TextPaint;

.field protected mEventSquarePaint:Landroid/graphics/Paint;

.field protected mEvents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/calendar/Event;",
            ">;>;"
        }
    .end annotation
.end field

.field protected mExtrasAscentHeight:I

.field protected mExtrasDescent:I

.field protected mExtrasHeight:I

.field protected mFramedEventPaint:Landroid/text/TextPaint;

.field protected mHasToday:Z

.field protected mMonthBGColor:I

.field protected mMonthBGOtherColor:I

.field protected mMonthBGTodayColor:I

.field protected mMonthBusyBitsBusyTimeColor:I

.field protected mMonthBusyBitsConflictTimeColor:I

.field protected mMonthDeclinedEventColor:I

.field protected mMonthDeclinedExtrasColor:I

.field protected mMonthEventColor:I

.field protected mMonthEventExtraColor:I

.field protected mMonthEventExtraOtherColor:I

.field protected mMonthEventOtherColor:I

.field protected mMonthNameColor:I

.field protected mMonthNameOtherColor:I

.field protected mMonthNumAscentHeight:I

.field protected mMonthNumColor:I

.field protected mMonthNumHeight:I

.field protected mMonthNumOtherColor:I

.field protected mMonthNumTodayColor:I

.field protected mMonthWeekNumColor:I

.field protected mOrientation:I

.field protected mSolidBackgroundEventPaint:Landroid/text/TextPaint;

.field protected mToday:Landroid/text/format/Time;

.field protected mTodayAnimateColor:I

.field private mTodayAnimator:Landroid/animation/ObjectAnimator;

.field protected mTodayDrawable:Landroid/graphics/drawable/Drawable;

.field protected mTodayIndex:I

.field protected mUnsortedEvents:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/calendar/Event;",
            ">;"
        }
    .end annotation
.end field

.field protected mWeekNumAscentHeight:I

.field protected mWeekNumPaint:Landroid/graphics/Paint;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/16 v5, 0x8

    const/16 v4, 0xc

    const/4 v3, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x4

    const/16 v0, 0x20

    sput v0, Lcom/android/calendar/month/MonthWeekEventsView;->TEXT_SIZE_MONTH_NUMBER:I

    sput v4, Lcom/android/calendar/month/MonthWeekEventsView;->TEXT_SIZE_EVENT:I

    const/16 v0, 0xe

    sput v0, Lcom/android/calendar/month/MonthWeekEventsView;->TEXT_SIZE_EVENT_TITLE:I

    sput v4, Lcom/android/calendar/month/MonthWeekEventsView;->TEXT_SIZE_MORE_EVENTS:I

    const/16 v0, 0xe

    sput v0, Lcom/android/calendar/month/MonthWeekEventsView;->TEXT_SIZE_MONTH_NAME:I

    sput v4, Lcom/android/calendar/month/MonthWeekEventsView;->TEXT_SIZE_WEEK_NUM:I

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->DNA_MARGIN:I

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->DNA_ALL_DAY_HEIGHT:I

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->DNA_MIN_SEGMENT_HEIGHT:I

    sput v5, Lcom/android/calendar/month/MonthWeekEventsView;->DNA_WIDTH:I

    const/16 v0, 0x20

    sput v0, Lcom/android/calendar/month/MonthWeekEventsView;->DNA_ALL_DAY_WIDTH:I

    const/4 v0, 0x6

    sput v0, Lcom/android/calendar/month/MonthWeekEventsView;->DNA_SIDE_PADDING:I

    const/high16 v0, -0x1000000

    sput v0, Lcom/android/calendar/month/MonthWeekEventsView;->CONFLICT_COLOR:I

    const/4 v0, -0x1

    sput v0, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_TEXT_COLOR:I

    sput v2, Lcom/android/calendar/month/MonthWeekEventsView;->DEFAULT_EDGE_SPACING:I

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->SIDE_PADDING_MONTH_NUMBER:I

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->TOP_PADDING_MONTH_NUMBER:I

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->TOP_PADDING_WEEK_NUMBER:I

    const/16 v0, 0x14

    sput v0, Lcom/android/calendar/month/MonthWeekEventsView;->SIDE_PADDING_WEEK_NUMBER:I

    sput v2, Lcom/android/calendar/month/MonthWeekEventsView;->DAY_SEPARATOR_OUTER_WIDTH:I

    const/4 v0, 0x1

    sput v0, Lcom/android/calendar/month/MonthWeekEventsView;->DAY_SEPARATOR_INNER_WIDTH:I

    const/16 v0, 0x35

    sput v0, Lcom/android/calendar/month/MonthWeekEventsView;->DAY_SEPARATOR_VERTICAL_LENGTH:I

    const/16 v0, 0x40

    sput v0, Lcom/android/calendar/month/MonthWeekEventsView;->DAY_SEPARATOR_VERTICAL_LENGHT_PORTRAIT:I

    const/16 v0, 0x32

    sput v0, Lcom/android/calendar/month/MonthWeekEventsView;->MIN_WEEK_WIDTH:I

    const/16 v0, 0x26

    sput v0, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_X_OFFSET_LANDSCAPE:I

    sput v5, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_Y_OFFSET_LANDSCAPE:I

    const/4 v0, 0x7

    sput v0, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_Y_OFFSET_PORTRAIT:I

    const/16 v0, 0xa

    sput v0, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_SQUARE_WIDTH:I

    sput v3, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_SQUARE_BORDER:I

    sput v3, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_LINE_PADDING:I

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_RIGHT_PADDING:I

    const/4 v0, 0x3

    sput v0, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_BOTTOM_PADDING:I

    sput v3, Lcom/android/calendar/month/MonthWeekEventsView;->TODAY_HIGHLIGHT_WIDTH:I

    const/16 v0, 0x18

    sput v0, Lcom/android/calendar/month/MonthWeekEventsView;->SPACING_WEEK_NUMBER:I

    sput-boolean v2, Lcom/android/calendar/month/MonthWeekEventsView;->mInitialized:Z

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    sput-object v0, Lcom/android/calendar/month/MonthWeekEventsView;->mStringBuilder:Ljava/lang/StringBuilder;

    new-instance v0, Ljava/util/Formatter;

    sget-object v1, Lcom/android/calendar/month/MonthWeekEventsView;->mStringBuilder:Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    sput-object v0, Lcom/android/calendar/month/MonthWeekEventsView;->mFormatter:Ljava/util/Formatter;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1    # Landroid/content/Context;

    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1}, Lcom/android/calendar/month/SimpleWeekView;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mToday:Landroid/text/format/Time;

    iput-boolean v4, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mHasToday:Z

    iput v3, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mTodayIndex:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mOrientation:I

    iput-object v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEvents:Ljava/util/List;

    iput-object v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mUnsortedEvents:Ljava/util/ArrayList;

    iput-object v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mDna:Ljava/util/HashMap;

    new-instance v0, Lcom/android/calendar/month/MonthWeekEventsView$FloatRef;

    const/16 v1, 0x460

    invoke-direct {v0, p0, v1}, Lcom/android/calendar/month/MonthWeekEventsView$FloatRef;-><init>(Lcom/android/calendar/month/MonthWeekEventsView;I)V

    iput-object v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventOutlines:Lcom/android/calendar/month/MonthWeekEventsView$FloatRef;

    iput v3, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mClickedDayIndex:I

    iput v3, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventChipOutlineColor:I

    iput v4, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mAnimateTodayAlpha:I

    iput-object v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mTodayAnimator:Landroid/animation/ObjectAnimator;

    new-instance v0, Lcom/android/calendar/month/MonthWeekEventsView$TodayAnimatorListener;

    invoke-direct {v0, p0}, Lcom/android/calendar/month/MonthWeekEventsView$TodayAnimatorListener;-><init>(Lcom/android/calendar/month/MonthWeekEventsView;)V

    iput-object v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mAnimatorListener:Lcom/android/calendar/month/MonthWeekEventsView$TodayAnimatorListener;

    return-void
.end method

.method static synthetic access$000(Lcom/android/calendar/month/MonthWeekEventsView;)Landroid/animation/ObjectAnimator;
    .locals 1
    .param p0    # Lcom/android/calendar/month/MonthWeekEventsView;

    iget-object v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mTodayAnimator:Landroid/animation/ObjectAnimator;

    return-object v0
.end method

.method static synthetic access$002(Lcom/android/calendar/month/MonthWeekEventsView;Landroid/animation/ObjectAnimator;)Landroid/animation/ObjectAnimator;
    .locals 0
    .param p0    # Lcom/android/calendar/month/MonthWeekEventsView;
    .param p1    # Landroid/animation/ObjectAnimator;

    iput-object p1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mTodayAnimator:Landroid/animation/ObjectAnimator;

    return-object p1
.end method

.method static synthetic access$102(Lcom/android/calendar/month/MonthWeekEventsView;Z)Z
    .locals 0
    .param p0    # Lcom/android/calendar/month/MonthWeekEventsView;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mAnimateToday:Z

    return p1
.end method

.method static synthetic access$202(Lcom/android/calendar/month/MonthWeekEventsView;I)I
    .locals 0
    .param p0    # Lcom/android/calendar/month/MonthWeekEventsView;
    .param p1    # I

    iput p1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mAnimateTodayAlpha:I

    return p1
.end method

.method private computeDayLeftPosition(I)I
    .locals 5
    .param p1    # I

    iget v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mWidth:I

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mShowWeekNum:Z

    if-eqz v3, :cond_0

    sget v3, Lcom/android/calendar/month/MonthWeekEventsView;->SPACING_WEEK_NUMBER:I

    iget v4, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mPadding:I

    add-int v2, v3, v4

    sub-int/2addr v0, v2

    :cond_0
    mul-int v3, p1, v0

    iget v4, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mNumDays:I

    div-int/2addr v3, v4

    add-int v1, v3, v2

    return v1
.end method

.method private drawClick(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1    # Landroid/graphics/Canvas;

    iget v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mClickedDayIndex:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->p:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getAlpha()I

    move-result v0

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->p:Landroid/graphics/Paint;

    iget v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mClickedDayColor:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->p:Landroid/graphics/Paint;

    const/16 v2, 0x80

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->r:Landroid/graphics/Rect;

    iget v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mClickedDayIndex:I

    invoke-direct {p0, v2}, Lcom/android/calendar/month/MonthWeekEventsView;->computeDayLeftPosition(I)I

    move-result v2

    iput v2, v1, Landroid/graphics/Rect;->left:I

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->r:Landroid/graphics/Rect;

    iget v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mClickedDayIndex:I

    add-int/lit8 v2, v2, 0x1

    invoke-direct {p0, v2}, Lcom/android/calendar/month/MonthWeekEventsView;->computeDayLeftPosition(I)I

    move-result v2

    iput v2, v1, Landroid/graphics/Rect;->right:I

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->r:Landroid/graphics/Rect;

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->DAY_SEPARATOR_INNER_WIDTH:I

    iput v2, v1, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->r:Landroid/graphics/Rect;

    iget v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mHeight:I

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->r:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->p:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->p:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setAlpha(I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public clearClickedDay()V
    .locals 1

    const/4 v0, -0x1

    iput v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mClickedDayIndex:I

    invoke-virtual {p0}, Lcom/android/calendar/month/MonthWeekEventsView;->invalidate()V

    return-void
.end method

.method public createDna(Ljava/util/ArrayList;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/calendar/Event;",
            ">;)V"
        }
    .end annotation

    const/4 v4, 0x0

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mWidth:I

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->MIN_WEEK_WIDTH:I

    if-le v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/android/calendar/month/MonthWeekEventsView;->getContext()Landroid/content/Context;

    move-result-object v0

    if-nez v0, :cond_2

    :cond_0
    iput-object p1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mUnsortedEvents:Ljava/util/ArrayList;

    iput-object v4, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mDna:Ljava/util/HashMap;

    :cond_1
    :goto_0
    return-void

    :cond_2
    iput-object v4, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mUnsortedEvents:Ljava/util/ArrayList;

    sget-boolean v0, Lcom/android/calendar/month/MonthWeekEventsView;->mShowDetailsInMonth:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEvents:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v9

    iget v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mWidth:I

    iget v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mPadding:I

    mul-int/lit8 v1, v1, 0x2

    sub-int v8, v0, v1

    iget-boolean v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mShowWeekNum:Z

    if-eqz v0, :cond_3

    sget v0, Lcom/android/calendar/month/MonthWeekEventsView;->SPACING_WEEK_NUMBER:I

    sub-int/2addr v8, v0

    :cond_3
    div-int v0, v8, v9

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->DNA_SIDE_PADDING:I

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    sput v0, Lcom/android/calendar/month/MonthWeekEventsView;->DNA_ALL_DAY_WIDTH:I

    iget-object v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mDNAAllDayPaint:Landroid/graphics/Paint;

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->DNA_ALL_DAY_WIDTH:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    new-array v0, v9, [I

    iput-object v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mDayXs:[I

    const/4 v7, 0x0

    :goto_1
    if-ge v7, v9, :cond_4

    iget-object v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mDayXs:[I

    invoke-direct {p0, v7}, Lcom/android/calendar/month/MonthWeekEventsView;->computeDayLeftPosition(I)I

    move-result v1

    sget v4, Lcom/android/calendar/month/MonthWeekEventsView;->DNA_WIDTH:I

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v1, v4

    sget v4, Lcom/android/calendar/month/MonthWeekEventsView;->DNA_SIDE_PADDING:I

    add-int/2addr v1, v4

    aput v1, v0, v7

    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_4
    sget v0, Lcom/android/calendar/month/MonthWeekEventsView;->DAY_SEPARATOR_INNER_WIDTH:I

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->DNA_MARGIN:I

    add-int/2addr v0, v1

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->DNA_ALL_DAY_HEIGHT:I

    add-int/2addr v0, v1

    add-int/lit8 v2, v0, 0x1

    iget v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mHeight:I

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->DNA_MARGIN:I

    sub-int v3, v0, v1

    iget v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mFirstJulianDay:I

    sget v4, Lcom/android/calendar/month/MonthWeekEventsView;->DNA_MIN_SEGMENT_HEIGHT:I

    iget-object v5, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mDayXs:[I

    invoke-virtual {p0}, Lcom/android/calendar/month/MonthWeekEventsView;->getContext()Landroid/content/Context;

    move-result-object v6

    move-object v1, p1

    invoke-static/range {v0 .. v6}, Lcom/android/calendar/Utils;->createDNAStrands(ILjava/util/ArrayList;III[ILandroid/content/Context;)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mDna:Ljava/util/HashMap;

    goto :goto_0
.end method

.method protected drawBackground(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1    # Landroid/graphics/Canvas;

    const/4 v0, 0x0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->r:Landroid/graphics/Rect;

    sget v3, Lcom/android/calendar/month/MonthWeekEventsView;->DAY_SEPARATOR_INNER_WIDTH:I

    iput v3, v2, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->r:Landroid/graphics/Rect;

    iget v3, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mHeight:I

    iput v3, v2, Landroid/graphics/Rect;->bottom:I

    iget-boolean v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mShowWeekNum:Z

    if-eqz v2, :cond_0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v1, v1, 0x1

    :cond_0
    iget-object v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mOddMonth:[Z

    aget-boolean v2, v2, v0

    if-nez v2, :cond_5

    :cond_1
    add-int/lit8 v0, v0, 0x1

    iget-object v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mOddMonth:[Z

    array-length v2, v2

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mOddMonth:[Z

    aget-boolean v2, v2, v0

    if-eqz v2, :cond_1

    :cond_2
    iget-object v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->r:Landroid/graphics/Rect;

    sub-int v3, v0, v1

    invoke-direct {p0, v3}, Lcom/android/calendar/month/MonthWeekEventsView;->computeDayLeftPosition(I)I

    move-result v3

    iput v3, v2, Landroid/graphics/Rect;->right:I

    iget-object v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->r:Landroid/graphics/Rect;

    const/4 v3, 0x0

    iput v3, v2, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->p:Landroid/graphics/Paint;

    iget v3, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthBGOtherColor:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->r:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/android/calendar/month/MonthWeekEventsView;->p:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_3
    :goto_0
    iget-boolean v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mHasToday:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->p:Landroid/graphics/Paint;

    iget v3, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthBGTodayColor:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->r:Landroid/graphics/Rect;

    iget v3, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mTodayIndex:I

    invoke-direct {p0, v3}, Lcom/android/calendar/month/MonthWeekEventsView;->computeDayLeftPosition(I)I

    move-result v3

    iput v3, v2, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->r:Landroid/graphics/Rect;

    iget v3, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mTodayIndex:I

    add-int/lit8 v3, v3, 0x1

    invoke-direct {p0, v3}, Lcom/android/calendar/month/MonthWeekEventsView;->computeDayLeftPosition(I)I

    move-result v3

    iput v3, v2, Landroid/graphics/Rect;->right:I

    iget-object v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->r:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/android/calendar/month/MonthWeekEventsView;->p:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_4
    return-void

    :cond_5
    iget-object v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mOddMonth:[Z

    iget-object v3, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mOddMonth:[Z

    array-length v3, v3

    add-int/lit8 v0, v3, -0x1

    aget-boolean v2, v2, v0

    if-nez v2, :cond_3

    :cond_6
    add-int/lit8 v0, v0, -0x1

    if-lt v0, v1, :cond_7

    iget-object v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mOddMonth:[Z

    aget-boolean v2, v2, v0

    if-eqz v2, :cond_6

    :cond_7
    add-int/lit8 v0, v0, 0x1

    iget-object v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->r:Landroid/graphics/Rect;

    iget v3, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mWidth:I

    iput v3, v2, Landroid/graphics/Rect;->right:I

    iget-object v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->r:Landroid/graphics/Rect;

    sub-int v3, v0, v1

    invoke-direct {p0, v3}, Lcom/android/calendar/month/MonthWeekEventsView;->computeDayLeftPosition(I)I

    move-result v3

    iput v3, v2, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->p:Landroid/graphics/Paint;

    iget v3, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthBGOtherColor:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->r:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/android/calendar/month/MonthWeekEventsView;->p:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method protected drawDNA(Landroid/graphics/Canvas;)V
    .locals 11
    .param p1    # Landroid/graphics/Canvas;

    iget-object v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mDna:Ljava/util/HashMap;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mDna:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/calendar/Utils$DNAStrand;

    iget v0, v9, Lcom/android/calendar/Utils$DNAStrand;->color:I

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->CONFLICT_COLOR:I

    if-eq v0, v1, :cond_0

    iget-object v0, v9, Lcom/android/calendar/Utils$DNAStrand;->points:[F

    if-eqz v0, :cond_0

    iget-object v0, v9, Lcom/android/calendar/Utils$DNAStrand;->points:[F

    array-length v0, v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mDNATimePaint:Landroid/graphics/Paint;

    iget v1, v9, Lcom/android/calendar/Utils$DNAStrand;->color:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, v9, Lcom/android/calendar/Utils$DNAStrand;->points:[F

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mDNATimePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawLines([FLandroid/graphics/Paint;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mDna:Ljava/util/HashMap;

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->CONFLICT_COLOR:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/android/calendar/Utils$DNAStrand;

    if-eqz v9, :cond_2

    iget-object v0, v9, Lcom/android/calendar/Utils$DNAStrand;->points:[F

    if-eqz v0, :cond_2

    iget-object v0, v9, Lcom/android/calendar/Utils$DNAStrand;->points:[F

    array-length v0, v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mDNATimePaint:Landroid/graphics/Paint;

    iget v1, v9, Lcom/android/calendar/Utils$DNAStrand;->color:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, v9, Lcom/android/calendar/Utils$DNAStrand;->points:[F

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mDNATimePaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawLines([FLandroid/graphics/Paint;)V

    :cond_2
    iget-object v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mDayXs:[I

    if-nez v0, :cond_4

    :cond_3
    return-void

    :cond_4
    iget-object v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mDayXs:[I

    array-length v8, v0

    sget v0, Lcom/android/calendar/month/MonthWeekEventsView;->DNA_ALL_DAY_WIDTH:I

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->DNA_WIDTH:I

    sub-int/2addr v0, v1

    div-int/lit8 v10, v0, 0x2

    if-eqz v9, :cond_3

    iget-object v0, v9, Lcom/android/calendar/Utils$DNAStrand;->allDays:[I

    if-eqz v0, :cond_3

    iget-object v0, v9, Lcom/android/calendar/Utils$DNAStrand;->allDays:[I

    array-length v0, v0

    if-ne v0, v8, :cond_3

    const/4 v6, 0x0

    :goto_1
    if-ge v6, v8, :cond_3

    iget-object v0, v9, Lcom/android/calendar/Utils$DNAStrand;->allDays:[I

    aget v0, v0, v6

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mDNAAllDayPaint:Landroid/graphics/Paint;

    iget-object v1, v9, Lcom/android/calendar/Utils$DNAStrand;->allDays:[I

    aget v1, v1, v6

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mDayXs:[I

    aget v0, v0, v6

    add-int/2addr v0, v10

    int-to-float v1, v0

    sget v0, Lcom/android/calendar/month/MonthWeekEventsView;->DNA_MARGIN:I

    int-to-float v2, v0

    iget-object v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mDayXs:[I

    aget v0, v0, v6

    add-int/2addr v0, v10

    int-to-float v3, v0

    sget v0, Lcom/android/calendar/month/MonthWeekEventsView;->DNA_MARGIN:I

    sget v4, Lcom/android/calendar/month/MonthWeekEventsView;->DNA_ALL_DAY_HEIGHT:I

    add-int/2addr v0, v4

    int-to-float v4, v0

    iget-object v5, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mDNAAllDayPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    :cond_5
    add-int/lit8 v6, v6, 0x1

    goto :goto_1
.end method

.method protected drawDaySeparators(Landroid/graphics/Canvas;)V
    .locals 12
    .param p1    # Landroid/graphics/Canvas;

    const/4 v11, 0x0

    const/16 v9, 0x20

    new-array v3, v9, [F

    const/16 v0, 0x18

    const/4 v4, 0x0

    const/4 v1, 0x0

    iget-boolean v9, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mShowWeekNum:Z

    if-eqz v9, :cond_0

    sget v9, Lcom/android/calendar/month/MonthWeekEventsView;->SPACING_WEEK_NUMBER:I

    iget v10, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mPadding:I

    add-int v6, v9, v10

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v2, v1, 0x1

    int-to-float v9, v6

    aput v9, v3, v1

    add-int/lit8 v1, v2, 0x1

    aput v11, v3, v2

    add-int/lit8 v2, v1, 0x1

    int-to-float v9, v6

    aput v9, v3, v1

    add-int/lit8 v1, v2, 0x1

    iget v9, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mHeight:I

    int-to-float v9, v9

    aput v9, v3, v2

    add-int/lit8 v4, v4, 0x1

    :cond_0
    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v2, v1, 0x1

    aput v11, v3, v1

    add-int/lit8 v1, v2, 0x1

    aput v11, v3, v2

    add-int/lit8 v2, v1, 0x1

    iget v9, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mWidth:I

    int-to-float v9, v9

    aput v9, v3, v1

    add-int/lit8 v1, v2, 0x1

    aput v11, v3, v2

    const/4 v7, 0x0

    iget v8, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mHeight:I

    move v2, v1

    :goto_0
    if-ge v2, v0, :cond_1

    div-int/lit8 v9, v2, 0x4

    sub-int/2addr v9, v4

    invoke-direct {p0, v9}, Lcom/android/calendar/month/MonthWeekEventsView;->computeDayLeftPosition(I)I

    move-result v5

    add-int/lit8 v1, v2, 0x1

    int-to-float v9, v5

    aput v9, v3, v2

    add-int/lit8 v2, v1, 0x1

    int-to-float v9, v7

    aput v9, v3, v1

    add-int/lit8 v1, v2, 0x1

    int-to-float v9, v5

    aput v9, v3, v2

    add-int/lit8 v2, v1, 0x1

    int-to-float v9, v8

    aput v9, v3, v1

    goto :goto_0

    :cond_1
    iget-object v9, p0, Lcom/android/calendar/month/MonthWeekEventsView;->p:Landroid/graphics/Paint;

    iget v10, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mDaySeparatorInnerColor:I

    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v9, p0, Lcom/android/calendar/month/MonthWeekEventsView;->p:Landroid/graphics/Paint;

    sget v10, Lcom/android/calendar/month/MonthWeekEventsView;->DAY_SEPARATOR_INNER_WIDTH:I

    int-to-float v10, v10

    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/android/calendar/month/MonthWeekEventsView;->p:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v9, v0, v10}, Landroid/graphics/Canvas;->drawLines([FIILandroid/graphics/Paint;)V

    return-void
.end method

.method protected drawEvent(Landroid/graphics/Canvas;Lcom/android/calendar/Event;IIIZZZ)I
    .locals 16

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_SQUARE_BORDER:I

    add-int/lit8 v5, v1, 0x1

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_SQUARE_BORDER:I

    div-int/lit8 v6, v1, 0x2

    move-object/from16 v0, p2

    iget-boolean v7, v0, Lcom/android/calendar/Event;->allDay:Z

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventHeight:I

    if-eqz v7, :cond_1

    mul-int/lit8 v2, v5, 0x2

    add-int/2addr v1, v2

    :cond_0
    :goto_0
    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_BOTTOM_PADDING:I

    if-eqz p6, :cond_e

    sget v3, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_LINE_PADDING:I

    add-int/2addr v3, v1

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/calendar/month/MonthWeekEventsView;->mExtrasHeight:I

    add-int/2addr v1, v2

    move v2, v3

    :goto_1
    add-int v3, p4, v2

    add-int/2addr v1, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/month/MonthWeekEventsView;->mHeight:I

    if-le v1, v3, :cond_2

    :goto_2
    return p4

    :cond_1
    if-eqz p7, :cond_0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/month/MonthWeekEventsView;->mExtrasHeight:I

    add-int/2addr v1, v2

    goto :goto_0

    :cond_2
    if-nez p8, :cond_3

    add-int p4, p4, v2

    goto :goto_2

    :cond_3
    move-object/from16 v0, p2

    iget v1, v0, Lcom/android/calendar/Event;->selfAttendeeStatus:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_6

    const/4 v1, 0x1

    move v9, v1

    :goto_3
    move-object/from16 v0, p2

    iget v1, v0, Lcom/android/calendar/Event;->color:I

    if-eqz v9, :cond_d

    invoke-static {v1}, Lcom/android/calendar/Utils;->getDeclinedColorFromColor(I)I

    move-result v1

    move v4, v1

    :goto_4
    if-eqz v7, :cond_7

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/month/MonthWeekEventsView;->r:Landroid/graphics/Rect;

    move/from16 v0, p3

    iput v0, v1, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/month/MonthWeekEventsView;->r:Landroid/graphics/Rect;

    sub-int v2, p5, v6

    iput v2, v1, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/month/MonthWeekEventsView;->r:Landroid/graphics/Rect;

    add-int v2, p4, v6

    iput v2, v1, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/month/MonthWeekEventsView;->r:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventHeight:I

    add-int v2, v2, p4

    mul-int/lit8 v3, v5, 0x2

    add-int/2addr v2, v3

    sub-int/2addr v2, v6

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    add-int v2, p3, v5

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventAscentHeight:I

    add-int v1, v1, p4

    add-int/2addr v1, v5

    sub-int p5, p5, v5

    move v10, v2

    :goto_5
    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    const/4 v2, 0x0

    move-object/from16 v0, p2

    iget v6, v0, Lcom/android/calendar/Event;->selfAttendeeStatus:I

    const/4 v8, 0x3

    if-eq v6, v8, :cond_4

    sget-object v3, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    if-eqz v7, :cond_4

    const/4 v2, 0x1

    :cond_4
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventSquarePaint:Landroid/graphics/Paint;

    invoke-virtual {v6, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventSquarePaint:Landroid/graphics/Paint;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/month/MonthWeekEventsView;->r:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventSquarePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v6}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    sub-int v3, p5, v10

    int-to-float v12, v3

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/android/calendar/Event;->title:Ljava/lang/CharSequence;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventPaint:Landroid/text/TextPaint;

    sget-object v8, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v3, v6, v12, v8}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v3

    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/MonthWeekEventsView;->mSolidBackgroundEventPaint:Landroid/text/TextPaint;

    :goto_6
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    int-to-float v4, v10

    int-to-float v1, v1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v4, v1, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventHeight:I

    add-int v11, p4, v1

    if-eqz v7, :cond_5

    mul-int/lit8 v1, v5, 0x2

    add-int/2addr v11, v1

    :cond_5
    if-eqz p7, :cond_c

    if-nez v7, :cond_c

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/calendar/month/MonthWeekEventsView;->mExtrasAscentHeight:I

    add-int v13, v11, v1

    sget-object v1, Lcom/android/calendar/month/MonthWeekEventsView;->mStringBuilder:Ljava/lang/StringBuilder;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/month/MonthWeekEventsView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/android/calendar/month/MonthWeekEventsView;->mFormatter:Ljava/util/Formatter;

    move-object/from16 v0, p2

    iget-wide v3, v0, Lcom/android/calendar/Event;->startMillis:J

    move-object/from16 v0, p2

    iget-wide v5, v0, Lcom/android/calendar/Event;->endMillis:J

    const v7, 0x80001

    invoke-virtual/range {p0 .. p0}, Lcom/android/calendar/month/MonthWeekEventsView;->getContext()Landroid/content/Context;

    move-result-object v8

    const/4 v14, 0x0

    invoke-static {v8, v14}, Lcom/android/calendar/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v8

    invoke-static/range {v1 .. v8}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;Ljava/util/Formatter;JJILjava/lang/String;)Ljava/util/Formatter;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventExtrasPaint:Landroid/text/TextPaint;

    sget-object v3, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v1, v2, v12, v3}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    int-to-float v3, v10

    int-to-float v4, v13

    if-eqz v9, :cond_b

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventDeclinedExtrasPaint:Landroid/text/TextPaint;

    :goto_7
    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/calendar/month/MonthWeekEventsView;->mExtrasHeight:I

    add-int/2addr v1, v11

    :goto_8
    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_LINE_PADDING:I

    add-int p4, v1, v2

    goto/16 :goto_2

    :cond_6
    const/4 v1, 0x0

    move v9, v1

    goto/16 :goto_3

    :cond_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/month/MonthWeekEventsView;->r:Landroid/graphics/Rect;

    move/from16 v0, p3

    iput v0, v1, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/month/MonthWeekEventsView;->r:Landroid/graphics/Rect;

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_SQUARE_WIDTH:I

    add-int v2, v2, p3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/month/MonthWeekEventsView;->r:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventAscentHeight:I

    add-int v2, v2, p4

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/month/MonthWeekEventsView;->r:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/MonthWeekEventsView;->r:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    sget v3, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_SQUARE_WIDTH:I

    sub-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->top:I

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_SQUARE_WIDTH:I

    add-int v1, v1, p3

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_RIGHT_PADDING:I

    add-int/2addr v2, v1

    move-object/from16 v0, p0

    iget v1, v0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventAscentHeight:I

    add-int v1, v1, p4

    move v10, v2

    goto/16 :goto_5

    :cond_8
    if-eqz v9, :cond_9

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/MonthWeekEventsView;->mDeclinedEventPaint:Landroid/text/TextPaint;

    goto/16 :goto_6

    :cond_9
    if-eqz v7, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/MonthWeekEventsView;->mFramedEventPaint:Landroid/text/TextPaint;

    invoke-virtual {v2, v4}, Landroid/text/TextPaint;->setColor(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/MonthWeekEventsView;->mFramedEventPaint:Landroid/text/TextPaint;

    goto/16 :goto_6

    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventPaint:Landroid/text/TextPaint;

    goto/16 :goto_6

    :cond_b
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventExtrasPaint:Landroid/text/TextPaint;

    goto :goto_7

    :cond_c
    move v1, v11

    goto :goto_8

    :cond_d
    move v4, v1

    goto/16 :goto_4

    :cond_e
    move v15, v2

    move v2, v1

    move v1, v15

    goto/16 :goto_1
.end method

.method protected drawEvents(Landroid/graphics/Canvas;)V
    .locals 27
    .param p1    # Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/month/MonthWeekEventsView;->mEvents:Ljava/util/List;

    if-nez v3, :cond_1

    :cond_0
    return-void

    :cond_1
    const/16 v20, -0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/android/calendar/month/MonthWeekEventsView;->mEvents:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v23

    :cond_2
    :goto_0
    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface/range {v23 .. v23}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v22

    check-cast v22, Ljava/util/ArrayList;

    add-int/lit8 v20, v20, 0x1

    if-eqz v22, :cond_2

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-direct {v0, v1}, Lcom/android/calendar/month/MonthWeekEventsView;->computeDayLeftPosition(I)I

    move-result v3

    sget v4, Lcom/android/calendar/month/MonthWeekEventsView;->SIDE_PADDING_MONTH_NUMBER:I

    add-int/2addr v3, v4

    add-int/lit8 v6, v3, 0x1

    add-int/lit8 v3, v20, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/android/calendar/month/MonthWeekEventsView;->computeDayLeftPosition(I)I

    move-result v8

    move-object/from16 v0, p0

    iget v3, v0, Lcom/android/calendar/month/MonthWeekEventsView;->mOrientation:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_5

    sget v3, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_Y_OFFSET_PORTRAIT:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthNumHeight:I

    add-int/2addr v3, v4

    sget v4, Lcom/android/calendar/month/MonthWeekEventsView;->TOP_PADDING_MONTH_NUMBER:I

    add-int v15, v3, v4

    sget v3, Lcom/android/calendar/month/MonthWeekEventsView;->SIDE_PADDING_MONTH_NUMBER:I

    add-int/lit8 v3, v3, 0x1

    sub-int/2addr v8, v3

    :goto_1
    const/4 v10, 0x1

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v24

    move v7, v15

    :goto_2
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/calendar/Event;

    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    const/4 v11, 0x0

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    invoke-virtual/range {v3 .. v11}, Lcom/android/calendar/month/MonthWeekEventsView;->drawEvent(Landroid/graphics/Canvas;Lcom/android/calendar/Event;IIIZZZ)I

    move-result v25

    move/from16 v0, v25

    if-ne v0, v7, :cond_6

    const/4 v10, 0x0

    :cond_3
    const/16 v21, 0x0

    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v24

    :goto_3
    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/android/calendar/Event;

    invoke-interface/range {v24 .. v24}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    const/16 v19, 0x1

    move-object/from16 v11, p0

    move-object/from16 v12, p1

    move-object v13, v5

    move v14, v6

    move/from16 v16, v8

    move/from16 v18, v10

    invoke-virtual/range {v11 .. v19}, Lcom/android/calendar/month/MonthWeekEventsView;->drawEvent(Landroid/graphics/Canvas;Lcom/android/calendar/Event;IIIZZZ)I

    move-result v25

    move/from16 v0, v25

    if-ne v0, v15, :cond_7

    :cond_4
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v3

    sub-int v26, v3, v21

    if-lez v26, :cond_2

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v26

    invoke-virtual {v0, v1, v2, v6}, Lcom/android/calendar/month/MonthWeekEventsView;->drawMoreEvents(Landroid/graphics/Canvas;II)V

    goto/16 :goto_0

    :cond_5
    sget v15, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_Y_OFFSET_LANDSCAPE:I

    sget v3, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_X_OFFSET_LANDSCAPE:I

    sub-int/2addr v8, v3

    goto :goto_1

    :cond_6
    move/from16 v7, v25

    goto :goto_2

    :cond_7
    add-int/lit8 v21, v21, 0x1

    move/from16 v15, v25

    goto :goto_3
.end method

.method protected drawMoreEvents(Landroid/graphics/Canvas;II)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x1

    iget v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mHeight:I

    iget v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mExtrasDescent:I

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_BOTTOM_PADDING:I

    add-int/2addr v1, v2

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/android/calendar/month/MonthWeekEventsView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0005    # com.android.calendar.R.plurals.month_more_events

    invoke-virtual {v1, v2, p2}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventExtrasPaint:Landroid/text/TextPaint;

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    iget-object v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventExtrasPaint:Landroid/text/TextPaint;

    invoke-virtual {v2, v3}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    int-to-float v2, p3

    int-to-float v0, v0

    iget-object v3, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventExtrasPaint:Landroid/text/TextPaint;

    invoke-virtual {p1, v1, v2, v0, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventExtrasPaint:Landroid/text/TextPaint;

    invoke-virtual {v0, v4}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    return-void
.end method

.method protected drawToday(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1    # Landroid/graphics/Canvas;

    const/high16 v4, 0x40000000    # 2.0f

    iget-object v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->r:Landroid/graphics/Rect;

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->DAY_SEPARATOR_INNER_WIDTH:I

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->TODAY_HIGHLIGHT_WIDTH:I

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    iget-object v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->r:Landroid/graphics/Rect;

    iget v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mHeight:I

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->TODAY_HIGHLIGHT_WIDTH:I

    int-to-float v2, v2

    div-float/2addr v2, v4

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->p:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->p:Landroid/graphics/Paint;

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->TODAY_HIGHLIGHT_WIDTH:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->r:Landroid/graphics/Rect;

    iget v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mTodayIndex:I

    invoke-direct {p0, v1}, Lcom/android/calendar/month/MonthWeekEventsView;->computeDayLeftPosition(I)I

    move-result v1

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->TODAY_HIGHLIGHT_WIDTH:I

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    iget-object v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->r:Landroid/graphics/Rect;

    iget v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mTodayIndex:I

    add-int/lit8 v1, v1, 0x1

    invoke-direct {p0, v1}, Lcom/android/calendar/month/MonthWeekEventsView;->computeDayLeftPosition(I)I

    move-result v1

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->TODAY_HIGHLIGHT_WIDTH:I

    int-to-float v2, v2

    div-float/2addr v2, v4

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    iget-object v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->p:Landroid/graphics/Paint;

    iget v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mTodayAnimateColor:I

    iget v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mAnimateTodayAlpha:I

    shl-int/lit8 v2, v2, 0x18

    or-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->r:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->p:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->p:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    return-void
.end method

.method protected drawWeekNums(Landroid/graphics/Canvas;)V
    .locals 13
    .param p1    # Landroid/graphics/Canvas;

    const/4 v9, 0x0

    const/4 v0, 0x0

    const/4 v4, -0x1

    iget v5, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mTodayIndex:I

    const/4 v6, 0x0

    iget v3, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mNumDays:I

    iget-boolean v8, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mShowWeekNum:Z

    if-eqz v8, :cond_0

    sget v8, Lcom/android/calendar/month/MonthWeekEventsView;->SIDE_PADDING_WEEK_NUMBER:I

    iget v10, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mPadding:I

    add-int v6, v8, v10

    iget v8, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mWeekNumAscentHeight:I

    sget v10, Lcom/android/calendar/month/MonthWeekEventsView;->TOP_PADDING_WEEK_NUMBER:I

    add-int v7, v8, v10

    iget-object v8, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mDayNumbers:[Ljava/lang/String;

    aget-object v8, v8, v9

    int-to-float v10, v6

    int-to-float v11, v7

    iget-object v12, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mWeekNumPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v8, v10, v11, v12}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v5, v5, 0x1

    add-int/lit8 v4, v4, 0x1

    :cond_0
    iget v8, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthNumAscentHeight:I

    sget v10, Lcom/android/calendar/month/MonthWeekEventsView;->TOP_PADDING_MONTH_NUMBER:I

    add-int v7, v8, v10

    iget-object v8, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mFocusDay:[Z

    aget-boolean v2, v8, v0

    const/4 v1, 0x0

    iget-object v10, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthNumPaint:Landroid/graphics/Paint;

    if-eqz v2, :cond_3

    iget v8, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthNumColor:I

    :goto_0
    invoke-virtual {v10, v8}, Landroid/graphics/Paint;->setColor(I)V

    :goto_1
    if-ge v0, v3, :cond_7

    iget-boolean v8, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mHasToday:Z

    if-eqz v8, :cond_5

    if-ne v5, v0, :cond_5

    iget-object v8, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthNumPaint:Landroid/graphics/Paint;

    iget v10, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthNumTodayColor:I

    invoke-virtual {v8, v10}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v8, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthNumPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v8, v1}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    add-int/lit8 v8, v0, 0x1

    if-ge v8, v3, :cond_1

    iget-object v8, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mFocusDay:[Z

    add-int/lit8 v10, v0, 0x1

    aget-boolean v8, v8, v10

    if-nez v8, :cond_4

    const/4 v2, 0x1

    :cond_1
    :goto_2
    sub-int v8, v0, v4

    invoke-direct {p0, v8}, Lcom/android/calendar/month/MonthWeekEventsView;->computeDayLeftPosition(I)I

    move-result v8

    sget v10, Lcom/android/calendar/month/MonthWeekEventsView;->SIDE_PADDING_MONTH_NUMBER:I

    sub-int v6, v8, v10

    iget-object v8, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mDayNumbers:[Ljava/lang/String;

    aget-object v8, v8, v0

    int-to-float v10, v6

    int-to-float v11, v7

    iget-object v12, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthNumPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v8, v10, v11, v12}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    if-eqz v1, :cond_2

    iget-object v8, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthNumPaint:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v8, v1}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget v8, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthNumOtherColor:I

    goto :goto_0

    :cond_4
    move v2, v9

    goto :goto_2

    :cond_5
    iget-object v8, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mFocusDay:[Z

    aget-boolean v8, v8, v0

    if-eq v8, v2, :cond_1

    iget-object v8, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mFocusDay:[Z

    aget-boolean v2, v8, v0

    iget-object v10, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthNumPaint:Landroid/graphics/Paint;

    if-eqz v2, :cond_6

    iget v8, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthNumColor:I

    :goto_3
    invoke-virtual {v10, v8}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_2

    :cond_6
    iget v8, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthNumOtherColor:I

    goto :goto_3

    :cond_7
    return-void
.end method

.method public getDayFromLocation(F)Landroid/text/format/Time;
    .locals 5

    const v4, 0x253d8c    # 3.419992E-39f

    const/4 v3, 0x1

    invoke-virtual {p0, p1}, Lcom/android/calendar/month/MonthWeekEventsView;->getDayIndexFromLocation(F)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mFirstJulianDay:I

    add-int/2addr v0, v1

    new-instance v1, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mTimeZone:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mWeek:I

    if-nez v2, :cond_1

    if-ge v0, v4, :cond_2

    add-int/lit8 v0, v0, 0x1

    :cond_1
    invoke-virtual {v1, v0}, Landroid/text/format/Time;->setJulianDay(I)J

    move-object v0, v1

    goto :goto_0

    :cond_2
    if-ne v0, v4, :cond_1

    const/4 v0, 0x0

    const/16 v2, 0x7b2

    invoke-virtual {v1, v3, v0, v2}, Landroid/text/format/Time;->set(III)V

    invoke-virtual {v1, v3}, Landroid/text/format/Time;->normalize(Z)J

    move-object v0, v1

    goto :goto_0
.end method

.method public getDayIndexFromLocation(F)I
    .locals 4
    .param p1    # F

    iget-boolean v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mShowWeekNum:Z

    if-eqz v1, :cond_1

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->SPACING_WEEK_NUMBER:I

    iget v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mPadding:I

    add-int v0, v1, v2

    :goto_0
    int-to-float v1, v0

    cmpg-float v1, p1, v1

    if-ltz v1, :cond_0

    iget v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mWidth:I

    iget v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mPadding:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    cmpl-float v1, p1, v1

    if-lez v1, :cond_2

    :cond_0
    const/4 v1, -0x1

    :goto_1
    return v1

    :cond_1
    iget v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mPadding:I

    goto :goto_0

    :cond_2
    int-to-float v1, v0

    sub-float v1, p1, v1

    iget v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mNumDays:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mWidth:I

    sub-int/2addr v2, v0

    iget v3, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mPadding:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    div-float/2addr v1, v2

    float-to-int v1, v1

    goto :goto_1
.end method

.method protected initView()V
    .locals 7

    const/4 v6, 0x0

    const/high16 v5, 0x3f000000    # 0.5f

    const/4 v4, 0x1

    invoke-super {p0}, Lcom/android/calendar/month/SimpleWeekView;->initView()V

    sget-boolean v1, Lcom/android/calendar/month/MonthWeekEventsView;->mInitialized:Z

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/android/calendar/month/MonthWeekEventsView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0}, Lcom/android/calendar/month/MonthWeekEventsView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f090002    # com.android.calendar.R.bool.show_details_in_month

    invoke-static {v1, v2}, Lcom/android/calendar/Utils;->getConfigBool(Landroid/content/Context;I)Z

    move-result v1

    sput-boolean v1, Lcom/android/calendar/month/MonthWeekEventsView;->mShowDetailsInMonth:Z

    const/high16 v1, 0x7f0b0000    # com.android.calendar.R.integer.text_size_event_title

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->TEXT_SIZE_EVENT_TITLE:I

    const v1, 0x7f0b0001    # com.android.calendar.R.integer.text_size_month_number

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->TEXT_SIZE_MONTH_NUMBER:I

    const v1, 0x7f0b0005    # com.android.calendar.R.integer.month_day_number_margin

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->SIDE_PADDING_MONTH_NUMBER:I

    const v1, 0x7f080029    # com.android.calendar.R.color.month_dna_conflict_time_color

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->CONFLICT_COLOR:I

    const v1, 0x7f080006    # com.android.calendar.R.color.calendar_event_text_color

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_TEXT_COLOR:I

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->mScale:F

    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_0

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->TOP_PADDING_MONTH_NUMBER:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->mScale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->TOP_PADDING_MONTH_NUMBER:I

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->TOP_PADDING_WEEK_NUMBER:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->mScale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->TOP_PADDING_WEEK_NUMBER:I

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->SIDE_PADDING_MONTH_NUMBER:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->mScale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->SIDE_PADDING_MONTH_NUMBER:I

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->SIDE_PADDING_WEEK_NUMBER:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->mScale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->SIDE_PADDING_WEEK_NUMBER:I

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->SPACING_WEEK_NUMBER:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->mScale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->SPACING_WEEK_NUMBER:I

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->TEXT_SIZE_MONTH_NUMBER:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->mScale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->TEXT_SIZE_MONTH_NUMBER:I

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->TEXT_SIZE_EVENT:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->mScale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->TEXT_SIZE_EVENT:I

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->TEXT_SIZE_EVENT_TITLE:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->mScale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->TEXT_SIZE_EVENT_TITLE:I

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->TEXT_SIZE_MORE_EVENTS:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->mScale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->TEXT_SIZE_MORE_EVENTS:I

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->TEXT_SIZE_MONTH_NAME:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->mScale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->TEXT_SIZE_MONTH_NAME:I

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->TEXT_SIZE_WEEK_NUM:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->mScale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->TEXT_SIZE_WEEK_NUM:I

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->DAY_SEPARATOR_OUTER_WIDTH:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->mScale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->DAY_SEPARATOR_OUTER_WIDTH:I

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->DAY_SEPARATOR_INNER_WIDTH:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->mScale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->DAY_SEPARATOR_INNER_WIDTH:I

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->DAY_SEPARATOR_VERTICAL_LENGTH:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->mScale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->DAY_SEPARATOR_VERTICAL_LENGTH:I

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->DAY_SEPARATOR_VERTICAL_LENGHT_PORTRAIT:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->mScale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->DAY_SEPARATOR_VERTICAL_LENGHT_PORTRAIT:I

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_X_OFFSET_LANDSCAPE:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->mScale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_X_OFFSET_LANDSCAPE:I

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_Y_OFFSET_LANDSCAPE:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->mScale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_Y_OFFSET_LANDSCAPE:I

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_Y_OFFSET_PORTRAIT:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->mScale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_Y_OFFSET_PORTRAIT:I

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_SQUARE_WIDTH:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->mScale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_SQUARE_WIDTH:I

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_SQUARE_BORDER:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->mScale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_SQUARE_BORDER:I

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_LINE_PADDING:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->mScale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_LINE_PADDING:I

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_BOTTOM_PADDING:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->mScale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_BOTTOM_PADDING:I

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_RIGHT_PADDING:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->mScale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_RIGHT_PADDING:I

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->DNA_MARGIN:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->mScale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->DNA_MARGIN:I

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->DNA_WIDTH:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->mScale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->DNA_WIDTH:I

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->DNA_ALL_DAY_HEIGHT:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->mScale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->DNA_ALL_DAY_HEIGHT:I

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->DNA_MIN_SEGMENT_HEIGHT:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->mScale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->DNA_MIN_SEGMENT_HEIGHT:I

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->DNA_SIDE_PADDING:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->mScale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->DNA_SIDE_PADDING:I

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->DEFAULT_EDGE_SPACING:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->mScale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->DEFAULT_EDGE_SPACING:I

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->DNA_ALL_DAY_WIDTH:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->mScale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->DNA_ALL_DAY_WIDTH:I

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->TODAY_HIGHLIGHT_WIDTH:I

    int-to-float v1, v1

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->mScale:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->TODAY_HIGHLIGHT_WIDTH:I

    :cond_0
    sget-boolean v1, Lcom/android/calendar/month/MonthWeekEventsView;->mShowDetailsInMonth:Z

    if-nez v1, :cond_1

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->TOP_PADDING_MONTH_NUMBER:I

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->DNA_ALL_DAY_HEIGHT:I

    sget v3, Lcom/android/calendar/month/MonthWeekEventsView;->DNA_MARGIN:I

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    sput v1, Lcom/android/calendar/month/MonthWeekEventsView;->TOP_PADDING_MONTH_NUMBER:I

    :cond_1
    sput-boolean v4, Lcom/android/calendar/month/MonthWeekEventsView;->mInitialized:Z

    :cond_2
    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->DEFAULT_EDGE_SPACING:I

    iput v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mPadding:I

    invoke-virtual {p0}, Lcom/android/calendar/month/MonthWeekEventsView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/android/calendar/month/MonthWeekEventsView;->loadColors(Landroid/content/Context;)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthNumPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthNumPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v6}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthNumPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthNumPaint:Landroid/graphics/Paint;

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->TEXT_SIZE_MONTH_NUMBER:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthNumPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthNumColor:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthNumPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthNumPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthNumPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthNumPaint:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->ascent()F

    move-result v1

    neg-float v1, v1

    add-float/2addr v1, v5

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthNumAscentHeight:I

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthNumPaint:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->descent()F

    move-result v1

    iget-object v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthNumPaint:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->ascent()F

    move-result v2

    sub-float/2addr v1, v2

    add-float/2addr v1, v5

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthNumHeight:I

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    iput-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventPaint:Landroid/text/TextPaint;

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v4}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v4}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventPaint:Landroid/text/TextPaint;

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->TEXT_SIZE_EVENT_TITLE:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventPaint:Landroid/text/TextPaint;

    iget v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthEventColor:I

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    new-instance v1, Landroid/text/TextPaint;

    iget-object v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventPaint:Landroid/text/TextPaint;

    invoke-direct {v1, v2}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    iput-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mSolidBackgroundEventPaint:Landroid/text/TextPaint;

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mSolidBackgroundEventPaint:Landroid/text/TextPaint;

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_TEXT_COLOR:I

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    new-instance v1, Landroid/text/TextPaint;

    iget-object v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mSolidBackgroundEventPaint:Landroid/text/TextPaint;

    invoke-direct {v1, v2}, Landroid/text/TextPaint;-><init>(Landroid/graphics/Paint;)V

    iput-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mFramedEventPaint:Landroid/text/TextPaint;

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    iput-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mDeclinedEventPaint:Landroid/text/TextPaint;

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mDeclinedEventPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v4}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mDeclinedEventPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v4}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mDeclinedEventPaint:Landroid/text/TextPaint;

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->TEXT_SIZE_EVENT_TITLE:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mDeclinedEventPaint:Landroid/text/TextPaint;

    iget v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthDeclinedEventColor:I

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventPaint:Landroid/text/TextPaint;

    invoke-virtual {v1}, Landroid/text/TextPaint;->ascent()F

    move-result v1

    neg-float v1, v1

    add-float/2addr v1, v5

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventAscentHeight:I

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventPaint:Landroid/text/TextPaint;

    invoke-virtual {v1}, Landroid/text/TextPaint;->descent()F

    move-result v1

    iget-object v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventPaint:Landroid/text/TextPaint;

    invoke-virtual {v2}, Landroid/text/TextPaint;->ascent()F

    move-result v2

    sub-float/2addr v1, v2

    add-float/2addr v1, v5

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventHeight:I

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    iput-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventExtrasPaint:Landroid/text/TextPaint;

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventExtrasPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v6}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventExtrasPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v4}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventExtrasPaint:Landroid/text/TextPaint;

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_SQUARE_BORDER:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setStrokeWidth(F)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventExtrasPaint:Landroid/text/TextPaint;

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->TEXT_SIZE_EVENT:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventExtrasPaint:Landroid/text/TextPaint;

    iget v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthEventExtraColor:I

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventExtrasPaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventExtrasPaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventExtrasPaint:Landroid/text/TextPaint;

    invoke-virtual {v1}, Landroid/text/TextPaint;->descent()F

    move-result v1

    iget-object v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventExtrasPaint:Landroid/text/TextPaint;

    invoke-virtual {v2}, Landroid/text/TextPaint;->ascent()F

    move-result v2

    sub-float/2addr v1, v2

    add-float/2addr v1, v5

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mExtrasHeight:I

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventExtrasPaint:Landroid/text/TextPaint;

    invoke-virtual {v1}, Landroid/text/TextPaint;->ascent()F

    move-result v1

    neg-float v1, v1

    add-float/2addr v1, v5

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mExtrasAscentHeight:I

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventExtrasPaint:Landroid/text/TextPaint;

    invoke-virtual {v1}, Landroid/text/TextPaint;->descent()F

    move-result v1

    add-float/2addr v1, v5

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mExtrasDescent:I

    new-instance v1, Landroid/text/TextPaint;

    invoke-direct {v1}, Landroid/text/TextPaint;-><init>()V

    iput-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventDeclinedExtrasPaint:Landroid/text/TextPaint;

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventDeclinedExtrasPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v6}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventDeclinedExtrasPaint:Landroid/text/TextPaint;

    invoke-virtual {v1, v4}, Landroid/text/TextPaint;->setAntiAlias(Z)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventDeclinedExtrasPaint:Landroid/text/TextPaint;

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_SQUARE_BORDER:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setStrokeWidth(F)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventDeclinedExtrasPaint:Landroid/text/TextPaint;

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->TEXT_SIZE_EVENT:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextSize(F)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventDeclinedExtrasPaint:Landroid/text/TextPaint;

    iget v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthDeclinedExtrasColor:I

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setColor(I)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventDeclinedExtrasPaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventDeclinedExtrasPaint:Landroid/text/TextPaint;

    sget-object v2, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v1, v2}, Landroid/text/TextPaint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mWeekNumPaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mWeekNumPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v6}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mWeekNumPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mWeekNumPaint:Landroid/graphics/Paint;

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->TEXT_SIZE_WEEK_NUM:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mWeekNumPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mWeekNumColor:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mWeekNumPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mWeekNumPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mWeekNumPaint:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->ascent()F

    move-result v1

    neg-float v1, v1

    add-float/2addr v1, v5

    float-to-int v1, v1

    iput v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mWeekNumAscentHeight:I

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mDNAAllDayPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mDNATimePaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mDNATimePaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthBusyBitsBusyTimeColor:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mDNATimePaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mDNATimePaint:Landroid/graphics/Paint;

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->DNA_WIDTH:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mDNATimePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v6}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mDNAAllDayPaint:Landroid/graphics/Paint;

    iget v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthBusyBitsConflictTimeColor:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mDNAAllDayPaint:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mDNAAllDayPaint:Landroid/graphics/Paint;

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->DNA_ALL_DAY_WIDTH:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mDNAAllDayPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v6}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventSquarePaint:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventSquarePaint:Landroid/graphics/Paint;

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->EVENT_SQUARE_BORDER:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEventSquarePaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v6}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    return-void
.end method

.method protected loadColors(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f08001a    # com.android.calendar.R.color.month_week_num_color

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthWeekNumColor:I

    const v1, 0x7f080017    # com.android.calendar.R.color.month_day_number

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthNumColor:I

    const v1, 0x7f080019    # com.android.calendar.R.color.month_day_number_other

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthNumOtherColor:I

    const v1, 0x7f08001c    # com.android.calendar.R.color.month_today_number

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthNumTodayColor:I

    iget v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthNumColor:I

    iput v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthNameColor:I

    iget v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthNumOtherColor:I

    iput v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthNameOtherColor:I

    const v1, 0x7f08001d    # com.android.calendar.R.color.month_event_color

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthEventColor:I

    const v1, 0x7f080031    # com.android.calendar.R.color.agenda_item_declined_color

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthDeclinedEventColor:I

    const v1, 0x7f080039    # com.android.calendar.R.color.agenda_item_where_declined_text_color

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthDeclinedExtrasColor:I

    const v1, 0x7f08001e    # com.android.calendar.R.color.month_event_extra_color

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthEventExtraColor:I

    const v1, 0x7f08001f    # com.android.calendar.R.color.month_event_other_color

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthEventOtherColor:I

    const v1, 0x7f080020    # com.android.calendar.R.color.month_event_extra_other_color

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthEventExtraOtherColor:I

    const v1, 0x7f080027    # com.android.calendar.R.color.month_today_bgcolor

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthBGTodayColor:I

    const v1, 0x7f080025    # com.android.calendar.R.color.month_other_bgcolor

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthBGOtherColor:I

    const v1, 0x7f080028    # com.android.calendar.R.color.month_bgcolor

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mMonthBGColor:I

    const v1, 0x7f080023    # com.android.calendar.R.color.month_grid_lines

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mDaySeparatorInnerColor:I

    const v1, 0x7f080002    # com.android.calendar.R.color.today_highlight_color

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mTodayAnimateColor:I

    const v1, 0x7f080041    # com.android.calendar.R.color.day_clicked_background_color

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mClickedDayColor:I

    const v1, 0x7f02005c    # com.android.calendar.R.drawable.today_blue_week_holo_light

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mTodayDrawable:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1    # Landroid/graphics/Canvas;

    invoke-virtual {p0, p1}, Lcom/android/calendar/month/MonthWeekEventsView;->drawBackground(Landroid/graphics/Canvas;)V

    invoke-virtual {p0, p1}, Lcom/android/calendar/month/MonthWeekEventsView;->drawWeekNums(Landroid/graphics/Canvas;)V

    invoke-virtual {p0, p1}, Lcom/android/calendar/month/MonthWeekEventsView;->drawDaySeparators(Landroid/graphics/Canvas;)V

    iget-boolean v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mHasToday:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mAnimateToday:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/android/calendar/month/MonthWeekEventsView;->drawToday(Landroid/graphics/Canvas;)V

    :cond_0
    sget-boolean v0, Lcom/android/calendar/month/MonthWeekEventsView;->mShowDetailsInMonth:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/android/calendar/month/MonthWeekEventsView;->drawEvents(Landroid/graphics/Canvas;)V

    :goto_0
    invoke-direct {p0, p1}, Lcom/android/calendar/month/MonthWeekEventsView;->drawClick(Landroid/graphics/Canvas;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mDna:Ljava/util/HashMap;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mUnsortedEvents:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mUnsortedEvents:Ljava/util/ArrayList;

    invoke-virtual {p0, v0}, Lcom/android/calendar/month/MonthWeekEventsView;->createDna(Ljava/util/ArrayList;)V

    :cond_2
    invoke-virtual {p0, p1}, Lcom/android/calendar/month/MonthWeekEventsView;->drawDNA(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 12

    const/4 v6, 0x1

    invoke-virtual {p0}, Lcom/android/calendar/month/MonthWeekEventsView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    invoke-super {p0, p1}, Lcom/android/calendar/month/SimpleWeekView;->onHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/16 v2, 0xa

    if-eq v1, v2, :cond_6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/android/calendar/month/MonthWeekEventsView;->getDayFromLocation(F)Landroid/text/format/Time;

    move-result-object v7

    if-eqz v7, :cond_6

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mLastHoverTime:Landroid/text/format/Time;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mLastHoverTime:Landroid/text/format/Time;

    invoke-static {v7, v1}, Landroid/text/format/Time;->compare(Landroid/text/format/Time;Landroid/text/format/Time;)I

    move-result v1

    if-eqz v1, :cond_6

    :cond_2
    invoke-virtual {v7, v6}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const/16 v5, 0x10

    invoke-static/range {v0 .. v5}, Lcom/android/calendar/Utils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x40

    invoke-static {v2}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-boolean v1, Lcom/android/calendar/month/MonthWeekEventsView;->mShowDetailsInMonth:Z

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEvents:Ljava/util/List;

    if-eqz v1, :cond_5

    sget v1, Lcom/android/calendar/month/MonthWeekEventsView;->SPACING_WEEK_NUMBER:I

    iget v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mPadding:I

    add-int/2addr v1, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    int-to-float v3, v1

    sub-float/2addr v2, v3

    iget v3, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mNumDays:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mWidth:I

    sub-int v1, v3, v1

    iget v3, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mPadding:I

    sub-int/2addr v1, v3

    int-to-float v1, v1

    div-float v1, v2, v1

    float-to-int v1, v1

    iget-object v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEvents:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v8}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v9

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Lcom/android/calendar/Event;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Lcom/android/calendar/Event;->getTitleAndLocation()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-boolean v1, v3, Lcom/android/calendar/Event;->allDay:Z

    if-nez v1, :cond_4

    const/16 v5, 0x15

    invoke-static {v0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v5, 0x95

    :cond_3
    :goto_2
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget-wide v1, v3, Lcom/android/calendar/Event;->startMillis:J

    iget-wide v3, v3, Lcom/android/calendar/Event;->endMillis:J

    invoke-static/range {v0 .. v5}, Lcom/android/calendar/Utils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v9, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    const/16 v5, 0x2014

    goto :goto_2

    :cond_5
    invoke-virtual {p0, v8}, Lcom/android/calendar/month/MonthWeekEventsView;->sendAccessibilityEventUnchecked(Landroid/view/accessibility/AccessibilityEvent;)V

    iput-object v7, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mLastHoverTime:Landroid/text/format/Time;

    :cond_6
    move v0, v6

    goto/16 :goto_0
.end method

.method public setAnimateTodayAlpha(I)V
    .locals 0
    .param p1    # I

    iput p1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mAnimateTodayAlpha:I

    invoke-virtual {p0}, Lcom/android/calendar/month/MonthWeekEventsView;->invalidate()V

    return-void
.end method

.method public setClickedDay(F)V
    .locals 1
    .param p1    # F

    invoke-virtual {p0, p1}, Lcom/android/calendar/month/MonthWeekEventsView;->getDayIndexFromLocation(F)I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mClickedDayIndex:I

    invoke-virtual {p0}, Lcom/android/calendar/month/MonthWeekEventsView;->invalidate()V

    return-void
.end method

.method public setEvents(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/calendar/Event;",
            ">;>;)V"
        }
    .end annotation

    iput-object p1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEvents:Ljava/util/List;

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iget v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mNumDays:I

    if-eq v0, v1, :cond_0

    const-string v0, "MonthView"

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "MonthView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Events size must be same as days displayed: size="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " days="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mNumDays:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mEvents:Ljava/util/List;

    goto :goto_0
.end method

.method public setEvents(Ljava/util/List;Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/calendar/Event;",
            ">;>;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/android/calendar/Event;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0, p1}, Lcom/android/calendar/month/MonthWeekEventsView;->setEvents(Ljava/util/List;)V

    invoke-virtual {p0, p2}, Lcom/android/calendar/month/MonthWeekEventsView;->createDna(Ljava/util/ArrayList;)V

    return-void
.end method

.method public setWeekParams(Ljava/util/HashMap;Ljava/lang/String;)V
    .locals 6
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-super {p0, p1, p2}, Lcom/android/calendar/month/SimpleWeekView;->setWeekParams(Ljava/util/HashMap;Ljava/lang/String;)V

    const-string v0, "orientation"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "orientation"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mOrientation:I

    :cond_0
    invoke-virtual {p0, p2}, Lcom/android/calendar/month/MonthWeekEventsView;->updateToday(Ljava/lang/String;)Z

    iget v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mNumDays:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mNumCells:I

    const-string v0, "animate_today"

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mHasToday:Z

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mAnimatorListener:Lcom/android/calendar/month/MonthWeekEventsView$TodayAnimatorListener;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mTodayAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mTodayAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->removeAllListeners()V

    iget-object v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mTodayAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    :cond_1
    const-string v0, "animateTodayAlpha"

    const/4 v2, 0x2

    new-array v2, v2, [I

    const/4 v3, 0x0

    iget v4, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mAnimateTodayAlpha:I

    const/16 v5, 0x50

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    aput v4, v2, v3

    const/4 v3, 0x1

    const/16 v4, 0xff

    aput v4, v2, v3

    invoke-static {p0, v0, v2}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mTodayAnimator:Landroid/animation/ObjectAnimator;

    iget-object v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mTodayAnimator:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0x96

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    iget-object v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mAnimatorListener:Lcom/android/calendar/month/MonthWeekEventsView$TodayAnimatorListener;

    iget-object v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mTodayAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, v2}, Lcom/android/calendar/month/MonthWeekEventsView$TodayAnimatorListener;->setAnimator(Landroid/animation/Animator;)V

    iget-object v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mAnimatorListener:Lcom/android/calendar/month/MonthWeekEventsView$TodayAnimatorListener;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/android/calendar/month/MonthWeekEventsView$TodayAnimatorListener;->setFadingIn(Z)V

    iget-object v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mTodayAnimator:Landroid/animation/ObjectAnimator;

    iget-object v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mAnimatorListener:Lcom/android/calendar/month/MonthWeekEventsView$TodayAnimatorListener;

    invoke-virtual {v0, v2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mAnimateToday:Z

    iget-object v0, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mTodayAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    monitor-exit v1

    :cond_2
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected updateSelectionPositions()V
    .locals 4

    iget-boolean v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mHasSelectedDay:Z

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mSelectedDay:I

    iget v3, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mWeekStart:I

    sub-int v1, v2, v3

    if-gez v1, :cond_0

    add-int/lit8 v1, v1, 0x7

    :cond_0
    iget v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mWidth:I

    iget v3, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mPadding:I

    mul-int/lit8 v3, v3, 0x2

    sub-int v0, v2, v3

    sget v2, Lcom/android/calendar/month/MonthWeekEventsView;->SPACING_WEEK_NUMBER:I

    sub-int/2addr v0, v2

    mul-int v2, v1, v0

    iget v3, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mNumDays:I

    div-int/2addr v2, v3

    iget v3, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mPadding:I

    add-int/2addr v2, v3

    iput v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mSelectedLeft:I

    add-int/lit8 v2, v1, 0x1

    mul-int/2addr v2, v0

    iget v3, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mNumDays:I

    div-int/2addr v2, v3

    iget v3, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mPadding:I

    add-int/2addr v2, v3

    iput v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mSelectedRight:I

    iget v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mSelectedLeft:I

    sget v3, Lcom/android/calendar/month/MonthWeekEventsView;->SPACING_WEEK_NUMBER:I

    add-int/2addr v2, v3

    iput v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mSelectedLeft:I

    iget v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mSelectedRight:I

    sget v3, Lcom/android/calendar/month/MonthWeekEventsView;->SPACING_WEEK_NUMBER:I

    add-int/2addr v2, v3

    iput v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mSelectedRight:I

    :cond_1
    return-void
.end method

.method public updateToday(Ljava/lang/String;)Z
    .locals 7
    .param p1    # Ljava/lang/String;

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mToday:Landroid/text/format/Time;

    iput-object p1, v1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mToday:Landroid/text/format/Time;

    invoke-virtual {v1}, Landroid/text/format/Time;->setToNow()V

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mToday:Landroid/text/format/Time;

    invoke-virtual {v1, v6}, Landroid/text/format/Time;->normalize(Z)J

    iget-object v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mToday:Landroid/text/format/Time;

    invoke-virtual {v1, v5}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v1

    iget-object v3, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mToday:Landroid/text/format/Time;

    iget-wide v3, v3, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v1, v2, v3, v4}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v0

    iget v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mFirstJulianDay:I

    if-lt v0, v1, :cond_0

    iget v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mFirstJulianDay:I

    iget v2, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mNumDays:I

    add-int/2addr v1, v2

    if-ge v0, v1, :cond_0

    iput-boolean v6, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mHasToday:Z

    iget v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mFirstJulianDay:I

    sub-int v1, v0, v1

    iput v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mTodayIndex:I

    :goto_0
    iget-boolean v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mHasToday:Z

    return v1

    :cond_0
    iput-boolean v5, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mHasToday:Z

    const/4 v1, -0x1

    iput v1, p0, Lcom/android/calendar/month/MonthWeekEventsView;->mTodayIndex:I

    goto :goto_0
.end method
