.class public Lcom/android/org/bouncycastle/asn1/cms/SignedData;
.super Lcom/android/org/bouncycastle/asn1/ASN1Object;
.source "SignedData.java"


# instance fields
.field private certificates:Lcom/android/org/bouncycastle/asn1/ASN1Set;

.field private certsBer:Z

.field private contentInfo:Lcom/android/org/bouncycastle/asn1/cms/ContentInfo;

.field private crls:Lcom/android/org/bouncycastle/asn1/ASN1Set;

.field private crlsBer:Z

.field private digestAlgorithms:Lcom/android/org/bouncycastle/asn1/ASN1Set;

.field private signerInfos:Lcom/android/org/bouncycastle/asn1/ASN1Set;

.field private version:Lcom/android/org/bouncycastle/asn1/ASN1Integer;


# direct methods
.method private constructor <init>(Lcom/android/org/bouncycastle/asn1/ASN1Sequence;)V
    .locals 6
    .param p1    # Lcom/android/org/bouncycastle/asn1/ASN1Sequence;

    const/4 v4, 0x0

    invoke-direct {p0}, Lcom/android/org/bouncycastle/asn1/ASN1Object;-><init>()V

    invoke-virtual {p1}, Lcom/android/org/bouncycastle/asn1/ASN1Sequence;->getObjects()Ljava/util/Enumeration;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Lcom/android/org/bouncycastle/asn1/ASN1Integer;->getInstance(Ljava/lang/Object;)Lcom/android/org/bouncycastle/asn1/ASN1Integer;

    move-result-object v3

    iput-object v3, p0, Lcom/android/org/bouncycastle/asn1/cms/SignedData;->version:Lcom/android/org/bouncycastle/asn1/ASN1Integer;

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/android/org/bouncycastle/asn1/ASN1Set;

    iput-object v3, p0, Lcom/android/org/bouncycastle/asn1/cms/SignedData;->digestAlgorithms:Lcom/android/org/bouncycastle/asn1/ASN1Set;

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Lcom/android/org/bouncycastle/asn1/cms/ContentInfo;->getInstance(Ljava/lang/Object;)Lcom/android/org/bouncycastle/asn1/cms/ContentInfo;

    move-result-object v3

    iput-object v3, p0, Lcom/android/org/bouncycastle/asn1/cms/SignedData;->contentInfo:Lcom/android/org/bouncycastle/asn1/cms/ContentInfo;

    :goto_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/android/org/bouncycastle/asn1/ASN1Primitive;

    instance-of v3, v1, Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;

    if-eqz v3, :cond_0

    move-object v2, v1

    check-cast v2, Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;

    invoke-virtual {v2}, Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;->getTagNo()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "unknown tag value "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;->getTagNo()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :pswitch_0
    instance-of v3, v2, Lcom/android/org/bouncycastle/asn1/BERTaggedObject;

    iput-boolean v3, p0, Lcom/android/org/bouncycastle/asn1/cms/SignedData;->certsBer:Z

    invoke-static {v2, v4}, Lcom/android/org/bouncycastle/asn1/ASN1Set;->getInstance(Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;Z)Lcom/android/org/bouncycastle/asn1/ASN1Set;

    move-result-object v3

    iput-object v3, p0, Lcom/android/org/bouncycastle/asn1/cms/SignedData;->certificates:Lcom/android/org/bouncycastle/asn1/ASN1Set;

    goto :goto_0

    :pswitch_1
    instance-of v3, v2, Lcom/android/org/bouncycastle/asn1/BERTaggedObject;

    iput-boolean v3, p0, Lcom/android/org/bouncycastle/asn1/cms/SignedData;->crlsBer:Z

    invoke-static {v2, v4}, Lcom/android/org/bouncycastle/asn1/ASN1Set;->getInstance(Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;Z)Lcom/android/org/bouncycastle/asn1/ASN1Set;

    move-result-object v3

    iput-object v3, p0, Lcom/android/org/bouncycastle/asn1/cms/SignedData;->crls:Lcom/android/org/bouncycastle/asn1/ASN1Set;

    goto :goto_0

    :cond_0
    check-cast v1, Lcom/android/org/bouncycastle/asn1/ASN1Set;

    iput-object v1, p0, Lcom/android/org/bouncycastle/asn1/cms/SignedData;->signerInfos:Lcom/android/org/bouncycastle/asn1/ASN1Set;

    goto :goto_0

    :cond_1
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public constructor <init>(Lcom/android/org/bouncycastle/asn1/ASN1Set;Lcom/android/org/bouncycastle/asn1/cms/ContentInfo;Lcom/android/org/bouncycastle/asn1/ASN1Set;Lcom/android/org/bouncycastle/asn1/ASN1Set;Lcom/android/org/bouncycastle/asn1/ASN1Set;)V
    .locals 1
    .param p1    # Lcom/android/org/bouncycastle/asn1/ASN1Set;
    .param p2    # Lcom/android/org/bouncycastle/asn1/cms/ContentInfo;
    .param p3    # Lcom/android/org/bouncycastle/asn1/ASN1Set;
    .param p4    # Lcom/android/org/bouncycastle/asn1/ASN1Set;
    .param p5    # Lcom/android/org/bouncycastle/asn1/ASN1Set;

    invoke-direct {p0}, Lcom/android/org/bouncycastle/asn1/ASN1Object;-><init>()V

    invoke-virtual {p2}, Lcom/android/org/bouncycastle/asn1/cms/ContentInfo;->getContentType()Lcom/android/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v0

    invoke-direct {p0, v0, p3, p4, p5}, Lcom/android/org/bouncycastle/asn1/cms/SignedData;->calculateVersion(Lcom/android/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/org/bouncycastle/asn1/ASN1Set;Lcom/android/org/bouncycastle/asn1/ASN1Set;Lcom/android/org/bouncycastle/asn1/ASN1Set;)Lcom/android/org/bouncycastle/asn1/ASN1Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/android/org/bouncycastle/asn1/cms/SignedData;->version:Lcom/android/org/bouncycastle/asn1/ASN1Integer;

    iput-object p1, p0, Lcom/android/org/bouncycastle/asn1/cms/SignedData;->digestAlgorithms:Lcom/android/org/bouncycastle/asn1/ASN1Set;

    iput-object p2, p0, Lcom/android/org/bouncycastle/asn1/cms/SignedData;->contentInfo:Lcom/android/org/bouncycastle/asn1/cms/ContentInfo;

    iput-object p3, p0, Lcom/android/org/bouncycastle/asn1/cms/SignedData;->certificates:Lcom/android/org/bouncycastle/asn1/ASN1Set;

    iput-object p4, p0, Lcom/android/org/bouncycastle/asn1/cms/SignedData;->crls:Lcom/android/org/bouncycastle/asn1/ASN1Set;

    iput-object p5, p0, Lcom/android/org/bouncycastle/asn1/cms/SignedData;->signerInfos:Lcom/android/org/bouncycastle/asn1/ASN1Set;

    instance-of v0, p4, Lcom/android/org/bouncycastle/asn1/BERSet;

    iput-boolean v0, p0, Lcom/android/org/bouncycastle/asn1/cms/SignedData;->crlsBer:Z

    instance-of v0, p3, Lcom/android/org/bouncycastle/asn1/BERSet;

    iput-boolean v0, p0, Lcom/android/org/bouncycastle/asn1/cms/SignedData;->certsBer:Z

    return-void
.end method

.method private calculateVersion(Lcom/android/org/bouncycastle/asn1/ASN1ObjectIdentifier;Lcom/android/org/bouncycastle/asn1/ASN1Set;Lcom/android/org/bouncycastle/asn1/ASN1Set;Lcom/android/org/bouncycastle/asn1/ASN1Set;)Lcom/android/org/bouncycastle/asn1/ASN1Integer;
    .locals 9
    .param p1    # Lcom/android/org/bouncycastle/asn1/ASN1ObjectIdentifier;
    .param p2    # Lcom/android/org/bouncycastle/asn1/ASN1Set;
    .param p3    # Lcom/android/org/bouncycastle/asn1/ASN1Set;
    .param p4    # Lcom/android/org/bouncycastle/asn1/ASN1Set;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x0

    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lcom/android/org/bouncycastle/asn1/ASN1Set;->getObjects()Ljava/util/Enumeration;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    instance-of v7, v3, Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;

    if-eqz v7, :cond_0

    invoke-static {v3}, Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;->getInstance(Ljava/lang/Object;)Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;

    move-result-object v6

    invoke-virtual {v6}, Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;->getTagNo()I

    move-result v7

    const/4 v8, 0x1

    if-ne v7, v8, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v6}, Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;->getTagNo()I

    move-result v7

    const/4 v8, 0x2

    if-ne v7, v8, :cond_2

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {v6}, Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;->getTagNo()I

    move-result v7

    const/4 v8, 0x3

    if-ne v7, v8, :cond_0

    const/4 v4, 0x1

    goto :goto_0

    :cond_3
    if-eqz v4, :cond_4

    new-instance v7, Lcom/android/org/bouncycastle/asn1/ASN1Integer;

    const/4 v8, 0x5

    invoke-direct {v7, v8}, Lcom/android/org/bouncycastle/asn1/ASN1Integer;-><init>(I)V

    :goto_1
    return-object v7

    :cond_4
    if-eqz p3, :cond_6

    invoke-virtual {p3}, Lcom/android/org/bouncycastle/asn1/ASN1Set;->getObjects()Ljava/util/Enumeration;

    move-result-object v2

    :cond_5
    :goto_2
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v3

    instance-of v7, v3, Lcom/android/org/bouncycastle/asn1/ASN1TaggedObject;

    if-eqz v7, :cond_5

    const/4 v5, 0x1

    goto :goto_2

    :cond_6
    if-eqz v5, :cond_7

    new-instance v7, Lcom/android/org/bouncycastle/asn1/ASN1Integer;

    const/4 v8, 0x5

    invoke-direct {v7, v8}, Lcom/android/org/bouncycastle/asn1/ASN1Integer;-><init>(I)V

    goto :goto_1

    :cond_7
    if-eqz v1, :cond_8

    new-instance v7, Lcom/android/org/bouncycastle/asn1/ASN1Integer;

    const/4 v8, 0x4

    invoke-direct {v7, v8}, Lcom/android/org/bouncycastle/asn1/ASN1Integer;-><init>(I)V

    goto :goto_1

    :cond_8
    if-eqz v0, :cond_9

    new-instance v7, Lcom/android/org/bouncycastle/asn1/ASN1Integer;

    const/4 v8, 0x3

    invoke-direct {v7, v8}, Lcom/android/org/bouncycastle/asn1/ASN1Integer;-><init>(I)V

    goto :goto_1

    :cond_9
    invoke-direct {p0, p4}, Lcom/android/org/bouncycastle/asn1/cms/SignedData;->checkForVersion3(Lcom/android/org/bouncycastle/asn1/ASN1Set;)Z

    move-result v7

    if-eqz v7, :cond_a

    new-instance v7, Lcom/android/org/bouncycastle/asn1/ASN1Integer;

    const/4 v8, 0x3

    invoke-direct {v7, v8}, Lcom/android/org/bouncycastle/asn1/ASN1Integer;-><init>(I)V

    goto :goto_1

    :cond_a
    sget-object v7, Lcom/android/org/bouncycastle/asn1/cms/CMSObjectIdentifiers;->data:Lcom/android/org/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v7, p1}, Lcom/android/org/bouncycastle/asn1/ASN1ObjectIdentifier;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_b

    new-instance v7, Lcom/android/org/bouncycastle/asn1/ASN1Integer;

    const/4 v8, 0x3

    invoke-direct {v7, v8}, Lcom/android/org/bouncycastle/asn1/ASN1Integer;-><init>(I)V

    goto :goto_1

    :cond_b
    new-instance v7, Lcom/android/org/bouncycastle/asn1/ASN1Integer;

    const/4 v8, 0x1

    invoke-direct {v7, v8}, Lcom/android/org/bouncycastle/asn1/ASN1Integer;-><init>(I)V

    goto :goto_1
.end method

.method private checkForVersion3(Lcom/android/org/bouncycastle/asn1/ASN1Set;)Z
    .locals 4
    .param p1    # Lcom/android/org/bouncycastle/asn1/ASN1Set;

    invoke-virtual {p1}, Lcom/android/org/bouncycastle/asn1/ASN1Set;->getObjects()Ljava/util/Enumeration;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lcom/android/org/bouncycastle/asn1/cms/SignerInfo;->getInstance(Ljava/lang/Object;)Lcom/android/org/bouncycastle/asn1/cms/SignerInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/org/bouncycastle/asn1/cms/SignerInfo;->getVersion()Lcom/android/org/bouncycastle/asn1/ASN1Integer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/org/bouncycastle/asn1/ASN1Integer;->getValue()Ljava/math/BigInteger;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigInteger;->intValue()I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static getInstance(Ljava/lang/Object;)Lcom/android/org/bouncycastle/asn1/cms/SignedData;
    .locals 2
    .param p0    # Ljava/lang/Object;

    instance-of v0, p0, Lcom/android/org/bouncycastle/asn1/cms/SignedData;

    if-eqz v0, :cond_0

    check-cast p0, Lcom/android/org/bouncycastle/asn1/cms/SignedData;

    :goto_0
    return-object p0

    :cond_0
    if-eqz p0, :cond_1

    new-instance v0, Lcom/android/org/bouncycastle/asn1/cms/SignedData;

    invoke-static {p0}, Lcom/android/org/bouncycastle/asn1/ASN1Sequence;->getInstance(Ljava/lang/Object;)Lcom/android/org/bouncycastle/asn1/ASN1Sequence;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/org/bouncycastle/asn1/cms/SignedData;-><init>(Lcom/android/org/bouncycastle/asn1/ASN1Sequence;)V

    move-object p0, v0

    goto :goto_0

    :cond_1
    const/4 p0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getCRLs()Lcom/android/org/bouncycastle/asn1/ASN1Set;
    .locals 1

    iget-object v0, p0, Lcom/android/org/bouncycastle/asn1/cms/SignedData;->crls:Lcom/android/org/bouncycastle/asn1/ASN1Set;

    return-object v0
.end method

.method public getCertificates()Lcom/android/org/bouncycastle/asn1/ASN1Set;
    .locals 1

    iget-object v0, p0, Lcom/android/org/bouncycastle/asn1/cms/SignedData;->certificates:Lcom/android/org/bouncycastle/asn1/ASN1Set;

    return-object v0
.end method

.method public getDigestAlgorithms()Lcom/android/org/bouncycastle/asn1/ASN1Set;
    .locals 1

    iget-object v0, p0, Lcom/android/org/bouncycastle/asn1/cms/SignedData;->digestAlgorithms:Lcom/android/org/bouncycastle/asn1/ASN1Set;

    return-object v0
.end method

.method public getEncapContentInfo()Lcom/android/org/bouncycastle/asn1/cms/ContentInfo;
    .locals 1

    iget-object v0, p0, Lcom/android/org/bouncycastle/asn1/cms/SignedData;->contentInfo:Lcom/android/org/bouncycastle/asn1/cms/ContentInfo;

    return-object v0
.end method

.method public getSignerInfos()Lcom/android/org/bouncycastle/asn1/ASN1Set;
    .locals 1

    iget-object v0, p0, Lcom/android/org/bouncycastle/asn1/cms/SignedData;->signerInfos:Lcom/android/org/bouncycastle/asn1/ASN1Set;

    return-object v0
.end method

.method public getVersion()Lcom/android/org/bouncycastle/asn1/ASN1Integer;
    .locals 1

    iget-object v0, p0, Lcom/android/org/bouncycastle/asn1/cms/SignedData;->version:Lcom/android/org/bouncycastle/asn1/ASN1Integer;

    return-object v0
.end method

.method public toASN1Primitive()Lcom/android/org/bouncycastle/asn1/ASN1Primitive;
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;

    invoke-direct {v0}, Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;-><init>()V

    iget-object v1, p0, Lcom/android/org/bouncycastle/asn1/cms/SignedData;->version:Lcom/android/org/bouncycastle/asn1/ASN1Integer;

    invoke-virtual {v0, v1}, Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/org/bouncycastle/asn1/ASN1Encodable;)V

    iget-object v1, p0, Lcom/android/org/bouncycastle/asn1/cms/SignedData;->digestAlgorithms:Lcom/android/org/bouncycastle/asn1/ASN1Set;

    invoke-virtual {v0, v1}, Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/org/bouncycastle/asn1/ASN1Encodable;)V

    iget-object v1, p0, Lcom/android/org/bouncycastle/asn1/cms/SignedData;->contentInfo:Lcom/android/org/bouncycastle/asn1/cms/ContentInfo;

    invoke-virtual {v0, v1}, Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/org/bouncycastle/asn1/ASN1Encodable;)V

    iget-object v1, p0, Lcom/android/org/bouncycastle/asn1/cms/SignedData;->certificates:Lcom/android/org/bouncycastle/asn1/ASN1Set;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/android/org/bouncycastle/asn1/cms/SignedData;->certsBer:Z

    if-eqz v1, :cond_2

    new-instance v1, Lcom/android/org/bouncycastle/asn1/BERTaggedObject;

    iget-object v2, p0, Lcom/android/org/bouncycastle/asn1/cms/SignedData;->certificates:Lcom/android/org/bouncycastle/asn1/ASN1Set;

    invoke-direct {v1, v3, v3, v2}, Lcom/android/org/bouncycastle/asn1/BERTaggedObject;-><init>(ZILcom/android/org/bouncycastle/asn1/ASN1Encodable;)V

    invoke-virtual {v0, v1}, Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/org/bouncycastle/asn1/ASN1Encodable;)V

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/org/bouncycastle/asn1/cms/SignedData;->crls:Lcom/android/org/bouncycastle/asn1/ASN1Set;

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/android/org/bouncycastle/asn1/cms/SignedData;->crlsBer:Z

    if-eqz v1, :cond_3

    new-instance v1, Lcom/android/org/bouncycastle/asn1/BERTaggedObject;

    iget-object v2, p0, Lcom/android/org/bouncycastle/asn1/cms/SignedData;->crls:Lcom/android/org/bouncycastle/asn1/ASN1Set;

    invoke-direct {v1, v3, v4, v2}, Lcom/android/org/bouncycastle/asn1/BERTaggedObject;-><init>(ZILcom/android/org/bouncycastle/asn1/ASN1Encodable;)V

    invoke-virtual {v0, v1}, Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/org/bouncycastle/asn1/ASN1Encodable;)V

    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/android/org/bouncycastle/asn1/cms/SignedData;->signerInfos:Lcom/android/org/bouncycastle/asn1/ASN1Set;

    invoke-virtual {v0, v1}, Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/org/bouncycastle/asn1/ASN1Encodable;)V

    new-instance v1, Lcom/android/org/bouncycastle/asn1/BERSequence;

    invoke-direct {v1, v0}, Lcom/android/org/bouncycastle/asn1/BERSequence;-><init>(Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;)V

    return-object v1

    :cond_2
    new-instance v1, Lcom/android/org/bouncycastle/asn1/DERTaggedObject;

    iget-object v2, p0, Lcom/android/org/bouncycastle/asn1/cms/SignedData;->certificates:Lcom/android/org/bouncycastle/asn1/ASN1Set;

    invoke-direct {v1, v3, v3, v2}, Lcom/android/org/bouncycastle/asn1/DERTaggedObject;-><init>(ZILcom/android/org/bouncycastle/asn1/ASN1Encodable;)V

    invoke-virtual {v0, v1}, Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/org/bouncycastle/asn1/ASN1Encodable;)V

    goto :goto_0

    :cond_3
    new-instance v1, Lcom/android/org/bouncycastle/asn1/DERTaggedObject;

    iget-object v2, p0, Lcom/android/org/bouncycastle/asn1/cms/SignedData;->crls:Lcom/android/org/bouncycastle/asn1/ASN1Set;

    invoke-direct {v1, v3, v4, v2}, Lcom/android/org/bouncycastle/asn1/DERTaggedObject;-><init>(ZILcom/android/org/bouncycastle/asn1/ASN1Encodable;)V

    invoke-virtual {v0, v1}, Lcom/android/org/bouncycastle/asn1/ASN1EncodableVector;->add(Lcom/android/org/bouncycastle/asn1/ASN1Encodable;)V

    goto :goto_1
.end method
