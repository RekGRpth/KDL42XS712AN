.class public Lcom/android/org/bouncycastle/asn1/ASN1Enumerated;
.super Lcom/android/org/bouncycastle/asn1/DEREnumerated;
.source "ASN1Enumerated.java"


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1    # I

    invoke-direct {p0, p1}, Lcom/android/org/bouncycastle/asn1/DEREnumerated;-><init>(I)V

    return-void
.end method

.method public constructor <init>(Ljava/math/BigInteger;)V
    .locals 0
    .param p1    # Ljava/math/BigInteger;

    invoke-direct {p0, p1}, Lcom/android/org/bouncycastle/asn1/DEREnumerated;-><init>(Ljava/math/BigInteger;)V

    return-void
.end method

.method constructor <init>([B)V
    .locals 0
    .param p1    # [B

    invoke-direct {p0, p1}, Lcom/android/org/bouncycastle/asn1/DEREnumerated;-><init>([B)V

    return-void
.end method
